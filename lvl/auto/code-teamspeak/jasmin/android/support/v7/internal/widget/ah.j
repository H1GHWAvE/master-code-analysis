.bytecode 50.0
.class public synchronized android/support/v7/internal/widget/ah
.super android/widget/ListView

.field public static final 'a' I = -1


.field public static final 'b' I = -1


.field private static final 'h' [I

.field final 'c' Landroid/graphics/Rect;

.field 'd' I

.field 'e' I

.field 'f' I

.field 'g' I

.field private 'i' Ljava/lang/reflect/Field;

.field private 'j' Landroid/support/v7/internal/widget/ai;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
iconst_0
iastore
putstatic android/support/v7/internal/widget/ah/h [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/internal/widget/ah/<init>(Landroid/content/Context;B)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Landroid/content/Context;B)V
aload 0
aload 1
iconst_0
invokespecial android/support/v7/internal/widget/ah/<init>(Landroid/content/Context;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;I)V
.catch java/lang/NoSuchFieldException from L0 to L1 using L2
aload 0
aload 1
aconst_null
iload 2
invokespecial android/widget/ListView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
aload 0
iconst_0
putfield android/support/v7/internal/widget/ah/d I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ah/e I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ah/f I
aload 0
iconst_0
putfield android/support/v7/internal/widget/ah/g I
L0:
aload 0
ldc android/widget/AbsListView
ldc "mIsChildViewEnabled"
invokevirtual java/lang/Class/getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
putfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
iconst_1
invokevirtual java/lang/reflect/Field/setAccessible(Z)V
L1:
return
L2:
astore 1
aload 1
invokevirtual java/lang/NoSuchFieldException/printStackTrace()V
return
.limit locals 3
.limit stack 4
.end method

.method private a(ILandroid/view/View;)V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L2
.catch java/lang/IllegalAccessException from L5 to L6 using L2
iconst_1
istore 7
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 8
aload 8
ifnull L7
iload 1
iconst_m1
if_icmpeq L7
iconst_1
istore 5
L8:
iload 5
ifeq L9
aload 8
iconst_0
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
L9:
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
astore 9
aload 9
aload 2
invokevirtual android/view/View/getLeft()I
aload 2
invokevirtual android/view/View/getTop()I
aload 2
invokevirtual android/view/View/getRight()I
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/Rect/set(IIII)V
aload 9
aload 9
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/internal/widget/ah/d I
isub
putfield android/graphics/Rect/left I
aload 9
aload 9
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/internal/widget/ah/e I
isub
putfield android/graphics/Rect/top I
aload 9
aload 9
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v7/internal/widget/ah/f I
iadd
putfield android/graphics/Rect/right I
aload 9
aload 9
getfield android/graphics/Rect/bottom I
aload 0
getfield android/support/v7/internal/widget/ah/g I
iadd
putfield android/graphics/Rect/bottom I
L0:
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/getBoolean(Ljava/lang/Object;)Z
istore 6
aload 2
invokevirtual android/view/View/isEnabled()Z
iload 6
if_icmpeq L6
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
astore 2
L1:
iload 6
ifne L10
iconst_1
istore 6
L3:
aload 2
aload 0
iload 6
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L4:
iload 1
iconst_m1
if_icmpeq L6
L5:
aload 0
invokevirtual android/support/v7/internal/widget/ah/refreshDrawableState()V
L6:
iload 5
ifeq L11
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
astore 2
aload 2
invokevirtual android/graphics/Rect/exactCenterX()F
fstore 3
aload 2
invokevirtual android/graphics/Rect/exactCenterY()F
fstore 4
aload 0
invokevirtual android/support/v7/internal/widget/ah/getVisibility()I
ifne L12
iload 7
istore 6
L13:
aload 8
iload 6
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
aload 8
fload 3
fload 4
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;FF)V
L11:
return
L7:
iconst_0
istore 5
goto L8
L10:
iconst_0
istore 6
goto L3
L2:
astore 2
aload 2
invokevirtual java/lang/IllegalAccessException/printStackTrace()V
goto L6
L12:
iconst_0
istore 6
goto L13
.limit locals 10
.limit stack 5
.end method

.method private a(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifne L0
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L0
aload 2
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
aload 2
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
return
.limit locals 3
.limit stack 2
.end method

.method private b()V
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L0
aload 0
invokevirtual android/support/v7/internal/widget/ah/a()Z
ifeq L1
aload 0
invokevirtual android/support/v7/internal/widget/ah/isPressed()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
aload 2
aload 0
invokevirtual android/support/v7/internal/widget/ah/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L0:
return
L1:
iconst_0
istore 1
goto L2
.limit locals 3
.limit stack 2
.end method

.method private b(ILandroid/view/View;)V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L2
.catch java/lang/IllegalAccessException from L5 to L6 using L2
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
astore 4
aload 4
aload 2
invokevirtual android/view/View/getLeft()I
aload 2
invokevirtual android/view/View/getTop()I
aload 2
invokevirtual android/view/View/getRight()I
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/Rect/set(IIII)V
aload 4
aload 4
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/internal/widget/ah/d I
isub
putfield android/graphics/Rect/left I
aload 4
aload 4
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/internal/widget/ah/e I
isub
putfield android/graphics/Rect/top I
aload 4
aload 4
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v7/internal/widget/ah/f I
iadd
putfield android/graphics/Rect/right I
aload 4
aload 4
getfield android/graphics/Rect/bottom I
aload 0
getfield android/support/v7/internal/widget/ah/g I
iadd
putfield android/graphics/Rect/bottom I
L0:
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/getBoolean(Ljava/lang/Object;)Z
istore 3
aload 2
invokevirtual android/view/View/isEnabled()Z
iload 3
if_icmpeq L6
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
astore 2
L1:
iload 3
ifne L7
iconst_1
istore 3
L3:
aload 2
aload 0
iload 3
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L4:
iload 1
iconst_m1
if_icmpeq L6
L5:
aload 0
invokevirtual android/support/v7/internal/widget/ah/refreshDrawableState()V
L6:
return
L7:
iconst_0
istore 3
goto L3
L2:
astore 2
aload 2
invokevirtual java/lang/IllegalAccessException/printStackTrace()V
return
.limit locals 5
.limit stack 5
.end method

.method private c()Z
aload 0
invokevirtual android/support/v7/internal/widget/ah/a()Z
ifeq L0
aload 0
invokevirtual android/support/v7/internal/widget/ah/isPressed()Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(II)I
aload 0
invokevirtual android/support/v7/internal/widget/ah/getListPaddingTop()I
istore 3
aload 0
invokevirtual android/support/v7/internal/widget/ah/getListPaddingBottom()I
istore 5
aload 0
invokevirtual android/support/v7/internal/widget/ah/getListPaddingLeft()I
pop
aload 0
invokevirtual android/support/v7/internal/widget/ah/getListPaddingRight()I
pop
aload 0
invokevirtual android/support/v7/internal/widget/ah/getDividerHeight()I
istore 4
aload 0
invokevirtual android/support/v7/internal/widget/ah/getDivider()Landroid/graphics/drawable/Drawable;
astore 9
aload 0
invokevirtual android/support/v7/internal/widget/ah/getAdapter()Landroid/widget/ListAdapter;
astore 10
aload 10
ifnonnull L0
iload 3
iload 5
iadd
istore 3
L1:
iload 3
ireturn
L0:
iload 5
iload 3
iadd
istore 3
iload 4
ifle L2
aload 9
ifnull L2
L3:
aload 10
invokeinterface android/widget/ListAdapter/getCount()I 0
istore 8
iconst_0
istore 5
iconst_0
istore 6
aconst_null
astore 9
L4:
iload 5
iload 8
if_icmpge L5
aload 10
iload 5
invokeinterface android/widget/ListAdapter/getItemViewType(I)I 1
istore 7
iload 7
iload 6
if_icmpeq L6
iload 7
istore 6
aconst_null
astore 9
L7:
aload 10
iload 5
aload 9
aload 0
invokeinterface android/widget/ListAdapter/getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View; 3
astore 9
aload 9
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 11
aload 11
ifnull L8
aload 11
getfield android/view/ViewGroup$LayoutParams/height I
ifle L8
aload 11
getfield android/view/ViewGroup$LayoutParams/height I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 7
L9:
aload 9
iload 1
iload 7
invokevirtual android/view/View/measure(II)V
iload 5
ifle L10
iload 3
iload 4
iadd
istore 3
L11:
aload 9
invokevirtual android/view/View/getMeasuredHeight()I
iload 3
iadd
istore 7
iload 2
istore 3
iload 7
iload 2
if_icmpge L1
iload 5
iconst_1
iadd
istore 5
iload 7
istore 3
goto L4
L2:
iconst_0
istore 4
goto L3
L8:
iconst_0
iconst_0
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 7
goto L9
L5:
iload 3
ireturn
L6:
goto L7
L10:
goto L11
.limit locals 12
.limit stack 4
.end method

.method public final a(IZ)I
aload 0
invokevirtual android/support/v7/internal/widget/ah/getAdapter()Landroid/widget/ListAdapter;
astore 5
aload 5
ifnull L0
aload 0
invokevirtual android/support/v7/internal/widget/ah/isInTouchMode()Z
ifeq L1
L0:
iconst_m1
istore 3
L2:
iload 3
ireturn
L1:
aload 5
invokeinterface android/widget/ListAdapter/getCount()I 0
istore 4
aload 0
invokevirtual android/support/v7/internal/widget/ah/getAdapter()Landroid/widget/ListAdapter;
invokeinterface android/widget/ListAdapter/areAllItemsEnabled()Z 0
ifne L3
iload 2
ifeq L4
iconst_0
iload 1
invokestatic java/lang/Math/max(II)I
istore 3
L5:
iload 3
istore 1
iload 3
iload 4
if_icmpge L6
iload 3
istore 1
aload 5
iload 3
invokeinterface android/widget/ListAdapter/isEnabled(I)Z 1
ifne L6
iload 3
iconst_1
iadd
istore 3
goto L5
L4:
iload 1
iload 4
iconst_1
isub
invokestatic java/lang/Math/min(II)I
istore 3
L7:
iload 3
istore 1
iload 3
iflt L6
iload 3
istore 1
aload 5
iload 3
invokeinterface android/widget/ListAdapter/isEnabled(I)Z 1
ifne L6
iload 3
iconst_1
isub
istore 3
goto L7
L6:
iload 1
iflt L8
iload 1
istore 3
iload 1
iload 4
if_icmplt L2
L8:
iconst_m1
ireturn
L3:
iload 1
iflt L9
iload 1
istore 3
iload 1
iload 4
if_icmplt L2
L9:
iconst_m1
ireturn
.limit locals 6
.limit stack 3
.end method

.method public final a(ILandroid/view/View;FF)V
.catch java/lang/IllegalAccessException from L0 to L1 using L2
.catch java/lang/IllegalAccessException from L3 to L4 using L2
.catch java/lang/IllegalAccessException from L5 to L6 using L2
iconst_1
istore 9
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 10
aload 10
ifnull L7
iload 1
iconst_m1
if_icmpeq L7
iconst_1
istore 7
L8:
iload 7
ifeq L9
aload 10
iconst_0
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
L9:
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
astore 11
aload 11
aload 2
invokevirtual android/view/View/getLeft()I
aload 2
invokevirtual android/view/View/getTop()I
aload 2
invokevirtual android/view/View/getRight()I
aload 2
invokevirtual android/view/View/getBottom()I
invokevirtual android/graphics/Rect/set(IIII)V
aload 11
aload 11
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/internal/widget/ah/d I
isub
putfield android/graphics/Rect/left I
aload 11
aload 11
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/internal/widget/ah/e I
isub
putfield android/graphics/Rect/top I
aload 11
aload 11
getfield android/graphics/Rect/right I
aload 0
getfield android/support/v7/internal/widget/ah/f I
iadd
putfield android/graphics/Rect/right I
aload 11
aload 11
getfield android/graphics/Rect/bottom I
aload 0
getfield android/support/v7/internal/widget/ah/g I
iadd
putfield android/graphics/Rect/bottom I
L0:
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
aload 0
invokevirtual java/lang/reflect/Field/getBoolean(Ljava/lang/Object;)Z
istore 8
aload 2
invokevirtual android/view/View/isEnabled()Z
iload 8
if_icmpeq L6
aload 0
getfield android/support/v7/internal/widget/ah/i Ljava/lang/reflect/Field;
astore 2
L1:
iload 8
ifne L10
iconst_1
istore 8
L3:
aload 2
aload 0
iload 8
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
invokevirtual java/lang/reflect/Field/set(Ljava/lang/Object;Ljava/lang/Object;)V
L4:
iload 1
iconst_m1
if_icmpeq L6
L5:
aload 0
invokevirtual android/support/v7/internal/widget/ah/refreshDrawableState()V
L6:
iload 7
ifeq L11
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
astore 2
aload 2
invokevirtual android/graphics/Rect/exactCenterX()F
fstore 5
aload 2
invokevirtual android/graphics/Rect/exactCenterY()F
fstore 6
aload 0
invokevirtual android/support/v7/internal/widget/ah/getVisibility()I
ifne L12
iload 9
istore 8
L13:
aload 10
iload 8
iconst_0
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
pop
aload 10
fload 5
fload 6
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;FF)V
L11:
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L14
iload 1
iconst_m1
if_icmpeq L14
aload 2
fload 3
fload 4
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;FF)V
L14:
return
L7:
iconst_0
istore 7
goto L8
L10:
iconst_0
istore 8
goto L3
L2:
astore 2
aload 2
invokevirtual java/lang/IllegalAccessException/printStackTrace()V
goto L6
L12:
iconst_0
istore 8
goto L13
.limit locals 12
.limit stack 5
.end method

.method public a()Z
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/isEmpty()Z
ifne L0
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L0
aload 2
aload 0
getfield android/support/v7/internal/widget/ah/c Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
aload 2
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
L0:
aload 0
aload 1
invokespecial android/widget/ListView/dispatchDraw(Landroid/graphics/Canvas;)V
return
.limit locals 3
.limit stack 2
.end method

.method public drawableStateChanged()V
iconst_1
istore 1
aload 0
invokespecial android/widget/ListView/drawableStateChanged()V
aload 0
iconst_1
invokevirtual android/support/v7/internal/widget/ah/setSelectorEnabled(Z)V
aload 0
invokevirtual android/support/v7/internal/widget/ah/getSelector()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L0
aload 0
invokevirtual android/support/v7/internal/widget/ah/a()Z
ifeq L1
aload 0
invokevirtual android/support/v7/internal/widget/ah/isPressed()Z
ifeq L1
L2:
iload 1
ifeq L0
aload 2
aload 0
invokevirtual android/support/v7/internal/widget/ah/getDrawableState()[I
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
pop
L0:
return
L1:
iconst_0
istore 1
goto L2
.limit locals 3
.limit stack 2
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
aload 1
ifnull L0
new android/support/v7/internal/widget/ai
dup
aload 1
invokespecial android/support/v7/internal/widget/ai/<init>(Landroid/graphics/drawable/Drawable;)V
astore 2
L1:
aload 0
aload 2
putfield android/support/v7/internal/widget/ah/j Landroid/support/v7/internal/widget/ai;
aload 0
aload 0
getfield android/support/v7/internal/widget/ah/j Landroid/support/v7/internal/widget/ai;
invokespecial android/widget/ListView/setSelector(Landroid/graphics/drawable/Drawable;)V
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 2
aload 1
ifnull L2
aload 1
aload 2
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
L2:
aload 0
aload 2
getfield android/graphics/Rect/left I
putfield android/support/v7/internal/widget/ah/d I
aload 0
aload 2
getfield android/graphics/Rect/top I
putfield android/support/v7/internal/widget/ah/e I
aload 0
aload 2
getfield android/graphics/Rect/right I
putfield android/support/v7/internal/widget/ah/f I
aload 0
aload 2
getfield android/graphics/Rect/bottom I
putfield android/support/v7/internal/widget/ah/g I
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 3
.end method

.method public setSelectorEnabled(Z)V
aload 0
getfield android/support/v7/internal/widget/ah/j Landroid/support/v7/internal/widget/ai;
ifnull L0
aload 0
getfield android/support/v7/internal/widget/ah/j Landroid/support/v7/internal/widget/ai;
iload 1
putfield android/support/v7/internal/widget/ai/a Z
L0:
return
.limit locals 2
.limit stack 2
.end method
