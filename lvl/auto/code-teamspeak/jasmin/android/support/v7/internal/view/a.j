.bytecode 50.0
.class public final synchronized android/support/v7/internal/view/a
.super java/lang/Object

.field public 'a' Landroid/content/Context;

.method private <init>(Landroid/content/Context;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/internal/view/a/a Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v7/internal/view/a;
new android/support/v7/internal/view/a
dup
aload 0
invokespecial android/support/v7/internal/view/a/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private d()I
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/j/abc_max_action_buttons I
invokevirtual android/content/res/Resources/getInteger(I)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method private e()Z
getstatic android/os/Build$VERSION/SDK_INT I
bipush 19
if_icmplt L0
L1:
iconst_1
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokestatic android/view/ViewConfiguration/get(Landroid/content/Context;)Landroid/view/ViewConfiguration;
invokestatic android/support/v4/view/du/b(Landroid/view/ViewConfiguration;)Z
ifeq L1
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private f()I
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
iconst_2
idiv
ireturn
.limit locals 1
.limit stack 2
.end method

.method private g()Z
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
bipush 14
if_icmpge L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final a()Z
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getApplicationInfo()Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/targetSdkVersion I
bipush 16
if_icmplt L0
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/e/abc_action_bar_embed_tabs I
invokevirtual android/content/res/Resources/getBoolean(I)Z
ireturn
L0:
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/e/abc_action_bar_embed_tabs_pre_jb I
invokevirtual android/content/res/Resources/getBoolean(I)Z
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final b()I
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/n/ActionBar [I
getstatic android/support/v7/a/d/actionBarStyle I
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 3
aload 3
getstatic android/support/v7/a/n/ActionBar_height I
iconst_0
invokevirtual android/content/res/TypedArray/getLayoutDimension(II)I
istore 2
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 4
iload 2
istore 1
aload 0
invokevirtual android/support/v7/internal/view/a/a()Z
ifne L0
iload 2
aload 4
getstatic android/support/v7/a/g/abc_action_bar_stacked_max_height I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
invokestatic java/lang/Math/min(II)I
istore 1
L0:
aload 3
invokevirtual android/content/res/TypedArray/recycle()V
iload 1
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final c()I
aload 0
getfield android/support/v7/internal/view/a/a Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/g/abc_action_bar_stacked_tab_max_width I
invokevirtual android/content/res/Resources/getDimensionPixelSize(I)I
ireturn
.limit locals 1
.limit stack 2
.end method
