.bytecode 50.0
.class public synchronized android/support/v7/internal/view/menu/ActionMenuItemView
.super android/support/v7/widget/ai
.implements android/support/v7/internal/view/menu/aa
.implements android/support/v7/widget/k
.implements android/view/View$OnClickListener
.implements android/view/View$OnLongClickListener

.field private static final 'a' Ljava/lang/String; = "ActionMenuItemView"

.field private static final 'l' I = 32


.field private 'b' Landroid/support/v7/internal/view/menu/m;

.field private 'c' Ljava/lang/CharSequence;

.field private 'd' Landroid/graphics/drawable/Drawable;

.field private 'e' Landroid/support/v7/internal/view/menu/k;

.field private 'f' Landroid/support/v7/widget/as;

.field private 'g' Landroid/support/v7/internal/view/menu/c;

.field private 'h' Z

.field private 'i' Z

.field private 'j' I

.field private 'k' I

.field private 'm' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/internal/view/menu/ActionMenuItemView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/internal/view/menu/ActionMenuItemView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
invokespecial android/support/v7/widget/ai/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
astore 4
aload 0
aload 4
getstatic android/support/v7/a/e/abc_config_allowActionMenuItemTextWithIcon I
invokevirtual android/content/res/Resources/getBoolean(I)Z
putfield android/support/v7/internal/view/menu/ActionMenuItemView/h Z
aload 1
aload 2
getstatic android/support/v7/a/n/ActionMenuItemView [I
iload 3
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/ActionMenuItemView_android_minWidth I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelSize(II)I
putfield android/support/v7/internal/view/menu/ActionMenuItemView/j I
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 4
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 32.0F
fmul
ldc_w 0.5F
fadd
f2i
putfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
aload 0
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
aload 0
iconst_m1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/k I
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/ActionMenuItemView;)Landroid/support/v7/internal/view/menu/c;
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/g Landroid/support/v7/internal/view/menu/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Landroid/support/v7/internal/view/menu/ActionMenuItemView;)Landroid/support/v7/internal/view/menu/k;
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/e Landroid/support/v7/internal/view/menu/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Landroid/support/v7/internal/view/menu/ActionMenuItemView;)Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
iconst_0
istore 4
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/c Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
istore 1
L1:
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/d Landroid/graphics/drawable/Drawable;
ifnull L2
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
getfield android/support/v7/internal/view/menu/m/h I
iconst_4
iand
iconst_4
if_icmpne L3
iconst_1
istore 2
L4:
iload 4
istore 3
iload 2
ifeq L5
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/h Z
ifne L2
iload 4
istore 3
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/i Z
ifeq L5
L2:
iconst_1
istore 3
L5:
iload 1
iload 3
iand
ifeq L6
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/c Ljava/lang/CharSequence;
astore 5
L7:
aload 0
aload 5
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setText(Ljava/lang/CharSequence;)V
return
L0:
iconst_0
istore 1
goto L1
L3:
iconst_0
istore 2
goto L4
L6:
aconst_null
astore 5
goto L7
.limit locals 6
.limit stack 2
.end method

.method public final a(Landroid/support/v7/internal/view/menu/m;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setIcon(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
aload 0
invokevirtual android/support/v7/internal/view/menu/m/a(Landroid/support/v7/internal/view/menu/aa;)Ljava/lang/CharSequence;
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setId(I)V
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isVisible()Z
ifeq L0
iconst_0
istore 2
L1:
aload 0
iload 2
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setVisibility(I)V
aload 0
aload 1
invokevirtual android/support/v7/internal/view/menu/m/isEnabled()Z
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setEnabled(Z)V
aload 1
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifeq L2
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/f Landroid/support/v7/widget/as;
ifnonnull L2
aload 0
new android/support/v7/internal/view/menu/b
dup
aload 0
invokespecial android/support/v7/internal/view/menu/b/<init>(Landroid/support/v7/internal/view/menu/ActionMenuItemView;)V
putfield android/support/v7/internal/view/menu/ActionMenuItemView/f Landroid/support/v7/widget/as;
L2:
return
L0:
bipush 8
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public final a()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()Z
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getText()Ljava/lang/CharSequence;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final d()Z
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getIcon()Landroid/graphics/drawable/Drawable;
ifnonnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final e()Z
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getItemData()Landroid/support/v7/internal/view/menu/m;
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method public onClick(Landroid/view/View;)V
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/e Landroid/support/v7/internal/view/menu/k;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/e Landroid/support/v7/internal/view/menu/k;
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokeinterface android/support/v7/internal/view/menu/k/a(Landroid/support/v7/internal/view/menu/m;)Z 1
pop
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 8
if_icmplt L0
aload 0
aload 1
invokespecial android/support/v7/widget/ai/onConfigurationChanged(Landroid/content/res/Configuration;)V
L0:
aload 0
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
getstatic android/support/v7/a/e/abc_config_allowActionMenuItemTextWithIcon I
invokevirtual android/content/res/Resources/getBoolean(I)Z
putfield android/support/v7/internal/view/menu/ActionMenuItemView/h Z
aload 0
invokespecial android/support/v7/internal/view/menu/ActionMenuItemView/f()V
return
.limit locals 2
.limit stack 3
.end method

.method public onLongClick(Landroid/view/View;)Z
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
ifeq L0
iconst_0
ireturn
L0:
iconst_2
newarray int
astore 7
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
astore 8
aload 0
aload 7
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getLocationOnScreen([I)V
aload 0
aload 8
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getContext()Landroid/content/Context;
astore 9
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getWidth()I
istore 2
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getHeight()I
istore 4
aload 7
iconst_1
iaload
istore 5
iload 4
iconst_2
idiv
istore 6
aload 7
iconst_0
iaload
istore 3
iload 2
iconst_2
idiv
iload 3
iadd
istore 3
iload 3
istore 2
aload 1
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
ifne L1
aload 9
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
iload 3
isub
istore 2
L1:
aload 9
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getTitle()Ljava/lang/CharSequence;
iconst_0
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
astore 1
iload 5
iload 6
iadd
aload 8
invokevirtual android/graphics/Rect/height()I
if_icmpge L2
aload 1
ldc_w 8388661
iload 2
aload 7
iconst_1
iaload
iload 4
iadd
aload 8
getfield android/graphics/Rect/top I
isub
invokevirtual android/widget/Toast/setGravity(III)V
L3:
aload 1
invokevirtual android/widget/Toast/show()V
iconst_1
ireturn
L2:
aload 1
bipush 81
iconst_0
iload 4
invokevirtual android/widget/Toast/setGravity(III)V
goto L3
.limit locals 10
.limit stack 5
.end method

.method protected onMeasure(II)V
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/c()Z
istore 5
iload 5
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/k I
iflt L0
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/k I
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getPaddingTop()I
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getPaddingRight()I
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getPaddingBottom()I
invokespecial android/support/v7/widget/ai/setPadding(IIII)V
L0:
aload 0
iload 1
iload 2
invokespecial android/support/v7/widget/ai/onMeasure(II)V
iload 1
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 3
iload 1
invokestatic android/view/View$MeasureSpec/getSize(I)I
istore 1
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getMeasuredWidth()I
istore 4
iload 3
ldc_w -2147483648
if_icmpne L1
iload 1
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/j I
invokestatic java/lang/Math/min(II)I
istore 1
L2:
iload 3
ldc_w 1073741824
if_icmpeq L3
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/j I
ifle L3
iload 4
iload 1
if_icmpge L3
aload 0
iload 1
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iload 2
invokespecial android/support/v7/widget/ai/onMeasure(II)V
L3:
iload 5
ifne L4
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/d Landroid/graphics/drawable/Drawable;
ifnull L4
aload 0
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getMeasuredWidth()I
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/d Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getBounds()Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/width()I
isub
iconst_2
idiv
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getPaddingTop()I
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getPaddingRight()I
aload 0
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/getPaddingBottom()I
invokespecial android/support/v7/widget/ai/setPadding(IIII)V
L4:
return
L1:
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/j I
istore 1
goto L2
.limit locals 6
.limit stack 5
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/hasSubMenu()Z
ifeq L0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/f Landroid/support/v7/widget/as;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/f Landroid/support/v7/widget/as;
aload 0
aload 1
invokevirtual android/support/v7/widget/as/onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
ifeq L0
iconst_1
ireturn
L0:
aload 0
aload 1
invokespecial android/support/v7/widget/ai/onTouchEvent(Landroid/view/MotionEvent;)Z
ireturn
.limit locals 2
.limit stack 3
.end method

.method public setCheckable(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public setChecked(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public setExpandedFormat(Z)V
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/i Z
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/i Z
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/b Landroid/support/v7/internal/view/menu/m;
getfield android/support/v7/internal/view/menu/m/g Landroid/support/v7/internal/view/menu/i;
invokevirtual android/support/v7/internal/view/menu/i/g()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/d Landroid/graphics/drawable/Drawable;
aload 1
ifnull L0
aload 1
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
istore 6
aload 1
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
istore 5
iload 5
istore 4
iload 6
istore 3
iload 6
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
if_icmple L1
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
i2f
iload 6
i2f
fdiv
fstore 2
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
istore 3
iload 5
i2f
fload 2
fmul
f2i
istore 4
L1:
iload 4
istore 6
iload 3
istore 5
iload 4
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
if_icmple L2
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
i2f
iload 4
i2f
fdiv
fstore 2
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/m I
istore 6
iload 3
i2f
fload 2
fmul
f2i
istore 5
L2:
aload 1
iconst_0
iconst_0
iload 5
iload 6
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
L0:
aload 0
aload 1
aconst_null
aconst_null
aconst_null
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
aload 0
invokespecial android/support/v7/internal/view/menu/ActionMenuItemView/f()V
return
.limit locals 7
.limit stack 5
.end method

.method public setItemInvoker(Landroid/support/v7/internal/view/menu/k;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/e Landroid/support/v7/internal/view/menu/k;
return
.limit locals 2
.limit stack 2
.end method

.method public setPadding(IIII)V
aload 0
iload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/k I
aload 0
iload 1
iload 2
iload 3
iload 4
invokespecial android/support/v7/widget/ai/setPadding(IIII)V
return
.limit locals 5
.limit stack 5
.end method

.method public setPopupCallback(Landroid/support/v7/internal/view/menu/c;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/g Landroid/support/v7/internal/view/menu/c;
return
.limit locals 2
.limit stack 2
.end method

.method public final setShortcut$25d965e(Z)V
return
.limit locals 2
.limit stack 0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/internal/view/menu/ActionMenuItemView/c Ljava/lang/CharSequence;
aload 0
aload 0
getfield android/support/v7/internal/view/menu/ActionMenuItemView/c Ljava/lang/CharSequence;
invokevirtual android/support/v7/internal/view/menu/ActionMenuItemView/setContentDescription(Ljava/lang/CharSequence;)V
aload 0
invokespecial android/support/v7/internal/view/menu/ActionMenuItemView/f()V
return
.limit locals 2
.limit stack 2
.end method
