.bytecode 50.0
.class public synchronized android/support/v7/b/a/b
.super android/graphics/drawable/Drawable
.implements android/graphics/drawable/Drawable$Callback

.field public 'p' Landroid/graphics/drawable/Drawable;

.method public <init>(Landroid/graphics/drawable/Drawable;)V
aload 0
invokespecial android/graphics/drawable/Drawable/<init>()V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L0:
aload 0
aload 1
putfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private a()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
ifnull L0
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aconst_null
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L0:
aload 0
aload 1
putfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
ifnull L1
aload 1
aload 0
invokevirtual android/graphics/drawable/Drawable/setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public draw(Landroid/graphics/Canvas;)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/draw(Landroid/graphics/Canvas;)V
return
.limit locals 2
.limit stack 2
.end method

.method public getChangingConfigurations()I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getChangingConfigurations()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getCurrent()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getCurrent()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicHeight()I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getIntrinsicWidth()I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimumHeight()I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getMinimumHeight()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMinimumWidth()I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getMinimumWidth()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getOpacity()I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getOpacity()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getState()[I
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getState()[I
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTransparentRegion()Landroid/graphics/Region;
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/getTransparentRegion()Landroid/graphics/Region;
areturn
.limit locals 1
.limit stack 1
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
invokevirtual android/support/v7/b/a/b/invalidateSelf()V
return
.limit locals 2
.limit stack 1
.end method

.method public isAutoMirrored()Z
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokestatic android/support/v4/e/a/a/b(Landroid/graphics/drawable/Drawable;)Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isStateful()Z
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokevirtual android/graphics/drawable/Drawable/isStateful()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public jumpToCurrentState()V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 1
.limit stack 1
.end method

.method public onBoundsChange(Landroid/graphics/Rect;)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setBounds(Landroid/graphics/Rect;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected onLevelChange(I)Z
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setLevel(I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
aload 0
aload 2
lload 3
invokevirtual android/support/v7/b/a/b/scheduleSelf(Ljava/lang/Runnable;J)V
return
.limit locals 5
.limit stack 4
.end method

.method public setAlpha(I)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setAlpha(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setAutoMirrored(Z)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setChangingConfigurations(I)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setChangingConfigurations(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setColorFilter(Landroid/graphics/ColorFilter;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setDither(Z)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setDither(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setFilterBitmap(Z)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokevirtual android/graphics/drawable/Drawable/setFilterBitmap(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public setHotspot(FF)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
fload 1
fload 2
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;FF)V
return
.limit locals 3
.limit stack 3
.end method

.method public setHotspotBounds(IIII)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
iload 2
iload 3
iload 4
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;IIII)V
return
.limit locals 5
.limit stack 5
.end method

.method public setState([I)Z
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokevirtual android/graphics/drawable/Drawable/setState([I)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public setTint(I)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
aload 1
invokestatic android/support/v4/e/a/a/a(Landroid/graphics/drawable/Drawable;Landroid/graphics/PorterDuff$Mode;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setVisible(ZZ)Z
aload 0
iload 1
iload 2
invokespecial android/graphics/drawable/Drawable/setVisible(ZZ)Z
ifne L0
aload 0
getfield android/support/v7/b/a/b/p Landroid/graphics/drawable/Drawable;
iload 1
iload 2
invokevirtual android/graphics/drawable/Drawable/setVisible(ZZ)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
aload 0
aload 2
invokevirtual android/support/v7/b/a/b/unscheduleSelf(Ljava/lang/Runnable;)V
return
.limit locals 3
.limit stack 2
.end method
