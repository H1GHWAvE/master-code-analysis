.bytecode 50.0
.class final synchronized android/support/v7/widget/ah
.super java/lang/Object

.field private static final 'a' [I

.field private static final 'b' [I

.field private final 'c' Landroid/widget/TextView;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16842804
iastore
putstatic android/support/v7/widget/ah/a [I
iconst_1
newarray int
dup
iconst_0
getstatic android/support/v7/a/d/textAllCaps I
iastore
putstatic android/support/v7/widget/ah/b [I
return
.limit locals 0
.limit stack 4
.end method

.method <init>(Landroid/widget/TextView;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield android/support/v7/widget/ah/c Landroid/widget/TextView;
return
.limit locals 2
.limit stack 2
.end method

.method final a(Landroid/content/Context;I)V
aload 1
iload 2
getstatic android/support/v7/widget/ah/b [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 1
aload 1
iconst_0
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L0
aload 0
aload 1
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokevirtual android/support/v7/widget/ah/a(Z)V
L0:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
return
.limit locals 3
.limit stack 4
.end method

.method final a(Landroid/util/AttributeSet;I)V
aload 0
getfield android/support/v7/widget/ah/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getContext()Landroid/content/Context;
astore 4
aload 4
aload 1
getstatic android/support/v7/widget/ah/a [I
iload 2
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 5
aload 5
iconst_0
iconst_m1
invokevirtual android/content/res/TypedArray/getResourceId(II)I
istore 3
aload 5
invokevirtual android/content/res/TypedArray/recycle()V
iload 3
iconst_m1
if_icmpeq L0
aload 4
iload 3
getstatic android/support/v7/a/n/TextAppearance [I
invokevirtual android/content/Context/obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;
astore 5
aload 5
getstatic android/support/v7/a/n/TextAppearance_textAllCaps I
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L1
aload 0
aload 5
getstatic android/support/v7/a/n/TextAppearance_textAllCaps I
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokevirtual android/support/v7/widget/ah/a(Z)V
L1:
aload 5
invokevirtual android/content/res/TypedArray/recycle()V
L0:
aload 4
aload 1
getstatic android/support/v7/widget/ah/b [I
iload 2
iconst_0
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 1
aload 1
iconst_0
invokevirtual android/content/res/TypedArray/hasValue(I)Z
ifeq L2
aload 0
aload 1
iconst_0
iconst_0
invokevirtual android/content/res/TypedArray/getBoolean(IZ)Z
invokevirtual android/support/v7/widget/ah/a(Z)V
L2:
aload 1
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
getfield android/support/v7/widget/ah/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getTextColors()Landroid/content/res/ColorStateList;
astore 1
aload 1
ifnull L3
aload 1
invokevirtual android/content/res/ColorStateList/isStateful()Z
ifne L3
getstatic android/os/Build$VERSION/SDK_INT I
bipush 21
if_icmpge L4
aload 4
ldc_w 16842808
invokestatic android/support/v7/internal/widget/ar/c(Landroid/content/Context;I)I
istore 2
L5:
aload 0
getfield android/support/v7/widget/ah/c Landroid/widget/TextView;
aload 1
invokevirtual android/content/res/ColorStateList/getDefaultColor()I
iload 2
invokestatic android/support/v7/internal/widget/ar/a(II)Landroid/content/res/ColorStateList;
invokevirtual android/widget/TextView/setTextColor(Landroid/content/res/ColorStateList;)V
L3:
return
L4:
aload 4
ldc_w 16842808
invokestatic android/support/v7/internal/widget/ar/a(Landroid/content/Context;I)I
istore 2
goto L5
.limit locals 6
.limit stack 5
.end method

.method final a(Z)V
aload 0
getfield android/support/v7/widget/ah/c Landroid/widget/TextView;
astore 3
iload 1
ifeq L0
new android/support/v7/internal/b/a
dup
aload 0
getfield android/support/v7/widget/ah/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/b/a/<init>(Landroid/content/Context;)V
astore 2
L1:
aload 3
aload 2
invokevirtual android/widget/TextView/setTransformationMethod(Landroid/text/method/TransformationMethod;)V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 4
.limit stack 3
.end method
