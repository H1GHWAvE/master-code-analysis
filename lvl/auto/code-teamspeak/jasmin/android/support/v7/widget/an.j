.bytecode 50.0
.class public synchronized android/support/v7/widget/an
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "ListPopupWindow"

.field private static final 'b' Z = 0


.field public static final 'n' I = 0


.field public static final 'o' I = 1


.field public static final 'p' I = -1


.field public static final 'q' I = -2


.field public static final 'r' I = 0


.field public static final 's' I = 1


.field public static final 't' I = 2


.field private static final 'u' I = 250


.field private static 'v' Ljava/lang/reflect/Method;

.field private 'A' Z

.field private 'B' Landroid/view/View;

.field private 'C' Landroid/database/DataSetObserver;

.field private 'D' Landroid/graphics/drawable/Drawable;

.field private 'E' Landroid/widget/AdapterView$OnItemSelectedListener;

.field private final 'F' Landroid/support/v7/widget/az;

.field private final 'G' Landroid/support/v7/widget/ay;

.field private final 'H' Landroid/support/v7/widget/ax;

.field private final 'I' Landroid/support/v7/widget/av;

.field private 'J' Ljava/lang/Runnable;

.field private 'K' Landroid/os/Handler;

.field private 'L' Landroid/graphics/Rect;

.field private 'M' Z

.field private 'N' I

.field public 'c' Landroid/widget/PopupWindow;

.field public 'd' Landroid/support/v7/widget/ar;

.field 'e' I

.field 'f' I

.field 'g' I

.field 'h' Z

.field public 'i' I

.field 'j' I

.field 'k' I

.field public 'l' Landroid/view/View;

.field public 'm' Landroid/widget/AdapterView$OnItemClickListener;

.field private 'w' Landroid/content/Context;

.field private 'x' Landroid/widget/ListAdapter;

.field private 'y' I

.field private 'z' Z

.method static <clinit>()V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
L0:
ldc android/widget/PopupWindow
ldc "setClipToScreenEnabled"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putstatic android/support/v7/widget/an/v Ljava/lang/reflect/Method;
L1:
return
L2:
astore 0
ldc "ListPopupWindow"
ldc "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 1
.limit stack 6
.end method

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
getstatic android/support/v7/a/d/listPopupWindowStyle I
invokespecial android/support/v7/widget/an/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
getstatic android/support/v7/a/d/listPopupWindowStyle I
invokespecial android/support/v7/widget/an/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
aload 2
iload 3
iconst_0
invokespecial android/support/v7/widget/an/<init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
return
.limit locals 4
.limit stack 5
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
bipush -2
putfield android/support/v7/widget/an/y I
aload 0
bipush -2
putfield android/support/v7/widget/an/e I
aload 0
iconst_0
putfield android/support/v7/widget/an/i I
aload 0
iconst_0
putfield android/support/v7/widget/an/z Z
aload 0
iconst_0
putfield android/support/v7/widget/an/A Z
aload 0
ldc_w 2147483647
putfield android/support/v7/widget/an/j I
aload 0
iconst_0
putfield android/support/v7/widget/an/k I
aload 0
new android/support/v7/widget/az
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/az/<init>(Landroid/support/v7/widget/an;B)V
putfield android/support/v7/widget/an/F Landroid/support/v7/widget/az;
aload 0
new android/support/v7/widget/ay
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/ay/<init>(Landroid/support/v7/widget/an;B)V
putfield android/support/v7/widget/an/G Landroid/support/v7/widget/ay;
aload 0
new android/support/v7/widget/ax
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/ax/<init>(Landroid/support/v7/widget/an;B)V
putfield android/support/v7/widget/an/H Landroid/support/v7/widget/ax;
aload 0
new android/support/v7/widget/av
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/av/<init>(Landroid/support/v7/widget/an;B)V
putfield android/support/v7/widget/an/I Landroid/support/v7/widget/av;
aload 0
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
putfield android/support/v7/widget/an/K Landroid/os/Handler;
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/widget/an/L Landroid/graphics/Rect;
aload 0
aload 1
putfield android/support/v7/widget/an/w Landroid/content/Context;
aload 1
aload 2
getstatic android/support/v7/a/n/ListPopupWindow [I
iload 3
iload 4
invokevirtual android/content/Context/obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;
astore 5
aload 0
aload 5
getstatic android/support/v7/a/n/ListPopupWindow_android_dropDownHorizontalOffset I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelOffset(II)I
putfield android/support/v7/widget/an/f I
aload 0
aload 5
getstatic android/support/v7/a/n/ListPopupWindow_android_dropDownVerticalOffset I
iconst_0
invokevirtual android/content/res/TypedArray/getDimensionPixelOffset(II)I
putfield android/support/v7/widget/an/g I
aload 0
getfield android/support/v7/widget/an/g I
ifeq L0
aload 0
iconst_1
putfield android/support/v7/widget/an/h Z
L0:
aload 5
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
new android/support/v7/internal/widget/aa
dup
aload 1
aload 2
iload 3
invokespecial android/support/v7/internal/widget/aa/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
putfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_1
invokevirtual android/widget/PopupWindow/setInputMethodMode(I)V
aload 0
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getConfiguration()Landroid/content/res/Configuration;
getfield android/content/res/Configuration/locale Ljava/util/Locale;
invokestatic android/support/v4/m/u/a(Ljava/util/Locale;)I
putfield android/support/v7/widget/an/N I
return
.limit locals 6
.limit stack 6
.end method

.method private A()Landroid/widget/ListView;
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
areturn
.limit locals 1
.limit stack 1
.end method

.method private B()I
iconst_0
istore 3
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
ifnonnull L0
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
astore 7
aload 0
new android/support/v7/widget/ap
dup
aload 0
invokespecial android/support/v7/widget/ap/<init>(Landroid/support/v7/widget/an;)V
putfield android/support/v7/widget/an/J Ljava/lang/Runnable;
aload 0
getfield android/support/v7/widget/an/M Z
ifne L1
iconst_1
istore 5
L2:
aload 0
new android/support/v7/widget/ar
dup
aload 7
iload 5
invokespecial android/support/v7/widget/ar/<init>(Landroid/content/Context;Z)V
putfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/D Landroid/graphics/drawable/Drawable;
ifnull L3
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/D Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/ar/setSelector(Landroid/graphics/drawable/Drawable;)V
L3:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
invokevirtual android/support/v7/widget/ar/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
invokevirtual android/support/v7/widget/ar/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_1
invokevirtual android/support/v7/widget/ar/setFocusable(Z)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_1
invokevirtual android/support/v7/widget/ar/setFocusableInTouchMode(Z)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
new android/support/v7/widget/aq
dup
aload 0
invokespecial android/support/v7/widget/aq/<init>(Landroid/support/v7/widget/an;)V
invokevirtual android/support/v7/widget/ar/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/H Landroid/support/v7/widget/ax;
invokevirtual android/support/v7/widget/ar/setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
aload 0
getfield android/support/v7/widget/an/E Landroid/widget/AdapterView$OnItemSelectedListener;
ifnull L4
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/E Landroid/widget/AdapterView$OnItemSelectedListener;
invokevirtual android/support/v7/widget/ar/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
L4:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
astore 6
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
astore 8
aload 8
ifnull L5
new android/widget/LinearLayout
dup
aload 7
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 7
aload 7
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_0
fconst_1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
astore 9
aload 0
getfield android/support/v7/widget/an/k I
tableswitch 0
L6
L7
default : L8
L8:
ldc "ListPopupWindow"
new java/lang/StringBuilder
dup
ldc "Invalid hint position "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/widget/an/k I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L9:
aload 8
aload 0
getfield android/support/v7/widget/an/e I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iconst_0
invokevirtual android/view/View/measure(II)V
aload 8
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
astore 6
aload 8
invokevirtual android/view/View/getMeasuredHeight()I
istore 1
aload 6
getfield android/widget/LinearLayout$LayoutParams/topMargin I
istore 2
aload 6
getfield android/widget/LinearLayout$LayoutParams/bottomMargin I
iload 1
iload 2
iadd
iadd
istore 1
aload 7
astore 6
L10:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 6
invokevirtual android/widget/PopupWindow/setContentView(Landroid/view/View;)V
L11:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getBackground()Landroid/graphics/drawable/Drawable;
astore 6
aload 6
ifnull L12
aload 6
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
istore 2
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
iload 2
iadd
istore 2
iload 2
istore 3
aload 0
getfield android/support/v7/widget/an/h Z
ifne L13
aload 0
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
ineg
putfield android/support/v7/widget/an/g I
iload 2
istore 3
L13:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getInputMethodMode()I
pop
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
getfield android/support/v7/widget/an/g I
invokevirtual android/widget/PopupWindow/getMaxAvailableHeight(Landroid/view/View;I)I
istore 4
aload 0
getfield android/support/v7/widget/an/z Z
ifne L14
aload 0
getfield android/support/v7/widget/an/y I
iconst_m1
if_icmpne L15
L14:
iload 4
iload 3
iadd
ireturn
L1:
iconst_0
istore 5
goto L2
L7:
aload 7
aload 6
aload 9
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 7
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
goto L9
L6:
aload 7
aload 8
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 7
aload 6
aload 9
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
goto L9
L0:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getContentView()Landroid/view/View;
pop
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
astore 6
aload 6
ifnull L16
aload 6
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
astore 7
aload 6
invokevirtual android/view/View/getMeasuredHeight()I
istore 1
aload 7
getfield android/widget/LinearLayout$LayoutParams/topMargin I
istore 2
aload 7
getfield android/widget/LinearLayout$LayoutParams/bottomMargin I
iload 1
iload 2
iadd
iadd
istore 1
goto L11
L12:
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/setEmpty()V
goto L13
L15:
aload 0
getfield android/support/v7/widget/an/e I
tableswitch -2
L17
L18
default : L19
L19:
aload 0
getfield android/support/v7/widget/an/e I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
L20:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iload 2
iload 4
iload 1
isub
invokevirtual android/support/v7/widget/ar/a(II)I
istore 4
iload 1
istore 2
iload 4
ifle L21
iload 1
iload 3
iadd
istore 2
L21:
iload 2
iload 4
iadd
ireturn
L17:
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
isub
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
goto L20
L18:
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
goto L20
L16:
iconst_0
istore 1
goto L11
L5:
iconst_0
istore 1
goto L10
.limit locals 10
.limit stack 5
.end method

.method private C()V
.catch java/lang/Exception from L0 to L1 using L2
getstatic android/support/v7/widget/an/v Ljava/lang/reflect/Method;
ifnull L1
L0:
getstatic android/support/v7/widget/an/v Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 1
ldc "ListPopupWindow"
ldc "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
return
.limit locals 2
.limit stack 6
.end method

.method static synthetic a(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a()V
aload 0
iconst_0
putfield android/support/v7/widget/an/k I
return
.limit locals 1
.limit stack 2
.end method

.method private a(Landroid/view/View;)V
aload 0
aload 1
putfield android/support/v7/widget/an/l Landroid/view/View;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
aload 1
putfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
putfield android/support/v7/widget/an/E Landroid/widget/AdapterView$OnItemSelectedListener;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Z)V
aload 0
iload 1
putfield android/support/v7/widget/an/A Z
return
.limit locals 2
.limit stack 2
.end method

.method private a(ILandroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
iload 1
bipush 62
if_icmpeq L0
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedItemPosition()I
ifge L1
iload 1
invokestatic android/support/v7/widget/an/l(I)Z
ifne L0
L1:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedItemPosition()I
istore 6
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isAboveAnchor()Z
ifne L2
iconst_1
istore 5
L3:
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
astore 8
ldc_w 2147483647
istore 3
ldc_w -2147483648
istore 4
aload 8
ifnull L4
aload 8
invokeinterface android/widget/ListAdapter/areAllItemsEnabled()Z 0
istore 7
iload 7
ifeq L5
iconst_0
istore 3
L6:
iload 7
ifeq L7
aload 8
invokeinterface android/widget/ListAdapter/getCount()I 0
iconst_1
isub
istore 4
L4:
iload 5
ifeq L8
iload 1
bipush 19
if_icmpne L8
iload 6
iload 3
if_icmple L9
L8:
iload 5
ifne L10
iload 1
bipush 20
if_icmpne L10
iload 6
iload 4
if_icmplt L10
L9:
aload 0
invokevirtual android/support/v7/widget/an/f()V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_1
invokevirtual android/widget/PopupWindow/setInputMethodMode(I)V
aload 0
invokevirtual android/support/v7/widget/an/b()V
L11:
iconst_1
ireturn
L2:
iconst_0
istore 5
goto L3
L5:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_0
iconst_1
invokevirtual android/support/v7/widget/ar/a(IZ)I
istore 3
goto L6
L7:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 8
invokeinterface android/widget/ListAdapter/getCount()I 0
iconst_1
isub
iconst_0
invokevirtual android/support/v7/widget/ar/a(IZ)I
istore 4
goto L4
L10:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_0
invokestatic android/support/v7/widget/ar/a(Landroid/support/v7/widget/ar;Z)Z
pop
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iload 1
aload 2
invokevirtual android/support/v7/widget/ar/onKeyDown(ILandroid/view/KeyEvent;)Z
ifeq L12
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_2
invokevirtual android/widget/PopupWindow/setInputMethodMode(I)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/requestFocusFromTouch()Z
pop
aload 0
invokevirtual android/support/v7/widget/an/b()V
iload 1
lookupswitch
19 : L11
20 : L11
23 : L11
66 : L11
default : L0
L0:
iconst_0
ireturn
L12:
iload 5
ifeq L13
iload 1
bipush 20
if_icmpne L13
iload 6
iload 4
if_icmpne L0
iconst_1
ireturn
L13:
iload 5
ifne L0
iload 1
bipush 19
if_icmpne L0
iload 6
iload 3
if_icmpne L0
iconst_1
ireturn
.limit locals 9
.limit stack 3
.end method

.method static synthetic b(Landroid/support/v7/widget/an;)Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iload 1
invokevirtual android/widget/PopupWindow/setSoftInputMode(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
putfield android/support/v7/widget/an/D Landroid/graphics/drawable/Drawable;
return
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
istore 2
iload 2
ifeq L0
aload 0
invokespecial android/support/v7/widget/an/t()V
L0:
aload 0
aload 1
putfield android/support/v7/widget/an/B Landroid/view/View;
iload 2
ifeq L1
aload 0
invokevirtual android/support/v7/widget/an/b()V
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private b(Z)V
aload 0
iload 1
putfield android/support/v7/widget/an/z Z
return
.limit locals 2
.limit stack 2
.end method

.method private b(ILandroid/view/KeyEvent;)Z
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedItemPosition()I
iflt L0
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iload 1
aload 2
invokevirtual android/support/v7/widget/ar/onKeyUp(ILandroid/view/KeyEvent;)Z
istore 3
iload 3
ifeq L1
iload 1
invokestatic android/support/v7/widget/an/l(I)Z
ifeq L1
aload 0
invokevirtual android/support/v7/widget/an/d()V
L1:
iload 3
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method static synthetic c(Landroid/support/v7/widget/an;)Landroid/support/v7/widget/az;
aload 0
getfield android/support/v7/widget/an/F Landroid/support/v7/widget/az;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c(Landroid/view/View;)Landroid/view/View$OnTouchListener;
new android/support/v7/widget/ao
dup
aload 0
aload 1
invokespecial android/support/v7/widget/ao/<init>(Landroid/support/v7/widget/an;Landroid/view/View;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private c(I)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iload 1
invokevirtual android/widget/PopupWindow/setAnimationStyle(I)V
return
.limit locals 2
.limit stack 2
.end method

.method private c(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
astore 3
aload 2
invokevirtual android/view/KeyEvent/getAction()I
ifne L1
aload 2
invokevirtual android/view/KeyEvent/getRepeatCount()I
ifne L1
aload 3
invokevirtual android/view/View/getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;
astore 3
aload 3
ifnull L2
aload 3
aload 2
aload 0
invokevirtual android/view/KeyEvent$DispatcherState/startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V
L2:
iconst_1
ireturn
L1:
aload 2
invokevirtual android/view/KeyEvent/getAction()I
iconst_1
if_icmpne L0
aload 3
invokevirtual android/view/View/getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;
astore 3
aload 3
ifnull L3
aload 3
aload 2
invokevirtual android/view/KeyEvent$DispatcherState/handleUpEvent(Landroid/view/KeyEvent;)V
L3:
aload 2
invokevirtual android/view/KeyEvent/isTracking()Z
ifeq L0
aload 2
invokevirtual android/view/KeyEvent/isCanceled()Z
ifne L0
aload 0
invokevirtual android/support/v7/widget/an/d()V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 4
.limit stack 3
.end method

.method static synthetic d(Landroid/support/v7/widget/an;)Landroid/os/Handler;
aload 0
getfield android/support/v7/widget/an/K Landroid/os/Handler;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)V
aload 0
iload 1
putfield android/support/v7/widget/an/f I
return
.limit locals 2
.limit stack 2
.end method

.method private e(I)V
aload 0
iload 1
putfield android/support/v7/widget/an/g I
aload 0
iconst_1
putfield android/support/v7/widget/an/h Z
return
.limit locals 2
.limit stack 2
.end method

.method private f(I)V
aload 0
iload 1
putfield android/support/v7/widget/an/i I
return
.limit locals 2
.limit stack 2
.end method

.method private g(I)V
aload 0
iload 1
putfield android/support/v7/widget/an/e I
return
.limit locals 2
.limit stack 2
.end method

.method private h()I
aload 0
getfield android/support/v7/widget/an/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h(I)V
aload 0
iload 1
putfield android/support/v7/widget/an/y I
return
.limit locals 2
.limit stack 2
.end method

.method private i(I)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
astore 2
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 2
ifnull L0
aload 2
iconst_0
invokestatic android/support/v7/widget/ar/a(Landroid/support/v7/widget/ar;Z)Z
pop
aload 2
iload 1
invokevirtual android/support/v7/widget/ar/setSelection(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 2
invokevirtual android/support/v7/widget/ar/getChoiceMode()I
ifeq L0
aload 2
iload 1
iconst_1
invokevirtual android/support/v7/widget/ar/setItemChecked(IZ)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private i()Z
aload 0
getfield android/support/v7/widget/an/M Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield android/support/v7/widget/an/z Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j(I)Z
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 0
getfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
ifnull L1
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
astore 2
aload 2
iload 1
aload 2
invokevirtual android/support/v7/widget/ar/getFirstVisiblePosition()I
isub
invokevirtual android/support/v7/widget/ar/getChildAt(I)Landroid/view/View;
astore 3
aload 2
invokevirtual android/support/v7/widget/ar/getAdapter()Landroid/widget/ListAdapter;
astore 4
aload 0
getfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
aload 2
aload 3
iload 1
aload 4
iload 1
invokeinterface android/widget/ListAdapter/getItemId(I)J 1
invokeinterface android/widget/AdapterView$OnItemClickListener/onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V 5
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 5
.limit stack 6
.end method

.method private k()I
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getSoftInputMode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k(I)V
aload 0
iload 1
putfield android/support/v7/widget/an/j I
return
.limit locals 2
.limit stack 2
.end method

.method private l()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getBackground()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static l(I)Z
iload 0
bipush 66
if_icmpeq L0
iload 0
bipush 23
if_icmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private m()I
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getAnimationStyle()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private n()Landroid/view/View;
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method private o()I
aload 0
getfield android/support/v7/widget/an/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private p()I
aload 0
getfield android/support/v7/widget/an/h Z
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/widget/an/g I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private q()I
aload 0
getfield android/support/v7/widget/an/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private r()I
aload 0
getfield android/support/v7/widget/an/y I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private s()V
aload 0
getfield android/support/v7/widget/an/K Landroid/os/Handler;
aload 0
getfield android/support/v7/widget/an/J Ljava/lang/Runnable;
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method private t()V
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
ifnull L0
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
astore 1
aload 1
instanceof android/view/ViewGroup
ifeq L0
aload 1
checkcast android/view/ViewGroup
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
invokevirtual android/view/ViewGroup/removeView(Landroid/view/View;)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method private u()I
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getInputMethodMode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private v()Z
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private w()Ljava/lang/Object;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedItem()Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method private x()I
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L0
iconst_m1
ireturn
L0:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedItemPosition()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private y()J
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L0
ldc2_w -9223372036854775808L
lreturn
L0:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedItemId()J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private z()Landroid/view/View;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifne L0
aconst_null
areturn
L0:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/getSelectedView()Landroid/view/View;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getBackground()Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L0
aload 2
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
iload 1
iadd
putfield android/support/v7/widget/an/e I
return
L0:
aload 0
iload 1
putfield android/support/v7/widget/an/e I
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 1
invokevirtual android/widget/PopupWindow/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public a(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/widget/an/C Landroid/database/DataSetObserver;
ifnonnull L0
aload 0
new android/support/v7/widget/aw
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/aw/<init>(Landroid/support/v7/widget/an;B)V
putfield android/support/v7/widget/an/C Landroid/database/DataSetObserver;
L1:
aload 0
aload 1
putfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
ifnull L2
aload 1
aload 0
getfield android/support/v7/widget/an/C Landroid/database/DataSetObserver;
invokeinterface android/widget/ListAdapter/registerDataSetObserver(Landroid/database/DataSetObserver;)V 1
L2:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
ifnull L3
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
invokevirtual android/support/v7/widget/ar/setAdapter(Landroid/widget/ListAdapter;)V
L3:
return
L0:
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
ifnull L1
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
aload 0
getfield android/support/v7/widget/an/C Landroid/database/DataSetObserver;
invokeinterface android/widget/ListAdapter/unregisterDataSetObserver(Landroid/database/DataSetObserver;)V 1
goto L1
.limit locals 2
.limit stack 5
.end method

.method public final a(Landroid/widget/PopupWindow$OnDismissListener;)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 1
invokevirtual android/widget/PopupWindow/setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method public b()V
.catch java/lang/Exception from L0 to L1 using L2
iconst_1
istore 8
iconst_1
istore 7
iconst_m1
istore 4
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
ifnonnull L3
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
astore 10
aload 0
new android/support/v7/widget/ap
dup
aload 0
invokespecial android/support/v7/widget/ap/<init>(Landroid/support/v7/widget/an;)V
putfield android/support/v7/widget/an/J Ljava/lang/Runnable;
aload 0
getfield android/support/v7/widget/an/M Z
ifne L4
iconst_1
istore 6
L5:
aload 0
new android/support/v7/widget/ar
dup
aload 10
iload 6
invokespecial android/support/v7/widget/ar/<init>(Landroid/content/Context;Z)V
putfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/D Landroid/graphics/drawable/Drawable;
ifnull L6
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/D Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/ar/setSelector(Landroid/graphics/drawable/Drawable;)V
L6:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/x Landroid/widget/ListAdapter;
invokevirtual android/support/v7/widget/ar/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
invokevirtual android/support/v7/widget/ar/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_1
invokevirtual android/support/v7/widget/ar/setFocusable(Z)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_1
invokevirtual android/support/v7/widget/ar/setFocusableInTouchMode(Z)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
new android/support/v7/widget/aq
dup
aload 0
invokespecial android/support/v7/widget/aq/<init>(Landroid/support/v7/widget/an;)V
invokevirtual android/support/v7/widget/ar/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/H Landroid/support/v7/widget/ax;
invokevirtual android/support/v7/widget/ar/setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
aload 0
getfield android/support/v7/widget/an/E Landroid/widget/AdapterView$OnItemSelectedListener;
ifnull L7
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/E Landroid/widget/AdapterView$OnItemSelectedListener;
invokevirtual android/support/v7/widget/ar/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
L7:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
astore 9
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
astore 11
aload 11
ifnull L8
new android/widget/LinearLayout
dup
aload 10
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 10
aload 10
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_0
fconst_1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(IIF)V
astore 12
aload 0
getfield android/support/v7/widget/an/k I
tableswitch 0
L9
L10
default : L11
L11:
ldc "ListPopupWindow"
new java/lang/StringBuilder
dup
ldc "Invalid hint position "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield android/support/v7/widget/an/k I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
L12:
aload 11
aload 0
getfield android/support/v7/widget/an/e I
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
iconst_0
invokevirtual android/view/View/measure(II)V
aload 11
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
astore 9
aload 11
invokevirtual android/view/View/getMeasuredHeight()I
istore 1
aload 9
getfield android/widget/LinearLayout$LayoutParams/topMargin I
istore 2
aload 9
getfield android/widget/LinearLayout$LayoutParams/bottomMargin I
iload 1
iload 2
iadd
iadd
istore 1
aload 10
astore 9
L13:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 9
invokevirtual android/widget/PopupWindow/setContentView(Landroid/view/View;)V
L14:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getBackground()Landroid/graphics/drawable/Drawable;
astore 9
aload 9
ifnull L15
aload 9
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/bottom I
iadd
istore 2
iload 2
istore 3
aload 0
getfield android/support/v7/widget/an/h Z
ifne L16
aload 0
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/top I
ineg
putfield android/support/v7/widget/an/g I
iload 2
istore 3
L16:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getInputMethodMode()I
pop
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
getfield android/support/v7/widget/an/g I
invokevirtual android/widget/PopupWindow/getMaxAvailableHeight(Landroid/view/View;I)I
istore 5
aload 0
getfield android/support/v7/widget/an/z Z
ifne L17
aload 0
getfield android/support/v7/widget/an/y I
iconst_m1
if_icmpne L18
L17:
iload 5
iload 3
iadd
istore 1
L19:
aload 0
invokevirtual android/support/v7/widget/an/g()Z
istore 6
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L20
aload 0
getfield android/support/v7/widget/an/e I
iconst_m1
if_icmpne L21
iconst_m1
istore 2
L22:
aload 0
getfield android/support/v7/widget/an/y I
iconst_m1
if_icmpne L23
iload 6
ifeq L24
L25:
iload 6
ifeq L26
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
astore 9
aload 0
getfield android/support/v7/widget/an/e I
iconst_m1
if_icmpne L27
iload 4
istore 3
L28:
aload 9
iload 3
iconst_0
invokevirtual android/widget/PopupWindow/setWindowLayoutMode(II)V
L29:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
astore 9
aload 0
getfield android/support/v7/widget/an/A Z
ifne L30
aload 0
getfield android/support/v7/widget/an/z Z
ifne L30
iload 7
istore 6
L31:
aload 9
iload 6
invokevirtual android/widget/PopupWindow/setOutsideTouchable(Z)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
getfield android/support/v7/widget/an/f I
aload 0
getfield android/support/v7/widget/an/g I
iload 2
iload 1
invokevirtual android/widget/PopupWindow/update(Landroid/view/View;IIII)V
L32:
return
L4:
iconst_0
istore 6
goto L5
L10:
aload 10
aload 9
aload 12
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 10
aload 11
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
goto L12
L9:
aload 10
aload 11
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 10
aload 9
aload 12
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
goto L12
L3:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getContentView()Landroid/view/View;
pop
aload 0
getfield android/support/v7/widget/an/B Landroid/view/View;
astore 9
aload 9
ifnull L33
aload 9
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/widget/LinearLayout$LayoutParams
astore 10
aload 9
invokevirtual android/view/View/getMeasuredHeight()I
istore 1
aload 10
getfield android/widget/LinearLayout$LayoutParams/topMargin I
istore 2
aload 10
getfield android/widget/LinearLayout$LayoutParams/bottomMargin I
iload 1
iload 2
iadd
iadd
istore 1
goto L14
L15:
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
invokevirtual android/graphics/Rect/setEmpty()V
iconst_0
istore 3
goto L16
L18:
aload 0
getfield android/support/v7/widget/an/e I
tableswitch -2
L34
L35
default : L36
L36:
aload 0
getfield android/support/v7/widget/an/e I
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
L37:
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iload 2
iload 5
iload 1
isub
invokevirtual android/support/v7/widget/ar/a(II)I
istore 5
iload 1
istore 2
iload 5
ifle L38
iload 1
iload 3
iadd
istore 2
L38:
iload 2
iload 5
iadd
istore 1
goto L19
L34:
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
isub
ldc_w -2147483648
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
goto L37
L35:
aload 0
getfield android/support/v7/widget/an/w Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
aload 0
getfield android/support/v7/widget/an/L Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
iadd
isub
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
goto L37
L21:
aload 0
getfield android/support/v7/widget/an/e I
bipush -2
if_icmpne L39
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
invokevirtual android/view/View/getWidth()I
istore 2
goto L22
L39:
aload 0
getfield android/support/v7/widget/an/e I
istore 2
goto L22
L24:
iconst_m1
istore 1
goto L25
L27:
iconst_0
istore 3
goto L28
L26:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
astore 9
aload 0
getfield android/support/v7/widget/an/e I
iconst_m1
if_icmpne L40
iconst_m1
istore 3
L41:
aload 9
iload 3
iconst_m1
invokevirtual android/widget/PopupWindow/setWindowLayoutMode(II)V
goto L29
L40:
iconst_0
istore 3
goto L41
L23:
aload 0
getfield android/support/v7/widget/an/y I
bipush -2
if_icmpne L42
goto L29
L42:
aload 0
getfield android/support/v7/widget/an/y I
istore 1
goto L29
L30:
iconst_0
istore 6
goto L31
L20:
aload 0
getfield android/support/v7/widget/an/e I
iconst_m1
if_icmpne L43
iconst_m1
istore 2
L44:
aload 0
getfield android/support/v7/widget/an/y I
iconst_m1
if_icmpne L45
iconst_m1
istore 1
L46:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iload 2
iload 1
invokevirtual android/widget/PopupWindow/setWindowLayoutMode(II)V
getstatic android/support/v7/widget/an/v Ljava/lang/reflect/Method;
ifnull L1
L0:
getstatic android/support/v7/widget/an/v Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
astore 9
aload 0
getfield android/support/v7/widget/an/A Z
ifne L47
aload 0
getfield android/support/v7/widget/an/z Z
ifne L47
iload 8
istore 6
L48:
aload 9
iload 6
invokevirtual android/widget/PopupWindow/setOutsideTouchable(Z)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/G Landroid/support/v7/widget/ay;
invokevirtual android/widget/PopupWindow/setTouchInterceptor(Landroid/view/View$OnTouchListener;)V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
getfield android/support/v7/widget/an/f I
aload 0
getfield android/support/v7/widget/an/g I
aload 0
getfield android/support/v7/widget/an/i I
invokestatic android/support/v4/widget/bo/a(Landroid/widget/PopupWindow;Landroid/view/View;III)V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_m1
invokevirtual android/support/v7/widget/ar/setSelection(I)V
aload 0
getfield android/support/v7/widget/an/M Z
ifeq L49
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
invokevirtual android/support/v7/widget/ar/isInTouchMode()Z
ifeq L50
L49:
aload 0
invokevirtual android/support/v7/widget/an/f()V
L50:
aload 0
getfield android/support/v7/widget/an/M Z
ifne L32
aload 0
getfield android/support/v7/widget/an/K Landroid/os/Handler;
aload 0
getfield android/support/v7/widget/an/I Landroid/support/v7/widget/av;
invokevirtual android/os/Handler/post(Ljava/lang/Runnable;)Z
pop
return
L43:
aload 0
getfield android/support/v7/widget/an/e I
bipush -2
if_icmpne L51
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/l Landroid/view/View;
invokevirtual android/view/View/getWidth()I
invokevirtual android/widget/PopupWindow/setWidth(I)V
iconst_0
istore 2
goto L44
L51:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/e I
invokevirtual android/widget/PopupWindow/setWidth(I)V
iconst_0
istore 2
goto L44
L45:
aload 0
getfield android/support/v7/widget/an/y I
bipush -2
if_icmpne L52
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iload 1
invokevirtual android/widget/PopupWindow/setHeight(I)V
iconst_0
istore 1
goto L46
L52:
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aload 0
getfield android/support/v7/widget/an/y I
invokevirtual android/widget/PopupWindow/setHeight(I)V
iconst_0
istore 1
goto L46
L2:
astore 9
ldc "ListPopupWindow"
ldc "Could not call setClipToScreenEnabled() on PopupWindow. Oh well."
invokestatic android/util/Log/i(Ljava/lang/String;Ljava/lang/String;)I
pop
goto L1
L47:
iconst_0
istore 6
goto L48
L33:
iconst_0
istore 1
goto L14
L8:
iconst_0
istore 1
goto L13
.limit locals 13
.limit stack 6
.end method

.method public final c()V
aload 0
iconst_1
putfield android/support/v7/widget/an/M Z
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_1
invokevirtual android/widget/PopupWindow/setFocusable(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method public final d()V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/dismiss()V
aload 0
invokespecial android/support/v7/widget/an/t()V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
aconst_null
invokevirtual android/widget/PopupWindow/setContentView(Landroid/view/View;)V
aload 0
aconst_null
putfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
aload 0
getfield android/support/v7/widget/an/K Landroid/os/Handler;
aload 0
getfield android/support/v7/widget/an/F Landroid/support/v7/widget/az;
invokevirtual android/os/Handler/removeCallbacks(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 2
.end method

.method public final e()V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
iconst_2
invokevirtual android/widget/PopupWindow/setInputMethodMode(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public final f()V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
astore 1
aload 1
ifnull L0
aload 1
iconst_1
invokestatic android/support/v7/widget/ar/a(Landroid/support/v7/widget/ar;Z)Z
pop
aload 1
invokevirtual android/support/v7/widget/ar/requestLayout()V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public final g()Z
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getInputMethodMode()I
iconst_2
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method
