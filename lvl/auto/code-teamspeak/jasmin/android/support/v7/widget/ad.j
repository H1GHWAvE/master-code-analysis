.bytecode 50.0
.class final synchronized android/support/v7/widget/ad
.super android/support/v7/widget/an

.field 'a' Ljava/lang/CharSequence;

.field final synthetic 'b' Landroid/support/v7/widget/aa;

.field private 'u' Landroid/widget/ListAdapter;

.field private final 'v' Landroid/graphics/Rect;

.method public <init>(Landroid/support/v7/widget/aa;Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
aload 1
putfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
aload 0
aload 2
aload 3
iload 4
invokespecial android/support/v7/widget/an/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/graphics/Rect
dup
invokespecial android/graphics/Rect/<init>()V
putfield android/support/v7/widget/ad/v Landroid/graphics/Rect;
aload 0
aload 1
putfield android/support/v7/widget/an/l Landroid/view/View;
aload 0
invokevirtual android/support/v7/widget/ad/c()V
aload 0
iconst_0
putfield android/support/v7/widget/an/k I
aload 0
new android/support/v7/widget/ae
dup
aload 0
aload 1
invokespecial android/support/v7/widget/ae/<init>(Landroid/support/v7/widget/ad;Landroid/support/v7/widget/aa;)V
putfield android/support/v7/widget/an/m Landroid/widget/AdapterView$OnItemClickListener;
return
.limit locals 5
.limit stack 5
.end method

.method static synthetic a(Landroid/support/v7/widget/ad;)Landroid/widget/ListAdapter;
aload 0
getfield android/support/v7/widget/ad/u Landroid/widget/ListAdapter;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/CharSequence;)V
aload 0
aload 1
putfield android/support/v7/widget/ad/a Ljava/lang/CharSequence;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Landroid/support/v7/widget/ad;Landroid/view/View;)Z
aload 1
invokestatic android/support/v4/view/cx/D(Landroid/view/View;)Z
ifeq L0
aload 1
aload 0
getfield android/support/v7/widget/ad/v Landroid/graphics/Rect;
invokevirtual android/view/View/getGlobalVisibleRect(Landroid/graphics/Rect;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/view/View;)Z
aload 1
invokestatic android/support/v4/view/cx/D(Landroid/view/View;)Z
ifeq L0
aload 1
aload 0
getfield android/support/v7/widget/ad/v Landroid/graphics/Rect;
invokevirtual android/view/View/getGlobalVisibleRect(Landroid/graphics/Rect;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Landroid/support/v7/widget/ad;)V
aload 0
invokespecial android/support/v7/widget/an/b()V
return
.limit locals 1
.limit stack 1
.end method

.method private h()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/ad/a Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method final a()V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getBackground()Landroid/graphics/drawable/Drawable;
astore 7
aload 7
ifnull L0
aload 7
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
invokevirtual android/graphics/drawable/Drawable/getPadding(Landroid/graphics/Rect;)Z
pop
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L1
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
istore 1
L2:
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokevirtual android/support/v7/widget/aa/getPaddingLeft()I
istore 4
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokevirtual android/support/v7/widget/aa/getPaddingRight()I
istore 5
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokevirtual android/support/v7/widget/aa/getWidth()I
istore 6
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/c(Landroid/support/v7/widget/aa;)I
bipush -2
if_icmpne L3
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
aload 0
getfield android/support/v7/widget/ad/u Landroid/widget/ListAdapter;
checkcast android/widget/SpinnerAdapter
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/getBackground()Landroid/graphics/drawable/Drawable;
invokestatic android/support/v7/widget/aa/a(Landroid/support/v7/widget/aa;Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I
istore 2
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokevirtual android/support/v7/widget/aa/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
isub
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
getfield android/graphics/Rect/right I
isub
istore 3
iload 2
iload 3
if_icmple L4
iload 3
istore 2
L5:
aload 0
iload 2
iload 6
iload 4
isub
iload 5
isub
invokestatic java/lang/Math/max(II)I
invokevirtual android/support/v7/widget/ad/a(I)V
L6:
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L7
iload 6
iload 5
isub
aload 0
getfield android/support/v7/widget/an/e I
isub
iload 1
iadd
istore 1
L8:
aload 0
iload 1
putfield android/support/v7/widget/an/f I
return
L1:
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
getfield android/graphics/Rect/left I
ineg
istore 1
goto L2
L0:
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
astore 7
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/b(Landroid/support/v7/widget/aa;)Landroid/graphics/Rect;
iconst_0
putfield android/graphics/Rect/right I
aload 7
iconst_0
putfield android/graphics/Rect/left I
iconst_0
istore 1
goto L2
L3:
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/c(Landroid/support/v7/widget/aa;)I
iconst_m1
if_icmpne L9
aload 0
iload 6
iload 4
isub
iload 5
isub
invokevirtual android/support/v7/widget/ad/a(I)V
goto L6
L9:
aload 0
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokestatic android/support/v7/widget/aa/c(Landroid/support/v7/widget/aa;)I
invokevirtual android/support/v7/widget/ad/a(I)V
goto L6
L7:
iload 1
iload 4
iadd
istore 1
goto L8
L4:
goto L5
.limit locals 8
.limit stack 4
.end method

.method public final a(Landroid/widget/ListAdapter;)V
aload 0
aload 1
invokespecial android/support/v7/widget/an/a(Landroid/widget/ListAdapter;)V
aload 0
aload 1
putfield android/support/v7/widget/ad/u Landroid/widget/ListAdapter;
return
.limit locals 2
.limit stack 2
.end method

.method public final b()V
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
istore 2
aload 0
invokevirtual android/support/v7/widget/ad/a()V
aload 0
invokevirtual android/support/v7/widget/ad/e()V
aload 0
invokespecial android/support/v7/widget/an/b()V
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
iconst_1
invokevirtual android/widget/ListView/setChoiceMode(I)V
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokevirtual android/support/v7/widget/aa/getSelectedItemPosition()I
istore 1
aload 0
getfield android/support/v7/widget/an/d Landroid/support/v7/widget/ar;
astore 3
aload 0
getfield android/support/v7/widget/an/c Landroid/widget/PopupWindow;
invokevirtual android/widget/PopupWindow/isShowing()Z
ifeq L0
aload 3
ifnull L0
aload 3
iconst_0
invokestatic android/support/v7/widget/ar/a(Landroid/support/v7/widget/ar;Z)Z
pop
aload 3
iload 1
invokevirtual android/support/v7/widget/ar/setSelection(I)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
aload 3
invokevirtual android/support/v7/widget/ar/getChoiceMode()I
ifeq L0
aload 3
iload 1
iconst_1
invokevirtual android/support/v7/widget/ar/setItemChecked(IZ)V
L0:
iload 2
ifeq L1
L2:
return
L1:
aload 0
getfield android/support/v7/widget/ad/b Landroid/support/v7/widget/aa;
invokevirtual android/support/v7/widget/aa/getViewTreeObserver()Landroid/view/ViewTreeObserver;
astore 3
aload 3
ifnull L2
new android/support/v7/widget/af
dup
aload 0
invokespecial android/support/v7/widget/af/<init>(Landroid/support/v7/widget/ad;)V
astore 4
aload 3
aload 4
invokevirtual android/view/ViewTreeObserver/addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
aload 0
new android/support/v7/widget/ag
dup
aload 0
aload 4
invokespecial android/support/v7/widget/ag/<init>(Landroid/support/v7/widget/ad;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
invokevirtual android/support/v7/widget/ad/a(Landroid/widget/PopupWindow$OnDismissListener;)V
return
.limit locals 5
.limit stack 5
.end method
