.bytecode 50.0
.class final synchronized android/support/v7/widget/bq
.super java/lang/Object

.field 'a' Ljava/lang/reflect/Method;

.field 'b' Ljava/lang/reflect/Method;

.field 'c' Ljava/lang/reflect/Method;

.field private 'd' Ljava/lang/reflect/Method;

.method <init>()V
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/NoSuchMethodException from L1 to L3 using L4
.catch java/lang/NoSuchMethodException from L3 to L5 using L6
.catch java/lang/NoSuchMethodException from L5 to L7 using L8
aload 0
invokespecial java/lang/Object/<init>()V
L0:
aload 0
ldc android/widget/AutoCompleteTextView
ldc "doBeforeTextChanged"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/widget/bq/a Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/widget/bq/a Ljava/lang/reflect/Method;
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L1:
aload 0
ldc android/widget/AutoCompleteTextView
ldc "doAfterTextChanged"
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/widget/bq/b Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/widget/bq/b Ljava/lang/reflect/Method;
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L3:
aload 0
ldc android/widget/AutoCompleteTextView
ldc "ensureImeVisible"
iconst_1
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Boolean/TYPE Ljava/lang/Class;
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/widget/bq/d Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/widget/bq/d Ljava/lang/reflect/Method;
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L5:
aload 0
ldc android/view/inputmethod/InputMethodManager
ldc "showSoftInputUnchecked"
iconst_2
anewarray java/lang/Class
dup
iconst_0
getstatic java/lang/Integer/TYPE Ljava/lang/Class;
aastore
dup
iconst_1
ldc android/os/ResultReceiver
aastore
invokevirtual java/lang/Class/getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
putfield android/support/v7/widget/bq/c Ljava/lang/reflect/Method;
aload 0
getfield android/support/v7/widget/bq/c Ljava/lang/reflect/Method;
iconst_1
invokevirtual java/lang/reflect/Method/setAccessible(Z)V
L7:
return
L8:
astore 1
return
L6:
astore 1
goto L5
L4:
astore 1
goto L3
L2:
astore 1
goto L1
.limit locals 2
.limit stack 7
.end method

.method private a(Landroid/view/inputmethod/InputMethodManager;Landroid/view/View;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v7/widget/bq/c Ljava/lang/reflect/Method;
ifnull L3
L0:
aload 0
getfield android/support/v7/widget/bq/c Ljava/lang/reflect/Method;
aload 1
iconst_2
anewarray java/lang/Object
dup
iconst_0
iconst_0
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
aconst_null
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 3
L3:
aload 1
aload 2
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/showSoftInput(Landroid/view/View;I)Z
pop
return
.limit locals 4
.limit stack 6
.end method

.method private b(Landroid/widget/AutoCompleteTextView;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v7/widget/bq/a Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 0
getfield android/support/v7/widget/bq/a Ljava/lang/reflect/Method;
aload 1
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 3
.end method

.method private c(Landroid/widget/AutoCompleteTextView;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v7/widget/bq/b Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 0
getfield android/support/v7/widget/bq/b Ljava/lang/reflect/Method;
aload 1
iconst_0
anewarray java/lang/Object
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 3
.end method

.method final a(Landroid/widget/AutoCompleteTextView;)V
.catch java/lang/Exception from L0 to L1 using L2
aload 0
getfield android/support/v7/widget/bq/d Ljava/lang/reflect/Method;
ifnull L1
L0:
aload 0
getfield android/support/v7/widget/bq/d Ljava/lang/reflect/Method;
aload 1
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
aastore
invokevirtual java/lang/reflect/Method/invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
return
L2:
astore 1
return
.limit locals 2
.limit stack 6
.end method
