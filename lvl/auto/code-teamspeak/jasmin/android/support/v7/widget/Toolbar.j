.bytecode 50.0
.class public synchronized android/support/v7/widget/Toolbar
.super android/view/ViewGroup

.field private static final 'o' Ljava/lang/String; = "Toolbar"

.field private 'A' I

.field private 'B' Ljava/lang/CharSequence;

.field private 'C' Ljava/lang/CharSequence;

.field private 'D' I

.field private 'E' I

.field private 'F' Z

.field private 'G' Z

.field private final 'H' Ljava/util/ArrayList;

.field private final 'I' [I

.field private 'J' Landroid/support/v7/widget/cl;

.field private final 'K' Landroid/support/v7/widget/o;

.field private 'L' Landroid/support/v7/internal/widget/ay;

.field private 'M' Z

.field private final 'N' Ljava/lang/Runnable;

.field private final 'O' Landroid/support/v7/internal/widget/av;

.field public 'a' Landroid/support/v7/widget/ActionMenuView;

.field public 'b' Landroid/widget/TextView;

.field public 'c' Landroid/widget/TextView;

.field 'd' Landroid/view/View;

.field public 'e' Landroid/content/Context;

.field public 'f' I

.field public 'g' I

.field public 'h' I

.field public final 'i' Landroid/support/v7/internal/widget/ak;

.field final 'j' Ljava/util/ArrayList;

.field public 'k' Landroid/support/v7/widget/ActionMenuPresenter;

.field public 'l' Landroid/support/v7/widget/cj;

.field public 'm' Landroid/support/v7/internal/view/menu/y;

.field public 'n' Landroid/support/v7/internal/view/menu/j;

.field private 'p' Landroid/widget/ImageButton;

.field private 'q' Landroid/widget/ImageView;

.field private 'r' Landroid/graphics/drawable/Drawable;

.field private 's' Ljava/lang/CharSequence;

.field private 't' Landroid/widget/ImageButton;

.field private 'u' I

.field private 'v' I

.field private 'w' I

.field private 'x' I

.field private 'y' I

.field private 'z' I

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/widget/Toolbar/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
aload 0
aload 1
aload 2
getstatic android/support/v7/a/d/toolbarStyle I
invokespecial android/support/v7/widget/Toolbar/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
return
.limit locals 3
.limit stack 4
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.annotation invisibleparam 2 Landroid/support/a/z;
.end annotation
aload 0
aload 1
aload 2
iload 3
invokespecial android/view/ViewGroup/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
aload 0
new android/support/v7/internal/widget/ak
dup
invokespecial android/support/v7/internal/widget/ak/<init>()V
putfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
aload 0
ldc_w 8388627
putfield android/support/v7/widget/Toolbar/A I
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 0
iconst_2
newarray int
putfield android/support/v7/widget/Toolbar/I [I
aload 0
new android/support/v7/widget/cg
dup
aload 0
invokespecial android/support/v7/widget/cg/<init>(Landroid/support/v7/widget/Toolbar;)V
putfield android/support/v7/widget/Toolbar/K Landroid/support/v7/widget/o;
aload 0
new android/support/v7/widget/ch
dup
aload 0
invokespecial android/support/v7/widget/ch/<init>(Landroid/support/v7/widget/Toolbar;)V
putfield android/support/v7/widget/Toolbar/N Ljava/lang/Runnable;
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aload 2
getstatic android/support/v7/a/n/Toolbar [I
iload 3
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_titleTextAppearance I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/widget/Toolbar/g I
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_subtitleTextAppearance I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
putfield android/support/v7/widget/Toolbar/h I
getstatic android/support/v7/a/n/Toolbar_android_gravity I
istore 3
aload 0
getfield android/support/v7/widget/Toolbar/A I
istore 4
aload 0
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
iload 3
iload 4
invokevirtual android/content/res/TypedArray/getInteger(II)I
putfield android/support/v7/widget/Toolbar/A I
aload 0
bipush 48
putfield android/support/v7/widget/Toolbar/u I
aload 1
getstatic android/support/v7/a/n/Toolbar_titleMargins I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 3
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/z I
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/y I
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/x I
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/w I
aload 1
getstatic android/support/v7/a/n/Toolbar_titleMarginStart I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 3
iload 3
iflt L0
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/w I
L0:
aload 1
getstatic android/support/v7/a/n/Toolbar_titleMarginEnd I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 3
iload 3
iflt L1
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/x I
L1:
aload 1
getstatic android/support/v7/a/n/Toolbar_titleMarginTop I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 3
iload 3
iflt L2
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/y I
L2:
aload 1
getstatic android/support/v7/a/n/Toolbar_titleMarginBottom I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 3
iload 3
iflt L3
aload 0
iload 3
putfield android/support/v7/widget/Toolbar/z I
L3:
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_maxButtonHeight I
iconst_m1
invokevirtual android/support/v7/internal/widget/ax/c(II)I
putfield android/support/v7/widget/Toolbar/v I
aload 1
getstatic android/support/v7/a/n/Toolbar_contentInsetStart I
ldc_w -2147483648
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 3
aload 1
getstatic android/support/v7/a/n/Toolbar_contentInsetEnd I
ldc_w -2147483648
invokevirtual android/support/v7/internal/widget/ax/b(II)I
istore 4
aload 1
getstatic android/support/v7/a/n/Toolbar_contentInsetLeft I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/c(II)I
istore 5
aload 1
getstatic android/support/v7/a/n/Toolbar_contentInsetRight I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/c(II)I
istore 6
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
iload 5
iload 6
invokevirtual android/support/v7/internal/widget/ak/b(II)V
iload 3
ldc_w -2147483648
if_icmpne L4
iload 4
ldc_w -2147483648
if_icmpeq L5
L4:
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
iload 3
iload 4
invokevirtual android/support/v7/internal/widget/ak/a(II)V
L5:
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_collapseIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
putfield android/support/v7/widget/Toolbar/r Landroid/graphics/drawable/Drawable;
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_collapseContentDescription I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
putfield android/support/v7/widget/Toolbar/s Ljava/lang/CharSequence;
aload 1
getstatic android/support/v7/a/n/Toolbar_title I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
astore 2
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L6
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setTitle(Ljava/lang/CharSequence;)V
L6:
aload 1
getstatic android/support/v7/a/n/Toolbar_subtitle I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
astore 2
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L7
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setSubtitle(Ljava/lang/CharSequence;)V
L7:
aload 0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
putfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_popupTheme I
iconst_0
invokevirtual android/support/v7/internal/widget/ax/e(II)I
invokevirtual android/support/v7/widget/Toolbar/setPopupTheme(I)V
aload 1
getstatic android/support/v7/a/n/Toolbar_navigationIcon I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L8
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
L8:
aload 1
getstatic android/support/v7/a/n/Toolbar_navigationContentDescription I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
astore 2
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L9
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setNavigationContentDescription(Ljava/lang/CharSequence;)V
L9:
aload 1
getstatic android/support/v7/a/n/Toolbar_logo I
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
ifnull L10
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setLogo(Landroid/graphics/drawable/Drawable;)V
L10:
aload 1
getstatic android/support/v7/a/n/Toolbar_logoDescription I
invokevirtual android/support/v7/internal/widget/ax/c(I)Ljava/lang/CharSequence;
astore 2
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L11
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setLogoDescription(Ljava/lang/CharSequence;)V
L11:
aload 1
getstatic android/support/v7/a/n/Toolbar_titleTextColor I
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L12
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_titleTextColor I
invokevirtual android/support/v7/internal/widget/ax/d(I)I
invokevirtual android/support/v7/widget/Toolbar/setTitleTextColor(I)V
L12:
aload 1
getstatic android/support/v7/a/n/Toolbar_subtitleTextColor I
invokevirtual android/support/v7/internal/widget/ax/e(I)Z
ifeq L13
aload 0
aload 1
getstatic android/support/v7/a/n/Toolbar_subtitleTextColor I
invokevirtual android/support/v7/internal/widget/ax/d(I)I
invokevirtual android/support/v7/widget/Toolbar/setSubtitleTextColor(I)V
L13:
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
putfield android/support/v7/widget/Toolbar/O Landroid/support/v7/internal/widget/av;
return
.limit locals 7
.limit stack 4
.end method

.method private a(Landroid/view/View;I)I
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 7
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
istore 5
iload 2
ifle L0
iload 5
iload 2
isub
iconst_2
idiv
istore 2
L1:
aload 7
getfield android/support/v7/widget/ck/a I
bipush 112
iand
istore 4
iload 4
istore 3
iload 4
lookupswitch
16 : L2
48 : L2
80 : L2
default : L3
L3:
aload 0
getfield android/support/v7/widget/Toolbar/A I
bipush 112
iand
istore 3
L2:
iload 3
lookupswitch
48 : L4
80 : L5
default : L6
L6:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
istore 3
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingBottom()I
istore 4
aload 0
invokevirtual android/support/v7/widget/Toolbar/getHeight()I
istore 6
iload 6
iload 3
isub
iload 4
isub
iload 5
isub
iconst_2
idiv
istore 2
iload 2
aload 7
getfield android/support/v7/widget/ck/topMargin I
if_icmpge L7
aload 7
getfield android/support/v7/widget/ck/topMargin I
istore 2
L8:
iload 2
iload 3
iadd
ireturn
L0:
iconst_0
istore 2
goto L1
L4:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
iload 2
isub
ireturn
L5:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getHeight()I
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingBottom()I
isub
iload 5
isub
aload 7
getfield android/support/v7/widget/ck/bottomMargin I
isub
iload 2
isub
ireturn
L7:
iload 6
iload 4
isub
iload 5
isub
iload 2
isub
iload 3
isub
istore 4
iload 4
aload 7
getfield android/support/v7/widget/ck/bottomMargin I
if_icmpge L9
iconst_0
iload 2
aload 7
getfield android/support/v7/widget/ck/bottomMargin I
iload 4
isub
isub
invokestatic java/lang/Math/max(II)I
istore 2
goto L8
L9:
goto L8
.limit locals 8
.limit stack 4
.end method

.method private a(Landroid/view/View;IIII[I)I
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 10
aload 10
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
aload 6
iconst_0
iaload
isub
istore 7
aload 10
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
aload 6
iconst_1
iaload
isub
istore 8
iconst_0
iload 7
invokestatic java/lang/Math/max(II)I
iconst_0
iload 8
invokestatic java/lang/Math/max(II)I
iadd
istore 9
aload 6
iconst_0
iconst_0
iload 7
ineg
invokestatic java/lang/Math/max(II)I
iastore
aload 6
iconst_1
iconst_0
iload 8
ineg
invokestatic java/lang/Math/max(II)I
iastore
aload 1
iload 2
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingRight()I
iadd
iload 9
iadd
iload 3
iadd
aload 10
getfield android/view/ViewGroup$MarginLayoutParams/width I
invokestatic android/support/v7/widget/Toolbar/getChildMeasureSpec(III)I
iload 4
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingBottom()I
iadd
aload 10
getfield android/view/ViewGroup$MarginLayoutParams/topMargin I
iadd
aload 10
getfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
iadd
iload 5
iadd
aload 10
getfield android/view/ViewGroup$MarginLayoutParams/height I
invokestatic android/support/v7/widget/Toolbar/getChildMeasureSpec(III)I
invokevirtual android/view/View/measure(II)V
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
iload 9
iadd
ireturn
.limit locals 11
.limit stack 5
.end method

.method private a(Landroid/view/View;I[II)I
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 6
aload 6
getfield android/support/v7/widget/ck/leftMargin I
aload 3
iconst_0
iaload
isub
istore 5
iconst_0
iload 5
invokestatic java/lang/Math/max(II)I
iload 2
iadd
istore 2
aload 3
iconst_0
iconst_0
iload 5
ineg
invokestatic java/lang/Math/max(II)I
iastore
aload 0
aload 1
iload 4
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I)I
istore 4
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 1
iload 2
iload 4
iload 2
iload 5
iadd
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
iload 4
iadd
invokevirtual android/view/View/layout(IIII)V
aload 6
getfield android/support/v7/widget/ck/rightMargin I
iload 5
iadd
iload 2
iadd
ireturn
.limit locals 7
.limit stack 6
.end method

.method private static a(Ljava/util/List;[I)I
aload 1
iconst_0
iaload
istore 5
aload 1
iconst_1
iaload
istore 4
aload 0
invokeinterface java/util/List/size()I 0
istore 6
iconst_0
istore 3
iconst_0
istore 2
L0:
iload 3
iload 6
if_icmpge L1
aload 0
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 1
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 10
aload 10
getfield android/support/v7/widget/ck/leftMargin I
iload 5
isub
istore 5
aload 10
getfield android/support/v7/widget/ck/rightMargin I
iload 4
isub
istore 4
iconst_0
iload 5
invokestatic java/lang/Math/max(II)I
istore 7
iconst_0
iload 4
invokestatic java/lang/Math/max(II)I
istore 8
iconst_0
iload 5
ineg
invokestatic java/lang/Math/max(II)I
istore 5
iconst_0
iload 4
ineg
invokestatic java/lang/Math/max(II)I
istore 4
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
istore 9
iload 3
iconst_1
iadd
istore 3
iload 2
iload 9
iload 7
iadd
iload 8
iadd
iadd
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 11
.limit stack 3
.end method

.method private a(Landroid/util/AttributeSet;)Landroid/support/v7/widget/ck;
new android/support/v7/widget/ck
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/widget/ck/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ck;
aload 0
instanceof android/support/v7/widget/ck
ifeq L0
new android/support/v7/widget/ck
dup
aload 0
checkcast android/support/v7/widget/ck
invokespecial android/support/v7/widget/ck/<init>(Landroid/support/v7/widget/ck;)V
areturn
L0:
aload 0
instanceof android/support/v7/app/c
ifeq L1
new android/support/v7/widget/ck
dup
aload 0
checkcast android/support/v7/app/c
invokespecial android/support/v7/widget/ck/<init>(Landroid/support/v7/app/c;)V
areturn
L1:
aload 0
instanceof android/view/ViewGroup$MarginLayoutParams
ifeq L2
new android/support/v7/widget/ck
dup
aload 0
checkcast android/view/ViewGroup$MarginLayoutParams
invokespecial android/support/v7/widget/ck/<init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
areturn
L2:
new android/support/v7/widget/ck
dup
aload 0
invokespecial android/support/v7/widget/ck/<init>(Landroid/view/ViewGroup$LayoutParams;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Landroid/support/v7/widget/Toolbar;)Landroid/support/v7/widget/cl;
aload 0
getfield android/support/v7/widget/Toolbar/J Landroid/support/v7/widget/cl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
.annotation invisibleparam 1 Landroid/support/a/x;
.end annotation
aload 0
invokespecial android/support/v7/widget/Toolbar/getMenuInflater()Landroid/view/MenuInflater;
iload 1
aload 0
invokevirtual android/support/v7/widget/Toolbar/getMenu()Landroid/view/Menu;
invokevirtual android/view/MenuInflater/inflate(ILandroid/view/Menu;)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(II)V
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
iload 1
iload 2
invokevirtual android/support/v7/internal/widget/ak/a(II)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/content/Context;I)V
.annotation invisibleparam 2 Landroid/support/a/ai;
.end annotation
aload 0
iload 2
putfield android/support/v7/widget/Toolbar/g I
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
aload 1
iload 2
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private a(Landroid/support/v7/internal/view/menu/i;Landroid/support/v7/widget/ActionMenuPresenter;)V
aload 1
ifnonnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnonnull L0
L1:
return
L0:
aload 0
invokevirtual android/support/v7/widget/Toolbar/d()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
astore 3
aload 3
aload 1
if_acmpeq L1
aload 3
ifnull L2
aload 3
aload 0
getfield android/support/v7/widget/Toolbar/k Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/x;)V
aload 3
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
invokevirtual android/support/v7/internal/view/menu/i/b(Landroid/support/v7/internal/view/menu/x;)V
L2:
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnonnull L3
aload 0
new android/support/v7/widget/cj
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/cj/<init>(Landroid/support/v7/widget/Toolbar;B)V
putfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
L3:
aload 2
iconst_1
putfield android/support/v7/widget/ActionMenuPresenter/o Z
aload 1
ifnull L4
aload 1
aload 2
aload 0
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
aload 1
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
aload 0
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
L5:
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 0
getfield android/support/v7/widget/Toolbar/f I
invokevirtual android/support/v7/widget/ActionMenuView/setPopupTheme(I)V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 2
invokevirtual android/support/v7/widget/ActionMenuView/setPresenter(Landroid/support/v7/widget/ActionMenuPresenter;)V
aload 0
aload 2
putfield android/support/v7/widget/Toolbar/k Landroid/support/v7/widget/ActionMenuPresenter;
return
L4:
aload 2
aload 0
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
aconst_null
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
aload 0
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
aconst_null
invokevirtual android/support/v7/widget/cj/a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;)V
aload 2
iconst_1
invokevirtual android/support/v7/widget/ActionMenuPresenter/a(Z)V
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
iconst_1
invokevirtual android/support/v7/widget/cj/a(Z)V
goto L5
.limit locals 4
.limit stack 5
.end method

.method private a(Landroid/support/v7/internal/view/menu/y;Landroid/support/v7/internal/view/menu/j;)V
aload 0
aload 1
putfield android/support/v7/widget/Toolbar/m Landroid/support/v7/internal/view/menu/y;
aload 0
aload 2
putfield android/support/v7/widget/Toolbar/n Landroid/support/v7/internal/view/menu/j;
return
.limit locals 3
.limit stack 2
.end method

.method private a(Landroid/view/View;IIII)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 7
iload 2
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingLeft()I
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingRight()I
iadd
aload 7
getfield android/view/ViewGroup$MarginLayoutParams/leftMargin I
iadd
aload 7
getfield android/view/ViewGroup$MarginLayoutParams/rightMargin I
iadd
iload 3
iadd
aload 7
getfield android/view/ViewGroup$MarginLayoutParams/width I
invokestatic android/support/v7/widget/Toolbar/getChildMeasureSpec(III)I
istore 6
iload 4
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingBottom()I
iadd
aload 7
getfield android/view/ViewGroup$MarginLayoutParams/topMargin I
iadd
aload 7
getfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
iadd
iconst_0
iadd
aload 7
getfield android/view/ViewGroup$MarginLayoutParams/height I
invokestatic android/support/v7/widget/Toolbar/getChildMeasureSpec(III)I
istore 3
iload 3
invokestatic android/view/View$MeasureSpec/getMode(I)I
istore 4
iload 3
istore 2
iload 4
ldc_w 1073741824
if_icmpeq L0
iload 3
istore 2
iload 5
iflt L0
iload 5
istore 2
iload 4
ifeq L1
iload 3
invokestatic android/view/View$MeasureSpec/getSize(I)I
iload 5
invokestatic java/lang/Math/min(II)I
istore 2
L1:
iload 2
ldc_w 1073741824
invokestatic android/view/View$MeasureSpec/makeMeasureSpec(II)I
istore 2
L0:
aload 1
iload 6
iload 2
invokevirtual android/view/View/measure(II)V
return
.limit locals 8
.limit stack 3
.end method

.method private a(Landroid/view/View;Z)V
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
astore 3
aload 3
ifnonnull L0
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
astore 3
L1:
aload 3
iconst_1
putfield android/support/v7/widget/ck/e I
iload 2
ifeq L2
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
ifnull L2
aload 1
aload 3
invokevirtual android/view/View/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
return
L0:
aload 0
aload 3
invokevirtual android/support/v7/widget/Toolbar/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifne L3
aload 3
invokestatic android/support/v7/widget/Toolbar/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ck;
astore 3
goto L1
L3:
aload 3
checkcast android/support/v7/widget/ck
astore 3
goto L1
L2:
aload 0
aload 1
aload 3
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/util/List;I)V
iconst_1
istore 3
iconst_0
istore 4
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
L1:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getChildCount()I
istore 6
iload 2
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
invokestatic android/support/v4/view/v/a(II)I
istore 5
aload 1
invokeinterface java/util/List/clear()V 0
iload 4
istore 2
iload 3
ifeq L2
iload 6
iconst_1
isub
istore 2
L3:
iload 2
iflt L4
aload 0
iload 2
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 7
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 8
aload 8
getfield android/support/v7/widget/ck/e I
ifne L5
aload 0
aload 7
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L5
aload 0
aload 8
getfield android/support/v7/widget/ck/a I
invokespecial android/support/v7/widget/Toolbar/c(I)I
iload 5
if_icmpne L5
aload 1
aload 7
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L5:
iload 2
iconst_1
isub
istore 2
goto L3
L0:
iconst_0
istore 3
goto L1
L2:
iload 2
iload 6
if_icmpge L4
aload 0
iload 2
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 7
aload 7
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 8
aload 8
getfield android/support/v7/widget/ck/e I
ifne L6
aload 0
aload 7
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L6
aload 0
aload 8
getfield android/support/v7/widget/ck/a I
invokespecial android/support/v7/widget/Toolbar/c(I)I
iload 5
if_icmpne L6
aload 1
aload 7
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L6:
iload 2
iconst_1
iadd
istore 2
goto L2
L4:
return
.limit locals 9
.limit stack 2
.end method

.method private a(Landroid/view/View;)Z
aload 1
ifnull L0
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpne L0
aload 1
invokevirtual android/view/View/getVisibility()I
bipush 8
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(I)I
iload 1
bipush 112
iand
istore 2
iload 2
istore 1
iload 2
lookupswitch
16 : L0
48 : L0
80 : L0
default : L1
L1:
aload 0
getfield android/support/v7/widget/Toolbar/A I
bipush 112
iand
istore 1
L0:
iload 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private static b(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 0
aload 0
invokestatic android/support/v4/view/at/a(Landroid/view/ViewGroup$MarginLayoutParams;)I
istore 1
aload 0
invokestatic android/support/v4/view/at/b(Landroid/view/ViewGroup$MarginLayoutParams;)I
iload 1
iadd
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Landroid/view/View;I[II)I
aload 1
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 6
aload 6
getfield android/support/v7/widget/ck/rightMargin I
aload 3
iconst_1
iaload
isub
istore 5
iload 2
iconst_0
iload 5
invokestatic java/lang/Math/max(II)I
isub
istore 2
aload 3
iconst_1
iconst_0
iload 5
ineg
invokestatic java/lang/Math/max(II)I
iastore
aload 0
aload 1
iload 4
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I)I
istore 4
aload 1
invokevirtual android/view/View/getMeasuredWidth()I
istore 5
aload 1
iload 2
iload 5
isub
iload 4
iload 2
aload 1
invokevirtual android/view/View/getMeasuredHeight()I
iload 4
iadd
invokevirtual android/view/View/layout(IIII)V
iload 2
aload 6
getfield android/support/v7/widget/ck/leftMargin I
iload 5
iadd
isub
ireturn
.limit locals 7
.limit stack 6
.end method

.method private b(II)V
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
iload 1
iload 2
invokevirtual android/support/v7/internal/widget/ak/b(II)V
return
.limit locals 3
.limit stack 3
.end method

.method private b(Landroid/content/Context;I)V
.annotation invisibleparam 2 Landroid/support/a/ai;
.end annotation
aload 0
iload 2
putfield android/support/v7/widget/Toolbar/h I
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
aload 1
iload 2
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L0:
return
.limit locals 3
.limit stack 3
.end method

.method static synthetic b(Landroid/support/v7/widget/Toolbar;)V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
ifnonnull L0
aload 0
new android/widget/ImageButton
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/toolbarNavigationButtonStyle I
invokespecial android/widget/ImageButton/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
putfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/r Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageButton/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/s Ljava/lang/CharSequence;
invokevirtual android/widget/ImageButton/setContentDescription(Ljava/lang/CharSequence;)V
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
astore 1
aload 1
ldc_w 8388611
aload 0
getfield android/support/v7/widget/Toolbar/u I
bipush 112
iand
ior
putfield android/support/v7/widget/ck/a I
aload 1
iconst_2
putfield android/support/v7/widget/ck/e I
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 1
invokevirtual android/widget/ImageButton/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
new android/support/v7/widget/ci
dup
aload 0
invokespecial android/support/v7/widget/ci/<init>(Landroid/support/v7/widget/Toolbar;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method private c(I)I
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
istore 3
iload 1
iload 3
invokestatic android/support/v4/view/v/a(II)I
bipush 7
iand
istore 2
iload 2
istore 1
iload 2
tableswitch 1
L0
L1
L0
L1
L0
default : L1
L1:
iload 3
iconst_1
if_icmpne L2
iconst_5
istore 1
L0:
iload 1
ireturn
L2:
iconst_3
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static c(Landroid/view/View;)I
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/view/ViewGroup$MarginLayoutParams
astore 0
aload 0
getfield android/view/ViewGroup$MarginLayoutParams/topMargin I
istore 1
aload 0
getfield android/view/ViewGroup$MarginLayoutParams/bottomMargin I
iload 1
iadd
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Landroid/support/v7/widget/Toolbar;)Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Landroid/support/v7/widget/Toolbar;)I
aload 0
getfield android/support/v7/widget/Toolbar/u I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Landroid/view/View;)Z
aload 0
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
getfield android/support/v7/widget/ck/e I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected static e()Landroid/support/v7/widget/ck;
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private e(Landroid/view/View;)Z
aload 1
invokevirtual android/view/View/getParent()Landroid/view/ViewParent;
aload 0
if_acmpeq L0
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 1
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private f()Z
aload 0
invokevirtual android/support/v7/widget/Toolbar/getVisibility()I
ifne L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
getfield android/support/v7/widget/ActionMenuView/d Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/j()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method private getMenuInflater()Landroid/view/MenuInflater;
new android/support/v7/internal/view/f
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
invokespecial android/support/v7/internal/view/f/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private h()Z
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/f()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method private i()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/b()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private j()Z
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnonnull L0
L1:
iconst_0
ireturn
L0:
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayout()Landroid/text/Layout;
astore 3
aload 3
ifnull L1
aload 3
invokevirtual android/text/Layout/getLineCount()I
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L1
aload 3
iload 1
invokevirtual android/text/Layout/getEllipsisCount(I)I
ifle L3
iconst_1
ireturn
L3:
iload 1
iconst_1
iadd
istore 1
goto L2
.limit locals 4
.limit stack 2
.end method

.method private k()V
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
ifnonnull L0
aload 0
new android/widget/ImageView
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
L0:
return
.limit locals 1
.limit stack 4
.end method

.method private l()Z
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private m()V
aload 0
invokevirtual android/support/v7/widget/Toolbar/d()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
ifnonnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/getMenu()Landroid/view/Menu;
checkcast android/support/v7/internal/view/menu/i
astore 1
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnonnull L1
aload 0
new android/support/v7/widget/cj
dup
aload 0
iconst_0
invokespecial android/support/v7/widget/cj/<init>(Landroid/support/v7/widget/Toolbar;B)V
putfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
L1:
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
iconst_1
invokevirtual android/support/v7/widget/ActionMenuView/setExpandedActionViewsExclusive(Z)V
aload 1
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
aload 0
getfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
invokevirtual android/support/v7/internal/view/menu/i/a(Landroid/support/v7/internal/view/menu/x;Landroid/content/Context;)V
L0:
return
.limit locals 2
.limit stack 5
.end method

.method private n()V
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
ifnonnull L0
aload 0
new android/widget/ImageButton
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/toolbarNavigationButtonStyle I
invokespecial android/widget/ImageButton/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
putfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
astore 1
aload 1
ldc_w 8388611
aload 0
getfield android/support/v7/widget/Toolbar/u I
bipush 112
iand
ior
putfield android/support/v7/widget/ck/a I
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
aload 1
invokevirtual android/widget/ImageButton/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method private o()V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
ifnonnull L0
aload 0
new android/widget/ImageButton
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aconst_null
getstatic android/support/v7/a/d/toolbarNavigationButtonStyle I
invokespecial android/widget/ImageButton/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
putfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/r Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/ImageButton/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 0
getfield android/support/v7/widget/Toolbar/s Ljava/lang/CharSequence;
invokevirtual android/widget/ImageButton/setContentDescription(Ljava/lang/CharSequence;)V
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
astore 1
aload 1
ldc_w 8388611
aload 0
getfield android/support/v7/widget/Toolbar/u I
bipush 112
iand
ior
putfield android/support/v7/widget/ck/a I
aload 1
iconst_2
putfield android/support/v7/widget/ck/e I
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
aload 1
invokevirtual android/widget/ImageButton/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
new android/support/v7/widget/ci
dup
aload 0
invokespecial android/support/v7/widget/ci/<init>(Landroid/support/v7/widget/Toolbar;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
L0:
return
.limit locals 2
.limit stack 6
.end method

.method private p()V
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/N Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/Toolbar/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/N Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/Toolbar/post(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method private q()Z
aload 0
getfield android/support/v7/widget/Toolbar/M Z
ifne L0
L1:
iconst_0
ireturn
L0:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getChildCount()I
istore 2
iconst_0
istore 1
L2:
iload 1
iload 2
if_icmpge L3
aload 0
iload 1
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 3
aload 0
aload 3
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L4
aload 3
invokevirtual android/view/View/getMeasuredWidth()I
ifle L4
aload 3
invokevirtual android/view/View/getMeasuredHeight()I
ifgt L1
L4:
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private r()V
aload 0
invokevirtual android/support/v7/widget/Toolbar/getChildCount()I
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
iload 1
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 2
aload 2
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
getfield android/support/v7/widget/ck/e I
iconst_2
if_icmpeq L2
aload 2
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
if_acmpeq L2
aload 0
iload 1
invokevirtual android/support/v7/widget/Toolbar/removeViewAt(I)V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 2
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L2:
iload 1
iconst_1
isub
istore 1
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method private s()V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
istore 1
L0:
iload 1
iflt L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
invokevirtual android/support/v7/widget/Toolbar/addView(Landroid/view/View;)V
iload 1
iconst_1
isub
istore 1
goto L0
L1:
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
return
.limit locals 2
.limit stack 3
.end method

.method public final a()Z
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/i()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public final b()Z
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 2
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
ifnull L1
aload 2
getfield android/support/v7/widget/ActionMenuView/e Landroid/support/v7/widget/ActionMenuPresenter;
invokevirtual android/support/v7/widget/ActionMenuPresenter/e()Z
ifeq L1
iconst_1
istore 1
L2:
iload 1
ifeq L0
iconst_1
ireturn
L1:
iconst_0
istore 1
goto L2
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 1
.end method

.method public final c()V
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnonnull L0
aconst_null
astore 1
L1:
aload 1
ifnull L2
aload 1
invokevirtual android/support/v7/internal/view/menu/m/collapseActionView()Z
pop
L2:
return
L0:
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
astore 1
goto L1
.limit locals 2
.limit stack 1
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
aload 0
aload 1
invokespecial android/view/ViewGroup/checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
ifeq L0
aload 1
instanceof android/support/v7/widget/ck
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final d()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnonnull L0
aload 0
new android/support/v7/widget/ActionMenuView
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
invokespecial android/support/v7/widget/ActionMenuView/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 0
getfield android/support/v7/widget/Toolbar/f I
invokevirtual android/support/v7/widget/ActionMenuView/setPopupTheme(I)V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 0
getfield android/support/v7/widget/Toolbar/K Landroid/support/v7/widget/o;
invokevirtual android/support/v7/widget/ActionMenuView/setOnMenuItemClickListener(Landroid/support/v7/widget/o;)V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
astore 1
aload 0
getfield android/support/v7/widget/Toolbar/m Landroid/support/v7/internal/view/menu/y;
astore 2
aload 0
getfield android/support/v7/widget/Toolbar/n Landroid/support/v7/internal/view/menu/j;
astore 3
aload 1
aload 2
putfield android/support/v7/widget/ActionMenuView/f Landroid/support/v7/internal/view/menu/y;
aload 1
aload 3
putfield android/support/v7/widget/ActionMenuView/g Landroid/support/v7/internal/view/menu/j;
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
astore 1
aload 1
ldc_w 8388613
aload 0
getfield android/support/v7/widget/Toolbar/u I
bipush 112
iand
ior
putfield android/support/v7/widget/ck/a I
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 1
invokevirtual android/support/v7/widget/ActionMenuView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
iconst_0
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;Z)V
L0:
return
.limit locals 4
.limit stack 4
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
new android/support/v7/widget/ck
dup
invokespecial android/support/v7/widget/ck/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
new android/support/v7/widget/ck
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
aload 1
invokespecial android/support/v7/widget/ck/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method protected synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
aload 1
invokestatic android/support/v7/widget/Toolbar/a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ck;
areturn
.limit locals 2
.limit stack 1
.end method

.method public getContentInsetEnd()I
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
astore 1
aload 1
getfield android/support/v7/internal/widget/ak/h Z
ifeq L0
aload 1
getfield android/support/v7/internal/widget/ak/b I
ireturn
L0:
aload 1
getfield android/support/v7/internal/widget/ak/c I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public getContentInsetLeft()I
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
getfield android/support/v7/internal/widget/ak/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getContentInsetRight()I
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
getfield android/support/v7/internal/widget/ak/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getContentInsetStart()I
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
astore 1
aload 1
getfield android/support/v7/internal/widget/ak/h Z
ifeq L0
aload 1
getfield android/support/v7/internal/widget/ak/c I
ireturn
L0:
aload 1
getfield android/support/v7/internal/widget/ak/b I
ireturn
.limit locals 2
.limit stack 1
.end method

.method public getLogo()Landroid/graphics/drawable/Drawable;
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getDrawable()Landroid/graphics/drawable/Drawable;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getLogoDescription()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getContentDescription()Ljava/lang/CharSequence;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getMenu()Landroid/view/Menu;
aload 0
invokespecial android/support/v7/widget/Toolbar/m()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/getMenu()Landroid/view/Menu;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNavigationContentDescription()Ljava/lang/CharSequence;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getContentDescription()Ljava/lang/CharSequence;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getNavigationIcon()Landroid/graphics/drawable/Drawable;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getDrawable()Landroid/graphics/drawable/Drawable;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOverflowIcon()Landroid/graphics/drawable/Drawable;
.annotation invisible Landroid/support/a/z;
.end annotation
aload 0
invokespecial android/support/v7/widget/Toolbar/m()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/getOverflowIcon()Landroid/graphics/drawable/Drawable;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getPopupTheme()I
aload 0
getfield android/support/v7/widget/Toolbar/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSubtitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/Toolbar/C Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getTitle()Ljava/lang/CharSequence;
aload 0
getfield android/support/v7/widget/Toolbar/B Ljava/lang/CharSequence;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getWrapper()Landroid/support/v7/internal/widget/ad;
aload 0
getfield android/support/v7/widget/Toolbar/L Landroid/support/v7/internal/widget/ay;
ifnonnull L0
aload 0
new android/support/v7/internal/widget/ay
dup
aload 0
iconst_1
invokespecial android/support/v7/internal/widget/ay/<init>(Landroid/support/v7/widget/Toolbar;Z)V
putfield android/support/v7/widget/Toolbar/L Landroid/support/v7/internal/widget/ay;
L0:
aload 0
getfield android/support/v7/widget/Toolbar/L Landroid/support/v7/internal/widget/ay;
areturn
.limit locals 1
.limit stack 5
.end method

.method protected onDetachedFromWindow()V
aload 0
invokespecial android/view/ViewGroup/onDetachedFromWindow()V
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/N Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/Toolbar/removeCallbacks(Ljava/lang/Runnable;)Z
pop
return
.limit locals 1
.limit stack 2
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
bipush 9
if_icmpne L0
aload 0
iconst_0
putfield android/support/v7/widget/Toolbar/G Z
L0:
aload 0
getfield android/support/v7/widget/Toolbar/G Z
ifne L1
aload 0
aload 1
invokespecial android/view/ViewGroup/onHoverEvent(Landroid/view/MotionEvent;)Z
istore 3
iload 2
bipush 9
if_icmpne L1
iload 3
ifne L1
aload 0
iconst_1
putfield android/support/v7/widget/Toolbar/G Z
L1:
iload 2
bipush 10
if_icmpeq L2
iload 2
iconst_3
if_icmpne L3
L2:
aload 0
iconst_0
putfield android/support/v7/widget/Toolbar/G Z
L3:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method protected onLayout(ZIIII)V
aload 0
invokestatic android/support/v4/view/cx/f(Landroid/view/View;)I
iconst_1
if_icmpne L0
iconst_1
istore 8
L1:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getWidth()I
istore 11
aload 0
invokevirtual android/support/v7/widget/Toolbar/getHeight()I
istore 14
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingLeft()I
istore 6
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingRight()I
istore 12
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
istore 13
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingBottom()I
istore 15
iload 11
iload 12
isub
istore 5
aload 0
getfield android/support/v7/widget/Toolbar/I [I
astore 19
aload 19
iconst_1
iconst_0
iastore
aload 19
iconst_0
iconst_0
iastore
aload 0
invokestatic android/support/v4/view/cx/o(Landroid/view/View;)I
istore 10
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L2
iload 8
ifeq L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
iload 5
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/b(Landroid/view/View;I[II)I
istore 5
iload 6
istore 2
L4:
iload 5
istore 3
iload 2
istore 4
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L5
iload 8
ifeq L6
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
iload 5
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/b(Landroid/view/View;I[II)I
istore 3
iload 2
istore 4
L5:
iload 3
istore 2
iload 4
istore 5
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L7
iload 8
ifeq L8
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
iload 4
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 5
iload 3
istore 2
L7:
aload 19
iconst_0
iconst_0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContentInsetLeft()I
iload 5
isub
invokestatic java/lang/Math/max(II)I
iastore
aload 19
iconst_1
iconst_0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContentInsetRight()I
iload 11
iload 12
isub
iload 2
isub
isub
invokestatic java/lang/Math/max(II)I
iastore
iload 5
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContentInsetLeft()I
invokestatic java/lang/Math/max(II)I
istore 4
iload 2
iload 11
iload 12
isub
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContentInsetRight()I
isub
invokestatic java/lang/Math/min(II)I
istore 5
iload 5
istore 3
iload 4
istore 2
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L9
iload 8
ifeq L10
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
iload 5
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/b(Landroid/view/View;I[II)I
istore 3
iload 4
istore 2
L9:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L11
iload 8
ifeq L12
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
iload 3
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/b(Landroid/view/View;I[II)I
istore 3
iload 2
istore 4
L13:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
istore 1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
istore 16
iconst_0
istore 2
iload 1
ifeq L14
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
aload 17
getfield android/support/v7/widget/ck/topMargin I
istore 2
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 5
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
iload 2
iload 5
iadd
iadd
iconst_0
iadd
istore 2
L14:
iload 16
ifeq L15
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
aload 17
getfield android/support/v7/widget/ck/topMargin I
istore 5
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 7
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
iload 5
iload 7
iadd
iadd
iload 2
iadd
istore 9
L16:
iload 1
ifne L17
iload 3
istore 5
iload 4
istore 2
iload 16
ifeq L18
L17:
iload 1
ifeq L19
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
astore 17
L20:
iload 16
ifeq L21
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
astore 18
L22:
aload 17
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
aload 18
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 18
iload 1
ifeq L23
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
ifgt L24
L23:
iload 16
ifeq L25
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
ifle L25
L24:
iconst_1
istore 7
L26:
aload 0
getfield android/support/v7/widget/Toolbar/A I
bipush 112
iand
lookupswitch
48 : L27
80 : L28
default : L29
L29:
iload 14
iload 13
isub
iload 15
isub
iload 9
isub
iconst_2
idiv
istore 2
iload 2
aload 17
getfield android/support/v7/widget/ck/topMargin I
aload 0
getfield android/support/v7/widget/Toolbar/y I
iadd
if_icmpge L30
aload 17
getfield android/support/v7/widget/ck/topMargin I
aload 0
getfield android/support/v7/widget/Toolbar/y I
iadd
istore 2
L31:
iload 13
iload 2
iadd
istore 2
L32:
iload 8
ifeq L33
iload 7
ifeq L34
aload 0
getfield android/support/v7/widget/Toolbar/w I
istore 5
L35:
iload 5
aload 19
iconst_1
iaload
isub
istore 5
iload 3
iconst_0
iload 5
invokestatic java/lang/Math/max(II)I
isub
istore 3
aload 19
iconst_1
iconst_0
iload 5
ineg
invokestatic java/lang/Math/max(II)I
iastore
iload 1
ifeq L36
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
iload 3
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
isub
istore 5
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
iload 2
iadd
istore 8
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
iload 5
iload 2
iload 3
iload 8
invokevirtual android/widget/TextView/layout(IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/x I
istore 9
iload 8
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
iadd
istore 2
iload 5
iload 9
isub
istore 5
L37:
iload 16
ifeq L38
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
aload 17
getfield android/support/v7/widget/ck/topMargin I
iload 2
iadd
istore 2
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
istore 8
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 9
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
iload 3
iload 8
isub
iload 2
iload 3
iload 9
iload 2
iadd
invokevirtual android/widget/TextView/layout(IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/x I
istore 2
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
istore 8
iload 3
iload 2
isub
istore 2
L39:
iload 7
ifeq L40
iload 5
iload 2
invokestatic java/lang/Math/min(II)I
istore 2
L41:
iload 2
istore 5
iload 4
istore 2
L18:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
iconst_3
invokespecial android/support/v7/widget/Toolbar/a(Ljava/util/List;I)V
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 4
iconst_0
istore 3
L42:
iload 3
iload 4
if_icmpge L43
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
iload 3
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
iload 2
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 2
iload 3
iconst_1
iadd
istore 3
goto L42
L0:
iconst_0
istore 8
goto L1
L3:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
iload 6
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 2
goto L4
L6:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
iload 2
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 4
iload 5
istore 3
goto L5
L8:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
iload 3
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/b(Landroid/view/View;I[II)I
istore 2
iload 4
istore 5
goto L7
L10:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
iload 4
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 2
iload 5
istore 3
goto L9
L12:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
iload 2
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 4
goto L13
L19:
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
astore 17
goto L20
L21:
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
astore 18
goto L22
L25:
iconst_0
istore 7
goto L26
L27:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
istore 2
aload 17
getfield android/support/v7/widget/ck/topMargin I
iload 2
iadd
aload 0
getfield android/support/v7/widget/Toolbar/y I
iadd
istore 2
goto L32
L30:
iload 14
iload 15
isub
iload 9
isub
iload 2
isub
iload 13
isub
istore 5
iload 5
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
aload 0
getfield android/support/v7/widget/Toolbar/z I
iadd
if_icmpge L44
iconst_0
iload 2
aload 18
getfield android/support/v7/widget/ck/bottomMargin I
aload 0
getfield android/support/v7/widget/Toolbar/z I
iadd
iload 5
isub
isub
invokestatic java/lang/Math/max(II)I
istore 2
goto L31
L28:
iload 14
iload 15
isub
aload 18
getfield android/support/v7/widget/ck/bottomMargin I
isub
aload 0
getfield android/support/v7/widget/Toolbar/z I
isub
iload 9
isub
istore 2
goto L32
L34:
iconst_0
istore 5
goto L35
L33:
iload 7
ifeq L45
aload 0
getfield android/support/v7/widget/Toolbar/w I
istore 5
L46:
iload 5
aload 19
iconst_0
iaload
isub
istore 5
iload 4
iconst_0
iload 5
invokestatic java/lang/Math/max(II)I
iadd
istore 4
aload 19
iconst_0
iconst_0
iload 5
ineg
invokestatic java/lang/Math/max(II)I
iastore
iload 1
ifeq L47
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
iload 4
iadd
istore 8
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
iload 2
iadd
istore 5
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
iload 4
iload 2
iload 8
iload 5
invokevirtual android/widget/TextView/layout(IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/x I
istore 9
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
istore 2
iload 8
iload 9
iadd
istore 8
iload 2
iload 5
iadd
istore 2
L48:
iload 16
ifeq L49
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 17
iload 2
aload 17
getfield android/support/v7/widget/ck/topMargin I
iadd
istore 5
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
iload 4
iadd
istore 2
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
istore 9
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
iload 4
iload 5
iload 2
iload 9
iload 5
iadd
invokevirtual android/widget/TextView/layout(IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/x I
istore 5
aload 17
getfield android/support/v7/widget/ck/bottomMargin I
istore 9
iload 5
iload 2
iadd
istore 9
L50:
iload 3
istore 5
iload 4
istore 2
iload 7
ifeq L18
iload 8
iload 9
invokestatic java/lang/Math/max(II)I
istore 2
iload 3
istore 5
goto L18
L45:
iconst_0
istore 5
goto L46
L43:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
iconst_5
invokespecial android/support/v7/widget/Toolbar/a(Ljava/util/List;I)V
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 7
iconst_0
istore 4
iload 5
istore 3
L51:
iload 4
iload 7
if_icmpge L52
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
iload 4
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
iload 3
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/b(Landroid/view/View;I[II)I
istore 3
iload 4
iconst_1
iadd
istore 4
goto L51
L52:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
iconst_1
invokespecial android/support/v7/widget/Toolbar/a(Ljava/util/List;I)V
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
astore 17
aload 19
iconst_0
iaload
istore 8
aload 19
iconst_1
iaload
istore 7
aload 17
invokeinterface java/util/List/size()I 0
istore 9
iconst_0
istore 5
iconst_0
istore 4
L53:
iload 5
iload 9
if_icmpge L54
aload 17
iload 5
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast android/view/View
astore 18
aload 18
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
astore 20
aload 20
getfield android/support/v7/widget/ck/leftMargin I
iload 8
isub
istore 8
aload 20
getfield android/support/v7/widget/ck/rightMargin I
iload 7
isub
istore 7
iconst_0
iload 8
invokestatic java/lang/Math/max(II)I
istore 13
iconst_0
iload 7
invokestatic java/lang/Math/max(II)I
istore 14
iconst_0
iload 8
ineg
invokestatic java/lang/Math/max(II)I
istore 8
iconst_0
iload 7
ineg
invokestatic java/lang/Math/max(II)I
istore 7
aload 18
invokevirtual android/view/View/getMeasuredWidth()I
istore 15
iload 5
iconst_1
iadd
istore 5
iload 4
iload 15
iload 13
iadd
iload 14
iadd
iadd
istore 4
goto L53
L54:
iload 11
iload 6
isub
iload 12
isub
iconst_2
idiv
iload 6
iadd
iload 4
iconst_2
idiv
isub
istore 5
iload 5
iload 4
iadd
istore 4
iload 5
iload 2
if_icmpge L55
L56:
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/size()I
istore 5
iconst_0
istore 4
iload 2
istore 3
iload 4
istore 2
L57:
iload 2
iload 5
if_icmpge L58
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
iload 2
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast android/view/View
iload 3
aload 19
iload 10
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;I[II)I
istore 3
iload 2
iconst_1
iadd
istore 2
goto L57
L55:
iload 5
istore 2
iload 4
iload 3
if_icmple L56
iload 5
iload 4
iload 3
isub
isub
istore 2
goto L56
L58:
aload 0
getfield android/support/v7/widget/Toolbar/H Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
return
L49:
iload 4
istore 9
goto L50
L47:
iload 4
istore 8
goto L48
L40:
iload 3
istore 2
goto L41
L38:
iload 3
istore 2
goto L39
L36:
iload 3
istore 5
goto L37
L44:
goto L31
L15:
iload 2
istore 9
goto L16
L11:
iload 2
istore 4
goto L13
L2:
iload 6
istore 2
goto L4
.limit locals 21
.limit stack 6
.end method

.method protected onMeasure(II)V
aload 0
getfield android/support/v7/widget/Toolbar/I [I
astore 13
aload 0
invokestatic android/support/v7/internal/widget/bd/a(Landroid/view/View;)Z
ifeq L0
iconst_0
istore 7
iconst_1
istore 8
L1:
iconst_0
istore 3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L2
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
iload 1
iconst_0
iload 2
aload 0
getfield android/support/v7/widget/Toolbar/v I
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getMeasuredWidth()I
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokestatic android/support/v7/widget/Toolbar/b(Landroid/view/View;)I
iadd
istore 3
iconst_0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
invokestatic java/lang/Math/max(II)I
istore 6
iconst_0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
L3:
iload 3
istore 9
iload 5
istore 3
iload 6
istore 4
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L4
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
iload 1
iconst_0
iload 2
aload 0
getfield android/support/v7/widget/Toolbar/v I
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getMeasuredWidth()I
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokestatic android/support/v7/widget/Toolbar/b(Landroid/view/View;)I
iadd
istore 9
iload 6
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokevirtual android/widget/ImageButton/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
invokestatic java/lang/Math/max(II)I
istore 4
iload 5
aload 0
getfield android/support/v7/widget/Toolbar/t Landroid/widget/ImageButton;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 3
L4:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContentInsetStart()I
istore 5
iload 5
iload 9
invokestatic java/lang/Math/max(II)I
iconst_0
iadd
istore 10
aload 13
iload 8
iconst_0
iload 5
iload 9
isub
invokestatic java/lang/Math/max(II)I
iastore
iconst_0
istore 8
iload 3
istore 5
iload 4
istore 6
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L5
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
iload 1
iload 10
iload 2
aload 0
getfield android/support/v7/widget/Toolbar/v I
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII)V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/getMeasuredWidth()I
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokestatic android/support/v7/widget/Toolbar/b(Landroid/view/View;)I
iadd
istore 8
iload 4
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokevirtual android/support/v7/widget/ActionMenuView/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
invokestatic java/lang/Math/max(II)I
istore 6
iload 3
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
L5:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContentInsetEnd()I
istore 3
iload 10
iload 3
iload 8
invokestatic java/lang/Math/max(II)I
iadd
istore 9
aload 13
iload 7
iconst_0
iload 3
iload 8
isub
invokestatic java/lang/Math/max(II)I
iastore
iload 9
istore 7
iload 5
istore 3
iload 6
istore 4
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L6
iload 9
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
iload 1
iload 9
iload 2
iconst_0
aload 13
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII[I)I
iadd
istore 7
iload 6
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokevirtual android/view/View/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
invokestatic java/lang/Math/max(II)I
istore 4
iload 5
aload 0
getfield android/support/v7/widget/Toolbar/d Landroid/view/View;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 3
L6:
iload 7
istore 5
iload 3
istore 8
iload 4
istore 6
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L7
iload 7
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
iload 1
iload 7
iload 2
iconst_0
aload 13
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII[I)I
iadd
istore 5
iload 4
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokevirtual android/widget/ImageView/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
invokestatic java/lang/Math/max(II)I
istore 6
iload 3
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 8
L7:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getChildCount()I
istore 9
iconst_0
istore 7
iload 8
istore 3
iload 6
istore 4
iload 7
istore 6
iload 5
istore 8
L8:
iload 6
iload 9
if_icmpge L9
aload 0
iload 6
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 14
aload 14
invokevirtual android/view/View/getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
checkcast android/support/v7/widget/ck
getfield android/support/v7/widget/ck/e I
ifne L10
aload 0
aload 14
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L10
iload 8
aload 0
aload 14
iload 1
iload 8
iload 2
iconst_0
aload 13
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII[I)I
iadd
istore 8
iload 4
aload 14
invokevirtual android/view/View/getMeasuredHeight()I
aload 14
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
invokestatic java/lang/Math/max(II)I
istore 5
iload 3
aload 14
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 4
iload 5
istore 3
L11:
iload 6
iconst_1
iadd
istore 6
iload 3
istore 5
iload 4
istore 3
iload 5
istore 4
goto L8
L0:
iconst_1
istore 7
iconst_0
istore 8
goto L1
L9:
iconst_0
istore 7
iconst_0
istore 6
aload 0
getfield android/support/v7/widget/Toolbar/y I
aload 0
getfield android/support/v7/widget/Toolbar/z I
iadd
istore 11
aload 0
getfield android/support/v7/widget/Toolbar/w I
aload 0
getfield android/support/v7/widget/Toolbar/x I
iadd
istore 12
iload 3
istore 5
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L12
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
iload 1
iload 8
iload 12
iadd
iload 2
iload 11
aload 13
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII[I)I
pop
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredWidth()I
istore 5
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokestatic android/support/v7/widget/Toolbar/b(Landroid/view/View;)I
iload 5
iadd
istore 7
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
istore 6
iload 3
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 5
L12:
iload 6
istore 10
iload 7
istore 9
iload 5
istore 3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L13
iload 7
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
iload 1
iload 8
iload 12
iadd
iload 2
iload 11
iload 6
iadd
aload 13
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;IIII[I)I
invokestatic java/lang/Math/max(II)I
istore 9
iload 6
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/getMeasuredHeight()I
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokestatic android/support/v7/widget/Toolbar/c(Landroid/view/View;)I
iadd
iadd
istore 10
iload 5
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokestatic android/support/v4/view/cx/j(Landroid/view/View;)I
invokestatic android/support/v7/internal/widget/bd/a(II)I
istore 3
L13:
iload 4
iload 10
invokestatic java/lang/Math/max(II)I
istore 5
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingLeft()I
istore 4
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingRight()I
istore 10
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingTop()I
istore 6
aload 0
invokevirtual android/support/v7/widget/Toolbar/getPaddingBottom()I
istore 7
iload 9
iload 8
iadd
iload 4
iload 10
iadd
iadd
aload 0
invokevirtual android/support/v7/widget/Toolbar/getSuggestedMinimumWidth()I
invokestatic java/lang/Math/max(II)I
iload 1
ldc_w -16777216
iload 3
iand
invokestatic android/support/v4/view/cx/a(III)I
istore 4
iload 5
iload 6
iload 7
iadd
iadd
aload 0
invokevirtual android/support/v7/widget/Toolbar/getSuggestedMinimumHeight()I
invokestatic java/lang/Math/max(II)I
iload 2
iload 3
bipush 16
ishl
invokestatic android/support/v4/view/cx/a(III)I
istore 2
aload 0
getfield android/support/v7/widget/Toolbar/M Z
ifne L14
iconst_0
istore 1
L15:
iload 1
ifeq L16
iconst_0
istore 2
L16:
aload 0
iload 4
iload 2
invokevirtual android/support/v7/widget/Toolbar/setMeasuredDimension(II)V
return
L14:
aload 0
invokevirtual android/support/v7/widget/Toolbar/getChildCount()I
istore 3
iconst_0
istore 1
L17:
iload 1
iload 3
if_icmpge L18
aload 0
iload 1
invokevirtual android/support/v7/widget/Toolbar/getChildAt(I)Landroid/view/View;
astore 13
aload 0
aload 13
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;)Z
ifeq L19
aload 13
invokevirtual android/view/View/getMeasuredWidth()I
ifle L19
aload 13
invokevirtual android/view/View/getMeasuredHeight()I
ifle L19
iconst_0
istore 1
goto L15
L19:
iload 1
iconst_1
iadd
istore 1
goto L17
L18:
iconst_1
istore 1
goto L15
L10:
iload 4
istore 5
iload 3
istore 4
iload 5
istore 3
goto L11
L2:
iconst_0
istore 5
iconst_0
istore 6
goto L3
.limit locals 15
.limit stack 8
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 1
checkcast android/support/v7/widget/Toolbar$SavedState
astore 2
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar$SavedState/getSuperState()Landroid/os/Parcelable;
invokespecial android/view/ViewGroup/onRestoreInstanceState(Landroid/os/Parcelable;)V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
getfield android/support/v7/widget/ActionMenuView/c Landroid/support/v7/internal/view/menu/i;
astore 1
L1:
aload 2
getfield android/support/v7/widget/Toolbar$SavedState/a I
ifeq L2
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnull L2
aload 1
ifnull L2
aload 1
aload 2
getfield android/support/v7/widget/Toolbar$SavedState/a I
invokeinterface android/view/Menu/findItem(I)Landroid/view/MenuItem; 1
astore 1
aload 1
ifnull L2
aload 1
invokestatic android/support/v4/view/az/b(Landroid/view/MenuItem;)Z
pop
L2:
aload 2
getfield android/support/v7/widget/Toolbar$SavedState/b Z
ifeq L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/N Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/Toolbar/removeCallbacks(Ljava/lang/Runnable;)Z
pop
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/N Ljava/lang/Runnable;
invokevirtual android/support/v7/widget/Toolbar/post(Ljava/lang/Runnable;)Z
pop
L3:
return
L0:
aconst_null
astore 1
goto L1
.limit locals 3
.limit stack 2
.end method

.method public onRtlPropertiesChanged(I)V
iconst_1
istore 2
getstatic android/os/Build$VERSION/SDK_INT I
bipush 17
if_icmplt L0
aload 0
iload 1
invokespecial android/view/ViewGroup/onRtlPropertiesChanged(I)V
L0:
aload 0
getfield android/support/v7/widget/Toolbar/i Landroid/support/v7/internal/widget/ak;
astore 3
iload 1
iconst_1
if_icmpne L1
L2:
iload 2
aload 3
getfield android/support/v7/internal/widget/ak/h Z
if_icmpeq L3
aload 3
iload 2
putfield android/support/v7/internal/widget/ak/h Z
aload 3
getfield android/support/v7/internal/widget/ak/i Z
ifeq L4
iload 2
ifeq L5
aload 3
getfield android/support/v7/internal/widget/ak/e I
ldc_w -2147483648
if_icmpeq L6
aload 3
getfield android/support/v7/internal/widget/ak/e I
istore 1
L7:
aload 3
iload 1
putfield android/support/v7/internal/widget/ak/b I
aload 3
getfield android/support/v7/internal/widget/ak/d I
ldc_w -2147483648
if_icmpeq L8
aload 3
getfield android/support/v7/internal/widget/ak/d I
istore 1
L9:
aload 3
iload 1
putfield android/support/v7/internal/widget/ak/c I
L3:
return
L1:
iconst_0
istore 2
goto L2
L6:
aload 3
getfield android/support/v7/internal/widget/ak/f I
istore 1
goto L7
L8:
aload 3
getfield android/support/v7/internal/widget/ak/g I
istore 1
goto L9
L5:
aload 3
getfield android/support/v7/internal/widget/ak/d I
ldc_w -2147483648
if_icmpeq L10
aload 3
getfield android/support/v7/internal/widget/ak/d I
istore 1
L11:
aload 3
iload 1
putfield android/support/v7/internal/widget/ak/b I
aload 3
getfield android/support/v7/internal/widget/ak/e I
ldc_w -2147483648
if_icmpeq L12
aload 3
getfield android/support/v7/internal/widget/ak/e I
istore 1
L13:
aload 3
iload 1
putfield android/support/v7/internal/widget/ak/c I
return
L10:
aload 3
getfield android/support/v7/internal/widget/ak/f I
istore 1
goto L11
L12:
aload 3
getfield android/support/v7/internal/widget/ak/g I
istore 1
goto L13
L4:
aload 3
aload 3
getfield android/support/v7/internal/widget/ak/f I
putfield android/support/v7/internal/widget/ak/b I
aload 3
aload 3
getfield android/support/v7/internal/widget/ak/g I
putfield android/support/v7/internal/widget/ak/c I
return
.limit locals 4
.limit stack 2
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
new android/support/v7/widget/Toolbar$SavedState
dup
aload 0
invokespecial android/view/ViewGroup/onSaveInstanceState()Landroid/os/Parcelable;
invokespecial android/support/v7/widget/Toolbar$SavedState/<init>(Landroid/os/Parcelable;)V
astore 1
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
ifnull L0
aload 1
aload 0
getfield android/support/v7/widget/Toolbar/l Landroid/support/v7/widget/cj;
getfield android/support/v7/widget/cj/b Landroid/support/v7/internal/view/menu/m;
invokevirtual android/support/v7/internal/view/menu/m/getItemId()I
putfield android/support/v7/widget/Toolbar$SavedState/a I
L0:
aload 1
aload 0
invokevirtual android/support/v7/widget/Toolbar/a()Z
putfield android/support/v7/widget/Toolbar$SavedState/b Z
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokestatic android/support/v4/view/bk/a(Landroid/view/MotionEvent;)I
istore 2
iload 2
ifne L0
aload 0
iconst_0
putfield android/support/v7/widget/Toolbar/F Z
L0:
aload 0
getfield android/support/v7/widget/Toolbar/F Z
ifne L1
aload 0
aload 1
invokespecial android/view/ViewGroup/onTouchEvent(Landroid/view/MotionEvent;)Z
istore 3
iload 2
ifne L1
iload 3
ifne L1
aload 0
iconst_1
putfield android/support/v7/widget/Toolbar/F Z
L1:
iload 2
iconst_1
if_icmpeq L2
iload 2
iconst_3
if_icmpne L3
L2:
aload 0
iconst_0
putfield android/support/v7/widget/Toolbar/F Z
L3:
iconst_1
ireturn
.limit locals 4
.limit stack 2
.end method

.method public setCollapsible(Z)V
aload 0
iload 1
putfield android/support/v7/widget/Toolbar/M Z
aload 0
invokevirtual android/support/v7/widget/Toolbar/requestLayout()V
return
.limit locals 2
.limit stack 2
.end method

.method public setLogo(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/O Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/Toolbar/setLogo(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public setLogo(Landroid/graphics/drawable/Drawable;)V
aload 1
ifnull L0
aload 0
invokespecial android/support/v7/widget/Toolbar/k()V
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifne L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
iconst_1
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;Z)V
L1:
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
ifnull L2
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
aload 1
invokevirtual android/widget/ImageView/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L2:
return
L0:
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
ifnull L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifeq L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
.limit locals 2
.limit stack 3
.end method

.method public setLogoDescription(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setLogoDescription(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setLogoDescription(Ljava/lang/CharSequence;)V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
invokespecial android/support/v7/widget/Toolbar/k()V
L0:
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
ifnull L1
aload 0
getfield android/support/v7/widget/Toolbar/q Landroid/widget/ImageView;
aload 1
invokevirtual android/widget/ImageView/setContentDescription(Ljava/lang/CharSequence;)V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public setNavigationContentDescription(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
iload 1
ifeq L0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
astore 2
L1:
aload 0
aload 2
invokevirtual android/support/v7/widget/Toolbar/setNavigationContentDescription(Ljava/lang/CharSequence;)V
return
L0:
aconst_null
astore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method public setNavigationContentDescription(Ljava/lang/CharSequence;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
invokespecial android/support/v7/widget/Toolbar/n()V
L0:
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
ifnull L1
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
aload 1
invokevirtual android/widget/ImageButton/setContentDescription(Ljava/lang/CharSequence;)V
L1:
return
.limit locals 2
.limit stack 2
.end method

.method public setNavigationIcon(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/O Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/Toolbar/setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 4
.end method

.method public setNavigationIcon(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 1
ifnull L0
aload 0
invokespecial android/support/v7/widget/Toolbar/n()V
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifne L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
iconst_1
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;Z)V
L1:
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
ifnull L2
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
aload 1
invokevirtual android/widget/ImageButton/setImageDrawable(Landroid/graphics/drawable/Drawable;)V
L2:
return
L0:
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
ifnull L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifeq L1
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
goto L1
.limit locals 2
.limit stack 3
.end method

.method public setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
invokespecial android/support/v7/widget/Toolbar/n()V
aload 0
getfield android/support/v7/widget/Toolbar/p Landroid/widget/ImageButton;
aload 1
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setOnMenuItemClickListener(Landroid/support/v7/widget/cl;)V
aload 0
aload 1
putfield android/support/v7/widget/Toolbar/J Landroid/support/v7/widget/cl;
return
.limit locals 2
.limit stack 2
.end method

.method public setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
.annotation invisibleparam 1 Landroid/support/a/z;
.end annotation
aload 0
invokespecial android/support/v7/widget/Toolbar/m()V
aload 0
getfield android/support/v7/widget/Toolbar/a Landroid/support/v7/widget/ActionMenuView;
aload 1
invokevirtual android/support/v7/widget/ActionMenuView/setOverflowIcon(Landroid/graphics/drawable/Drawable;)V
return
.limit locals 2
.limit stack 2
.end method

.method public setPopupTheme(I)V
.annotation invisibleparam 1 Landroid/support/a/ai;
.end annotation
aload 0
getfield android/support/v7/widget/Toolbar/f I
iload 1
if_icmpeq L0
aload 0
iload 1
putfield android/support/v7/widget/Toolbar/f I
iload 1
ifne L1
aload 0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
putfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
L0:
return
L1:
aload 0
new android/view/ContextThemeWrapper
dup
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
iload 1
invokespecial android/view/ContextThemeWrapper/<init>(Landroid/content/Context;I)V
putfield android/support/v7/widget/Toolbar/e Landroid/content/Context;
return
.limit locals 2
.limit stack 5
.end method

.method public setSubtitle(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setSubtitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
ifnonnull L1
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
astore 2
aload 0
new android/widget/TextView
dup
aload 2
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/widget/TextView/setSingleLine()V
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 0
getfield android/support/v7/widget/Toolbar/h I
ifeq L2
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
aload 2
aload 0
getfield android/support/v7/widget/Toolbar/h I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L2:
aload 0
getfield android/support/v7/widget/Toolbar/E I
ifeq L1
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
aload 0
getfield android/support/v7/widget/Toolbar/E I
invokevirtual android/widget/TextView/setTextColor(I)V
L1:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifne L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
iconst_1
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;Z)V
L3:
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
ifnull L4
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L4:
aload 0
aload 1
putfield android/support/v7/widget/Toolbar/C Ljava/lang/CharSequence;
return
L0:
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
ifnull L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifeq L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
goto L3
.limit locals 3
.limit stack 4
.end method

.method public setSubtitleTextColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v7/widget/Toolbar/E I
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/c Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method public setTitle(I)V
.annotation invisibleparam 1 Landroid/support/a/ah;
.end annotation
aload 0
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
iload 1
invokevirtual android/content/Context/getText(I)Ljava/lang/CharSequence;
invokevirtual android/support/v7/widget/Toolbar/setTitle(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 3
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
aload 1
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifne L0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnonnull L1
aload 0
invokevirtual android/support/v7/widget/Toolbar/getContext()Landroid/content/Context;
astore 2
aload 0
new android/widget/TextView
dup
aload 2
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
putfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/widget/TextView/setSingleLine()V
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 0
getfield android/support/v7/widget/Toolbar/g I
ifeq L2
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
aload 2
aload 0
getfield android/support/v7/widget/Toolbar/g I
invokevirtual android/widget/TextView/setTextAppearance(Landroid/content/Context;I)V
L2:
aload 0
getfield android/support/v7/widget/Toolbar/D I
ifeq L1
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
aload 0
getfield android/support/v7/widget/Toolbar/D I
invokevirtual android/widget/TextView/setTextColor(I)V
L1:
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifne L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
iconst_1
invokespecial android/support/v7/widget/Toolbar/a(Landroid/view/View;Z)V
L3:
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnull L4
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
aload 1
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L4:
aload 0
aload 1
putfield android/support/v7/widget/Toolbar/B Ljava/lang/CharSequence;
return
L0:
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnull L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokespecial android/support/v7/widget/Toolbar/e(Landroid/view/View;)Z
ifeq L3
aload 0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual android/support/v7/widget/Toolbar/removeView(Landroid/view/View;)V
aload 0
getfield android/support/v7/widget/Toolbar/j Ljava/util/ArrayList;
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
invokevirtual java/util/ArrayList/remove(Ljava/lang/Object;)Z
pop
goto L3
.limit locals 3
.limit stack 4
.end method

.method public setTitleTextColor(I)V
.annotation invisibleparam 1 Landroid/support/a/j;
.end annotation
aload 0
iload 1
putfield android/support/v7/widget/Toolbar/D I
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
ifnull L0
aload 0
getfield android/support/v7/widget/Toolbar/b Landroid/widget/TextView;
iload 1
invokevirtual android/widget/TextView/setTextColor(I)V
L0:
return
.limit locals 2
.limit stack 2
.end method
