.bytecode 50.0
.class public final synchronized android/support/v7/widget/t
.super android/widget/CheckedTextView

.field private static final 'a' [I

.field private 'b' Landroid/support/v7/internal/widget/av;

.method static <clinit>()V
iconst_1
newarray int
dup
iconst_0
ldc_w 16843016
iastore
putstatic android/support/v7/widget/t/a [I
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;)V
aload 0
aload 1
aconst_null
invokespecial android/support/v7/widget/t/<init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
return
.limit locals 2
.limit stack 3
.end method

.method public <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
aload 0
aload 1
aload 2
iconst_0
invokespecial android/support/v7/widget/t/<init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
return
.limit locals 3
.limit stack 4
.end method

.method private <init>(Landroid/content/Context;Landroid/util/AttributeSet;B)V
aload 0
aload 1
aload 2
ldc_w 16843720
invokespecial android/widget/CheckedTextView/<init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
getstatic android/support/v7/internal/widget/av/a Z
ifeq L0
aload 0
invokevirtual android/support/v7/widget/t/getContext()Landroid/content/Context;
aload 2
getstatic android/support/v7/widget/t/a [I
ldc_w 16843720
invokestatic android/support/v7/internal/widget/ax/a(Landroid/content/Context;Landroid/util/AttributeSet;[II)Landroid/support/v7/internal/widget/ax;
astore 1
aload 0
aload 1
iconst_0
invokevirtual android/support/v7/internal/widget/ax/a(I)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/t/setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
getfield android/support/v7/internal/widget/ax/a Landroid/content/res/TypedArray;
invokevirtual android/content/res/TypedArray/recycle()V
aload 0
aload 1
invokevirtual android/support/v7/internal/widget/ax/a()Landroid/support/v7/internal/widget/av;
putfield android/support/v7/widget/t/b Landroid/support/v7/internal/widget/av;
L0:
return
.limit locals 4
.limit stack 4
.end method

.method public final setCheckMarkDrawable(I)V
.annotation invisibleparam 1 Landroid/support/a/m;
.end annotation
aload 0
getfield android/support/v7/widget/t/b Landroid/support/v7/internal/widget/av;
ifnull L0
aload 0
aload 0
getfield android/support/v7/widget/t/b Landroid/support/v7/internal/widget/av;
iload 1
iconst_0
invokevirtual android/support/v7/internal/widget/av/a(IZ)Landroid/graphics/drawable/Drawable;
invokevirtual android/support/v7/widget/t/setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
return
L0:
aload 0
iload 1
invokespecial android/widget/CheckedTextView/setCheckMarkDrawable(I)V
return
.limit locals 2
.limit stack 4
.end method
