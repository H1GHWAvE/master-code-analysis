.bytecode 50.0
.class public final synchronized javax/annotation/RegEx$Checker
.super java/lang/Object
.implements javax/annotation/meta/TypeQualifierValidator

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static forConstantValue$78dac8c8(Ljava/lang/Object;)Ljavax/annotation/meta/When;
.catch java/util/regex/PatternSyntaxException from L0 to L1 using L2
aload 0
instanceof java/lang/String
ifne L0
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
L0:
aload 0
checkcast java/lang/String
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
pop
L1:
getstatic javax/annotation/meta/When/ALWAYS Ljavax/annotation/meta/When;
areturn
L2:
astore 0
getstatic javax/annotation/meta/When/NEVER Ljavax/annotation/meta/When;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic forConstantValue(Ljava/lang/annotation/Annotation;Ljava/lang/Object;)Ljavax/annotation/meta/When;
aload 2
invokestatic javax/annotation/RegEx$Checker/forConstantValue$78dac8c8(Ljava/lang/Object;)Ljavax/annotation/meta/When;
areturn
.limit locals 3
.limit stack 1
.end method
