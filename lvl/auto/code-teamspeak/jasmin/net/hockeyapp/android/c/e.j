.bytecode 50.0
.class public synchronized net/hockeyapp/android/c/e
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'g' J = 5059651319640956830L


.field public 'a' I

.field public 'b' I

.field public 'c' Ljava/lang/String;

.field public 'd' Ljava/lang/String;

.field public 'e' Ljava/lang/String;

.field public 'f' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield net/hockeyapp/android/c/e/a I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/e/c Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private b()I
aload 0
getfield net/hockeyapp/android/c/e/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
iload 1
putfield net/hockeyapp/android/c/e/b I
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/e/d Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private c()I
aload 0
getfield net/hockeyapp/android/c/e/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/e/e Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/e/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/c/e/f Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private e()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/e/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/e/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/c/e/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()Z
invokestatic net/hockeyapp/android/a/a()Ljava/io/File;
astore 1
aload 1
invokevirtual java/io/File/exists()Z
ifeq L0
aload 1
invokevirtual java/io/File/isDirectory()Z
ifeq L0
aload 1
new net/hockeyapp/android/c/f
dup
aload 0
invokespecial net/hockeyapp/android/c/f/<init>(Lnet/hockeyapp/android/c/e;)V
invokevirtual java/io/File/listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;
astore 1
aload 1
ifnull L1
aload 1
arraylength
iconst_1
if_icmpne L1
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public final a()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/c/e/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "\n"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
ldc net/hockeyapp/android/c/e
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\nid         "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "\nmessage id "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "\nfilename   "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\nurl        "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\ncreatedAt  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\nupdatedAt  "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield net/hockeyapp/android/c/e/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
