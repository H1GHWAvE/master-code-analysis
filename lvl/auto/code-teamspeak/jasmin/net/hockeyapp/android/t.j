.bytecode 50.0
.class public final synchronized net/hockeyapp/android/t
.super java/lang/Object

.field private static final 'a' I = 1


.field private static final 'b' I = 1


.field private static final 'c' Ljava/lang/String; = "net.hockeyapp.android.SCREENSHOT"

.field private static 'd' Landroid/content/BroadcastReceiver;

.field private static 'e' Landroid/app/Activity;

.field private static 'f' Z

.field private static 'g' Ljava/lang/String;

.field private static 'h' Ljava/lang/String;

.field private static 'i' Lnet/hockeyapp/android/c/i;

.field private static 'j' Lnet/hockeyapp/android/c/i;

.field private static 'k' Lnet/hockeyapp/android/y;

.method static <clinit>()V
aconst_null
putstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
iconst_0
putstatic net/hockeyapp/android/t/f Z
aconst_null
putstatic net/hockeyapp/android/t/g Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/t/h Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Lnet/hockeyapp/android/y;
getstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Landroid/app/Activity;)V
aload 0
putstatic net/hockeyapp/android/t/e Landroid/app/Activity;
getstatic net/hockeyapp/android/t/f Z
ifne L0
iconst_1
putstatic net/hockeyapp/android/t/f Z
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
ldc "notification"
invokevirtual android/app/Activity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
astore 0
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
invokevirtual android/app/Activity/getResources()Landroid/content/res/Resources;
ldc "ic_menu_camera"
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 1
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 2
aload 2
ldc "net.hockeyapp.android.SCREENSHOT"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
iconst_1
aload 2
ldc_w 1073741824
invokestatic android/app/PendingIntent/getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 2
aload 0
iconst_1
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
aload 2
ldc "HockeyApp Feedback"
ldc "Take a screenshot for your feedback."
iload 1
invokestatic net/hockeyapp/android/e/w/a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
getstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
ifnonnull L1
new net/hockeyapp/android/w
dup
invokespecial net/hockeyapp/android/w/<init>()V
putstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
L1:
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
getstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
new android/content/IntentFilter
dup
ldc "net.hockeyapp.android.SCREENSHOT"
invokespecial android/content/IntentFilter/<init>(Ljava/lang/String;)V
invokevirtual android/app/Activity/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
L0:
return
.limit locals 3
.limit stack 7
.end method

.method public static a(Landroid/content/Context;)V
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
invokevirtual android/app/Activity/getWindow()Landroid/view/Window;
invokevirtual android/view/Window/getDecorView()Landroid/view/View;
astore 2
aload 2
iconst_1
invokevirtual android/view/View/setDrawingCacheEnabled(Z)V
aload 2
invokevirtual android/view/View/getDrawingCache()Landroid/graphics/Bitmap;
astore 3
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
invokevirtual android/app/Activity/getLocalClassName()Ljava/lang/String;
astore 4
invokestatic net/hockeyapp/android/a/a()Ljava/io/File;
astore 5
new java/io/File
dup
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".jpg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 2
iconst_1
istore 1
L0:
aload 2
invokevirtual java/io/File/exists()Z
ifeq L1
new java/io/File
dup
aload 5
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ".jpg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new net/hockeyapp/android/v
dup
aload 3
aload 0
invokespecial net/hockeyapp/android/v/<init>(Landroid/graphics/Bitmap;Landroid/content/Context;)V
iconst_1
anewarray java/io/File
dup
iconst_0
aload 2
aastore
invokevirtual net/hockeyapp/android/v/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
new net/hockeyapp/android/x
dup
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
iconst_0
invokespecial net/hockeyapp/android/x/<init>(Ljava/lang/String;B)V
astore 3
new android/media/MediaScannerConnection
dup
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
aload 3
invokespecial android/media/MediaScannerConnection/<init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V
astore 4
aload 3
aload 4
putfield net/hockeyapp/android/x/a Landroid/media/MediaScannerConnection;
aload 4
invokevirtual android/media/MediaScannerConnection/connect()V
aload 0
new java/lang/StringBuilder
dup
ldc "Screenshot '"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/io/File/getName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "' is available in gallery."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
sipush 2000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
return
.limit locals 6
.limit stack 5
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
aload 0
ifnull L0
aload 1
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/t/g Ljava/lang/String;
ldc "https://sdk.hockeyapp.net/"
putstatic net/hockeyapp/android/t/h Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/y;)V
aload 0
ifnull L0
aload 2
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/t/g Ljava/lang/String;
aload 1
putstatic net/hockeyapp/android/t/h Ljava/lang/String;
aload 3
putstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 4
.limit stack 1
.end method

.method private static transient a(Landroid/content/Context;[Landroid/net/Uri;)V
aload 0
ifnull L0
aconst_null
astore 2
getstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
ifnull L1
ldc net/hockeyapp/android/FeedbackActivity
astore 2
L1:
aload 2
astore 3
aload 2
ifnonnull L2
ldc net/hockeyapp/android/FeedbackActivity
astore 3
L2:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 2
aload 2
ldc_w 268435456
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 2
aload 0
aload 3
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 2
ldc "url"
invokestatic net/hockeyapp/android/t/f()Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
ldc "initialAttachments"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 0
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method private static a(Lnet/hockeyapp/android/c/i;)V
aload 0
putstatic net/hockeyapp/android/t/i Lnet/hockeyapp/android/c/i;
return
.limit locals 1
.limit stack 1
.end method

.method public static b()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/t/i Lnet/hockeyapp/android/c/i;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(Landroid/app/Activity;)V
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
ifnull L0
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
aload 0
if_acmpeq L1
L0:
return
L1:
iconst_0
putstatic net/hockeyapp/android/t/f Z
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
getstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
invokevirtual android/app/Activity/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
ldc "notification"
invokevirtual android/app/Activity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
iconst_1
invokevirtual android/app/NotificationManager/cancel(I)V
aconst_null
putstatic net/hockeyapp/android/t/e Landroid/app/Activity;
return
.limit locals 1
.limit stack 2
.end method

.method private static b(Landroid/content/Context;)V
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
aload 0
invokevirtual net/hockeyapp/android/e/n/a(Landroid/content/Context;)Ljava/lang/String;
astore 2
aload 2
ifnonnull L0
return
L0:
aload 0
ldc "net.hockeyapp.android.feedback"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
ldc "idLastMessageSend"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 1
new net/hockeyapp/android/d/r
dup
aload 0
invokestatic net/hockeyapp/android/t/f()Ljava/lang/String;
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
aload 2
new net/hockeyapp/android/u
dup
aload 0
invokespecial net/hockeyapp/android/u/<init>(Landroid/content/Context;)V
iconst_1
invokespecial net/hockeyapp/android/d/r/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
astore 0
aload 0
iconst_0
putfield net/hockeyapp/android/d/r/a Z
aload 0
iload 1
putfield net/hockeyapp/android/d/r/b I
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 3
.limit stack 13
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)V
aload 0
ifnull L0
aload 1
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/t/g Ljava/lang/String;
ldc "https://sdk.hockeyapp.net/"
putstatic net/hockeyapp/android/t/h Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 2
.limit stack 1
.end method

.method private static transient b(Landroid/content/Context;[Landroid/net/Uri;)V
aload 0
ifnull L0
aconst_null
astore 2
getstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
ifnull L1
ldc net/hockeyapp/android/FeedbackActivity
astore 2
L1:
aload 2
astore 3
aload 2
ifnonnull L2
ldc net/hockeyapp/android/FeedbackActivity
astore 3
L2:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 2
aload 2
ldc_w 268435456
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 2
aload 0
aload 3
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 2
ldc "url"
invokestatic net/hockeyapp/android/t/f()Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
ldc "initialAttachments"
aload 1
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 0
aload 2
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method private static b(Lnet/hockeyapp/android/c/i;)V
aload 0
putstatic net/hockeyapp/android/t/j Lnet/hockeyapp/android/c/i;
return
.limit locals 1
.limit stack 1
.end method

.method public static c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/t/j Lnet/hockeyapp/android/c/i;
areturn
.limit locals 0
.limit stack 1
.end method

.method static synthetic d()Ljava/lang/String;
invokestatic net/hockeyapp/android/t/f()Ljava/lang/String;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static e()V
aconst_null
putstatic net/hockeyapp/android/t/k Lnet/hockeyapp/android/y;
return
.limit locals 0
.limit stack 1
.end method

.method private static f()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/t/h Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "api/2/apps/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/hockeyapp/android/t/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/feedback/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static g()V
iconst_1
putstatic net/hockeyapp/android/t/f Z
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
ldc "notification"
invokevirtual android/app/Activity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
astore 1
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
invokevirtual android/app/Activity/getResources()Landroid/content/res/Resources;
ldc "ic_menu_camera"
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 0
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 2
aload 2
ldc "net.hockeyapp.android.SCREENSHOT"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
iconst_1
aload 2
ldc_w 1073741824
invokestatic android/app/PendingIntent/getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
astore 2
aload 1
iconst_1
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
aload 2
ldc "HockeyApp Feedback"
ldc "Take a screenshot for your feedback."
iload 0
invokestatic net/hockeyapp/android/e/w/a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
getstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
ifnonnull L0
new net/hockeyapp/android/w
dup
invokespecial net/hockeyapp/android/w/<init>()V
putstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
L0:
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
getstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
new android/content/IntentFilter
dup
ldc "net.hockeyapp.android.SCREENSHOT"
invokespecial android/content/IntentFilter/<init>(Ljava/lang/String;)V
invokevirtual android/app/Activity/registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
pop
return
.limit locals 3
.limit stack 7
.end method

.method private static h()V
iconst_0
putstatic net/hockeyapp/android/t/f Z
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
getstatic net/hockeyapp/android/t/d Landroid/content/BroadcastReceiver;
invokevirtual android/app/Activity/unregisterReceiver(Landroid/content/BroadcastReceiver;)V
getstatic net/hockeyapp/android/t/e Landroid/app/Activity;
ldc "notification"
invokevirtual android/app/Activity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
iconst_1
invokevirtual android/app/NotificationManager/cancel(I)V
return
.limit locals 0
.limit stack 2
.end method
