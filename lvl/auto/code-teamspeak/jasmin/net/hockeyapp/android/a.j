.bytecode 50.0
.class public final synchronized net/hockeyapp/android/a
.super java/lang/Object

.field public static 'a' Ljava/lang/String;

.field public static 'b' Ljava/lang/String;

.field public static 'c' Ljava/lang/String;

.field public static 'd' Ljava/lang/String;

.field public static 'e' Ljava/lang/String;

.field public static 'f' Ljava/lang/String;

.field public static 'g' Ljava/lang/String;

.field public static 'h' Ljava/lang/String;

.field public static final 'i' Ljava/lang/String; = "HockeyApp"

.field public static final 'j' Ljava/lang/String; = "https://sdk.hockeyapp.net/"

.field public static final 'k' Ljava/lang/String; = "HockeySDK"

.field public static final 'l' Ljava/lang/String; = "3.6.0"

.field public static final 'm' I = 1


.method static <clinit>()V
aconst_null
putstatic net/hockeyapp/android/a/a Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/b Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/c Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/d Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/e Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/f Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/g Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/a/h Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;Landroid/content/pm/PackageManager;)I
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
iconst_0
istore 2
L0:
aload 1
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
sipush 128
invokevirtual android/content/pm/PackageManager/getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
getfield android/content/pm/ApplicationInfo/metaData Landroid/os/Bundle;
astore 0
L1:
aload 0
ifnull L4
L3:
aload 0
ldc "buildNumber"
iconst_0
invokevirtual android/os/Bundle/getInt(Ljava/lang/String;I)I
istore 2
L4:
iload 2
ireturn
L2:
astore 0
ldc "HockeyApp"
ldc "Exception thrown when accessing the application info:"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public static a()Ljava/io/File;
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
astore 0
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/HockeyApp"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/mkdirs()Z
pop
aload 0
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a([B)Ljava/lang/String;
ldc "0123456789ABCDEF"
invokevirtual java/lang/String/toCharArray()[C
astore 3
aload 0
arraylength
iconst_2
imul
newarray char
astore 4
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
baload
sipush 255
iand
istore 2
aload 4
iload 1
iconst_2
imul
aload 3
iload 2
iconst_4
iushr
caload
castore
aload 4
iload 1
iconst_2
imul
iconst_1
iadd
aload 3
iload 2
bipush 15
iand
caload
castore
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
new java/lang/String
dup
aload 4
invokespecial java/lang/String/<init>([C)V
ldc "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})"
ldc "$1-$2-$3-$4-$5"
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
areturn
.limit locals 5
.limit stack 5
.end method

.method public static a(Landroid/content/Context;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L7
.catch java/lang/Exception from L8 to L9 using L7
.catch java/lang/Throwable from L10 to L11 using L12
getstatic android/os/Build$VERSION/RELEASE Ljava/lang/String;
putstatic net/hockeyapp/android/a/e Ljava/lang/String;
getstatic android/os/Build/MODEL Ljava/lang/String;
putstatic net/hockeyapp/android/a/f Ljava/lang/String;
getstatic android/os/Build/MANUFACTURER Ljava/lang/String;
putstatic net/hockeyapp/android/a/g Ljava/lang/String;
aload 0
ifnull L4
L0:
aload 0
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
astore 2
L1:
aload 2
ifnull L4
L3:
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
putstatic net/hockeyapp/android/a/a Ljava/lang/String;
L4:
aload 0
ifnull L9
L5:
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 2
aload 2
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 3
aload 3
getfield android/content/pm/PackageInfo/packageName Ljava/lang/String;
putstatic net/hockeyapp/android/a/d Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
getfield android/content/pm/PackageInfo/versionCode I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putstatic net/hockeyapp/android/a/b Ljava/lang/String;
aload 3
getfield android/content/pm/PackageInfo/versionName Ljava/lang/String;
putstatic net/hockeyapp/android/a/c Ljava/lang/String;
aload 0
aload 2
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;Landroid/content/pm/PackageManager;)I
istore 1
L6:
iload 1
ifeq L9
L8:
iload 1
aload 3
getfield android/content/pm/PackageInfo/versionCode I
if_icmple L9
iload 1
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
putstatic net/hockeyapp/android/a/b Ljava/lang/String;
L9:
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
astore 0
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
ifnull L11
aload 0
ifnull L11
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic net/hockeyapp/android/a/b()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
L10:
ldc "SHA-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 0
aload 2
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 2
aload 0
aload 2
iconst_0
aload 2
arraylength
invokevirtual java/security/MessageDigest/update([BII)V
aload 0
invokevirtual java/security/MessageDigest/digest()[B
invokestatic net/hockeyapp/android/a/a([B)Ljava/lang/String;
putstatic net/hockeyapp/android/a/h Ljava/lang/String;
L11:
return
L2:
astore 2
ldc "HockeyApp"
ldc "Exception thrown when accessing the files dir:"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L4
L7:
astore 2
ldc "HockeyApp"
ldc "Exception thrown when accessing the package info:"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 2
invokevirtual java/lang/Exception/printStackTrace()V
goto L9
L12:
astore 0
return
.limit locals 4
.limit stack 4
.end method

.method private static b()Ljava/lang/String;
.catch java/lang/Throwable from L0 to L1 using L2
new java/lang/StringBuilder
dup
ldc "HA"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
getstatic android/os/Build/BOARD Ljava/lang/String;
invokevirtual java/lang/String/length()I
bipush 10
irem
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
getstatic android/os/Build/BRAND Ljava/lang/String;
invokevirtual java/lang/String/length()I
bipush 10
irem
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
getstatic android/os/Build/CPU_ABI Ljava/lang/String;
invokevirtual java/lang/String/length()I
bipush 10
irem
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
getstatic android/os/Build/PRODUCT Ljava/lang/String;
invokevirtual java/lang/String/length()I
bipush 10
irem
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 2
ldc ""
astore 1
aload 1
astore 0
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmplt L1
L0:
ldc android/os/Build
ldc "SERIAL"
invokevirtual java/lang/Class/getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
aconst_null
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
astore 0
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L2:
astore 0
aload 1
astore 0
goto L1
.limit locals 3
.limit stack 3
.end method

.method private static b(Landroid/content/Context;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
aload 0
ifnull L4
L0:
aload 0
invokevirtual android/content/Context/getFilesDir()Ljava/io/File;
astore 0
L1:
aload 0
ifnull L4
L3:
aload 0
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
putstatic net/hockeyapp/android/a/a Ljava/lang/String;
L4:
return
L2:
astore 0
ldc "HockeyApp"
ldc "Exception thrown when accessing the files dir:"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 1
.limit stack 2
.end method

.method private static c(Landroid/content/Context;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
aload 0
ifnull L4
L0:
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
astore 2
aload 2
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
astore 3
aload 3
getfield android/content/pm/PackageInfo/packageName Ljava/lang/String;
putstatic net/hockeyapp/android/a/d Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
getfield android/content/pm/PackageInfo/versionCode I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putstatic net/hockeyapp/android/a/b Ljava/lang/String;
aload 3
getfield android/content/pm/PackageInfo/versionName Ljava/lang/String;
putstatic net/hockeyapp/android/a/c Ljava/lang/String;
aload 0
aload 2
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;Landroid/content/pm/PackageManager;)I
istore 1
L1:
iload 1
ifeq L4
L3:
iload 1
aload 3
getfield android/content/pm/PackageInfo/versionCode I
if_icmple L4
iload 1
invokestatic java/lang/String/valueOf(I)Ljava/lang/String;
putstatic net/hockeyapp/android/a/b Ljava/lang/String;
L4:
return
L2:
astore 0
ldc "HockeyApp"
ldc "Exception thrown when accessing the package info:"
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
aload 0
invokevirtual java/lang/Exception/printStackTrace()V
return
.limit locals 4
.limit stack 3
.end method

.method private static d(Landroid/content/Context;)V
.catch java/lang/Throwable from L0 to L1 using L2
aload 0
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
ldc "android_id"
invokestatic android/provider/Settings$Secure/getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
astore 0
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
ifnull L1
aload 0
ifnull L1
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/a/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ":"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic net/hockeyapp/android/a/b()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 1
L0:
ldc "SHA-1"
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 0
aload 1
ldc "UTF-8"
invokevirtual java/lang/String/getBytes(Ljava/lang/String;)[B
astore 1
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual java/security/MessageDigest/update([BII)V
aload 0
invokevirtual java/security/MessageDigest/digest()[B
invokestatic net/hockeyapp/android/a/a([B)Ljava/lang/String;
putstatic net/hockeyapp/android/a/h Ljava/lang/String;
L1:
return
L2:
astore 0
return
.limit locals 2
.limit stack 4
.end method
