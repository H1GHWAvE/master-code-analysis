.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/i
.super android/widget/LinearLayout

.field public static final 'a' I = 8192


.field public static final 'b' I = 8194


.field public static final 'c' I = 8196


.field public static final 'd' I = 8198


.field public static final 'e' I = 8200


.field public static final 'f' I = 8201


.field public static final 'g' I = 8208


.field public static final 'h' I = 8209


.field public static final 'i' I = 131088


.field public static final 'j' I = 131089


.field public static final 'k' I = 131090


.field public static final 'l' I = 131091


.field public static final 'm' I = 131092


.field public static final 'n' I = 131093


.field public static final 'o' I = 131094


.field public static final 'p' I = 131095


.field private 'q' Landroid/widget/LinearLayout;

.field private 'r' Landroid/widget/ScrollView;

.field private 's' Landroid/widget/LinearLayout;

.field private 't' Landroid/widget/LinearLayout;

.field private 'u' Landroid/widget/LinearLayout;

.field private 'v' Landroid/view/ViewGroup;

.field private 'w' Landroid/widget/ListView;

.method public <init>(Landroid/content/Context;)V
aload 0
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/i/setBackgroundColor(I)V
aload 0
aload 5
invokevirtual net/hockeyapp/android/f/i/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
ldc_w 131090
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
aload 5
bipush 49
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
invokevirtual net/hockeyapp/android/f/i/addView(Landroid/view/View;)V
aload 0
new android/widget/ScrollView
dup
aload 1
invokespecial android/widget/ScrollView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
ldc_w 131095
invokevirtual android/widget/ScrollView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
bipush 17
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
aload 5
invokevirtual android/widget/ScrollView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
iload 2
iconst_0
iload 2
iconst_0
invokevirtual android/widget/ScrollView/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
ldc_w 131091
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
iconst_3
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
iload 2
iload 2
iload 2
iload 2
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
bipush 48
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
ldc_w 131093
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
bipush 17
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
iload 2
iload 2
iload 2
iconst_0
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
bipush 48
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8194
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 6
iconst_0
iload 2
iconst_2
idiv
iconst_0
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
aload 6
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 5
sipush 16385
invokevirtual android/widget/EditText/setInputType(I)V
aload 5
iconst_1
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 5
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 5
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 5
sipush 1026
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 5
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 5
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
invokestatic net/hockeyapp/android/t/b()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/a Lnet/hockeyapp/android/c/i;
if_acmpne L0
bipush 8
istore 2
L1:
aload 5
iload 2
invokevirtual android/widget/EditText/setVisibility(I)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8196
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
aload 6
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
aload 6
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 5
bipush 33
invokevirtual android/widget/EditText/setInputType(I)V
aload 5
iconst_1
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 5
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 5
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 5
sipush 1027
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 5
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 5
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
invokestatic net/hockeyapp/android/t/c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/a Lnet/hockeyapp/android/c/i;
if_acmpne L2
bipush 8
istore 2
L3:
aload 5
iload 2
invokevirtual android/widget/EditText/setVisibility(I)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8198
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
aload 6
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
aload 6
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 5
sipush 16433
invokevirtual android/widget/EditText/setInputType(I)V
aload 5
iconst_1
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 5
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 5
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 5
sipush 1028
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 5
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 5
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8200
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 100.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
aload 6
iconst_0
iconst_0
iconst_0
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
aload 6
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 5
sipush 16385
invokevirtual android/widget/EditText/setInputType(I)V
aload 5
iconst_0
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 5
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 5
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 5
iload 3
invokevirtual android/widget/EditText/setMinimumHeight(I)V
aload 5
sipush 1029
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 5
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 5
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
new net/hockeyapp/android/f/a
dup
aload 1
invokespecial net/hockeyapp/android/f/a/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
sipush 8209
invokevirtual android/view/ViewGroup/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
iconst_3
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
aload 5
invokevirtual android/view/ViewGroup/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
iconst_0
iconst_0
iconst_0
iload 2
invokevirtual android/view/ViewGroup/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8208
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
aload 6
iconst_0
iconst_0
iconst_0
iload 4
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 6
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 5
aload 6
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 5
iload 3
iload 2
iload 3
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 5
sipush 1031
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 5
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8201
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
aload 6
iconst_0
iconst_0
iconst_0
iload 4
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 6
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 5
aload 6
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 5
iload 3
iload 2
iload 3
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 5
sipush 1032
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 5
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 5
aload 5
sipush 8192
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 6
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
aload 6
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/TextView/setPadding(IIII)V
aload 5
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 5
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 5
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 5
sipush 1030
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 5
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 5
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
ldc_w 131092
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 5
iconst_3
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
bipush 48
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 5
aload 5
ldc_w 131088
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 5.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
aload 6
iconst_0
iconst_0
iload 4
iload 3
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 6
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 6
fconst_1
putfield android/widget/LinearLayout$LayoutParams/weight F
aload 5
aload 6
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 5
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 5
bipush 17
invokevirtual android/widget/Button/setGravity(I)V
aload 5
sipush 1033
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 5
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 5
aload 5
ldc_w 131089
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 5.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 6
aload 6
iload 4
iconst_0
iconst_0
iload 3
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 6
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 5
aload 6
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 5
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 5
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 5
bipush 17
invokevirtual android/widget/Button/setGravity(I)V
aload 5
sipush 1034
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 5
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 5
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 6
fconst_1
putfield android/widget/LinearLayout$LayoutParams/weight F
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 5
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
new android/widget/ListView
dup
aload 1
invokespecial android/widget/ListView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
ldc_w 131094
invokevirtual android/widget/ListView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
aload 1
invokevirtual android/widget/ListView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/ListView/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 7
.limit stack 7
.end method

.method private a()V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
aload 0
iconst_m1
invokevirtual net/hockeyapp/android/f/i/setBackgroundColor(I)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/f/i/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a(Landroid/content/Context;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
ldc_w 131090
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
aload 1
bipush 49
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
invokevirtual net/hockeyapp/android/f/i/addView(Landroid/view/View;)V
return
.limit locals 2
.limit stack 5
.end method

.method private static a(Landroid/content/Context;Landroid/widget/EditText;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmpge L0
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 10.0F
fmul
f2i
istore 2
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 3
aload 3
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 4
aload 4
iconst_m1
invokevirtual android/graphics/Paint/setColor(I)V
aload 4
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 4
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 3
iload 2
iload 2
iload 2
iload 2
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
f2d
ldc2_w 1.5D
dmul
d2i
istore 2
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 0
aload 0
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 4
aload 4
ldc_w -12303292
invokevirtual android/graphics/Paint/setColor(I)V
aload 4
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 4
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
iconst_0
iconst_0
iconst_0
iload 2
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
aload 1
new android/graphics/drawable/LayerDrawable
dup
iconst_2
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 3
aastore
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
invokevirtual android/widget/EditText/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
L0:
return
.limit locals 5
.limit stack 7
.end method

.method private b(Landroid/content/Context;)V
aload 0
new android/widget/ScrollView
dup
aload 1
invokespecial android/widget/ScrollView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
ldc_w 131095
invokevirtual android/widget/ScrollView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
bipush 17
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
aload 1
invokevirtual android/widget/ScrollView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
iload 2
iconst_0
iload 2
iconst_0
invokevirtual android/widget/ScrollView/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private c(Landroid/content/Context;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
ldc_w 131091
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iconst_3
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
iload 2
iload 2
iload 2
iload 2
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
bipush 48
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield net/hockeyapp/android/f/i/r Landroid/widget/ScrollView;
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private d(Landroid/content/Context;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
ldc_w 131093
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
bipush 17
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
iload 2
iload 2
iload 2
iconst_0
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
bipush 48
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield net/hockeyapp/android/f/i/q Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private e(Landroid/content/Context;)V
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
ldc_w 131092
invokevirtual android/widget/LinearLayout/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iconst_3
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
bipush 48
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private f(Landroid/content/Context;)V
aload 0
new android/widget/ListView
dup
aload 1
invokespecial android/widget/ListView/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
ldc_w 131094
invokevirtual android/widget/ListView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
aload 1
invokevirtual android/widget/ListView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/ListView/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/w Landroid/widget/ListView;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private g(Landroid/content/Context;)V
iconst_0
istore 2
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 4
aload 4
sipush 8194
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
aload 5
iconst_0
iload 3
iconst_2
idiv
iconst_0
iload 3
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 4
aload 5
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 4
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 4
sipush 16385
invokevirtual android/widget/EditText/setInputType(I)V
aload 4
iconst_1
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 4
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 4
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 4
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 4
sipush 1026
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 4
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 4
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
invokestatic net/hockeyapp/android/t/b()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/a Lnet/hockeyapp/android/c/i;
if_acmpne L0
bipush 8
istore 2
L0:
aload 4
iload 2
invokevirtual android/widget/EditText/setVisibility(I)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 5
.end method

.method private getButtonSelector()Landroid/graphics/drawable/Drawable;
new android/graphics/drawable/StateListDrawable
dup
invokespecial android/graphics/drawable/StateListDrawable/<init>()V
astore 1
new android/graphics/drawable/ColorDrawable
dup
ldc_w -16777216
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_1
newarray int
dup
iconst_0
ldc_w -16842919
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
new android/graphics/drawable/ColorDrawable
dup
ldc_w -12303292
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_2
newarray int
dup
iconst_0
ldc_w -16842919
iastore
dup
iconst_1
ldc_w 16842908
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
new android/graphics/drawable/ColorDrawable
dup
ldc_w -7829368
invokespecial android/graphics/drawable/ColorDrawable/<init>(I)V
astore 2
aload 1
iconst_1
newarray int
dup
iconst_0
ldc_w 16842919
iastore
aload 2
invokevirtual android/graphics/drawable/StateListDrawable/addState([ILandroid/graphics/drawable/Drawable;)V
aload 1
areturn
.limit locals 3
.limit stack 5
.end method

.method private h(Landroid/content/Context;)V
iconst_0
istore 2
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 3
aload 3
sipush 8196
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 4
aload 4
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 3
aload 4
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 3
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 3
bipush 33
invokevirtual android/widget/EditText/setInputType(I)V
aload 3
iconst_1
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 3
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 3
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 3
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 3
sipush 1027
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 3
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 3
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
invokestatic net/hockeyapp/android/t/c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/a Lnet/hockeyapp/android/c/i;
if_acmpne L0
bipush 8
istore 2
L0:
aload 3
iload 2
invokevirtual android/widget/EditText/setVisibility(I)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 7
.end method

.method private i(Landroid/content/Context;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 2
aload 2
sipush 8198
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
aload 3
iconst_0
iconst_0
iconst_0
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 2
aload 3
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 2
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 2
sipush 16433
invokevirtual android/widget/EditText/setInputType(I)V
aload 2
iconst_1
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 2
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 2
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 2
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 2
sipush 1028
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 2
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 2
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 2
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 4
.limit stack 7
.end method

.method private j(Landroid/content/Context;)V
new android/widget/EditText
dup
aload 1
invokespecial android/widget/EditText/<init>(Landroid/content/Context;)V
astore 4
aload 4
sipush 8200
invokevirtual android/widget/EditText/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
iconst_1
ldc_w 20.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 100.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
aload 5
iconst_0
iconst_0
iconst_0
iload 2
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 4
aload 5
invokevirtual android/widget/EditText/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 4
iconst_5
invokevirtual android/widget/EditText/setImeOptions(I)V
aload 4
sipush 16385
invokevirtual android/widget/EditText/setInputType(I)V
aload 4
iconst_0
invokevirtual android/widget/EditText/setSingleLine(Z)V
aload 4
ldc_w -7829368
invokevirtual android/widget/EditText/setTextColor(I)V
aload 4
iconst_2
ldc_w 15.0F
invokevirtual android/widget/EditText/setTextSize(IF)V
aload 4
aconst_null
iconst_0
invokevirtual android/widget/EditText/setTypeface(Landroid/graphics/Typeface;I)V
aload 4
iload 3
invokevirtual android/widget/EditText/setMinimumHeight(I)V
aload 4
sipush 1029
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setHint(Ljava/lang/CharSequence;)V
aload 4
ldc_w -3355444
invokevirtual android/widget/EditText/setHintTextColor(I)V
aload 1
aload 4
invokestatic net/hockeyapp/android/f/i/a(Landroid/content/Context;Landroid/widget/EditText;)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 4
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 5
.end method

.method private k(Landroid/content/Context;)V
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 8192
invokevirtual android/widget/TextView/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 3
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 3
iconst_0
iconst_0
iconst_0
iconst_0
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 1
aload 3
invokevirtual android/widget/TextView/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/TextView/setPadding(IIII)V
aload 1
getstatic android/text/TextUtils$TruncateAt/END Landroid/text/TextUtils$TruncateAt;
invokevirtual android/widget/TextView/setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
aload 1
fconst_1
fconst_0
fconst_1
iconst_m1
invokevirtual android/widget/TextView/setShadowLayer(FFFI)V
aload 1
iconst_1
invokevirtual android/widget/TextView/setSingleLine(Z)V
aload 1
sipush 1030
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
ldc_w -7829368
invokevirtual android/widget/TextView/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/TextView/setTextSize(IF)V
aload 1
aconst_null
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
getfield net/hockeyapp/android/f/i/t Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 4
.limit stack 5
.end method

.method private l(Landroid/content/Context;)V
aload 0
new net/hockeyapp/android/f/a
dup
aload 1
invokespecial net/hockeyapp/android/f/a/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
sipush 8209
invokevirtual android/view/ViewGroup/setId(I)V
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 1
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
aload 1
iconst_3
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
aload 1
invokevirtual android/view/ViewGroup/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
iconst_0
iconst_0
iconst_0
iload 2
invokevirtual android/view/ViewGroup/setPadding(IIII)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 0
getfield net/hockeyapp/android/f/i/v Landroid/view/ViewGroup;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 3
.limit stack 5
.end method

.method private m(Landroid/content/Context;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 8208
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
aload 5
iconst_0
iconst_0
iconst_0
iload 4
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 1
aload 5
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
iload 3
iload 2
iload 3
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 1
sipush 1031
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 5
.end method

.method private n(Landroid/content/Context;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
sipush 8201
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 30.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
aload 5
iconst_0
iconst_0
iconst_0
iload 4
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 1
aload 5
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
iload 3
iload 2
iload 3
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 1
sipush 1032
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/i/s Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 5
.end method

.method private o(Landroid/content/Context;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc_w 131088
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 5.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
aload 5
iconst_0
iconst_0
iload 4
iload 3
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 5
fconst_1
putfield android/widget/LinearLayout$LayoutParams/weight F
aload 1
aload 5
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 1
bipush 17
invokevirtual android/widget/Button/setGravity(I)V
aload 1
sipush 1033
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 5
.end method

.method private p(Landroid/content/Context;)V
new android/widget/Button
dup
aload 1
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 1
aload 1
ldc_w 131089
invokevirtual android/widget/Button/setId(I)V
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 2
iconst_1
ldc_w 10.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 3
iconst_1
ldc_w 5.0F
aload 0
invokevirtual net/hockeyapp/android/f/i/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
invokestatic android/util/TypedValue/applyDimension(IFLandroid/util/DisplayMetrics;)F
f2i
istore 4
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
astore 5
aload 5
iload 4
iconst_0
iconst_0
iload 3
invokevirtual android/widget/LinearLayout$LayoutParams/setMargins(IIII)V
aload 5
iconst_1
putfield android/widget/LinearLayout$LayoutParams/gravity I
aload 1
aload 5
invokevirtual android/widget/Button/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 0
invokespecial net/hockeyapp/android/f/i/getButtonSelector()Landroid/graphics/drawable/Drawable;
invokevirtual android/widget/Button/setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
aload 1
iconst_0
iload 2
iconst_0
iload 2
invokevirtual android/widget/Button/setPadding(IIII)V
aload 1
bipush 17
invokevirtual android/widget/Button/setGravity(I)V
aload 1
sipush 1034
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 1
iconst_m1
invokevirtual android/widget/Button/setTextColor(I)V
aload 1
iconst_2
ldc_w 15.0F
invokevirtual android/widget/Button/setTextSize(IF)V
aload 5
fconst_1
putfield android/widget/LinearLayout$LayoutParams/weight F
aload 0
getfield net/hockeyapp/android/f/i/u Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
return
.limit locals 6
.limit stack 5
.end method

.method private static q(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
ldc_w 10.0F
fmul
f2i
istore 1
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 2
aload 2
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 3
aload 3
iconst_m1
invokevirtual android/graphics/Paint/setColor(I)V
aload 3
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 3
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 2
iload 1
iload 1
iload 1
iload 1
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
aload 0
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/density F
f2d
ldc2_w 1.5D
dmul
d2i
istore 1
new android/graphics/drawable/ShapeDrawable
dup
new android/graphics/drawable/shapes/RectShape
dup
invokespecial android/graphics/drawable/shapes/RectShape/<init>()V
invokespecial android/graphics/drawable/ShapeDrawable/<init>(Landroid/graphics/drawable/shapes/Shape;)V
astore 0
aload 0
invokevirtual android/graphics/drawable/ShapeDrawable/getPaint()Landroid/graphics/Paint;
astore 3
aload 3
ldc_w -12303292
invokevirtual android/graphics/Paint/setColor(I)V
aload 3
getstatic android/graphics/Paint$Style/FILL_AND_STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 3
fconst_1
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
aload 0
iconst_0
iconst_0
iconst_0
iload 1
invokevirtual android/graphics/drawable/ShapeDrawable/setPadding(IIII)V
new android/graphics/drawable/LayerDrawable
dup
iconst_2
anewarray android/graphics/drawable/Drawable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 2
aastore
invokespecial android/graphics/drawable/LayerDrawable/<init>([Landroid/graphics/drawable/Drawable;)V
areturn
.limit locals 4
.limit stack 6
.end method
