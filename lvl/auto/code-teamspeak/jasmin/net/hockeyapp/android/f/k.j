.bytecode 50.0
.class public final synchronized net/hockeyapp/android/f/k
.super android/widget/ImageView

.field private static final 'b' F = 4.0F


.field public 'a' Ljava/util/Stack;

.field private 'c' Landroid/graphics/Path;

.field private 'd' Landroid/graphics/Paint;

.field private 'e' F

.field private 'f' F

.method public <init>(Landroid/content/Context;Landroid/net/Uri;II)V
aload 0
aload 1
invokespecial android/widget/ImageView/<init>(Landroid/content/Context;)V
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
new java/util/Stack
dup
invokespecial java/util/Stack/<init>()V
putfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
aload 0
new android/graphics/Paint
dup
invokespecial android/graphics/Paint/<init>()V
putfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
iconst_1
invokevirtual android/graphics/Paint/setAntiAlias(Z)V
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
iconst_1
invokevirtual android/graphics/Paint/setDither(Z)V
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
ldc_w -65536
invokevirtual android/graphics/Paint/setColor(I)V
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
getstatic android/graphics/Paint$Style/STROKE Landroid/graphics/Paint$Style;
invokevirtual android/graphics/Paint/setStyle(Landroid/graphics/Paint$Style;)V
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
getstatic android/graphics/Paint$Join/ROUND Landroid/graphics/Paint$Join;
invokevirtual android/graphics/Paint/setStrokeJoin(Landroid/graphics/Paint$Join;)V
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
getstatic android/graphics/Paint$Cap/ROUND Landroid/graphics/Paint$Cap;
invokevirtual android/graphics/Paint/setStrokeCap(Landroid/graphics/Paint$Cap;)V
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
ldc_w 12.0F
invokevirtual android/graphics/Paint/setStrokeWidth(F)V
new net/hockeyapp/android/f/l
dup
aload 0
invokespecial net/hockeyapp/android/f/l/<init>(Lnet/hockeyapp/android/f/k;)V
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
dup
iconst_2
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_3
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual net/hockeyapp/android/f/l/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 5
.limit stack 5
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;)I
.catch java/io/IOException from L0 to L1 using L2
iconst_1
istore 3
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 5
aload 5
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
L0:
aload 0
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 5
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 5
getfield android/graphics/BitmapFactory$Options/outWidth I
i2f
fstore 2
aload 5
getfield android/graphics/BitmapFactory$Options/outHeight I
istore 4
L1:
fload 2
iload 4
i2f
fdiv
fconst_1
fcmpl
ifle L3
iconst_0
istore 3
L3:
iload 3
ireturn
L2:
astore 0
ldc "HockeyApp"
ldc "Unable to determine necessary screen orientation."
aload 0
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
iconst_1
ireturn
.limit locals 6
.limit stack 3
.end method

.method private static a(Landroid/graphics/BitmapFactory$Options;II)I
aload 0
getfield android/graphics/BitmapFactory$Options/outHeight I
istore 5
aload 0
getfield android/graphics/BitmapFactory$Options/outWidth I
istore 6
iconst_1
istore 4
iconst_1
istore 3
iload 5
iload 2
if_icmpgt L0
iload 6
iload 1
if_icmple L1
L0:
iload 5
iconst_2
idiv
istore 5
iload 6
iconst_2
idiv
istore 6
L2:
iload 3
istore 4
iload 5
iload 3
idiv
iload 2
if_icmple L1
iload 3
istore 4
iload 6
iload 3
idiv
iload 1
if_icmple L1
iload 3
iconst_2
imul
istore 3
goto L2
L1:
iload 4
ireturn
.limit locals 7
.limit stack 2
.end method

.method static synthetic a(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 4
aload 4
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 4
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 4
aload 4
iload 2
iload 3
invokestatic net/hockeyapp/android/f/k/a(Landroid/graphics/BitmapFactory$Options;II)I
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 4
iconst_0
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 4
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
.limit locals 5
.limit stack 4
.end method

.method private a()V
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/clear()V
aload 0
invokevirtual net/hockeyapp/android/f/k/invalidate()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(FF)V
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
fload 1
fload 2
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
fload 1
putfield net/hockeyapp/android/f/k/e F
aload 0
fload 2
putfield net/hockeyapp/android/f/k/f F
return
.limit locals 3
.limit stack 3
.end method

.method private static b(Landroid/content/ContentResolver;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
new android/graphics/BitmapFactory$Options
dup
invokespecial android/graphics/BitmapFactory$Options/<init>()V
astore 4
aload 4
iconst_1
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 4
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
pop
aload 4
aload 4
iload 2
iload 3
invokestatic net/hockeyapp/android/f/k/a(Landroid/graphics/BitmapFactory$Options;II)I
putfield android/graphics/BitmapFactory$Options/inSampleSize I
aload 4
iconst_0
putfield android/graphics/BitmapFactory$Options/inJustDecodeBounds Z
aload 0
aload 1
invokevirtual android/content/ContentResolver/openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
aconst_null
aload 4
invokestatic android/graphics/BitmapFactory/decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
areturn
.limit locals 5
.limit stack 4
.end method

.method private b()V
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/empty()Z
ifne L0
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
pop
aload 0
invokevirtual net/hockeyapp/android/f/k/invalidate()V
L0:
return
.limit locals 1
.limit stack 1
.end method

.method private b(FF)V
fload 1
aload 0
getfield net/hockeyapp/android/f/k/e F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 3
fload 2
aload 0
getfield net/hockeyapp/android/f/k/f F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 4
fload 3
ldc_w 4.0F
fcmpl
ifge L0
fload 4
ldc_w 4.0F
fcmpl
iflt L1
L0:
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
getfield net/hockeyapp/android/f/k/e F
aload 0
getfield net/hockeyapp/android/f/k/f F
aload 0
getfield net/hockeyapp/android/f/k/e F
fload 1
fadd
fconst_2
fdiv
aload 0
getfield net/hockeyapp/android/f/k/f F
fload 2
fadd
fconst_2
fdiv
invokevirtual android/graphics/Path/quadTo(FFFF)V
aload 0
fload 1
putfield net/hockeyapp/android/f/k/e F
aload 0
fload 2
putfield net/hockeyapp/android/f/k/f F
L1:
return
.limit locals 5
.limit stack 6
.end method

.method private c()Z
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/empty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()V
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
getfield net/hockeyapp/android/f/k/e F
aload 0
getfield net/hockeyapp/android/f/k/f F
invokevirtual android/graphics/Path/lineTo(FF)V
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
return
.limit locals 1
.limit stack 3
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
aload 0
aload 1
invokespecial android/widget/ImageView/onDraw(Landroid/graphics/Canvas;)V
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/graphics/Path
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
goto L0
L1:
aload 1
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
getfield net/hockeyapp/android/f/k/d Landroid/graphics/Paint;
invokevirtual android/graphics/Canvas/drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
aload 1
invokevirtual android/view/MotionEvent/getX()F
fstore 2
aload 1
invokevirtual android/view/MotionEvent/getY()F
fstore 3
aload 1
invokevirtual android/view/MotionEvent/getAction()I
tableswitch 0
L0
L1
L2
default : L3
L3:
iconst_1
ireturn
L0:
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
invokevirtual android/graphics/Path/reset()V
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
fload 2
fload 3
invokevirtual android/graphics/Path/moveTo(FF)V
aload 0
fload 2
putfield net/hockeyapp/android/f/k/e F
aload 0
fload 3
putfield net/hockeyapp/android/f/k/f F
aload 0
invokevirtual net/hockeyapp/android/f/k/invalidate()V
goto L3
L2:
fload 2
aload 0
getfield net/hockeyapp/android/f/k/e F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 4
fload 3
aload 0
getfield net/hockeyapp/android/f/k/f F
fsub
invokestatic java/lang/Math/abs(F)F
fstore 5
fload 4
ldc_w 4.0F
fcmpl
ifge L4
fload 5
ldc_w 4.0F
fcmpl
iflt L5
L4:
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
getfield net/hockeyapp/android/f/k/e F
aload 0
getfield net/hockeyapp/android/f/k/f F
aload 0
getfield net/hockeyapp/android/f/k/e F
fload 2
fadd
fconst_2
fdiv
aload 0
getfield net/hockeyapp/android/f/k/f F
fload 3
fadd
fconst_2
fdiv
invokevirtual android/graphics/Path/quadTo(FFFF)V
aload 0
fload 2
putfield net/hockeyapp/android/f/k/e F
aload 0
fload 3
putfield net/hockeyapp/android/f/k/f F
L5:
aload 0
invokevirtual net/hockeyapp/android/f/k/invalidate()V
goto L3
L1:
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
getfield net/hockeyapp/android/f/k/e F
aload 0
getfield net/hockeyapp/android/f/k/f F
invokevirtual android/graphics/Path/lineTo(FF)V
aload 0
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
aload 0
getfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
invokevirtual java/util/Stack/push(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
new android/graphics/Path
dup
invokespecial android/graphics/Path/<init>()V
putfield net/hockeyapp/android/f/k/c Landroid/graphics/Path;
aload 0
invokevirtual net/hockeyapp/android/f/k/invalidate()V
goto L3
.limit locals 6
.limit stack 6
.end method
