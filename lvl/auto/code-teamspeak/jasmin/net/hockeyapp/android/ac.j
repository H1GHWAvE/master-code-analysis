.bytecode 50.0
.class public final synchronized net/hockeyapp/android/ac
.super java/lang/Object

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field public static final 'c' I = 2


.field public static final 'd' I = 3


.field static final 'e' Ljava/lang/String; = "net.hockeyapp.android.EXIT"

.field static 'f' Ljava/lang/Class;

.field static 'g' Lnet/hockeyapp/android/ae;

.field private static 'h' Ljava/lang/String;

.field private static 'i' Ljava/lang/String;

.field private static 'j' Landroid/os/Handler;

.field private static 'k' Ljava/lang/String;

.field private static 'l' I

.method static <clinit>()V
aconst_null
putstatic net/hockeyapp/android/ac/h Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/ac/i Ljava/lang/String;
aconst_null
putstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
aconst_null
putstatic net/hockeyapp/android/ac/k Ljava/lang/String;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(I)Ljava/lang/String;
ldc ""
astore 1
iload 0
iconst_2
if_icmpne L0
ldc "authorize"
astore 1
L1:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic net/hockeyapp/android/ac/k Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "api/3/apps/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic net/hockeyapp/android/ac/h Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/identity/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
iload 0
iconst_1
if_icmpne L2
ldc "check"
astore 1
goto L1
L2:
iload 0
iconst_3
if_icmpne L1
ldc "validate"
astore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method private static a(Landroid/app/Activity;Landroid/content/Intent;)V
iconst_1
istore 4
aload 1
ifnull L0
aload 1
ldc "net.hockeyapp.android.EXIT"
iconst_0
invokevirtual android/content/Intent/getBooleanExtra(Ljava/lang/String;Z)Z
ifeq L0
aload 0
invokevirtual android/app/Activity/finish()V
L1:
return
L0:
aload 0
ifnull L1
getstatic net/hockeyapp/android/ac/l I
ifeq L1
getstatic net/hockeyapp/android/ac/l I
iconst_3
if_icmpeq L1
aload 0
ldc "net.hockeyapp.android.login"
iconst_0
invokevirtual android/app/Activity/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 5
aload 5
ldc "mode"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
getstatic net/hockeyapp/android/ac/l I
if_icmpeq L2
aload 5
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "auid"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
ldc "iuid"
invokeinterface android/content/SharedPreferences$Editor/remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 1
ldc "mode"
getstatic net/hockeyapp/android/ac/l I
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L2:
aload 5
ldc "auid"
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 1
aload 5
ldc "iuid"
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 5
aload 1
ifnonnull L3
aload 5
ifnonnull L3
iconst_1
istore 2
L4:
aload 1
ifnonnull L5
getstatic net/hockeyapp/android/ac/l I
iconst_2
if_icmpne L5
iconst_1
istore 3
L6:
aload 5
ifnonnull L7
getstatic net/hockeyapp/android/ac/l I
iconst_1
if_icmpne L7
L8:
iload 2
ifne L9
iload 3
ifne L9
iload 4
ifeq L10
L9:
aload 0
invokestatic net/hockeyapp/android/ac/b(Landroid/content/Context;)V
return
L3:
iconst_0
istore 2
goto L4
L5:
iconst_0
istore 3
goto L6
L7:
iconst_0
istore 4
goto L8
L10:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 6
aload 1
ifnull L11
aload 6
ldc "type"
ldc "auid"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 6
ldc "id"
aload 1
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L12:
new net/hockeyapp/android/d/p
dup
aload 0
getstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
iconst_3
invokestatic net/hockeyapp/android/ac/a(I)Ljava/lang/String;
iconst_3
aload 6
invokespecial net/hockeyapp/android/d/p/<init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;ILjava/util/Map;)V
astore 0
aload 0
iconst_0
putfield net/hockeyapp/android/d/p/d Z
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
L11:
aload 5
ifnull L12
aload 6
ldc "type"
ldc "iuid"
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 6
ldc "id"
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
goto L12
.limit locals 7
.limit stack 7
.end method

.method static synthetic a(Landroid/content/Context;)V
aload 0
invokestatic net/hockeyapp/android/ac/b(Landroid/content/Context;)V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
aload 0
ifnull L0
aload 1
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/ac/h Ljava/lang/String;
aload 2
putstatic net/hockeyapp/android/ac/i Ljava/lang/String;
ldc "https://sdk.hockeyapp.net/"
putstatic net/hockeyapp/android/ac/k Ljava/lang/String;
iload 3
putstatic net/hockeyapp/android/ac/l I
aconst_null
putstatic net/hockeyapp/android/ac/f Ljava/lang/Class;
getstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
ifnonnull L1
new net/hockeyapp/android/ad
dup
aload 0
invokespecial net/hockeyapp/android/ad/<init>(Landroid/content/Context;)V
putstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
L1:
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILnet/hockeyapp/android/ae;)V
aload 4
putstatic net/hockeyapp/android/ac/g Lnet/hockeyapp/android/ae;
aload 0
ifnull L0
aload 1
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/ac/h Ljava/lang/String;
aload 2
putstatic net/hockeyapp/android/ac/i Ljava/lang/String;
ldc "https://sdk.hockeyapp.net/"
putstatic net/hockeyapp/android/ac/k Ljava/lang/String;
iload 3
putstatic net/hockeyapp/android/ac/l I
aconst_null
putstatic net/hockeyapp/android/ac/f Ljava/lang/Class;
getstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
ifnonnull L1
new net/hockeyapp/android/ad
dup
aload 0
invokespecial net/hockeyapp/android/ad/<init>(Landroid/content/Context;)V
putstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
L1:
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 5
.limit stack 3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Class;)V
aload 0
ifnull L0
aload 1
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
putstatic net/hockeyapp/android/ac/h Ljava/lang/String;
aload 2
putstatic net/hockeyapp/android/ac/i Ljava/lang/String;
aload 3
putstatic net/hockeyapp/android/ac/k Ljava/lang/String;
iload 4
putstatic net/hockeyapp/android/ac/l I
aload 5
putstatic net/hockeyapp/android/ac/f Ljava/lang/Class;
getstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
ifnonnull L1
new net/hockeyapp/android/ad
dup
aload 0
invokespecial net/hockeyapp/android/ad/<init>(Landroid/content/Context;)V
putstatic net/hockeyapp/android/ac/j Landroid/os/Handler;
L1:
aload 0
invokestatic net/hockeyapp/android/a/a(Landroid/content/Context;)V
L0:
return
.limit locals 6
.limit stack 3
.end method

.method private static b(Landroid/content/Context;)V
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 1
aload 1
ldc_w 1342177280
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 1
aload 0
ldc net/hockeyapp/android/aa
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 1
ldc "url"
getstatic net/hockeyapp/android/ac/l I
invokestatic net/hockeyapp/android/ac/a(I)Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "mode"
getstatic net/hockeyapp/android/ac/l I
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;I)Landroid/content/Intent;
pop
aload 1
ldc "secret"
getstatic net/hockeyapp/android/ac/i Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
return
.limit locals 2
.limit stack 3
.end method
