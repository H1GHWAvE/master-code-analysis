.bytecode 50.0
.class public final synchronized net/hockeyapp/android/d/q
.super android/os/AsyncTask

.field public static final 'a' I = 2


.field public static final 'b' Ljava/lang/String; = "net.hockeyapp.android.feedback"

.field public static final 'c' Ljava/lang/String; = "idLastMessageSend"

.field public static final 'd' Ljava/lang/String; = "idLastMessageProcessed"

.field public 'e' Ljava/lang/String;

.field private 'f' Landroid/content/Context;

.field private 'g' Ljava/lang/String;

.field private 'h' Landroid/os/Handler;

.field private 'i' Ljava/lang/String;

.method public <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
aload 0
invokespecial android/os/AsyncTask/<init>()V
aload 0
aload 1
putfield net/hockeyapp/android/d/q/f Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/d/q/g Ljava/lang/String;
aload 0
aload 3
putfield net/hockeyapp/android/d/q/h Landroid/os/Handler;
aload 0
aload 4
putfield net/hockeyapp/android/d/q/i Ljava/lang/String;
aload 0
aconst_null
putfield net/hockeyapp/android/d/q/e Ljava/lang/String;
return
.limit locals 5
.limit stack 2
.end method

.method private transient a()Lnet/hockeyapp/android/c/h;
aconst_null
astore 5
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/q/g Ljava/lang/String;
ifnull L0
getstatic net/hockeyapp/android/e/k/a Lnet/hockeyapp/android/e/i;
astore 6
aload 0
getfield net/hockeyapp/android/d/q/g Ljava/lang/String;
invokestatic net/hockeyapp/android/e/i/a(Ljava/lang/String;)Lnet/hockeyapp/android/c/h;
astore 7
aload 7
ifnull L1
aload 7
getfield net/hockeyapp/android/c/h/b Lnet/hockeyapp/android/c/d;
ifnull L1
aload 7
getfield net/hockeyapp/android/c/h/b Lnet/hockeyapp/android/c/d;
getfield net/hockeyapp/android/c/d/e Ljava/util/ArrayList;
astore 6
aload 6
ifnull L1
aload 6
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L1
aload 6
aload 6
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/hockeyapp/android/c/g
getfield net/hockeyapp/android/c/g/g I
istore 1
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
ldc "net.hockeyapp.android.feedback"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 6
aload 0
getfield net/hockeyapp/android/d/q/i Ljava/lang/String;
ldc "send"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "idLastMessageSend"
iload 1
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
ldc "idLastMessageProcessed"
iload 1
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L1:
aload 7
areturn
L2:
aload 0
getfield net/hockeyapp/android/d/q/i Ljava/lang/String;
ldc "fetch"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 6
ldc "idLastMessageSend"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 2
aload 6
ldc "idLastMessageProcessed"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
iload 1
iload 2
if_icmpeq L1
iload 1
iload 3
if_icmpeq L1
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "idLastMessageProcessed"
iload 1
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
astore 6
aload 6
ifnull L3
aload 6
invokevirtual net/hockeyapp/android/y/a()Z
istore 4
L4:
iload 4
ifne L1
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
astore 9
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
ifnull L1
aload 9
ldc "notification"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
astore 8
aload 9
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
ldc "ic_menu_refresh"
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 1
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
ifnull L5
ldc net/hockeyapp/android/FeedbackActivity
astore 5
L5:
aload 5
astore 6
aload 5
ifnonnull L6
ldc net/hockeyapp/android/FeedbackActivity
astore 6
L6:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 5
aload 5
ldc_w 805306368
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 5
aload 9
aload 6
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 5
ldc "url"
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 9
aload 9
iconst_0
aload 5
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
ldc "HockeyApp Feedback"
ldc "A new answer to your feedback is available."
iload 1
invokestatic net/hockeyapp/android/e/w/a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
astore 5
aload 5
ifnull L1
aload 8
iconst_2
aload 5
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
goto L1
L0:
aconst_null
areturn
L3:
iconst_0
istore 4
goto L4
.limit locals 10
.limit stack 5
.end method

.method private a(Landroid/content/Context;)V
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
ifnonnull L0
L1:
return
L0:
aload 1
ldc "notification"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
astore 5
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
ldc "ic_menu_refresh"
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 2
aconst_null
astore 3
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
ifnull L2
ldc net/hockeyapp/android/FeedbackActivity
astore 3
L2:
aload 3
astore 4
aload 3
ifnonnull L3
ldc net/hockeyapp/android/FeedbackActivity
astore 4
L3:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 3
aload 3
ldc_w 805306368
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 3
aload 1
aload 4
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 3
ldc "url"
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
aload 1
iconst_0
aload 3
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
ldc "HockeyApp Feedback"
ldc "A new answer to your feedback is available."
iload 2
invokestatic net/hockeyapp/android/e/w/a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
astore 1
aload 1
ifnull L1
aload 5
iconst_2
aload 1
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
return
.limit locals 6
.limit stack 5
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield net/hockeyapp/android/d/q/e Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/ArrayList;)V
aload 1
aload 1
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/hockeyapp/android/c/g
getfield net/hockeyapp/android/c/g/g I
istore 2
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
ldc "net.hockeyapp.android.feedback"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 1
aload 0
getfield net/hockeyapp/android/d/q/i Ljava/lang/String;
ldc "send"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 1
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "idLastMessageSend"
iload 2
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
ldc "idLastMessageProcessed"
iload 2
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L1:
return
L0:
aload 0
getfield net/hockeyapp/android/d/q/i Ljava/lang/String;
ldc "fetch"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 1
ldc "idLastMessageSend"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
aload 1
ldc "idLastMessageProcessed"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 4
iload 2
iload 3
if_icmpeq L1
iload 2
iload 4
if_icmpeq L1
aload 1
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "idLastMessageProcessed"
iload 2
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
astore 1
aload 1
ifnull L2
aload 1
invokevirtual net/hockeyapp/android/y/a()Z
istore 5
L3:
iload 5
ifne L1
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
astore 8
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
ifnull L1
aload 8
ldc "notification"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
astore 7
aload 8
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
ldc "ic_menu_refresh"
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 2
aconst_null
astore 1
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
ifnull L4
ldc net/hockeyapp/android/FeedbackActivity
astore 1
L4:
aload 1
astore 6
aload 1
ifnonnull L5
ldc net/hockeyapp/android/FeedbackActivity
astore 6
L5:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 1
aload 1
ldc_w 805306368
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 1
aload 8
aload 6
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 1
ldc "url"
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 8
aload 8
iconst_0
aload 1
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
ldc "HockeyApp Feedback"
ldc "A new answer to your feedback is available."
iload 2
invokestatic net/hockeyapp/android/e/w/a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
astore 1
aload 1
ifnull L1
aload 7
iconst_2
aload 1
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
return
L2:
iconst_0
istore 5
goto L3
.limit locals 9
.limit stack 5
.end method

.method private a(Lnet/hockeyapp/android/c/h;)V
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/d/q/h Landroid/os/Handler;
ifnull L0
new android/os/Message
dup
invokespecial android/os/Message/<init>()V
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 3
ldc "parse_feedback_response"
aload 1
invokevirtual android/os/Bundle/putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 0
getfield net/hockeyapp/android/d/q/h Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/sendMessage(Landroid/os/Message;)Z
pop
L0:
return
.limit locals 4
.limit stack 3
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aconst_null
astore 1
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
ifnull L0
aload 0
getfield net/hockeyapp/android/d/q/g Ljava/lang/String;
ifnull L0
getstatic net/hockeyapp/android/e/k/a Lnet/hockeyapp/android/e/i;
astore 6
aload 0
getfield net/hockeyapp/android/d/q/g Ljava/lang/String;
invokestatic net/hockeyapp/android/e/i/a(Ljava/lang/String;)Lnet/hockeyapp/android/c/h;
astore 7
aload 7
ifnull L1
aload 7
getfield net/hockeyapp/android/c/h/b Lnet/hockeyapp/android/c/d;
ifnull L1
aload 7
getfield net/hockeyapp/android/c/h/b Lnet/hockeyapp/android/c/d;
getfield net/hockeyapp/android/c/d/e Ljava/util/ArrayList;
astore 6
aload 6
ifnull L1
aload 6
invokevirtual java/util/ArrayList/isEmpty()Z
ifne L1
aload 6
aload 6
invokevirtual java/util/ArrayList/size()I
iconst_1
isub
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast net/hockeyapp/android/c/g
getfield net/hockeyapp/android/c/g/g I
istore 2
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
ldc "net.hockeyapp.android.feedback"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
astore 6
aload 0
getfield net/hockeyapp/android/d/q/i Ljava/lang/String;
ldc "send"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "idLastMessageSend"
iload 2
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
ldc "idLastMessageProcessed"
iload 2
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L1:
aload 7
areturn
L2:
aload 0
getfield net/hockeyapp/android/d/q/i Ljava/lang/String;
ldc "fetch"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 6
ldc "idLastMessageSend"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 3
aload 6
ldc "idLastMessageProcessed"
iconst_m1
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
istore 4
iload 2
iload 3
if_icmpeq L1
iload 2
iload 4
if_icmpeq L1
aload 6
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "idLastMessageProcessed"
iload 2
invokeinterface android/content/SharedPreferences$Editor/putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor; 2
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
astore 6
aload 6
ifnull L3
aload 6
invokevirtual net/hockeyapp/android/y/a()Z
istore 5
L4:
iload 5
ifne L1
aload 0
getfield net/hockeyapp/android/d/q/f Landroid/content/Context;
astore 9
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
ifnull L1
aload 9
ldc "notification"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
astore 8
aload 9
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
ldc "ic_menu_refresh"
ldc "drawable"
ldc "android"
invokevirtual android/content/res/Resources/getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
istore 2
invokestatic net/hockeyapp/android/t/a()Lnet/hockeyapp/android/y;
ifnull L5
ldc net/hockeyapp/android/FeedbackActivity
astore 1
L5:
aload 1
astore 6
aload 1
ifnonnull L6
ldc net/hockeyapp/android/FeedbackActivity
astore 6
L6:
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 1
aload 1
ldc_w 805306368
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 1
aload 9
aload 6
invokevirtual android/content/Intent/setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
pop
aload 1
ldc "url"
aload 0
getfield net/hockeyapp/android/d/q/e Ljava/lang/String;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 9
aload 9
iconst_0
aload 1
ldc_w 1073741824
invokestatic android/app/PendingIntent/getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
ldc "HockeyApp Feedback"
ldc "A new answer to your feedback is available."
iload 2
invokestatic net/hockeyapp/android/e/w/a(Landroid/content/Context;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;I)Landroid/app/Notification;
astore 1
aload 1
ifnull L1
aload 8
iconst_2
aload 1
invokevirtual android/app/NotificationManager/notify(ILandroid/app/Notification;)V
goto L1
L0:
aconst_null
areturn
L3:
iconst_0
istore 5
goto L4
.limit locals 10
.limit stack 5
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast net/hockeyapp/android/c/h
astore 1
aload 1
ifnull L0
aload 0
getfield net/hockeyapp/android/d/q/h Landroid/os/Handler;
ifnull L0
new android/os/Message
dup
invokespecial android/os/Message/<init>()V
astore 2
new android/os/Bundle
dup
invokespecial android/os/Bundle/<init>()V
astore 3
aload 3
ldc "parse_feedback_response"
aload 1
invokevirtual android/os/Bundle/putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
aload 2
aload 3
invokevirtual android/os/Message/setData(Landroid/os/Bundle;)V
aload 0
getfield net/hockeyapp/android/d/q/h Landroid/os/Handler;
aload 2
invokevirtual android/os/Handler/sendMessage(Landroid/os/Message;)Z
pop
L0:
return
.limit locals 4
.limit stack 3
.end method
