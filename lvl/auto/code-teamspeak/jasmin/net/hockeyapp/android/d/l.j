.bytecode 50.0
.class public synchronized net/hockeyapp/android/d/l
.super android/os/AsyncTask

.field protected static final 'a' I = 6


.field protected 'b' Landroid/content/Context;

.field protected 'c' Lnet/hockeyapp/android/b/a;

.field protected 'd' Ljava/lang/String;

.field protected 'e' Ljava/lang/String;

.field protected 'f' Ljava/lang/String;

.field protected 'g' Landroid/app/ProgressDialog;

.field private 'h' Ljava/lang/String;

.method public <init>(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/b/a;)V
aload 0
invokespecial android/os/AsyncTask/<init>()V
aload 0
aload 1
putfield net/hockeyapp/android/d/l/b Landroid/content/Context;
aload 0
aload 2
putfield net/hockeyapp/android/d/l/d Ljava/lang/String;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic java/util/UUID/randomUUID()Ljava/util/UUID;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ".apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield net/hockeyapp/android/d/l/e Ljava/lang/String;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/Download"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield net/hockeyapp/android/d/l/f Ljava/lang/String;
aload 0
aload 3
putfield net/hockeyapp/android/d/l/c Lnet/hockeyapp/android/b/a;
aload 0
aconst_null
putfield net/hockeyapp/android/d/l/h Ljava/lang/String;
return
.limit locals 4
.limit stack 3
.end method

.method protected static a(Ljava/net/URL;I)Ljava/net/URLConnection;
L0:
aload 0
invokevirtual java/net/URL/openConnection()Ljava/net/URLConnection;
checkcast java/net/HttpURLConnection
astore 4
aload 4
ldc "User-Agent"
ldc "HockeySDK/Android"
invokevirtual java/net/HttpURLConnection/addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
aload 4
iconst_1
invokevirtual java/net/HttpURLConnection/setInstanceFollowRedirects(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L1
aload 4
ldc "connection"
ldc "close"
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L1:
aload 4
invokevirtual java/net/HttpURLConnection/getResponseCode()I
istore 2
iload 2
sipush 301
if_icmpeq L2
iload 2
sipush 302
if_icmpeq L2
iload 2
sipush 303
if_icmpne L3
L2:
iload 1
ifne L4
L3:
aload 4
areturn
L4:
new java/net/URL
dup
aload 4
ldc "Location"
invokevirtual java/net/HttpURLConnection/getHeaderField(Ljava/lang/String;)Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
astore 3
aload 0
invokevirtual java/net/URL/getProtocol()Ljava/lang/String;
aload 3
invokevirtual java/net/URL/getProtocol()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L3
aload 4
invokevirtual java/net/HttpURLConnection/disconnect()V
iload 1
iconst_1
isub
istore 1
aload 3
astore 0
goto L0
.limit locals 5
.limit stack 4
.end method

.method private static a(Ljava/net/HttpURLConnection;)V
aload 0
ldc "User-Agent"
ldc "HockeySDK/Android"
invokevirtual java/net/HttpURLConnection/addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
aload 0
iconst_1
invokevirtual java/net/HttpURLConnection/setInstanceFollowRedirects(Z)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 9
if_icmpgt L0
aload 0
ldc "connection"
ldc "close"
invokevirtual java/net/HttpURLConnection/setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
L0:
return
.limit locals 1
.limit stack 3
.end method

.method public final a()V
aload 0
aconst_null
putfield net/hockeyapp/android/d/l/b Landroid/content/Context;
aload 0
aconst_null
putfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
return
.limit locals 1
.limit stack 2
.end method

.method public final a(Landroid/content/Context;)V
aload 0
aload 1
putfield net/hockeyapp/android/d/l/b Landroid/content/Context;
return
.limit locals 2
.limit stack 2
.end method

.method protected a(Ljava/lang/Long;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L5
.catch java/lang/Exception from L7 to L8 using L5
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
ifnull L1
L0:
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/dismiss()V
L1:
aload 1
invokevirtual java/lang/Long/longValue()J
lconst_0
lcmp
ifle L3
aload 0
getfield net/hockeyapp/android/d/l/c Lnet/hockeyapp/android/b/a;
aload 0
invokevirtual net/hockeyapp/android/b/a/a(Lnet/hockeyapp/android/d/l;)V
new android/content/Intent
dup
ldc "android.intent.action.VIEW"
invokespecial android/content/Intent/<init>(Ljava/lang/String;)V
astore 1
aload 1
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/l/f Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/d/l/e Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;Ljava/lang/String;)V
invokestatic android/net/Uri/fromFile(Ljava/io/File;)Landroid/net/Uri;
ldc "application/vnd.android.package-archive"
invokevirtual android/content/Intent/setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc_w 268435456
invokevirtual android/content/Intent/setFlags(I)Landroid/content/Intent;
pop
aload 0
getfield net/hockeyapp/android/d/l/b Landroid/content/Context;
aload 1
invokevirtual android/content/Context/startActivity(Landroid/content/Intent;)V
return
L3:
new android/app/AlertDialog$Builder
dup
aload 0
getfield net/hockeyapp/android/d/l/b Landroid/content/Context;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 2
aload 2
aload 0
getfield net/hockeyapp/android/d/l/c Lnet/hockeyapp/android/b/a;
sipush 256
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 0
getfield net/hockeyapp/android/d/l/h Ljava/lang/String;
ifnonnull L7
aload 0
getfield net/hockeyapp/android/d/l/c Lnet/hockeyapp/android/b/a;
sipush 257
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
astore 1
L4:
aload 2
aload 1
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
pop
aload 2
aload 0
getfield net/hockeyapp/android/d/l/c Lnet/hockeyapp/android/b/a;
sipush 258
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d/m
dup
aload 0
invokespecial net/hockeyapp/android/d/m/<init>(Lnet/hockeyapp/android/d/l;)V
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 2
aload 0
getfield net/hockeyapp/android/d/l/c Lnet/hockeyapp/android/b/a;
sipush 259
invokestatic net/hockeyapp/android/aj/a(Lnet/hockeyapp/android/ai;I)Ljava/lang/String;
new net/hockeyapp/android/d/n
dup
aload 0
invokespecial net/hockeyapp/android/d/n/<init>(Lnet/hockeyapp/android/d/l;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 2
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
invokevirtual android/app/AlertDialog/show()V
L6:
return
L7:
aload 0
getfield net/hockeyapp/android/d/l/h Ljava/lang/String;
astore 1
L8:
goto L4
L2:
astore 2
goto L1
L5:
astore 1
return
.limit locals 3
.limit stack 5
.end method

.method protected transient a([Ljava/lang/Integer;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L1 to L3 using L2
L0:
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
ifnonnull L1
aload 0
new android/app/ProgressDialog
dup
aload 0
getfield net/hockeyapp/android/d/l/b Landroid/content/Context;
invokespecial android/app/ProgressDialog/<init>(Landroid/content/Context;)V
putfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
iconst_1
invokevirtual android/app/ProgressDialog/setProgressStyle(I)V
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
ldc "Loading..."
invokevirtual android/app/ProgressDialog/setMessage(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
iconst_0
invokevirtual android/app/ProgressDialog/setCancelable(Z)V
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
invokevirtual android/app/ProgressDialog/show()V
L1:
aload 0
getfield net/hockeyapp/android/d/l/g Landroid/app/ProgressDialog;
aload 1
iconst_0
aaload
invokevirtual java/lang/Integer/intValue()I
invokevirtual android/app/ProgressDialog/setProgress(I)V
L3:
return
L2:
astore 1
return
.limit locals 2
.limit stack 4
.end method

.method protected transient b()Ljava/lang/Long;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L4 to L2 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
.catch java/lang/Exception from L11 to L12 using L2
L0:
new java/net/URL
dup
aload 0
invokevirtual net/hockeyapp/android/d/l/c()Ljava/lang/String;
invokespecial java/net/URL/<init>(Ljava/lang/String;)V
bipush 6
invokestatic net/hockeyapp/android/d/l/a(Ljava/net/URL;I)Ljava/net/URLConnection;
astore 5
aload 5
invokevirtual java/net/URLConnection/connect()V
aload 5
invokevirtual java/net/URLConnection/getContentLength()I
istore 1
aload 5
invokevirtual java/net/URLConnection/getContentType()Ljava/lang/String;
astore 6
L1:
aload 6
ifnull L4
L3:
aload 6
ldc "text"
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L4
aload 0
ldc "The requested download does not appear to be a file."
putfield net/hockeyapp/android/d/l/h Ljava/lang/String;
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
L4:
new java/io/File
dup
aload 0
getfield net/hockeyapp/android/d/l/f Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 6
aload 6
invokevirtual java/io/File/mkdirs()Z
ifne L5
aload 6
invokevirtual java/io/File/exists()Z
ifne L5
new java/io/IOException
dup
new java/lang/StringBuilder
dup
ldc "Could not create the dir(s):"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L2:
astore 5
aload 5
invokevirtual java/lang/Exception/printStackTrace()V
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
L5:
new java/io/File
dup
aload 6
aload 0
getfield net/hockeyapp/android/d/l/e Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 6
new java/io/BufferedInputStream
dup
aload 5
invokevirtual java/net/URLConnection/getInputStream()Ljava/io/InputStream;
invokespecial java/io/BufferedInputStream/<init>(Ljava/io/InputStream;)V
astore 5
new java/io/FileOutputStream
dup
aload 6
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 6
sipush 1024
newarray byte
astore 7
L6:
lconst_0
lstore 3
L7:
aload 5
aload 7
invokevirtual java/io/InputStream/read([B)I
istore 2
L8:
iload 2
iconst_m1
if_icmpeq L11
lload 3
iload 2
i2l
ladd
lstore 3
L9:
aload 0
iconst_1
anewarray java/lang/Integer
dup
iconst_0
lload 3
l2f
ldc_w 100.0F
fmul
iload 1
i2f
fdiv
invokestatic java/lang/Math/round(F)I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokevirtual net/hockeyapp/android/d/l/publishProgress([Ljava/lang/Object;)V
aload 6
aload 7
iconst_0
iload 2
invokevirtual java/io/OutputStream/write([BII)V
L10:
goto L7
L11:
aload 6
invokevirtual java/io/OutputStream/flush()V
aload 6
invokevirtual java/io/OutputStream/close()V
aload 5
invokevirtual java/io/InputStream/close()V
L12:
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
.limit locals 8
.limit stack 6
.end method

.method protected final c()Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/d/l/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "&type=apk"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual net/hockeyapp/android/d/l/b()Ljava/lang/Long;
areturn
.limit locals 2
.limit stack 1
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
aload 0
aload 1
checkcast java/lang/Long
invokevirtual net/hockeyapp/android/d/l/a(Ljava/lang/Long;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
aload 0
aload 1
checkcast [Ljava/lang/Integer;
invokevirtual net/hockeyapp/android/d/l/a([Ljava/lang/Integer;)V
return
.limit locals 2
.limit stack 2
.end method
