.bytecode 50.0
.class public synchronized net/hockeyapp/android/af
.super android/app/Activity

.field private static final 'a' I = 1


.field private static final 'b' I = 2


.field private static final 'c' I = 3


.field private 'd' Lnet/hockeyapp/android/f/k;

.field private 'e' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
.catch all from L0 to L1 using L2
aconst_null
astore 4
aconst_null
astore 3
aload 0
invokevirtual net/hockeyapp/android/af/getApplicationContext()Landroid/content/Context;
invokevirtual android/content/Context/getContentResolver()Landroid/content/ContentResolver;
aload 1
iconst_1
anewarray java/lang/String
dup
iconst_0
ldc "_data"
aastore
aconst_null
aconst_null
aconst_null
invokevirtual android/content/ContentResolver/query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
astore 5
aload 4
astore 1
aload 5
ifnull L3
aload 3
astore 1
L0:
aload 5
invokeinterface android/database/Cursor/moveToFirst()Z 0
ifeq L1
aload 5
iconst_0
invokeinterface android/database/Cursor/getString(I)Ljava/lang/String; 1
astore 1
L1:
aload 5
invokeinterface android/database/Cursor/close()V 0
L3:
aload 1
ifnonnull L4
aload 2
areturn
L2:
astore 1
aload 5
invokeinterface android/database/Cursor/close()V 0
aload 1
athrow
L4:
new java/io/File
dup
aload 1
invokespecial java/io/File/<init>(Ljava/lang/String;)V
invokevirtual java/io/File/getName()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 6
.end method

.method private a()V
new java/io/File
dup
aload 0
invokevirtual net/hockeyapp/android/af/getCacheDir()Ljava/io/File;
ldc "HockeyApp"
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 3
aload 3
invokevirtual java/io/File/mkdir()Z
pop
new java/io/File
dup
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/af/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".jpg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 2
iconst_1
istore 1
L0:
aload 2
invokevirtual java/io/File/exists()Z
ifeq L1
new java/io/File
dup
aload 3
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/af/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ".jpg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
iconst_1
invokevirtual net/hockeyapp/android/f/k/setDrawingCacheEnabled(Z)V
new net/hockeyapp/android/ah
dup
aload 0
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
invokevirtual net/hockeyapp/android/f/k/getDrawingCache()Landroid/graphics/Bitmap;
invokespecial net/hockeyapp/android/ah/<init>(Lnet/hockeyapp/android/af;Landroid/graphics/Bitmap;)V
iconst_1
anewarray java/io/File
dup
iconst_0
aload 2
aastore
invokevirtual net/hockeyapp/android/ah/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 3
aload 3
ldc "imageUri"
aload 2
invokestatic android/net/Uri/fromFile(Ljava/io/File;)Landroid/net/Uri;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 0
invokevirtual net/hockeyapp/android/af/getParent()Landroid/app/Activity;
ifnonnull L2
aload 0
iconst_m1
aload 3
invokevirtual net/hockeyapp/android/af/setResult(ILandroid/content/Intent;)V
L3:
aload 0
invokevirtual net/hockeyapp/android/af/finish()V
return
L2:
aload 0
invokevirtual net/hockeyapp/android/af/getParent()Landroid/app/Activity;
iconst_m1
aload 3
invokevirtual android/app/Activity/setResult(ILandroid/content/Intent;)V
goto L3
.limit locals 4
.limit stack 5
.end method

.method public onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
invokevirtual net/hockeyapp/android/af/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
ldc "imageUri"
invokevirtual android/os/Bundle/getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/net/Uri
astore 1
aload 0
aload 0
aload 1
aload 1
invokevirtual android/net/Uri/getLastPathSegment()Ljava/lang/String;
invokespecial net/hockeyapp/android/af/a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
putfield net/hockeyapp/android/af/e Ljava/lang/String;
aload 0
invokevirtual net/hockeyapp/android/af/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/widthPixels I
istore 3
aload 0
invokevirtual net/hockeyapp/android/af/getResources()Landroid/content/res/Resources;
invokevirtual android/content/res/Resources/getDisplayMetrics()Landroid/util/DisplayMetrics;
getfield android/util/DisplayMetrics/heightPixels I
istore 4
iload 3
iload 4
if_icmple L0
iconst_0
istore 2
L1:
aload 0
invokevirtual net/hockeyapp/android/af/getContentResolver()Landroid/content/ContentResolver;
aload 1
invokestatic net/hockeyapp/android/f/k/a(Landroid/content/ContentResolver;Landroid/net/Uri;)I
istore 5
aload 0
iload 5
invokevirtual net/hockeyapp/android/af/setRequestedOrientation(I)V
iload 2
iload 5
if_icmpeq L2
ldc "HockeyApp"
ldc "Image loading skipped because activity will be destroyed for orientation change."
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
return
L0:
iconst_1
istore 2
goto L1
L2:
aload 0
new net/hockeyapp/android/f/k
dup
aload 0
aload 1
iload 3
iload 4
invokespecial net/hockeyapp/android/f/k/<init>(Landroid/content/Context;Landroid/net/Uri;II)V
putfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
new android/widget/LinearLayout
dup
aload 0
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 1
aload 1
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
bipush 17
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 1
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
new android/widget/LinearLayout
dup
aload 0
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 6
aload 6
new android/widget/LinearLayout$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/widget/LinearLayout$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 6
bipush 17
invokevirtual android/widget/LinearLayout/setGravity(I)V
aload 6
iconst_0
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 1
aload 6
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 6
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
aload 1
invokevirtual net/hockeyapp/android/af/setContentView(Landroid/view/View;)V
aload 0
sipush 1536
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
sipush 1000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
return
.limit locals 7
.limit stack 7
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
aload 0
aload 1
invokespecial android/app/Activity/onCreateOptionsMenu(Landroid/view/Menu;)Z
pop
aload 1
iconst_0
iconst_1
iconst_0
sipush 1537
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokeinterface android/view/Menu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
pop
aload 1
iconst_0
iconst_2
iconst_0
sipush 1538
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokeinterface android/view/Menu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
pop
aload 1
iconst_0
iconst_3
iconst_0
sipush 1539
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokeinterface android/view/Menu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
pop
iconst_1
ireturn
.limit locals 2
.limit stack 5
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/empty()Z
ifne L0
new net/hockeyapp/android/ag
dup
aload 0
invokespecial net/hockeyapp/android/ag/<init>(Lnet/hockeyapp/android/af;)V
astore 2
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
sipush 1540
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
sipush 1542
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
aload 2
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
sipush 1541
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
aload 2
invokevirtual android/app/AlertDialog$Builder/setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
invokevirtual android/app/AlertDialog$Builder/show()Landroid/app/AlertDialog;
pop
iconst_1
ireturn
L0:
aload 0
iload 1
aload 2
invokespecial android/app/Activity/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
iconst_1
istore 3
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
tableswitch 1
L0
L1
L2
default : L3
L3:
aload 0
aload 1
invokespecial android/app/Activity/onOptionsItemSelected(Landroid/view/MenuItem;)Z
istore 3
L4:
iload 3
ireturn
L0:
new java/io/File
dup
aload 0
invokevirtual net/hockeyapp/android/af/getCacheDir()Ljava/io/File;
ldc "HockeyApp"
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 4
aload 4
invokevirtual java/io/File/mkdir()Z
pop
new java/io/File
dup
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/af/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".jpg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
iconst_1
istore 2
L5:
aload 1
invokevirtual java/io/File/exists()Z
ifeq L6
new java/io/File
dup
aload 4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield net/hockeyapp/android/af/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "_"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ".jpg"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/io/File;Ljava/lang/String;)V
astore 1
iload 2
iconst_1
iadd
istore 2
goto L5
L6:
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
iconst_1
invokevirtual net/hockeyapp/android/f/k/setDrawingCacheEnabled(Z)V
new net/hockeyapp/android/ah
dup
aload 0
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
invokevirtual net/hockeyapp/android/f/k/getDrawingCache()Landroid/graphics/Bitmap;
invokespecial net/hockeyapp/android/ah/<init>(Lnet/hockeyapp/android/af;Landroid/graphics/Bitmap;)V
iconst_1
anewarray java/io/File
dup
iconst_0
aload 1
aastore
invokevirtual net/hockeyapp/android/ah/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 4
aload 4
ldc "imageUri"
aload 1
invokestatic android/net/Uri/fromFile(Ljava/io/File;)Landroid/net/Uri;
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 0
invokevirtual net/hockeyapp/android/af/getParent()Landroid/app/Activity;
ifnonnull L7
aload 0
iconst_m1
aload 4
invokevirtual net/hockeyapp/android/af/setResult(ILandroid/content/Intent;)V
L8:
aload 0
invokevirtual net/hockeyapp/android/af/finish()V
iconst_1
ireturn
L7:
aload 0
invokevirtual net/hockeyapp/android/af/getParent()Landroid/app/Activity;
iconst_m1
aload 4
invokevirtual android/app/Activity/setResult(ILandroid/content/Intent;)V
goto L8
L1:
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
astore 1
aload 1
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/empty()Z
ifne L4
aload 1
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/pop()Ljava/lang/Object;
pop
aload 1
invokevirtual net/hockeyapp/android/f/k/invalidate()V
iconst_1
ireturn
L2:
aload 0
getfield net/hockeyapp/android/af/d Lnet/hockeyapp/android/f/k;
astore 1
aload 1
getfield net/hockeyapp/android/f/k/a Ljava/util/Stack;
invokevirtual java/util/Stack/clear()V
aload 1
invokevirtual net/hockeyapp/android/f/k/invalidate()V
iconst_1
ireturn
.limit locals 5
.limit stack 5
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
aload 0
aload 1
invokespecial android/app/Activity/onPrepareOptionsMenu(Landroid/view/Menu;)Z
pop
iconst_1
ireturn
.limit locals 2
.limit stack 2
.end method
