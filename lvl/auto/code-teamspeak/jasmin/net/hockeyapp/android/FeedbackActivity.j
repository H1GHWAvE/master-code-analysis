.bytecode 50.0
.class public synchronized net/hockeyapp/android/FeedbackActivity
.super android/app/Activity
.implements android/view/View$OnClickListener
.implements net/hockeyapp/android/s

.field private static final 'a' I = 3


.field private 'A' Ljava/util/ArrayList;

.field private 'B' Z

.field private 'C' Z

.field private 'D' Ljava/lang/String;

.field private final 'b' I

.field private final 'c' I

.field private final 'd' I

.field private final 'e' I

.field private 'f' Landroid/content/Context;

.field private 'g' Landroid/widget/TextView;

.field private 'h' Landroid/widget/EditText;

.field private 'i' Landroid/widget/EditText;

.field private 'j' Landroid/widget/EditText;

.field private 'k' Landroid/widget/EditText;

.field private 'l' Landroid/widget/Button;

.field private 'm' Landroid/widget/Button;

.field private 'n' Landroid/widget/Button;

.field private 'o' Landroid/widget/Button;

.field private 'p' Landroid/widget/ScrollView;

.field private 'q' Landroid/widget/LinearLayout;

.field private 'r' Landroid/widget/ListView;

.field private 's' Lnet/hockeyapp/android/d/r;

.field private 't' Landroid/os/Handler;

.field private 'u' Lnet/hockeyapp/android/d/q;

.field private 'v' Landroid/os/Handler;

.field private 'w' Ljava/util/List;

.field private 'x' Ljava/lang/String;

.field private 'y' Lnet/hockeyapp/android/c/c;

.field private 'z' Lnet/hockeyapp/android/a/a;

.method public <init>()V
aload 0
invokespecial android/app/Activity/<init>()V
aload 0
iconst_0
putfield net/hockeyapp/android/FeedbackActivity/b I
aload 0
iconst_1
putfield net/hockeyapp/android/FeedbackActivity/c I
aload 0
iconst_2
putfield net/hockeyapp/android/FeedbackActivity/d I
aload 0
iconst_3
putfield net/hockeyapp/android/FeedbackActivity/e I
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
aload 0
aload 1
putfield net/hockeyapp/android/FeedbackActivity/A Ljava/util/ArrayList;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/a/a;)Lnet/hockeyapp/android/a/a;
aload 0
aload 1
putfield net/hockeyapp/android/FeedbackActivity/z Lnet/hockeyapp/android/a/a;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/c/c;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/y Lnet/hockeyapp/android/c/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/c;)Lnet/hockeyapp/android/c/c;
aload 0
aload 1
putfield net/hockeyapp/android/FeedbackActivity/y Lnet/hockeyapp/android/c/c;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Landroid/widget/EditText;I)V
aload 1
iload 2
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/widget/EditText/setError(Ljava/lang/CharSequence;)V
aload 0
iconst_1
invokevirtual net/hockeyapp/android/FeedbackActivity/a(Z)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
new net/hockeyapp/android/d/q
dup
aload 0
aload 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/v Landroid/os/Handler;
aload 2
invokespecial net/hockeyapp/android/d/q/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
putfield net/hockeyapp/android/FeedbackActivity/u Lnet/hockeyapp/android/d/q;
return
.limit locals 3
.limit stack 7
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
aload 0
new net/hockeyapp/android/d/r
dup
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
aload 1
aload 2
aload 3
aload 4
aload 5
aload 6
aload 7
aload 8
iload 9
invokespecial net/hockeyapp/android/d/r/<init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
putfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 10
.limit stack 13
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Ljava/lang/String;Ljava/lang/String;)V
aload 0
new net/hockeyapp/android/d/q
dup
aload 0
aload 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/v Landroid/os/Handler;
aload 2
invokespecial net/hockeyapp/android/d/q/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
putfield net/hockeyapp/android/FeedbackActivity/u Lnet/hockeyapp/android/d/q;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/u Lnet/hockeyapp/android/d/q;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 3
.limit stack 7
.end method

.method static synthetic a(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V
aload 0
new net/hockeyapp/android/q
dup
aload 0
aload 1
invokespecial net/hockeyapp/android/q/<init>(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V
invokevirtual net/hockeyapp/android/FeedbackActivity/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(Lnet/hockeyapp/android/c/h;)V
aload 0
new net/hockeyapp/android/q
dup
aload 0
aload 1
invokespecial net/hockeyapp/android/q/<init>(Lnet/hockeyapp/android/FeedbackActivity;Lnet/hockeyapp/android/c/h;)V
invokevirtual net/hockeyapp/android/FeedbackActivity/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 2
.limit stack 5
.end method

.method private a(I)Z
iload 1
iconst_2
if_icmpne L0
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 2
aload 2
ldc "*/*"
invokevirtual android/content/Intent/setType(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
ldc "android.intent.action.GET_CONTENT"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 2
sipush 1046
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokestatic android/content/Intent/createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
iconst_2
invokevirtual net/hockeyapp/android/FeedbackActivity/startActivityForResult(Landroid/content/Intent;I)V
iconst_1
ireturn
L0:
iload 1
iconst_1
if_icmpne L1
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 2
aload 2
ldc "image/*"
invokevirtual android/content/Intent/setType(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 2
ldc "android.intent.action.GET_CONTENT"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 2
sipush 1047
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokestatic android/content/Intent/createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
iconst_1
invokevirtual net/hockeyapp/android/FeedbackActivity/startActivityForResult(Landroid/content/Intent;I)V
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method private b()Landroid/view/ViewGroup;
new net/hockeyapp/android/f/i
dup
aload 0
invokespecial net/hockeyapp/android/f/i/<init>(Landroid/content/Context;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
aload 0
new net/hockeyapp/android/d/q
dup
aload 0
aload 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/v Landroid/os/Handler;
aload 2
invokespecial net/hockeyapp/android/d/q/<init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Ljava/lang/String;)V
putfield net/hockeyapp/android/FeedbackActivity/u Lnet/hockeyapp/android/d/q;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/u Lnet/hockeyapp/android/d/q;
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
.limit locals 3
.limit stack 7
.end method

.method static synthetic b(Lnet/hockeyapp/android/FeedbackActivity;)V
aload 0
new net/hockeyapp/android/r
dup
aload 0
invokespecial net/hockeyapp/android/r/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
invokevirtual net/hockeyapp/android/FeedbackActivity/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic c(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/content/Context;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c()V
return
.limit locals 0
.limit stack 0
.end method

.method private d()V
aload 0
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
aload 0
invokevirtual net/hockeyapp/android/e/n/a(Landroid/content/Context;)Ljava/lang/String;
putfield net/hockeyapp/android/FeedbackActivity/D Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/D Ljava/lang/String;
ifnull L0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/B Z
ifeq L1
L0:
aload 0
iconst_0
invokevirtual net/hockeyapp/android/FeedbackActivity/b(Z)V
return
L1:
aload 0
iconst_1
invokevirtual net/hockeyapp/android/FeedbackActivity/b(Z)V
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/x Ljava/lang/String;
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
aload 0
getfield net/hockeyapp/android/FeedbackActivity/D Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/t Landroid/os/Handler;
iconst_1
invokespecial net/hockeyapp/android/FeedbackActivity/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
return
.limit locals 1
.limit stack 10
.end method

.method static synthetic d(Lnet/hockeyapp/android/FeedbackActivity;)Z
aload 0
iconst_0
putfield net/hockeyapp/android/FeedbackActivity/B Z
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic e(Lnet/hockeyapp/android/FeedbackActivity;)Ljava/util/ArrayList;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/A Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
ifnull L0
aload 0
ldc "input_method"
invokevirtual net/hockeyapp/android/FeedbackActivity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/inputmethod/InputMethodManager
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
invokevirtual android/widget/EditText/getWindowToken()Landroid/os/IBinder;
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
pop
L0:
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic f(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/widget/TextView;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/g Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()V
aload 0
new net/hockeyapp/android/m
dup
aload 0
invokespecial net/hockeyapp/android/m/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
putfield net/hockeyapp/android/FeedbackActivity/t Landroid/os/Handler;
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic g(Lnet/hockeyapp/android/FeedbackActivity;)Lnet/hockeyapp/android/a/a;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/z Lnet/hockeyapp/android/a/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()V
aload 0
new net/hockeyapp/android/o
dup
aload 0
invokespecial net/hockeyapp/android/o/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
putfield net/hockeyapp/android/FeedbackActivity/v Landroid/os/Handler;
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic h(Lnet/hockeyapp/android/FeedbackActivity;)Landroid/widget/ListView;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/r Landroid/widget/ListView;
areturn
.limit locals 1
.limit stack 1
.end method

.method private h()V
aload 0
new net/hockeyapp/android/r
dup
aload 0
invokespecial net/hockeyapp/android/r/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
invokevirtual net/hockeyapp/android/FeedbackActivity/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method private i()V
aload 0
iconst_0
invokevirtual net/hockeyapp/android/FeedbackActivity/a(Z)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
ifnull L0
aload 0
ldc "input_method"
invokevirtual net/hockeyapp/android/FeedbackActivity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/inputmethod/InputMethodManager
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
invokevirtual android/widget/EditText/getWindowToken()Landroid/os/IBinder;
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
pop
L0:
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
invokevirtual net/hockeyapp/android/e/n/a(Landroid/content/Context;)Ljava/lang/String;
astore 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 2
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 3
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 4
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 5
aload 4
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setVisibility(I)V
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
sipush 1038
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L1:
invokestatic net/hockeyapp/android/t/b()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/c Lnet/hockeyapp/android/c/i;
if_acmpne L2
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L2
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
sipush 1041
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L2:
invokestatic net/hockeyapp/android/t/c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/c Lnet/hockeyapp/android/c/i;
if_acmpne L3
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L3
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
sipush 1042
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L3:
aload 5
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L4
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
sipush 1043
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L4:
invokestatic net/hockeyapp/android/t/c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/c Lnet/hockeyapp/android/c/i;
if_acmpne L5
aload 3
invokestatic net/hockeyapp/android/e/w/b(Ljava/lang/String;)Z
ifne L5
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
sipush 1039
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L5:
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
astore 6
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
astore 7
aload 7
ifnull L6
aload 6
aload 7
ldc "net.hockeyapp.android.prefs_name_email"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
aload 6
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ifnull L6
aload 6
aload 6
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
putfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
aload 2
ifnull L7
aload 3
ifnull L7
aload 4
ifnonnull L8
L7:
aload 6
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_name_email"
aconst_null
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
L9:
aload 6
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L6:
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast net/hockeyapp/android/f/a
invokevirtual net/hockeyapp/android/f/a/getAttachments()Ljava/util/ArrayList;
astore 6
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/x Ljava/lang/String;
aload 2
aload 3
aload 4
aload 5
aload 6
aload 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/t Landroid/os/Handler;
iconst_0
invokespecial net/hockeyapp/android/FeedbackActivity/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
return
L8:
aload 6
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_name_email"
ldc "%s|%s|%s"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
dup
iconst_2
aload 4
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
goto L9
.limit locals 8
.limit stack 10
.end method

.method public final synthetic a()Landroid/view/View;
aload 0
invokespecial net/hockeyapp/android/FeedbackActivity/b()Landroid/view/ViewGroup;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Z)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/l Landroid/widget/Button;
ifnull L0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/l Landroid/widget/Button;
iload 1
invokevirtual android/widget/Button/setEnabled(Z)V
L0:
return
.limit locals 2
.limit stack 2
.end method

.method protected final b(Z)V
aload 0
aload 0
ldc_w 131095
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/ScrollView
putfield net/hockeyapp/android/FeedbackActivity/p Landroid/widget/ScrollView;
aload 0
aload 0
ldc_w 131093
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/LinearLayout
putfield net/hockeyapp/android/FeedbackActivity/q Landroid/widget/LinearLayout;
aload 0
aload 0
ldc_w 131094
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/ListView
putfield net/hockeyapp/android/FeedbackActivity/r Landroid/widget/ListView;
iload 1
ifeq L0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/q Landroid/widget/LinearLayout;
iconst_0
invokevirtual android/widget/LinearLayout/setVisibility(I)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/p Landroid/widget/ScrollView;
bipush 8
invokevirtual android/widget/ScrollView/setVisibility(I)V
aload 0
aload 0
sipush 8192
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield net/hockeyapp/android/FeedbackActivity/g Landroid/widget/TextView;
aload 0
aload 0
ldc_w 131088
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield net/hockeyapp/android/FeedbackActivity/n Landroid/widget/Button;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/n Landroid/widget/Button;
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 0
ldc_w 131089
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield net/hockeyapp/android/FeedbackActivity/o Landroid/widget/Button;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/o Landroid/widget/Button;
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
L0:
aload 0
getfield net/hockeyapp/android/FeedbackActivity/q Landroid/widget/LinearLayout;
bipush 8
invokevirtual android/widget/LinearLayout/setVisibility(I)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/p Landroid/widget/ScrollView;
iconst_0
invokevirtual android/widget/ScrollView/setVisibility(I)V
aload 0
aload 0
sipush 8194
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
aload 0
aload 0
sipush 8196
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
aload 0
aload 0
sipush 8198
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
aload 0
aload 0
sipush 8200
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/C Z
ifne L1
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
astore 2
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
astore 3
aload 3
ifnonnull L2
aconst_null
astore 2
L3:
aload 2
ifnull L4
aload 2
ldc "\\|"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 2
aload 2
ifnull L5
aload 2
arraylength
iconst_2
if_icmplt L5
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
aload 2
iconst_0
aaload
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
aload 2
iconst_1
aaload
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 2
arraylength
iconst_3
if_icmplt L6
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
aload 2
iconst_2
aaload
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
invokevirtual android/widget/EditText/requestFocus()Z
pop
L5:
aload 0
iconst_1
putfield net/hockeyapp/android/FeedbackActivity/C Z
L1:
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
ldc ""
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
invokevirtual net/hockeyapp/android/e/n/a(Landroid/content/Context;)Ljava/lang/String;
ifnull L7
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
bipush 8
invokevirtual android/widget/EditText/setVisibility(I)V
L8:
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/view/ViewGroup
astore 2
aload 2
invokevirtual android/view/ViewGroup/removeAllViews()V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/w Ljava/util/List;
ifnull L9
aload 0
getfield net/hockeyapp/android/FeedbackActivity/w Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 3
L10:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L9
aload 2
new net/hockeyapp/android/f/b
dup
aload 0
aload 2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/net/Uri
invokespecial net/hockeyapp/android/f/b/<init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
goto L10
L2:
aload 2
aload 3
ldc "net.hockeyapp.android.prefs_name_email"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
aload 2
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ifnonnull L11
aconst_null
astore 2
goto L3
L11:
aload 2
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ldc "net.hockeyapp.android.prefs_key_name_email"
aconst_null
invokeinterface android/content/SharedPreferences/getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String; 2
astore 2
goto L3
L6:
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
invokevirtual android/widget/EditText/requestFocus()Z
pop
goto L5
L4:
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
ldc ""
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
ldc ""
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
ldc ""
invokevirtual android/widget/EditText/setText(Ljava/lang/CharSequence;)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
invokevirtual android/widget/EditText/requestFocus()Z
pop
goto L5
L7:
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setVisibility(I)V
goto L8
L9:
aload 0
aload 0
sipush 8208
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield net/hockeyapp/android/FeedbackActivity/m Landroid/widget/Button;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/m Landroid/widget/Button;
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/m Landroid/widget/Button;
invokevirtual net/hockeyapp/android/FeedbackActivity/registerForContextMenu(Landroid/view/View;)V
aload 0
aload 0
sipush 8201
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield net/hockeyapp/android/FeedbackActivity/l Landroid/widget/Button;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/l Landroid/widget/Button;
aload 0
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 4
.limit stack 6
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
.catch android/content/ActivityNotFoundException from L0 to L1 using L2
iload 2
iconst_m1
if_icmpeq L3
L4:
return
L3:
iload 1
iconst_2
if_icmpne L5
aload 3
invokevirtual android/content/Intent/getData()Landroid/net/Uri;
astore 3
aload 3
ifnull L4
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/view/ViewGroup
astore 4
aload 4
new net/hockeyapp/android/f/b
dup
aload 0
aload 4
aload 3
invokespecial net/hockeyapp/android/f/b/<init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
return
L5:
iload 1
iconst_1
if_icmpne L6
aload 3
invokevirtual android/content/Intent/getData()Landroid/net/Uri;
astore 3
aload 3
ifnull L4
L0:
new android/content/Intent
dup
aload 0
ldc net/hockeyapp/android/af
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 4
aload 4
ldc "imageUri"
aload 3
invokevirtual android/content/Intent/putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
pop
aload 0
aload 4
iconst_3
invokevirtual net/hockeyapp/android/FeedbackActivity/startActivityForResult(Landroid/content/Intent;I)V
L1:
return
L2:
astore 3
ldc "HockeyApp"
ldc "Paint activity not declared!"
aload 3
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
pop
return
L6:
iload 1
iconst_3
if_icmpne L4
aload 3
ldc "imageUri"
invokevirtual android/content/Intent/getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;
checkcast android/net/Uri
astore 3
aload 3
ifnull L4
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/view/ViewGroup
astore 4
aload 4
new net/hockeyapp/android/f/b
dup
aload 0
aload 4
aload 3
invokespecial net/hockeyapp/android/f/b/<init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
return
.limit locals 5
.limit stack 6
.end method

.method public onClick(Landroid/view/View;)V
aload 1
invokevirtual android/view/View/getId()I
lookupswitch
8201 : L0
8208 : L1
131088 : L2
131089 : L3
default : L4
L4:
return
L0:
aload 0
iconst_0
invokevirtual net/hockeyapp/android/FeedbackActivity/a(Z)V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
ifnull L5
aload 0
ldc "input_method"
invokevirtual net/hockeyapp/android/FeedbackActivity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/view/inputmethod/InputMethodManager
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
invokevirtual android/widget/EditText/getWindowToken()Landroid/os/IBinder;
iconst_0
invokevirtual android/view/inputmethod/InputMethodManager/hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
pop
L5:
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
invokevirtual net/hockeyapp/android/e/n/a(Landroid/content/Context;)Ljava/lang/String;
astore 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 2
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 3
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 4
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
invokevirtual android/widget/EditText/getText()Landroid/text/Editable;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
invokevirtual java/lang/String/trim()Ljava/lang/String;
astore 5
aload 4
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L6
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setVisibility(I)V
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/j Landroid/widget/EditText;
sipush 1038
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L6:
invokestatic net/hockeyapp/android/t/b()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/c Lnet/hockeyapp/android/c/i;
if_acmpne L7
aload 2
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L7
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/h Landroid/widget/EditText;
sipush 1041
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L7:
invokestatic net/hockeyapp/android/t/c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/c Lnet/hockeyapp/android/c/i;
if_acmpne L8
aload 3
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L8
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
sipush 1042
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L8:
aload 5
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
ifeq L9
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/k Landroid/widget/EditText;
sipush 1043
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L9:
invokestatic net/hockeyapp/android/t/c()Lnet/hockeyapp/android/c/i;
getstatic net/hockeyapp/android/c/i/c Lnet/hockeyapp/android/c/i;
if_acmpne L10
aload 3
invokestatic net/hockeyapp/android/e/w/b(Ljava/lang/String;)Z
ifne L10
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/i Landroid/widget/EditText;
sipush 1039
invokespecial net/hockeyapp/android/FeedbackActivity/a(Landroid/widget/EditText;I)V
return
L10:
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
astore 6
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
astore 7
aload 7
ifnull L11
aload 6
aload 7
ldc "net.hockeyapp.android.prefs_name_email"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
putfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
aload 6
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
ifnull L11
aload 6
aload 6
getfield net/hockeyapp/android/e/n/a Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
putfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
aload 2
ifnull L12
aload 3
ifnull L12
aload 4
ifnonnull L13
L12:
aload 6
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_name_email"
aconst_null
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
L14:
aload 6
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
invokestatic net/hockeyapp/android/e/n/a(Landroid/content/SharedPreferences$Editor;)V
L11:
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast net/hockeyapp/android/f/a
invokevirtual net/hockeyapp/android/f/a/getAttachments()Ljava/util/ArrayList;
astore 6
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/x Ljava/lang/String;
aload 2
aload 3
aload 4
aload 5
aload 6
aload 1
aload 0
getfield net/hockeyapp/android/FeedbackActivity/t Landroid/os/Handler;
iconst_0
invokespecial net/hockeyapp/android/FeedbackActivity/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
return
L13:
aload 6
getfield net/hockeyapp/android/e/n/b Landroid/content/SharedPreferences$Editor;
ldc "net.hockeyapp.android.prefs_key_name_email"
ldc "%s|%s|%s"
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 2
aastore
dup
iconst_1
aload 3
aastore
dup
iconst_2
aload 4
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokeinterface android/content/SharedPreferences$Editor/putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor; 2
pop
goto L14
L1:
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/view/ViewGroup
invokevirtual android/view/ViewGroup/getChildCount()I
iconst_3
if_icmplt L15
aload 0
ldc ""
iconst_1
anewarray java/lang/Object
dup
iconst_0
iconst_3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
sipush 1000
invokestatic android/widget/Toast/makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
invokevirtual android/widget/Toast/show()V
return
L15:
aload 0
aload 1
invokevirtual net/hockeyapp/android/FeedbackActivity/openContextMenu(Landroid/view/View;)V
return
L2:
aload 0
iconst_0
invokevirtual net/hockeyapp/android/FeedbackActivity/b(Z)V
aload 0
iconst_1
putfield net/hockeyapp/android/FeedbackActivity/B Z
return
L3:
aload 0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/x Ljava/lang/String;
aconst_null
aconst_null
aconst_null
aconst_null
aconst_null
getstatic net/hockeyapp/android/e/p/a Lnet/hockeyapp/android/e/n;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
invokevirtual net/hockeyapp/android/e/n/a(Landroid/content/Context;)Ljava/lang/String;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/t Landroid/os/Handler;
iconst_1
invokespecial net/hockeyapp/android/FeedbackActivity/a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
return
.limit locals 8
.limit stack 10
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
tableswitch 1
L0
L0
default : L1
L1:
aload 0
aload 1
invokespecial android/app/Activity/onContextItemSelected(Landroid/view/MenuItem;)Z
ireturn
L0:
aload 1
invokeinterface android/view/MenuItem/getItemId()I 0
istore 2
iload 2
iconst_2
if_icmpne L2
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 1
aload 1
ldc "*/*"
invokevirtual android/content/Intent/setType(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.action.GET_CONTENT"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 1
sipush 1046
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokestatic android/content/Intent/createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
iconst_2
invokevirtual net/hockeyapp/android/FeedbackActivity/startActivityForResult(Landroid/content/Intent;I)V
iconst_1
ireturn
L2:
iload 2
iconst_1
if_icmpne L3
new android/content/Intent
dup
invokespecial android/content/Intent/<init>()V
astore 1
aload 1
ldc "image/*"
invokevirtual android/content/Intent/setType(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 1
ldc "android.intent.action.GET_CONTENT"
invokevirtual android/content/Intent/setAction(Ljava/lang/String;)Landroid/content/Intent;
pop
aload 0
aload 1
sipush 1047
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokestatic android/content/Intent/createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;
iconst_1
invokevirtual net/hockeyapp/android/FeedbackActivity/startActivityForResult(Landroid/content/Intent;I)V
iconst_1
ireturn
L3:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public onCreate(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/app/Activity/onCreate(Landroid/os/Bundle;)V
aload 0
aload 0
invokespecial net/hockeyapp/android/FeedbackActivity/b()Landroid/view/ViewGroup;
invokevirtual net/hockeyapp/android/FeedbackActivity/setContentView(Landroid/view/View;)V
aload 0
sipush 1035
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual net/hockeyapp/android/FeedbackActivity/setTitle(Ljava/lang/CharSequence;)V
aload 0
aload 0
putfield net/hockeyapp/android/FeedbackActivity/f Landroid/content/Context;
aload 0
invokevirtual net/hockeyapp/android/FeedbackActivity/getIntent()Landroid/content/Intent;
invokevirtual android/content/Intent/getExtras()Landroid/os/Bundle;
astore 4
aload 4
ifnull L0
aload 0
aload 4
ldc "url"
invokevirtual android/os/Bundle/getString(Ljava/lang/String;)Ljava/lang/String;
putfield net/hockeyapp/android/FeedbackActivity/x Ljava/lang/String;
aload 4
ldc "initialAttachments"
invokevirtual android/os/Bundle/getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;
astore 4
aload 4
ifnull L0
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield net/hockeyapp/android/FeedbackActivity/w Ljava/util/List;
aload 4
arraylength
istore 3
iconst_0
istore 2
L1:
iload 2
iload 3
if_icmpge L0
aload 4
iload 2
aaload
astore 5
aload 0
getfield net/hockeyapp/android/FeedbackActivity/w Ljava/util/List;
aload 5
checkcast android/net/Uri
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 1
ifnull L2
aload 0
aload 1
ldc "feedbackViewInitialized"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
putfield net/hockeyapp/android/FeedbackActivity/C Z
aload 0
aload 1
ldc "inSendFeedback"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
putfield net/hockeyapp/android/FeedbackActivity/B Z
L3:
aload 0
ldc "notification"
invokevirtual net/hockeyapp/android/FeedbackActivity/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/app/NotificationManager
iconst_2
invokevirtual android/app/NotificationManager/cancel(I)V
aload 0
new net/hockeyapp/android/m
dup
aload 0
invokespecial net/hockeyapp/android/m/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
putfield net/hockeyapp/android/FeedbackActivity/t Landroid/os/Handler;
aload 0
new net/hockeyapp/android/o
dup
aload 0
invokespecial net/hockeyapp/android/o/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
putfield net/hockeyapp/android/FeedbackActivity/v Landroid/os/Handler;
aload 0
invokespecial net/hockeyapp/android/FeedbackActivity/d()V
return
L2:
aload 0
iconst_0
putfield net/hockeyapp/android/FeedbackActivity/B Z
aload 0
iconst_0
putfield net/hockeyapp/android/FeedbackActivity/C Z
goto L3
.limit locals 6
.limit stack 4
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
aload 0
aload 1
aload 2
aload 3
invokespecial android/app/Activity/onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
aload 1
iconst_0
iconst_2
iconst_0
sipush 1044
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokeinterface android/view/ContextMenu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
pop
aload 1
iconst_0
iconst_1
iconst_0
sipush 1045
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokeinterface android/view/ContextMenu/add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem; 4
pop
return
.limit locals 4
.limit stack 5
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
iload 1
tableswitch 0
L0
default : L1
L1:
aconst_null
areturn
L0:
new android/app/AlertDialog$Builder
dup
aload 0
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
sipush 2051
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
iconst_0
invokevirtual android/app/AlertDialog$Builder/setCancelable(Z)Landroid/app/AlertDialog$Builder;
sipush 2050
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog$Builder/setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
ldc_w 17301543
invokevirtual android/app/AlertDialog$Builder/setIcon(I)Landroid/app/AlertDialog$Builder;
sipush 2048
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
new net/hockeyapp/android/l
dup
aload 0
invokespecial net/hockeyapp/android/l/<init>(Lnet/hockeyapp/android/FeedbackActivity;)V
invokevirtual android/app/AlertDialog$Builder/setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
areturn
.limit locals 2
.limit stack 5
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
iload 1
iconst_4
if_icmpne L0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/B Z
ifeq L1
aload 0
iconst_0
putfield net/hockeyapp/android/FeedbackActivity/B Z
aload 0
invokespecial net/hockeyapp/android/FeedbackActivity/d()V
L2:
iconst_1
ireturn
L1:
aload 0
invokevirtual net/hockeyapp/android/FeedbackActivity/finish()V
goto L2
L0:
aload 0
iload 1
aload 2
invokespecial android/app/Activity/onKeyDown(ILandroid/view/KeyEvent;)Z
ireturn
.limit locals 3
.limit stack 3
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
iload 1
tableswitch 0
L0
default : L1
L1:
return
L0:
aload 2
checkcast android/app/AlertDialog
astore 2
aload 0
getfield net/hockeyapp/android/FeedbackActivity/y Lnet/hockeyapp/android/c/c;
ifnull L2
aload 2
aload 0
getfield net/hockeyapp/android/FeedbackActivity/y Lnet/hockeyapp/android/c/c;
getfield net/hockeyapp/android/c/c/a Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
return
L2:
aload 2
sipush 1040
invokestatic net/hockeyapp/android/aj/a(I)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 2
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
aload 1
ifnull L0
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast android/view/ViewGroup
astore 2
aload 1
ldc "attachments"
invokevirtual android/os/Bundle/getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/iterator()Ljava/util/Iterator;
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 2
new net/hockeyapp/android/f/b
dup
aload 0
aload 2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast android/net/Uri
invokespecial net/hockeyapp/android/f/b/<init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/net/Uri;)V
invokevirtual android/view/ViewGroup/addView(Landroid/view/View;)V
goto L1
L2:
aload 0
aload 1
ldc "feedbackViewInitialized"
invokevirtual android/os/Bundle/getBoolean(Ljava/lang/String;)Z
putfield net/hockeyapp/android/FeedbackActivity/C Z
L0:
aload 0
aload 1
invokespecial android/app/Activity/onRestoreInstanceState(Landroid/os/Bundle;)V
return
.limit locals 4
.limit stack 6
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
aload 0
getfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
ifnull L0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
invokevirtual net/hockeyapp/android/d/r/a()V
L0:
aload 0
getfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
aload 1
ldc "attachments"
aload 0
sipush 8209
invokevirtual net/hockeyapp/android/FeedbackActivity/findViewById(I)Landroid/view/View;
checkcast net/hockeyapp/android/f/a
invokevirtual net/hockeyapp/android/f/a/getAttachments()Ljava/util/ArrayList;
invokevirtual android/os/Bundle/putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
aload 1
ldc "feedbackViewInitialized"
aload 0
getfield net/hockeyapp/android/FeedbackActivity/C Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
aload 1
ldc "inSendFeedback"
aload 0
getfield net/hockeyapp/android/FeedbackActivity/B Z
invokevirtual android/os/Bundle/putBoolean(Ljava/lang/String;Z)V
aload 0
aload 1
invokespecial android/app/Activity/onSaveInstanceState(Landroid/os/Bundle;)V
return
.limit locals 2
.limit stack 4
.end method

.method protected onStop()V
aload 0
invokespecial android/app/Activity/onStop()V
aload 0
getfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
ifnull L0
aload 0
getfield net/hockeyapp/android/FeedbackActivity/s Lnet/hockeyapp/android/d/r;
invokevirtual net/hockeyapp/android/d/r/a()V
L0:
return
.limit locals 1
.limit stack 1
.end method
