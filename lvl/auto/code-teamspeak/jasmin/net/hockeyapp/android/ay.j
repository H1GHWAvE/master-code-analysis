.bytecode 50.0
.class public final synchronized net/hockeyapp/android/ay
.super java/lang/Object

.field private static 'a' Lnet/hockeyapp/android/d/g;

.field private static 'b' Lnet/hockeyapp/android/az;

.method static <clinit>()V
aconst_null
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aconst_null
putstatic net/hockeyapp/android/ay/b Lnet/hockeyapp/android/az;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()Lnet/hockeyapp/android/az;
getstatic net/hockeyapp/android/ay/b Lnet/hockeyapp/android/az;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aconst_null
iconst_1
invokestatic net/hockeyapp/android/ay/a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
return
.limit locals 2
.limit stack 5
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
aload 0
aload 1
aload 2
aload 3
iconst_1
invokestatic net/hockeyapp/android/ay/a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
return
.limit locals 4
.limit stack 5
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
aload 2
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 3
putstatic net/hockeyapp/android/ay/b Lnet/hockeyapp/android/az;
new java/lang/ref/WeakReference
dup
aload 0
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 0
invokestatic net/hockeyapp/android/e/w/a()Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L0
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/app/Activity
astore 6
aload 6
ifnull L1
aload 6
invokevirtual android/app/Activity/getFragmentManager()Landroid/app/FragmentManager;
ldc "hockey_update_dialog"
invokevirtual android/app/FragmentManager/findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
ifnull L2
iconst_1
istore 5
L3:
iload 5
ifeq L0
L4:
return
L2:
iconst_0
istore 5
goto L3
L1:
iconst_0
istore 5
goto L3
L0:
aload 0
invokestatic net/hockeyapp/android/ay/a(Ljava/lang/ref/WeakReference;)Z
ifne L4
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
ifnull L5
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
invokevirtual net/hockeyapp/android/d/g/getStatus()Landroid/os/AsyncTask$Status;
getstatic android/os/AsyncTask$Status/FINISHED Landroid/os/AsyncTask$Status;
if_acmpne L6
L5:
new net/hockeyapp/android/d/h
dup
aload 0
aload 1
aload 2
aload 3
iload 4
invokespecial net/hockeyapp/android/d/h/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
astore 0
aload 0
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
L6:
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/ref/WeakReference;)V
return
.limit locals 7
.limit stack 7
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aload 2
iconst_1
invokestatic net/hockeyapp/android/ay/a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Landroid/app/Activity;Ljava/lang/String;Z)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aconst_null
iload 2
invokestatic net/hockeyapp/android/ay/a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
return
.limit locals 3
.limit stack 5
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
aload 2
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
astore 2
aload 3
putstatic net/hockeyapp/android/ay/b Lnet/hockeyapp/android/az;
new java/lang/ref/WeakReference
dup
aload 0
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 0
aload 0
invokestatic net/hockeyapp/android/ay/a(Ljava/lang/ref/WeakReference;)Z
ifne L0
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
ifnull L1
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
invokevirtual net/hockeyapp/android/d/g/getStatus()Landroid/os/AsyncTask$Status;
getstatic android/os/AsyncTask$Status/FINISHED Landroid/os/AsyncTask$Status;
if_acmpne L2
L1:
new net/hockeyapp/android/d/g
dup
aload 0
aload 1
aload 2
aload 3
invokespecial net/hockeyapp/android/d/g/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
astore 0
aload 0
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
L0:
return
L2:
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/ref/WeakReference;)V
return
.limit locals 4
.limit stack 6
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
aload 1
invokestatic net/hockeyapp/android/e/w/c(Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 2
putstatic net/hockeyapp/android/ay/b Lnet/hockeyapp/android/az;
new java/lang/ref/WeakReference
dup
aload 0
invokespecial java/lang/ref/WeakReference/<init>(Ljava/lang/Object;)V
astore 0
aload 0
invokestatic net/hockeyapp/android/ay/a(Ljava/lang/ref/WeakReference;)Z
ifne L0
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
ifnull L1
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
invokevirtual net/hockeyapp/android/d/g/getStatus()Landroid/os/AsyncTask$Status;
getstatic android/os/AsyncTask$Status/FINISHED Landroid/os/AsyncTask$Status;
if_acmpne L2
L1:
new net/hockeyapp/android/d/g
dup
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aload 2
invokespecial net/hockeyapp/android/d/g/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
astore 0
aload 0
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
L0:
return
L2:
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/ref/WeakReference;)V
return
.limit locals 3
.limit stack 6
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
ifnull L0
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
invokevirtual net/hockeyapp/android/d/g/getStatus()Landroid/os/AsyncTask$Status;
getstatic android/os/AsyncTask$Status/FINISHED Landroid/os/AsyncTask$Status;
if_acmpne L1
L0:
new net/hockeyapp/android/d/g
dup
aload 0
aload 1
aload 2
aload 3
invokespecial net/hockeyapp/android/d/g/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;)V
astore 0
aload 0
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
L1:
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/ref/WeakReference;)V
return
.limit locals 4
.limit stack 6
.end method

.method private static a(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
ifnull L0
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
invokevirtual net/hockeyapp/android/d/g/getStatus()Landroid/os/AsyncTask$Status;
getstatic android/os/AsyncTask$Status/FINISHED Landroid/os/AsyncTask$Status;
if_acmpne L1
L0:
new net/hockeyapp/android/d/h
dup
aload 0
aload 1
aload 2
aload 3
iload 4
invokespecial net/hockeyapp/android/d/h/<init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
astore 0
aload 0
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokestatic net/hockeyapp/android/e/a/a(Landroid/os/AsyncTask;)V
return
L1:
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
aload 0
invokevirtual net/hockeyapp/android/d/g/a(Ljava/lang/ref/WeakReference;)V
return
.limit locals 5
.limit stack 7
.end method

.method private static a(Ljava/lang/ref/WeakReference;)Z
.catch java/lang/Throwable from L0 to L1 using L2
iconst_0
istore 1
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/content/Context
astore 0
aload 0
ifnull L3
L0:
aload 0
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 0
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
invokevirtual android/content/pm/PackageManager/getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;
invokestatic android/text/TextUtils/isEmpty(Ljava/lang/CharSequence;)Z
istore 1
L1:
iload 1
ifne L4
iconst_1
istore 1
L3:
iload 1
ireturn
L4:
iconst_0
istore 1
goto L3
L2:
astore 0
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static b()V
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
ifnull L0
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
iconst_1
invokevirtual net/hockeyapp/android/d/g/cancel(Z)Z
pop
getstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
invokevirtual net/hockeyapp/android/d/g/a()V
aconst_null
putstatic net/hockeyapp/android/ay/a Lnet/hockeyapp/android/d/g;
L0:
aconst_null
putstatic net/hockeyapp/android/ay/b Lnet/hockeyapp/android/az;
return
.limit locals 0
.limit stack 2
.end method

.method private static b(Landroid/app/Activity;Ljava/lang/String;)V
aload 0
ldc "https://sdk.hockeyapp.net/"
aload 1
aconst_null
iconst_1
invokestatic net/hockeyapp/android/ay/a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lnet/hockeyapp/android/az;Z)V
return
.limit locals 2
.limit stack 5
.end method

.method private static b(Ljava/lang/ref/WeakReference;)V
aload 0
ifnull L0
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/app/Activity
astore 0
aload 0
ifnull L0
aload 0
invokevirtual android/app/Activity/finish()V
new android/content/Intent
dup
aload 0
ldc net/hockeyapp/android/k
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
astore 1
aload 1
ldc_w 335544320
invokevirtual android/content/Intent/addFlags(I)Landroid/content/Intent;
pop
aload 0
aload 1
invokevirtual android/app/Activity/startActivity(Landroid/content/Intent;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method private static c()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method

.method private static c(Ljava/lang/ref/WeakReference;)Z
.annotation invisible Landroid/annotation/TargetApi;
value I = 11
.end annotation
aload 0
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast android/app/Activity
astore 0
aload 0
ifnull L0
aload 0
invokevirtual android/app/Activity/getFragmentManager()Landroid/app/FragmentManager;
ldc "hockey_update_dialog"
invokevirtual android/app/FragmentManager/findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;
ifnull L1
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private static d()Z
iconst_0
ireturn
.limit locals 0
.limit stack 1
.end method
