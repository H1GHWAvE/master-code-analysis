.bytecode 50.0
.class public synchronized org/xbill/DNS/UNKRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -4193583311594626915L


.field private 'data' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public getData()[B
aload 0
getfield org/xbill/DNS/UNKRecord/data [B
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/UNKRecord
dup
invokespecial org/xbill/DNS/UNKRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
ldc "invalid unknown RR encoding"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/UNKRecord/data [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/UNKRecord/data [B
invokestatic org/xbill/DNS/UNKRecord/unknownToString([B)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/UNKRecord/data [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
