.bytecode 50.0
.class public synchronized org/xbill/DNS/Cache
.super java/lang/Object

.field private static final 'defaultMaxEntries' I = 50000


.field private 'data' Lorg/xbill/DNS/Cache$CacheMap;

.field private 'dclass' I

.field private 'maxcache' I

.field private 'maxncache' I

.method public <init>()V
aload 0
iconst_1
invokespecial org/xbill/DNS/Cache/<init>(I)V
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield org/xbill/DNS/Cache/maxncache I
aload 0
iconst_m1
putfield org/xbill/DNS/Cache/maxcache I
aload 0
iload 1
putfield org/xbill/DNS/Cache/dclass I
aload 0
new org/xbill/DNS/Cache$CacheMap
dup
ldc_w 50000
invokespecial org/xbill/DNS/Cache$CacheMap/<init>(I)V
putfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
return
.limit locals 2
.limit stack 4
.end method

.method public <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_m1
putfield org/xbill/DNS/Cache/maxncache I
aload 0
iconst_m1
putfield org/xbill/DNS/Cache/maxcache I
aload 0
new org/xbill/DNS/Cache$CacheMap
dup
ldc_w 50000
invokespecial org/xbill/DNS/Cache$CacheMap/<init>(I)V
putfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
new org/xbill/DNS/Master
dup
aload 1
invokespecial org/xbill/DNS/Master/<init>(Ljava/lang/String;)V
astore 1
L0:
aload 1
invokevirtual org/xbill/DNS/Master/nextRecord()Lorg/xbill/DNS/Record;
astore 2
aload 2
ifnull L1
aload 0
aload 2
iconst_0
aload 1
invokevirtual org/xbill/DNS/Cache/addRecord(Lorg/xbill/DNS/Record;ILjava/lang/Object;)V
goto L0
L1:
return
.limit locals 3
.limit stack 4
.end method

.method static access$000(JJ)I
lload 0
lload 2
invokestatic org/xbill/DNS/Cache/limitExpire(JJ)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private addElement(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Cache$Element;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/get(Ljava/lang/Object;)Ljava/lang/Object;
astore 5
L1:
aload 5
ifnonnull L5
L3:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
aload 2
invokevirtual org/xbill/DNS/Cache$CacheMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L4:
aload 0
monitorexit
return
L5:
aload 2
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
istore 4
aload 5
instanceof java/util/List
ifeq L11
aload 5
checkcast java/util/List
astore 1
L6:
iconst_0
istore 3
L7:
iload 3
aload 1
invokeinterface java/util/List/size()I 0
if_icmpge L9
aload 1
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Cache$Element
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
iload 4
if_icmpne L15
aload 1
iload 3
aload 2
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
L8:
goto L4
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L15:
iload 3
iconst_1
iadd
istore 3
goto L7
L9:
aload 1
aload 2
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L10:
goto L4
L11:
aload 5
checkcast org/xbill/DNS/Cache$Element
astore 5
aload 5
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
iload 4
if_icmpne L13
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
aload 2
invokevirtual org/xbill/DNS/Cache$CacheMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L12:
goto L4
L13:
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
astore 6
aload 6
aload 5
invokevirtual java/util/LinkedList/add(Ljava/lang/Object;)Z
pop
aload 6
aload 2
invokevirtual java/util/LinkedList/add(Ljava/lang/Object;)Z
pop
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
aload 6
invokevirtual org/xbill/DNS/Cache$CacheMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
L14:
goto L4
.limit locals 7
.limit stack 3
.end method

.method private allElements(Ljava/lang/Object;)[Lorg/xbill/DNS/Cache$Element;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 1
instanceof java/util/List
ifeq L3
aload 1
checkcast java/util/List
astore 1
aload 1
aload 1
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Cache$Element
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Cache$Element;
checkcast [Lorg/xbill/DNS/Cache$Element;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L3:
aload 1
checkcast org/xbill/DNS/Cache$Element
astore 2
iconst_1
anewarray org/xbill/DNS/Cache$Element
astore 1
L4:
aload 1
iconst_0
aload 2
aastore
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 3
.limit stack 3
.end method

.method private exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/get(Ljava/lang/Object;)Ljava/lang/Object;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method private findElement(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Cache$Element;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
aload 1
invokespecial org/xbill/DNS/Cache/exactName(Lorg/xbill/DNS/Name;)Ljava/lang/Object;
astore 4
L1:
aload 4
ifnonnull L3
aconst_null
astore 1
L5:
aload 0
monitorexit
aload 1
areturn
L3:
aload 0
aload 1
aload 4
iload 2
iload 3
invokespecial org/xbill/DNS/Cache/oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
astore 1
L4:
goto L5
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 5
.limit stack 5
.end method

.method private findRecords(Lorg/xbill/DNS/Name;II)[Lorg/xbill/DNS/RRset;
aload 0
aload 1
iload 2
iload 3
invokevirtual org/xbill/DNS/Cache/lookupRecords(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;
astore 1
aload 1
invokevirtual org/xbill/DNS/SetResponse/isSuccessful()Z
ifeq L0
aload 1
invokevirtual org/xbill/DNS/SetResponse/answers()[Lorg/xbill/DNS/RRset;
areturn
L0:
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method private final getCred(IZ)I
iload 1
iconst_1
if_icmpne L0
iload 2
ifeq L1
L2:
iconst_4
ireturn
L1:
iconst_3
ireturn
L0:
iload 1
iconst_2
if_icmpne L3
iload 2
ifne L2
iconst_3
ireturn
L3:
iload 1
iconst_3
if_icmpne L4
iconst_1
ireturn
L4:
new java/lang/IllegalArgumentException
dup
ldc "getCred: invalid section"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method private static limitExpire(JJ)I
lload 0
lstore 4
lload 2
lconst_0
lcmp
iflt L0
lload 0
lstore 4
lload 2
lload 0
lcmp
ifge L0
lload 2
lstore 4
L0:
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 1000L
ldiv
lload 4
ladd
lstore 0
lload 0
lconst_0
lcmp
iflt L1
lload 0
ldc2_w 2147483647L
lcmp
ifle L2
L1:
ldc_w 2147483647
ireturn
L2:
lload 0
l2i
ireturn
.limit locals 6
.limit stack 4
.end method

.method private static markAdditional(Lorg/xbill/DNS/RRset;Ljava/util/Set;)V
aload 0
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
invokevirtual org/xbill/DNS/Record/getAdditionalName()Lorg/xbill/DNS/Name;
ifnonnull L0
L1:
return
L0:
aload 0
invokevirtual org/xbill/DNS/RRset/rrs()Ljava/util/Iterator;
astore 0
L2:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast org/xbill/DNS/Record
invokevirtual org/xbill/DNS/Record/getAdditionalName()Lorg/xbill/DNS/Name;
astore 2
aload 2
ifnull L2
aload 1
aload 2
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
goto L2
.limit locals 3
.limit stack 2
.end method

.method private oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
.catch all from L4 to L5 using L1
.catch all from L6 to L7 using L1
.catch all from L8 to L9 using L1
.catch all from L10 to L11 using L1
aload 0
monitorenter
iload 3
sipush 255
if_icmpne L2
L0:
new java/lang/IllegalArgumentException
dup
ldc "oneElement(ANY)"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
monitorexit
aload 1
athrow
L2:
aload 2
instanceof java/util/List
ifeq L6
aload 2
checkcast java/util/List
astore 7
L3:
iconst_0
istore 5
L4:
iload 5
aload 7
invokeinterface java/util/List/size()I 0
if_icmpge L12
aload 7
iload 5
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Cache$Element
astore 2
aload 2
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
istore 6
L5:
iload 6
iload 3
if_icmpne L13
L14:
aload 2
ifnonnull L8
aconst_null
astore 2
L15:
aload 0
monitorexit
aload 2
areturn
L13:
iload 5
iconst_1
iadd
istore 5
goto L4
L12:
aconst_null
astore 2
goto L14
L6:
aload 2
checkcast org/xbill/DNS/Cache$Element
astore 2
aload 2
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
iload 3
if_icmpne L16
L7:
goto L14
L8:
aload 2
invokeinterface org/xbill/DNS/Cache$Element/expired()Z 0
ifeq L10
aload 0
aload 1
iload 3
invokespecial org/xbill/DNS/Cache/removeElement(Lorg/xbill/DNS/Name;I)V
L9:
aconst_null
astore 2
goto L15
L10:
aload 2
iload 4
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
istore 3
L11:
iload 3
ifge L15
aconst_null
astore 2
goto L15
L16:
aconst_null
astore 2
goto L14
.limit locals 8
.limit stack 3
.end method

.method private removeElement(Lorg/xbill/DNS/Name;I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/get(Ljava/lang/Object;)Ljava/lang/Object;
astore 4
L1:
aload 4
ifnonnull L3
L9:
aload 0
monitorexit
return
L3:
aload 4
instanceof java/util/List
ifeq L7
aload 4
checkcast java/util/List
astore 4
L4:
iconst_0
istore 3
L5:
iload 3
aload 4
invokeinterface java/util/List/size()I 0
if_icmpge L9
aload 4
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Cache$Element
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
iload 2
if_icmpne L10
aload 4
iload 3
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
aload 4
invokeinterface java/util/List/size()I 0
ifne L9
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L6:
goto L9
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L10:
iload 3
iconst_1
iadd
istore 3
goto L5
L7:
aload 4
checkcast org/xbill/DNS/Cache$Element
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
iload 2
if_icmpne L9
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L8:
goto L9
.limit locals 5
.limit stack 2
.end method

.method private removeName(Lorg/xbill/DNS/Name;)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method public addMessage(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/SetResponse;
.catch org/xbill/DNS/NameTooLongException from L0 to L1 using L2
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
iconst_5
invokevirtual org/xbill/DNS/Header/getFlag(I)Z
istore 9
aload 1
invokevirtual org/xbill/DNS/Message/getQuestion()Lorg/xbill/DNS/Record;
astore 11
aload 1
invokevirtual org/xbill/DNS/Message/getHeader()Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getRcode()I
istore 5
iconst_0
istore 2
ldc "verbosecache"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
istore 10
iload 5
ifeq L3
iload 5
iconst_3
if_icmpne L4
L3:
aload 11
ifnonnull L5
L4:
aconst_null
astore 1
L6:
aload 1
areturn
L5:
aload 11
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 14
aload 11
invokevirtual org/xbill/DNS/Record/getType()I
istore 4
aload 11
invokevirtual org/xbill/DNS/Record/getDClass()I
istore 6
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 17
aload 1
iconst_1
invokevirtual org/xbill/DNS/Message/getSectionRRsets(I)[Lorg/xbill/DNS/RRset;
astore 15
iconst_0
istore 3
aload 14
astore 12
aconst_null
astore 11
L7:
iload 3
aload 15
arraylength
if_icmpge L8
aload 15
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getDClass()I
iload 6
if_icmpne L9
aload 15
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getType()I
istore 7
aload 15
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
astore 13
aload 0
iconst_1
iload 9
invokespecial org/xbill/DNS/Cache/getCred(IZ)I
istore 8
iload 7
iload 4
if_icmpeq L10
iload 4
sipush 255
if_icmpne L11
L10:
aload 13
aload 12
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L11
aload 0
aload 15
iload 3
aaload
iload 8
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
iconst_1
istore 2
aload 11
astore 13
aload 12
aload 14
if_acmpne L12
aload 11
astore 13
aload 11
ifnonnull L13
new org/xbill/DNS/SetResponse
dup
bipush 6
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 13
L13:
aload 13
aload 15
iload 3
aaload
invokevirtual org/xbill/DNS/SetResponse/addRRset(Lorg/xbill/DNS/RRset;)V
L12:
aload 15
iload 3
aaload
aload 17
invokestatic org/xbill/DNS/Cache/markAdditional(Lorg/xbill/DNS/RRset;Ljava/util/Set;)V
aload 12
astore 11
aload 13
astore 12
L14:
iload 3
iconst_1
iadd
istore 3
aload 11
astore 13
aload 12
astore 11
aload 13
astore 12
goto L7
L11:
iload 7
iconst_5
if_icmpne L15
aload 13
aload 12
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L15
aload 0
aload 15
iload 3
aaload
iload 8
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
aload 12
aload 14
if_acmpne L16
new org/xbill/DNS/SetResponse
dup
iconst_4
aload 15
iload 3
aaload
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 11
L17:
aload 15
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
checkcast org/xbill/DNS/CNAMERecord
invokevirtual org/xbill/DNS/CNAMERecord/getTarget()Lorg/xbill/DNS/Name;
astore 13
aload 11
astore 12
aload 13
astore 11
goto L14
L15:
iload 7
bipush 39
if_icmpne L9
aload 12
aload 13
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifeq L9
aload 0
aload 15
iload 3
aaload
iload 8
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
aload 12
aload 14
if_acmpne L18
new org/xbill/DNS/SetResponse
dup
iconst_5
aload 15
iload 3
aaload
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 11
L19:
aload 15
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
checkcast org/xbill/DNS/DNAMERecord
astore 13
L0:
aload 12
aload 13
invokevirtual org/xbill/DNS/Name/fromDNAME(Lorg/xbill/DNS/DNAMERecord;)Lorg/xbill/DNS/Name;
astore 13
L1:
aload 11
astore 12
aload 13
astore 11
goto L14
L2:
astore 13
aload 11
astore 13
L20:
aload 1
iconst_2
invokevirtual org/xbill/DNS/Message/getSectionRRsets(I)[Lorg/xbill/DNS/RRset;
astore 18
aconst_null
astore 14
aconst_null
astore 11
iconst_0
istore 3
L21:
iload 3
aload 18
arraylength
if_icmpge L22
aload 18
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getType()I
bipush 6
if_icmpne L23
aload 12
aload 18
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifeq L23
aload 18
iload 3
aaload
astore 16
aload 14
astore 15
L24:
iload 3
iconst_1
iadd
istore 3
aload 15
astore 14
aload 16
astore 11
goto L21
L23:
aload 14
astore 15
aload 11
astore 16
aload 18
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getType()I
iconst_2
if_icmpne L24
aload 14
astore 15
aload 11
astore 16
aload 12
aload 18
iload 3
aaload
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/subdomain(Lorg/xbill/DNS/Name;)Z
ifeq L24
aload 18
iload 3
aaload
astore 15
aload 11
astore 16
goto L24
L22:
iload 2
ifne L25
iload 5
iconst_3
if_icmpne L26
iconst_0
istore 2
L27:
iload 5
iconst_3
if_icmpeq L28
aload 11
ifnonnull L28
aload 14
ifnonnull L29
L28:
aload 0
iconst_2
iload 9
invokespecial org/xbill/DNS/Cache/getCred(IZ)I
istore 3
aconst_null
astore 14
aload 11
ifnull L30
aload 11
invokevirtual org/xbill/DNS/RRset/first()Lorg/xbill/DNS/Record;
checkcast org/xbill/DNS/SOARecord
astore 14
L30:
aload 0
aload 12
iload 2
aload 14
iload 3
invokevirtual org/xbill/DNS/Cache/addNegative(Lorg/xbill/DNS/Name;ILorg/xbill/DNS/SOARecord;I)V
aload 13
astore 11
aload 13
ifnonnull L31
iload 5
iconst_3
if_icmpne L32
iconst_1
istore 2
L33:
iload 2
invokestatic org/xbill/DNS/SetResponse/ofType(I)Lorg/xbill/DNS/SetResponse;
astore 11
L31:
aload 1
iconst_3
invokevirtual org/xbill/DNS/Message/getSectionRRsets(I)[Lorg/xbill/DNS/RRset;
astore 1
iconst_0
istore 2
L34:
iload 2
aload 1
arraylength
if_icmpge L35
aload 1
iload 2
aaload
invokevirtual org/xbill/DNS/RRset/getType()I
istore 3
iload 3
iconst_1
if_icmpeq L36
iload 3
bipush 28
if_icmpeq L36
iload 3
bipush 38
if_icmpne L37
L36:
aload 17
aload 1
iload 2
aaload
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
invokevirtual java/util/HashSet/contains(Ljava/lang/Object;)Z
ifeq L37
aload 0
iconst_3
iload 9
invokespecial org/xbill/DNS/Cache/getCred(IZ)I
istore 3
aload 0
aload 1
iload 2
aaload
iload 3
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
L37:
iload 2
iconst_1
iadd
istore 2
goto L34
L26:
iload 4
istore 2
goto L27
L32:
iconst_2
istore 2
goto L33
L29:
aload 0
aload 14
aload 0
iconst_2
iload 9
invokespecial org/xbill/DNS/Cache/getCred(IZ)I
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
aload 14
aload 17
invokestatic org/xbill/DNS/Cache/markAdditional(Lorg/xbill/DNS/RRset;Ljava/util/Set;)V
aload 13
astore 11
aload 13
ifnonnull L31
new org/xbill/DNS/SetResponse
dup
iconst_3
aload 14
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 11
goto L31
L25:
aload 13
astore 11
iload 5
ifne L31
aload 13
astore 11
aload 14
ifnull L31
aload 0
aload 14
aload 0
iconst_2
iload 9
invokespecial org/xbill/DNS/Cache/getCred(IZ)I
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
aload 14
aload 17
invokestatic org/xbill/DNS/Cache/markAdditional(Lorg/xbill/DNS/RRset;Ljava/util/Set;)V
aload 13
astore 11
goto L31
L35:
aload 11
astore 1
iload 10
ifeq L6
getstatic java/lang/System/out Ljava/io/PrintStream;
new java/lang/StringBuffer
dup
ldc "addMessage: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 11
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
aload 11
areturn
L18:
goto L19
L9:
aload 12
astore 13
aload 11
astore 12
aload 13
astore 11
goto L14
L16:
goto L17
L8:
aload 11
astore 13
goto L20
.limit locals 19
.limit stack 5
.end method

.method public addNegative(Lorg/xbill/DNS/Name;ILorg/xbill/DNS/SOARecord;I)V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
aload 0
monitorenter
aload 3
ifnull L10
L0:
aload 3
invokevirtual org/xbill/DNS/SOARecord/getTTL()J
lstore 5
L1:
aload 0
aload 1
iload 2
iconst_0
invokespecial org/xbill/DNS/Cache/findElement(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Cache$Element;
astore 8
L3:
lload 5
lconst_0
lcmp
ifne L11
aload 8
ifnull L5
L4:
aload 8
iload 4
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
ifgt L5
aload 0
aload 1
iload 2
invokespecial org/xbill/DNS/Cache/removeElement(Lorg/xbill/DNS/Name;I)V
L5:
aload 0
monitorexit
return
L11:
aload 8
astore 7
aload 8
ifnull L12
aload 8
astore 7
L6:
aload 8
iload 4
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
ifgt L12
L7:
aconst_null
astore 7
L12:
aload 7
ifnonnull L5
L8:
aload 0
aload 1
new org/xbill/DNS/Cache$NegativeElement
dup
aload 1
iload 2
aload 3
iload 4
aload 0
getfield org/xbill/DNS/Cache/maxncache I
i2l
invokespecial org/xbill/DNS/Cache$NegativeElement/<init>(Lorg/xbill/DNS/Name;ILorg/xbill/DNS/SOARecord;IJ)V
invokespecial org/xbill/DNS/Cache/addElement(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Cache$Element;)V
L9:
goto L5
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L10:
lconst_0
lstore 5
goto L1
.limit locals 9
.limit stack 10
.end method

.method public addRRset(Lorg/xbill/DNS/RRset;I)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
aload 0
monitorenter
L0:
aload 1
invokevirtual org/xbill/DNS/RRset/getTTL()J
lstore 4
aload 1
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
astore 8
aload 1
invokevirtual org/xbill/DNS/RRset/getType()I
istore 3
aload 0
aload 8
iload 3
iconst_0
invokespecial org/xbill/DNS/Cache/findElement(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Cache$Element;
astore 7
L1:
lload 4
lconst_0
lcmp
ifne L12
aload 7
ifnull L4
L3:
aload 7
iload 2
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
ifgt L4
aload 0
aload 8
iload 3
invokespecial org/xbill/DNS/Cache/removeElement(Lorg/xbill/DNS/Name;I)V
L4:
aload 0
monitorexit
return
L12:
aload 7
astore 6
aload 7
ifnull L13
aload 7
astore 6
L5:
aload 7
iload 2
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
ifgt L13
L6:
aconst_null
astore 6
L13:
aload 6
ifnonnull L4
L7:
aload 1
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L10
aload 1
checkcast org/xbill/DNS/Cache$CacheRRset
astore 1
L8:
aload 0
aload 8
aload 1
invokespecial org/xbill/DNS/Cache/addElement(Lorg/xbill/DNS/Name;Lorg/xbill/DNS/Cache$Element;)V
L9:
goto L4
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L10:
new org/xbill/DNS/Cache$CacheRRset
dup
aload 1
iload 2
aload 0
getfield org/xbill/DNS/Cache/maxcache I
i2l
invokespecial org/xbill/DNS/Cache$CacheRRset/<init>(Lorg/xbill/DNS/RRset;IJ)V
astore 1
L11:
goto L8
.limit locals 9
.limit stack 6
.end method

.method public addRecord(Lorg/xbill/DNS/Record;ILjava/lang/Object;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
monitorenter
L0:
aload 1
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 3
aload 1
invokevirtual org/xbill/DNS/Record/getRRsetType()I
istore 4
iload 4
invokestatic org/xbill/DNS/Type/isRR(I)Z
istore 5
L1:
iload 5
ifne L3
L9:
aload 0
monitorexit
return
L3:
aload 0
aload 3
iload 4
iload 2
invokespecial org/xbill/DNS/Cache/findElement(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Cache$Element;
astore 3
L4:
aload 3
ifnonnull L7
L5:
aload 0
new org/xbill/DNS/Cache$CacheRRset
dup
aload 1
iload 2
aload 0
getfield org/xbill/DNS/Cache/maxcache I
i2l
invokespecial org/xbill/DNS/Cache$CacheRRset/<init>(Lorg/xbill/DNS/Record;IJ)V
iload 2
invokevirtual org/xbill/DNS/Cache/addRRset(Lorg/xbill/DNS/RRset;I)V
L6:
goto L9
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L7:
aload 3
iload 2
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
ifne L9
aload 3
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L9
aload 3
checkcast org/xbill/DNS/Cache$CacheRRset
aload 1
invokevirtual org/xbill/DNS/Cache$CacheRRset/addRR(Lorg/xbill/DNS/Record;)V
L8:
goto L9
.limit locals 6
.limit stack 7
.end method

.method public clearCache()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
invokevirtual org/xbill/DNS/Cache$CacheMap/clear()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public findAnyRecords(Lorg/xbill/DNS/Name;I)[Lorg/xbill/DNS/RRset;
aload 0
aload 1
iload 2
iconst_2
invokespecial org/xbill/DNS/Cache/findRecords(Lorg/xbill/DNS/Name;II)[Lorg/xbill/DNS/RRset;
areturn
.limit locals 3
.limit stack 4
.end method

.method public findRecords(Lorg/xbill/DNS/Name;I)[Lorg/xbill/DNS/RRset;
aload 0
aload 1
iload 2
iconst_3
invokespecial org/xbill/DNS/Cache/findRecords(Lorg/xbill/DNS/Name;II)[Lorg/xbill/DNS/RRset;
areturn
.limit locals 3
.limit stack 4
.end method

.method public flushName(Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokespecial org/xbill/DNS/Cache/removeName(Lorg/xbill/DNS/Name;)V
return
.limit locals 2
.limit stack 2
.end method

.method public flushSet(Lorg/xbill/DNS/Name;I)V
aload 0
aload 1
iload 2
invokespecial org/xbill/DNS/Cache/removeElement(Lorg/xbill/DNS/Name;I)V
return
.limit locals 3
.limit stack 3
.end method

.method public getDClass()I
aload 0
getfield org/xbill/DNS/Cache/dclass I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMaxCache()I
aload 0
getfield org/xbill/DNS/Cache/maxcache I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMaxEntries()I
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
invokevirtual org/xbill/DNS/Cache$CacheMap/getMaxSize()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMaxNCache()I
aload 0
getfield org/xbill/DNS/Cache/maxncache I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getSize()I
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
invokevirtual org/xbill/DNS/Cache$CacheMap/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected lookup(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
.catch all from L14 to L15 using L2
.catch all from L16 to L17 using L2
.catch all from L18 to L19 using L2
.catch all from L20 to L21 using L2
.catch all from L22 to L23 using L2
.catch all from L24 to L25 using L2
.catch all from L26 to L27 using L2
.catch all from L28 to L29 using L2
.catch all from L30 to L31 using L2
.catch all from L32 to L33 using L2
.catch all from L34 to L35 using L2
.catch all from L36 to L37 using L2
aload 0
monitorenter
L0:
aload 1
invokevirtual org/xbill/DNS/Name/labels()I
istore 8
L1:
iload 8
istore 5
goto L38
L39:
iload 4
ifeq L40
L3:
getstatic org/xbill/DNS/Name/root Lorg/xbill/DNS/Name;
astore 9
L4:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
aload 9
invokevirtual org/xbill/DNS/Cache$CacheMap/get(Ljava/lang/Object;)Ljava/lang/Object;
astore 11
L5:
aload 11
ifnull L41
iload 6
ifeq L42
iload 2
sipush 255
if_icmpne L42
L6:
new org/xbill/DNS/SetResponse
dup
bipush 6
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 10
aload 0
aload 11
invokespecial org/xbill/DNS/Cache/allElements(Ljava/lang/Object;)[Lorg/xbill/DNS/Cache$Element;
astore 12
L7:
iconst_0
istore 4
iconst_0
istore 7
L8:
iload 7
aload 12
arraylength
if_icmpge L43
L9:
aload 12
iload 7
aaload
astore 13
L10:
aload 13
invokeinterface org/xbill/DNS/Cache$Element/expired()Z 0
ifeq L14
aload 0
aload 9
aload 13
invokeinterface org/xbill/DNS/Cache$Element/getType()I 0
invokespecial org/xbill/DNS/Cache/removeElement(Lorg/xbill/DNS/Name;I)V
L11:
goto L44
L12:
new org/xbill/DNS/Name
dup
aload 1
iload 8
iload 5
isub
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/Name;I)V
astore 9
L13:
goto L4
L14:
aload 13
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L45
aload 13
iload 3
invokeinterface org/xbill/DNS/Cache$Element/compareCredibility(I)I 1
iflt L45
aload 10
aload 13
checkcast org/xbill/DNS/Cache$CacheRRset
invokevirtual org/xbill/DNS/SetResponse/addRRset(Lorg/xbill/DNS/RRset;)V
L15:
iload 4
iconst_1
iadd
istore 4
goto L44
L43:
iload 4
ifle L30
aload 10
astore 1
L46:
aload 0
monitorexit
aload 1
areturn
L42:
iload 6
ifeq L26
L16:
aload 0
aload 9
aload 11
iload 2
iload 3
invokespecial org/xbill/DNS/Cache/oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
astore 10
L17:
aload 10
ifnull L47
L18:
aload 10
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L47
new org/xbill/DNS/SetResponse
dup
bipush 6
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 1
aload 1
aload 10
checkcast org/xbill/DNS/Cache$CacheRRset
invokevirtual org/xbill/DNS/SetResponse/addRRset(Lorg/xbill/DNS/RRset;)V
L19:
goto L46
L47:
aload 10
ifnull L22
L20:
new org/xbill/DNS/SetResponse
dup
iconst_2
invokespecial org/xbill/DNS/SetResponse/<init>(I)V
astore 1
L21:
goto L46
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
L22:
aload 0
aload 9
aload 11
iconst_5
iload 3
invokespecial org/xbill/DNS/Cache/oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
astore 10
L23:
aload 10
ifnull L30
L24:
aload 10
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L30
new org/xbill/DNS/SetResponse
dup
iconst_4
aload 10
checkcast org/xbill/DNS/Cache$CacheRRset
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 1
L25:
goto L46
L26:
aload 0
aload 9
aload 11
bipush 39
iload 3
invokespecial org/xbill/DNS/Cache/oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
astore 10
L27:
aload 10
ifnull L30
L28:
aload 10
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L30
new org/xbill/DNS/SetResponse
dup
iconst_5
aload 10
checkcast org/xbill/DNS/Cache$CacheRRset
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 1
L29:
goto L46
L30:
aload 0
aload 9
aload 11
iconst_2
iload 3
invokespecial org/xbill/DNS/Cache/oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
astore 10
L31:
aload 10
ifnull L48
L32:
aload 10
instanceof org/xbill/DNS/Cache$CacheRRset
ifeq L48
new org/xbill/DNS/SetResponse
dup
iconst_3
aload 10
checkcast org/xbill/DNS/Cache$CacheRRset
invokespecial org/xbill/DNS/SetResponse/<init>(ILorg/xbill/DNS/RRset;)V
astore 1
L33:
goto L46
L48:
iload 6
ifeq L41
L34:
aload 0
aload 9
aload 11
iconst_0
iload 3
invokespecial org/xbill/DNS/Cache/oneElement(Lorg/xbill/DNS/Name;Ljava/lang/Object;II)Lorg/xbill/DNS/Cache$Element;
ifnull L41
iconst_1
invokestatic org/xbill/DNS/SetResponse/ofType(I)Lorg/xbill/DNS/SetResponse;
astore 1
L35:
goto L46
L36:
iconst_0
invokestatic org/xbill/DNS/SetResponse/ofType(I)Lorg/xbill/DNS/SetResponse;
astore 1
L37:
goto L46
L45:
goto L44
L38:
iload 5
ifle L36
iload 5
iconst_1
if_icmpne L49
iconst_1
istore 4
L50:
iload 5
iload 8
if_icmpne L51
iconst_1
istore 6
goto L39
L44:
iload 7
iconst_1
iadd
istore 7
goto L8
L49:
iconst_0
istore 4
goto L50
L51:
iconst_0
istore 6
goto L39
L40:
iload 6
ifeq L12
aload 1
astore 9
goto L4
L41:
iload 5
iconst_1
isub
istore 5
goto L38
.limit locals 14
.limit stack 5
.end method

.method public lookupRecords(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;
aload 0
aload 1
iload 2
iload 3
invokevirtual org/xbill/DNS/Cache/lookup(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/SetResponse;
areturn
.limit locals 4
.limit stack 4
.end method

.method public setMaxCache(I)V
aload 0
iload 1
putfield org/xbill/DNS/Cache/maxcache I
return
.limit locals 2
.limit stack 2
.end method

.method public setMaxEntries(I)V
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
iload 1
invokevirtual org/xbill/DNS/Cache$CacheMap/setMaxSize(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public setMaxNCache(I)V
aload 0
iload 1
putfield org/xbill/DNS/Cache/maxncache I
return
.limit locals 2
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
aload 0
monitorenter
L0:
aload 0
getfield org/xbill/DNS/Cache/data Lorg/xbill/DNS/Cache$CacheMap;
invokevirtual org/xbill/DNS/Cache$CacheMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 0
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokespecial org/xbill/DNS/Cache/allElements(Ljava/lang/Object;)[Lorg/xbill/DNS/Cache$Element;
astore 4
L3:
iconst_0
istore 1
L4:
iload 1
aload 4
arraylength
if_icmpge L1
aload 2
aload 4
iload 1
aaload
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 2
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L5:
iload 1
iconst_1
iadd
istore 1
goto L4
L6:
aload 0
monitorexit
L7:
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L2:
astore 2
L8:
aload 0
monitorexit
L9:
aload 2
athrow
.limit locals 5
.limit stack 3
.end method
