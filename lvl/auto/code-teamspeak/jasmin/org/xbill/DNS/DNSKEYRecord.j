.bytecode 50.0
.class public synchronized org/xbill/DNS/DNSKEYRecord
.super org/xbill/DNS/KEYBase

.field private static final 'serialVersionUID' J = -8679800040426675002L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/KEYBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIIILjava/security/PublicKey;)V
aload 0
aload 1
bipush 48
iload 2
lload 3
iload 5
iload 6
iload 7
aload 8
iload 7
invokestatic org/xbill/DNS/DNSSEC/fromPublicKey(Ljava/security/PublicKey;I)[B
invokespecial org/xbill/DNS/KEYBase/<init>(Lorg/xbill/DNS/Name;IIJIII[B)V
aload 0
aload 8
putfield org/xbill/DNS/DNSKEYRecord/publicKey Ljava/security/PublicKey;
return
.limit locals 9
.limit stack 11
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
bipush 48
iload 2
lload 3
iload 5
iload 6
iload 7
aload 8
invokespecial org/xbill/DNS/KEYBase/<init>(Lorg/xbill/DNS/Name;IIJIII[B)V
return
.limit locals 9
.limit stack 10
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/DNSKEYRecord
dup
invokespecial org/xbill/DNS/DNSKEYRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/DNSKEYRecord/flags I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/DNSKEYRecord/proto I
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/DNSSEC$Algorithm/value(Ljava/lang/String;)I
putfield org/xbill/DNS/DNSKEYRecord/alg I
aload 0
getfield org/xbill/DNS/DNSKEYRecord/alg I
ifge L0
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid algorithm: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getBase64()[B
putfield org/xbill/DNS/DNSKEYRecord/key [B
return
.limit locals 3
.limit stack 4
.end method
