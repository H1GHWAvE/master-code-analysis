.bytecode 50.0
.class public synchronized org/xbill/DNS/HINFORecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -4732870630947452112L


.field private 'cpu' [B

.field private 'os' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/lang/String;Ljava/lang/String;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
bipush 13
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
L0:
aload 0
aload 5
invokestatic org/xbill/DNS/HINFORecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/HINFORecord/cpu [B
aload 0
aload 6
invokestatic org/xbill/DNS/HINFORecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/HINFORecord/os [B
L1:
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 7
.limit stack 6
.end method

.method public getCPU()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/HINFORecord/cpu [B
iconst_0
invokestatic org/xbill/DNS/HINFORecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getOS()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/HINFORecord/os [B
iconst_0
invokestatic org/xbill/DNS/HINFORecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/HINFORecord
dup
invokespecial org/xbill/DNS/HINFORecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/HINFORecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/HINFORecord/cpu [B
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/HINFORecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/HINFORecord/os [B
L1:
return
L2:
astore 2
aload 1
aload 2
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/HINFORecord/cpu [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/HINFORecord/os [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/HINFORecord/cpu [B
iconst_1
invokestatic org/xbill/DNS/HINFORecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/HINFORecord/os [B
iconst_1
invokestatic org/xbill/DNS/HINFORecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/HINFORecord/cpu [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 1
aload 0
getfield org/xbill/DNS/HINFORecord/os [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
return
.limit locals 4
.limit stack 2
.end method
