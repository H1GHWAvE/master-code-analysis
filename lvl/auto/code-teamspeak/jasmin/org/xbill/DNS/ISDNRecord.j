.bytecode 50.0
.class public synchronized org/xbill/DNS/ISDNRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -8730801385178968798L


.field private 'address' [B

.field private 'subAddress' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/lang/String;Ljava/lang/String;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
.catch org/xbill/DNS/TextParseException from L3 to L4 using L2
aload 0
aload 1
bipush 20
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
L0:
aload 0
aload 5
invokestatic org/xbill/DNS/ISDNRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/ISDNRecord/address [B
L1:
aload 6
ifnull L4
L3:
aload 0
aload 6
invokestatic org/xbill/DNS/ISDNRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/ISDNRecord/subAddress [B
L4:
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 7
.limit stack 6
.end method

.method public getAddress()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/ISDNRecord/address [B
iconst_0
invokestatic org/xbill/DNS/ISDNRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/ISDNRecord
dup
invokespecial org/xbill/DNS/ISDNRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getSubAddress()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/ISDNRecord/subAddress [B
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield org/xbill/DNS/ISDNRecord/subAddress [B
iconst_0
invokestatic org/xbill/DNS/ISDNRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
.catch org/xbill/DNS/TextParseException from L3 to L4 using L2
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/ISDNRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/ISDNRecord/address [B
aload 1
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 2
aload 2
invokevirtual org/xbill/DNS/Tokenizer$Token/isString()Z
ifeq L3
aload 0
aload 2
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
invokestatic org/xbill/DNS/ISDNRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/ISDNRecord/subAddress [B
L1:
return
L3:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/unget()V
L4:
return
L2:
astore 2
aload 1
aload 2
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/ISDNRecord/address [B
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L0
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/ISDNRecord/subAddress [B
L0:
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/ISDNRecord/address [B
iconst_1
invokestatic org/xbill/DNS/ISDNRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/ISDNRecord/subAddress [B
ifnull L0
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/ISDNRecord/subAddress [B
iconst_1
invokestatic org/xbill/DNS/ISDNRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/ISDNRecord/address [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 0
getfield org/xbill/DNS/ISDNRecord/subAddress [B
ifnull L0
aload 1
aload 0
getfield org/xbill/DNS/ISDNRecord/subAddress [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
L0:
return
.limit locals 4
.limit stack 2
.end method
