.bytecode 50.0
.class public synchronized org/xbill/DNS/Generator
.super java/lang/Object

.field private 'current' J

.field public final 'dclass' I

.field public 'end' J

.field public final 'namePattern' Ljava/lang/String;

.field public final 'origin' Lorg/xbill/DNS/Name;

.field public final 'rdataPattern' Ljava/lang/String;

.field public 'start' J

.field public 'step' J

.field public final 'ttl' J

.field public final 'type' I

.method public <init>(JJJLjava/lang/String;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)V
aload 0
invokespecial java/lang/Object/<init>()V
lload 1
lconst_0
lcmp
iflt L0
lload 3
lconst_0
lcmp
iflt L0
lload 1
lload 3
lcmp
ifgt L0
lload 5
lconst_0
lcmp
ifgt L1
L0:
new java/lang/IllegalArgumentException
dup
ldc "invalid range specification"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 8
invokestatic org/xbill/DNS/Generator/supportedType(I)Z
ifne L2
new java/lang/IllegalArgumentException
dup
ldc "unsupported type"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 9
invokestatic org/xbill/DNS/DClass/check(I)V
aload 0
lload 1
putfield org/xbill/DNS/Generator/start J
aload 0
lload 3
putfield org/xbill/DNS/Generator/end J
aload 0
lload 5
putfield org/xbill/DNS/Generator/step J
aload 0
aload 7
putfield org/xbill/DNS/Generator/namePattern Ljava/lang/String;
aload 0
iload 8
putfield org/xbill/DNS/Generator/type I
aload 0
iload 9
putfield org/xbill/DNS/Generator/dclass I
aload 0
lload 10
putfield org/xbill/DNS/Generator/ttl J
aload 0
aload 12
putfield org/xbill/DNS/Generator/rdataPattern Ljava/lang/String;
aload 0
aload 13
putfield org/xbill/DNS/Generator/origin Lorg/xbill/DNS/Name;
aload 0
lload 1
putfield org/xbill/DNS/Generator/current J
return
.limit locals 14
.limit stack 4
.end method

.method private substitute(Ljava/lang/String;J)Ljava/lang/String;
iconst_0
istore 8
aload 1
invokevirtual java/lang/String/getBytes()[B
astore 23
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 24
iconst_0
istore 5
L0:
iload 5
aload 23
arraylength
if_icmpge L1
aload 23
iload 5
baload
sipush 255
iand
i2c
istore 4
iload 8
ifeq L2
aload 24
iload 4
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iconst_0
istore 6
L3:
iload 5
iconst_1
iadd
istore 5
iload 6
istore 8
goto L0
L2:
iload 4
bipush 92
if_icmpne L4
iload 5
iconst_1
iadd
aload 23
arraylength
if_icmpne L5
new org/xbill/DNS/TextParseException
dup
ldc "invalid escape character"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L5:
iconst_1
istore 6
goto L3
L4:
iload 4
bipush 36
if_icmpne L6
lconst_0
lstore 17
lconst_0
lstore 15
ldc2_w 10L
lstore 21
iconst_0
istore 9
iload 5
iconst_1
iadd
aload 23
arraylength
if_icmpge L7
aload 23
iload 5
iconst_1
iadd
baload
bipush 36
if_icmpne L7
iload 5
iconst_1
iadd
istore 5
aload 24
aload 23
iload 5
baload
sipush 255
iand
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 8
istore 6
goto L3
L7:
iload 5
istore 6
lload 17
lstore 19
iload 9
istore 7
lload 15
lstore 13
lload 21
lstore 11
iload 5
iconst_1
iadd
aload 23
arraylength
if_icmpge L8
iload 5
istore 6
lload 17
lstore 19
iload 9
istore 7
lload 15
lstore 13
lload 21
lstore 11
aload 23
iload 5
iconst_1
iadd
baload
bipush 123
if_icmpne L8
iload 5
iconst_1
iadd
istore 6
iload 6
iconst_1
iadd
aload 23
arraylength
if_icmpge L9
aload 23
iload 6
iconst_1
iadd
baload
bipush 45
if_icmpne L9
iconst_1
istore 9
iload 6
iconst_1
iadd
istore 6
iload 4
istore 5
lload 17
lstore 11
L10:
iload 6
istore 7
iload 6
iconst_1
iadd
aload 23
arraylength
if_icmpge L11
iload 6
iconst_1
iadd
istore 6
aload 23
iload 6
baload
sipush 255
iand
i2c
istore 10
iload 10
istore 5
iload 6
istore 7
iload 10
bipush 44
if_icmpeq L11
iload 10
istore 5
iload 6
istore 7
iload 10
bipush 125
if_icmpeq L11
iload 10
bipush 48
if_icmplt L12
iload 10
bipush 57
if_icmple L13
L12:
new org/xbill/DNS/TextParseException
dup
ldc "invalid offset"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L13:
iload 10
bipush 48
isub
i2c
istore 5
lload 11
ldc2_w 10L
lmul
iload 5
i2l
ladd
lstore 11
goto L10
L11:
iload 9
ifeq L14
lload 11
lneg
lstore 13
L15:
iload 5
bipush 44
if_icmpne L16
lconst_0
lstore 11
iload 7
istore 6
L17:
iload 6
istore 7
iload 6
iconst_1
iadd
aload 23
arraylength
if_icmpge L18
iload 6
iconst_1
iadd
istore 6
aload 23
iload 6
baload
sipush 255
iand
i2c
istore 9
iload 9
istore 5
iload 6
istore 7
iload 9
bipush 44
if_icmpeq L18
iload 9
istore 5
iload 6
istore 7
iload 9
bipush 125
if_icmpeq L18
iload 9
bipush 48
if_icmplt L19
iload 9
bipush 57
if_icmple L20
L19:
new org/xbill/DNS/TextParseException
dup
ldc "invalid width"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L20:
iload 9
bipush 48
isub
i2c
istore 5
lload 11
ldc2_w 10L
lmul
iload 5
i2l
ladd
lstore 11
goto L17
L18:
iload 7
istore 6
lload 11
lstore 15
L21:
iload 5
bipush 44
if_icmpne L22
iload 6
iconst_1
iadd
aload 23
arraylength
if_icmpne L23
new org/xbill/DNS/TextParseException
dup
ldc "invalid base"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L23:
iload 6
iconst_1
iadd
istore 6
aload 23
iload 6
baload
sipush 255
iand
i2c
istore 5
iload 5
bipush 111
if_icmpne L24
ldc2_w 8L
lstore 11
iconst_0
istore 5
L25:
iload 6
iconst_1
iadd
aload 23
arraylength
if_icmpeq L26
aload 23
iload 6
iconst_1
iadd
baload
bipush 125
if_icmpeq L27
L26:
new org/xbill/DNS/TextParseException
dup
ldc "invalid modifiers"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L24:
iload 5
bipush 120
if_icmpne L28
ldc2_w 16L
lstore 11
iconst_0
istore 5
goto L25
L28:
iload 5
bipush 88
if_icmpne L29
ldc2_w 16L
lstore 11
iconst_1
istore 5
goto L25
L29:
iload 5
bipush 100
if_icmpeq L30
new org/xbill/DNS/TextParseException
dup
ldc "invalid base"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L27:
lload 13
lstore 19
iload 6
iconst_1
iadd
istore 6
lload 15
lstore 13
iload 5
istore 7
L8:
lload 19
lload 2
ladd
lstore 15
lload 15
lconst_0
lcmp
ifge L31
new org/xbill/DNS/TextParseException
dup
ldc "invalid offset expansion"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L31:
lload 11
ldc2_w 8L
lcmp
ifne L32
lload 15
invokestatic java/lang/Long/toOctalString(J)Ljava/lang/String;
astore 1
L33:
iload 7
ifeq L34
aload 1
invokevirtual java/lang/String/toUpperCase()Ljava/lang/String;
astore 1
L35:
lload 13
lconst_0
lcmp
ifeq L36
lload 13
aload 1
invokevirtual java/lang/String/length()I
i2l
lcmp
ifle L36
lload 13
l2i
aload 1
invokevirtual java/lang/String/length()I
isub
istore 5
L37:
iload 5
ifle L36
aload 24
bipush 48
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 5
iconst_1
isub
istore 5
goto L37
L32:
lload 11
ldc2_w 16L
lcmp
ifne L38
lload 15
invokestatic java/lang/Long/toHexString(J)Ljava/lang/String;
astore 1
goto L33
L38:
lload 15
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
astore 1
goto L33
L36:
aload 24
aload 1
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 6
istore 5
iload 8
istore 6
goto L3
L6:
aload 24
iload 4
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 8
istore 6
goto L3
L1:
aload 24
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L34:
goto L35
L30:
ldc2_w 10L
lstore 11
iconst_0
istore 5
goto L25
L22:
ldc2_w 10L
lstore 11
iconst_0
istore 5
goto L25
L16:
iload 7
istore 6
goto L21
L14:
lload 11
lstore 13
goto L15
L9:
iconst_0
istore 9
iload 4
istore 5
lload 17
lstore 11
goto L10
.limit locals 25
.limit stack 4
.end method

.method public static supportedType(I)Z
iload 0
invokestatic org/xbill/DNS/Type/check(I)V
iload 0
bipush 12
if_icmpeq L0
iload 0
iconst_5
if_icmpeq L0
iload 0
bipush 39
if_icmpeq L0
iload 0
iconst_1
if_icmpeq L0
iload 0
bipush 28
if_icmpeq L0
iload 0
iconst_2
if_icmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public expand()[Lorg/xbill/DNS/Record;
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 3
aload 0
getfield org/xbill/DNS/Generator/start J
lstore 1
L0:
lload 1
aload 0
getfield org/xbill/DNS/Generator/end J
lcmp
ifge L1
aload 0
aload 0
getfield org/xbill/DNS/Generator/namePattern Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Generator/current J
invokespecial org/xbill/DNS/Generator/substitute(Ljava/lang/String;J)Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Generator/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 4
aload 0
aload 0
getfield org/xbill/DNS/Generator/rdataPattern Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Generator/current J
invokespecial org/xbill/DNS/Generator/substitute(Ljava/lang/String;J)Ljava/lang/String;
astore 5
aload 3
aload 4
aload 0
getfield org/xbill/DNS/Generator/type I
aload 0
getfield org/xbill/DNS/Generator/dclass I
aload 0
getfield org/xbill/DNS/Generator/ttl J
aload 5
aload 0
getfield org/xbill/DNS/Generator/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield org/xbill/DNS/Generator/step J
lload 1
ladd
lstore 1
goto L0
L1:
aload 3
aload 3
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Record
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Record;
checkcast [Lorg/xbill/DNS/Record;
areturn
.limit locals 6
.limit stack 8
.end method

.method public nextRecord()Lorg/xbill/DNS/Record;
aload 0
getfield org/xbill/DNS/Generator/current J
aload 0
getfield org/xbill/DNS/Generator/end J
lcmp
ifle L0
aconst_null
areturn
L0:
aload 0
aload 0
getfield org/xbill/DNS/Generator/namePattern Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Generator/current J
invokespecial org/xbill/DNS/Generator/substitute(Ljava/lang/String;J)Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Generator/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Name/fromString(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
astore 1
aload 0
aload 0
getfield org/xbill/DNS/Generator/rdataPattern Ljava/lang/String;
aload 0
getfield org/xbill/DNS/Generator/current J
invokespecial org/xbill/DNS/Generator/substitute(Ljava/lang/String;J)Ljava/lang/String;
astore 2
aload 0
aload 0
getfield org/xbill/DNS/Generator/current J
aload 0
getfield org/xbill/DNS/Generator/step J
ladd
putfield org/xbill/DNS/Generator/current J
aload 1
aload 0
getfield org/xbill/DNS/Generator/type I
aload 0
getfield org/xbill/DNS/Generator/dclass I
aload 0
getfield org/xbill/DNS/Generator/ttl J
aload 2
aload 0
getfield org/xbill/DNS/Generator/origin Lorg/xbill/DNS/Name;
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
areturn
.limit locals 3
.limit stack 7
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
ldc "$GENERATE "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Generator/start J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc "-"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
getfield org/xbill/DNS/Generator/end J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/Generator/step J
lconst_1
lcmp
ifle L0
aload 1
new java/lang/StringBuffer
dup
ldc "/"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Generator/step J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Generator/namePattern Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Generator/ttl J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/Generator/dclass I
iconst_1
if_icmpne L1
ldc "noPrintIN"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifne L2
L1:
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Generator/dclass I
invokestatic org/xbill/DNS/DClass/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L2:
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Generator/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Generator/rdataPattern Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
