.bytecode 50.0
.class synchronized org/xbill/DNS/DNSSEC$ECKeyInfo
.super java/lang/Object

.field public 'a' Ljava/math/BigInteger;

.field public 'b' Ljava/math/BigInteger;

.field 'curve' Ljava/security/spec/EllipticCurve;

.field public 'gx' Ljava/math/BigInteger;

.field public 'gy' Ljava/math/BigInteger;

.field 'length' I

.field public 'n' Ljava/math/BigInteger;

.field public 'p' Ljava/math/BigInteger;

.field 'spec' Ljava/security/spec/ECParameterSpec;

.method <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/length I
aload 0
new java/math/BigInteger
dup
aload 2
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/p Ljava/math/BigInteger;
aload 0
new java/math/BigInteger
dup
aload 3
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/a Ljava/math/BigInteger;
aload 0
new java/math/BigInteger
dup
aload 4
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/b Ljava/math/BigInteger;
aload 0
new java/math/BigInteger
dup
aload 5
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/gx Ljava/math/BigInteger;
aload 0
new java/math/BigInteger
dup
aload 6
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/gy Ljava/math/BigInteger;
aload 0
new java/math/BigInteger
dup
aload 7
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/n Ljava/math/BigInteger;
aload 0
new java/security/spec/EllipticCurve
dup
new java/security/spec/ECFieldFp
dup
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/p Ljava/math/BigInteger;
invokespecial java/security/spec/ECFieldFp/<init>(Ljava/math/BigInteger;)V
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/a Ljava/math/BigInteger;
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/b Ljava/math/BigInteger;
invokespecial java/security/spec/EllipticCurve/<init>(Ljava/security/spec/ECField;Ljava/math/BigInteger;Ljava/math/BigInteger;)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/curve Ljava/security/spec/EllipticCurve;
aload 0
new java/security/spec/ECParameterSpec
dup
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/curve Ljava/security/spec/EllipticCurve;
new java/security/spec/ECPoint
dup
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/gx Ljava/math/BigInteger;
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/gy Ljava/math/BigInteger;
invokespecial java/security/spec/ECPoint/<init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
aload 0
getfield org/xbill/DNS/DNSSEC$ECKeyInfo/n Ljava/math/BigInteger;
iconst_1
invokespecial java/security/spec/ECParameterSpec/<init>(Ljava/security/spec/EllipticCurve;Ljava/security/spec/ECPoint;Ljava/math/BigInteger;I)V
putfield org/xbill/DNS/DNSSEC$ECKeyInfo/spec Ljava/security/spec/ECParameterSpec;
return
.limit locals 8
.limit stack 8
.end method
