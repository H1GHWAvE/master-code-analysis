.bytecode 50.0
.class public synchronized abstract org/xbill/DNS/Record
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/Cloneable
.implements java/lang/Comparable

.field private static final 'byteFormat' Ljava/text/DecimalFormat;

.field private static final 'serialVersionUID' J = 2694906050116005466L


.field protected 'dclass' I

.field protected 'name' Lorg/xbill/DNS/Name;

.field protected 'ttl' J

.field protected 'type' I

.method static <clinit>()V
new java/text/DecimalFormat
dup
invokespecial java/text/DecimalFormat/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/Record/byteFormat Ljava/text/DecimalFormat;
aload 0
iconst_3
invokevirtual java/text/DecimalFormat/setMinimumIntegerDigits(I)V
return
.limit locals 1
.limit stack 2
.end method

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method <init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 1
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
iload 2
invokestatic org/xbill/DNS/Type/check(I)V
iload 3
invokestatic org/xbill/DNS/DClass/check(I)V
lload 4
invokestatic org/xbill/DNS/TTL/check(J)V
aload 0
aload 1
putfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 0
iload 2
putfield org/xbill/DNS/Record/type I
aload 0
iload 3
putfield org/xbill/DNS/Record/dclass I
aload 0
lload 4
putfield org/xbill/DNS/Record/ttl J
return
.limit locals 6
.limit stack 3
.end method

.method protected static byteArrayFromString(Ljava/lang/String;)[B
aload 0
invokevirtual java/lang/String/getBytes()[B
astore 0
iconst_0
istore 1
L0:
iload 1
aload 0
arraylength
if_icmpge L1
aload 0
iload 1
baload
bipush 92
if_icmpne L2
iconst_1
istore 1
L3:
iload 1
ifne L4
aload 0
arraylength
sipush 255
if_icmple L5
new org/xbill/DNS/TextParseException
dup
ldc "text string too long"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L5:
aload 0
areturn
L4:
new java/io/ByteArrayOutputStream
dup
invokespecial java/io/ByteArrayOutputStream/<init>()V
astore 6
iconst_0
istore 4
iconst_0
istore 1
iconst_0
istore 2
iconst_0
istore 3
L6:
iload 4
aload 0
arraylength
if_icmpge L7
aload 0
iload 4
baload
istore 5
iload 3
ifeq L8
iload 5
bipush 48
if_icmplt L9
iload 5
bipush 57
if_icmpgt L9
iload 2
iconst_3
if_icmpge L9
iload 2
iconst_1
iadd
istore 2
iload 5
bipush 48
isub
iload 1
bipush 10
imul
iadd
istore 5
iload 5
sipush 255
if_icmple L10
new org/xbill/DNS/TextParseException
dup
ldc "bad escape"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L10:
iload 2
iconst_3
if_icmplt L11
iload 5
i2b
istore 3
iload 2
istore 1
iload 5
istore 2
iload 3
istore 5
L12:
aload 6
iload 5
invokevirtual java/io/ByteArrayOutputStream/write(I)V
iload 2
istore 5
iconst_0
istore 3
iload 1
istore 2
iload 5
istore 1
L13:
iload 4
iconst_1
iadd
istore 4
goto L6
L9:
iload 2
ifle L14
iload 2
iconst_3
if_icmpge L14
new org/xbill/DNS/TextParseException
dup
ldc "bad escape"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 0
iload 4
baload
bipush 92
if_icmpne L15
iconst_0
istore 1
iconst_0
istore 2
iconst_1
istore 3
goto L13
L15:
aload 6
aload 0
iload 4
baload
invokevirtual java/io/ByteArrayOutputStream/write(I)V
goto L13
L7:
iload 2
ifle L16
iload 2
iconst_3
if_icmpge L16
new org/xbill/DNS/TextParseException
dup
ldc "bad escape"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L16:
aload 6
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
arraylength
sipush 255
if_icmple L17
new org/xbill/DNS/TextParseException
dup
ldc "text string too long"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L17:
aload 6
invokevirtual java/io/ByteArrayOutputStream/toByteArray()[B
areturn
L14:
iload 2
istore 3
iload 1
istore 2
iload 3
istore 1
goto L12
L11:
iload 5
istore 1
goto L13
L1:
iconst_0
istore 1
goto L3
.limit locals 7
.limit stack 3
.end method

.method protected static byteArrayToString([BZ)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 4
iload 1
ifeq L0
aload 4
bipush 34
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
if_icmpge L2
aload 0
iload 2
baload
sipush 255
iand
istore 3
iload 3
bipush 32
if_icmplt L3
iload 3
bipush 127
if_icmplt L4
L3:
aload 4
bipush 92
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 4
getstatic org/xbill/DNS/Record/byteFormat Ljava/text/DecimalFormat;
iload 3
i2l
invokevirtual java/text/DecimalFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L4:
iload 3
bipush 34
if_icmpeq L6
iload 3
bipush 92
if_icmpne L7
L6:
aload 4
bipush 92
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 4
iload 3
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L5
L7:
aload 4
iload 3
i2c
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
goto L5
L2:
iload 1
ifeq L8
aload 4
bipush 34
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L8:
aload 4
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 4
.end method

.method static checkByteArrayLength(Ljava/lang/String;[BI)[B
aload 1
arraylength
ldc_w 65535
if_icmple L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "\""
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\" array must have no more than "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 2
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " elements"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
arraylength
newarray byte
astore 0
aload 1
iconst_0
aload 0
iconst_0
aload 1
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 0
areturn
.limit locals 3
.limit stack 5
.end method

.method static checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 1
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method static checkU16(Ljava/lang/String;I)I
iload 1
iflt L0
iload 1
ldc_w 65535
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "\""
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\" "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " must be an unsigned 16 bit value"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 1
ireturn
.limit locals 2
.limit stack 5
.end method

.method static checkU32(Ljava/lang/String;J)J
lload 1
lconst_0
lcmp
iflt L0
lload 1
ldc2_w 4294967295L
lcmp
ifle L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "\""
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\" "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
lload 1
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
ldc " must be an unsigned 32 bit value"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
lload 1
lreturn
.limit locals 3
.limit stack 5
.end method

.method static checkU8(Ljava/lang/String;I)I
iload 1
iflt L0
iload 1
sipush 255
if_icmple L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "\""
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\" "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
iload 1
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " must be an unsigned 8 bit value"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
iload 1
ireturn
.limit locals 2
.limit stack 5
.end method

.method public static fromString(Lorg/xbill/DNS/Name;IIJLjava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
aload 0
iload 1
iload 2
lload 3
new org/xbill/DNS/Tokenizer
dup
aload 5
invokespecial org/xbill/DNS/Tokenizer/<init>(Ljava/lang/String;)V
aload 6
invokestatic org/xbill/DNS/Record/fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
areturn
.limit locals 7
.limit stack 8
.end method

.method public static fromString(Lorg/xbill/DNS/Name;IIJLorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
aload 0
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 0
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
iload 1
invokestatic org/xbill/DNS/Type/check(I)V
iload 2
invokestatic org/xbill/DNS/DClass/check(I)V
lload 3
invokestatic org/xbill/DNS/TTL/check(J)V
aload 5
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 8
aload 8
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_3
if_icmpne L1
aload 8
getfield org/xbill/DNS/Tokenizer$Token/value Ljava/lang/String;
ldc "\\#"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 5
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
istore 7
aload 5
invokevirtual org/xbill/DNS/Tokenizer/getHex()[B
astore 8
aload 8
astore 6
aload 8
ifnonnull L2
iconst_0
newarray byte
astore 6
L2:
iload 7
aload 6
arraylength
if_icmpeq L3
aload 5
ldc "invalid unknown RR encoding: length mismatch"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L3:
aload 0
iload 1
iload 2
lload 3
iload 7
new org/xbill/DNS/DNSInput
dup
aload 6
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJILorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/Record;
astore 0
L4:
aload 0
areturn
L1:
aload 5
invokevirtual org/xbill/DNS/Tokenizer/unget()V
aload 0
iload 1
iload 2
lload 3
iconst_1
invokestatic org/xbill/DNS/Record/getEmptyRecord(Lorg/xbill/DNS/Name;IIJZ)Lorg/xbill/DNS/Record;
astore 8
aload 8
aload 5
aload 6
invokevirtual org/xbill/DNS/Record/rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 5
invokevirtual org/xbill/DNS/Tokenizer/get()Lorg/xbill/DNS/Tokenizer$Token;
astore 6
aload 8
astore 0
aload 6
getfield org/xbill/DNS/Tokenizer$Token/type I
iconst_1
if_icmpeq L4
aload 8
astore 0
aload 6
getfield org/xbill/DNS/Tokenizer$Token/type I
ifeq L4
aload 5
ldc "unexpected tokens at end of record"
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 9
.limit stack 9
.end method

.method static fromWire(Lorg/xbill/DNS/DNSInput;I)Lorg/xbill/DNS/Record;
aload 0
iload 1
iconst_0
invokestatic org/xbill/DNS/Record/fromWire(Lorg/xbill/DNS/DNSInput;IZ)Lorg/xbill/DNS/Record;
areturn
.limit locals 2
.limit stack 3
.end method

.method static fromWire(Lorg/xbill/DNS/DNSInput;IZ)Lorg/xbill/DNS/Record;
new org/xbill/DNS/Name
dup
aload 0
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
astore 8
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 3
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 4
iload 1
ifne L0
aload 8
iload 3
iload 4
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;
areturn
L0:
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU32()J
lstore 6
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 5
iload 5
ifne L1
iload 2
ifeq L1
iload 1
iconst_1
if_icmpeq L2
iload 1
iconst_2
if_icmpne L1
L2:
aload 8
iload 3
iload 4
lload 6
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
areturn
L1:
aload 8
iload 3
iload 4
lload 6
iload 5
aload 0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJILorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/Record;
areturn
.limit locals 9
.limit stack 7
.end method

.method public static fromWire([BI)Lorg/xbill/DNS/Record;
new org/xbill/DNS/DNSInput
dup
aload 0
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
iload 1
iconst_0
invokestatic org/xbill/DNS/Record/fromWire(Lorg/xbill/DNS/DNSInput;IZ)Lorg/xbill/DNS/Record;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static final getEmptyRecord(Lorg/xbill/DNS/Name;IIJZ)Lorg/xbill/DNS/Record;
iload 5
ifeq L0
iload 1
invokestatic org/xbill/DNS/Type/getProto(I)Lorg/xbill/DNS/Record;
astore 6
aload 6
ifnull L1
aload 6
invokevirtual org/xbill/DNS/Record/getObject()Lorg/xbill/DNS/Record;
astore 6
L2:
aload 6
aload 0
putfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 6
iload 1
putfield org/xbill/DNS/Record/type I
aload 6
iload 2
putfield org/xbill/DNS/Record/dclass I
aload 6
lload 3
putfield org/xbill/DNS/Record/ttl J
aload 6
areturn
L1:
new org/xbill/DNS/UNKRecord
dup
invokespecial org/xbill/DNS/UNKRecord/<init>()V
astore 6
goto L2
L0:
new org/xbill/DNS/EmptyRecord
dup
invokespecial org/xbill/DNS/EmptyRecord/<init>()V
astore 6
goto L2
.limit locals 7
.limit stack 3
.end method

.method public static newRecord(Lorg/xbill/DNS/Name;II)Lorg/xbill/DNS/Record;
aload 0
iload 1
iload 2
lconst_0
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
areturn
.limit locals 3
.limit stack 5
.end method

.method public static newRecord(Lorg/xbill/DNS/Name;IIJ)Lorg/xbill/DNS/Record;
aload 0
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 0
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
iload 1
invokestatic org/xbill/DNS/Type/check(I)V
iload 2
invokestatic org/xbill/DNS/DClass/check(I)V
lload 3
invokestatic org/xbill/DNS/TTL/check(J)V
aload 0
iload 1
iload 2
lload 3
iconst_0
invokestatic org/xbill/DNS/Record/getEmptyRecord(Lorg/xbill/DNS/Name;IIJZ)Lorg/xbill/DNS/Record;
areturn
.limit locals 5
.limit stack 6
.end method

.method private static newRecord(Lorg/xbill/DNS/Name;IIJILorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/Record;
aload 6
ifnull L0
iconst_1
istore 7
L1:
aload 0
iload 1
iload 2
lload 3
iload 7
invokestatic org/xbill/DNS/Record/getEmptyRecord(Lorg/xbill/DNS/Name;IIJZ)Lorg/xbill/DNS/Record;
astore 0
aload 6
ifnull L2
aload 6
invokevirtual org/xbill/DNS/DNSInput/remaining()I
iload 5
if_icmpge L3
new org/xbill/DNS/WireParseException
dup
ldc "truncated record"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
iconst_0
istore 7
goto L1
L3:
aload 6
iload 5
invokevirtual org/xbill/DNS/DNSInput/setActive(I)V
aload 0
aload 6
invokevirtual org/xbill/DNS/Record/rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 6
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L4
new org/xbill/DNS/WireParseException
dup
ldc "invalid record length"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L4:
aload 6
invokevirtual org/xbill/DNS/DNSInput/clearActive()V
L2:
aload 0
areturn
.limit locals 8
.limit stack 6
.end method

.method public static newRecord(Lorg/xbill/DNS/Name;IIJI[B)Lorg/xbill/DNS/Record;
.catch java/io/IOException from L0 to L1 using L2
aload 0
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L3
new org/xbill/DNS/RelativeNameException
dup
aload 0
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L3:
iload 1
invokestatic org/xbill/DNS/Type/check(I)V
iload 2
invokestatic org/xbill/DNS/DClass/check(I)V
lload 3
invokestatic org/xbill/DNS/TTL/check(J)V
aload 6
ifnull L4
new org/xbill/DNS/DNSInput
dup
aload 6
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
astore 6
L0:
aload 0
iload 1
iload 2
lload 3
iload 5
aload 6
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJILorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/Record;
astore 0
L1:
aload 0
areturn
L4:
aconst_null
astore 6
goto L0
L2:
astore 0
aconst_null
areturn
.limit locals 7
.limit stack 7
.end method

.method public static newRecord(Lorg/xbill/DNS/Name;IIJ[B)Lorg/xbill/DNS/Record;
aload 0
iload 1
iload 2
lload 3
aload 5
arraylength
aload 5
invokestatic org/xbill/DNS/Record/newRecord(Lorg/xbill/DNS/Name;IIJI[B)Lorg/xbill/DNS/Record;
areturn
.limit locals 6
.limit stack 7
.end method

.method private toWireCanonical(Lorg/xbill/DNS/DNSOutput;Z)V
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/Name/toWireCanonical(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 0
getfield org/xbill/DNS/Record/type I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/Record/dclass I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
iload 2
ifeq L0
aload 1
lconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
L1:
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 3
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
aload 1
aconst_null
iconst_1
invokevirtual org/xbill/DNS/Record/rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
iload 3
isub
iconst_2
isub
iload 3
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
return
L0:
aload 1
aload 0
getfield org/xbill/DNS/Record/ttl J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
goto L1
.limit locals 4
.limit stack 4
.end method

.method private toWireCanonical(Z)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
aload 2
iload 1
invokespecial org/xbill/DNS/Record/toWireCanonical(Lorg/xbill/DNS/DNSOutput;Z)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 3
.limit stack 3
.end method

.method protected static unknownToString([B)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
ldc "\\# "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
arraylength
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method cloneRecord()Lorg/xbill/DNS/Record;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
invokevirtual java/lang/Object/clone()Ljava/lang/Object;
checkcast org/xbill/DNS/Record
astore 1
L1:
aload 1
areturn
L2:
astore 1
new java/lang/IllegalStateException
dup
invokespecial java/lang/IllegalStateException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public compareTo(Ljava/lang/Object;)I
iconst_0
istore 2
aload 1
checkcast org/xbill/DNS/Record
astore 4
aload 0
aload 4
if_acmpne L0
iconst_0
ireturn
L0:
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 4
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/compareTo(Ljava/lang/Object;)I
istore 3
iload 3
ifeq L1
iload 3
ireturn
L1:
aload 0
getfield org/xbill/DNS/Record/dclass I
aload 4
getfield org/xbill/DNS/Record/dclass I
isub
istore 3
iload 3
ifeq L2
iload 3
ireturn
L2:
aload 0
getfield org/xbill/DNS/Record/type I
aload 4
getfield org/xbill/DNS/Record/type I
isub
istore 3
iload 3
ifeq L3
iload 3
ireturn
L3:
aload 0
invokevirtual org/xbill/DNS/Record/rdataToWireCanonical()[B
astore 1
aload 4
invokevirtual org/xbill/DNS/Record/rdataToWireCanonical()[B
astore 4
L4:
iload 2
aload 1
arraylength
if_icmpge L5
iload 2
aload 4
arraylength
if_icmpge L5
aload 1
iload 2
baload
sipush 255
iand
aload 4
iload 2
baload
sipush 255
iand
isub
istore 3
iload 3
ifeq L6
iload 3
ireturn
L6:
iload 2
iconst_1
iadd
istore 2
goto L4
L5:
aload 1
arraylength
aload 4
arraylength
isub
ireturn
.limit locals 5
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
aload 1
ifnull L0
aload 1
instanceof org/xbill/DNS/Record
ifne L1
L0:
iconst_0
ireturn
L1:
aload 1
checkcast org/xbill/DNS/Record
astore 1
aload 0
getfield org/xbill/DNS/Record/type I
aload 1
getfield org/xbill/DNS/Record/type I
if_icmpne L0
aload 0
getfield org/xbill/DNS/Record/dclass I
aload 1
getfield org/xbill/DNS/Record/dclass I
if_icmpne L0
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 1
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L0
aload 0
invokevirtual org/xbill/DNS/Record/rdataToWireCanonical()[B
aload 1
invokevirtual org/xbill/DNS/Record/rdataToWireCanonical()[B
invokestatic java/util/Arrays/equals([B[B)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getAdditionalName()Lorg/xbill/DNS/Name;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method

.method public getDClass()I
aload 0
getfield org/xbill/DNS/Record/dclass I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getName()Lorg/xbill/DNS/Name;
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
areturn
.limit locals 1
.limit stack 1
.end method

.method abstract getObject()Lorg/xbill/DNS/Record;
.end method

.method public getRRsetType()I
aload 0
getfield org/xbill/DNS/Record/type I
bipush 46
if_icmpne L0
aload 0
checkcast org/xbill/DNS/RRSIGRecord
invokevirtual org/xbill/DNS/RRSIGRecord/getTypeCovered()I
ireturn
L0:
aload 0
getfield org/xbill/DNS/Record/type I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public getTTL()J
aload 0
getfield org/xbill/DNS/Record/ttl J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public getType()I
aload 0
getfield org/xbill/DNS/Record/type I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public hashCode()I
iconst_0
istore 1
aload 0
iconst_1
invokespecial org/xbill/DNS/Record/toWireCanonical(Z)[B
astore 3
iconst_0
istore 2
L0:
iload 1
aload 3
arraylength
if_icmpge L1
iload 2
iload 2
iconst_3
ishl
aload 3
iload 1
baload
sipush 255
iand
iadd
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method abstract rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.end method

.method public rdataToString()Ljava/lang/String;
aload 0
invokevirtual org/xbill/DNS/Record/rrToString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public rdataToWireCanonical()[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 1
aload 0
aload 1
aconst_null
iconst_1
invokevirtual org/xbill/DNS/Record/rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 2
.limit stack 4
.end method

.method abstract rrFromWire(Lorg/xbill/DNS/DNSInput;)V
.end method

.method abstract rrToString()Ljava/lang/String;
.end method

.method abstract rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
.end method

.method public sameRRset(Lorg/xbill/DNS/Record;)Z
aload 0
invokevirtual org/xbill/DNS/Record/getRRsetType()I
aload 1
invokevirtual org/xbill/DNS/Record/getRRsetType()I
if_icmpne L0
aload 0
getfield org/xbill/DNS/Record/dclass I
aload 1
getfield org/xbill/DNS/Record/dclass I
if_icmpne L0
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 1
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method setTTL(J)V
aload 0
lload 1
putfield org/xbill/DNS/Record/ttl J
return
.limit locals 3
.limit stack 3
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/length()I
bipush 8
if_icmpge L0
aload 1
ldc "\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 1
invokevirtual java/lang/StringBuffer/length()I
bipush 16
if_icmpge L1
aload 1
ldc "\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
aload 1
ldc "\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
ldc "BINDTTL"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L2
aload 1
aload 0
getfield org/xbill/DNS/Record/ttl J
invokestatic org/xbill/DNS/TTL/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L3:
aload 1
ldc "\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/Record/dclass I
iconst_1
if_icmpne L4
ldc "noPrintIN"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifne L5
L4:
aload 1
aload 0
getfield org/xbill/DNS/Record/dclass I
invokestatic org/xbill/DNS/DClass/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc "\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L5:
aload 1
aload 0
getfield org/xbill/DNS/Record/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
invokevirtual org/xbill/DNS/Record/rrToString()Ljava/lang/String;
astore 2
aload 2
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L6
aload 1
ldc "\u0009"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L6:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L2:
aload 1
aload 0
getfield org/xbill/DNS/Record/ttl J
invokevirtual java/lang/StringBuffer/append(J)Ljava/lang/StringBuffer;
pop
goto L3
.limit locals 3
.limit stack 3
.end method

.method toWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;)V
aload 0
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 1
aload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;)V
aload 1
aload 0
getfield org/xbill/DNS/Record/type I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/Record/dclass I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
iload 2
ifne L0
return
L0:
aload 1
aload 0
getfield org/xbill/DNS/Record/ttl J
invokevirtual org/xbill/DNS/DNSOutput/writeU32(J)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 2
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
aload 1
aload 3
iconst_0
invokevirtual org/xbill/DNS/Record/rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
iload 2
isub
iconst_2
isub
iload 2
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
return
.limit locals 4
.limit stack 4
.end method

.method public toWire(I)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
aload 2
iload 1
aconst_null
invokevirtual org/xbill/DNS/Record/toWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;)V
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 3
.limit stack 4
.end method

.method public toWireCanonical()[B
aload 0
iconst_0
invokespecial org/xbill/DNS/Record/toWireCanonical(Z)[B
areturn
.limit locals 1
.limit stack 2
.end method

.method withDClass(IJ)Lorg/xbill/DNS/Record;
aload 0
invokevirtual org/xbill/DNS/Record/cloneRecord()Lorg/xbill/DNS/Record;
astore 4
aload 4
iload 1
putfield org/xbill/DNS/Record/dclass I
aload 4
lload 2
putfield org/xbill/DNS/Record/ttl J
aload 4
areturn
.limit locals 5
.limit stack 3
.end method

.method public withName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Record;
aload 1
invokevirtual org/xbill/DNS/Name/isAbsolute()Z
ifne L0
new org/xbill/DNS/RelativeNameException
dup
aload 1
invokespecial org/xbill/DNS/RelativeNameException/<init>(Lorg/xbill/DNS/Name;)V
athrow
L0:
aload 0
invokevirtual org/xbill/DNS/Record/cloneRecord()Lorg/xbill/DNS/Record;
astore 2
aload 2
aload 1
putfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
aload 2
areturn
.limit locals 3
.limit stack 3
.end method
