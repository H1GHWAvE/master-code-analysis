.bytecode 50.0
.class public synchronized org/xbill/DNS/IPSECKEYRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 3050449702765909687L


.field private 'algorithmType' I

.field private 'gateway' Ljava/lang/Object;

.field private 'gatewayType' I

.field private 'key' [B

.field private 'precedence' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIIILjava/lang/Object;[B)V
aload 0
aload 1
bipush 45
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "precedence"
iload 5
invokestatic org/xbill/DNS/IPSECKEYRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/IPSECKEYRecord/precedence I
aload 0
ldc "gatewayType"
iload 6
invokestatic org/xbill/DNS/IPSECKEYRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
aload 0
ldc "algorithmType"
iload 7
invokestatic org/xbill/DNS/IPSECKEYRecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/IPSECKEYRecord/algorithmType I
iload 6
tableswitch 0
L0
L1
L2
L3
default : L4
L4:
new java/lang/IllegalArgumentException
dup
ldc "\"gatewayType\" must be between 0 and 3"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aconst_null
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
L5:
aload 0
aload 9
putfield org/xbill/DNS/IPSECKEYRecord/key [B
return
L1:
aload 8
instanceof java/net/InetAddress
ifne L6
new java/lang/IllegalArgumentException
dup
ldc "\"gateway\" must be an IPv4 address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L6:
aload 0
aload 8
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L5
L2:
aload 8
instanceof java/net/Inet6Address
ifne L7
new java/lang/IllegalArgumentException
dup
ldc "\"gateway\" must be an IPv6 address"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L7:
aload 0
aload 8
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L5
L3:
aload 8
instanceof org/xbill/DNS/Name
ifne L8
new java/lang/IllegalArgumentException
dup
ldc "\"gateway\" must be a DNS name"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L8:
aload 0
ldc "gateway"
aload 8
checkcast org/xbill/DNS/Name
invokestatic org/xbill/DNS/IPSECKEYRecord/checkName(Ljava/lang/String;Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L5
.limit locals 10
.limit stack 6
.end method

.method public getAlgorithmType()I
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/algorithmType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getGateway()Ljava/lang/Object;
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getGatewayType()I
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getKey()[B
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/key [B
areturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/IPSECKEYRecord
dup
invokespecial org/xbill/DNS/IPSECKEYRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getPrecedence()I
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/precedence I
ireturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/IPSECKEYRecord/precedence I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/IPSECKEYRecord/algorithmType I
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
tableswitch 0
L0
L1
L2
L3
default : L4
L4:
new org/xbill/DNS/WireParseException
dup
ldc "invalid gateway type"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
ldc "."
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L5
new org/xbill/DNS/TextParseException
dup
ldc "invalid gateway format"
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
aconst_null
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
L6:
aload 0
aload 1
iconst_0
invokevirtual org/xbill/DNS/Tokenizer/getBase64(Z)[B
putfield org/xbill/DNS/IPSECKEYRecord/key [B
return
L1:
aload 0
aload 1
iconst_1
invokevirtual org/xbill/DNS/Tokenizer/getAddress(I)Ljava/net/InetAddress;
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L6
L2:
aload 0
aload 1
iconst_2
invokevirtual org/xbill/DNS/Tokenizer/getAddress(I)Ljava/net/InetAddress;
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L6
L3:
aload 0
aload 1
aload 2
invokevirtual org/xbill/DNS/Tokenizer/getName(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Name;
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L6
.limit locals 3
.limit stack 3
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/IPSECKEYRecord/precedence I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/IPSECKEYRecord/algorithmType I
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
tableswitch 0
L0
L1
L2
L3
default : L4
L4:
new org/xbill/DNS/WireParseException
dup
ldc "invalid gateway type"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
aconst_null
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
L5:
aload 1
invokevirtual org/xbill/DNS/DNSInput/remaining()I
ifle L6
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/IPSECKEYRecord/key [B
L6:
return
L1:
aload 0
aload 1
iconst_4
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L5
L2:
aload 0
aload 1
bipush 16
invokevirtual org/xbill/DNS/DNSInput/readByteArray(I)[B
invokestatic java/net/InetAddress/getByAddress([B)Ljava/net/InetAddress;
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L5
L3:
aload 0
new org/xbill/DNS/Name
dup
aload 1
invokespecial org/xbill/DNS/Name/<init>(Lorg/xbill/DNS/DNSInput;)V
putfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
goto L5
.limit locals 2
.limit stack 4
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/precedence I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/algorithmType I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
tableswitch 0
L0
L1
L1
L2
default : L3
L3:
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/key [B
ifnull L4
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/key [B
invokestatic org/xbill/DNS/utils/base64/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L0:
aload 1
ldc "."
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L3
L1:
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
checkcast java/net/InetAddress
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L3
L2:
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
goto L3
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/precedence I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/algorithmType I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gatewayType I
tableswitch 0
L0
L1
L1
L2
default : L0
L0:
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/key [B
ifnull L3
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/key [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
L3:
return
L1:
aload 1
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
checkcast java/net/InetAddress
invokevirtual java/net/InetAddress/getAddress()[B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
goto L0
L2:
aload 0
getfield org/xbill/DNS/IPSECKEYRecord/gateway Ljava/lang/Object;
checkcast org/xbill/DNS/Name
aload 1
aconst_null
iload 3
invokevirtual org/xbill/DNS/Name/toWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
goto L0
.limit locals 4
.limit stack 4
.end method
