.bytecode 50.0
.class public synchronized org/xbill/DNS/KEYRecord$Flags
.super java/lang/Object

.field public static final 'EXTEND' I = 4096


.field public static final 'FLAG10' I = 32


.field public static final 'FLAG11' I = 16


.field public static final 'FLAG2' I = 8192


.field public static final 'FLAG4' I = 2048


.field public static final 'FLAG5' I = 1024


.field public static final 'FLAG8' I = 128


.field public static final 'FLAG9' I = 64


.field public static final 'HOST' I = 512


.field public static final 'NOAUTH' I = 32768


.field public static final 'NOCONF' I = 16384


.field public static final 'NOKEY' I = 49152


.field public static final 'NTYP3' I = 768


.field public static final 'OWNER_MASK' I = 768


.field public static final 'SIG0' I = 0


.field public static final 'SIG1' I = 1


.field public static final 'SIG10' I = 10


.field public static final 'SIG11' I = 11


.field public static final 'SIG12' I = 12


.field public static final 'SIG13' I = 13


.field public static final 'SIG14' I = 14


.field public static final 'SIG15' I = 15


.field public static final 'SIG2' I = 2


.field public static final 'SIG3' I = 3


.field public static final 'SIG4' I = 4


.field public static final 'SIG5' I = 5


.field public static final 'SIG6' I = 6


.field public static final 'SIG7' I = 7


.field public static final 'SIG8' I = 8


.field public static final 'SIG9' I = 9


.field public static final 'USER' I = 0


.field public static final 'USE_MASK' I = 49152


.field public static final 'ZONE' I = 256


.field private static 'flags' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "KEY flags"
iconst_2
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
aload 0
ldc_w 65535
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_0
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 16384
ldc "NOCONF"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
ldc_w 32768
ldc "NOAUTH"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
ldc_w 49152
ldc "NOKEY"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 8192
ldc "FLAG2"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 4096
ldc "EXTEND"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 2048
ldc "FLAG4"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 1024
ldc "FLAG5"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "USER"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 256
ldc "ZONE"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 512
ldc "HOST"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 768
ldc "NTYP3"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
sipush 128
ldc "FLAG8"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 64
ldc "FLAG9"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 32
ldc "FLAG10"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 16
ldc "FLAG11"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "SIG0"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_1
ldc "SIG1"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_2
ldc "SIG2"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_3
ldc "SIG3"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_4
ldc "SIG4"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "SIG5"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 6
ldc "SIG6"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 7
ldc "SIG7"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 8
ldc "SIG8"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 9
ldc "SIG9"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 10
ldc "SIG10"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 11
ldc "SIG11"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 12
ldc "SIG12"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 13
ldc "SIG13"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 14
ldc "SIG14"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 15
ldc "SIG15"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static value(Ljava/lang/String;)I
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 0
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
istore 2
L1:
iload 2
iflt L3
iload 2
ldc_w 65535
if_icmpgt L3
L4:
iload 2
ireturn
L3:
iconst_m1
ireturn
L2:
astore 3
new java/util/StringTokenizer
dup
aload 0
ldc "|"
invokespecial java/util/StringTokenizer/<init>(Ljava/lang/String;Ljava/lang/String;)V
astore 0
iconst_0
istore 1
L5:
iload 1
istore 2
aload 0
invokevirtual java/util/StringTokenizer/hasMoreTokens()Z
ifeq L4
getstatic org/xbill/DNS/KEYRecord$Flags/flags Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual java/util/StringTokenizer/nextToken()Ljava/lang/String;
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
istore 2
iload 2
ifge L6
iconst_m1
ireturn
L6:
iload 1
iload 2
ior
istore 1
goto L5
.limit locals 4
.limit stack 4
.end method
