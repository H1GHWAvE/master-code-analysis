.bytecode 50.0
.class synchronized org/xbill/DNS/Cache$NegativeElement
.super java/lang/Object
.implements org/xbill/DNS/Cache$Element

.field 'credibility' I

.field 'expire' I

.field 'name' Lorg/xbill/DNS/Name;

.field 'type' I

.method public <init>(Lorg/xbill/DNS/Name;ILorg/xbill/DNS/SOARecord;IJ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield org/xbill/DNS/Cache$NegativeElement/name Lorg/xbill/DNS/Name;
aload 0
iload 2
putfield org/xbill/DNS/Cache$NegativeElement/type I
lconst_0
lstore 7
aload 3
ifnull L0
aload 3
invokevirtual org/xbill/DNS/SOARecord/getMinimum()J
lstore 7
L0:
aload 0
iload 4
putfield org/xbill/DNS/Cache$NegativeElement/credibility I
aload 0
lload 7
lload 5
invokestatic org/xbill/DNS/Cache/access$000(JJ)I
putfield org/xbill/DNS/Cache$NegativeElement/expire I
return
.limit locals 9
.limit stack 5
.end method

.method public final compareCredibility(I)I
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/credibility I
iload 1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final expired()Z
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 1000L
ldiv
l2i
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/expire I
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 4
.end method

.method public getType()I
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/type I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/type I
ifne L0
aload 1
new java/lang/StringBuffer
dup
ldc "NXDOMAIN "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/name Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
aload 1
ldc " cl = "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/credibility I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L0:
aload 1
new java/lang/StringBuffer
dup
ldc "NXRRSET "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/name Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
aload 0
getfield org/xbill/DNS/Cache$NegativeElement/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L1
.limit locals 2
.limit stack 4
.end method
