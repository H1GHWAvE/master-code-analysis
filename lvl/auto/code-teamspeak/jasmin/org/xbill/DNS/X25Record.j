.bytecode 50.0
.class public synchronized org/xbill/DNS/X25Record
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 4267576252335579764L


.field private 'address' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/lang/String;)V
aload 0
aload 1
bipush 19
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
aload 5
invokestatic org/xbill/DNS/X25Record/checkAndConvertAddress(Ljava/lang/String;)[B
putfield org/xbill/DNS/X25Record/address [B
aload 0
getfield org/xbill/DNS/X25Record/address [B
ifnonnull L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "invalid PSDN address "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 5
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 6
.limit stack 6
.end method

.method private static final checkAndConvertAddress(Ljava/lang/String;)[B
aload 0
invokevirtual java/lang/String/length()I
istore 3
iload 3
newarray byte
astore 5
iconst_0
istore 2
L0:
aload 5
astore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
iload 1
invokestatic java/lang/Character/isDigit(C)Z
ifne L2
aconst_null
astore 4
L1:
aload 4
areturn
L2:
aload 5
iload 2
iload 1
i2b
bastore
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 3
.end method

.method public getAddress()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/X25Record/address [B
iconst_0
invokestatic org/xbill/DNS/X25Record/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/X25Record
dup
invokespecial org/xbill/DNS/X25Record/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/X25Record/checkAndConvertAddress(Ljava/lang/String;)[B
putfield org/xbill/DNS/X25Record/address [B
aload 0
getfield org/xbill/DNS/X25Record/address [B
ifnonnull L0
aload 1
new java/lang/StringBuffer
dup
ldc "invalid PSDN address "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
return
.limit locals 3
.limit stack 4
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/X25Record/address [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/X25Record/address [B
iconst_1
invokestatic org/xbill/DNS/X25Record/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/X25Record/address [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
return
.limit locals 4
.limit stack 2
.end method
