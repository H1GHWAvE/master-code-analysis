.bytecode 50.0
.class public synchronized org/xbill/DNS/Message
.super java/lang/Object
.implements java/lang/Cloneable

.field public static final 'MAXLENGTH' I = 65535


.field static final 'TSIG_FAILED' I = 4


.field static final 'TSIG_INTERMEDIATE' I = 2


.field static final 'TSIG_SIGNED' I = 3


.field static final 'TSIG_UNSIGNED' I = 0


.field static final 'TSIG_VERIFIED' I = 1


.field private static 'emptyRRsetArray' [Lorg/xbill/DNS/RRset;

.field private static 'emptyRecordArray' [Lorg/xbill/DNS/Record;

.field private 'header' Lorg/xbill/DNS/Header;

.field private 'querytsig' Lorg/xbill/DNS/TSIGRecord;

.field private 'sections' [Ljava/util/List;

.field 'sig0start' I

.field private 'size' I

.field 'tsigState' I

.field private 'tsigerror' I

.field private 'tsigkey' Lorg/xbill/DNS/TSIG;

.field 'tsigstart' I

.method static <clinit>()V
iconst_0
anewarray org/xbill/DNS/Record
putstatic org/xbill/DNS/Message/emptyRecordArray [Lorg/xbill/DNS/Record;
iconst_0
anewarray org/xbill/DNS/RRset
putstatic org/xbill/DNS/Message/emptyRRsetArray [Lorg/xbill/DNS/RRset;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
new org/xbill/DNS/Header
dup
invokespecial org/xbill/DNS/Header/<init>()V
invokespecial org/xbill/DNS/Message/<init>(Lorg/xbill/DNS/Header;)V
return
.limit locals 1
.limit stack 3
.end method

.method public <init>(I)V
aload 0
new org/xbill/DNS/Header
dup
iload 1
invokespecial org/xbill/DNS/Header/<init>(I)V
invokespecial org/xbill/DNS/Message/<init>(Lorg/xbill/DNS/Header;)V
return
.limit locals 2
.limit stack 4
.end method

.method <init>(Lorg/xbill/DNS/DNSInput;)V
.catch org/xbill/DNS/WireParseException from L0 to L1 using L2
.catch org/xbill/DNS/WireParseException from L3 to L4 using L2
.catch org/xbill/DNS/WireParseException from L5 to L6 using L2
.catch org/xbill/DNS/WireParseException from L7 to L8 using L2
.catch org/xbill/DNS/WireParseException from L8 to L9 using L2
aload 0
new org/xbill/DNS/Header
dup
aload 1
invokespecial org/xbill/DNS/Header/<init>(Lorg/xbill/DNS/DNSInput;)V
invokespecial org/xbill/DNS/Message/<init>(Lorg/xbill/DNS/Header;)V
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getOpcode()I
iconst_5
if_icmpne L10
iconst_1
istore 6
L11:
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
bipush 6
invokevirtual org/xbill/DNS/Header/getFlag(I)Z
istore 7
iconst_0
istore 2
L12:
iload 2
iconst_4
if_icmpge L13
L0:
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iload 2
invokevirtual org/xbill/DNS/Header/getCount(I)I
istore 4
L1:
iload 4
ifle L14
L3:
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
new java/util/ArrayList
dup
iload 4
invokespecial java/util/ArrayList/<init>(I)V
aastore
L4:
goto L14
L15:
iload 3
iload 4
if_icmpge L16
L5:
aload 1
invokevirtual org/xbill/DNS/DNSInput/current()I
istore 5
aload 1
iload 2
iload 6
invokestatic org/xbill/DNS/Record/fromWire(Lorg/xbill/DNS/DNSInput;IZ)Lorg/xbill/DNS/Record;
astore 8
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
aload 8
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L6:
iload 2
iconst_3
if_icmpne L9
L7:
aload 8
invokevirtual org/xbill/DNS/Record/getType()I
sipush 250
if_icmpne L8
aload 0
iload 5
putfield org/xbill/DNS/Message/tsigstart I
L8:
aload 8
invokevirtual org/xbill/DNS/Record/getType()I
bipush 24
if_icmpne L9
aload 8
checkcast org/xbill/DNS/SIGRecord
invokevirtual org/xbill/DNS/SIGRecord/getTypeCovered()I
ifne L9
aload 0
iload 5
putfield org/xbill/DNS/Message/sig0start I
L9:
iload 3
iconst_1
iadd
istore 3
goto L15
L10:
iconst_0
istore 6
goto L11
L16:
iload 2
iconst_1
iadd
istore 2
goto L12
L2:
astore 8
iload 7
ifne L13
aload 8
athrow
L13:
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/current()I
putfield org/xbill/DNS/Message/size I
return
L14:
iconst_0
istore 3
goto L15
.limit locals 9
.limit stack 5
.end method

.method private <init>(Lorg/xbill/DNS/Header;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_4
anewarray java/util/List
putfield org/xbill/DNS/Message/sections [Ljava/util/List;
aload 0
aload 1
putfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
return
.limit locals 2
.limit stack 2
.end method

.method public <init>([B)V
aload 0
new org/xbill/DNS/DNSInput
dup
aload 1
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
invokespecial org/xbill/DNS/Message/<init>(Lorg/xbill/DNS/DNSInput;)V
return
.limit locals 2
.limit stack 4
.end method

.method public static newQuery(Lorg/xbill/DNS/Record;)Lorg/xbill/DNS/Message;
new org/xbill/DNS/Message
dup
invokespecial org/xbill/DNS/Message/<init>()V
astore 1
aload 1
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iconst_0
invokevirtual org/xbill/DNS/Header/setOpcode(I)V
aload 1
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
bipush 7
invokevirtual org/xbill/DNS/Header/setFlag(I)V
aload 1
aload 0
iconst_0
invokevirtual org/xbill/DNS/Message/addRecord(Lorg/xbill/DNS/Record;I)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method public static newUpdate(Lorg/xbill/DNS/Name;)Lorg/xbill/DNS/Message;
new org/xbill/DNS/Update
dup
aload 0
invokespecial org/xbill/DNS/Update/<init>(Lorg/xbill/DNS/Name;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static sameSet(Lorg/xbill/DNS/Record;Lorg/xbill/DNS/Record;)Z
aload 0
invokevirtual org/xbill/DNS/Record/getRRsetType()I
aload 1
invokevirtual org/xbill/DNS/Record/getRRsetType()I
if_icmpne L0
aload 0
invokevirtual org/xbill/DNS/Record/getDClass()I
aload 1
invokevirtual org/xbill/DNS/Record/getDClass()I
if_icmpne L0
aload 0
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
aload 1
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private sectionToWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;I)I
iconst_0
istore 8
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
invokeinterface java/util/List/size()I 0
istore 10
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 6
aconst_null
astore 11
iconst_0
istore 5
iconst_0
istore 7
L0:
iload 5
istore 9
iload 8
iload 10
if_icmpge L1
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
iload 8
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
astore 12
iload 2
iconst_3
if_icmpne L2
aload 12
instanceof org/xbill/DNS/OPTRecord
ifeq L2
iload 5
iconst_1
iadd
istore 5
L3:
iload 8
iconst_1
iadd
istore 8
goto L0
L2:
aload 11
ifnull L4
aload 12
aload 11
invokestatic org/xbill/DNS/Message/sameSet(Lorg/xbill/DNS/Record;Lorg/xbill/DNS/Record;)Z
ifne L4
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 6
iload 8
istore 7
L5:
aload 12
aload 1
iload 2
aload 3
invokevirtual org/xbill/DNS/Record/toWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
iload 4
if_icmple L6
aload 1
iload 6
invokevirtual org/xbill/DNS/DNSOutput/jump(I)V
iload 5
iload 10
iload 7
isub
iadd
istore 9
L1:
iload 9
ireturn
L4:
goto L5
L6:
aload 12
astore 11
goto L3
.limit locals 13
.limit stack 4
.end method

.method private toWire(Lorg/xbill/DNS/DNSOutput;I)Z
iload 2
bipush 12
if_icmpge L0
iconst_0
ireturn
L0:
iload 2
istore 3
aload 0
getfield org/xbill/DNS/Message/tsigkey Lorg/xbill/DNS/TSIG;
ifnull L1
iload 2
aload 0
getfield org/xbill/DNS/Message/tsigkey Lorg/xbill/DNS/TSIG;
invokevirtual org/xbill/DNS/TSIG/recordLength()I
isub
istore 3
L1:
aload 0
invokevirtual org/xbill/DNS/Message/getOPT()Lorg/xbill/DNS/OPTRecord;
astore 10
aconst_null
astore 9
iload 3
istore 4
aload 10
ifnull L2
aload 10
iconst_3
invokevirtual org/xbill/DNS/OPTRecord/toWire(I)[B
astore 9
iload 3
aload 9
arraylength
isub
istore 4
L2:
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 7
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
aload 1
invokevirtual org/xbill/DNS/Header/toWire(Lorg/xbill/DNS/DNSOutput;)V
new org/xbill/DNS/Compression
dup
invokespecial org/xbill/DNS/Compression/<init>()V
astore 10
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getFlagsByte()I
istore 6
iconst_0
istore 3
iconst_0
istore 2
L3:
iload 3
iconst_4
if_icmpge L4
iload 2
istore 5
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 3
aaload
ifnull L5
aload 0
aload 1
iload 3
aload 10
iload 4
invokespecial org/xbill/DNS/Message/sectionToWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;I)I
istore 8
iload 8
ifeq L6
iload 3
iconst_3
if_icmpeq L6
iload 6
bipush 6
iconst_1
invokestatic org/xbill/DNS/Header/setFlag(IIZ)I
istore 4
aload 1
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iload 3
invokevirtual org/xbill/DNS/Header/getCount(I)I
iload 8
isub
iload 7
iconst_4
iadd
iload 3
iconst_2
imul
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
iload 3
iconst_1
iadd
istore 3
L7:
iload 3
iconst_3
if_icmpge L8
aload 1
iconst_0
iload 7
iconst_4
iadd
iload 3
iconst_2
imul
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
iload 3
iconst_1
iadd
istore 3
goto L7
L8:
iload 4
istore 3
L9:
iload 2
istore 4
aload 9
ifnull L10
aload 1
aload 9
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
iload 2
iconst_1
iadd
istore 4
L10:
iload 3
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getFlagsByte()I
if_icmpeq L11
aload 1
iload 3
iload 7
iconst_2
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
L11:
iload 4
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/getCount(I)I
if_icmpeq L12
aload 1
iload 4
iload 7
bipush 10
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
L12:
aload 0
getfield org/xbill/DNS/Message/tsigkey Lorg/xbill/DNS/TSIG;
ifnull L13
aload 0
getfield org/xbill/DNS/Message/tsigkey Lorg/xbill/DNS/TSIG;
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
aload 0
getfield org/xbill/DNS/Message/tsigerror I
aload 0
getfield org/xbill/DNS/Message/querytsig Lorg/xbill/DNS/TSIGRecord;
invokevirtual org/xbill/DNS/TSIG/generate(Lorg/xbill/DNS/Message;[BILorg/xbill/DNS/TSIGRecord;)Lorg/xbill/DNS/TSIGRecord;
aload 1
iconst_3
aload 10
invokevirtual org/xbill/DNS/TSIGRecord/toWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;)V
aload 1
iload 4
iconst_1
iadd
iload 7
bipush 10
iadd
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
L13:
iconst_1
ireturn
L6:
iload 2
istore 5
iload 3
iconst_3
if_icmpne L5
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iload 3
invokevirtual org/xbill/DNS/Header/getCount(I)I
iload 8
isub
istore 5
L5:
iload 3
iconst_1
iadd
istore 3
iload 5
istore 2
goto L3
L4:
iload 6
istore 3
goto L9
.limit locals 11
.limit stack 5
.end method

.method public addRecord(Lorg/xbill/DNS/Record;I)V
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
ifnonnull L0
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
aastore
L0:
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iload 2
invokevirtual org/xbill/DNS/Header/incCount(I)V
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 3
.limit stack 4
.end method

.method public clone()Ljava/lang/Object;
new org/xbill/DNS/Message
dup
invokespecial org/xbill/DNS/Message/<init>()V
astore 2
iconst_0
istore 1
L0:
iload 1
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
arraylength
if_icmpge L1
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
aaload
ifnull L2
aload 2
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
new java/util/LinkedList
dup
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
aaload
invokespecial java/util/LinkedList/<init>(Ljava/util/Collection;)V
aastore
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 2
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/clone()Ljava/lang/Object;
checkcast org/xbill/DNS/Header
putfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
aload 2
aload 0
getfield org/xbill/DNS/Message/size I
putfield org/xbill/DNS/Message/size I
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method public findRRset(Lorg/xbill/DNS/Name;I)Z
aload 0
aload 1
iload 2
iconst_1
invokevirtual org/xbill/DNS/Message/findRRset(Lorg/xbill/DNS/Name;II)Z
ifne L0
aload 0
aload 1
iload 2
iconst_2
invokevirtual org/xbill/DNS/Message/findRRset(Lorg/xbill/DNS/Name;II)Z
ifne L0
aload 0
aload 1
iload 2
iconst_3
invokevirtual org/xbill/DNS/Message/findRRset(Lorg/xbill/DNS/Name;II)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 4
.end method

.method public findRRset(Lorg/xbill/DNS/Name;II)Z
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 3
aaload
ifnonnull L0
L1:
iconst_0
ireturn
L0:
iconst_0
istore 4
L2:
iload 4
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 3
aaload
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 3
aaload
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
astore 5
aload 5
invokevirtual org/xbill/DNS/Record/getType()I
iload 2
if_icmpne L3
aload 1
aload 5
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L3
iconst_1
ireturn
L3:
iload 4
iconst_1
iadd
istore 4
goto L2
.limit locals 6
.limit stack 3
.end method

.method public findRecord(Lorg/xbill/DNS/Record;)Z
iconst_1
istore 2
L0:
iload 2
iconst_3
if_icmpgt L1
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
ifnull L2
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
aload 1
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifeq L2
iconst_1
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public findRecord(Lorg/xbill/DNS/Record;I)Z
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
ifnull L0
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
aload 1
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public getHeader()Lorg/xbill/DNS/Header;
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getOPT()Lorg/xbill/DNS/OPTRecord;
aload 0
iconst_3
invokevirtual org/xbill/DNS/Message/getSectionArray(I)[Lorg/xbill/DNS/Record;
astore 2
iconst_0
istore 1
L0:
iload 1
aload 2
arraylength
if_icmpge L1
aload 2
iload 1
aaload
instanceof org/xbill/DNS/OPTRecord
ifeq L2
aload 2
iload 1
aaload
checkcast org/xbill/DNS/OPTRecord
areturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method public getQuestion()Lorg/xbill/DNS/Record;
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iconst_0
aaload
astore 1
aload 1
ifnull L0
aload 1
invokeinterface java/util/List/size()I 0
ifne L1
L0:
aconst_null
areturn
L1:
aload 1
iconst_0
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
areturn
.limit locals 2
.limit stack 2
.end method

.method public getRcode()I
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getRcode()I
istore 2
aload 0
invokevirtual org/xbill/DNS/Message/getOPT()Lorg/xbill/DNS/OPTRecord;
astore 3
iload 2
istore 1
aload 3
ifnull L0
iload 2
aload 3
invokevirtual org/xbill/DNS/OPTRecord/getExtendedRcode()I
iconst_4
ishl
iadd
istore 1
L0:
iload 1
ireturn
.limit locals 4
.limit stack 3
.end method

.method public getSectionArray(I)[Lorg/xbill/DNS/Record;
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
aaload
ifnonnull L0
getstatic org/xbill/DNS/Message/emptyRecordArray [Lorg/xbill/DNS/Record;
areturn
L0:
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
aaload
astore 2
aload 2
aload 2
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Record
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Record;
checkcast [Lorg/xbill/DNS/Record;
areturn
.limit locals 3
.limit stack 2
.end method

.method public getSectionRRsets(I)[Lorg/xbill/DNS/RRset;
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
aaload
ifnonnull L0
getstatic org/xbill/DNS/Message/emptyRRsetArray [Lorg/xbill/DNS/RRset;
areturn
L0:
new java/util/LinkedList
dup
invokespecial java/util/LinkedList/<init>()V
astore 3
aload 0
iload 1
invokevirtual org/xbill/DNS/Message/getSectionArray(I)[Lorg/xbill/DNS/Record;
astore 4
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
astore 5
iconst_0
istore 1
L1:
iload 1
aload 4
arraylength
if_icmpge L2
aload 4
iload 1
aaload
invokevirtual org/xbill/DNS/Record/getName()Lorg/xbill/DNS/Name;
astore 6
aload 5
aload 6
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L3
aload 3
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 2
L4:
iload 2
iflt L3
aload 3
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/RRset
astore 7
aload 7
invokevirtual org/xbill/DNS/RRset/getType()I
aload 4
iload 1
aaload
invokevirtual org/xbill/DNS/Record/getRRsetType()I
if_icmpne L5
aload 7
invokevirtual org/xbill/DNS/RRset/getDClass()I
aload 4
iload 1
aaload
invokevirtual org/xbill/DNS/Record/getDClass()I
if_icmpne L5
aload 7
invokevirtual org/xbill/DNS/RRset/getName()Lorg/xbill/DNS/Name;
aload 6
invokevirtual org/xbill/DNS/Name/equals(Ljava/lang/Object;)Z
ifeq L5
aload 7
aload 4
iload 1
aaload
invokevirtual org/xbill/DNS/RRset/addRR(Lorg/xbill/DNS/Record;)V
iconst_0
istore 2
L6:
iload 2
ifeq L7
aload 3
new org/xbill/DNS/RRset
dup
aload 4
iload 1
aaload
invokespecial org/xbill/DNS/RRset/<init>(Lorg/xbill/DNS/Record;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 5
aload 6
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
L7:
iload 1
iconst_1
iadd
istore 1
goto L1
L5:
iload 2
iconst_1
isub
istore 2
goto L4
L2:
aload 3
aload 3
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/RRset
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/RRset;
checkcast [Lorg/xbill/DNS/RRset;
areturn
L3:
iconst_1
istore 2
goto L6
.limit locals 8
.limit stack 5
.end method

.method public getTSIG()Lorg/xbill/DNS/TSIGRecord;
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iconst_3
invokevirtual org/xbill/DNS/Header/getCount(I)I
istore 1
iload 1
ifne L0
aconst_null
areturn
L0:
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iconst_3
aaload
iload 1
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
astore 2
aload 2
getfield org/xbill/DNS/Record/type I
sipush 250
if_icmpeq L1
aconst_null
areturn
L1:
aload 2
checkcast org/xbill/DNS/TSIGRecord
areturn
.limit locals 3
.limit stack 3
.end method

.method public isSigned()Z
aload 0
getfield org/xbill/DNS/Message/tsigState I
iconst_3
if_icmpeq L0
aload 0
getfield org/xbill/DNS/Message/tsigState I
iconst_1
if_icmpeq L0
aload 0
getfield org/xbill/DNS/Message/tsigState I
iconst_4
if_icmpne L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public isVerified()Z
aload 0
getfield org/xbill/DNS/Message/tsigState I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public numBytes()I
aload 0
getfield org/xbill/DNS/Message/size I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public removeAllRecords(I)V
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 1
aconst_null
aastore
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iload 1
iconst_0
invokevirtual org/xbill/DNS/Header/setCount(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public removeRecord(Lorg/xbill/DNS/Record;I)Z
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
ifnull L0
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
aload 1
invokeinterface java/util/List/remove(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
iload 2
invokevirtual org/xbill/DNS/Header/decCount(I)V
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public sectionToString(I)Ljava/lang/String;
iload 1
iconst_3
if_icmple L0
aconst_null
areturn
L0:
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 3
aload 0
iload 1
invokevirtual org/xbill/DNS/Message/getSectionArray(I)[Lorg/xbill/DNS/Record;
astore 4
iconst_0
istore 2
L1:
iload 2
aload 4
arraylength
if_icmpge L2
aload 4
iload 2
aaload
astore 5
iload 1
ifne L3
aload 3
new java/lang/StringBuffer
dup
ldc ";;\u0009"
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 5
getfield org/xbill/DNS/Record/name Lorg/xbill/DNS/Name;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 3
new java/lang/StringBuffer
dup
ldc ", type = "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 5
getfield org/xbill/DNS/Record/type I
invokestatic org/xbill/DNS/Type/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 3
new java/lang/StringBuffer
dup
ldc ", class = "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 5
getfield org/xbill/DNS/Record/dclass I
invokestatic org/xbill/DNS/DClass/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
aload 3
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L3:
aload 3
aload 5
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
pop
goto L4
L2:
aload 3
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 6
.limit stack 4
.end method

.method public setHeader(Lorg/xbill/DNS/Header;)V
aload 0
aload 1
putfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
return
.limit locals 2
.limit stack 2
.end method

.method public setTSIG(Lorg/xbill/DNS/TSIG;ILorg/xbill/DNS/TSIGRecord;)V
aload 0
aload 1
putfield org/xbill/DNS/Message/tsigkey Lorg/xbill/DNS/TSIG;
aload 0
iload 2
putfield org/xbill/DNS/Message/tsigerror I
aload 0
aload 3
putfield org/xbill/DNS/Message/querytsig Lorg/xbill/DNS/TSIGRecord;
return
.limit locals 4
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
aload 0
invokevirtual org/xbill/DNS/Message/getOPT()Lorg/xbill/DNS/OPTRecord;
ifnull L0
aload 2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
aload 0
invokevirtual org/xbill/DNS/Message/getRcode()I
invokevirtual org/xbill/DNS/Header/toStringWithRcode(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L1:
aload 0
invokevirtual org/xbill/DNS/Message/isSigned()Z
ifeq L2
aload 2
ldc ";; TSIG "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
invokevirtual org/xbill/DNS/Message/isVerified()Z
ifeq L3
aload 2
ldc "ok"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L4:
aload 2
bipush 10
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
L2:
iconst_0
istore 1
L5:
iload 1
iconst_4
if_icmpge L6
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual org/xbill/DNS/Header/getOpcode()I
iconst_5
if_icmpeq L7
aload 2
new java/lang/StringBuffer
dup
ldc ";; "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokestatic org/xbill/DNS/Section/longString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ":\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L8:
aload 2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
iload 1
invokevirtual org/xbill/DNS/Message/sectionToString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
iload 1
iconst_1
iadd
istore 1
goto L5
L0:
aload 2
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/Object;)Ljava/lang/StringBuffer;
ldc "\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L1
L3:
aload 2
ldc "invalid"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L4
L7:
aload 2
new java/lang/StringBuffer
dup
ldc ";; "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
iload 1
invokestatic org/xbill/DNS/Section/updString(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ":\n"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
goto L8
L6:
aload 2
new java/lang/StringBuffer
dup
ldc ";; Message size: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual org/xbill/DNS/Message/numBytes()I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " bytes"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 0
getfield org/xbill/DNS/Message/header Lorg/xbill/DNS/Header;
aload 1
invokevirtual org/xbill/DNS/Header/toWire(Lorg/xbill/DNS/DNSOutput;)V
new org/xbill/DNS/Compression
dup
invokespecial org/xbill/DNS/Compression/<init>()V
astore 4
iconst_0
istore 2
L0:
iload 2
iconst_4
if_icmpge L1
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
ifnull L2
iconst_0
istore 3
L3:
iload 3
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
invokeinterface java/util/List/size()I 0
if_icmpge L2
aload 0
getfield org/xbill/DNS/Message/sections [Ljava/util/List;
iload 2
aaload
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Record
aload 1
iload 2
aload 4
invokevirtual org/xbill/DNS/Record/toWire(Lorg/xbill/DNS/DNSOutput;ILorg/xbill/DNS/Compression;)V
iload 3
iconst_1
iadd
istore 3
goto L3
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 5
.limit stack 4
.end method

.method public toWire()[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 1
aload 0
aload 1
invokevirtual org/xbill/DNS/Message/toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
putfield org/xbill/DNS/Message/size I
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public toWire(I)[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 2
aload 0
aload 2
iload 1
invokespecial org/xbill/DNS/Message/toWire(Lorg/xbill/DNS/DNSOutput;I)Z
pop
aload 0
aload 2
invokevirtual org/xbill/DNS/DNSOutput/current()I
putfield org/xbill/DNS/Message/size I
aload 2
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 3
.limit stack 3
.end method
