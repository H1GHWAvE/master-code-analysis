.bytecode 50.0
.class public synchronized org/xbill/DNS/ExtendedResolver
.super java/lang/Object
.implements org/xbill/DNS/Resolver

.field private static final 'quantum' I = 5


.field private 'lbStart' I

.field private 'loadBalance' Z

.field private 'resolvers' Ljava/util/List;

.field private 'retries' I

.method public <init>()V
iconst_0
istore 1
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield org/xbill/DNS/ExtendedResolver/loadBalance Z
aload 0
iconst_0
putfield org/xbill/DNS/ExtendedResolver/lbStart I
aload 0
iconst_3
putfield org/xbill/DNS/ExtendedResolver/retries I
aload 0
invokespecial org/xbill/DNS/ExtendedResolver/init()V
invokestatic org/xbill/DNS/ResolverConfig/getCurrentConfig()Lorg/xbill/DNS/ResolverConfig;
invokevirtual org/xbill/DNS/ResolverConfig/servers()[Ljava/lang/String;
astore 2
aload 2
ifnull L0
L1:
iload 1
aload 2
arraylength
if_icmpge L2
new org/xbill/DNS/SimpleResolver
dup
aload 2
iload 1
aaload
invokespecial org/xbill/DNS/SimpleResolver/<init>(Ljava/lang/String;)V
astore 3
aload 3
iconst_5
invokeinterface org/xbill/DNS/Resolver/setTimeout(I)V 1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 1
iconst_1
iadd
istore 1
goto L1
L0:
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
new org/xbill/DNS/SimpleResolver
dup
invokespecial org/xbill/DNS/SimpleResolver/<init>()V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L2:
return
.limit locals 4
.limit stack 4
.end method

.method public <init>([Ljava/lang/String;)V
iconst_0
istore 2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield org/xbill/DNS/ExtendedResolver/loadBalance Z
aload 0
iconst_0
putfield org/xbill/DNS/ExtendedResolver/lbStart I
aload 0
iconst_3
putfield org/xbill/DNS/ExtendedResolver/retries I
aload 0
invokespecial org/xbill/DNS/ExtendedResolver/init()V
L0:
iload 2
aload 1
arraylength
if_icmpge L1
new org/xbill/DNS/SimpleResolver
dup
aload 1
iload 2
aaload
invokespecial org/xbill/DNS/SimpleResolver/<init>(Ljava/lang/String;)V
astore 3
aload 3
iconst_5
invokeinterface org/xbill/DNS/Resolver/setTimeout(I)V 1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method public <init>([Lorg/xbill/DNS/Resolver;)V
iconst_0
istore 2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield org/xbill/DNS/ExtendedResolver/loadBalance Z
aload 0
iconst_0
putfield org/xbill/DNS/ExtendedResolver/lbStart I
aload 0
iconst_3
putfield org/xbill/DNS/ExtendedResolver/retries I
aload 0
invokespecial org/xbill/DNS/ExtendedResolver/init()V
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
aload 1
iload 2
aaload
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method static access$000(Lorg/xbill/DNS/ExtendedResolver;)Ljava/util/List;
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method static access$100(Lorg/xbill/DNS/ExtendedResolver;)Z
aload 0
getfield org/xbill/DNS/ExtendedResolver/loadBalance Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static access$200(Lorg/xbill/DNS/ExtendedResolver;)I
aload 0
getfield org/xbill/DNS/ExtendedResolver/lbStart I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static access$208(Lorg/xbill/DNS/ExtendedResolver;)I
aload 0
getfield org/xbill/DNS/ExtendedResolver/lbStart I
istore 1
aload 0
iload 1
iconst_1
iadd
putfield org/xbill/DNS/ExtendedResolver/lbStart I
iload 1
ireturn
.limit locals 2
.limit stack 3
.end method

.method static access$244(Lorg/xbill/DNS/ExtendedResolver;I)I
aload 0
getfield org/xbill/DNS/ExtendedResolver/lbStart I
iload 1
irem
istore 1
aload 0
iload 1
putfield org/xbill/DNS/ExtendedResolver/lbStart I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static access$300(Lorg/xbill/DNS/ExtendedResolver;)I
aload 0
getfield org/xbill/DNS/ExtendedResolver/retries I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private init()V
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
putfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
return
.limit locals 1
.limit stack 3
.end method

.method public addResolver(Lorg/xbill/DNS/Resolver;)V
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public deleteResolver(Lorg/xbill/DNS/Resolver;)V
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
aload 1
invokeinterface java/util/List/remove(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method public getResolver(I)Lorg/xbill/DNS/Resolver;
iload 1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L0
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public getResolvers()[Lorg/xbill/DNS/Resolver;
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
anewarray org/xbill/DNS/Resolver
invokeinterface java/util/List/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
checkcast [Lorg/xbill/DNS/Resolver;
checkcast [Lorg/xbill/DNS/Resolver;
areturn
.limit locals 1
.limit stack 2
.end method

.method public send(Lorg/xbill/DNS/Message;)Lorg/xbill/DNS/Message;
new org/xbill/DNS/ExtendedResolver$Resolution
dup
aload 0
aload 1
invokespecial org/xbill/DNS/ExtendedResolver$Resolution/<init>(Lorg/xbill/DNS/ExtendedResolver;Lorg/xbill/DNS/Message;)V
invokevirtual org/xbill/DNS/ExtendedResolver$Resolution/start()Lorg/xbill/DNS/Message;
areturn
.limit locals 2
.limit stack 4
.end method

.method public sendAsync(Lorg/xbill/DNS/Message;Lorg/xbill/DNS/ResolverListener;)Ljava/lang/Object;
new org/xbill/DNS/ExtendedResolver$Resolution
dup
aload 0
aload 1
invokespecial org/xbill/DNS/ExtendedResolver$Resolution/<init>(Lorg/xbill/DNS/ExtendedResolver;Lorg/xbill/DNS/Message;)V
astore 1
aload 1
aload 2
invokevirtual org/xbill/DNS/ExtendedResolver$Resolution/startAsync(Lorg/xbill/DNS/ResolverListener;)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public setEDNS(I)V
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
iload 1
invokeinterface org/xbill/DNS/Resolver/setEDNS(I)V 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public setEDNS(IIILjava/util/List;)V
iconst_0
istore 5
L0:
iload 5
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 5
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
iload 1
iload 2
iload 3
aload 4
invokeinterface org/xbill/DNS/Resolver/setEDNS(IIILjava/util/List;)V 4
iload 5
iconst_1
iadd
istore 5
goto L0
L1:
return
.limit locals 6
.limit stack 5
.end method

.method public setIgnoreTruncation(Z)V
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
iload 1
invokeinterface org/xbill/DNS/Resolver/setIgnoreTruncation(Z)V 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public setLoadBalance(Z)V
aload 0
iload 1
putfield org/xbill/DNS/ExtendedResolver/loadBalance Z
return
.limit locals 2
.limit stack 2
.end method

.method public setPort(I)V
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
iload 1
invokeinterface org/xbill/DNS/Resolver/setPort(I)V 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public setRetries(I)V
aload 0
iload 1
putfield org/xbill/DNS/ExtendedResolver/retries I
return
.limit locals 2
.limit stack 2
.end method

.method public setTCP(Z)V
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
iload 1
invokeinterface org/xbill/DNS/Resolver/setTCP(Z)V 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public setTSIGKey(Lorg/xbill/DNS/TSIG;)V
iconst_0
istore 2
L0:
iload 2
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
aload 1
invokeinterface org/xbill/DNS/Resolver/setTSIGKey(Lorg/xbill/DNS/TSIG;)V 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 2
.end method

.method public setTimeout(I)V
aload 0
iload 1
iconst_0
invokevirtual org/xbill/DNS/ExtendedResolver/setTimeout(II)V
return
.limit locals 2
.limit stack 3
.end method

.method public setTimeout(II)V
iconst_0
istore 3
L0:
iload 3
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
getfield org/xbill/DNS/ExtendedResolver/resolvers Ljava/util/List;
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast org/xbill/DNS/Resolver
iload 1
iload 2
invokeinterface org/xbill/DNS/Resolver/setTimeout(II)V 2
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method
