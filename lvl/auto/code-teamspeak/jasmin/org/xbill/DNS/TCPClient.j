.bytecode 50.0
.class final synchronized org/xbill/DNS/TCPClient
.super org/xbill/DNS/Client

.method public <init>(J)V
aload 0
invokestatic java/nio/channels/SocketChannel/open()Ljava/nio/channels/SocketChannel;
lload 1
invokespecial org/xbill/DNS/Client/<init>(Ljava/nio/channels/SelectableChannel;J)V
return
.limit locals 3
.limit stack 4
.end method

.method private _recv(I)[B
.catch all from L0 to L1 using L2
.catch all from L3 to L2 using L2
.catch all from L4 to L5 using L2
.catch all from L5 to L6 using L2
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/SocketChannel
astore 6
iload 1
newarray byte
astore 7
aload 7
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 8
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_1
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
iconst_0
istore 2
L7:
iload 2
iload 1
if_icmpge L8
L0:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isReadable()Z
ifeq L5
aload 6
aload 8
invokevirtual java/nio/channels/SocketChannel/read(Ljava/nio/ByteBuffer;)I
i2l
lstore 4
L1:
lload 4
lconst_0
lcmp
ifge L9
L3:
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L2:
astore 6
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L10
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L10:
aload 6
athrow
L9:
iload 2
lload 4
l2i
iadd
istore 3
iload 3
istore 2
iload 3
iload 1
if_icmpge L7
iload 3
istore 2
L4:
invokestatic java/lang/System/currentTimeMillis()J
aload 0
getfield org/xbill/DNS/TCPClient/endTime J
lcmp
ifle L7
new java/net/SocketTimeoutException
dup
invokespecial java/net/SocketTimeoutException/<init>()V
athrow
L5:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
aload 0
getfield org/xbill/DNS/TCPClient/endTime J
invokestatic org/xbill/DNS/TCPClient/blockUntil(Ljava/nio/channels/SelectionKey;J)V
L6:
goto L7
L8:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L11
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L11:
aload 7
areturn
.limit locals 9
.limit stack 4
.end method

.method static sendrecv(Ljava/net/SocketAddress;Ljava/net/SocketAddress;[BJ)[B
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
new org/xbill/DNS/TCPClient
dup
lload 3
invokespecial org/xbill/DNS/TCPClient/<init>(J)V
astore 5
aload 0
ifnull L1
L0:
aload 5
aload 0
invokevirtual org/xbill/DNS/TCPClient/bind(Ljava/net/SocketAddress;)V
L1:
aload 5
aload 1
invokevirtual org/xbill/DNS/TCPClient/connect(Ljava/net/SocketAddress;)V
aload 5
aload 2
invokevirtual org/xbill/DNS/TCPClient/send([B)V
aload 5
invokevirtual org/xbill/DNS/TCPClient/recv()[B
astore 0
L3:
aload 5
invokevirtual org/xbill/DNS/TCPClient/cleanup()V
aload 0
areturn
L2:
astore 0
aload 5
invokevirtual org/xbill/DNS/TCPClient/cleanup()V
aload 0
athrow
.limit locals 6
.limit stack 4
.end method

.method static sendrecv(Ljava/net/SocketAddress;[BJ)[B
aconst_null
aload 0
aload 1
lload 2
invokestatic org/xbill/DNS/TCPClient/sendrecv(Ljava/net/SocketAddress;Ljava/net/SocketAddress;[BJ)[B
areturn
.limit locals 4
.limit stack 5
.end method

.method final bind(Ljava/net/SocketAddress;)V
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/SocketChannel
invokevirtual java/nio/channels/SocketChannel/socket()Ljava/net/Socket;
aload 1
invokevirtual java/net/Socket/bind(Ljava/net/SocketAddress;)V
return
.limit locals 2
.limit stack 2
.end method

.method final connect(Ljava/net/SocketAddress;)V
.catch all from L0 to L1 using L2
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/SocketChannel
astore 2
aload 2
aload 1
invokevirtual java/nio/channels/SocketChannel/connect(Ljava/net/SocketAddress;)Z
ifeq L3
L4:
return
L3:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
bipush 8
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L0:
aload 2
invokevirtual java/nio/channels/SocketChannel/finishConnect()Z
ifne L5
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isConnectable()Z
ifne L0
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
aload 0
getfield org/xbill/DNS/TCPClient/endTime J
invokestatic org/xbill/DNS/TCPClient/blockUntil(Ljava/nio/channels/SelectionKey;J)V
L1:
goto L0
L2:
astore 1
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L6
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L6:
aload 1
athrow
L5:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L4
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
return
.limit locals 3
.limit stack 3
.end method

.method final recv()[B
aload 0
iconst_2
invokespecial org/xbill/DNS/TCPClient/_recv(I)[B
astore 2
aload 2
iconst_0
baload
istore 1
aload 0
aload 2
iconst_1
baload
sipush 255
iand
iload 1
sipush 255
iand
bipush 8
ishl
iadd
invokespecial org/xbill/DNS/TCPClient/_recv(I)[B
astore 2
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/SocketChannel
astore 3
ldc "TCP read"
aload 3
invokevirtual java/nio/channels/SocketChannel/socket()Ljava/net/Socket;
invokevirtual java/net/Socket/getLocalSocketAddress()Ljava/net/SocketAddress;
aload 3
invokevirtual java/nio/channels/SocketChannel/socket()Ljava/net/Socket;
invokevirtual java/net/Socket/getRemoteSocketAddress()Ljava/net/SocketAddress;
aload 2
invokestatic org/xbill/DNS/TCPClient/verboseLog(Ljava/lang/String;Ljava/net/SocketAddress;Ljava/net/SocketAddress;[B)V
aload 2
areturn
.limit locals 4
.limit stack 4
.end method

.method final send([B)V
.catch all from L0 to L1 using L2
.catch all from L3 to L2 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L7 to L8 using L2
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/SocketChannel
astore 6
ldc "TCP write"
aload 6
invokevirtual java/nio/channels/SocketChannel/socket()Ljava/net/Socket;
invokevirtual java/net/Socket/getLocalSocketAddress()Ljava/net/SocketAddress;
aload 6
invokevirtual java/nio/channels/SocketChannel/socket()Ljava/net/Socket;
invokevirtual java/net/Socket/getRemoteSocketAddress()Ljava/net/SocketAddress;
aload 1
invokestatic org/xbill/DNS/TCPClient/verboseLog(Ljava/lang/String;Ljava/net/SocketAddress;Ljava/net/SocketAddress;[B)V
iconst_2
newarray byte
dup
iconst_0
aload 1
arraylength
bipush 8
iushr
i2b
bastore
dup
iconst_1
aload 1
arraylength
sipush 255
iand
i2b
bastore
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 7
aload 1
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
astore 8
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_4
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
iconst_2
iadd
if_icmpge L9
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isWritable()Z
ifeq L7
aload 6
iconst_2
anewarray java/nio/ByteBuffer
dup
iconst_0
aload 7
aastore
dup
iconst_1
aload 8
aastore
invokevirtual java/nio/channels/SocketChannel/write([Ljava/nio/ByteBuffer;)J
lstore 4
L1:
lload 4
lconst_0
lcmp
ifge L10
L3:
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L2:
astore 1
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L11
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L11:
aload 1
athrow
L10:
iload 2
lload 4
l2i
iadd
istore 3
iload 3
istore 2
L4:
iload 3
aload 1
arraylength
iconst_2
iadd
if_icmpge L0
L5:
iload 3
istore 2
L6:
invokestatic java/lang/System/currentTimeMillis()J
aload 0
getfield org/xbill/DNS/TCPClient/endTime J
lcmp
ifle L0
new java/net/SocketTimeoutException
dup
invokespecial java/net/SocketTimeoutException/<init>()V
athrow
L7:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
aload 0
getfield org/xbill/DNS/TCPClient/endTime J
invokestatic org/xbill/DNS/TCPClient/blockUntil(Ljava/nio/channels/SelectionKey;J)V
L8:
goto L0
L9:
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L12
aload 0
getfield org/xbill/DNS/TCPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L12:
return
.limit locals 9
.limit stack 5
.end method
