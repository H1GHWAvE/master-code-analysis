.bytecode 50.0
.class final synchronized org/xbill/DNS/FormattedTime
.super java/lang/Object

.field private static 'w2' Ljava/text/NumberFormat;

.field private static 'w4' Ljava/text/NumberFormat;

.method static <clinit>()V
new java/text/DecimalFormat
dup
invokespecial java/text/DecimalFormat/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/FormattedTime/w2 Ljava/text/NumberFormat;
aload 0
iconst_2
invokevirtual java/text/NumberFormat/setMinimumIntegerDigits(I)V
new java/text/DecimalFormat
dup
invokespecial java/text/DecimalFormat/<init>()V
astore 0
aload 0
putstatic org/xbill/DNS/FormattedTime/w4 Ljava/text/NumberFormat;
aload 0
iconst_4
invokevirtual java/text/NumberFormat/setMinimumIntegerDigits(I)V
getstatic org/xbill/DNS/FormattedTime/w4 Ljava/text/NumberFormat;
iconst_0
invokevirtual java/text/NumberFormat/setGroupingUsed(Z)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static format(Ljava/util/Date;)Ljava/lang/String;
new java/util/GregorianCalendar
dup
ldc "UTC"
invokestatic java/util/TimeZone/getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;
invokespecial java/util/GregorianCalendar/<init>(Ljava/util/TimeZone;)V
astore 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 2
aload 1
aload 0
invokevirtual java/util/Calendar/setTime(Ljava/util/Date;)V
aload 2
getstatic org/xbill/DNS/FormattedTime/w4 Ljava/text/NumberFormat;
aload 1
iconst_1
invokevirtual java/util/Calendar/get(I)I
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
getstatic org/xbill/DNS/FormattedTime/w2 Ljava/text/NumberFormat;
aload 1
iconst_2
invokevirtual java/util/Calendar/get(I)I
iconst_1
iadd
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
getstatic org/xbill/DNS/FormattedTime/w2 Ljava/text/NumberFormat;
aload 1
iconst_5
invokevirtual java/util/Calendar/get(I)I
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
getstatic org/xbill/DNS/FormattedTime/w2 Ljava/text/NumberFormat;
aload 1
bipush 11
invokevirtual java/util/Calendar/get(I)I
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
getstatic org/xbill/DNS/FormattedTime/w2 Ljava/text/NumberFormat;
aload 1
bipush 12
invokevirtual java/util/Calendar/get(I)I
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
getstatic org/xbill/DNS/FormattedTime/w2 Ljava/text/NumberFormat;
aload 1
bipush 13
invokevirtual java/util/Calendar/get(I)I
i2l
invokevirtual java/text/NumberFormat/format(J)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 2
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method public static parse(Ljava/lang/String;)Ljava/util/Date;
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
invokevirtual java/lang/String/length()I
bipush 14
if_icmpeq L3
new org/xbill/DNS/TextParseException
dup
new java/lang/StringBuffer
dup
ldc "Invalid time encoding: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
L3:
new java/util/GregorianCalendar
dup
ldc "UTC"
invokestatic java/util/TimeZone/getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;
invokespecial java/util/GregorianCalendar/<init>(Ljava/util/TimeZone;)V
astore 1
aload 1
invokevirtual java/util/Calendar/clear()V
L0:
aload 1
aload 0
iconst_0
iconst_4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 0
iconst_4
bipush 6
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
aload 0
bipush 6
bipush 8
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 0
bipush 8
bipush 10
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 0
bipush 10
bipush 12
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
aload 0
bipush 12
bipush 14
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
invokevirtual java/util/Calendar/set(IIIIII)V
L1:
aload 1
invokevirtual java/util/Calendar/getTime()Ljava/util/Date;
areturn
L2:
astore 1
new org/xbill/DNS/TextParseException
dup
new java/lang/StringBuffer
dup
ldc "Invalid time encoding: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial org/xbill/DNS/TextParseException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 9
.end method
