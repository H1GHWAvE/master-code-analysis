.bytecode 50.0
.class public synchronized org/xbill/DNS/spi/DNSJavaNameServiceDescriptor
.super java/lang/Object
.implements sun/net/spi/nameservice/NameServiceDescriptor

.field static 'class$sun$net$spi$nameservice$NameService' Ljava/lang/Class;

.field private static 'nameService' Lsun/net/spi/nameservice/NameService;

.method static <clinit>()V
getstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$sun$net$spi$nameservice$NameService Ljava/lang/Class;
ifnonnull L0
ldc "sun.net.spi.nameservice.NameService"
invokestatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 0
aload 0
putstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$sun$net$spi$nameservice$NameService Ljava/lang/Class;
L1:
aload 0
invokevirtual java/lang/Class/getClassLoader()Ljava/lang/ClassLoader;
astore 1
getstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$sun$net$spi$nameservice$NameService Ljava/lang/Class;
ifnonnull L2
ldc "sun.net.spi.nameservice.NameService"
invokestatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$(Ljava/lang/String;)Ljava/lang/Class;
astore 0
aload 0
putstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$sun$net$spi$nameservice$NameService Ljava/lang/Class;
L3:
new org/xbill/DNS/spi/DNSJavaNameService
dup
invokespecial org/xbill/DNS/spi/DNSJavaNameService/<init>()V
astore 2
aload 1
iconst_1
anewarray java/lang/Class
dup
iconst_0
aload 0
aastore
aload 2
invokestatic java/lang/reflect/Proxy/newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;
checkcast sun/net/spi/nameservice/NameService
putstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/nameService Lsun/net/spi/nameservice/NameService;
return
L0:
getstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$sun$net$spi$nameservice$NameService Ljava/lang/Class;
astore 0
goto L1
L2:
getstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/class$sun$net$spi$nameservice$NameService Ljava/lang/Class;
astore 0
goto L3
.limit locals 3
.limit stack 5
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
.catch java/lang/ClassNotFoundException from L0 to L1 using L2
L0:
aload 0
invokestatic java/lang/Class/forName(Ljava/lang/String;)Ljava/lang/Class;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/NoClassDefFoundError
dup
invokespecial java/lang/NoClassDefFoundError/<init>()V
aload 0
invokevirtual java/lang/NoClassDefFoundError/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
athrow
.limit locals 1
.limit stack 2
.end method

.method public createNameService()Lsun/net/spi/nameservice/NameService;
getstatic org/xbill/DNS/spi/DNSJavaNameServiceDescriptor/nameService Lsun/net/spi/nameservice/NameService;
areturn
.limit locals 1
.limit stack 1
.end method

.method public getProviderName()Ljava/lang/String;
ldc "dnsjava"
areturn
.limit locals 1
.limit stack 1
.end method

.method public getType()Ljava/lang/String;
ldc "dns"
areturn
.limit locals 1
.limit stack 1
.end method
