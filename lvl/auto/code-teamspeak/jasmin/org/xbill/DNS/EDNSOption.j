.bytecode 50.0
.class public synchronized abstract org/xbill/DNS/EDNSOption
.super java/lang/Object

.field private final 'code' I

.method public <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc "code"
iload 1
invokestatic org/xbill/DNS/Record/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/EDNSOption/code I
return
.limit locals 2
.limit stack 3
.end method

.method static fromWire(Lorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/EDNSOption;
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 1
aload 0
invokevirtual org/xbill/DNS/DNSInput/readU16()I
istore 2
aload 0
invokevirtual org/xbill/DNS/DNSInput/remaining()I
iload 2
if_icmpge L0
new org/xbill/DNS/WireParseException
dup
ldc "truncated option"
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
invokevirtual org/xbill/DNS/DNSInput/saveActive()I
istore 3
aload 0
iload 2
invokevirtual org/xbill/DNS/DNSInput/setActive(I)V
iload 1
lookupswitch
3 : L1
8 : L2
default : L3
L3:
new org/xbill/DNS/GenericEDNSOption
dup
iload 1
invokespecial org/xbill/DNS/GenericEDNSOption/<init>(I)V
astore 4
L4:
aload 4
aload 0
invokevirtual org/xbill/DNS/EDNSOption/optionFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
iload 3
invokevirtual org/xbill/DNS/DNSInput/restoreActive(I)V
aload 4
areturn
L1:
new org/xbill/DNS/NSIDOption
dup
invokespecial org/xbill/DNS/NSIDOption/<init>()V
astore 4
goto L4
L2:
new org/xbill/DNS/ClientSubnetOption
dup
invokespecial org/xbill/DNS/ClientSubnetOption/<init>()V
astore 4
goto L4
.limit locals 5
.limit stack 3
.end method

.method public static fromWire([B)Lorg/xbill/DNS/EDNSOption;
new org/xbill/DNS/DNSInput
dup
aload 0
invokespecial org/xbill/DNS/DNSInput/<init>([B)V
invokestatic org/xbill/DNS/EDNSOption/fromWire(Lorg/xbill/DNS/DNSInput;)Lorg/xbill/DNS/EDNSOption;
areturn
.limit locals 1
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
aload 1
ifnull L0
aload 1
instanceof org/xbill/DNS/EDNSOption
ifne L1
L0:
iconst_0
ireturn
L1:
aload 1
checkcast org/xbill/DNS/EDNSOption
astore 1
aload 0
getfield org/xbill/DNS/EDNSOption/code I
aload 1
getfield org/xbill/DNS/EDNSOption/code I
if_icmpne L0
aload 0
invokevirtual org/xbill/DNS/EDNSOption/getData()[B
aload 1
invokevirtual org/xbill/DNS/EDNSOption/getData()[B
invokestatic java/util/Arrays/equals([B[B)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public getCode()I
aload 0
getfield org/xbill/DNS/EDNSOption/code I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getData()[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 1
aload 0
aload 1
invokevirtual org/xbill/DNS/EDNSOption/optionToWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
iconst_0
istore 1
aload 0
invokevirtual org/xbill/DNS/EDNSOption/getData()[B
astore 3
iconst_0
istore 2
L0:
iload 1
aload 3
arraylength
if_icmpge L1
iload 2
iload 2
iconst_3
ishl
aload 3
iload 1
baload
sipush 255
iand
iadd
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
ireturn
.limit locals 4
.limit stack 4
.end method

.method abstract optionFromWire(Lorg/xbill/DNS/DNSInput;)V
.end method

.method abstract optionToString()Ljava/lang/String;
.end method

.method abstract optionToWire(Lorg/xbill/DNS/DNSOutput;)V
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
ldc "{"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/EDNSOption/code I
invokestatic org/xbill/DNS/EDNSOption$Code/string(I)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc ": "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
invokevirtual org/xbill/DNS/EDNSOption/optionToString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc "}"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 0
getfield org/xbill/DNS/EDNSOption/code I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
istore 2
aload 1
iconst_0
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 0
aload 1
invokevirtual org/xbill/DNS/EDNSOption/optionToWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
aload 1
invokevirtual org/xbill/DNS/DNSOutput/current()I
iload 2
isub
iconst_2
isub
iload 2
invokevirtual org/xbill/DNS/DNSOutput/writeU16At(II)V
return
.limit locals 3
.limit stack 3
.end method

.method public toWire()[B
new org/xbill/DNS/DNSOutput
dup
invokespecial org/xbill/DNS/DNSOutput/<init>()V
astore 1
aload 0
aload 1
invokevirtual org/xbill/DNS/EDNSOption/toWire(Lorg/xbill/DNS/DNSOutput;)V
aload 1
invokevirtual org/xbill/DNS/DNSOutput/toByteArray()[B
areturn
.limit locals 2
.limit stack 2
.end method
