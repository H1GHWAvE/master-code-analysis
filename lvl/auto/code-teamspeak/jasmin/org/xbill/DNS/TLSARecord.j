.bytecode 50.0
.class public synchronized org/xbill/DNS/TLSARecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 356494267028580169L


.field private 'certificateAssociationData' [B

.field private 'certificateUsage' I

.field private 'matchingType' I

.field private 'selector' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
bipush 52
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "certificateUsage"
iload 5
invokestatic org/xbill/DNS/TLSARecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/TLSARecord/certificateUsage I
aload 0
ldc "selector"
iload 6
invokestatic org/xbill/DNS/TLSARecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/TLSARecord/selector I
aload 0
ldc "matchingType"
iload 7
invokestatic org/xbill/DNS/TLSARecord/checkU8(Ljava/lang/String;I)I
putfield org/xbill/DNS/TLSARecord/matchingType I
aload 0
ldc "certificateAssociationData"
aload 8
ldc_w 65535
invokestatic org/xbill/DNS/TLSARecord/checkByteArrayLength(Ljava/lang/String;[BI)[B
putfield org/xbill/DNS/TLSARecord/certificateAssociationData [B
return
.limit locals 9
.limit stack 6
.end method

.method public final getCertificateAssociationData()[B
aload 0
getfield org/xbill/DNS/TLSARecord/certificateAssociationData [B
areturn
.limit locals 1
.limit stack 1
.end method

.method public getCertificateUsage()I
aload 0
getfield org/xbill/DNS/TLSARecord/certificateUsage I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getMatchingType()I
aload 0
getfield org/xbill/DNS/TLSARecord/matchingType I
ireturn
.limit locals 1
.limit stack 1
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/TLSARecord
dup
invokespecial org/xbill/DNS/TLSARecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getSelector()I
aload 0
getfield org/xbill/DNS/TLSARecord/selector I
ireturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/TLSARecord/certificateUsage I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/TLSARecord/selector I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt8()I
putfield org/xbill/DNS/TLSARecord/matchingType I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getHex()[B
putfield org/xbill/DNS/TLSARecord/certificateAssociationData [B
return
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/TLSARecord/certificateUsage I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/TLSARecord/selector I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU8()I
putfield org/xbill/DNS/TLSARecord/matchingType I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readByteArray()[B
putfield org/xbill/DNS/TLSARecord/certificateAssociationData [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/certificateUsage I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/selector I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/matchingType I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/certificateAssociationData [B
invokestatic org/xbill/DNS/utils/base16/toString([B)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/certificateUsage I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/selector I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/matchingType I
invokevirtual org/xbill/DNS/DNSOutput/writeU8(I)V
aload 1
aload 0
getfield org/xbill/DNS/TLSARecord/certificateAssociationData [B
invokevirtual org/xbill/DNS/DNSOutput/writeByteArray([B)V
return
.limit locals 4
.limit stack 2
.end method
