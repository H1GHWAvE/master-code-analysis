.bytecode 50.0
.class synchronized org/xbill/DNS/Client
.super java/lang/Object

.field private static 'packetLogger' Lorg/xbill/DNS/PacketLogger;

.field protected 'endTime' J

.field protected 'key' Ljava/nio/channels/SelectionKey;

.method static <clinit>()V
aconst_null
putstatic org/xbill/DNS/Client/packetLogger Lorg/xbill/DNS/PacketLogger;
return
.limit locals 0
.limit stack 1
.end method

.method protected <init>(Ljava/nio/channels/SelectableChannel;J)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aconst_null
astore 4
aload 0
lload 2
putfield org/xbill/DNS/Client/endTime J
L0:
invokestatic java/nio/channels/Selector/open()Ljava/nio/channels/Selector;
astore 5
L1:
aload 5
astore 4
L3:
aload 1
iconst_0
invokevirtual java/nio/channels/SelectableChannel/configureBlocking(Z)Ljava/nio/channels/SelectableChannel;
pop
L4:
aload 5
astore 4
L5:
aload 0
aload 1
aload 5
iconst_1
invokevirtual java/nio/channels/SelectableChannel/register(Ljava/nio/channels/Selector;I)Ljava/nio/channels/SelectionKey;
putfield org/xbill/DNS/Client/key Ljava/nio/channels/SelectionKey;
L6:
return
L2:
astore 5
aload 4
ifnull L7
aload 4
invokevirtual java/nio/channels/Selector/close()V
L7:
aload 1
invokevirtual java/nio/channels/SelectableChannel/close()V
aload 5
athrow
.limit locals 6
.limit stack 4
.end method

.method protected static blockUntil(Ljava/nio/channels/SelectionKey;J)V
lload 1
invokestatic java/lang/System/currentTimeMillis()J
lsub
lstore 1
iconst_0
istore 3
lload 1
lconst_0
lcmp
ifle L0
aload 0
invokevirtual java/nio/channels/SelectionKey/selector()Ljava/nio/channels/Selector;
lload 1
invokevirtual java/nio/channels/Selector/select(J)I
istore 3
L1:
iload 3
ifne L2
new java/net/SocketTimeoutException
dup
invokespecial java/net/SocketTimeoutException/<init>()V
athrow
L0:
lload 1
lconst_0
lcmp
ifne L1
aload 0
invokevirtual java/nio/channels/SelectionKey/selector()Ljava/nio/channels/Selector;
invokevirtual java/nio/channels/Selector/selectNow()I
istore 3
goto L1
L2:
return
.limit locals 4
.limit stack 4
.end method

.method static setPacketLogger(Lorg/xbill/DNS/PacketLogger;)V
aload 0
putstatic org/xbill/DNS/Client/packetLogger Lorg/xbill/DNS/PacketLogger;
return
.limit locals 1
.limit stack 1
.end method

.method protected static verboseLog(Ljava/lang/String;Ljava/net/SocketAddress;Ljava/net/SocketAddress;[B)V
ldc "verbosemsg"
invokestatic org/xbill/DNS/Options/check(Ljava/lang/String;)Z
ifeq L0
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 0
aload 3
invokestatic org/xbill/DNS/utils/hexdump/dump(Ljava/lang/String;[B)Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L0:
getstatic org/xbill/DNS/Client/packetLogger Lorg/xbill/DNS/PacketLogger;
ifnull L1
getstatic org/xbill/DNS/Client/packetLogger Lorg/xbill/DNS/PacketLogger;
aload 0
aload 1
aload 2
aload 3
invokeinterface org/xbill/DNS/PacketLogger/log(Ljava/lang/String;Ljava/net/SocketAddress;Ljava/net/SocketAddress;[B)V 4
L1:
return
.limit locals 4
.limit stack 5
.end method

.method cleanup()V
aload 0
getfield org/xbill/DNS/Client/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/selector()Ljava/nio/channels/Selector;
invokevirtual java/nio/channels/Selector/close()V
aload 0
getfield org/xbill/DNS/Client/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
invokevirtual java/nio/channels/SelectableChannel/close()V
return
.limit locals 1
.limit stack 1
.end method
