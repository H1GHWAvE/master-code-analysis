.bytecode 50.0
.class public final synchronized org/xbill/DNS/Flags
.super java/lang/Object

.field public static final 'AA' B = 5


.field public static final 'AD' B = 10


.field public static final 'CD' B = 11


.field public static final 'DO' I = 32768


.field public static final 'QR' B = 0


.field public static final 'RA' B = 8


.field public static final 'RD' B = 7


.field public static final 'TC' B = 6


.field private static 'flags' Lorg/xbill/DNS/Mnemonic;

.method static <clinit>()V
new org/xbill/DNS/Mnemonic
dup
ldc "DNS Header Flag"
iconst_3
invokespecial org/xbill/DNS/Mnemonic/<init>(Ljava/lang/String;I)V
astore 0
aload 0
putstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
aload 0
bipush 15
invokevirtual org/xbill/DNS/Mnemonic/setMaximum(I)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
ldc "FLAG"
invokevirtual org/xbill/DNS/Mnemonic/setPrefix(Ljava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_1
invokevirtual org/xbill/DNS/Mnemonic/setNumericAllowed(Z)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_0
ldc "qr"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
iconst_5
ldc "aa"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 6
ldc "tc"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 7
ldc "rd"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 8
ldc "ra"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 10
ldc "ad"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
bipush 11
ldc "cd"
invokevirtual org/xbill/DNS/Mnemonic/add(ILjava/lang/String;)V
return
.limit locals 1
.limit stack 4
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static isFlag(I)Z
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/check(I)V
iload 0
ifle L0
iload 0
iconst_4
if_icmple L1
L0:
iload 0
bipush 12
if_icmplt L2
L1:
iconst_0
ireturn
L2:
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public static string(I)Ljava/lang/String;
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
iload 0
invokevirtual org/xbill/DNS/Mnemonic/getText(I)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static value(Ljava/lang/String;)I
getstatic org/xbill/DNS/Flags/flags Lorg/xbill/DNS/Mnemonic;
aload 0
invokevirtual org/xbill/DNS/Mnemonic/getValue(Ljava/lang/String;)I
ireturn
.limit locals 1
.limit stack 2
.end method
