.bytecode 50.0
.class final synchronized org/xbill/DNS/UDPClient
.super org/xbill/DNS/Client

.field private static final 'EPHEMERAL_RANGE' I = 64511


.field private static final 'EPHEMERAL_START' I = 1024


.field private static final 'EPHEMERAL_STOP' I = 65535


.field private static 'prng' Ljava/security/SecureRandom;

.field private static volatile 'prng_initializing' Z

.field private 'bound' Z

.method static <clinit>()V
new java/security/SecureRandom
dup
invokespecial java/security/SecureRandom/<init>()V
putstatic org/xbill/DNS/UDPClient/prng Ljava/security/SecureRandom;
iconst_1
putstatic org/xbill/DNS/UDPClient/prng_initializing Z
new java/lang/Thread
dup
new org/xbill/DNS/UDPClient$1
dup
invokespecial org/xbill/DNS/UDPClient$1/<init>()V
invokespecial java/lang/Thread/<init>(Ljava/lang/Runnable;)V
invokevirtual java/lang/Thread/start()V
return
.limit locals 0
.limit stack 4
.end method

.method public <init>(J)V
aload 0
invokestatic java/nio/channels/DatagramChannel/open()Ljava/nio/channels/DatagramChannel;
lload 1
invokespecial org/xbill/DNS/Client/<init>(Ljava/nio/channels/SelectableChannel;J)V
aload 0
iconst_0
putfield org/xbill/DNS/UDPClient/bound Z
return
.limit locals 3
.limit stack 4
.end method

.method static access$000()Ljava/security/SecureRandom;
getstatic org/xbill/DNS/UDPClient/prng Ljava/security/SecureRandom;
areturn
.limit locals 0
.limit stack 1
.end method

.method static access$102(Z)Z
iload 0
putstatic org/xbill/DNS/UDPClient/prng_initializing Z
iload 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private bind_random(Ljava/net/InetSocketAddress;)V
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch java/net/SocketException from L3 to L4 using L5
.catch java/net/SocketException from L6 to L7 using L5
.catch java/net/SocketException from L7 to L8 using L5
.catch java/net/SocketException from L9 to L10 using L5
getstatic org/xbill/DNS/UDPClient/prng_initializing Z
ifeq L11
L0:
ldc2_w 2L
invokestatic java/lang/Thread/sleep(J)V
L1:
getstatic org/xbill/DNS/UDPClient/prng_initializing Z
ifeq L11
L12:
return
L11:
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/DatagramChannel
astore 5
iconst_0
istore 2
L13:
iload 2
sipush 1024
if_icmpge L12
L3:
getstatic org/xbill/DNS/UDPClient/prng Ljava/security/SecureRandom;
ldc_w 64511
invokevirtual java/security/SecureRandom/nextInt(I)I
sipush 1024
iadd
istore 3
L4:
aload 1
ifnull L9
L6:
new java/net/InetSocketAddress
dup
aload 1
invokevirtual java/net/InetSocketAddress/getAddress()Ljava/net/InetAddress;
iload 3
invokespecial java/net/InetSocketAddress/<init>(Ljava/net/InetAddress;I)V
astore 4
L7:
aload 5
invokevirtual java/nio/channels/DatagramChannel/socket()Ljava/net/DatagramSocket;
aload 4
invokevirtual java/net/DatagramSocket/bind(Ljava/net/SocketAddress;)V
aload 0
iconst_1
putfield org/xbill/DNS/UDPClient/bound Z
L8:
return
L9:
new java/net/InetSocketAddress
dup
iload 3
invokespecial java/net/InetSocketAddress/<init>(I)V
astore 4
L10:
goto L7
L2:
astore 4
goto L1
L5:
astore 4
iload 2
iconst_1
iadd
istore 2
goto L13
.limit locals 6
.limit stack 4
.end method

.method static sendrecv(Ljava/net/SocketAddress;Ljava/net/SocketAddress;[BIJ)[B
.catch all from L0 to L1 using L2
new org/xbill/DNS/UDPClient
dup
lload 4
invokespecial org/xbill/DNS/UDPClient/<init>(J)V
astore 6
L0:
aload 6
aload 0
invokevirtual org/xbill/DNS/UDPClient/bind(Ljava/net/SocketAddress;)V
aload 6
aload 1
invokevirtual org/xbill/DNS/UDPClient/connect(Ljava/net/SocketAddress;)V
aload 6
aload 2
invokevirtual org/xbill/DNS/UDPClient/send([B)V
aload 6
iload 3
invokevirtual org/xbill/DNS/UDPClient/recv(I)[B
astore 0
L1:
aload 6
invokevirtual org/xbill/DNS/UDPClient/cleanup()V
aload 0
areturn
L2:
astore 0
aload 6
invokevirtual org/xbill/DNS/UDPClient/cleanup()V
aload 0
athrow
.limit locals 7
.limit stack 4
.end method

.method static sendrecv(Ljava/net/SocketAddress;[BIJ)[B
aconst_null
aload 0
aload 1
iload 2
lload 3
invokestatic org/xbill/DNS/UDPClient/sendrecv(Ljava/net/SocketAddress;Ljava/net/SocketAddress;[BIJ)[B
areturn
.limit locals 5
.limit stack 6
.end method

.method final bind(Ljava/net/SocketAddress;)V
aload 1
ifnull L0
aload 1
instanceof java/net/InetSocketAddress
ifeq L1
aload 1
checkcast java/net/InetSocketAddress
invokevirtual java/net/InetSocketAddress/getPort()I
ifne L1
L0:
aload 0
aload 1
checkcast java/net/InetSocketAddress
invokespecial org/xbill/DNS/UDPClient/bind_random(Ljava/net/InetSocketAddress;)V
aload 0
getfield org/xbill/DNS/UDPClient/bound Z
ifeq L1
L2:
return
L1:
aload 1
ifnull L2
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/DatagramChannel
invokevirtual java/nio/channels/DatagramChannel/socket()Ljava/net/DatagramSocket;
aload 1
invokevirtual java/net/DatagramSocket/bind(Ljava/net/SocketAddress;)V
aload 0
iconst_1
putfield org/xbill/DNS/UDPClient/bound Z
return
.limit locals 2
.limit stack 2
.end method

.method final connect(Ljava/net/SocketAddress;)V
aload 0
getfield org/xbill/DNS/UDPClient/bound Z
ifne L0
aload 0
aconst_null
invokevirtual org/xbill/DNS/UDPClient/bind(Ljava/net/SocketAddress;)V
L0:
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/DatagramChannel
aload 1
invokevirtual java/nio/channels/DatagramChannel/connect(Ljava/net/SocketAddress;)Ljava/nio/channels/DatagramChannel;
pop
return
.limit locals 2
.limit stack 2
.end method

.method final recv(I)[B
.catch all from L0 to L1 using L2
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/DatagramChannel
astore 4
iload 1
newarray byte
astore 5
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
iconst_1
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L0:
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isReadable()Z
ifne L3
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
aload 0
getfield org/xbill/DNS/UDPClient/endTime J
invokestatic org/xbill/DNS/UDPClient/blockUntil(Ljava/nio/channels/SelectionKey;J)V
L1:
goto L0
L2:
astore 4
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L4
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L4:
aload 4
athrow
L3:
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/isValid()Z
ifeq L5
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
iconst_0
invokevirtual java/nio/channels/SelectionKey/interestOps(I)Ljava/nio/channels/SelectionKey;
pop
L5:
aload 4
aload 5
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
invokevirtual java/nio/channels/DatagramChannel/read(Ljava/nio/ByteBuffer;)I
i2l
lstore 2
lload 2
lconst_0
lcmp
ifgt L6
new java/io/EOFException
dup
invokespecial java/io/EOFException/<init>()V
athrow
L6:
lload 2
l2i
istore 1
iload 1
newarray byte
astore 6
aload 5
iconst_0
aload 6
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
ldc "UDP read"
aload 4
invokevirtual java/nio/channels/DatagramChannel/socket()Ljava/net/DatagramSocket;
invokevirtual java/net/DatagramSocket/getLocalSocketAddress()Ljava/net/SocketAddress;
aload 4
invokevirtual java/nio/channels/DatagramChannel/socket()Ljava/net/DatagramSocket;
invokevirtual java/net/DatagramSocket/getRemoteSocketAddress()Ljava/net/SocketAddress;
aload 6
invokestatic org/xbill/DNS/UDPClient/verboseLog(Ljava/lang/String;Ljava/net/SocketAddress;Ljava/net/SocketAddress;[B)V
aload 6
areturn
.limit locals 7
.limit stack 5
.end method

.method final send([B)V
aload 0
getfield org/xbill/DNS/UDPClient/key Ljava/nio/channels/SelectionKey;
invokevirtual java/nio/channels/SelectionKey/channel()Ljava/nio/channels/SelectableChannel;
checkcast java/nio/channels/DatagramChannel
astore 2
ldc "UDP write"
aload 2
invokevirtual java/nio/channels/DatagramChannel/socket()Ljava/net/DatagramSocket;
invokevirtual java/net/DatagramSocket/getLocalSocketAddress()Ljava/net/SocketAddress;
aload 2
invokevirtual java/nio/channels/DatagramChannel/socket()Ljava/net/DatagramSocket;
invokevirtual java/net/DatagramSocket/getRemoteSocketAddress()Ljava/net/SocketAddress;
aload 1
invokestatic org/xbill/DNS/UDPClient/verboseLog(Ljava/lang/String;Ljava/net/SocketAddress;Ljava/net/SocketAddress;[B)V
aload 2
aload 1
invokestatic java/nio/ByteBuffer/wrap([B)Ljava/nio/ByteBuffer;
invokevirtual java/nio/channels/DatagramChannel/write(Ljava/nio/ByteBuffer;)I
pop
return
.limit locals 3
.limit stack 4
.end method
