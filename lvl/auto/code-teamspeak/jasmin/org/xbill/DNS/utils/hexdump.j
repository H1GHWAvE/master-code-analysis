.bytecode 50.0
.class public synchronized org/xbill/DNS/utils/hexdump
.super java/lang/Object

.field private static final 'hex' [C

.method static <clinit>()V
ldc "0123456789ABCDEF"
invokevirtual java/lang/String/toCharArray()[C
putstatic org/xbill/DNS/utils/hexdump/hex [C
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static dump(Ljava/lang/String;[B)Ljava/lang/String;
aload 0
aload 1
iconst_0
aload 1
arraylength
invokestatic org/xbill/DNS/utils/hexdump/dump(Ljava/lang/String;[BII)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method

.method public static dump(Ljava/lang/String;[BII)Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 8
aload 8
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
iload 3
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc "b"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 0
ifnull L0
aload 8
new java/lang/StringBuffer
dup
ldc " ("
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
ldc ")"
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
L0:
aload 8
bipush 58
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 8
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/String/length()I
bipush 8
iadd
bipush -8
iand
istore 6
aload 8
bipush 9
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
bipush 80
iload 6
isub
iconst_3
idiv
istore 7
iconst_0
istore 4
L1:
iload 4
iload 3
if_icmpge L2
iload 4
ifeq L3
iload 4
iload 7
irem
ifne L3
aload 8
bipush 10
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iconst_0
istore 5
L4:
iload 5
iload 6
bipush 8
idiv
if_icmpge L3
aload 8
bipush 9
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 5
iconst_1
iadd
istore 5
goto L4
L3:
aload 1
iload 4
iload 2
iadd
baload
sipush 255
iand
istore 5
aload 8
getstatic org/xbill/DNS/utils/hexdump/hex [C
iload 5
iconst_4
ishr
caload
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 8
getstatic org/xbill/DNS/utils/hexdump/hex [C
iload 5
bipush 15
iand
caload
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 8
bipush 32
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
iload 4
iconst_1
iadd
istore 4
goto L1
L2:
aload 8
bipush 10
invokevirtual java/lang/StringBuffer/append(C)Ljava/lang/StringBuffer;
pop
aload 8
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 9
.limit stack 4
.end method
