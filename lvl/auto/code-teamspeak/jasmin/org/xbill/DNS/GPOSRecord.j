.bytecode 50.0
.class public synchronized org/xbill/DNS/GPOSRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = -6349714958085750705L


.field private 'altitude' [B

.field private 'latitude' [B

.field private 'longitude' [B

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJDDD)V
aload 0
aload 1
bipush 27
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
dload 5
dload 7
invokespecial org/xbill/DNS/GPOSRecord/validate(DD)V
aload 0
dload 5
invokestatic java/lang/Double/toString(D)Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
putfield org/xbill/DNS/GPOSRecord/longitude [B
aload 0
dload 7
invokestatic java/lang/Double/toString(D)Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
putfield org/xbill/DNS/GPOSRecord/latitude [B
aload 0
dload 9
invokestatic java/lang/Double/toString(D)Ljava/lang/String;
invokevirtual java/lang/String/getBytes()[B
putfield org/xbill/DNS/GPOSRecord/altitude [B
return
.limit locals 11
.limit stack 6
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
bipush 27
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
L0:
aload 0
aload 5
invokestatic org/xbill/DNS/GPOSRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/GPOSRecord/longitude [B
aload 0
aload 6
invokestatic org/xbill/DNS/GPOSRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/GPOSRecord/latitude [B
aload 0
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLongitude()D
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLatitude()D
invokespecial org/xbill/DNS/GPOSRecord/validate(DD)V
aload 0
aload 7
invokestatic org/xbill/DNS/GPOSRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/GPOSRecord/altitude [B
L1:
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 8
.limit stack 6
.end method

.method private validate(DD)V
dload 1
ldc2_w -90.0D
dcmpg
iflt L0
dload 1
ldc2_w 90.0D
dcmpl
ifle L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "illegal longitude "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
dload 1
invokevirtual java/lang/StringBuffer/append(D)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
dload 3
ldc2_w -180.0D
dcmpg
iflt L2
dload 3
ldc2_w 180.0D
dcmpl
ifle L3
L2:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuffer
dup
ldc "illegal latitude "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
dload 3
invokevirtual java/lang/StringBuffer/append(D)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L3:
return
.limit locals 5
.limit stack 5
.end method

.method public getAltitude()D
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getAltitudeString()Ljava/lang/String;
invokestatic java/lang/Double/parseDouble(Ljava/lang/String;)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method public getAltitudeString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/GPOSRecord/altitude [B
iconst_0
invokestatic org/xbill/DNS/GPOSRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getLatitude()D
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLatitudeString()Ljava/lang/String;
invokestatic java/lang/Double/parseDouble(Ljava/lang/String;)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method public getLatitudeString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/GPOSRecord/latitude [B
iconst_0
invokestatic org/xbill/DNS/GPOSRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getLongitude()D
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLongitudeString()Ljava/lang/String;
invokestatic java/lang/Double/parseDouble(Ljava/lang/String;)D
dreturn
.limit locals 1
.limit stack 2
.end method

.method public getLongitudeString()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/GPOSRecord/longitude [B
iconst_0
invokestatic org/xbill/DNS/GPOSRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/GPOSRecord
dup
invokespecial org/xbill/DNS/GPOSRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
.catch java/lang/IllegalArgumentException from L1 to L3 using L4
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/GPOSRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/GPOSRecord/longitude [B
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/GPOSRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/GPOSRecord/latitude [B
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/GPOSRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/GPOSRecord/altitude [B
L1:
aload 0
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLongitude()D
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLatitude()D
invokespecial org/xbill/DNS/GPOSRecord/validate(DD)V
L3:
return
L2:
astore 2
aload 1
aload 2
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L4:
astore 1
new org/xbill/DNS/WireParseException
dup
aload 1
invokevirtual java/lang/IllegalArgumentException/getMessage()Ljava/lang/String;
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 5
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/GPOSRecord/longitude [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/GPOSRecord/latitude [B
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/GPOSRecord/altitude [B
L0:
aload 0
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLongitude()D
aload 0
invokevirtual org/xbill/DNS/GPOSRecord/getLatitude()D
invokespecial org/xbill/DNS/GPOSRecord/validate(DD)V
L1:
return
L2:
astore 1
new org/xbill/DNS/WireParseException
dup
aload 1
invokevirtual java/lang/IllegalArgumentException/getMessage()Ljava/lang/String;
invokespecial org/xbill/DNS/WireParseException/<init>(Ljava/lang/String;)V
athrow
.limit locals 2
.limit stack 5
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
aload 0
getfield org/xbill/DNS/GPOSRecord/longitude [B
iconst_1
invokestatic org/xbill/DNS/GPOSRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/GPOSRecord/latitude [B
iconst_1
invokestatic org/xbill/DNS/GPOSRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/GPOSRecord/altitude [B
iconst_1
invokestatic org/xbill/DNS/GPOSRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/GPOSRecord/longitude [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 1
aload 0
getfield org/xbill/DNS/GPOSRecord/latitude [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
aload 1
aload 0
getfield org/xbill/DNS/GPOSRecord/altitude [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
return
.limit locals 4
.limit stack 2
.end method
