.bytecode 50.0
.class public synchronized org/xbill/DNS/URIRecord
.super org/xbill/DNS/Record

.field private static final 'serialVersionUID' J = 7955422413971804232L


.field private 'priority' I

.field private 'target' [B

.field private 'weight' I

.method <init>()V
aload 0
invokespecial org/xbill/DNS/Record/<init>()V
aload 0
iconst_0
newarray byte
putfield org/xbill/DNS/URIRecord/target [B
return
.limit locals 1
.limit stack 2
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIILjava/lang/String;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
sipush 256
iload 2
lload 3
invokespecial org/xbill/DNS/Record/<init>(Lorg/xbill/DNS/Name;IIJ)V
aload 0
ldc "priority"
iload 5
invokestatic org/xbill/DNS/URIRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/URIRecord/priority I
aload 0
ldc "weight"
iload 6
invokestatic org/xbill/DNS/URIRecord/checkU16(Ljava/lang/String;I)I
putfield org/xbill/DNS/URIRecord/weight I
L0:
aload 0
aload 7
invokestatic org/xbill/DNS/URIRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/URIRecord/target [B
L1:
return
L2:
astore 1
new java/lang/IllegalArgumentException
dup
aload 1
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 8
.limit stack 6
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/URIRecord
dup
invokespecial org/xbill/DNS/URIRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method public getPriority()I
aload 0
getfield org/xbill/DNS/URIRecord/priority I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public getTarget()Ljava/lang/String;
aload 0
getfield org/xbill/DNS/URIRecord/target [B
iconst_0
invokestatic org/xbill/DNS/URIRecord/byteArrayToString([BZ)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 2
.end method

.method public getWeight()I
aload 0
getfield org/xbill/DNS/URIRecord/weight I
ireturn
.limit locals 1
.limit stack 1
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
.catch org/xbill/DNS/TextParseException from L0 to L1 using L2
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/URIRecord/priority I
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getUInt16()I
putfield org/xbill/DNS/URIRecord/weight I
L0:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getString()Ljava/lang/String;
invokestatic org/xbill/DNS/URIRecord/byteArrayFromString(Ljava/lang/String;)[B
putfield org/xbill/DNS/URIRecord/target [B
L1:
return
L2:
astore 2
aload 1
aload 2
invokevirtual org/xbill/DNS/TextParseException/getMessage()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
.limit locals 3
.limit stack 2
.end method

.method rrFromWire(Lorg/xbill/DNS/DNSInput;)V
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/URIRecord/priority I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readU16()I
putfield org/xbill/DNS/URIRecord/weight I
aload 0
aload 1
invokevirtual org/xbill/DNS/DNSInput/readCountedString()[B
putfield org/xbill/DNS/URIRecord/target [B
return
.limit locals 2
.limit stack 2
.end method

.method rrToString()Ljava/lang/String;
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/URIRecord/priority I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
aload 0
getfield org/xbill/DNS/URIRecord/weight I
invokevirtual java/lang/StringBuffer/append(I)Ljava/lang/StringBuffer;
ldc " "
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
aload 0
getfield org/xbill/DNS/URIRecord/target [B
iconst_1
invokestatic org/xbill/DNS/URIRecord/byteArrayToString([BZ)Ljava/lang/String;
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method rrToWire(Lorg/xbill/DNS/DNSOutput;Lorg/xbill/DNS/Compression;Z)V
aload 1
aload 0
getfield org/xbill/DNS/URIRecord/priority I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/URIRecord/weight I
invokevirtual org/xbill/DNS/DNSOutput/writeU16(I)V
aload 1
aload 0
getfield org/xbill/DNS/URIRecord/target [B
invokevirtual org/xbill/DNS/DNSOutput/writeCountedString([B)V
return
.limit locals 4
.limit stack 2
.end method
