.bytecode 50.0
.class public synchronized org/xbill/DNS/KEYRecord
.super org/xbill/DNS/KEYBase

.field public static final 'FLAG_NOAUTH' I = 32768


.field public static final 'FLAG_NOCONF' I = 16384


.field public static final 'FLAG_NOKEY' I = 49152


.field public static final 'OWNER_HOST' I = 512


.field public static final 'OWNER_USER' I = 0


.field public static final 'OWNER_ZONE' I = 256


.field public static final 'PROTOCOL_ANY' I = 255


.field public static final 'PROTOCOL_DNSSEC' I = 3


.field public static final 'PROTOCOL_EMAIL' I = 2


.field public static final 'PROTOCOL_IPSEC' I = 4


.field public static final 'PROTOCOL_TLS' I = 1


.field private static final 'serialVersionUID' J = 6385613447571488906L


.method <init>()V
aload 0
invokespecial org/xbill/DNS/KEYBase/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIIILjava/security/PublicKey;)V
aload 0
aload 1
bipush 25
iload 2
lload 3
iload 5
iload 6
iload 7
aload 8
iload 7
invokestatic org/xbill/DNS/DNSSEC/fromPublicKey(Ljava/security/PublicKey;I)[B
invokespecial org/xbill/DNS/KEYBase/<init>(Lorg/xbill/DNS/Name;IIJIII[B)V
aload 0
aload 8
putfield org/xbill/DNS/KEYRecord/publicKey Ljava/security/PublicKey;
return
.limit locals 9
.limit stack 11
.end method

.method public <init>(Lorg/xbill/DNS/Name;IJIII[B)V
aload 0
aload 1
bipush 25
iload 2
lload 3
iload 5
iload 6
iload 7
aload 8
invokespecial org/xbill/DNS/KEYBase/<init>(Lorg/xbill/DNS/Name;IIJIII[B)V
return
.limit locals 9
.limit stack 10
.end method

.method getObject()Lorg/xbill/DNS/Record;
new org/xbill/DNS/KEYRecord
dup
invokespecial org/xbill/DNS/KEYRecord/<init>()V
areturn
.limit locals 1
.limit stack 2
.end method

.method rdataFromString(Lorg/xbill/DNS/Tokenizer;Lorg/xbill/DNS/Name;)V
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getIdentifier()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/KEYRecord$Flags/value(Ljava/lang/String;)I
putfield org/xbill/DNS/KEYRecord/flags I
aload 0
getfield org/xbill/DNS/KEYRecord/flags I
ifge L0
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid flags: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L0:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getIdentifier()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/KEYRecord$Protocol/value(Ljava/lang/String;)I
putfield org/xbill/DNS/KEYRecord/proto I
aload 0
getfield org/xbill/DNS/KEYRecord/proto I
ifge L1
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid protocol: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L1:
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getIdentifier()Ljava/lang/String;
astore 2
aload 0
aload 2
invokestatic org/xbill/DNS/DNSSEC$Algorithm/value(Ljava/lang/String;)I
putfield org/xbill/DNS/KEYRecord/alg I
aload 0
getfield org/xbill/DNS/KEYRecord/alg I
ifge L2
aload 1
new java/lang/StringBuffer
dup
ldc "Invalid algorithm: "
invokespecial java/lang/StringBuffer/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual org/xbill/DNS/Tokenizer/exception(Ljava/lang/String;)Lorg/xbill/DNS/TextParseException;
athrow
L2:
aload 0
getfield org/xbill/DNS/KEYRecord/flags I
ldc_w 49152
iand
ldc_w 49152
if_icmpne L3
aload 0
aconst_null
putfield org/xbill/DNS/KEYRecord/key [B
return
L3:
aload 0
aload 1
invokevirtual org/xbill/DNS/Tokenizer/getBase64()[B
putfield org/xbill/DNS/KEYRecord/key [B
return
.limit locals 3
.limit stack 4
.end method
