.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/a/m
.super android/support/v4/app/ax
.implements com/teamspeak/ts3client/data/w

.field private 'aA' Landroid/widget/Spinner;

.field private 'aB' Landroid/widget/CheckBox;

.field private 'aC' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'aD' Lcom/teamspeak/ts3client/d/a/m;

.field private 'aE' Ljava/lang/String;

.field private 'aF' Ljava/lang/String;

.field private 'aG' I

.field private 'aH' I

.field private 'aI' I

.field private 'aJ' I

.field private 'aK' Ljava/lang/String;

.field private 'aL' Landroid/widget/Button;

.field private 'aM' I

.field private 'aN' I

.field private 'aO' Z

.field private 'aP' Z

.field private 'aQ' Landroid/widget/ImageView;

.field private 'aR' Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

.field private 'aS' Lcom/teamspeak/ts3client/customs/a;

.field private 'aT' Landroid/widget/TextView;

.field private 'aU' I

.field private 'aV' I

.field private 'aW' Z

.field 'at' Ljava/lang/String;

.field 'au' Z

.field private 'av' Lcom/teamspeak/ts3client/data/a;

.field private 'aw' Lcom/teamspeak/ts3client/customs/CustomCodecSettings;

.field private 'ax' Lcom/teamspeak/ts3client/bookmark/CustomEditText;

.field private 'ay' Landroid/widget/EditText;

.field private 'az' Landroid/widget/EditText;

.method public <init>(Lcom/teamspeak/ts3client/data/a;)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
ldc ""
putfield com/teamspeak/ts3client/d/a/m/aE Ljava/lang/String;
aload 0
ldc ""
putfield com/teamspeak/ts3client/d/a/m/aF Ljava/lang/String;
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aG I
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aH I
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aI I
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aJ I
aload 0
ldc ""
putfield com/teamspeak/ts3client/d/a/m/at Ljava/lang/String;
aload 0
ldc ""
putfield com/teamspeak/ts3client/d/a/m/aK Ljava/lang/String;
aload 0
iconst_4
putfield com/teamspeak/ts3client/d/a/m/aM I
aload 0
bipush 6
putfield com/teamspeak/ts3client/d/a/m/aN I
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aO Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aP Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/au Z
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aU I
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 1
ifnull L0
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/m/av Lcom/teamspeak/ts3client/data/a;
L1:
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/au Z
aload 0
aload 0
putfield com/teamspeak/ts3client/d/a/m/aD Lcom/teamspeak/ts3client/d/a/m;
return
L0:
aload 0
new com/teamspeak/ts3client/data/a
dup
invokespecial com/teamspeak/ts3client/data/a/<init>()V
putfield com/teamspeak/ts3client/d/a/m/av Lcom/teamspeak/ts3client/data/a;
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/aO Z
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/aP Z
goto L1
.limit locals 2
.limit stack 3
.end method

.method public <init>(Lcom/teamspeak/ts3client/data/a;Z)V
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/a/m/<init>(Lcom/teamspeak/ts3client/data/a;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/aO Z
aload 0
iload 2
putfield com/teamspeak/ts3client/d/a/m/aP Z
return
.limit locals 3
.limit stack 2
.end method

.method private A()V
aload 0
invokevirtual com/teamspeak/ts3client/d/a/m/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/a/s
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/s/<init>(Lcom/teamspeak/ts3client/d/a/m;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
return
.limit locals 1
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/m;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/d/a/m/aG I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/d/a/m;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aD Lcom/teamspeak/ts3client/d/a/m;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/m/aE Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/m;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/d/a/m/aH I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/m/aK Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/String;)V
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/au Z
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/m/at Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a/m;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/d/a/m/aI I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/m/aF Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/d/a/m;)Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aW Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a/m;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/d/a/m/aM I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic d(Lcom/teamspeak/ts3client/d/a/m;Ljava/lang/String;)Ljava/lang/String;
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/m/at Ljava/lang/String;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/a/m;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/d/a/m/aN I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic e(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/bookmark/CustomEditText;
aload 0
getfield com/teamspeak/ts3client/d/a/m/ax Lcom/teamspeak/ts3client/bookmark/CustomEditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/a/m;I)I
aload 0
iload 1
putfield com/teamspeak/ts3client/d/a/m/aJ I
iload 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static synthetic f(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic g(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aG I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic h(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic i(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/d/a/m/av Lcom/teamspeak/ts3client/data/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic j(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aJ I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic k(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic l(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/a/m/ay Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic m(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aK Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic n(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aE Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic o(Lcom/teamspeak/ts3client/d/a/m;)Landroid/widget/EditText;
aload 0
getfield com/teamspeak/ts3client/d/a/m/az Landroid/widget/EditText;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic p(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aF Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic q(Lcom/teamspeak/ts3client/d/a/m;)Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/au Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic r(Lcom/teamspeak/ts3client/d/a/m;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/a/m/at Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic s(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aH I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic t(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aI I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic u(Lcom/teamspeak/ts3client/d/a/m;)Lcom/teamspeak/ts3client/customs/CustomCodecSettings;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aR Lcom/teamspeak/ts3client/customs/CustomCodecSettings;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic v(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aM I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic w(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aN I
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic x(Lcom/teamspeak/ts3client/d/a/m;)Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aO Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private y()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/d/a/m/at Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic y(Lcom/teamspeak/ts3client/d/a/m;)Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aP Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic z(Lcom/teamspeak/ts3client/d/a/m;)I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private z()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aO Z
ifeq L0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aI Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L1
aload 0
getfield com/teamspeak/ts3client/d/a/m/az Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L1:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aK Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L2
aload 0
getfield com/teamspeak/ts3client/d/a/m/ay Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L2:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aJ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L3
aload 0
getfield com/teamspeak/ts3client/d/a/m/aQ Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setEnabled(Z)V
L3:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aW Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L4
aload 0
getfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setEnabled(Z)V
L4:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aE Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L5
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
L5:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aF Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L6
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
L6:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aG Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L7
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L7:
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/aW Z
L8:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aZ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
istore 1
aload 2
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L9
aload 0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aZ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/b(I)I
putfield com/teamspeak/ts3client/d/a/m/aU I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
iconst_m1
if_icmpge L10
L9:
aload 0
iconst_m1
putfield com/teamspeak/ts3client/d/a/m/aU I
L10:
aload 0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/aB Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/i;)I
putfield com/teamspeak/ts3client/d/a/m/aV I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
ifle L11
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
if_icmple L11
aload 0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
putfield com/teamspeak/ts3client/d/a/m/aV I
L11:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
tableswitch -1
L12
L13
default : L14
L14:
return
L0:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bf Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L15
aload 0
getfield com/teamspeak/ts3client/d/a/m/ax Lcom/teamspeak/ts3client/bookmark/CustomEditText;
iconst_0
invokevirtual com/teamspeak/ts3client/bookmark/CustomEditText/setEnabled(Z)V
L15:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bg Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L16
aload 0
getfield com/teamspeak/ts3client/d/a/m/az Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L16:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bi Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L17
aload 0
getfield com/teamspeak/ts3client/d/a/m/ay Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L17:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bh Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L18
aload 0
getfield com/teamspeak/ts3client/d/a/m/aQ Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setEnabled(Z)V
L18:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bb Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L19
aload 0
getfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setEnabled(Z)V
L19:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bc Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L20
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L20:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bd Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L21
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L21:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/be Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L22
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L22:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bt Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L8
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
goto L8
L12:
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
L13:
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
ldc "0"
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 5
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
aload 1
ldc_w 2130903073
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
checkcast android/widget/ScrollView
astore 1
aload 0
getfield com/teamspeak/ts3client/d/a/m/aO Z
ifne L0
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/a/m/av Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
L1:
ldc "channeldialog.editname"
aload 1
ldc_w 2131493039
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "channeldialog.editpassword"
aload 1
ldc_w 2131493042
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "channeldialog.edittopic"
aload 1
ldc_w 2131493045
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "channeldialog.editdesc"
aload 1
ldc_w 2131493048
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "channeldialog.edittype"
aload 1
ldc_w 2131493050
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "channeldialog.editdefault"
aload 1
ldc_w 2131493053
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
ldc "channeldialog.editdeletedelay"
aload 1
ldc_w 2131493056
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
aload 1
ldc_w 2131493040
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/bookmark/CustomEditText
putfield com/teamspeak/ts3client/d/a/m/ax Lcom/teamspeak/ts3client/bookmark/CustomEditText;
aload 0
aload 1
ldc_w 2131493043
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield com/teamspeak/ts3client/d/a/m/ay Landroid/widget/EditText;
aload 0
aload 1
ldc_w 2131493046
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/EditText
putfield com/teamspeak/ts3client/d/a/m/az Landroid/widget/EditText;
aload 0
aload 1
ldc_w 2131493049
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageView
putfield com/teamspeak/ts3client/d/a/m/aQ Landroid/widget/ImageView;
aload 0
aload 1
ldc_w 2131493059
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/CustomCodecSettings
putfield com/teamspeak/ts3client/d/a/m/aR Lcom/teamspeak/ts3client/customs/CustomCodecSettings;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aQ Landroid/widget/ImageView;
new com/teamspeak/ts3client/d/a/n
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/n/<init>(Lcom/teamspeak/ts3client/d/a/m;)V
invokevirtual android/widget/ImageView/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493051
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/Spinner
putfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
aload 0
ldc "channeldialog.edittype.spinner"
aload 0
invokevirtual com/teamspeak/ts3client/d/a/m/i()Landroid/support/v4/app/bb;
iconst_3
invokestatic com/teamspeak/ts3client/data/e/a/b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;
putfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
invokevirtual android/widget/Spinner/setAdapter(Landroid/widget/SpinnerAdapter;)V
aload 0
aload 1
ldc_w 2131493054
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/CheckBox
putfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
new com/teamspeak/ts3client/d/a/o
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/o/<init>(Lcom/teamspeak/ts3client/d/a/m;)V
invokevirtual android/widget/CheckBox/setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
aload 0
aload 1
ldc_w 2131493057
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
putfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
new com/teamspeak/ts3client/d/a/p
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/p/<init>(Lcom/teamspeak/ts3client/d/a/m;)V
invokevirtual android/widget/Spinner/setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
aload 0
aload 1
ldc_w 2131493060
invokevirtual android/widget/ScrollView/findViewById(I)Landroid/view/View;
checkcast android/widget/Button
putfield com/teamspeak/ts3client/d/a/m/aL Landroid/widget/Button;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aL Landroid/widget/Button;
ldc "button.save"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aL Landroid/widget/Button;
new com/teamspeak/ts3client/d/a/q
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/q/<init>(Lcom/teamspeak/ts3client/d/a/m;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aO Z
ifeq L2
aload 0
getfield com/teamspeak/ts3client/d/a/m/aR Lcom/teamspeak/ts3client/customs/CustomCodecSettings;
new com/teamspeak/ts3client/data/d/ab
dup
aload 0
getfield com/teamspeak/ts3client/d/a/m/aM I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aN I
invokespecial com/teamspeak/ts3client/data/d/ab/<init>(II)V
iconst_1
invokevirtual com/teamspeak/ts3client/customs/CustomCodecSettings/a(Lcom/teamspeak/ts3client/data/d/ab;Z)V
L2:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aO Z
ifne L3
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
aload 0
getfield com/teamspeak/ts3client/d/a/m/av Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
ldc "ChannelEdit"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestChannelDescription(JJLjava/lang/String;)I
pop
L3:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aO Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aI Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L5
aload 0
getfield com/teamspeak/ts3client/d/a/m/az Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L5:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aK Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L6
aload 0
getfield com/teamspeak/ts3client/d/a/m/ay Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L6:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aJ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L7
aload 0
getfield com/teamspeak/ts3client/d/a/m/aQ Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setEnabled(Z)V
L7:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aW Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L8
aload 0
getfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setEnabled(Z)V
L8:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aE Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L9
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
iconst_1
invokevirtual android/widget/Spinner/setSelection(I)V
L9:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aF Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L10
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aA Landroid/widget/Spinner;
iconst_0
invokevirtual android/widget/Spinner/setSelection(I)V
L10:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/aG Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L11
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L11:
aload 0
iconst_1
putfield com/teamspeak/ts3client/d/a/m/aW Z
L12:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
astore 2
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aZ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
istore 3
aload 2
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 3
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L13
aload 0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/aZ Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/b(I)I
putfield com/teamspeak/ts3client/d/a/m/aU I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
iconst_m1
if_icmpge L14
L13:
aload 0
iconst_m1
putfield com/teamspeak/ts3client/d/a/m/aU I
L14:
aload 0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
getstatic com/teamspeak/ts3client/jni/i/aB Lcom/teamspeak/ts3client/jni/i;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/a(JLcom/teamspeak/ts3client/jni/i;)I
putfield com/teamspeak/ts3client/d/a/m/aV I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
ifle L15
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
if_icmple L15
aload 0
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
putfield com/teamspeak/ts3client/d/a/m/aV I
L15:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aU I
tableswitch -1
L16
L17
default : L18
L18:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
invokevirtual android/widget/TextView/isEnabled()Z
ifeq L19
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
new com/teamspeak/ts3client/d/a/r
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/r/<init>(Lcom/teamspeak/ts3client/d/a/m;)V
invokevirtual android/widget/TextView/addTextChangedListener(Landroid/text/TextWatcher;)V
L19:
aload 1
areturn
L0:
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
ldc "channeldialog.create.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
goto L1
L4:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bf Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L20
aload 0
getfield com/teamspeak/ts3client/d/a/m/ax Lcom/teamspeak/ts3client/bookmark/CustomEditText;
iconst_0
invokevirtual com/teamspeak/ts3client/bookmark/CustomEditText/setEnabled(Z)V
L20:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bg Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L21
aload 0
getfield com/teamspeak/ts3client/d/a/m/az Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L21:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bi Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L22
aload 0
getfield com/teamspeak/ts3client/d/a/m/ay Landroid/widget/EditText;
iconst_0
invokevirtual android/widget/EditText/setEnabled(Z)V
L22:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bh Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L23
aload 0
getfield com/teamspeak/ts3client/d/a/m/aQ Landroid/widget/ImageView;
iconst_0
invokevirtual android/widget/ImageView/setEnabled(Z)V
L23:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bb Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L24
aload 0
getfield com/teamspeak/ts3client/d/a/m/aB Landroid/widget/CheckBox;
iconst_0
invokevirtual android/widget/CheckBox/setEnabled(Z)V
L24:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bc Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L25
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_2
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L25:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bd Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L26
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_1
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L26:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/be Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L27
aload 0
getfield com/teamspeak/ts3client/d/a/m/aS Lcom/teamspeak/ts3client/customs/a;
iconst_0
invokevirtual com/teamspeak/ts3client/customs/a/a(I)V
L27:
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
getstatic com/teamspeak/ts3client/jni/g/bt Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/f/a/a(Lcom/teamspeak/ts3client/jni/g;)Z
ifne L12
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
goto L12
L16:
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aV I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L18
L17:
aload 0
iconst_0
putfield com/teamspeak/ts3client/d/a/m/aW Z
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
iconst_0
invokevirtual android/widget/TextView/setEnabled(Z)V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aT Landroid/widget/TextView;
ldc "0"
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L18
.limit locals 4
.limit stack 6
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/UpdateChannel
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/UpdateChannel
getfield com/teamspeak/ts3client/jni/events/UpdateChannel/a J
aload 0
getfield com/teamspeak/ts3client/d/a/m/av Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
lcmp
ifne L1
aload 0
invokespecial com/teamspeak/ts3client/d/a/m/A()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L1:
return
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
ifeq L1
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerPermissionError
getfield com/teamspeak/ts3client/jni/events/rare/ServerPermissionError/a I
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getstatic com/teamspeak/ts3client/jni/g/bK Lcom/teamspeak/ts3client/jni/g;
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
if_icmpne L1
aload 0
invokespecial com/teamspeak/ts3client/d/a/m/A()V
aload 0
getfield com/teamspeak/ts3client/d/a/m/aC Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method
