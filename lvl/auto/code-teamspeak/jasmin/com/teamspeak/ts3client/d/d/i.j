.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/d/i
.super android/support/v4/app/Fragment
.implements com/teamspeak/ts3client/data/w

.field private 'a' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'b' Landroid/widget/ListView;

.field private 'c' Lcom/teamspeak/ts3client/d/d/a;

.field private 'd' Landroid/widget/ImageButton;

.field private 'e' Landroid/widget/ImageButton;

.field private 'f' Lcom/teamspeak/ts3client/d/d/i;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/d/i;)Lcom/teamspeak/ts3client/d/d/a;
aload 0
getfield com/teamspeak/ts3client/d/d/i/c Lcom/teamspeak/ts3client/d/d/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 1
ldc_w 2130903128
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 0
aload 1
ldc_w 2131493347
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ListView
putfield com/teamspeak/ts3client/d/d/i/b Landroid/widget/ListView;
aload 0
aload 1
ldc_w 2131493349
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageButton
putfield com/teamspeak/ts3client/d/d/i/d Landroid/widget/ImageButton;
aload 0
getfield com/teamspeak/ts3client/d/d/i/d Landroid/widget/ImageButton;
new com/teamspeak/ts3client/d/d/l
dup
aload 0
invokespecial com/teamspeak/ts3client/d/d/l/<init>(Lcom/teamspeak/ts3client/d/d/i;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
aload 1
ldc_w 2131493350
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ImageButton
putfield com/teamspeak/ts3client/d/d/i/e Landroid/widget/ImageButton;
aload 0
getfield com/teamspeak/ts3client/d/d/i/e Landroid/widget/ImageButton;
new com/teamspeak/ts3client/d/d/m
dup
aload 0
invokespecial com/teamspeak/ts3client/d/d/m/<init>(Lcom/teamspeak/ts3client/d/d/i;)V
invokevirtual android/widget/ImageButton/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/n()V
aload 0
getfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "menu.temporarypasswords"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a()V
aload 0
getfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
getfield com/teamspeak/ts3client/d/d/i/c Lcom/teamspeak/ts3client/d/d/a;
ifnonnull L0
aload 0
new com/teamspeak/ts3client/d/d/a
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
invokespecial com/teamspeak/ts3client/d/d/a/<init>(Landroid/content/Context;Landroid/support/v4/app/bi;)V
putfield com/teamspeak/ts3client/d/d/i/c Lcom/teamspeak/ts3client/d/d/a;
L1:
aload 0
getfield com/teamspeak/ts3client/d/d/i/b Landroid/widget/ListView;
aload 0
getfield com/teamspeak/ts3client/d/d/i/c Lcom/teamspeak/ts3client/d/d/a;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
aload 0
getfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
getfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
ldc "request temppasslist"
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestServerTemporaryPasswordList(JLjava/lang/String;)I
pop
return
L0:
aload 0
getfield com/teamspeak/ts3client/d/d/i/c Lcom/teamspeak/ts3client/d/d/a;
getfield com/teamspeak/ts3client/d/d/a/a Ljava/util/ArrayList;
invokevirtual java/util/ArrayList/clear()V
goto L1
.limit locals 1
.limit stack 5
.end method

.method public final a(Landroid/app/Activity;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/app/Activity;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 0
putfield com/teamspeak/ts3client/d/d/i/f Lcom/teamspeak/ts3client/d/d/i;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
aload 1
invokeinterface android/view/Menu/clear()V 0
aload 0
aload 1
aload 2
invokespecial android/support/v4/app/Fragment/a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
astore 2
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
ifne L0
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
ldc "CreatedNewTempPw"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifne L1
aload 2
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
ldc "DeletedOldTempPw"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
L1:
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/d/j
dup
aload 0
invokespecial com/teamspeak/ts3client/d/d/j/<init>(Lcom/teamspeak/ts3client/d/d/i;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList
ifeq L2
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList
astore 1
new com/teamspeak/ts3client/d/d/c
dup
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/a J
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/b Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/c Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/d Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/e Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/f J
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/g J
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/h J
aload 1
getfield com/teamspeak/ts3client/jni/events/rare/ServerTemporaryPasswordList/i Ljava/lang/String;
invokespecial com/teamspeak/ts3client/d/d/c/<init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLjava/lang/String;)V
astore 1
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/i()Landroid/support/v4/app/bb;
new com/teamspeak/ts3client/d/d/k
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/d/d/k/<init>(Lcom/teamspeak/ts3client/d/d/i;Lcom/teamspeak/ts3client/d/d/c;)V
invokevirtual android/support/v4/app/bb/runOnUiThread(Ljava/lang/Runnable;)V
L2:
return
.limit locals 3
.limit stack 15
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
invokevirtual com/teamspeak/ts3client/d/d/i/a()V
return
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
invokespecial android/support/v4/app/Fragment/g()V
aload 0
getfield com/teamspeak/ts3client/d/d/i/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
return
.limit locals 1
.limit stack 2
.end method
