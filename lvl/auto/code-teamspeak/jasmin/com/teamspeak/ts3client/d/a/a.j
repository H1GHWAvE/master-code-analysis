.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/d/a/a
.super android/support/v4/app/ax
.implements com/teamspeak/ts3client/data/w

.field private static 'at' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'au' Lcom/teamspeak/ts3client/data/a;

.field private 'av' Z

.method public <init>(Lcom/teamspeak/ts3client/data/a;Z)V
aload 0
invokespecial android/support/v4/app/ax/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
aload 0
iload 2
putfield com/teamspeak/ts3client/d/a/a/av Z
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/d/a/a;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic y()Lcom/teamspeak/ts3client/Ts3Application;
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 1
invokevirtual android/view/LayoutInflater/getContext()Landroid/content/Context;
invokevirtual android/content/Context/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
astore 1
aload 1
putstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
new android/widget/ScrollView
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/ScrollView/<init>(Landroid/content/Context;)V
astore 1
new android/widget/LinearLayout
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
astore 2
aload 2
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 2
new android/view/ViewGroup$LayoutParams
dup
iconst_m1
iconst_m1
invokespecial android/view/ViewGroup$LayoutParams/<init>(II)V
invokevirtual android/widget/LinearLayout/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 1
aload 2
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 0
getfield android/support/v4/app/ax/j Landroid/app/Dialog;
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/a Ljava/lang/String;
invokevirtual android/app/Dialog/setTitle(Ljava/lang/CharSequence;)V
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
lcmp
ifeq L0
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc "dialog.channel.join.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 3
new com/teamspeak/ts3client/d/a/b
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/b/<init>(Lcom/teamspeak/ts3client/d/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L0:
aload 0
getfield com/teamspeak/ts3client/d/a/a/av Z
ifeq L1
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc "dialog.channel.info.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 3
new com/teamspeak/ts3client/d/a/c
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/c/<init>(Lcom/teamspeak/ts3client/d/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L1:
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc "dialog.channel.edit.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 3
new com/teamspeak/ts3client/d/a/d
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/d/<init>(Lcom/teamspeak/ts3client/d/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/r Z
ifne L2
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc "dialog.channel.delete.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 3
new com/teamspeak/ts3client/d/a/e
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/e/<init>(Lcom/teamspeak/ts3client/d/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L2:
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 3
aload 3
ldc "dialog.channel.move.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
aload 3
new com/teamspeak/ts3client/d/a/h
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/h/<init>(Lcom/teamspeak/ts3client/d/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/g J
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
lcmp
ifeq L3
new android/widget/Button
dup
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/i()Landroid/support/v4/app/bb;
invokespecial android/widget/Button/<init>(Landroid/content/Context;)V
astore 3
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/k Z
ifeq L4
aload 3
ldc "dialog.channel.subscribe.text2"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
L5:
aload 3
new com/teamspeak/ts3client/d/a/l
dup
aload 0
invokespecial com/teamspeak/ts3client/d/a/l/<init>(Lcom/teamspeak/ts3client/d/a/a;)V
invokevirtual android/widget/Button/setOnClickListener(Landroid/view/View$OnClickListener;)V
aload 2
aload 3
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
L3:
aload 1
areturn
L4:
aload 3
ldc "dialog.channel.subscribe.text1"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/widget/Button/setText(Ljava/lang/CharSequence;)V
goto L5
.limit locals 4
.limit stack 5
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/ServerError
ifeq L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/b I
ifne L0
aload 1
checkcast com/teamspeak/ts3client/jni/events/ServerError
getfield com/teamspeak/ts3client/jni/events/ServerError/c Ljava/lang/String;
ldc "checkPW"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
iconst_0
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/j Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
new java/lang/StringBuilder
dup
ldc "join "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/d/a/a/au Lcom/teamspeak/ts3client/data/a;
getfield com/teamspeak/ts3client/data/a/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I
pop
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
aload 0
invokevirtual com/teamspeak/ts3client/d/a/a/b()V
L0:
return
.limit locals 2
.limit stack 10
.end method

.method public final e()V
aload 0
invokespecial android/support/v4/app/ax/e()V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 1
.end method

.method public final f()V
aload 0
invokespecial android/support/v4/app/ax/f()V
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
ifnull L0
getstatic com/teamspeak/ts3client/d/a/a/at Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/b(Lcom/teamspeak/ts3client/data/w;)V
L0:
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/b(Landroid/support/v4/app/ax;)V
return
.limit locals 1
.limit stack 2
.end method
