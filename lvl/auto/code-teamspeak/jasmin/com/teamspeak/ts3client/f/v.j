.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/f/v
.super android/widget/RelativeLayout
.implements android/view/View$OnClickListener

.field private 'a' Ljava/lang/String;

.field private 'b' Lcom/teamspeak/ts3client/f/x;

.field private 'c' Landroid/support/v4/app/bi;

.field private 'd' Ljava/util/TreeMap;

.field private 'e' Ljava/lang/String;

.method public <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/TreeMap;Landroid/support/v4/app/bi;)V
aload 0
aload 1
invokespecial android/widget/RelativeLayout/<init>(Landroid/content/Context;)V
aload 0
new android/widget/RelativeLayout$LayoutParams
dup
iconst_m1
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
invokevirtual com/teamspeak/ts3client/f/v/setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_0
bipush 18
iconst_0
bipush 18
invokevirtual com/teamspeak/ts3client/f/v/setPadding(IIII)V
aload 0
ldc_w 2130837686
invokevirtual com/teamspeak/ts3client/f/v/setBackgroundResource(I)V
aload 0
aload 6
putfield com/teamspeak/ts3client/f/v/c Landroid/support/v4/app/bi;
aload 0
aload 2
putfield com/teamspeak/ts3client/f/v/a Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/f/v/d Ljava/util/TreeMap;
aload 0
aload 4
putfield com/teamspeak/ts3client/f/v/e Ljava/lang/String;
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 4
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 4
aload 2
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 4
ldc_w 20.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 4
aconst_null
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 4
iconst_1
invokevirtual android/widget/TextView/setId(I)V
aload 1
aload 3
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
ldc_w 10.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 1
aconst_null
iconst_2
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 1
iconst_2
invokevirtual android/widget/TextView/setId(I)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 2
bipush 10
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(I)V
aload 0
aload 4
aload 2
invokevirtual com/teamspeak/ts3client/f/v/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
new android/widget/RelativeLayout$LayoutParams
dup
bipush -2
bipush -2
invokespecial android/widget/RelativeLayout$LayoutParams/<init>(II)V
astore 2
aload 2
iconst_3
aload 4
invokevirtual android/widget/TextView/getId()I
invokevirtual android/widget/RelativeLayout$LayoutParams/addRule(II)V
aload 0
aload 1
aload 2
invokevirtual com/teamspeak/ts3client/f/v/addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
aload 0
iconst_1
invokevirtual com/teamspeak/ts3client/f/v/setClickable(Z)V
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/f/v/setOnClickListener(Landroid/view/View$OnClickListener;)V
return
.limit locals 7
.limit stack 5
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/f/v;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/f/v/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/f/v;)Ljava/util/TreeMap;
aload 0
getfield com/teamspeak/ts3client/f/v/d Ljava/util/TreeMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/f/v;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/f/v/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final onClick(Landroid/view/View;)V
aload 0
new com/teamspeak/ts3client/f/x
dup
aload 0
iconst_0
invokespecial com/teamspeak/ts3client/f/x/<init>(Lcom/teamspeak/ts3client/f/v;B)V
putfield com/teamspeak/ts3client/f/v/b Lcom/teamspeak/ts3client/f/x;
aload 0
getfield com/teamspeak/ts3client/f/v/b Lcom/teamspeak/ts3client/f/x;
aload 0
getfield com/teamspeak/ts3client/f/v/c Landroid/support/v4/app/bi;
aload 0
getfield com/teamspeak/ts3client/f/v/a Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/f/x/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
aload 1
iconst_1
invokevirtual android/view/View/performHapticFeedback(I)Z
pop
return
.limit locals 2
.limit stack 5
.end method
