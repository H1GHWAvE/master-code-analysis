.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/a/r
.super java/lang/Object
.implements android/speech/tts/TextToSpeech$OnInitListener
.implements com/teamspeak/ts3client/a/n

.field 'a' Landroid/speech/tts/TextToSpeech;

.field private 'b' Ljava/util/HashMap;

.field private 'c' Ljava/util/HashMap;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/a/r/c Ljava/util/HashMap;
return
.limit locals 1
.limit stack 3
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/a/r;)Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/a/r/c Ljava/util/HashMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
new com/teamspeak/ts3client/a/s
dup
aload 0
invokespecial com/teamspeak/ts3client/a/s/<init>(Lcom/teamspeak/ts3client/a/r;)V
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/s/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 1
.limit stack 3
.end method

.method public final a(Landroid/content/Context;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
aload 0
new android/speech/tts/TextToSpeech
dup
aload 1
aload 0
invokespecial android/speech/tts/TextToSpeech/<init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V
putfield com/teamspeak/ts3client/a/r/a Landroid/speech/tts/TextToSpeech;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/a/r/b Ljava/util/HashMap;
aload 0
getfield com/teamspeak/ts3client/a/r/b Ljava/util/HashMap;
ldc "streamType"
ldc "0"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/teamspeak/ts3client/a/r/b Ljava/util/HashMap;
ldc "volume"
ldc "0.4"
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
new com/teamspeak/ts3client/a/s
dup
aload 0
invokespecial com/teamspeak/ts3client/a/s/<init>(Lcom/teamspeak/ts3client/a/r;)V
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/s/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
L0:
new com/teamspeak/ts3client/a/s
dup
aload 0
invokespecial com/teamspeak/ts3client/a/s/<init>(Lcom/teamspeak/ts3client/a/r;)V
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/s/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L6 to L7 using L5
aload 0
getfield com/teamspeak/ts3client/a/r/c Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
astore 4
aload 4
ifnonnull L8
return
L8:
aload 4
astore 3
aload 2
ifnull L9
L0:
aload 4
ldc "${clientname}"
aload 2
getfield com/teamspeak/ts3client/a/o/a Ljava/lang/String;
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 3
L1:
aload 3
astore 4
aload 4
astore 3
L3:
aload 4
ldc "${channelname}"
aload 2
getfield com/teamspeak/ts3client/a/o/c Ljava/lang/String;
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 4
L4:
aload 4
astore 3
L6:
aload 4
ldc "${servername}"
aload 2
getfield com/teamspeak/ts3client/a/o/d Ljava/lang/String;
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 2
L7:
aload 2
astore 3
L9:
aload 0
getfield com/teamspeak/ts3client/a/r/a Landroid/speech/tts/TextToSpeech;
aload 3
iconst_0
aload 0
getfield com/teamspeak/ts3client/a/r/b Ljava/util/HashMap;
invokevirtual android/speech/tts/TextToSpeech/speak(Ljava/lang/String;ILjava/util/HashMap;)I
pop
return
L2:
astore 2
aload 4
astore 3
L10:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "SOUND ERROR: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
goto L9
L5:
astore 2
goto L10
.limit locals 5
.limit stack 5
.end method

.method public final a(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/a/r/a Landroid/speech/tts/TextToSpeech;
aload 1
iconst_0
aload 0
getfield com/teamspeak/ts3client/a/r/b Ljava/util/HashMap;
invokevirtual android/speech/tts/TextToSpeech/speak(Ljava/lang/String;ILjava/util/HashMap;)I
pop
return
.limit locals 2
.limit stack 4
.end method

.method public final b()V
aload 0
getfield com/teamspeak/ts3client/a/r/a Landroid/speech/tts/TextToSpeech;
invokevirtual android/speech/tts/TextToSpeech/shutdown()V
return
.limit locals 1
.limit stack 1
.end method

.method public final onInit(I)V
aload 0
getfield com/teamspeak/ts3client/a/r/a Landroid/speech/tts/TextToSpeech;
getstatic java/util/Locale/US Ljava/util/Locale;
invokevirtual android/speech/tts/TextToSpeech/setLanguage(Ljava/util/Locale;)I
pop
return
.limit locals 2
.limit stack 2
.end method
