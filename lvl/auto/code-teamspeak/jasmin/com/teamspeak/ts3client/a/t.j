.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/a/t
.super java/lang/Object
.implements com/teamspeak/ts3client/a/n

.field 'a' Ljava/util/HashMap;

.field 'b' Ljava/lang/String;

.field 'c' Landroid/media/SoundPool;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
putfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
aload 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/content/sound/default/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/a/t/b Ljava/lang/String;
return
.limit locals 1
.limit stack 3
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/a/t;)Landroid/media/SoundPool;
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a()V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
invokevirtual android/media/SoundPool/release()V
aload 0
new android/media/SoundPool
dup
iconst_4
iconst_0
iconst_0
invokespecial android/media/SoundPool/<init>(III)V
putfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
new com/teamspeak/ts3client/a/u
dup
aload 0
invokespecial com/teamspeak/ts3client/a/u/<init>(Lcom/teamspeak/ts3client/a/t;)V
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/u/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
L0:
new com/teamspeak/ts3client/a/u
dup
aload 0
invokespecial com/teamspeak/ts3client/a/u/<init>(Lcom/teamspeak/ts3client/a/t;)V
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/u/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 1
.limit stack 6
.end method

.method public final a(Landroid/content/Context;)V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
aload 0
new android/media/SoundPool
dup
iconst_4
iconst_0
iconst_0
invokespecial android/media/SoundPool/<init>(III)V
putfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
new com/teamspeak/ts3client/a/u
dup
aload 0
invokespecial com/teamspeak/ts3client/a/u/<init>(Lcom/teamspeak/ts3client/a/t;)V
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/u/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
L0:
new com/teamspeak/ts3client/a/u
dup
aload 0
invokespecial com/teamspeak/ts3client/a/u/<init>(Lcom/teamspeak/ts3client/a/t;)V
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/a/u/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 2
.limit stack 6
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/h;Lcom/teamspeak/ts3client/a/o;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
ldc "CLIENT_"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L0
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
ldc "CLIENT_R"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifne L0
aload 2
getfield com/teamspeak/ts3client/a/o/b I
tableswitch 0
L1
L2
L3
default : L4
L4:
return
L1:
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "neutral_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "neutral_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ldc_w 0.3F
ldc_w 0.3F
iconst_1
iconst_0
fconst_1
invokevirtual android/media/SoundPool/play(IFFIIF)I
pop
return
L2:
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "blocked_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "blocked_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ldc_w 0.3F
ldc_w 0.3F
iconst_1
iconst_0
fconst_1
invokevirtual android/media/SoundPool/play(IFFIIF)I
pop
return
L3:
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "friend_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
new java/lang/StringBuilder
dup
ldc "friend_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ldc_w 0.3F
ldc_w 0.3F
iconst_1
iconst_0
fconst_1
invokevirtual android/media/SoundPool/play(IFFIIF)I
pop
return
L0:
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L4
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
aload 0
getfield com/teamspeak/ts3client/a/t/a Ljava/util/HashMap;
aload 1
invokevirtual com/teamspeak/ts3client/jni/h/toString()Ljava/lang/String;
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ldc_w 0.3F
ldc_w 0.3F
iconst_1
iconst_0
fconst_1
invokevirtual android/media/SoundPool/play(IFFIIF)I
pop
return
.limit locals 3
.limit stack 7
.end method

.method public final a(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final b()V
aload 0
getfield com/teamspeak/ts3client/a/t/c Landroid/media/SoundPool;
invokevirtual android/media/SoundPool/release()V
return
.limit locals 1
.limit stack 1
.end method
