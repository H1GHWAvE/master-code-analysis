.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/tsdns/h
.super java/lang/Object

.field public 'a' Ljava/lang/String;

.field 'b' I

.field public 'c' I

.field public 'd' I

.field 'e' I

.field 'f' Ljava/lang/String;

.method public <init>(Lcom/teamspeak/ts3client/tsdns/h;)V
aload 0
aload 1
getfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/tsdns/h/c I
aload 1
getfield com/teamspeak/ts3client/tsdns/h/b I
aload 1
getfield com/teamspeak/ts3client/tsdns/h/d I
aload 1
getfield com/teamspeak/ts3client/tsdns/h/e I
iconst_1
invokespecial com/teamspeak/ts3client/tsdns/h/<init>(Ljava/lang/String;IIIIZ)V
return
.limit locals 2
.limit stack 7
.end method

.method public <init>(Ljava/lang/String;IIIIZ)V
.catch java/net/UnknownHostException from L0 to L1 using L2
.catch java/net/UnknownHostException from L3 to L4 using L2
.catch java/net/UnknownHostException from L5 to L6 using L2
.catch java/net/UnknownHostException from L7 to L8 using L2
.catch java/net/UnknownHostException from L9 to L10 using L2
.catch java/net/UnknownHostException from L11 to L12 using L2
.catch java/net/UnknownHostException from L13 to L14 using L2
.catch java/net/UnknownHostException from L15 to L16 using L2
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/tsdns/h/f Ljava/lang/String;
aload 0
iload 5
putfield com/teamspeak/ts3client/tsdns/h/e I
aload 1
astore 8
aload 1
astore 7
L0:
aload 1
ldc "_ts3._udp."
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L4
L1:
aload 1
astore 7
L3:
aload 1
ldc "_ts3._udp."
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 8
L4:
aload 8
astore 1
aload 8
astore 7
L5:
aload 8
ldc "_tsdns._tcp."
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L8
L6:
aload 8
astore 7
L7:
aload 8
ldc "_tsdns._tcp."
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
astore 1
L8:
aload 1
astore 8
aload 1
astore 7
L9:
aload 1
ldc "."
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L12
L10:
aload 1
astore 7
L11:
aload 1
iconst_0
aload 1
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 8
L12:
iload 6
ifeq L17
aload 8
astore 7
L13:
aload 0
aload 8
invokestatic org/xbill/DNS/Address/getByName(Ljava/lang/String;)Ljava/net/InetAddress;
invokevirtual java/net/InetAddress/getHostAddress()Ljava/lang/String;
putfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
L14:
aload 0
iload 2
putfield com/teamspeak/ts3client/tsdns/h/c I
aload 0
iload 3
putfield com/teamspeak/ts3client/tsdns/h/b I
aload 0
iload 4
putfield com/teamspeak/ts3client/tsdns/h/d I
return
L17:
aload 8
astore 7
L15:
aload 0
aload 8
putfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
L16:
goto L14
L2:
astore 1
aload 0
aload 7
putfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
getstatic com/teamspeak/ts3client/tsdns/f/c I
istore 4
goto L14
.limit locals 9
.limit stack 4
.end method

.method private a()I
aload 0
getfield com/teamspeak/ts3client/tsdns/h/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/tsdns/h/e I
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/String;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
return
.limit locals 2
.limit stack 2
.end method

.method private b()I
aload 0
getfield com/teamspeak/ts3client/tsdns/h/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/tsdns/h/b I
return
.limit locals 2
.limit stack 2
.end method

.method private c()I
aload 0
getfield com/teamspeak/ts3client/tsdns/h/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private c(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/tsdns/h/c I
return
.limit locals 2
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/h/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d(I)V
aload 0
iload 1
putfield com/teamspeak/ts3client/tsdns/h/d I
return
.limit locals 2
.limit stack 2
.end method

.method private e()I
aload 0
getfield com/teamspeak/ts3client/tsdns/h/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/tsdns/h/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
