.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/Ts3Jni
.super java/lang/Object

.field private static 'a' Lcom/teamspeak/ts3client/jni/Ts3Jni;

.field private static 'b' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a()I
getstatic com/teamspeak/ts3client/jni/Ts3Jni/b I
ireturn
.limit locals 0
.limit stack 1
.end method

.method private a(JJLcom/teamspeak/ts3client/jni/i;D)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/i/aD I
dload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setServerVariableAsDouble(JJID)I
ireturn
.limit locals 8
.limit stack 8
.end method

.method private a(JJLcom/teamspeak/ts3client/jni/i;I)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/i/aD I
iload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setServerVariableAsInt(JJII)I
ireturn
.limit locals 7
.limit stack 7
.end method

.method private a(JJLcom/teamspeak/ts3client/jni/i;J)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/i/aD I
lload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setServerVariableAsUint64(JJIJ)I
ireturn
.limit locals 8
.limit stack 8
.end method

.method private b(JLcom/teamspeak/ts3client/jni/f;)F
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/f/ap I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerConnectionVariableAsFloat(JI)F
freturn
.limit locals 4
.limit stack 4
.end method

.method public static b()Lcom/teamspeak/ts3client/jni/Ts3Jni;
.catch all from L0 to L1 using L2
.catch java/lang/UnsatisfiedLinkError from L3 to L4 using L5
.catch all from L3 to L4 using L2
.catch all from L4 to L6 using L2
.catch all from L6 to L7 using L2
ldc com/teamspeak/ts3client/jni/Ts3Jni
monitorenter
L0:
getstatic com/teamspeak/ts3client/jni/Ts3Jni/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
astore 0
L1:
aload 0
ifnonnull L6
L3:
ldc "ts3client_android"
invokestatic java/lang/System/loadLibrary(Ljava/lang/String;)V
getstatic java/lang/System/out Ljava/io/PrintStream;
ldc "Load Lib"
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L4:
new com/teamspeak/ts3client/jni/Ts3Jni
dup
invokespecial com/teamspeak/ts3client/jni/Ts3Jni/<init>()V
astore 0
aload 0
putstatic com/teamspeak/ts3client/jni/Ts3Jni/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 0
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_startInit()I
putstatic com/teamspeak/ts3client/jni/Ts3Jni/b I
L6:
getstatic com/teamspeak/ts3client/jni/Ts3Jni/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
astore 0
L7:
ldc com/teamspeak/ts3client/jni/Ts3Jni
monitorexit
aload 0
areturn
L2:
astore 0
ldc com/teamspeak/ts3client/jni/Ts3Jni
monitorexit
aload 0
athrow
L5:
astore 0
goto L4
.limit locals 1
.limit stack 2
.end method

.method private b(JLcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/d/ak I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getClientSelfVariableAsString(JI)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method public static c()V
.catch java/lang/UnsatisfiedLinkError from L0 to L1 using L2
getstatic com/teamspeak/ts3client/jni/Ts3Jni/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
ifnull L3
L0:
getstatic com/teamspeak/ts3client/jni/Ts3Jni/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_destroyClientLib()I
pop
L1:
aconst_null
putstatic com/teamspeak/ts3client/jni/Ts3Jni/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
L3:
return
L2:
astore 0
goto L1
.limit locals 1
.limit stack 1
.end method

.method public final a(JILcom/teamspeak/ts3client/jni/d;)I
aload 0
lload 1
iload 3
aload 4
getfield com/teamspeak/ts3client/jni/d/ak I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getClientVariableAsInt(JII)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/c/I I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getChannelVariableAsInt(JJI)I
ireturn
.limit locals 6
.limit stack 6
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;I)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/c/I I
iload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setChannelVariableAsInt(JJII)I
ireturn
.limit locals 7
.limit stack 7
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;J)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/c/I I
lload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setChannelVariableAsUInt64(JJIJ)I
ireturn
.limit locals 8
.limit stack 8
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/c;Ljava/lang/String;)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/c/I I
aload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setChannelVariableAsString(JJILjava/lang/String;)I
ireturn
.limit locals 7
.limit stack 7
.end method

.method public final a(JJLcom/teamspeak/ts3client/jni/i;Ljava/lang/String;)I
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/i/aD I
aload 6
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setServerVariableAsString(JJILjava/lang/String;)I
ireturn
.limit locals 7
.limit stack 7
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/d;)I
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/d/ak I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getClientSelfVariableAsInt(JI)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/d;I)I
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/d/ak I
iload 4
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setClientSelfVariableAsInt(JII)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/d;Ljava/lang/String;)I
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/d/ak I
aload 4
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_setClientSelfVariableAsString(JILjava/lang/String;)I
ireturn
.limit locals 5
.limit stack 5
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/i;)I
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/i/aD I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerVariableAsInt(JI)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final a(JILcom/teamspeak/ts3client/jni/f;)J
aload 0
lload 1
iload 3
aload 4
getfield com/teamspeak/ts3client/jni/f/ap I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getConnectionVariableAsUInt64(JII)J
lreturn
.limit locals 5
.limit stack 5
.end method

.method public final a(JLcom/teamspeak/ts3client/jni/f;)J
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/f/ap I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerConnectionVariableAsUInt64(JI)J
lreturn
.limit locals 4
.limit stack 4
.end method

.method public final b(JILcom/teamspeak/ts3client/jni/f;)D
aload 0
lload 1
iload 3
aload 4
getfield com/teamspeak/ts3client/jni/f/ap I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getConnectionVariableAsDouble(JII)D
dreturn
.limit locals 5
.limit stack 5
.end method

.method public final b(JILcom/teamspeak/ts3client/jni/d;)Ljava/lang/String;
aload 0
lload 1
iload 3
aload 4
getfield com/teamspeak/ts3client/jni/d/ak I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getClientVariableAsString(JII)Ljava/lang/String;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final b(JJLcom/teamspeak/ts3client/jni/c;)Ljava/lang/String;
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/c/I I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getChannelVariableAsString(JJI)Ljava/lang/String;
areturn
.limit locals 6
.limit stack 6
.end method

.method public final b(JLcom/teamspeak/ts3client/jni/i;)Ljava/lang/String;
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/i/aD I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerVariableAsString(JI)Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final c(JILcom/teamspeak/ts3client/jni/d;)J
aload 0
lload 1
iload 3
aload 4
getfield com/teamspeak/ts3client/jni/d/ak I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getClientVariableAsUInt64(JII)J
lreturn
.limit locals 5
.limit stack 5
.end method

.method public final c(JJLcom/teamspeak/ts3client/jni/c;)J
aload 0
lload 1
lload 3
aload 5
getfield com/teamspeak/ts3client/jni/c/I I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getChannelVariableAsUInt64(JJI)J
lreturn
.limit locals 6
.limit stack 6
.end method

.method public final c(JLcom/teamspeak/ts3client/jni/i;)J
aload 0
lload 1
aload 3
getfield com/teamspeak/ts3client/jni/i/aD I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getServerVariableAsUInt64(JI)J
lreturn
.limit locals 4
.limit stack 4
.end method

.method public final c(JILcom/teamspeak/ts3client/jni/f;)Ljava/lang/String;
aload 0
lload 1
iload 3
aload 4
getfield com/teamspeak/ts3client/jni/f/ap I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_getConnectionVariableAsString(JII)Ljava/lang/String;
areturn
.limit locals 5
.limit stack 5
.end method

.method public native requestAudioData()[S
.end method

.method public native ts3client_activateCaptureDevice(J)I
.end method

.method public native ts3client_allowWhispersFrom(JI)I
.end method

.method public native ts3client_android_checkSignatureData(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;I)I
.end method

.method public native ts3client_android_getLicenseAgreementVersion()I
.end method

.method public native ts3client_android_getUpdaterURL()Ljava/lang/String;
.end method

.method public native ts3client_android_parseProtobufDataResult(Ljava/nio/ByteBuffer;I)Z
.end method

.method public native ts3client_banadd(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_banclient(JIJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_banclientUID(JLjava/lang/String;JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_banclientdbid(JJJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_bandel(JJLjava/lang/String;)I
.end method

.method public native ts3client_bandelall(JLjava/lang/String;)I
.end method

.method public native ts3client_bbcode_shouldPrependHTTP(Ljava/lang/String;)Z
.end method

.method public native ts3client_cleanUpConnectionInfo(JI)I
.end method

.method public native ts3client_clientChatClosed(JLjava/lang/String;ILjava/lang/String;)I
.end method

.method public native ts3client_clientChatComposing(JILjava/lang/String;)I
.end method

.method public native ts3client_closeCaptureDevice(J)I
.end method

.method public native ts3client_closePlaybackDevice(J)I
.end method

.method public native ts3client_convertBBCodetoHTMLtags_v2(Ljava/lang/String;IZ)Ljava/lang/String;
.end method

.method public native ts3client_createIdentity()Ljava/lang/String;
.end method

.method public native ts3client_destroyClientLib()I
.end method

.method public native ts3client_destroyServerConnectionHandler(J)I
.end method

.method public native ts3client_flushChannelCreation(JJLjava/lang/String;)I
.end method

.method public native ts3client_flushChannelUpdates(JJLjava/lang/String;)I
.end method

.method public native ts3client_flushClientSelfUpdates(JLjava/lang/String;)I
.end method

.method public native ts3client_flushServerUpdates(JJLjava/lang/String;)I
.end method

.method public native ts3client_getChannelClientList(JJ)[I
.end method

.method public native ts3client_getChannelList(J)[J
.end method

.method public native ts3client_getChannelOfClient(JI)J
.end method

.method public native ts3client_getChannelVariableAsInt(JJI)I
.end method

.method public native ts3client_getChannelVariableAsString(JJI)Ljava/lang/String;
.end method

.method public native ts3client_getChannelVariableAsUInt64(JJI)J
.end method

.method public native ts3client_getClientID(J)I
.end method

.method public native ts3client_getClientLibVersion()Ljava/lang/String;
.end method

.method public native ts3client_getClientLibVersionNumber()I
.end method

.method public native ts3client_getClientList(J)[I
.end method

.method public native ts3client_getClientSelfVariableAsInt(JI)I
.end method

.method public native ts3client_getClientSelfVariableAsString(JI)Ljava/lang/String;
.end method

.method public native ts3client_getClientVariableAsInt(JII)I
.end method

.method public native ts3client_getClientVariableAsString(JII)Ljava/lang/String;
.end method

.method public native ts3client_getClientVariableAsUInt64(JII)J
.end method

.method public native ts3client_getConnectionVariableAsDouble(JII)D
.end method

.method public native ts3client_getConnectionVariableAsString(JII)Ljava/lang/String;
.end method

.method public native ts3client_getConnectionVariableAsUInt64(JII)J
.end method

.method public native ts3client_getParentChannelOfChannel(JJ)J
.end method

.method public native ts3client_getPlaybackConfigValueAsFloat(JLjava/lang/String;)F
.end method

.method public native ts3client_getPreProcessorConfigValue(JLjava/lang/String;)Ljava/lang/String;
.end method

.method public native ts3client_getPreProcessorInfoValueFloat(JLjava/lang/String;)F
.end method

.method public native ts3client_getServerConnectionVariableAsFloat(JI)F
.end method

.method public native ts3client_getServerConnectionVariableAsUInt64(JI)J
.end method

.method public native ts3client_getServerLicenseType(J)I
.end method

.method public native ts3client_getServerVariableAsInt(JI)I
.end method

.method public native ts3client_getServerVariableAsString(JI)Ljava/lang/String;
.end method

.method public native ts3client_getServerVariableAsUInt64(JI)J
.end method

.method public native ts3client_identityStringToFilename(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public native ts3client_openCaptureDevice(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_openPlaybackDevice(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_prepareAudioDevice(II)I
.end method

.method public native ts3client_privilegeKeyUse(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_processCustomCaptureData(Ljava/lang/String;[SI)I
.end method

.method public native ts3client_removeFromAllowedWhispersFrom(JI)I
.end method

.method public native ts3client_requestBanList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelClientAddPerm(JJJ[J[IILjava/lang/String;)I
.end method

.method public native ts3client_requestChannelClientDelPerm(JJJ[JILjava/lang/String;)I
.end method

.method public native ts3client_requestChannelDelete(JJILjava/lang/String;)I
.end method

.method public native ts3client_requestChannelDescription(JJLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelGroupList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelMove(JJJJLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelSubscribe(J[JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelSubscribeAll(JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelUnsubscribe(J[JLjava/lang/String;)I
.end method

.method public native ts3client_requestChannelUnsubscribeAll(JLjava/lang/String;)I
.end method

.method public native ts3client_requestClientIDs(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientKickFromChannel(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientKickFromServer(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientMove(JIJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientPoke(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestClientSetIsTalker(JJILjava/lang/String;)I
.end method

.method public native ts3client_requestClientUIDfromClientID(JILjava/lang/String;)I
.end method

.method public native ts3client_requestClientVariables(JILjava/lang/String;)I
.end method

.method public native ts3client_requestComplainAdd(JJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestConnectionInfo(JILjava/lang/String;)I
.end method

.method public native ts3client_requestFile(JJLjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestFileInfo(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestIsTalker(JILjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestMuteClients(J[ILjava/lang/String;)I
.end method

.method public native ts3client_requestPermissionList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestSendChannelTextMsg(JLjava/lang/String;JLjava/lang/String;)I
.end method

.method public native ts3client_requestSendPrivateTextMsg(JLjava/lang/String;ILjava/lang/String;)I
.end method

.method public native ts3client_requestSendServerTextMsg(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestServerConnectionInfo(JLjava/lang/String;)I
.end method

.method public native ts3client_requestServerGroupAddClient(JJJLjava/lang/String;)I
.end method

.method public native ts3client_requestServerGroupDelClient(JJJLjava/lang/String;)I
.end method

.method public native ts3client_requestServerGroupList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestServerTemporaryPasswordAdd(JLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestServerTemporaryPasswordDel(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_requestServerTemporaryPasswordList(JLjava/lang/String;)I
.end method

.method public native ts3client_requestServerVariables(J)I
.end method

.method public native ts3client_requestSetClientChannelGroup(J[J[J[JILjava/lang/String;)I
.end method

.method public native ts3client_requestUnmuteClients(J[ILjava/lang/String;)I
.end method

.method public native ts3client_setChannelVariableAsInt(JJII)I
.end method

.method public native ts3client_setChannelVariableAsString(JJILjava/lang/String;)I
.end method

.method public native ts3client_setChannelVariableAsUInt64(JJIJ)I
.end method

.method public native ts3client_setClientSelfVariableAsInt(JII)I
.end method

.method public native ts3client_setClientSelfVariableAsString(JILjava/lang/String;)I
.end method

.method public native ts3client_setClientVolumeModifier(JIF)I
.end method

.method public native ts3client_setLocalTestMode(JI)I
.end method

.method public native ts3client_setPlaybackConfigValue(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_setPreProcessorConfigValue(JLjava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_setServerVariableAsDouble(JJID)I
.end method

.method public native ts3client_setServerVariableAsInt(JJII)I
.end method

.method public native ts3client_setServerVariableAsString(JJILjava/lang/String;)I
.end method

.method public native ts3client_setServerVariableAsUint64(JJIJ)I
.end method

.method public native ts3client_spawnNewServerConnectionHandler()J
.end method

.method public native ts3client_startConnection(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_startConnectionEx(JLjava/lang/String;Ljava/lang/String;ILjava/lang/String;[Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native ts3client_startInit()I
.end method

.method public native ts3client_stopConnection(JLjava/lang/String;)I
.end method

.method public native ts3client_unregisterCustomDevice(Ljava/lang/String;)I
.end method

.method public native ts3client_verifyChannelPassword(JJLjava/lang/String;Ljava/lang/String;)I
.end method
