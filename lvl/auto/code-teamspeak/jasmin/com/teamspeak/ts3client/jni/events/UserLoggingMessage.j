.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/UserLoggingMessage
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field private 'a' Ljava/lang/String;

.field private 'b' I

.field private 'c' Ljava/lang/String;

.field private 'd' J

.field private 'e' Ljava/lang/String;

.field private 'f' Ljava/lang/String;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/a Ljava/lang/String;
aload 0
iload 2
putfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/b I
aload 0
aload 3
putfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/c Ljava/lang/String;
aload 0
lload 4
putfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/d J
aload 0
aload 6
putfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/e Ljava/lang/String;
aload 0
aload 7
putfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/f Ljava/lang/String;
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 8
.limit stack 3
.end method

.method private a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/f Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d()I
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/e Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "UserLoggingMessage [logMessage="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/a Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", logLevel="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", logChannel="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", logID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", logTime="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/e Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", completeLogString="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/UserLoggingMessage/f Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
