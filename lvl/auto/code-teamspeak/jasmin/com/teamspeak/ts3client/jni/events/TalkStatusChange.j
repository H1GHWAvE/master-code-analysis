.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/TalkStatusChange
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' J

.field public 'b' I

.field private 'c' I

.field private 'd' I

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(JIII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/jni/events/TalkStatusChange/a J
aload 0
iload 3
putfield com/teamspeak/ts3client/jni/events/TalkStatusChange/b I
aload 0
iload 4
putfield com/teamspeak/ts3client/jni/events/TalkStatusChange/c I
aload 0
iload 5
putfield com/teamspeak/ts3client/jni/events/TalkStatusChange/d I
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 6
.limit stack 3
.end method

.method private d()J
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final a()I
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/d I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final b()I
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final c()I
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/b I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "TalkStatusChange [serverConnectionHandlerID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/a J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", status="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/b I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", isReceivedWhisper="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", clientID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/TalkStatusChange/d I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
