.bytecode 50.0
.class public synchronized com/teamspeak/ts3client/jni/events/rare/FileTransferStatus
.super java/lang/Object
.implements com/teamspeak/ts3client/jni/k

.field public 'a' I

.field public 'b' J

.field private 'c' Ljava/lang/String;

.field private 'd' J

.field private 'e' J

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private <init>(IJLjava/lang/String;JJ)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iload 1
putfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
aload 0
lload 2
putfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/b J
aload 0
aload 4
putfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/c Ljava/lang/String;
aload 0
lload 5
putfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/d J
aload 0
lload 7
putfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/e J
aload 0
invokestatic com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/jni/k;)V
return
.limit locals 9
.limit stack 3
.end method

.method private a()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/e J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()J
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/b J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/c Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()I
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "FileTransferStatus [transferID="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/a I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", status="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/b J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", statusMessage="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/c Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", remotefileSize="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc ", serverConnectionHandlerID="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/jni/events/rare/FileTransferStatus/e J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
