.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/b
.super android/app/Dialog

.field private 'a' Landroid/widget/ScrollView;

.field private 'b' Landroid/widget/LinearLayout;

.method public <init>(Landroid/content/Context;)V
.catch android/content/pm/PackageManager$NameNotFoundException from L0 to L1 using L2
.catch java/lang/NullPointerException from L0 to L1 using L3
aload 0
aload 1
ldc_w 2131165365
invokespecial android/app/Dialog/<init>(Landroid/content/Context;I)V
aload 0
ldc "changelog.info"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/teamspeak/ts3client/b/setTitle(Ljava/lang/CharSequence;)V
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
L0:
aload 1
invokevirtual android/content/Context/getPackageManager()Landroid/content/pm/PackageManager;
aload 1
invokevirtual android/content/Context/getPackageName()Ljava/lang/String;
iconst_0
invokevirtual android/content/pm/PackageManager/getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
getfield android/content/pm/PackageInfo/versionCode I
istore 2
L1:
aload 0
new android/widget/ScrollView
dup
aload 1
invokespecial android/widget/ScrollView/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/b/a Landroid/widget/ScrollView;
aload 0
new android/widget/LinearLayout
dup
aload 1
invokespecial android/widget/LinearLayout/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/b/b Landroid/widget/LinearLayout;
aload 0
getfield com/teamspeak/ts3client/b/b Landroid/widget/LinearLayout;
iconst_1
invokevirtual android/widget/LinearLayout/setOrientation(I)V
aload 0
getfield com/teamspeak/ts3client/b/b Landroid/widget/LinearLayout;
iconst_5
iconst_5
iconst_5
iconst_5
invokevirtual android/widget/LinearLayout/setPadding(IIII)V
aload 1
invokevirtual android/content/Context/getResources()Landroid/content/res/Resources;
ldc_w 2131361793
invokevirtual android/content/res/Resources/getStringArray(I)[Ljava/lang/String;
astore 5
aload 5
arraylength
istore 4
iconst_0
istore 3
L4:
iload 3
iload 4
if_icmpge L5
aload 5
iload 3
aaload
astore 6
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 7
aload 7
iconst_0
iconst_0
iconst_0
iconst_2
invokevirtual android/widget/TextView/setPadding(IIII)V
aload 6
ldc "->"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L6
aload 7
ldc_w 16.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 7
getstatic android/graphics/Typeface/DEFAULT Landroid/graphics/Typeface;
iconst_1
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 7
aload 6
ldc "->"
ldc "\u2605"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
L7:
aload 0
getfield com/teamspeak/ts3client/b/b Landroid/widget/LinearLayout;
aload 7
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
iload 3
iconst_1
iadd
istore 3
goto L4
L2:
astore 5
iconst_0
istore 2
goto L1
L3:
astore 5
iconst_0
istore 2
goto L1
L6:
aload 6
ldc "***"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L8
aload 7
ldc_w 22.0F
invokevirtual android/widget/TextView/setTextSize(F)V
aload 7
getstatic android/graphics/Typeface/DEFAULT Landroid/graphics/Typeface;
iconst_3
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 7
aload 6
ldc "***"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L7
L8:
aload 7
getstatic android/graphics/Typeface/DEFAULT Landroid/graphics/Typeface;
iconst_0
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 7
new java/lang/StringBuilder
dup
ldc "\u2022"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
goto L7
L5:
new android/widget/TextView
dup
aload 1
invokespecial android/widget/TextView/<init>(Landroid/content/Context;)V
astore 1
aload 1
new java/lang/StringBuilder
dup
ldc "\nPlay Store version: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
aload 1
aconst_null
iconst_2
invokevirtual android/widget/TextView/setTypeface(Landroid/graphics/Typeface;I)V
aload 0
getfield com/teamspeak/ts3client/b/b Landroid/widget/LinearLayout;
aload 1
invokevirtual android/widget/LinearLayout/addView(Landroid/view/View;)V
aload 0
getfield com/teamspeak/ts3client/b/a Landroid/widget/ScrollView;
aload 0
getfield com/teamspeak/ts3client/b/b Landroid/widget/LinearLayout;
invokevirtual android/widget/ScrollView/addView(Landroid/view/View;)V
aload 0
aload 0
getfield com/teamspeak/ts3client/b/a Landroid/widget/ScrollView;
invokevirtual com/teamspeak/ts3client/b/setContentView(Landroid/view/View;)V
return
.limit locals 8
.limit stack 5
.end method
