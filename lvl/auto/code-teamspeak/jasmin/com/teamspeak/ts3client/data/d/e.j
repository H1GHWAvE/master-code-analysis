.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/data/d/e
.super android/os/AsyncTask

.method <init>()V
aload 0
invokespecial android/os/AsyncTask/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static transient a([Lcom/teamspeak/ts3client/data/d/d;)Lcom/teamspeak/ts3client/data/d/d;
aload 0
iconst_0
aaload
getfield com/teamspeak/ts3client/data/d/d/e Ljava/lang/String;
astore 1
aload 1
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 1
aload 1
iconst_0
iconst_0
aload 1
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 1
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 0
iconst_0
aaload
aload 1
putfield com/teamspeak/ts3client/data/d/d/c Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
aaload
areturn
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_0
aaload
getfield com/teamspeak/ts3client/data/d/d/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 2
aload 0
iconst_0
aaload
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
aload 2
invokevirtual java/io/File/exists()Z
ifne L1
aload 2
invokevirtual java/io/File/mkdirs()Z
pop
L1:
aload 1
ldc "ts3image://"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L2
invokestatic com/teamspeak/ts3client/data/d/a/a()Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 3
aload 3
invokevirtual java/util/regex/Matcher/find()Z
ifeq L3
aload 3
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
pop2
aload 3
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L4
aload 0
iconst_0
aaload
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
L3:
new com/teamspeak/ts3client/data/c/e
dup
invokespecial com/teamspeak/ts3client/data/c/e/<init>()V
astore 3
aload 0
iconst_0
aaload
aload 3
aload 1
aload 2
invokevirtual com/teamspeak/ts3client/data/c/e/a(Ljava/lang/String;Ljava/io/File;)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/d/d/c Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
aaload
areturn
L4:
aload 0
iconst_0
aaload
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 2
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 3
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
goto L3
L2:
aload 0
iconst_0
aaload
aconst_null
putfield com/teamspeak/ts3client/data/d/d/c Landroid/graphics/drawable/Drawable;
aload 0
iconst_0
aaload
areturn
.limit locals 4
.limit stack 6
.end method

.method private a(Lcom/teamspeak/ts3client/data/d/d;)V
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 1
getfield com/teamspeak/ts3client/data/d/d/b Lcom/teamspeak/ts3client/data/d/c;
aload 1
getfield com/teamspeak/ts3client/data/d/d/a Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
invokeinterface com/teamspeak/ts3client/data/d/c/a(Ljava/lang/String;Ljava/lang/String;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method private transient a([Ljava/lang/Void;)V
aload 0
aload 1
invokespecial android/os/AsyncTask/onProgressUpdate([Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast [Lcom/teamspeak/ts3client/data/d/d;
astore 1
aload 1
iconst_0
aaload
getfield com/teamspeak/ts3client/data/d/d/e Ljava/lang/String;
astore 2
aload 2
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getResources()Landroid/content/res/Resources;
ldc_w 2130837580
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
astore 2
aload 2
iconst_0
iconst_0
aload 2
invokevirtual android/graphics/drawable/Drawable/getIntrinsicWidth()I
iconst_0
iadd
aload 2
invokevirtual android/graphics/drawable/Drawable/getIntrinsicHeight()I
iconst_0
iadd
invokevirtual android/graphics/drawable/Drawable/setBounds(IIII)V
aload 1
iconst_0
aaload
aload 2
putfield com/teamspeak/ts3client/data/d/d/c Landroid/graphics/drawable/Drawable;
aload 1
iconst_0
aaload
areturn
L0:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/cache/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/q Ljava/lang/String;
ldc "/"
ldc ""
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iconst_0
aaload
getfield com/teamspeak/ts3client/data/d/d/d J
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 3
aload 1
iconst_0
aaload
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
aload 3
invokevirtual java/io/File/exists()Z
ifne L1
aload 3
invokevirtual java/io/File/mkdirs()Z
pop
L1:
aload 2
ldc "ts3image://"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L2
invokestatic com/teamspeak/ts3client/data/d/a/a()Ljava/util/regex/Pattern;
aload 2
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 4
aload 4
invokevirtual java/util/regex/Matcher/find()Z
ifeq L3
aload 4
iconst_2
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
pop2
aload 4
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_1
if_icmple L4
aload 1
iconst_0
aaload
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
L3:
new com/teamspeak/ts3client/data/c/e
dup
invokespecial com/teamspeak/ts3client/data/c/e/<init>()V
astore 4
aload 1
iconst_0
aaload
aload 4
aload 2
aload 3
invokevirtual com/teamspeak/ts3client/data/c/e/a(Ljava/lang/String;Ljava/io/File;)Landroid/graphics/drawable/Drawable;
putfield com/teamspeak/ts3client/data/d/d/c Landroid/graphics/drawable/Drawable;
aload 1
iconst_0
aaload
areturn
L4:
aload 1
iconst_0
aaload
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 3
invokevirtual java/io/File/getAbsolutePath()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_3
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
putfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
goto L3
L2:
aload 1
iconst_0
aaload
aconst_null
putfield com/teamspeak/ts3client/data/d/d/c Landroid/graphics/drawable/Drawable;
aload 1
iconst_0
aaload
areturn
.limit locals 5
.limit stack 6
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
aload 1
checkcast com/teamspeak/ts3client/data/d/d
astore 1
aload 0
aload 1
invokespecial android/os/AsyncTask/onPostExecute(Ljava/lang/Object;)V
aload 1
getfield com/teamspeak/ts3client/data/d/d/b Lcom/teamspeak/ts3client/data/d/c;
aload 1
getfield com/teamspeak/ts3client/data/d/d/a Ljava/lang/String;
aload 1
getfield com/teamspeak/ts3client/data/d/d/f Ljava/lang/String;
invokeinterface com/teamspeak/ts3client/data/d/c/a(Ljava/lang/String;Ljava/lang/String;)V 2
return
.limit locals 2
.limit stack 3
.end method

.method protected final volatile synthetic onProgressUpdate([Ljava/lang/Object;)V
aload 0
aload 1
checkcast [Ljava/lang/Void;
invokespecial android/os/AsyncTask/onProgressUpdate([Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 2
.end method
