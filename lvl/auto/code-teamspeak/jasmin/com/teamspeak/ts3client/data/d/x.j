.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/x
.super java/lang/Object

.field private static 'a' Ljava/util/regex/Pattern;

.method static <clinit>()V
ldc "([\\u0591-\\u07FF\\uFB1D-\\uFDFD\\uFE70-\\uFEFC])"
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;)Ljava/util/regex/Pattern;
putstatic com/teamspeak/ts3client/data/d/x/a Ljava/util/regex/Pattern;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
aload 0
ifnonnull L0
ldc "error"
areturn
L0:
new java/lang/StringBuffer
dup
aload 0
invokevirtual java/lang/String/length()I
invokespecial java/lang/StringBuffer/<init>(I)V
astore 1
getstatic com/teamspeak/ts3client/data/d/x/a Ljava/util/regex/Pattern;
aload 0
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 0
L1:
aload 0
invokevirtual java/util/regex/Matcher/find()Z
ifeq L2
aload 0
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 2
aload 0
aload 1
new java/lang/StringBuilder
dup
ldc "\u202e"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "\u202c"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic java/util/regex/Matcher/quoteReplacement(Ljava/lang/String;)Ljava/lang/String;
invokevirtual java/util/regex/Matcher/appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;
pop
goto L1
L2:
aload 0
aload 1
invokevirtual java/util/regex/Matcher/appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
pop
new java/lang/StringBuilder
dup
ldc "\u200e"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 5
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
ldc ""
astore 10
ldc ""
astore 12
aload 0
invokevirtual java/lang/String/toCharArray()[C
astore 13
aload 13
arraylength
istore 9
iconst_0
istore 5
iconst_0
istore 8
iconst_0
istore 6
iconst_0
istore 7
L0:
iload 5
iload 9
if_icmpge L1
aload 13
iload 5
caload
istore 1
iload 8
ifeq L2
iload 1
bipush 92
if_icmpne L3
aload 10
invokevirtual java/lang/String/length()I
iconst_2
if_icmpne L4
new java/lang/StringBuilder
dup
ldc "00"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L5:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_0
iconst_4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
ldc ""
astore 0
iconst_0
istore 2
iconst_1
istore 3
iload 6
istore 4
L6:
iload 5
iconst_1
iadd
istore 5
aload 11
astore 12
aload 0
astore 10
iload 2
istore 8
iload 4
istore 6
iload 3
istore 7
goto L0
L4:
aload 10
astore 0
aload 10
invokevirtual java/lang/String/length()I
iconst_3
if_icmpne L5
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L5
L3:
iload 1
bipush 48
if_icmplt L7
iload 1
bipush 57
if_icmple L8
L7:
iload 1
bipush 97
if_icmplt L9
iload 1
bipush 102
if_icmple L8
L9:
iload 1
bipush 65
if_icmplt L10
iload 1
bipush 70
if_icmpgt L10
L8:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
aload 12
astore 11
iload 8
istore 2
iload 6
istore 4
iload 7
istore 3
goto L6
L10:
aload 10
invokevirtual java/lang/String/length()I
iconst_2
if_icmpne L11
new java/lang/StringBuilder
dup
ldc "00"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L12:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_0
iconst_4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
ldc ""
astore 0
iconst_0
istore 2
iload 6
istore 4
iload 7
istore 3
goto L6
L11:
aload 10
astore 0
aload 10
invokevirtual java/lang/String/length()I
iconst_3
if_icmpne L12
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L12
L2:
iload 6
ifeq L13
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 10
aload 12
astore 11
aload 10
astore 0
iload 8
istore 2
iload 6
istore 4
iload 7
istore 3
aload 10
invokevirtual java/lang/String/length()I
iconst_2
if_icmplt L6
iconst_1
istore 2
iconst_0
istore 4
aload 12
astore 11
aload 10
astore 0
iload 7
istore 3
goto L6
L13:
iload 7
ifeq L14
aload 12
astore 11
aload 10
astore 0
iload 8
istore 2
iload 6
istore 4
iload 7
istore 3
iload 1
bipush 92
if_icmpeq L6
aload 12
astore 11
aload 10
astore 0
iload 8
istore 2
iload 6
istore 4
iload 7
istore 3
iload 1
bipush 120
if_icmpne L6
ldc ""
astore 0
iconst_1
istore 4
iconst_0
istore 3
aload 12
astore 11
iload 8
istore 2
goto L6
L14:
iload 1
bipush 92
if_icmpne L15
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
iconst_1
istore 3
aload 12
astore 11
iload 8
istore 2
iload 6
istore 4
goto L6
L15:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 11
aload 10
astore 0
iload 8
istore 2
iload 6
istore 4
iload 7
istore 3
goto L6
L1:
aload 12
astore 0
iload 8
ifeq L16
aload 10
invokevirtual java/lang/String/length()I
iconst_2
if_icmpne L17
new java/lang/StringBuilder
dup
ldc "00"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L18:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 12
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
iconst_0
iconst_4
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
bipush 16
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;I)I
i2c
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L16:
aload 0
areturn
L17:
aload 10
astore 0
aload 10
invokevirtual java/lang/String/length()I
iconst_3
if_icmpne L18
new java/lang/StringBuilder
dup
ldc "0"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 10
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
goto L18
.limit locals 14
.limit stack 4
.end method
