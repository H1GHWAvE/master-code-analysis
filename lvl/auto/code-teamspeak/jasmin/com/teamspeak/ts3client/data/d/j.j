.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/d/j
.super java/lang/Object

.field 'a' Ljava/io/File;

.field 'b' Ljava/lang/String;

.field 'c' I

.field 'd' Ljava/lang/String;

.field 'e' Lcom/teamspeak/ts3client/Ts3Application;

.field 'f' Landroid/app/ProgressDialog;

.field 'g' Ljava/lang/String;

.field 'h' I

.field private 'i' Lcom/teamspeak/ts3client/data/d/k;

.method private <init>(Ljava/io/File;Ljava/lang/String;ILjava/lang/String;Lcom/teamspeak/ts3client/Ts3Application;Landroid/content/Context;Lcom/teamspeak/ts3client/data/d/k;Ljava/lang/String;I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/d/j/a Ljava/io/File;
aload 0
aload 2
putfield com/teamspeak/ts3client/data/d/j/b Ljava/lang/String;
aload 0
iload 3
putfield com/teamspeak/ts3client/data/d/j/c I
aload 0
aload 4
putfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
aload 0
aload 5
putfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
aload 0
aload 7
putfield com/teamspeak/ts3client/data/d/j/i Lcom/teamspeak/ts3client/data/d/k;
aload 0
aload 8
putfield com/teamspeak/ts3client/data/d/j/g Ljava/lang/String;
aload 0
iload 9
putfield com/teamspeak/ts3client/data/d/j/h I
aload 0
new android/app/ProgressDialog
dup
aload 6
invokespecial android/app/ProgressDialog/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
return
.limit locals 10
.limit stack 4
.end method

.method private a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/j/g Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic a(Lcom/teamspeak/ts3client/data/d/j;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/j/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic b(Lcom/teamspeak/ts3client/data/d/j;)I
aload 0
getfield com/teamspeak/ts3client/data/d/j/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b()V
.annotation invisible Landroid/annotation/SuppressLint;
value [s = "NewApi" 
.end annotation
aload 0
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Updating Content_"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
getstatic android/os/Build$VERSION/SDK_INT I
bipush 11
if_icmplt L0
new com/teamspeak/ts3client/data/d/l
dup
aload 0
invokespecial com/teamspeak/ts3client/data/d/l/<init>(Lcom/teamspeak/ts3client/data/d/j;)V
getstatic android/os/AsyncTask/THREAD_POOL_EXECUTOR Ljava/util/concurrent/Executor;
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/data/d/l/executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
L0:
new com/teamspeak/ts3client/data/d/l
dup
aload 0
invokespecial com/teamspeak/ts3client/data/d/l/<init>(Lcom/teamspeak/ts3client/data/d/j;)V
iconst_0
anewarray java/lang/Void
invokevirtual com/teamspeak/ts3client/data/d/l/execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
pop
return
.limit locals 1
.limit stack 5
.end method

.method private static synthetic c(Lcom/teamspeak/ts3client/data/d/j;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic d(Lcom/teamspeak/ts3client/data/d/j;)I
aload 0
getfield com/teamspeak/ts3client/data/d/j/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic e(Lcom/teamspeak/ts3client/data/d/j;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic f(Lcom/teamspeak/ts3client/data/d/j;)Ljava/io/File;
aload 0
getfield com/teamspeak/ts3client/data/d/j/a Ljava/io/File;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic g(Lcom/teamspeak/ts3client/data/d/j;)Landroid/app/ProgressDialog;
aload 0
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic h(Lcom/teamspeak/ts3client/data/d/j;)Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/d/j/g Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic i(Lcom/teamspeak/ts3client/data/d/j;)Lcom/teamspeak/ts3client/data/d/k;
aload 0
getfield com/teamspeak/ts3client/data/d/j/i Lcom/teamspeak/ts3client/data/d/k;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "ContentDownloader [targetPath="
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/data/d/j/a Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", NewVersion="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/c I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc ", Setting="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/d Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", app="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/e Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", event="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/i Lcom/teamspeak/ts3client/data/d/k;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", dialog="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/f Landroid/app/ProgressDialog;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ", description="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/g Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ", size="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/teamspeak/ts3client/data/d/j/h I
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
ldc "]"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
