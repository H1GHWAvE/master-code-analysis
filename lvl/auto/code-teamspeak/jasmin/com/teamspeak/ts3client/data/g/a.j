.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/g/a
.super java/lang/Object
.implements java/lang/Comparable

.field public 'a' J

.field public 'b' Ljava/lang/String;

.field public 'c' I

.field public 'd' J

.field public 'e' I

.field public 'f' I

.field private 'g' J

.field private 'h' I

.field private 'i' I

.field private 'j' I

.field private 'k' I

.method public <init>(JJLjava/lang/String;IJIIIIII)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lload 1
putfield com/teamspeak/ts3client/data/g/a/g J
aload 0
lload 3
putfield com/teamspeak/ts3client/data/g/a/a J
aload 0
aload 5
putfield com/teamspeak/ts3client/data/g/a/b Ljava/lang/String;
aload 0
iload 6
putfield com/teamspeak/ts3client/data/g/a/c I
aload 0
ldc2_w 4294967295L
lload 7
land
putfield com/teamspeak/ts3client/data/g/a/d J
aload 0
iload 9
putfield com/teamspeak/ts3client/data/g/a/h I
aload 0
iload 10
putfield com/teamspeak/ts3client/data/g/a/i I
aload 0
iload 11
putfield com/teamspeak/ts3client/data/g/a/j I
aload 0
iload 12
putfield com/teamspeak/ts3client/data/g/a/k I
aload 0
iload 13
putfield com/teamspeak/ts3client/data/g/a/e I
aload 0
iload 14
putfield com/teamspeak/ts3client/data/g/a/f I
return
.limit locals 15
.limit stack 5
.end method

.method private a(Lcom/teamspeak/ts3client/data/g/a;)I
aload 0
getfield com/teamspeak/ts3client/data/g/a/i I
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/g/a/i I
istore 2
L1:
aload 1
getfield com/teamspeak/ts3client/data/g/a/i I
ifeq L2
aload 1
getfield com/teamspeak/ts3client/data/g/a/i I
istore 3
L3:
iload 2
iload 3
if_icmpne L4
aload 0
getfield com/teamspeak/ts3client/data/g/a/a J
aload 1
getfield com/teamspeak/ts3client/data/g/a/a J
lcmp
ifge L5
iconst_1
istore 2
L6:
iload 2
ifeq L7
iconst_m1
ireturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/g/a/a J
l2i
istore 2
goto L1
L2:
aload 1
getfield com/teamspeak/ts3client/data/g/a/a J
l2i
istore 3
goto L3
L5:
iconst_0
istore 2
goto L6
L4:
iload 2
iload 3
if_icmpge L8
iconst_1
istore 2
goto L6
L8:
iconst_0
istore 2
goto L6
L7:
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method private b()J
aload 0
getfield com/teamspeak/ts3client/data/g/a/d J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private c()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/j I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/e I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/f I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/k I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/h I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()J
aload 0
getfield com/teamspeak/ts3client/data/g/a/g J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private i()J
aload 0
getfield com/teamspeak/ts3client/data/g/a/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private j()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/i I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private k()I
aload 0
getfield com/teamspeak/ts3client/data/g/a/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/g/a/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic compareTo(Ljava/lang/Object;)I
aload 1
checkcast com/teamspeak/ts3client/data/g/a
astore 1
aload 0
getfield com/teamspeak/ts3client/data/g/a/i I
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/g/a/i I
istore 2
L1:
aload 1
getfield com/teamspeak/ts3client/data/g/a/i I
ifeq L2
aload 1
getfield com/teamspeak/ts3client/data/g/a/i I
istore 3
L3:
iload 2
iload 3
if_icmpne L4
aload 0
getfield com/teamspeak/ts3client/data/g/a/a J
aload 1
getfield com/teamspeak/ts3client/data/g/a/a J
lcmp
ifge L5
iconst_1
istore 2
L6:
iload 2
ifeq L7
iconst_m1
ireturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/g/a/a J
l2i
istore 2
goto L1
L2:
aload 1
getfield com/teamspeak/ts3client/data/g/a/a J
l2i
istore 3
goto L3
L5:
iconst_0
istore 2
goto L6
L4:
iload 2
iload 3
if_icmpge L8
iconst_1
istore 2
goto L6
L8:
iconst_0
istore 2
goto L6
L7:
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/teamspeak/ts3client/data/g/a/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
