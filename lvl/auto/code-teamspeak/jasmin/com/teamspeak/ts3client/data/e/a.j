.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/e/a
.super java/lang/Object

.field private static final 'a' Ljava/lang/String; = "LangSystem"

.field private static 'b' Ljava/util/HashMap;

.field private 'c' Ljava/util/regex/Pattern;

.field private 'd' Ljava/util/regex/Pattern;

.field private 'e' Ljava/util/regex/Pattern;

.method public <init>(Landroid/content/Context;Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
ldc "(<entry>.+?</entry>)"
bipush 32
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putfield com/teamspeak/ts3client/data/e/a/c Ljava/util/regex/Pattern;
aload 0
ldc "<key>(.+)?</key>"
bipush 32
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putfield com/teamspeak/ts3client/data/e/a/d Ljava/util/regex/Pattern;
aload 0
ldc "<value>(.+)?</value>"
bipush 32
invokestatic java/util/regex/Pattern/compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;
putfield com/teamspeak/ts3client/data/e/a/e Ljava/util/regex/Pattern;
getstatic com/teamspeak/ts3client/data/e/c/h Lcom/teamspeak/ts3client/data/e/c;
getfield com/teamspeak/ts3client/data/e/c/i Ljava/lang/String;
astore 6
ldc "defaultLang_tag"
aload 6
invokestatic android/util/Log/d(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic java/util/Locale/getDefault()Ljava/util/Locale;
invokevirtual java/util/Locale/getLanguage()Ljava/lang/String;
astore 7
invokestatic com/teamspeak/ts3client/data/e/c/values()[Lcom/teamspeak/ts3client/data/e/c;
astore 8
aload 8
arraylength
istore 4
iconst_0
istore 3
L0:
aload 6
astore 5
iload 3
iload 4
if_icmpge L1
aload 8
iload 3
aaload
astore 5
aload 7
aload 5
getfield com/teamspeak/ts3client/data/e/c/i Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 5
getfield com/teamspeak/ts3client/data/e/c/i Ljava/lang/String;
astore 5
L1:
aload 0
aload 1
aload 5
invokespecial com/teamspeak/ts3client/data/e/a/a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;
putstatic com/teamspeak/ts3client/data/e/a/b Ljava/util/HashMap;
aload 2
invokevirtual java/lang/String/isEmpty()Z
ifeq L3
L4:
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
pop
aload 0
aload 1
aload 5
invokespecial com/teamspeak/ts3client/data/e/a/a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;
astore 1
getstatic com/teamspeak/ts3client/data/e/a/b Ljava/util/HashMap;
aload 1
invokevirtual java/util/HashMap/putAll(Ljava/util/Map;)V
return
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L3:
aload 2
astore 5
goto L4
.limit locals 9
.limit stack 3
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;I)Landroid/widget/SpinnerAdapter;
iconst_0
istore 4
iconst_0
istore 3
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 5
aload 5
ldc ","
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
aload 5
ldc "(?<!\\\\),"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 0
iload 3
istore 2
L1:
iload 2
aload 0
arraylength
if_icmpge L2
aload 0
iload 2
aload 0
iload 2
aaload
ldc "\\,"
ldc ","
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
new android/widget/ArrayAdapter
dup
aload 1
ldc_w 17367049
aload 0
invokespecial android/widget/ArrayAdapter/<init>(Landroid/content/Context;I[Ljava/lang/Object;)V
areturn
L0:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Broken LangEntry:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 2
anewarray java/lang/String
astore 0
iload 4
istore 3
L3:
iload 3
iload 2
if_icmpge L4
aload 0
iload 3
aload 5
aastore
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
goto L2
.limit locals 6
.limit stack 5
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 5
.end method

.method public static transient a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
.catch java/lang/Exception from L0 to L1 using L2
getstatic com/teamspeak/ts3client/data/e/a/b Ljava/util/HashMap;
aload 0
invokevirtual java/util/HashMap/containsKey(Ljava/lang/Object;)Z
ifeq L3
L0:
getstatic com/teamspeak/ts3client/data/e/a/b Ljava/util/HashMap;
aload 0
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
aload 1
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 1
L1:
aload 1
areturn
L2:
astore 1
ldc "LangSystem"
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ": "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/Exception/getMessage()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/util/Log/e(Ljava/lang/String;Ljava/lang/String;)I
pop
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
getstatic com/teamspeak/ts3client/data/e/a/b Ljava/util/HashMap;
aload 0
invokevirtual java/util/HashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "(PARSING ERROR)"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L3:
new java/lang/StringBuilder
dup
ldc "<"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ">"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 3
aload 1
aload 2
invokestatic com/teamspeak/ts3client/data/e/a/b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
astore 1
aload 0
getfield com/teamspeak/ts3client/data/e/a/c Ljava/util/regex/Pattern;
aload 1
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 1
L0:
aload 1
invokevirtual java/util/regex/Matcher/find()Z
ifeq L1
aload 1
invokevirtual java/util/regex/Matcher/group()Ljava/lang/String;
astore 2
aload 0
getfield com/teamspeak/ts3client/data/e/a/d Ljava/util/regex/Pattern;
aload 2
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 4
aload 4
invokevirtual java/util/regex/Matcher/find()Z
ifeq L2
aload 4
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 4
aload 4
ifnonnull L3
L2:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Broken LangEntry:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L0
L3:
aload 0
getfield com/teamspeak/ts3client/data/e/a/e Ljava/util/regex/Pattern;
aload 2
invokevirtual java/util/regex/Pattern/matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;
astore 5
aload 5
invokevirtual java/util/regex/Matcher/find()Z
ifeq L4
aload 5
iconst_1
invokevirtual java/util/regex/Matcher/group(I)Ljava/lang/String;
astore 5
aload 5
ifnonnull L5
L4:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Broken LangEntry:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L0
L5:
aload 3
aload 4
aload 5
ldc "\\n"
ldc "\n"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
ldc "\\r"
ldc "\r"
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
invokevirtual java/util/HashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
goto L0
L1:
aload 3
areturn
.limit locals 6
.limit stack 5
.end method

.method private static a()Ljava/util/List;
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/content/lang/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 0
aload 0
invokevirtual java/io/File/exists()Z
ifeq L0
aload 0
new com/teamspeak/ts3client/data/e/b
dup
aload 0
invokespecial com/teamspeak/ts3client/data/e/b/<init>(Ljava/io/File;)V
invokevirtual java/io/File/list(Ljava/io/FilenameFilter;)[Ljava/lang/String;
astore 0
L1:
aload 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
areturn
L0:
iconst_0
anewarray java/lang/String
astore 0
goto L1
.limit locals 1
.limit stack 4
.end method

.method private static a(Ljava/lang/String;Landroid/support/v7/app/i;I)V
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
iload 2
invokevirtual android/support/v7/app/i/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 5
.end method

.method public static a(Ljava/lang/String;Landroid/view/View;I)V
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
iload 2
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 5
.end method

.method public static a(Ljava/lang/String;Landroid/view/ViewGroup;I)V
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 1
iload 2
invokevirtual android/view/ViewGroup/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
aload 0
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 3
.limit stack 5
.end method

.method public static a(Ljava/lang/String;Landroid/widget/TextView;)V
aload 1
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/widget/TextView/setText(Ljava/lang/CharSequence;)V
return
.limit locals 2
.limit stack 6
.end method

.method private static a(Ljava/lang/String;I)[Ljava/lang/String;
iconst_0
istore 3
iconst_0
istore 2
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 4
aload 4
ldc ","
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
aload 4
ldc "(?<!\\\\),"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 0
iload 2
istore 1
L1:
iload 1
aload 0
arraylength
if_icmpge L2
aload 0
iload 1
aload 0
iload 1
aaload
ldc "\\,"
ldc ","
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 0
areturn
L0:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Broken LangEntry:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 1
anewarray java/lang/String
astore 0
iload 3
istore 2
L3:
iload 2
iload 1
if_icmpge L4
aload 0
iload 2
aload 4
aastore
iload 2
iconst_1
iadd
istore 2
goto L3
L4:
aload 0
areturn
.limit locals 5
.limit stack 5
.end method

.method public static b(Ljava/lang/String;Landroid/content/Context;I)Lcom/teamspeak/ts3client/customs/a;
iconst_0
istore 4
iconst_0
istore 3
aload 0
iconst_1
anewarray java/lang/Object
dup
iconst_0
aconst_null
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
astore 5
aload 5
ldc ","
invokevirtual java/lang/String/contains(Ljava/lang/CharSequence;)Z
ifeq L0
aload 5
ldc "(?<!\\\\),"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 0
iload 3
istore 2
L1:
iload 2
aload 0
arraylength
if_icmpge L2
aload 0
iload 2
aload 0
iload 2
aaload
ldc "\\,"
ldc ","
invokevirtual java/lang/String/replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
aastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
new com/teamspeak/ts3client/customs/a
dup
aload 1
aload 0
invokespecial com/teamspeak/ts3client/customs/a/<init>(Landroid/content/Context;[Ljava/lang/String;)V
areturn
L0:
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "Broken LangEntry:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
iload 2
anewarray java/lang/String
astore 0
iload 4
istore 3
L3:
iload 3
iload 2
if_icmpge L4
aload 0
iload 3
aload 5
aastore
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
goto L2
.limit locals 6
.limit stack 5
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L2
.catch java/io/IOException from L5 to L6 using L2
.catch java/io/IOException from L7 to L8 using L2
.catch java/io/IOException from L9 to L10 using L2
.catch java/io/IOException from L11 to L12 using L2
.catch java/io/IOException from L13 to L14 using L2
.catch java/io/IOException from L15 to L16 using L2
.catch java/io/IOException from L17 to L18 using L2
.catch java/io/IOException from L19 to L20 using L2
.catch java/io/IOException from L21 to L22 using L23
.catch java/io/IOException from L24 to L25 using L23
.catch java/io/IOException from L26 to L27 using L23
iconst_0
istore 4
aconst_null
astore 9
aconst_null
astore 8
aload 9
astore 7
L0:
invokestatic com/teamspeak/ts3client/data/e/c/values()[Lcom/teamspeak/ts3client/data/e/c;
astore 10
L1:
aload 9
astore 7
L3:
aload 10
arraylength
istore 5
L4:
iconst_0
istore 2
L28:
aload 8
astore 6
iload 4
istore 3
iload 2
iload 5
if_icmpge L29
aload 10
iload 2
aaload
astore 6
aload 9
astore 7
L5:
aload 1
aload 6
getfield com/teamspeak/ts3client/data/e/c/i Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L30
L6:
aload 9
astore 7
L7:
aload 0
invokevirtual android/content/Context/getAssets()Landroid/content/res/AssetManager;
new java/lang/StringBuilder
dup
ldc "lang/"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 6
getfield com/teamspeak/ts3client/data/e/c/j Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual android/content/res/AssetManager/open(Ljava/lang/String;)Ljava/io/InputStream;
astore 6
L8:
iconst_1
istore 3
L29:
aload 6
astore 7
iload 3
ifne L31
aload 6
astore 7
L9:
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
invokestatic android/os/Environment/getExternalStorageDirectory()Ljava/io/File;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "/TS3/content/lang/"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 8
L10:
aload 6
astore 7
L11:
new java/io/File
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 8
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/File/<init>(Ljava/lang/String;)V
astore 8
L12:
aload 6
astore 7
L13:
aload 8
invokevirtual java/io/File/exists()Z
ifeq L32
L14:
aload 6
astore 7
L15:
aload 8
invokevirtual java/io/File/isFile()Z
ifeq L32
L16:
aload 6
astore 7
L17:
new java/io/FileInputStream
dup
aload 8
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 0
L18:
aload 0
astore 7
L31:
aload 7
ifnonnull L33
ldc ""
areturn
L30:
iload 2
iconst_1
iadd
istore 2
goto L28
L32:
aload 6
astore 7
L19:
aload 0
invokevirtual android/content/Context/getAssets()Landroid/content/res/AssetManager;
ldc "lang_eng.xml"
invokevirtual android/content/res/AssetManager/open(Ljava/lang/String;)Ljava/io/InputStream;
astore 0
L20:
aload 0
astore 7
goto L31
L2:
astore 0
invokestatic com/teamspeak/ts3client/Ts3Application/a()Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
new java/lang/StringBuilder
dup
ldc "No Lang File for: "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
goto L31
L33:
new java/lang/StringBuffer
dup
invokespecial java/lang/StringBuffer/<init>()V
astore 1
new java/io/InputStreamReader
dup
aload 7
invokespecial java/io/InputStreamReader/<init>(Ljava/io/InputStream;)V
astore 6
new java/io/BufferedReader
dup
aload 6
invokespecial java/io/BufferedReader/<init>(Ljava/io/Reader;)V
astore 7
L21:
aload 7
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 0
L22:
aload 0
ifnull L26
L24:
aload 1
aload 0
invokevirtual java/lang/StringBuffer/append(Ljava/lang/String;)Ljava/lang/StringBuffer;
pop
aload 7
invokevirtual java/io/BufferedReader/readLine()Ljava/lang/String;
astore 0
L25:
goto L22
L26:
aload 7
invokevirtual java/io/BufferedReader/close()V
aload 6
invokevirtual java/io/InputStreamReader/close()V
L27:
aload 1
invokevirtual java/lang/StringBuffer/toString()Ljava/lang/String;
areturn
L23:
astore 0
aload 0
invokevirtual java/io/IOException/printStackTrace()V
goto L27
.limit locals 11
.limit stack 5
.end method
