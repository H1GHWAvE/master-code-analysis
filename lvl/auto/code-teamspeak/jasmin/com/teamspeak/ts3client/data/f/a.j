.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/f/a
.super java/lang/Object
.implements com/teamspeak/ts3client/data/w

.field public 'a' Ljava/util/SortedMap;

.field public 'b' Lcom/teamspeak/ts3client/Ts3Application;

.field public 'c' Z

.method public <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/f/a/c Z
aload 0
aload 1
putfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
aload 1
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/c Lcom/teamspeak/ts3client/jni/l;
aload 0
invokevirtual com/teamspeak/ts3client/jni/l/a(Lcom/teamspeak/ts3client/data/w;)V
aload 0
new java/util/TreeMap
dup
invokespecial java/util/TreeMap/<init>()V
putfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
return
.limit locals 2
.limit stack 3
.end method

.method private a()Z
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
getfield com/teamspeak/ts3client/data/d/v/c Z
ifne L0
iconst_0
ireturn
L0:
aload 0
getfield com/teamspeak/ts3client/data/f/a/c Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private b(Ljava/lang/String;)I
aload 0
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/v/a(Ljava/lang/String;)I
invokevirtual com/teamspeak/ts3client/data/f/a/b(I)I
ireturn
.limit locals 2
.limit stack 3
.end method

.method private c(I)Z
aload 0
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/k;)V
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/f/a/c Z
ifeq L1
aload 0
iconst_0
putfield com/teamspeak/ts3client/data/f/a/c Z
L1:
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions
astore 2
aload 0
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/a I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aload 2
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissions/b I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L0:
aload 1
instanceof com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissionsFinished
ifeq L2
aload 1
checkcast com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissionsFinished
getfield com/teamspeak/ts3client/jni/events/rare/ClientNeededPermissionsFinished/a J
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/e J
lcmp
ifne L2
aload 0
iconst_1
putfield com/teamspeak/ts3client/data/f/a/c Z
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
invokevirtual com/teamspeak/ts3client/t/y()V
L2:
return
.limit locals 3
.limit stack 4
.end method

.method public final a(I)Z
aload 0
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
iconst_1
if_icmpne L1
iconst_1
ireturn
L1:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Lcom/teamspeak/ts3client/jni/g;)Z
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/v/a(Lcom/teamspeak/ts3client/jni/g;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final a(Ljava/lang/String;)Z
.catch java/lang/Exception from L0 to L1 using L2
L0:
aload 0
aload 0
getfield com/teamspeak/ts3client/data/f/a/b Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/u Lcom/teamspeak/ts3client/data/d/v;
aload 1
invokevirtual com/teamspeak/ts3client/data/d/v/a(Ljava/lang/String;)I
invokevirtual com/teamspeak/ts3client/data/f/a/a(I)Z
istore 2
L1:
iload 2
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final b(I)I
aload 0
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/teamspeak/ts3client/data/f/a/a Ljava/util/SortedMap;
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokeinterface java/util/SortedMap/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/Integer
invokevirtual java/lang/Integer/intValue()I
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method
