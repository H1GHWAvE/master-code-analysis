.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/data/b
.super java/lang/Object

.field public 'a' Ljava/util/concurrent/ConcurrentHashMap;

.field public 'b' Lcom/teamspeak/ts3client/data/a;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/teamspeak/ts3client/data/b/b Lcom/teamspeak/ts3client/data/a;
aload 0
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
putfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
return
.limit locals 1
.limit stack 3
.end method

.method public final a(Ljava/lang/Long;)Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/a
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Long;I)Ljava/lang/Long;
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a
astore 4
aload 4
getfield com/teamspeak/ts3client/data/a/g J
aload 1
invokevirtual java/lang/Long/longValue()J
lcmp
ifne L0
aload 4
getfield com/teamspeak/ts3client/data/a/f I
iload 2
if_icmpne L0
aload 4
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
L1:
aconst_null
areturn
.limit locals 5
.limit stack 4
.end method

.method public final a()Ljava/util/concurrent/ConcurrentHashMap;
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/teamspeak/ts3client/data/a;)V
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 3
.end method

.method public final b()Lcom/teamspeak/ts3client/data/a;
aload 0
getfield com/teamspeak/ts3client/data/b/b Lcom/teamspeak/ts3client/data/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b(Ljava/lang/Long;)Ljava/lang/Long;
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/teamspeak/ts3client/data/a
astore 3
aload 3
getfield com/teamspeak/ts3client/data/a/g J
aload 1
invokevirtual java/lang/Long/longValue()J
lcmp
ifne L0
aload 3
getfield com/teamspeak/ts3client/data/a/b J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
areturn
L1:
aconst_null
areturn
.limit locals 4
.limit stack 4
.end method

.method public final b(Lcom/teamspeak/ts3client/data/a;)V
aload 0
aload 1
putfield com/teamspeak/ts3client/data/b/b Lcom/teamspeak/ts3client/data/a;
return
.limit locals 2
.limit stack 2
.end method

.method public final c(Ljava/lang/Long;)V
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/remove(Ljava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 2
.limit stack 2
.end method

.method public final d(Ljava/lang/Long;)Z
aload 0
getfield com/teamspeak/ts3client/data/b/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method
