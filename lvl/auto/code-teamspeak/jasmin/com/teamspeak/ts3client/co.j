.bytecode 50.0
.class final synchronized com/teamspeak/ts3client/co
.super java/lang/Object
.implements android/content/SharedPreferences$OnSharedPreferenceChangeListener

.field private 'a' Lcom/teamspeak/ts3client/Ts3Application;

.method public <init>(Lcom/teamspeak/ts3client/Ts3Application;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
return
.limit locals 2
.limit stack 2
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
aload 2
ldc "screen_rotation"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
aload 1
ldc "screen_rotation"
iconst_0
invokeinterface android/content/SharedPreferences/getInt(Ljava/lang/String;I)I 2
tableswitch 0
L1
L2
L3
default : L4
L4:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
iconst_m1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
L0:
aload 2
ldc "android_overlay_setting"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 1
ldc "android_overlay_setting"
iconst_0
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
ifeq L6
getstatic android/os/Build$VERSION/SDK_INT I
bipush 23
if_icmpge L7
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
L5:
return
L1:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
iconst_m1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
goto L0
L2:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
iconst_1
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
goto L0
L3:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
iconst_0
invokevirtual com/teamspeak/ts3client/StartGUIFragment/setRequestedOrientation(I)V
goto L0
L7:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
invokestatic android/provider/Settings/canDrawOverlays(Landroid/content/Context;)Z
ifne L8
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
new android/content/Intent
dup
ldc "android.settings.action.MANAGE_OVERLAY_PERMISSION"
new java/lang/StringBuilder
dup
ldc "package:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getPackageName()Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic android/net/Uri/parse(Ljava/lang/String;)Landroid/net/Uri;
invokespecial android/content/Intent/<init>(Ljava/lang/String;Landroid/net/Uri;)V
sipush 7474
invokevirtual com/teamspeak/ts3client/StartGUIFragment/startActivityForResult(Landroid/content/Intent;I)V
return
L8:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay"
iconst_1
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
L6:
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
aload 0
getfield com/teamspeak/ts3client/co/a Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
invokeinterface android/content/SharedPreferences/edit()Landroid/content/SharedPreferences$Editor; 0
ldc "android_overlay_setting"
iconst_0
invokeinterface android/content/SharedPreferences$Editor/putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor; 2
invokeinterface android/content/SharedPreferences$Editor/commit()Z 0
pop
return
.limit locals 3
.limit stack 7
.end method
