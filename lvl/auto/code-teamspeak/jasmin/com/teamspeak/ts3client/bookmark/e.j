.bytecode 50.0
.class public final synchronized com/teamspeak/ts3client/bookmark/e
.super android/support/v4/app/Fragment

.field public 'a' Z

.field public 'b' Lcom/teamspeak/ts3client/data/b/a;

.field private 'c' Lcom/teamspeak/ts3client/Ts3Application;

.field private 'd' Landroid/widget/ListView;

.field private 'e' Lcom/teamspeak/ts3client/bookmark/c;

.method public <init>()V
aload 0
invokespecial android/support/v4/app/Fragment/<init>()V
aload 0
iconst_0
putfield com/teamspeak/ts3client/bookmark/e/a Z
return
.limit locals 1
.limit stack 2
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/bookmark/c;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/e Lcom/teamspeak/ts3client/bookmark/c;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
getfield com/teamspeak/ts3client/bookmark/e/e Lcom/teamspeak/ts3client/bookmark/c;
iload 1
invokevirtual com/teamspeak/ts3client/bookmark/c/getItem(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/ab
astore 2
new com/teamspeak/ts3client/bookmark/a
dup
aload 0
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
aload 2
invokespecial com/teamspeak/ts3client/bookmark/a/<init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ldc "AddBookmark"
invokevirtual com/teamspeak/ts3client/bookmark/a/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(ILcom/teamspeak/ts3client/data/ab;)V
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 3
aload 3
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 3
ldc "bookmark.listbookmark.delete"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
ldc "bookmark.listbookmark.delete.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 3
iconst_m1
ldc "button.delete"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bookmark/f
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/bookmark/f/<init>(Lcom/teamspeak/ts3client/bookmark/e;I)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bookmark/g
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/bookmark/g/<init>(Lcom/teamspeak/ts3client/bookmark/e;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 3
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 4
.limit stack 7
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/e;I)V
aload 0
getfield com/teamspeak/ts3client/bookmark/e/e Lcom/teamspeak/ts3client/bookmark/c;
iload 1
invokevirtual com/teamspeak/ts3client/bookmark/c/getItem(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/ab
astore 2
new com/teamspeak/ts3client/bookmark/a
dup
aload 0
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
aload 2
invokespecial com/teamspeak/ts3client/bookmark/a/<init>(Lcom/teamspeak/ts3client/data/b/a;Lcom/teamspeak/ts3client/data/ab;)V
aload 0
getfield android/support/v4/app/Fragment/M Landroid/support/v4/app/bl;
ldc "AddBookmark"
invokevirtual com/teamspeak/ts3client/bookmark/a/a(Landroid/support/v4/app/bi;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 3
aload 3
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 3
ldc "bookmark.listbookmark.delete"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 3
ldc "bookmark.listbookmark.delete.text"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 2
getfield com/teamspeak/ts3client/data/ab/a Ljava/lang/String;
aastore
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 3
iconst_m1
ldc "button.delete"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bookmark/f
dup
aload 0
iload 1
invokespecial com/teamspeak/ts3client/bookmark/f/<init>(Lcom/teamspeak/ts3client/bookmark/e;I)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
bipush -2
ldc "button.cancel"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bookmark/g
dup
aload 0
aload 3
invokespecial com/teamspeak/ts3client/bookmark/g/<init>(Lcom/teamspeak/ts3client/bookmark/e;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 3
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 3
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 4
.limit stack 7
.end method

.method private b()Lcom/teamspeak/ts3client/data/b/a;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/data/b/a;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(ILcom/teamspeak/ts3client/data/ab;)V
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 3
ldc "button.edit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 4
ldc "button.delete"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 5
new com/teamspeak/ts3client/bookmark/h
dup
aload 0
iload 1
aload 2
invokespecial com/teamspeak/ts3client/bookmark/h/<init>(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V
astore 2
aload 3
iconst_2
anewarray java/lang/CharSequence
dup
iconst_0
aload 4
aastore
dup
iconst_1
aload 5
aastore
aload 2
invokevirtual android/app/AlertDialog$Builder/setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 3
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 2
aload 2
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 2
ldc "bookmark.listbookmark.options"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 2
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 6
.limit stack 5
.end method

.method static synthetic b(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
astore 3
ldc "button.edit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 4
ldc "button.delete"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
astore 5
new com/teamspeak/ts3client/bookmark/h
dup
aload 0
iload 1
aload 2
invokespecial com/teamspeak/ts3client/bookmark/h/<init>(Lcom/teamspeak/ts3client/bookmark/e;ILcom/teamspeak/ts3client/data/ab;)V
astore 0
aload 3
iconst_2
anewarray java/lang/CharSequence
dup
iconst_0
aload 4
aastore
dup
iconst_1
aload 5
aastore
aload 0
invokevirtual android/app/AlertDialog$Builder/setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
pop
aload 3
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 0
aload 0
invokestatic com/teamspeak/ts3client/data/d/q/a(Landroid/app/Dialog;)V
aload 0
ldc "bookmark.listbookmark.options"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
aload 0
invokevirtual android/app/AlertDialog/show()V
return
.limit locals 6
.limit stack 5
.end method

.method static synthetic c(Lcom/teamspeak/ts3client/bookmark/e;)Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
aload 0
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
checkcast com/teamspeak/ts3client/Ts3Application
putfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
aload 0
putfield com/teamspeak/ts3client/Ts3Application/c Landroid/support/v4/app/Fragment;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/o Landroid/support/v7/app/a;
ldc "bookmark.text"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/support/v7/app/a/b(Ljava/lang/CharSequence;)V
aload 1
ldc_w 2130903070
aload 2
iconst_0
invokevirtual android/view/LayoutInflater/inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
astore 1
aload 1
ldc_w 2131493021
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast com/teamspeak/ts3client/customs/FloatingButton
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/j()Landroid/content/res/Resources;
ldc_w 2130837664
invokevirtual android/content/res/Resources/getDrawable(I)Landroid/graphics/drawable/Drawable;
invokevirtual com/teamspeak/ts3client/customs/FloatingButton/setDrawable(Landroid/graphics/drawable/Drawable;)V
aload 0
aload 1
ldc_w 2131493022
invokevirtual android/view/View/findViewById(I)Landroid/view/View;
checkcast android/widget/ListView
putfield com/teamspeak/ts3client/bookmark/e/d Landroid/widget/ListView;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/d Landroid/widget/ListView;
ldc "Bookmark List"
invokevirtual android/widget/ListView/setContentDescription(Ljava/lang/CharSequence;)V
aload 0
getfield com/teamspeak/ts3client/bookmark/e/d Landroid/widget/ListView;
new com/teamspeak/ts3client/bookmark/i
dup
aload 0
invokespecial com/teamspeak/ts3client/bookmark/i/<init>(Lcom/teamspeak/ts3client/bookmark/e;)V
invokevirtual android/widget/ListView/setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
aload 0
getfield com/teamspeak/ts3client/bookmark/e/d Landroid/widget/ListView;
new com/teamspeak/ts3client/bookmark/l
dup
aload 0
invokespecial com/teamspeak/ts3client/bookmark/l/<init>(Lcom/teamspeak/ts3client/bookmark/e;)V
invokevirtual android/widget/ListView/setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a()V
aload 0
new com/teamspeak/ts3client/bookmark/c
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokevirtual android/support/v4/app/bb/getApplicationContext()Landroid/content/Context;
invokespecial com/teamspeak/ts3client/bookmark/c/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/bookmark/e/e Lcom/teamspeak/ts3client/bookmark/c;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
invokevirtual com/teamspeak/ts3client/data/b/a/a()Ljava/util/ArrayList;
astore 2
iconst_0
istore 1
L0:
iload 1
aload 2
invokevirtual java/util/ArrayList/size()I
if_icmpge L1
aload 0
getfield com/teamspeak/ts3client/bookmark/e/e Lcom/teamspeak/ts3client/bookmark/c;
astore 3
aload 2
iload 1
invokevirtual java/util/ArrayList/get(I)Ljava/lang/Object;
checkcast com/teamspeak/ts3client/data/ab
astore 4
aload 3
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/contains(Ljava/lang/Object;)Z
ifne L2
aload 3
getfield com/teamspeak/ts3client/bookmark/c/a Ljava/util/ArrayList;
aload 4
invokevirtual java/util/ArrayList/add(Ljava/lang/Object;)Z
pop
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
aload 0
getfield com/teamspeak/ts3client/bookmark/e/d Landroid/widget/ListView;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/e Lcom/teamspeak/ts3client/bookmark/c;
invokevirtual android/widget/ListView/setAdapter(Landroid/widget/ListAdapter;)V
return
.limit locals 5
.limit stack 4
.end method

.method public final a(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/a(Landroid/os/Bundle;)V
aload 0
new com/teamspeak/ts3client/data/b/a
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokespecial com/teamspeak/ts3client/data/b/a/<init>(Landroid/content/Context;)V
putfield com/teamspeak/ts3client/bookmark/e/b Lcom/teamspeak/ts3client/data/b/a;
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Lcom/teamspeak/ts3client/data/ab;)V
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L5
.catch java/lang/Exception from L4 to L6 using L5
.catch java/lang/Exception from L7 to L8 using L9
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/e Landroid/content/SharedPreferences;
ldc "use_proximity"
iconst_1
invokeinterface android/content/SharedPreferences/getBoolean(Ljava/lang/String;Z)Z 2
istore 3
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
astore 4
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc "power"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/os/PowerManager
iconst_1
ldc "Ts3WakeLOCK"
invokevirtual android/os/PowerManager/newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
astore 5
aload 5
invokevirtual android/os/PowerManager$WakeLock/acquire()V
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 5
putfield com/teamspeak/ts3client/Ts3Application/b Landroid/os/PowerManager$WakeLock;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
astore 4
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc "wifi"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/net/wifi/WifiManager
iconst_3
ldc "Ts3WifiLOCK"
invokevirtual android/net/wifi/WifiManager/createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;
astore 5
aload 5
invokevirtual android/net/wifi/WifiManager$WifiLock/acquire()V
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 5
putfield com/teamspeak/ts3client/Ts3Application/n Landroid/net/wifi/WifiManager$WifiLock;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
astore 4
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/getApplicationContext()Landroid/content/Context;
ldc "power"
invokevirtual android/content/Context/getSystemService(Ljava/lang/String;)Ljava/lang/Object;
checkcast android/os/PowerManager
bipush 32
ldc "Ts3WakeLOCKSensor"
invokevirtual android/os/PowerManager/newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
astore 5
aload 5
iconst_0
invokevirtual android/os/PowerManager$WakeLock/setReferenceCounted(Z)V
aload 4
getfield com/teamspeak/ts3client/StartGUIFragment/n Lcom/teamspeak/ts3client/Ts3Application;
aload 5
putfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
invokevirtual com/teamspeak/ts3client/Ts3Application/g()Lcom/teamspeak/ts3client/a/p;
iconst_0
putfield com/teamspeak/ts3client/a/p/a Z
iload 3
ifeq L0
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/m Landroid/os/PowerManager$WakeLock;
ifnull L0
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/f Lcom/teamspeak/ts3client/StartGUIFragment;
invokevirtual com/teamspeak/ts3client/StartGUIFragment/k()V
L0:
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
aload 1
getfield com/teamspeak/ts3client/data/ab/d I
i2l
invokevirtual com/teamspeak/ts3client/data/b/f/b(J)Lcom/teamspeak/ts3client/e/a;
getfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
astore 5
L1:
aload 5
ifnull L4
aload 5
astore 4
L3:
aload 5
ldc ""
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
L4:
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/d()Lcom/teamspeak/ts3client/e/a;
getfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
astore 4
L6:
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
new com/teamspeak/ts3client/data/e
dup
aload 4
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
aload 1
invokespecial com/teamspeak/ts3client/data/e/<init>(Ljava/lang/String;Landroid/content/Context;Lcom/teamspeak/ts3client/data/ab;)V
putfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
astore 4
aload 4
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/d Ljava/util/logging/Logger;
getstatic java/util/logging/Level/INFO Ljava/util/logging/Level;
ldc "Loading lib"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
invokestatic com/teamspeak/ts3client/jni/Ts3Jni/a()I
istore 2
iload 2
ifle L10
L11:
iload 2
ifle L12
iload 2
iconst_5
if_icmpne L13
new android/app/AlertDialog$Builder
dup
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/i()Landroid/support/v4/app/bb;
invokespecial android/app/AlertDialog$Builder/<init>(Landroid/content/Context;)V
invokevirtual android/app/AlertDialog$Builder/create()Landroid/app/AlertDialog;
astore 1
aload 1
ldc "critical.payment"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual android/app/AlertDialog/setTitle(Ljava/lang/CharSequence;)V
new android/text/SpannableString
dup
ldc "This beta version is expired, please visit our forum or \nhttp://play.google.com/store/apps/details?id=com.teamspeak.ts3client"
invokespecial android/text/SpannableString/<init>(Ljava/lang/CharSequence;)V
astore 4
aload 4
bipush 15
invokestatic android/text/util/Linkify/addLinks(Landroid/text/Spannable;I)Z
pop
aload 1
aload 4
invokevirtual android/app/AlertDialog/setMessage(Ljava/lang/CharSequence;)V
aload 1
bipush -2
ldc "button.exit"
invokestatic com/teamspeak/ts3client/data/e/a/a(Ljava/lang/String;)Ljava/lang/String;
new com/teamspeak/ts3client/bookmark/m
dup
aload 0
aload 1
invokespecial com/teamspeak/ts3client/bookmark/m/<init>(Lcom/teamspeak/ts3client/bookmark/e;Landroid/app/AlertDialog;)V
invokevirtual android/app/AlertDialog/setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V
aload 1
iconst_0
invokevirtual android/app/AlertDialog/setCancelable(Z)V
aload 1
invokevirtual android/app/AlertDialog/show()V
aload 1
ldc_w 16908299
invokevirtual android/app/AlertDialog/findViewById(I)Landroid/view/View;
checkcast android/widget/TextView
invokestatic android/text/method/LinkMovementMethod/getInstance()Landroid/text/method/MovementMethod;
invokevirtual android/widget/TextView/setMovementMethod(Landroid/text/method/MovementMethod;)V
aload 0
iconst_0
putfield com/teamspeak/ts3client/bookmark/e/a Z
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
aconst_null
putfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
L13:
return
L2:
astore 4
aconst_null
astore 5
goto L1
L5:
astore 4
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/b()V
L7:
invokestatic com/teamspeak/ts3client/data/b/f/a()Lcom/teamspeak/ts3client/data/b/f;
invokevirtual com/teamspeak/ts3client/data/b/f/d()Lcom/teamspeak/ts3client/e/a;
getfield com/teamspeak/ts3client/e/a/c Ljava/lang/String;
astore 4
L8:
goto L6
L9:
astore 4
ldc ""
astore 4
goto L6
L10:
aload 4
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
aload 4
getfield com/teamspeak/ts3client/data/e/y I
aload 4
getfield com/teamspeak/ts3client/data/e/x I
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_prepareAudioDevice(II)I
pop
aload 4
new com/teamspeak/ts3client/data/f/a
dup
aload 4
getfield com/teamspeak/ts3client/data/e/m Lcom/teamspeak/ts3client/Ts3Application;
invokespecial com/teamspeak/ts3client/data/f/a/<init>(Lcom/teamspeak/ts3client/Ts3Application;)V
putfield com/teamspeak/ts3client/data/e/s Lcom/teamspeak/ts3client/data/f/a;
aload 4
aload 4
getfield com/teamspeak/ts3client/data/e/a Lcom/teamspeak/ts3client/jni/Ts3Jni;
invokevirtual com/teamspeak/ts3client/jni/Ts3Jni/ts3client_spawnNewServerConnectionHandler()J
putfield com/teamspeak/ts3client/data/e/e J
goto L11
L12:
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
invokevirtual com/teamspeak/ts3client/data/e/n()V
new com/teamspeak/ts3client/t
dup
invokespecial com/teamspeak/ts3client/t/<init>()V
astore 4
aload 4
aload 0
getfield android/support/v4/app/Fragment/B Landroid/os/Bundle;
invokevirtual com/teamspeak/ts3client/t/e(Landroid/os/Bundle;)V
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
getfield com/teamspeak/ts3client/data/e/l Lcom/teamspeak/ts3client/ConnectionBackground;
ifnonnull L14
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
new android/content/Intent
dup
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
ldc com/teamspeak/ts3client/ConnectionBackground
invokespecial android/content/Intent/<init>(Landroid/content/Context;Ljava/lang/Class;)V
putfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/k Landroid/content/Intent;
invokevirtual com/teamspeak/ts3client/Ts3Application/startService(Landroid/content/Intent;)Landroid/content/ComponentName;
pop
L14:
aload 0
getfield com/teamspeak/ts3client/bookmark/e/c Lcom/teamspeak/ts3client/Ts3Application;
getfield com/teamspeak/ts3client/Ts3Application/a Lcom/teamspeak/ts3client/data/e;
aload 4
putfield com/teamspeak/ts3client/data/e/k Lcom/teamspeak/ts3client/t;
aload 1
getfield com/teamspeak/ts3client/data/ab/c Ljava/lang/String;
ldc "(.*://)+"
ldc ""
invokevirtual java/lang/String/replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
astore 4
new android/os/Handler
dup
invokespecial android/os/Handler/<init>()V
astore 5
aload 5
new com/teamspeak/ts3client/bookmark/n
dup
aload 0
aload 1
aload 4
invokespecial com/teamspeak/ts3client/bookmark/n/<init>(Lcom/teamspeak/ts3client/bookmark/e;Lcom/teamspeak/ts3client/data/ab;Ljava/lang/String;)V
ldc2_w 500L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
aload 5
new com/teamspeak/ts3client/bookmark/o
dup
aload 0
invokespecial com/teamspeak/ts3client/bookmark/o/<init>(Lcom/teamspeak/ts3client/bookmark/e;)V
ldc2_w 2000L
invokevirtual android/os/Handler/postDelayed(Ljava/lang/Runnable;J)Z
pop
return
.limit locals 6
.limit stack 7
.end method

.method public final c(Landroid/os/Bundle;)V
aload 0
aload 1
invokespecial android/support/v4/app/Fragment/c(Landroid/os/Bundle;)V
aload 0
invokevirtual com/teamspeak/ts3client/bookmark/e/a()V
return
.limit locals 2
.limit stack 2
.end method
