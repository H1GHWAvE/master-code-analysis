.bytecode 50.0
.class public final synchronized com/a/c/b/f
.super java/lang/Object

.field private final 'a' Ljava/util/Map;

.method public <init>(Ljava/util/Map;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/c/b/f/a Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Class;)Lcom/a/c/b/ao;
.catch java/lang/NoSuchMethodException from L0 to L1 using L2
.catch java/lang/NoSuchMethodException from L1 to L3 using L2
L0:
aload 1
iconst_0
anewarray java/lang/Class
invokevirtual java/lang/Class/getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
astore 1
aload 1
invokevirtual java/lang/reflect/Constructor/isAccessible()Z
ifne L1
aload 1
iconst_1
invokevirtual java/lang/reflect/Constructor/setAccessible(Z)V
L1:
new com/a/c/b/l
dup
aload 0
aload 1
invokespecial com/a/c/b/l/<init>(Lcom/a/c/b/f;Ljava/lang/reflect/Constructor;)V
astore 1
L3:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/a/c/b/ao;
ldc java/util/Collection
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L0
ldc java/util/SortedSet
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L1
new com/a/c/b/m
dup
aload 0
invokespecial com/a/c/b/m/<init>(Lcom/a/c/b/f;)V
areturn
L1:
ldc java/util/EnumSet
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L2
new com/a/c/b/n
dup
aload 0
aload 1
invokespecial com/a/c/b/n/<init>(Lcom/a/c/b/f;Ljava/lang/reflect/Type;)V
areturn
L2:
ldc java/util/Set
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L3
new com/a/c/b/o
dup
aload 0
invokespecial com/a/c/b/o/<init>(Lcom/a/c/b/f;)V
areturn
L3:
ldc java/util/Queue
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L4
new com/a/c/b/p
dup
aload 0
invokespecial com/a/c/b/p/<init>(Lcom/a/c/b/f;)V
areturn
L4:
new com/a/c/b/q
dup
aload 0
invokespecial com/a/c/b/q/<init>(Lcom/a/c/b/f;)V
areturn
L0:
ldc java/util/Map
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L5
ldc java/util/SortedMap
aload 2
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L6
new com/a/c/b/r
dup
aload 0
invokespecial com/a/c/b/r/<init>(Lcom/a/c/b/f;)V
areturn
L6:
aload 1
instanceof java/lang/reflect/ParameterizedType
ifeq L7
ldc java/lang/String
aload 1
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
getfield com/a/c/c/a/a Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L7
new com/a/c/b/h
dup
aload 0
invokespecial com/a/c/b/h/<init>(Lcom/a/c/b/f;)V
areturn
L7:
new com/a/c/b/i
dup
aload 0
invokespecial com/a/c/b/i/<init>(Lcom/a/c/b/f;)V
areturn
L5:
aconst_null
areturn
.limit locals 3
.limit stack 4
.end method

.method private b(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/a/c/b/ao;
new com/a/c/b/j
dup
aload 0
aload 2
aload 1
invokespecial com/a/c/b/j/<init>(Lcom/a/c/b/f;Ljava/lang/Class;Ljava/lang/reflect/Type;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Lcom/a/c/c/a;)Lcom/a/c/b/ao;
aload 1
getfield com/a/c/c/a/b Ljava/lang/reflect/Type;
astore 3
aload 1
getfield com/a/c/c/a/a Ljava/lang/Class;
astore 4
aload 0
getfield com/a/c/b/f/a Ljava/util/Map;
aload 3
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/c/s
astore 1
aload 1
ifnull L0
new com/a/c/b/g
dup
aload 0
aload 1
aload 3
invokespecial com/a/c/b/g/<init>(Lcom/a/c/b/f;Lcom/a/c/s;Ljava/lang/reflect/Type;)V
astore 2
L1:
aload 2
areturn
L0:
aload 0
getfield com/a/c/b/f/a Ljava/util/Map;
aload 4
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/c/s
astore 1
aload 1
ifnull L2
new com/a/c/b/k
dup
aload 0
aload 1
aload 3
invokespecial com/a/c/b/k/<init>(Lcom/a/c/b/f;Lcom/a/c/s;Ljava/lang/reflect/Type;)V
areturn
L2:
aload 0
aload 4
invokespecial com/a/c/b/f/a(Ljava/lang/Class;)Lcom/a/c/b/ao;
astore 1
aload 1
astore 2
aload 1
ifnonnull L1
ldc java/util/Collection
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L3
ldc java/util/SortedSet
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L4
new com/a/c/b/m
dup
aload 0
invokespecial com/a/c/b/m/<init>(Lcom/a/c/b/f;)V
astore 1
L5:
aload 1
astore 2
aload 1
ifnonnull L1
new com/a/c/b/j
dup
aload 0
aload 4
aload 3
invokespecial com/a/c/b/j/<init>(Lcom/a/c/b/f;Ljava/lang/Class;Ljava/lang/reflect/Type;)V
areturn
L4:
ldc java/util/EnumSet
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L6
new com/a/c/b/n
dup
aload 0
aload 3
invokespecial com/a/c/b/n/<init>(Lcom/a/c/b/f;Ljava/lang/reflect/Type;)V
astore 1
goto L5
L6:
ldc java/util/Set
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L7
new com/a/c/b/o
dup
aload 0
invokespecial com/a/c/b/o/<init>(Lcom/a/c/b/f;)V
astore 1
goto L5
L7:
ldc java/util/Queue
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L8
new com/a/c/b/p
dup
aload 0
invokespecial com/a/c/b/p/<init>(Lcom/a/c/b/f;)V
astore 1
goto L5
L8:
new com/a/c/b/q
dup
aload 0
invokespecial com/a/c/b/q/<init>(Lcom/a/c/b/f;)V
astore 1
goto L5
L3:
ldc java/util/Map
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L9
ldc java/util/SortedMap
aload 4
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifeq L10
new com/a/c/b/r
dup
aload 0
invokespecial com/a/c/b/r/<init>(Lcom/a/c/b/f;)V
astore 1
goto L5
L10:
aload 3
instanceof java/lang/reflect/ParameterizedType
ifeq L11
ldc java/lang/String
aload 3
checkcast java/lang/reflect/ParameterizedType
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
iconst_0
aaload
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
getfield com/a/c/c/a/a Ljava/lang/Class;
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ifne L11
new com/a/c/b/h
dup
aload 0
invokespecial com/a/c/b/h/<init>(Lcom/a/c/b/f;)V
astore 1
goto L5
L11:
new com/a/c/b/i
dup
aload 0
invokespecial com/a/c/b/i/<init>(Lcom/a/c/b/f;)V
astore 1
goto L5
L9:
aconst_null
astore 1
goto L5
.limit locals 5
.limit stack 5
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/c/b/f/a Ljava/util/Map;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
