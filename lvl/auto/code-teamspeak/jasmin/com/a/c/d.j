.bytecode 50.0
.class public synchronized abstract enum com/a/c/d
.super java/lang/Enum
.implements com/a/c/j

.field public static final enum 'a' Lcom/a/c/d;

.field public static final enum 'b' Lcom/a/c/d;

.field public static final enum 'c' Lcom/a/c/d;

.field public static final enum 'd' Lcom/a/c/d;

.field public static final enum 'e' Lcom/a/c/d;

.field private static final synthetic 'f' [Lcom/a/c/d;

.method static <clinit>()V
new com/a/c/e
dup
ldc "IDENTITY"
invokespecial com/a/c/e/<init>(Ljava/lang/String;)V
putstatic com/a/c/d/a Lcom/a/c/d;
new com/a/c/f
dup
ldc "UPPER_CAMEL_CASE"
invokespecial com/a/c/f/<init>(Ljava/lang/String;)V
putstatic com/a/c/d/b Lcom/a/c/d;
new com/a/c/g
dup
ldc "UPPER_CAMEL_CASE_WITH_SPACES"
invokespecial com/a/c/g/<init>(Ljava/lang/String;)V
putstatic com/a/c/d/c Lcom/a/c/d;
new com/a/c/h
dup
ldc "LOWER_CASE_WITH_UNDERSCORES"
invokespecial com/a/c/h/<init>(Ljava/lang/String;)V
putstatic com/a/c/d/d Lcom/a/c/d;
new com/a/c/i
dup
ldc "LOWER_CASE_WITH_DASHES"
invokespecial com/a/c/i/<init>(Ljava/lang/String;)V
putstatic com/a/c/d/e Lcom/a/c/d;
iconst_5
anewarray com/a/c/d
dup
iconst_0
getstatic com/a/c/d/a Lcom/a/c/d;
aastore
dup
iconst_1
getstatic com/a/c/d/b Lcom/a/c/d;
aastore
dup
iconst_2
getstatic com/a/c/d/c Lcom/a/c/d;
aastore
dup
iconst_3
getstatic com/a/c/d/d Lcom/a/c/d;
aastore
dup
iconst_4
getstatic com/a/c/d/e Lcom/a/c/d;
aastore
putstatic com/a/c/d/f [Lcom/a/c/d;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/c/d/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method private static a(CLjava/lang/String;I)Ljava/lang/String;
iload 2
aload 1
invokevirtual java/lang/String/length()I
if_icmpge L0
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
iload 0
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 1
iload 2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L0:
iload 0
invokestatic java/lang/String/valueOf(C)Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
iconst_0
istore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
istore 1
L0:
iload 2
aload 0
invokevirtual java/lang/String/length()I
iconst_1
isub
if_icmpge L1
iload 1
invokestatic java/lang/Character/isLetter(C)Z
ifne L1
aload 4
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
goto L0
L1:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L2
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L3:
aload 3
areturn
L2:
aload 0
astore 3
iload 1
invokestatic java/lang/Character/isUpperCase(C)Z
ifne L3
iload 1
invokestatic java/lang/Character/toUpperCase(C)C
istore 1
iload 2
iconst_1
iadd
istore 2
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
iload 2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L5:
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L4:
iload 1
invokestatic java/lang/String/valueOf(C)Ljava/lang/String;
astore 0
goto L5
.limit locals 5
.limit stack 3
.end method

.method static synthetic a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
iconst_0
istore 3
L0:
iload 3
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 0
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 2
iload 2
invokestatic java/lang/Character/isUpperCase(C)Z
ifeq L2
aload 4
invokevirtual java/lang/StringBuilder/length()I
ifeq L2
aload 4
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
aload 4
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 2
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
iconst_0
istore 2
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
istore 1
L0:
iload 2
aload 0
invokevirtual java/lang/String/length()I
iconst_1
isub
if_icmpge L1
iload 1
invokestatic java/lang/Character/isLetter(C)Z
ifne L1
aload 4
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
aload 0
iload 2
invokevirtual java/lang/String/charAt(I)C
istore 1
goto L0
L1:
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpne L2
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 3
L3:
aload 3
areturn
L2:
aload 0
astore 3
iload 1
invokestatic java/lang/Character/isUpperCase(C)Z
ifne L3
iload 1
invokestatic java/lang/Character/toUpperCase(C)C
istore 1
iload 2
iconst_1
iadd
istore 2
iload 2
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L4
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
aload 0
iload 2
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L5:
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L4:
iload 1
invokestatic java/lang/String/valueOf(C)Ljava/lang/String;
astore 0
goto L5
.limit locals 5
.limit stack 3
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
astore 4
iconst_0
istore 3
L0:
iload 3
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 0
iload 3
invokevirtual java/lang/String/charAt(I)C
istore 2
iload 2
invokestatic java/lang/Character/isUpperCase(C)Z
ifeq L2
aload 4
invokevirtual java/lang/StringBuilder/length()I
ifeq L2
aload 4
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
L2:
aload 4
iload 2
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/c/d;
ldc com/a/c/d
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/c/d
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/c/d;
getstatic com/a/c/d/f [Lcom/a/c/d;
invokevirtual [Lcom/a/c/d;/clone()Ljava/lang/Object;
checkcast [Lcom/a/c/d;
areturn
.limit locals 0
.limit stack 1
.end method
