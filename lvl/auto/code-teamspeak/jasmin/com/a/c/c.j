.bytecode 50.0
.class public final synchronized com/a/c/c
.super java/lang/Object

.field private final 'a' Ljava/lang/reflect/Field;

.method public <init>(Ljava/lang/reflect/Field;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
invokestatic com/a/c/b/a/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
putfield com/a/c/c/a Ljava/lang/reflect/Field;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Ljava/lang/Class;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/getDeclaringClass()Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
aload 1
invokevirtual java/lang/reflect/Field/get(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
aload 1
invokevirtual java/lang/reflect/Field/getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(I)Z
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/getModifiers()I
iload 1
iand
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/getName()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private c()Ljava/lang/reflect/Type;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/getGenericType()Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/lang/Class;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/getType()Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method private e()Ljava/util/Collection;
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/getAnnotations()[Ljava/lang/annotation/Annotation;
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
areturn
.limit locals 1
.limit stack 1
.end method

.method private f()Z
aload 0
getfield com/a/c/c/a Ljava/lang/reflect/Field;
invokevirtual java/lang/reflect/Field/isSynthetic()Z
ireturn
.limit locals 1
.limit stack 1
.end method
