.bytecode 50.0
.class public synchronized abstract com/a/c/an
.super java/lang/Object

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private a()Lcom/a/c/an;
new com/a/c/ao
dup
aload 0
invokespecial com/a/c/ao/<init>(Lcom/a/c/an;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Lcom/a/c/w;)Ljava/lang/Object;
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
new com/a/c/b/a/h
dup
aload 1
invokespecial com/a/c/b/a/h/<init>(Lcom/a/c/w;)V
invokevirtual com/a/c/an/a(Lcom/a/c/d/a;)Ljava/lang/Object;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new com/a/c/x
dup
aload 1
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/io/Reader;)Ljava/lang/Object;
aload 0
new com/a/c/d/a
dup
aload 1
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
invokevirtual com/a/c/an/a(Lcom/a/c/d/a;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/lang/String;)Ljava/lang/Object;
aload 0
new com/a/c/d/a
dup
new java/io/StringReader
dup
aload 1
invokespecial java/io/StringReader/<init>(Ljava/lang/String;)V
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
invokevirtual com/a/c/an/a(Lcom/a/c/d/a;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 6
.end method

.method private a(Ljava/io/Writer;Ljava/lang/Object;)V
aload 0
new com/a/c/d/e
dup
aload 1
invokespecial com/a/c/d/e/<init>(Ljava/io/Writer;)V
aload 2
invokevirtual com/a/c/an/a(Lcom/a/c/d/e;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 4
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/String;
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 2
aload 0
new com/a/c/d/e
dup
aload 2
invokespecial com/a/c/d/e/<init>(Ljava/io/Writer;)V
aload 1
invokevirtual com/a/c/an/a(Lcom/a/c/d/e;Ljava/lang/Object;)V
aload 2
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Ljava/lang/Object;)Lcom/a/c/w;
.catch java/io/IOException from L0 to L1 using L2
L0:
new com/a/c/b/a/j
dup
invokespecial com/a/c/b/a/j/<init>()V
astore 2
aload 0
aload 2
aload 1
invokevirtual com/a/c/an/a(Lcom/a/c/d/e;Ljava/lang/Object;)V
aload 2
invokevirtual com/a/c/b/a/j/a()Lcom/a/c/w;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new com/a/c/x
dup
aload 1
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 3
.limit stack 3
.end method

.method public abstract a(Lcom/a/c/d/a;)Ljava/lang/Object;
.end method

.method public abstract a(Lcom/a/c/d/e;Ljava/lang/Object;)V
.end method
