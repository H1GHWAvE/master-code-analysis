.bytecode 50.0
.class public final synchronized com/a/c/k
.super java/lang/Object

.field static final 'a' Z = 0


.field private static final 'd' Ljava/lang/String; = ")]}'\n"

.field final 'b' Lcom/a/c/u;

.field final 'c' Lcom/a/c/ad;

.field private final 'e' Ljava/lang/ThreadLocal;

.field private final 'f' Ljava/util/Map;

.field private final 'g' Ljava/util/List;

.field private final 'h' Lcom/a/c/b/f;

.field private final 'i' Z

.field private final 'j' Z

.field private final 'k' Z

.field private final 'l' Z

.method public <init>()V
aload 0
getstatic com/a/c/b/s/a Lcom/a/c/b/s;
getstatic com/a/c/d/a Lcom/a/c/d;
invokestatic java/util/Collections/emptyMap()Ljava/util/Map;
iconst_0
iconst_0
iconst_0
iconst_1
iconst_0
iconst_0
getstatic com/a/c/ah/a Lcom/a/c/ah;
invokestatic java/util/Collections/emptyList()Ljava/util/List;
invokespecial com/a/c/k/<init>(Lcom/a/c/b/s;Lcom/a/c/j;Ljava/util/Map;ZZZZZZLcom/a/c/ah;Ljava/util/List;)V
return
.limit locals 1
.limit stack 12
.end method

.method <init>(Lcom/a/c/b/s;Lcom/a/c/j;Ljava/util/Map;ZZZZZZLcom/a/c/ah;Ljava/util/List;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/lang/ThreadLocal
dup
invokespecial java/lang/ThreadLocal/<init>()V
putfield com/a/c/k/e Ljava/lang/ThreadLocal;
aload 0
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
invokestatic java/util/Collections/synchronizedMap(Ljava/util/Map;)Ljava/util/Map;
putfield com/a/c/k/f Ljava/util/Map;
aload 0
new com/a/c/l
dup
aload 0
invokespecial com/a/c/l/<init>(Lcom/a/c/k;)V
putfield com/a/c/k/b Lcom/a/c/u;
aload 0
new com/a/c/m
dup
aload 0
invokespecial com/a/c/m/<init>(Lcom/a/c/k;)V
putfield com/a/c/k/c Lcom/a/c/ad;
aload 0
new com/a/c/b/f
dup
aload 3
invokespecial com/a/c/b/f/<init>(Ljava/util/Map;)V
putfield com/a/c/k/h Lcom/a/c/b/f;
aload 0
iload 4
putfield com/a/c/k/i Z
aload 0
iload 6
putfield com/a/c/k/k Z
aload 0
iload 7
putfield com/a/c/k/j Z
aload 0
iload 8
putfield com/a/c/k/l Z
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 12
aload 12
getstatic com/a/c/b/a/z/Q Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/n/a Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
aload 1
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
aload 11
invokeinterface java/util/List/addAll(Ljava/util/Collection;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/x Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/m Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/g Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/i Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/k Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
getstatic java/lang/Long/TYPE Ljava/lang/Class;
astore 11
aload 10
getstatic com/a/c/ah/a Lcom/a/c/ah;
if_acmpne L0
getstatic com/a/c/b/a/z/n Lcom/a/c/an;
astore 3
L1:
aload 12
aload 11
ldc java/lang/Long
aload 3
invokestatic com/a/c/b/a/z/a(Ljava/lang/Class;Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
getstatic java/lang/Double/TYPE Ljava/lang/Class;
astore 10
iload 9
ifeq L2
getstatic com/a/c/b/a/z/p Lcom/a/c/an;
astore 3
L3:
aload 12
aload 10
ldc java/lang/Double
aload 3
invokestatic com/a/c/b/a/z/a(Ljava/lang/Class;Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
getstatic java/lang/Float/TYPE Ljava/lang/Class;
astore 10
iload 9
ifeq L4
getstatic com/a/c/b/a/z/o Lcom/a/c/an;
astore 3
L5:
aload 12
aload 10
ldc java/lang/Float
aload 3
invokestatic com/a/c/b/a/z/a(Ljava/lang/Class;Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/r Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/t Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/z Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/B Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
ldc java/math/BigDecimal
getstatic com/a/c/b/a/z/v Lcom/a/c/an;
invokestatic com/a/c/b/a/z/a(Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
ldc java/math/BigInteger
getstatic com/a/c/b/a/z/w Lcom/a/c/an;
invokestatic com/a/c/b/a/z/a(Ljava/lang/Class;Lcom/a/c/an;)Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/D Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/F Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/J Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/O Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/H Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/d Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/e/a Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/M Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/w/a Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/u/a Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/K Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/a/a Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/R Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
getstatic com/a/c/b/a/z/b Lcom/a/c/ap;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
new com/a/c/b/a/c
dup
aload 0
getfield com/a/c/k/h Lcom/a/c/b/f;
invokespecial com/a/c/b/a/c/<init>(Lcom/a/c/b/f;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
new com/a/c/b/a/l
dup
aload 0
getfield com/a/c/k/h Lcom/a/c/b/f;
iload 5
invokespecial com/a/c/b/a/l/<init>(Lcom/a/c/b/f;Z)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
new com/a/c/b/a/g
dup
aload 0
getfield com/a/c/k/h Lcom/a/c/b/f;
invokespecial com/a/c/b/a/g/<init>(Lcom/a/c/b/f;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 12
new com/a/c/b/a/q
dup
aload 0
getfield com/a/c/k/h Lcom/a/c/b/f;
aload 2
aload 1
invokespecial com/a/c/b/a/q/<init>(Lcom/a/c/b/f;Lcom/a/c/j;Lcom/a/c/b/s;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
aload 0
aload 12
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
putfield com/a/c/k/g Ljava/util/List;
return
L0:
new com/a/c/p
dup
aload 0
invokespecial com/a/c/p/<init>(Lcom/a/c/k;)V
astore 3
goto L1
L2:
new com/a/c/n
dup
aload 0
invokespecial com/a/c/n/<init>(Lcom/a/c/k;)V
astore 3
goto L3
L4:
new com/a/c/o
dup
aload 0
invokespecial com/a/c/o/<init>(Lcom/a/c/k;)V
astore 3
goto L5
.limit locals 13
.limit stack 6
.end method

.method private a(Lcom/a/c/ah;)Lcom/a/c/an;
aload 1
getstatic com/a/c/ah/a Lcom/a/c/ah;
if_acmpne L0
getstatic com/a/c/b/a/z/n Lcom/a/c/an;
areturn
L0:
new com/a/c/p
dup
aload 0
invokespecial com/a/c/p/<init>(Lcom/a/c/k;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Z)Lcom/a/c/an;
iload 1
ifeq L0
getstatic com/a/c/b/a/z/p Lcom/a/c/an;
areturn
L0:
new com/a/c/n
dup
aload 0
invokespecial com/a/c/n/<init>(Lcom/a/c/k;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/io/Writer;)Lcom/a/c/d/e;
aload 0
getfield com/a/c/k/k Z
ifeq L0
aload 1
ldc ")]}'\n"
invokevirtual java/io/Writer/write(Ljava/lang/String;)V
L0:
new com/a/c/d/e
dup
aload 1
invokespecial com/a/c/d/e/<init>(Ljava/io/Writer;)V
astore 1
aload 0
getfield com/a/c/k/l Z
ifeq L1
ldc "  "
invokevirtual java/lang/String/length()I
ifne L2
aload 1
aconst_null
putfield com/a/c/d/e/a Ljava/lang/String;
aload 1
ldc ":"
putfield com/a/c/d/e/b Ljava/lang/String;
L1:
aload 1
aload 0
getfield com/a/c/k/i Z
putfield com/a/c/d/e/e Z
aload 1
areturn
L2:
aload 1
ldc "  "
putfield com/a/c/d/e/a Ljava/lang/String;
aload 1
ldc ": "
putfield com/a/c/d/e/b Ljava/lang/String;
goto L1
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Object;)Lcom/a/c/w;
aload 1
ifnonnull L0
getstatic com/a/c/y/a Lcom/a/c/y;
areturn
L0:
aload 0
aload 1
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/a/c/w;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/a/c/w;Ljava/lang/Class;)Ljava/lang/Object;
aload 0
aload 1
aload 2
invokevirtual com/a/c/k/a(Lcom/a/c/w;Ljava/lang/reflect/Type;)Ljava/lang/Object;
astore 1
aload 2
invokestatic com/a/c/b/ap/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/Reader;Ljava/lang/Class;)Ljava/lang/Object;
new com/a/c/d/a
dup
aload 1
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
astore 1
aload 0
aload 1
aload 2
invokevirtual com/a/c/k/a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
astore 3
aload 3
aload 1
invokestatic com/a/c/k/a(Ljava/lang/Object;Lcom/a/c/d/a;)V
aload 2
invokestatic com/a/c/b/ap/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 3
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
new com/a/c/d/a
dup
aload 1
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
astore 1
aload 0
aload 1
aload 2
invokevirtual com/a/c/k/a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
astore 2
aload 2
aload 1
invokestatic com/a/c/k/a(Ljava/lang/Object;Lcom/a/c/d/a;)V
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
aload 1
ifnonnull L0
aconst_null
astore 1
L1:
aload 2
invokestatic com/a/c/b/ap/a(Ljava/lang/Class;)Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
areturn
L0:
new com/a/c/d/a
dup
new java/io/StringReader
dup
aload 1
invokespecial java/io/StringReader/<init>(Ljava/lang/String;)V
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
astore 3
aload 0
aload 3
aload 2
invokevirtual com/a/c/k/a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
astore 1
aload 1
aload 3
invokestatic com/a/c/k/a(Ljava/lang/Object;Lcom/a/c/d/a;)V
goto L1
.limit locals 4
.limit stack 5
.end method

.method private a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
new com/a/c/d/a
dup
new java/io/StringReader
dup
aload 1
invokespecial java/io/StringReader/<init>(Ljava/lang/String;)V
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
astore 1
aload 0
aload 1
aload 2
invokevirtual com/a/c/k/a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
astore 2
aload 2
aload 1
invokestatic com/a/c/k/a(Ljava/lang/Object;Lcom/a/c/d/a;)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method private a(Lcom/a/c/w;)Ljava/lang/String;
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 2
aload 0
aload 1
aload 2
invokespecial com/a/c/k/a(Lcom/a/c/w;Ljava/lang/Appendable;)V
aload 2
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 3
.end method

.method static synthetic a(D)V
dload 0
invokestatic java/lang/Double/isNaN(D)Z
ifne L0
dload 0
invokestatic java/lang/Double/isInfinite(D)Z
ifeq L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
dload 0
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
ldc " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 2
.limit stack 5
.end method

.method private a(Lcom/a/c/w;Lcom/a/c/d/e;)V
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 2
getfield com/a/c/d/e/c Z
istore 3
aload 2
iconst_1
putfield com/a/c/d/e/c Z
aload 2
getfield com/a/c/d/e/d Z
istore 4
aload 2
aload 0
getfield com/a/c/k/j Z
putfield com/a/c/d/e/d Z
aload 2
getfield com/a/c/d/e/e Z
istore 5
aload 2
aload 0
getfield com/a/c/k/i Z
putfield com/a/c/d/e/e Z
L0:
aload 1
aload 2
invokestatic com/a/c/b/aq/a(Lcom/a/c/w;Lcom/a/c/d/e;)V
L1:
aload 2
iload 3
putfield com/a/c/d/e/c Z
aload 2
iload 4
putfield com/a/c/d/e/d Z
aload 2
iload 5
putfield com/a/c/d/e/e Z
return
L2:
astore 1
L4:
new com/a/c/x
dup
aload 1
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
aload 2
iload 3
putfield com/a/c/d/e/c Z
aload 2
iload 4
putfield com/a/c/d/e/d Z
aload 2
iload 5
putfield com/a/c/d/e/e Z
aload 1
athrow
.limit locals 6
.limit stack 3
.end method

.method private a(Lcom/a/c/w;Ljava/lang/Appendable;)V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L1 to L3 using L4
.catch all from L1 to L3 using L5
.catch java/io/IOException from L3 to L6 using L2
.catch all from L7 to L5 using L5
.catch java/io/IOException from L8 to L2 using L2
L0:
aload 0
aload 2
invokestatic com/a/c/b/aq/a(Ljava/lang/Appendable;)Ljava/io/Writer;
invokespecial com/a/c/k/a(Ljava/io/Writer;)Lcom/a/c/d/e;
astore 2
aload 2
getfield com/a/c/d/e/c Z
istore 3
aload 2
iconst_1
putfield com/a/c/d/e/c Z
aload 2
getfield com/a/c/d/e/d Z
istore 4
aload 2
aload 0
getfield com/a/c/k/j Z
putfield com/a/c/d/e/d Z
aload 2
getfield com/a/c/d/e/e Z
istore 5
aload 2
aload 0
getfield com/a/c/k/i Z
putfield com/a/c/d/e/e Z
L1:
aload 1
aload 2
invokestatic com/a/c/b/aq/a(Lcom/a/c/w;Lcom/a/c/d/e;)V
L3:
aload 2
iload 3
putfield com/a/c/d/e/c Z
aload 2
iload 4
putfield com/a/c/d/e/d Z
aload 2
iload 5
putfield com/a/c/d/e/e Z
L6:
return
L4:
astore 1
L7:
new com/a/c/x
dup
aload 1
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
L5:
astore 1
L8:
aload 2
iload 3
putfield com/a/c/d/e/c Z
aload 2
iload 4
putfield com/a/c/d/e/d Z
aload 2
iload 5
putfield com/a/c/d/e/e Z
aload 1
athrow
L2:
astore 1
new java/lang/RuntimeException
dup
aload 1
invokespecial java/lang/RuntimeException/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 6
.limit stack 3
.end method

.method public static a(Ljava/lang/Object;Lcom/a/c/d/a;)V
.catch com/a/c/d/f from L0 to L1 using L1
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnull L3
L0:
aload 1
invokevirtual com/a/c/d/a/f()Lcom/a/c/d/d;
getstatic com/a/c/d/d/j Lcom/a/c/d/d;
if_acmpeq L3
new com/a/c/x
dup
ldc "JSON document was not fully consumed."
invokespecial com/a/c/x/<init>(Ljava/lang/String;)V
athrow
L1:
astore 0
new com/a/c/ag
dup
aload 0
invokespecial com/a/c/ag/<init>(Ljava/lang/Throwable;)V
athrow
L2:
astore 0
new com/a/c/x
dup
aload 0
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
L3:
return
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Appendable;)V
aload 1
ifnull L0
aload 0
aload 1
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
aload 2
invokevirtual com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
return
L0:
aload 0
getstatic com/a/c/y/a Lcom/a/c/y;
aload 2
invokespecial com/a/c/k/a(Lcom/a/c/w;Ljava/lang/Appendable;)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/a/c/d/e;)V
.catch java/io/IOException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L3 using L3
aload 0
aload 2
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
astore 2
aload 3
getfield com/a/c/d/e/c Z
istore 4
aload 3
iconst_1
putfield com/a/c/d/e/c Z
aload 3
getfield com/a/c/d/e/d Z
istore 5
aload 3
aload 0
getfield com/a/c/k/j Z
putfield com/a/c/d/e/d Z
aload 3
getfield com/a/c/d/e/e Z
istore 6
aload 3
aload 0
getfield com/a/c/k/i Z
putfield com/a/c/d/e/e Z
L0:
aload 2
aload 3
aload 1
invokevirtual com/a/c/an/a(Lcom/a/c/d/e;Ljava/lang/Object;)V
L1:
aload 3
iload 4
putfield com/a/c/d/e/c Z
aload 3
iload 5
putfield com/a/c/d/e/d Z
aload 3
iload 6
putfield com/a/c/d/e/e Z
return
L2:
astore 1
L4:
new com/a/c/x
dup
aload 1
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
L3:
astore 1
aload 3
iload 4
putfield com/a/c/d/e/c Z
aload 3
iload 5
putfield com/a/c/d/e/d Z
aload 3
iload 6
putfield com/a/c/d/e/e Z
aload 1
athrow
.limit locals 7
.limit stack 3
.end method

.method private b(Z)Lcom/a/c/an;
iload 1
ifeq L0
getstatic com/a/c/b/a/z/o Lcom/a/c/an;
areturn
L0:
new com/a/c/o
dup
aload 0
invokespecial com/a/c/o/<init>(Lcom/a/c/k;)V
areturn
.limit locals 2
.limit stack 3
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/String;
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 2
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 3
aload 0
aload 1
aload 2
aload 3
invokevirtual com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
aload 3
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private b(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;
new java/io/StringWriter
dup
invokespecial java/io/StringWriter/<init>()V
astore 3
aload 0
aload 1
aload 2
aload 3
invokevirtual com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
aload 3
invokevirtual java/io/StringWriter/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static b(D)V
dload 0
invokestatic java/lang/Double/isNaN(D)Z
ifne L0
dload 0
invokestatic java/lang/Double/isInfinite(D)Z
ifeq L1
L0:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
dload 0
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
ldc " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L1:
return
.limit locals 2
.limit stack 5
.end method

.method public final a(Lcom/a/c/ap;Lcom/a/c/c/a;)Lcom/a/c/an;
aload 0
getfield com/a/c/k/g Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 4
iconst_0
istore 3
L0:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/c/ap
astore 5
iload 3
ifne L2
aload 5
aload 1
if_acmpne L0
iconst_1
istore 3
goto L0
L2:
aload 5
aload 0
aload 2
invokeinterface com/a/c/ap/a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an; 2
astore 5
aload 5
ifnull L0
aload 5
areturn
L1:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "GSON cannot serialize "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 6
.limit stack 5
.end method

.method public final a(Lcom/a/c/c/a;)Lcom/a/c/an;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L2 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
getfield com/a/c/k/f Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/c/an
astore 3
aload 3
ifnull L9
L10:
aload 3
areturn
L9:
aload 0
getfield com/a/c/k/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/get()Ljava/lang/Object;
checkcast java/util/Map
astore 4
iconst_0
istore 2
aload 4
ifnonnull L8
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
aload 0
getfield com/a/c/k/e Ljava/lang/ThreadLocal;
aload 4
invokevirtual java/lang/ThreadLocal/set(Ljava/lang/Object;)V
iconst_1
istore 2
L11:
aload 4
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/c/q
astore 5
aload 5
astore 3
aload 5
ifnonnull L10
L0:
new com/a/c/q
dup
invokespecial com/a/c/q/<init>()V
astore 3
aload 4
aload 1
aload 3
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 0
getfield com/a/c/k/g Ljava/util/List;
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 6
L1:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L7
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/c/ap
aload 0
aload 1
invokeinterface com/a/c/ap/a(Lcom/a/c/k;Lcom/a/c/c/a;)Lcom/a/c/an; 2
astore 5
L3:
aload 5
ifnull L1
L4:
aload 3
getfield com/a/c/q/a Lcom/a/c/an;
ifnull L5
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L2:
astore 3
aload 4
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
iload 2
ifeq L12
aload 0
getfield com/a/c/k/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/remove()V
L12:
aload 3
athrow
L5:
aload 3
aload 5
putfield com/a/c/q/a Lcom/a/c/an;
aload 0
getfield com/a/c/k/f Ljava/util/Map;
aload 1
aload 5
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L6:
aload 4
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
pop
aload 5
astore 3
iload 2
ifeq L10
aload 0
getfield com/a/c/k/e Ljava/lang/ThreadLocal;
invokevirtual java/lang/ThreadLocal/remove()V
aload 5
areturn
L7:
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
ldc "GSON cannot handle "
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L8:
goto L11
.limit locals 7
.limit stack 5
.end method

.method public final a(Ljava/lang/Class;)Lcom/a/c/an;
aload 0
aload 1
invokestatic com/a/c/c/a/a(Ljava/lang/Class;)Lcom/a/c/c/a;
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/reflect/Type;)Lcom/a/c/w;
new com/a/c/b/a/j
dup
invokespecial com/a/c/b/a/j/<init>()V
astore 3
aload 0
aload 1
aload 2
aload 3
invokespecial com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/a/c/d/e;)V
aload 3
invokevirtual com/a/c/b/a/j/a()Lcom/a/c/w;
areturn
.limit locals 4
.limit stack 4
.end method

.method public final a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
.catch java/io/EOFException from L0 to L1 using L2
.catch java/lang/IllegalStateException from L0 to L1 using L3
.catch java/io/IOException from L0 to L1 using L4
.catch all from L0 to L1 using L5
.catch java/io/EOFException from L6 to L7 using L2
.catch java/lang/IllegalStateException from L6 to L7 using L3
.catch java/io/IOException from L6 to L7 using L4
.catch all from L6 to L7 using L5
.catch all from L8 to L5 using L5
.catch all from L9 to L4 using L5
.catch all from L10 to L11 using L5
iconst_1
istore 3
aload 1
getfield com/a/c/d/a/b Z
istore 4
aload 1
iconst_1
putfield com/a/c/d/a/b Z
L0:
aload 1
invokevirtual com/a/c/d/a/f()Lcom/a/c/d/d;
pop
L1:
iconst_0
istore 3
L6:
aload 0
aload 2
invokestatic com/a/c/c/a/a(Ljava/lang/reflect/Type;)Lcom/a/c/c/a;
invokevirtual com/a/c/k/a(Lcom/a/c/c/a;)Lcom/a/c/an;
aload 1
invokevirtual com/a/c/an/a(Lcom/a/c/d/a;)Ljava/lang/Object;
astore 2
L7:
aload 1
iload 4
putfield com/a/c/d/a/b Z
aload 2
areturn
L2:
astore 2
iload 3
ifeq L8
aload 1
iload 4
putfield com/a/c/d/a/b Z
aconst_null
areturn
L8:
new com/a/c/ag
dup
aload 2
invokespecial com/a/c/ag/<init>(Ljava/lang/Throwable;)V
athrow
L5:
astore 2
aload 1
iload 4
putfield com/a/c/d/a/b Z
aload 2
athrow
L3:
astore 2
L9:
new com/a/c/ag
dup
aload 2
invokespecial com/a/c/ag/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 2
L10:
new com/a/c/ag
dup
aload 2
invokespecial com/a/c/ag/<init>(Ljava/lang/Throwable;)V
athrow
L11:
.limit locals 5
.limit stack 3
.end method

.method public final a(Lcom/a/c/w;Ljava/lang/reflect/Type;)Ljava/lang/Object;
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
new com/a/c/b/a/h
dup
aload 1
invokespecial com/a/c/b/a/h/<init>(Lcom/a/c/w;)V
aload 2
invokevirtual com/a/c/k/a(Lcom/a/c/d/a;Ljava/lang/reflect/Type;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 4
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/reflect/Type;Ljava/lang/Appendable;)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
aload 1
aload 2
aload 0
aload 3
invokestatic com/a/c/b/aq/a(Ljava/lang/Appendable;)Ljava/io/Writer;
invokespecial com/a/c/k/a(Ljava/io/Writer;)Lcom/a/c/d/e;
invokespecial com/a/c/k/a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/a/c/d/e;)V
L1:
return
L2:
astore 1
new com/a/c/x
dup
aload 1
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
.limit locals 4
.limit stack 5
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "{serializeNulls:"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
aload 0
getfield com/a/c/k/i Z
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
ldc "factories:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/a/c/k/g Ljava/util/List;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc ",instanceCreators:"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
getfield com/a/c/k/h Lcom/a/c/b/f;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
ldc "}"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 3
.end method
