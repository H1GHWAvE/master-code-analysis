.bytecode 50.0
.class public final synchronized com/a/c/af
.super java/lang/Object
.implements java/util/Iterator

.field private final 'a' Lcom/a/c/d/a;

.field private final 'b' Ljava/lang/Object;

.method private <init>(Ljava/io/Reader;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/a/c/d/a
dup
aload 1
invokespecial com/a/c/d/a/<init>(Ljava/io/Reader;)V
putfield com/a/c/af/a Lcom/a/c/d/a;
aload 0
getfield com/a/c/af/a Lcom/a/c/d/a;
iconst_1
putfield com/a/c/d/a/b Z
aload 0
new java/lang/Object
dup
invokespecial java/lang/Object/<init>()V
putfield com/a/c/af/b Ljava/lang/Object;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;)V
aload 0
new java/io/StringReader
dup
aload 1
invokespecial java/io/StringReader/<init>(Ljava/lang/String;)V
invokespecial com/a/c/af/<init>(Ljava/io/Reader;)V
return
.limit locals 2
.limit stack 4
.end method

.method private a()Lcom/a/c/w;
.catch java/lang/StackOverflowError from L0 to L1 using L2
.catch java/lang/OutOfMemoryError from L0 to L1 using L3
.catch com/a/c/aa from L0 to L1 using L4
aload 0
invokevirtual com/a/c/af/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/c/af/a Lcom/a/c/d/a;
invokestatic com/a/c/b/aq/a(Lcom/a/c/d/a;)Lcom/a/c/w;
astore 1
L1:
aload 1
areturn
L2:
astore 1
new com/a/c/aa
dup
ldc "Failed parsing JSON source to Json"
aload 1
invokespecial com/a/c/aa/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L3:
astore 1
new com/a/c/aa
dup
ldc "Failed parsing JSON source to Json"
aload 1
invokespecial com/a/c/aa/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L4:
astore 2
aload 2
astore 1
aload 2
invokevirtual com/a/c/aa/getCause()Ljava/lang/Throwable;
instanceof java/io/EOFException
ifeq L5
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
astore 1
L5:
aload 1
athrow
.limit locals 3
.limit stack 4
.end method

.method public final hasNext()Z
.catch com/a/c/d/f from L0 to L1 using L2
.catch java/io/IOException from L0 to L1 using L3
.catch all from L0 to L1 using L4
.catch all from L5 to L6 using L4
.catch all from L7 to L4 using L4
.catch all from L8 to L9 using L4
.catch all from L10 to L11 using L4
aload 0
getfield com/a/c/af/b Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield com/a/c/af/a Lcom/a/c/d/a;
invokevirtual com/a/c/d/a/f()Lcom/a/c/d/d;
astore 3
getstatic com/a/c/d/d/j Lcom/a/c/d/d;
astore 4
L1:
aload 3
aload 4
if_acmpeq L11
iconst_1
istore 1
L5:
aload 2
monitorexit
L6:
iload 1
ireturn
L2:
astore 3
L7:
new com/a/c/ag
dup
aload 3
invokespecial com/a/c/ag/<init>(Ljava/lang/Throwable;)V
athrow
L4:
astore 3
L8:
aload 2
monitorexit
L9:
aload 3
athrow
L3:
astore 3
L10:
new com/a/c/x
dup
aload 3
invokespecial com/a/c/x/<init>(Ljava/lang/Throwable;)V
athrow
L11:
iconst_0
istore 1
goto L5
.limit locals 5
.limit stack 3
.end method

.method public final synthetic next()Ljava/lang/Object;
aload 0
invokespecial com/a/c/af/a()Lcom/a/c/w;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final remove()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method
