.bytecode 50.0
.class public final synchronized com/a/b/l/ap
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field public static final 'a' J = -1L


.field private static final 'b' [J

.field private static final 'c' [I

.field private static final 'd' [I

.method static <clinit>()V
bipush 37
newarray long
putstatic com/a/b/l/ap/b [J
bipush 37
newarray int
putstatic com/a/b/l/ap/c [I
bipush 37
newarray int
putstatic com/a/b/l/ap/d [I
new java/math/BigInteger
dup
ldc "10000000000000000"
bipush 16
invokespecial java/math/BigInteger/<init>(Ljava/lang/String;I)V
astore 1
iconst_2
istore 0
L0:
iload 0
bipush 36
if_icmpgt L1
getstatic com/a/b/l/ap/b [J
iload 0
ldc2_w -1L
iload 0
i2l
invokestatic com/a/b/l/ap/b(JJ)J
lastore
getstatic com/a/b/l/ap/c [I
iload 0
ldc2_w -1L
iload 0
i2l
invokestatic com/a/b/l/ap/c(JJ)J
l2i
iastore
getstatic com/a/b/l/ap/d [I
iload 0
aload 1
iload 0
invokevirtual java/math/BigInteger/toString(I)Ljava/lang/String;
invokevirtual java/lang/String/length()I
iconst_1
isub
iastore
iload 0
iconst_1
iadd
istore 0
goto L0
L1:
return
.limit locals 2
.limit stack 6
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(JJ)I
lload 0
ldc2_w -9223372036854775808L
lxor
ldc2_w -9223372036854775808L
lload 2
lxor
invokestatic com/a/b/l/u/a(JJ)I
ireturn
.limit locals 4
.limit stack 6
.end method

.method private static a(Ljava/lang/String;)J
aload 0
bipush 10
invokestatic com/a/b/l/ap/a(Ljava/lang/String;I)J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Ljava/lang/String;I)J
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokevirtual java/lang/String/length()I
ifne L0
new java/lang/NumberFormatException
dup
ldc "empty string"
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 1
iconst_2
if_icmplt L1
iload 1
bipush 36
if_icmple L2
L1:
new java/lang/NumberFormatException
dup
new java/lang/StringBuilder
dup
bipush 26
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "illegal radix: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
L2:
getstatic com/a/b/l/ap/d [I
iload 1
iaload
istore 4
iconst_0
istore 3
lconst_0
lstore 6
L3:
iload 3
aload 0
invokevirtual java/lang/String/length()I
if_icmpge L4
aload 0
iload 3
invokevirtual java/lang/String/charAt(I)C
iload 1
invokestatic java/lang/Character/digit(CI)I
istore 5
iload 5
iconst_m1
if_icmpne L5
new java/lang/NumberFormatException
dup
aload 0
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
L5:
iload 3
iload 4
iconst_1
isub
if_icmple L6
lload 6
lconst_0
lcmp
iflt L7
lload 6
getstatic com/a/b/l/ap/b [J
iload 1
laload
lcmp
ifge L8
iconst_0
istore 2
L9:
iload 2
ifeq L6
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifeq L10
ldc "Too large for unsigned long: "
aload 0
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
L11:
new java/lang/NumberFormatException
dup
aload 0
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
athrow
L8:
lload 6
getstatic com/a/b/l/ap/b [J
iload 1
laload
lcmp
ifgt L7
iload 5
getstatic com/a/b/l/ap/c [I
iload 1
iaload
if_icmpgt L7
iconst_0
istore 2
goto L9
L7:
iconst_1
istore 2
goto L9
L10:
new java/lang/String
dup
ldc "Too large for unsigned long: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 0
goto L11
L6:
lload 6
iload 1
i2l
lmul
iload 5
i2l
ladd
lstore 6
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
lload 6
lreturn
.limit locals 8
.limit stack 5
.end method

.method private static transient a([J)J
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
laload
ldc2_w -9223372036854775808L
lxor
lstore 3
L2:
iload 1
aload 0
arraylength
if_icmpge L3
aload 0
iload 1
laload
ldc2_w -9223372036854775808L
lxor
lstore 7
lload 3
lstore 5
lload 7
lload 3
lcmp
ifge L4
lload 7
lstore 5
L4:
iload 1
iconst_1
iadd
istore 1
lload 5
lstore 3
goto L2
L0:
iconst_0
istore 2
goto L1
L3:
lload 3
ldc2_w -9223372036854775808L
lxor
lreturn
.limit locals 9
.limit stack 4
.end method

.method public static a(J)Ljava/lang/String;
lload 0
bipush 10
invokestatic com/a/b/l/ap/a(JI)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 3
.end method

.method public static a(JI)Ljava/lang/String;
iload 2
iconst_2
if_icmplt L0
iload 2
bipush 36
if_icmpgt L0
iconst_1
istore 6
L1:
iload 6
ldc "radix (%s) must be between Character.MIN_RADIX and Character.MAX_RADIX"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 0
lconst_0
lcmp
ifne L2
ldc "0"
areturn
L0:
iconst_0
istore 6
goto L1
L2:
bipush 64
newarray char
astore 7
lload 0
lconst_0
lcmp
ifge L3
lload 0
iload 2
i2l
invokestatic com/a/b/l/ap/b(JJ)J
lstore 4
aload 7
bipush 63
lload 0
iload 2
i2l
lload 4
lmul
lsub
l2i
iload 2
invokestatic java/lang/Character/forDigit(II)C
castore
lload 4
lstore 0
bipush 63
istore 3
L4:
lload 0
lconst_0
lcmp
ifle L5
iload 3
iconst_1
isub
istore 3
aload 7
iload 3
lload 0
iload 2
i2l
lrem
l2i
iload 2
invokestatic java/lang/Character/forDigit(II)C
castore
lload 0
iload 2
i2l
ldiv
lstore 0
goto L4
L5:
new java/lang/String
dup
aload 7
iload 3
bipush 64
iload 3
isub
invokespecial java/lang/String/<init>([CII)V
areturn
L3:
bipush 64
istore 3
goto L4
.limit locals 8
.limit stack 8
.end method

.method private static transient a(Ljava/lang/String;[J)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
iconst_5
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
laload
invokestatic com/a/b/l/ap/a(J)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
laload
invokestatic com/a/b/l/ap/a(J)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a()Ljava/util/Comparator;
getstatic com/a/b/l/aq/a Lcom/a/b/l/aq;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a(JII)Z
lload 0
lconst_0
lcmp
iflt L0
lload 0
getstatic com/a/b/l/ap/b [J
iload 3
laload
lcmp
ifge L1
L2:
iconst_0
ireturn
L1:
lload 0
getstatic com/a/b/l/ap/b [J
iload 3
laload
lcmp
ifle L3
iconst_1
ireturn
L3:
iload 2
getstatic com/a/b/l/ap/c [I
iload 3
iaload
if_icmple L2
iconst_1
ireturn
L0:
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static b(J)J
ldc2_w -9223372036854775808L
lload 0
lxor
lreturn
.limit locals 2
.limit stack 4
.end method

.method public static b(JJ)J
iconst_1
istore 4
lload 2
lconst_0
lcmp
ifge L0
lload 0
lload 2
invokestatic com/a/b/l/ap/a(JJ)I
ifge L1
lconst_0
lreturn
L1:
lconst_1
lreturn
L0:
lload 0
lconst_0
lcmp
iflt L2
lload 0
lload 2
ldiv
lreturn
L2:
lload 0
iconst_1
lushr
lload 2
ldiv
iconst_1
lshl
lstore 5
lload 0
lload 5
lload 2
lmul
lsub
lload 2
invokestatic com/a/b/l/ap/a(JJ)I
iflt L3
L4:
iload 4
i2l
lload 5
ladd
lreturn
L3:
iconst_0
istore 4
goto L4
.limit locals 7
.limit stack 6
.end method

.method private static b(Ljava/lang/String;)J
.catch java/lang/NumberFormatException from L0 to L1 using L2
aload 0
invokestatic com/a/b/l/y/a(Ljava/lang/String;)Lcom/a/b/l/y;
astore 3
L0:
aload 3
getfield com/a/b/l/y/a Ljava/lang/String;
aload 3
getfield com/a/b/l/y/b I
invokestatic com/a/b/l/ap/a(Ljava/lang/String;I)J
lstore 1
L1:
lload 1
lreturn
L2:
astore 3
aload 0
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
aload 0
invokevirtual java/lang/String/length()I
ifeq L3
ldc "Error parsing value: "
aload 0
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 0
L4:
new java/lang/NumberFormatException
dup
aload 0
invokespecial java/lang/NumberFormatException/<init>(Ljava/lang/String;)V
astore 0
aload 0
aload 3
invokevirtual java/lang/NumberFormatException/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
aload 0
athrow
L3:
new java/lang/String
dup
ldc "Error parsing value: "
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 0
goto L4
.limit locals 4
.limit stack 3
.end method

.method private static transient b([J)J
iconst_1
istore 1
aload 0
arraylength
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
laload
ldc2_w -9223372036854775808L
lxor
lstore 3
L2:
iload 1
aload 0
arraylength
if_icmpge L3
aload 0
iload 1
laload
ldc2_w -9223372036854775808L
lxor
lstore 7
lload 3
lstore 5
lload 7
lload 3
lcmp
ifle L4
lload 7
lstore 5
L4:
iload 1
iconst_1
iadd
istore 1
lload 5
lstore 3
goto L2
L0:
iconst_0
istore 2
goto L1
L3:
lload 3
ldc2_w -9223372036854775808L
lxor
lreturn
.limit locals 9
.limit stack 4
.end method

.method public static c(JJ)J
lload 2
lconst_0
lcmp
ifge L0
lload 0
lload 2
invokestatic com/a/b/l/ap/a(JJ)I
ifge L1
lload 0
lreturn
L1:
lload 0
lload 2
lsub
lreturn
L0:
lload 0
lconst_0
lcmp
iflt L2
lload 0
lload 2
lrem
lreturn
L2:
lload 0
lload 0
iconst_1
lushr
lload 2
ldiv
iconst_1
lshl
lload 2
lmul
lsub
lstore 0
lload 0
lload 2
invokestatic com/a/b/l/ap/a(JJ)I
iflt L3
L4:
lload 0
lload 2
lsub
lreturn
L3:
lconst_0
lstore 2
goto L4
.limit locals 4
.limit stack 6
.end method
