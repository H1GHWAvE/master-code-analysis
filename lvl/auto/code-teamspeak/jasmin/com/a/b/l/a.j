.bytecode 50.0
.class public final synchronized com/a/b/l/a
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Z)I
iload 0
ifeq L0
sipush 1231
ireturn
L0:
sipush 1237
ireturn
.limit locals 1
.limit stack 1
.end method

.method public static a(ZZ)I
iload 0
iload 1
if_icmpne L0
iconst_0
ireturn
L0:
iload 0
ifeq L1
iconst_1
ireturn
L1:
iconst_m1
ireturn
.limit locals 2
.limit stack 2
.end method

.method static a([ZZII)I
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
baload
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static a([Z[Z)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
baload
aload 1
iload 3
baload
if_icmpne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static transient a(Ljava/lang/String;[Z)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 1
arraylength
bipush 7
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
aload 1
iconst_0
baload
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
aload 1
arraylength
if_icmpge L2
aload 3
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
baload
invokevirtual java/lang/StringBuilder/append(Z)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 3
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a()Ljava/util/Comparator;
getstatic com/a/b/l/c/a Lcom/a/b/l/c;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static transient a([Z)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/b
dup
aload 0
invokespecial com/a/b/l/b/<init>([Z)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a([ZZ)Z
iconst_0
istore 5
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
baload
iload 1
if_icmpne L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;)[Z
iconst_0
istore 1
aload 0
instanceof com/a/b/l/b
ifeq L0
aload 0
checkcast com/a/b/l/b
astore 0
aload 0
invokevirtual com/a/b/l/b/size()I
istore 1
iload 1
newarray boolean
astore 3
aload 0
getfield com/a/b/l/b/a [Z
aload 0
getfield com/a/b/l/b/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray boolean
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Boolean
invokevirtual java/lang/Boolean/booleanValue()Z
bastore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([ZI)[Z
iload 1
newarray boolean
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a([ZII)[Z
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray boolean
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[Z)[Z
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray boolean
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static transient b([Z)I
.annotation invisible Lcom/a/b/a/a;
.end annotation
iconst_0
istore 2
aload 0
arraylength
istore 4
iconst_0
istore 1
L0:
iload 1
iload 4
if_icmpge L1
iload 2
istore 3
aload 0
iload 1
baload
ifeq L2
iload 2
iconst_1
iadd
istore 3
L2:
iload 1
iconst_1
iadd
istore 1
iload 3
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static b([ZZ)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/a/a([ZZII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method static b([ZZII)I
iload 3
iconst_1
isub
istore 3
L0:
iload 3
iload 2
if_icmplt L1
aload 0
iload 3
baload
iload 1
if_icmpne L2
iload 3
ireturn
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static c([ZZ)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/a/b([ZZII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static synthetic c([ZZII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/a/a([ZZII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static synthetic d([ZZII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/a/b([ZZII)I
ireturn
.limit locals 4
.limit stack 4
.end method
