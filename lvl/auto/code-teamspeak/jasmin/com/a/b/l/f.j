.bytecode 50.0
.class public final synchronized com/a/b/l/f
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public static final 'a' I = 2


.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(BB)C
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
iload 0
bipush 8
ishl
iload 1
sipush 255
iand
ior
i2c
ireturn
.limit locals 2
.limit stack 3
.end method

.method private static a(J)C
lload 0
l2i
i2c
istore 2
iload 2
i2l
lload 0
lcmp
ifeq L0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 34
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Out of range: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 0
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L0:
iload 2
ireturn
.limit locals 3
.limit stack 5
.end method

.method private static a([B)C
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
aload 0
arraylength
iconst_2
if_icmplt L0
iconst_1
istore 1
L1:
iload 1
ldc "array too small: %s < %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
arraylength
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iconst_2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_0
baload
bipush 8
ishl
aload 0
iconst_1
baload
sipush 255
iand
ior
i2c
ireturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method private static transient a([C)C
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
caload
istore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
iload 1
istore 2
aload 0
iload 3
caload
iload 1
if_icmpge L4
aload 0
iload 3
caload
istore 2
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 1
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static a(C)I
iload 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static a(CC)I
iload 0
iload 1
isub
ireturn
.limit locals 2
.limit stack 2
.end method

.method static a([CCII)I
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
caload
iload 1
if_icmpne L2
iload 2
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static a([C[C)I
aload 0
ldc "array"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "target"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 2
L1:
iload 2
aload 0
arraylength
aload 1
arraylength
isub
iconst_1
iadd
if_icmpge L2
iconst_0
istore 3
L3:
iload 3
aload 1
arraylength
if_icmpge L4
aload 0
iload 2
iload 3
iadd
caload
aload 1
iload 3
caload
if_icmpne L5
iload 3
iconst_1
iadd
istore 3
goto L3
L4:
iload 2
ireturn
L5:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
iconst_m1
ireturn
.limit locals 4
.limit stack 3
.end method

.method private static transient a(Ljava/lang/String;[C)Ljava/lang/String;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
arraylength
istore 3
iload 3
ifne L0
ldc ""
areturn
L0:
new java/lang/StringBuilder
dup
aload 0
invokevirtual java/lang/String/length()I
iload 3
iconst_1
isub
imul
iload 3
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
astore 4
aload 4
aload 1
iconst_0
caload
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iconst_1
istore 2
L1:
iload 2
iload 3
if_icmpge L2
aload 4
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
iload 2
caload
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
aload 4
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 5
.limit stack 5
.end method

.method private static a()Ljava/util/Comparator;
getstatic com/a/b/l/h/a Lcom/a/b/l/h;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static a([CC)Z
iconst_0
istore 5
aload 0
arraylength
istore 3
iconst_0
istore 2
L0:
iload 5
istore 4
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
caload
iload 1
if_icmpne L2
iconst_1
istore 4
L1:
iload 4
ireturn
L2:
iload 2
iconst_1
iadd
istore 2
goto L0
.limit locals 6
.limit stack 2
.end method

.method private static a(Ljava/util/Collection;)[C
iconst_0
istore 1
aload 0
instanceof com/a/b/l/g
ifeq L0
aload 0
checkcast com/a/b/l/g
astore 0
aload 0
invokevirtual com/a/b/l/g/size()I
istore 1
iload 1
newarray char
astore 3
aload 0
getfield com/a/b/l/g/a [C
aload 0
getfield com/a/b/l/g/b I
aload 3
iconst_0
iload 1
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
L0:
aload 0
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
astore 0
aload 0
arraylength
istore 2
iload 2
newarray char
astore 3
L1:
iload 1
iload 2
if_icmpge L2
aload 3
iload 1
aload 0
iload 1
aaload
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Character
invokevirtual java/lang/Character/charValue()C
castore
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a([CI)[C
iload 1
newarray char
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static a([CII)[C
iload 1
iflt L0
iconst_1
istore 3
L1:
iload 3
ldc "Invalid minLength: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 2
iflt L2
iconst_1
istore 3
L3:
iload 3
ldc "Invalid padding: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
astore 4
aload 0
arraylength
iload 1
if_icmpge L4
iload 1
iload 2
iadd
istore 1
iload 1
newarray char
astore 4
aload 0
iconst_0
aload 4
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
L4:
aload 4
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
.limit locals 5
.limit stack 6
.end method

.method private static transient a([[C)[C
aload 0
arraylength
istore 3
iconst_0
istore 1
iconst_0
istore 2
L0:
iload 1
iload 3
if_icmpge L1
iload 2
aload 0
iload 1
aaload
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iload 2
newarray char
astore 4
aload 0
arraylength
istore 3
iconst_0
istore 2
iconst_0
istore 1
L2:
iload 1
iload 3
if_icmpge L3
aload 0
iload 1
aaload
astore 5
aload 5
iconst_0
aload 4
iload 2
aload 5
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
iload 2
aload 5
arraylength
iadd
istore 2
iload 1
iconst_1
iadd
istore 1
goto L2
L3:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method

.method private static b(J)C
lload 0
ldc2_w 65535L
lcmp
ifle L0
ldc_w 65535
ireturn
L0:
lload 0
lconst_0
lcmp
ifge L1
iconst_0
ireturn
L1:
lload 0
l2i
i2c
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static transient b([C)C
iconst_1
istore 3
aload 0
arraylength
ifle L0
iconst_1
istore 4
L1:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iconst_0
caload
istore 1
L2:
iload 3
aload 0
arraylength
if_icmpge L3
iload 1
istore 2
aload 0
iload 3
caload
iload 1
if_icmple L4
aload 0
iload 3
caload
istore 2
L4:
iload 3
iconst_1
iadd
istore 3
iload 2
istore 1
goto L2
L0:
iconst_0
istore 4
goto L1
L3:
iload 1
ireturn
.limit locals 5
.limit stack 2
.end method

.method private static b([CC)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/f/a([CCII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method static b([CCII)I
iload 3
iconst_1
isub
istore 3
L0:
iload 3
iload 2
if_icmplt L1
aload 0
iload 3
caload
iload 1
if_icmpne L2
iload 3
ireturn
L2:
iload 3
iconst_1
isub
istore 3
goto L0
L1:
iconst_m1
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static b(C)[B
.annotation invisible Lcom/a/b/a/c;
a s = "doesn't work"
.end annotation
iconst_2
newarray byte
dup
iconst_0
iload 0
bipush 8
ishr
i2b
bastore
dup
iconst_1
iload 0
i2b
bastore
areturn
.limit locals 1
.limit stack 5
.end method

.method private static c([CC)I
aload 0
iload 1
iconst_0
aload 0
arraylength
invokestatic com/a/b/l/f/b([CCII)I
ireturn
.limit locals 2
.limit stack 4
.end method

.method private static synthetic c([CCII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/f/a([CCII)I
ireturn
.limit locals 4
.limit stack 4
.end method

.method private static transient c([C)Ljava/util/List;
aload 0
arraylength
ifne L0
invokestatic java/util/Collections/emptyList()Ljava/util/List;
areturn
L0:
new com/a/b/l/g
dup
aload 0
invokespecial com/a/b/l/g/<init>([C)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static synthetic d([CCII)I
aload 0
iload 1
iload 2
iload 3
invokestatic com/a/b/l/f/b([CCII)I
ireturn
.limit locals 4
.limit stack 4
.end method
