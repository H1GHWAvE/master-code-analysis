.bytecode 50.0
.class final synchronized com/a/b/g/bh
.super com/a/b/g/a

.field private final 'a' Ljava/security/MessageDigest;

.field private final 'b' I

.field private 'c' Z

.method private <init>(Ljava/security/MessageDigest;I)V
aload 0
invokespecial com/a/b/g/a/<init>()V
aload 0
aload 1
putfield com/a/b/g/bh/a Ljava/security/MessageDigest;
aload 0
iload 2
putfield com/a/b/g/bh/b I
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Ljava/security/MessageDigest;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/g/bh/<init>(Ljava/security/MessageDigest;I)V
return
.limit locals 4
.limit stack 3
.end method

.method private b()V
aload 0
getfield com/a/b/g/bh/c Z
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "Cannot re-use a Hasher after calling hash() on it"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 2
.end method

.method public final a()Lcom/a/b/g/ag;
aload 0
invokespecial com/a/b/g/bh/b()V
aload 0
iconst_1
putfield com/a/b/g/bh/c Z
aload 0
getfield com/a/b/g/bh/b I
aload 0
getfield com/a/b/g/bh/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/getDigestLength()I
if_icmpne L0
aload 0
getfield com/a/b/g/bh/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/digest()[B
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
L0:
aload 0
getfield com/a/b/g/bh/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/digest()[B
aload 0
getfield com/a/b/g/bh/b I
invokestatic java/util/Arrays/copyOf([BI)[B
invokestatic com/a/b/g/ag/a([B)Lcom/a/b/g/ag;
areturn
.limit locals 1
.limit stack 2
.end method

.method protected final a(B)V
aload 0
invokespecial com/a/b/g/bh/b()V
aload 0
getfield com/a/b/g/bh/a Ljava/security/MessageDigest;
iload 1
invokevirtual java/security/MessageDigest/update(B)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final a([B)V
aload 0
invokespecial com/a/b/g/bh/b()V
aload 0
getfield com/a/b/g/bh/a Ljava/security/MessageDigest;
aload 1
invokevirtual java/security/MessageDigest/update([B)V
return
.limit locals 2
.limit stack 2
.end method

.method protected final a([BII)V
aload 0
invokespecial com/a/b/g/bh/b()V
aload 0
getfield com/a/b/g/bh/a Ljava/security/MessageDigest;
aload 1
iload 2
iload 3
invokevirtual java/security/MessageDigest/update([BII)V
return
.limit locals 4
.limit stack 4
.end method
