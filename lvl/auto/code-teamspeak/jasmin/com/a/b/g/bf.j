.bytecode 50.0
.class final synchronized com/a/b/g/bf
.super com/a/b/g/h
.implements java/io/Serializable

.field private final 'a' Ljava/security/MessageDigest;

.field private final 'b' I

.field private final 'c' Z

.field private final 'd' Ljava/lang/String;

.method <init>(Ljava/lang/String;ILjava/lang/String;)V
aload 0
invokespecial com/a/b/g/h/<init>()V
aload 0
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/g/bf/d Ljava/lang/String;
aload 0
aload 1
invokestatic com/a/b/g/bf/a(Ljava/lang/String;)Ljava/security/MessageDigest;
putfield com/a/b/g/bf/a Ljava/security/MessageDigest;
aload 0
getfield com/a/b/g/bf/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/getDigestLength()I
istore 4
iload 2
iconst_4
if_icmplt L0
iload 2
iload 4
if_icmpgt L0
iconst_1
istore 5
L1:
iload 5
ldc "bytes (%s) must be >= 4 and < %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
iload 2
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
dup
iconst_1
iload 4
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iload 2
putfield com/a/b/g/bf/b I
aload 0
aload 0
invokespecial com/a/b/g/bf/c()Z
putfield com/a/b/g/bf/c Z
return
L0:
iconst_0
istore 5
goto L1
.limit locals 6
.limit stack 6
.end method

.method <init>(Ljava/lang/String;Ljava/lang/String;)V
aload 0
invokespecial com/a/b/g/h/<init>()V
aload 0
aload 1
invokestatic com/a/b/g/bf/a(Ljava/lang/String;)Ljava/security/MessageDigest;
putfield com/a/b/g/bf/a Ljava/security/MessageDigest;
aload 0
aload 0
getfield com/a/b/g/bf/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/getDigestLength()I
putfield com/a/b/g/bf/b I
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/g/bf/d Ljava/lang/String;
aload 0
aload 0
invokespecial com/a/b/g/bf/c()Z
putfield com/a/b/g/bf/c Z
return
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/lang/String;)Ljava/security/MessageDigest;
.catch java/security/NoSuchAlgorithmException from L0 to L1 using L2
L0:
aload 0
invokestatic java/security/MessageDigest/getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
astore 0
L1:
aload 0
areturn
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private c()Z
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
L0:
aload 0
getfield com/a/b/g/bf/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/clone()Ljava/lang/Object;
pop
L1:
iconst_1
ireturn
L2:
astore 1
iconst_0
ireturn
.limit locals 2
.limit stack 1
.end method

.method private d()Ljava/lang/Object;
new com/a/b/g/bi
dup
aload 0
getfield com/a/b/g/bf/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/getAlgorithm()Ljava/lang/String;
aload 0
getfield com/a/b/g/bf/b I
aload 0
getfield com/a/b/g/bf/d Ljava/lang/String;
iconst_0
invokespecial com/a/b/g/bi/<init>(Ljava/lang/String;ILjava/lang/String;B)V
areturn
.limit locals 1
.limit stack 6
.end method

.method public final a()Lcom/a/b/g/al;
.catch java/lang/CloneNotSupportedException from L0 to L1 using L2
aload 0
getfield com/a/b/g/bf/c Z
ifeq L3
L0:
new com/a/b/g/bh
dup
aload 0
getfield com/a/b/g/bf/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/clone()Ljava/lang/Object;
checkcast java/security/MessageDigest
aload 0
getfield com/a/b/g/bf/b I
iconst_0
invokespecial com/a/b/g/bh/<init>(Ljava/security/MessageDigest;IB)V
astore 1
L1:
aload 1
areturn
L2:
astore 1
L3:
new com/a/b/g/bh
dup
aload 0
getfield com/a/b/g/bf/a Ljava/security/MessageDigest;
invokevirtual java/security/MessageDigest/getAlgorithm()Ljava/lang/String;
invokestatic com/a/b/g/bf/a(Ljava/lang/String;)Ljava/security/MessageDigest;
aload 0
getfield com/a/b/g/bf/b I
iconst_0
invokespecial com/a/b/g/bh/<init>(Ljava/security/MessageDigest;IB)V
areturn
.limit locals 2
.limit stack 5
.end method

.method public final b()I
aload 0
getfield com/a/b/g/bf/b I
bipush 8
imul
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/g/bf/d Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
