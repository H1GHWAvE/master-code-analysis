.bytecode 50.0
.class synchronized com/a/b/g/ab
.super java/lang/Object
.implements com/a/b/g/w
.implements java/io/Serializable

.field private final 'a' Lcom/a/b/g/w;

.method <init>(Lcom/a/b/g/w;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/g/w
putfield com/a/b/g/ab/a Lcom/a/b/g/w;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Iterable;Lcom/a/b/g/bn;)V
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 0
getfield com/a/b/g/ab/a Lcom/a/b/g/w;
aload 3
aload 2
invokeinterface com/a/b/g/w/a(Ljava/lang/Object;Lcom/a/b/g/bn;)V 2
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/g/bn;)V
aload 1
checkcast java/lang/Iterable
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 3
aload 0
getfield com/a/b/g/ab/a Lcom/a/b/g/w;
aload 3
aload 2
invokeinterface com/a/b/g/w/a(Ljava/lang/Object;Lcom/a/b/g/bn;)V 2
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/g/ab
ifeq L0
aload 1
checkcast com/a/b/g/ab
astore 1
aload 0
getfield com/a/b/g/ab/a Lcom/a/b/g/w;
aload 1
getfield com/a/b/g/ab/a Lcom/a/b/g/w;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
ldc com/a/b/g/ab
invokevirtual java/lang/Object/hashCode()I
aload 0
getfield com/a/b/g/ab/a Lcom/a/b/g/w;
invokevirtual java/lang/Object/hashCode()I
ixor
ireturn
.limit locals 1
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/g/ab/a Lcom/a/b/g/w;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 26
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Funnels.sequentialFunnel("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
