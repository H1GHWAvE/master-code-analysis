.bytecode 50.0
.class public final synchronized com/a/b/i/cc
.super java/io/FilterOutputStream
.implements java/io/DataOutput
.annotation invisible Lcom/a/b/a/a;
.end annotation

.method private <init>(Ljava/io/OutputStream;)V
aload 0
new java/io/DataOutputStream
dup
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/io/OutputStream
invokespecial java/io/DataOutputStream/<init>(Ljava/io/OutputStream;)V
invokespecial java/io/FilterOutputStream/<init>(Ljava/io/OutputStream;)V
return
.limit locals 2
.limit stack 4
.end method

.method public final close()V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
return
.limit locals 1
.limit stack 1
.end method

.method public final write([BII)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
return
.limit locals 4
.limit stack 4
.end method

.method public final writeBoolean(Z)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
checkcast java/io/DataOutputStream
iload 1
invokevirtual java/io/DataOutputStream/writeBoolean(Z)V
return
.limit locals 2
.limit stack 2
.end method

.method public final writeByte(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
checkcast java/io/DataOutputStream
iload 1
invokevirtual java/io/DataOutputStream/writeByte(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final writeBytes(Ljava/lang/String;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
checkcast java/io/DataOutputStream
aload 1
invokevirtual java/io/DataOutputStream/writeBytes(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final writeChar(I)V
aload 0
iload 1
invokevirtual com/a/b/i/cc/writeShort(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final writeChars(Ljava/lang/String;)V
iconst_0
istore 2
L0:
iload 2
aload 1
invokevirtual java/lang/String/length()I
if_icmpge L1
aload 0
aload 1
iload 2
invokevirtual java/lang/String/charAt(I)C
invokevirtual com/a/b/i/cc/writeChar(I)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 3
.limit stack 3
.end method

.method public final writeDouble(D)V
aload 0
dload 1
invokestatic java/lang/Double/doubleToLongBits(D)J
invokevirtual com/a/b/i/cc/writeLong(J)V
return
.limit locals 3
.limit stack 3
.end method

.method public final writeFloat(F)V
aload 0
fload 1
invokestatic java/lang/Float/floatToIntBits(F)I
invokevirtual com/a/b/i/cc/writeInt(I)V
return
.limit locals 2
.limit stack 2
.end method

.method public final writeInt(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
iload 1
sipush 255
iand
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
iload 1
bipush 8
ishr
sipush 255
iand
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
iload 1
bipush 16
ishr
sipush 255
iand
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
iload 1
bipush 24
ishr
sipush 255
iand
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final writeLong(J)V
lload 1
invokestatic java/lang/Long/reverseBytes(J)J
lstore 1
bipush 8
newarray byte
astore 4
bipush 7
istore 3
L0:
iload 3
iflt L1
aload 4
iload 3
ldc2_w 255L
lload 1
land
l2i
i2b
bastore
lload 1
bipush 8
lshr
lstore 1
iload 3
iconst_1
isub
istore 3
goto L0
L1:
aload 0
aload 4
iconst_0
bipush 8
invokevirtual com/a/b/i/cc/write([BII)V
return
.limit locals 5
.limit stack 6
.end method

.method public final writeShort(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
iload 1
sipush 255
iand
invokevirtual java/io/OutputStream/write(I)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
iload 1
bipush 8
ishr
sipush 255
iand
invokevirtual java/io/OutputStream/write(I)V
return
.limit locals 2
.limit stack 3
.end method

.method public final writeUTF(Ljava/lang/String;)V
aload 0
getfield com/a/b/i/cc/out Ljava/io/OutputStream;
checkcast java/io/DataOutputStream
aload 1
invokevirtual java/io/DataOutputStream/writeUTF(Ljava/lang/String;)V
return
.limit locals 2
.limit stack 2
.end method
