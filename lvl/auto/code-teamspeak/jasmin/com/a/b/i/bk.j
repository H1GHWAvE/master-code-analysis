.bytecode 50.0
.class public final synchronized com/a/b/i/bk
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' Ljava/util/logging/Logger;

.method static <clinit>()V
ldc com/a/b/i/bk
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/i/bk/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/io/Flushable;)V
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
invokeinterface java/io/Flushable/flush()V 0
L1:
return
L2:
astore 0
getstatic com/a/b/i/bk/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "IOException thrown while flushing Flushable."
aload 0
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 1
.limit stack 4
.end method

.method private static b(Ljava/io/Flushable;)V
.catch java/io/IOException from L0 to L1 using L2
.catch java/io/IOException from L3 to L4 using L5
L0:
aload 0
invokeinterface java/io/Flushable/flush()V 0
L1:
return
L2:
astore 0
L3:
getstatic com/a/b/i/bk/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "IOException thrown while flushing Flushable."
aload 0
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
L4:
return
L5:
astore 0
getstatic com/a/b/i/bk/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "IOException should not have been thrown."
aload 0
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 1
.limit stack 4
.end method
