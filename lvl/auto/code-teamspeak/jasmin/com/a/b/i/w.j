.bytecode 50.0
.class final synchronized com/a/b/i/w
.super com/a/b/i/s

.field private final 'a' Ljava/lang/Iterable;

.method <init>(Ljava/lang/Iterable;)V
aload 0
invokespecial com/a/b/i/s/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Iterable
putfield com/a/b/i/w/a Ljava/lang/Iterable;
return
.limit locals 2
.limit stack 2
.end method

.method public final a()Ljava/io/InputStream;
new com/a/b/i/cd
dup
aload 0
getfield com/a/b/i/w/a Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/i/cd/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final c()Z
aload 0
getfield com/a/b/i/w/a Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/i/s
invokevirtual com/a/b/i/s/c()Z
ifne L0
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 2
.limit stack 1
.end method

.method public final d()J
aload 0
getfield com/a/b/i/w/a Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 3
lconst_0
lstore 1
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/i/s
invokevirtual com/a/b/i/s/d()J
lload 1
ladd
lstore 1
goto L0
L1:
lload 1
lreturn
.limit locals 4
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/w/a Ljava/lang/Iterable;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 19
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "ByteSource.concat("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
