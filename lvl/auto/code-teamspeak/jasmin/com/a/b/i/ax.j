.bytecode 50.0
.class public final synchronized com/a/b/i/ax
.super java/io/OutputStream
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' I

.field private final 'b' Z

.field private final 'c' Lcom/a/b/i/s;

.field private 'd' Ljava/io/OutputStream;

.field private 'e' Lcom/a/b/i/ba;

.field private 'f' Ljava/io/File;

.method private <init>(I)V
aload 0
iload 1
iconst_0
invokespecial com/a/b/i/ax/<init>(IB)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(IB)V
aload 0
invokespecial java/io/OutputStream/<init>()V
aload 0
iload 1
putfield com/a/b/i/ax/a I
aload 0
iconst_0
putfield com/a/b/i/ax/b Z
aload 0
new com/a/b/i/ba
dup
iconst_0
invokespecial com/a/b/i/ba/<init>(B)V
putfield com/a/b/i/ax/e Lcom/a/b/i/ba;
aload 0
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
putfield com/a/b/i/ax/d Ljava/io/OutputStream;
aload 0
new com/a/b/i/az
dup
aload 0
invokespecial com/a/b/i/az/<init>(Lcom/a/b/i/ax;)V
putfield com/a/b/i/ax/c Lcom/a/b/i/s;
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/i/ax;)Ljava/io/InputStream;
aload 0
invokespecial com/a/b/i/ax/d()Ljava/io/InputStream;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(I)V
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
ifnonnull L0
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/b()I
iload 1
iadd
aload 0
getfield com/a/b/i/ax/a I
if_icmple L0
ldc "FileBackedOutputStream"
aconst_null
invokestatic java/io/File/createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
astore 2
aload 0
getfield com/a/b/i/ax/b Z
ifeq L1
aload 2
invokevirtual java/io/File/deleteOnExit()V
L1:
new java/io/FileOutputStream
dup
aload 2
invokespecial java/io/FileOutputStream/<init>(Ljava/io/File;)V
astore 3
aload 3
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/a()[B
iconst_0
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/b()I
invokevirtual java/io/FileOutputStream/write([BII)V
aload 3
invokevirtual java/io/FileOutputStream/flush()V
aload 0
aload 3
putfield com/a/b/i/ax/d Ljava/io/OutputStream;
aload 0
aload 2
putfield com/a/b/i/ax/f Ljava/io/File;
aload 0
aconst_null
putfield com/a/b/i/ax/e Lcom/a/b/i/ba;
L0:
return
.limit locals 4
.limit stack 4
.end method

.method private b()Ljava/io/File;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method private c()Lcom/a/b/i/s;
aload 0
getfield com/a/b/i/ax/c Lcom/a/b/i/s;
areturn
.limit locals 1
.limit stack 1
.end method

.method private d()Ljava/io/InputStream;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
ifnull L3
new java/io/FileInputStream
dup
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
invokespecial java/io/FileInputStream/<init>(Ljava/io/File;)V
astore 1
L1:
aload 0
monitorexit
aload 1
areturn
L3:
new java/io/ByteArrayInputStream
dup
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/a()[B
iconst_0
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/b()I
invokespecial java/io/ByteArrayInputStream/<init>([BII)V
astore 1
L4:
goto L1
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 5
.end method

.method public final a()V
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L4
.catch all from L3 to L4 using L4
.catch all from L5 to L6 using L4
.catch all from L7 to L8 using L4
.catch all from L8 to L9 using L4
.catch all from L9 to L10 using L4
.catch all from L11 to L12 using L4
aload 0
monitorenter
L0:
aload 0
invokevirtual com/a/b/i/ax/close()V
L1:
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
ifnonnull L5
aload 0
new com/a/b/i/ba
dup
iconst_0
invokespecial com/a/b/i/ba/<init>(B)V
putfield com/a/b/i/ax/e Lcom/a/b/i/ba;
L3:
aload 0
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
putfield com/a/b/i/ax/d Ljava/io/OutputStream;
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
ifnull L12
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
astore 1
aload 0
aconst_null
putfield com/a/b/i/ax/f Ljava/io/File;
aload 1
invokevirtual java/io/File/delete()Z
ifne L12
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/io/IOException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 18
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Could not delete: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L4:
astore 1
aload 0
monitorexit
aload 1
athrow
L5:
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/reset()V
L6:
goto L3
L2:
astore 1
L7:
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
ifnonnull L9
aload 0
new com/a/b/i/ba
dup
iconst_0
invokespecial com/a/b/i/ba/<init>(B)V
putfield com/a/b/i/ax/e Lcom/a/b/i/ba;
L8:
aload 0
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
putfield com/a/b/i/ax/d Ljava/io/OutputStream;
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
ifnull L11
aload 0
getfield com/a/b/i/ax/f Ljava/io/File;
astore 2
aload 0
aconst_null
putfield com/a/b/i/ax/f Ljava/io/File;
aload 2
invokevirtual java/io/File/delete()Z
ifne L11
aload 2
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/io/IOException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 18
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Could not delete: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L9:
aload 0
getfield com/a/b/i/ax/e Lcom/a/b/i/ba;
invokevirtual com/a/b/i/ba/reset()V
L10:
goto L8
L11:
aload 1
athrow
L12:
aload 0
monitorexit
return
.limit locals 3
.limit stack 6
.end method

.method public final close()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/ax/d Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/close()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public final flush()V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/ax/d Ljava/io/OutputStream;
invokevirtual java/io/OutputStream/flush()V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method public final write(I)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
iconst_1
invokespecial com/a/b/i/ax/a(I)V
aload 0
getfield com/a/b/i/ax/d Ljava/io/OutputStream;
iload 1
invokevirtual java/io/OutputStream/write(I)V
L1:
aload 0
monitorexit
return
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public final write([B)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
aload 1
iconst_0
aload 1
arraylength
invokevirtual com/a/b/i/ax/write([BII)V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 2
.limit stack 4
.end method

.method public final write([BII)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
iload 3
invokespecial com/a/b/i/ax/a(I)V
aload 0
getfield com/a/b/i/ax/d Ljava/io/OutputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/OutputStream/write([BII)V
L1:
aload 0
monitorexit
return
L2:
astore 1
aload 0
monitorexit
aload 1
athrow
.limit locals 4
.limit stack 4
.end method
