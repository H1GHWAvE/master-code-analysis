.bytecode 50.0
.class public final synchronized com/a/b/i/aq
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field static final 'a' Ljava/util/logging/Logger;
.annotation invisible Lcom/a/b/a/d;
.end annotation
.end field

.method static <clinit>()V
ldc com/a/b/i/aq
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/i/aq/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/io/Closeable;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/io/IOException from L0 to L1 using L2
aload 0
ifnonnull L0
return
L0:
aload 0
invokeinterface java/io/Closeable/close()V 0
L1:
return
L2:
astore 0
getstatic com/a/b/i/aq/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "IOException thrown while closing Closeable."
aload 0
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
return
.limit locals 1
.limit stack 4
.end method

.method private static a(Ljava/io/InputStream;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
invokestatic com/a/b/i/aq/a(Ljava/io/Closeable;)V
L1:
return
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/io/Reader;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.catch java/io/IOException from L0 to L1 using L2
L0:
aload 0
invokestatic com/a/b/i/aq/a(Ljava/io/Closeable;)V
L1:
return
L2:
astore 0
new java/lang/AssertionError
dup
aload 0
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method
