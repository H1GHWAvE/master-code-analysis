.bytecode 50.0
.class final synchronized com/a/b/i/g
.super com/a/b/b/m

.field final 's' Ljava/lang/String;

.field final 't' [C

.field final 'u' I

.field final 'v' I

.field final 'w' I

.field final 'x' I

.field final 'y' [B

.field private final 'z' [Z

.method <init>(Ljava/lang/String;[C)V
.catch java/lang/ArithmeticException from L0 to L1 using L2
iconst_0
istore 5
aload 0
invokespecial com/a/b/b/m/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/i/g/s Ljava/lang/String;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast [C
putfield com/a/b/i/g/t [C
L0:
aload 0
aload 2
arraylength
getstatic java/math/RoundingMode/UNNECESSARY Ljava/math/RoundingMode;
invokestatic com/a/b/j/g/a(ILjava/math/RoundingMode;)I
putfield com/a/b/i/g/v I
L1:
bipush 8
aload 0
getfield com/a/b/i/g/v I
invokestatic java/lang/Integer/lowestOneBit(I)I
invokestatic java/lang/Math/min(II)I
istore 4
aload 0
bipush 8
iload 4
idiv
putfield com/a/b/i/g/w I
aload 0
aload 0
getfield com/a/b/i/g/v I
iload 4
idiv
putfield com/a/b/i/g/x I
aload 0
aload 2
arraylength
iconst_1
isub
putfield com/a/b/i/g/u I
sipush 128
newarray byte
astore 1
aload 1
iconst_m1
invokestatic java/util/Arrays/fill([BB)V
iconst_0
istore 4
L3:
iload 4
aload 2
arraylength
if_icmpge L4
aload 2
iload 4
caload
istore 3
getstatic com/a/b/b/m/b Lcom/a/b/b/m;
iload 3
invokevirtual com/a/b/b/m/c(C)Z
ldc "Non-ASCII character: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 1
iload 3
baload
iconst_m1
if_icmpne L5
iconst_1
istore 6
L6:
iload 6
ldc "Duplicate character: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
iload 3
invokestatic java/lang/Character/valueOf(C)Ljava/lang/Character;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 1
iload 3
iload 4
i2b
bastore
iload 4
iconst_1
iadd
istore 4
goto L3
L2:
astore 1
aload 2
arraylength
istore 4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 35
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Illegal alphabet length "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 4
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;Ljava/lang/Throwable;)V
athrow
L5:
iconst_0
istore 6
goto L6
L4:
aload 0
aload 1
putfield com/a/b/i/g/y [B
aload 0
getfield com/a/b/i/g/w I
newarray boolean
astore 1
iload 5
istore 4
L7:
iload 4
aload 0
getfield com/a/b/i/g/x I
if_icmpge L8
aload 1
iload 4
bipush 8
imul
aload 0
getfield com/a/b/i/g/v I
getstatic java/math/RoundingMode/CEILING Ljava/math/RoundingMode;
invokestatic com/a/b/j/g/a(IILjava/math/RoundingMode;)I
iconst_1
bastore
iload 4
iconst_1
iadd
istore 4
goto L7
L8:
aload 0
aload 1
putfield com/a/b/i/g/z [Z
return
.limit locals 7
.limit stack 6
.end method

.method private b(I)C
aload 0
getfield com/a/b/i/g/t [C
iload 1
caload
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d(C)I
iload 1
bipush 127
if_icmpgt L0
aload 0
getfield com/a/b/i/g/y [B
iload 1
baload
iconst_m1
if_icmpne L1
L0:
new com/a/b/i/h
dup
new java/lang/StringBuilder
dup
bipush 25
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unrecognized character: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial com/a/b/i/h/<init>(Ljava/lang/String;)V
athrow
L1:
aload 0
getfield com/a/b/i/g/y [B
iload 1
baload
ireturn
.limit locals 2
.limit stack 5
.end method

.method private e()Lcom/a/b/i/g;
iconst_0
istore 1
aload 0
invokevirtual com/a/b/i/g/c()Z
ifne L0
aload 0
areturn
L0:
aload 0
invokevirtual com/a/b/i/g/d()Z
ifne L1
iconst_1
istore 2
L2:
iload 2
ldc "Cannot call upperCase() on a mixed-case alphabet"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/i/g/t [C
arraylength
newarray char
astore 3
L3:
iload 1
aload 0
getfield com/a/b/i/g/t [C
arraylength
if_icmpge L4
aload 3
iload 1
aload 0
getfield com/a/b/i/g/t [C
iload 1
caload
invokestatic com/a/b/b/e/b(C)C
castore
iload 1
iconst_1
iadd
istore 1
goto L3
L1:
iconst_0
istore 2
goto L2
L4:
new com/a/b/i/g
dup
aload 0
getfield com/a/b/i/g/s Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc ".upperCase()"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
aload 3
invokespecial com/a/b/i/g/<init>(Ljava/lang/String;[C)V
areturn
.limit locals 4
.limit stack 4
.end method

.method private f()Lcom/a/b/i/g;
iconst_0
istore 1
aload 0
invokevirtual com/a/b/i/g/d()Z
ifne L0
aload 0
areturn
L0:
aload 0
invokevirtual com/a/b/i/g/c()Z
ifne L1
iconst_1
istore 2
L2:
iload 2
ldc "Cannot call lowerCase() on a mixed-case alphabet"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/i/g/t [C
arraylength
newarray char
astore 3
L3:
iload 1
aload 0
getfield com/a/b/i/g/t [C
arraylength
if_icmpge L4
aload 3
iload 1
aload 0
getfield com/a/b/i/g/t [C
iload 1
caload
invokestatic com/a/b/b/e/a(C)C
castore
iload 1
iconst_1
iadd
istore 1
goto L3
L1:
iconst_0
istore 2
goto L2
L4:
new com/a/b/i/g
dup
aload 0
getfield com/a/b/i/g/s Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
ldc ".lowerCase()"
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
aload 3
invokespecial com/a/b/i/g/<init>(Ljava/lang/String;[C)V
areturn
.limit locals 4
.limit stack 4
.end method

.method final a(I)Z
aload 0
getfield com/a/b/i/g/z [Z
iload 1
aload 0
getfield com/a/b/i/g/w I
irem
baload
ireturn
.limit locals 2
.limit stack 3
.end method

.method final c()Z
iconst_0
istore 4
aload 0
getfield com/a/b/i/g/t [C
astore 5
aload 5
arraylength
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
iload 1
iload 2
if_icmpge L1
aload 5
iload 1
caload
invokestatic com/a/b/b/e/c(C)Z
ifeq L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 6
.limit stack 2
.end method

.method public final c(C)Z
getstatic com/a/b/b/m/b Lcom/a/b/b/m;
iload 1
invokevirtual com/a/b/b/m/c(C)Z
ifeq L0
aload 0
getfield com/a/b/i/g/y [B
iload 1
baload
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method final d()Z
iconst_0
istore 4
aload 0
getfield com/a/b/i/g/t [C
astore 5
aload 5
arraylength
istore 2
iconst_0
istore 1
L0:
iload 4
istore 3
iload 1
iload 2
if_icmpge L1
aload 5
iload 1
caload
invokestatic com/a/b/b/e/d(C)Z
ifeq L2
iconst_1
istore 3
L1:
iload 3
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
.limit locals 6
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/i/g/s Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
