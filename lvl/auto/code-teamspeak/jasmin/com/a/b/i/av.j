.bytecode 50.0
.class public final synchronized com/a/b/i/av
.super java/io/FilterInputStream
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private 'a' J

.field private 'b' J

.method private <init>(Ljava/io/InputStream;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial java/io/FilterInputStream/<init>(Ljava/io/InputStream;)V
aload 0
ldc2_w -1L
putfield com/a/b/i/av/b J
return
.limit locals 2
.limit stack 3
.end method

.method private a()J
aload 0
getfield com/a/b/i/av/a J
lreturn
.limit locals 1
.limit stack 2
.end method

.method public final mark(I)V
.catch all from L0 to L1 using L2
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/av/in Ljava/io/InputStream;
iload 1
invokevirtual java/io/InputStream/mark(I)V
aload 0
aload 0
getfield com/a/b/i/av/a J
putfield com/a/b/i/av/b J
L1:
aload 0
monitorexit
return
L2:
astore 2
aload 0
monitorexit
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final read()I
aload 0
getfield com/a/b/i/av/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/read()I
istore 1
iload 1
iconst_m1
if_icmpeq L0
aload 0
aload 0
getfield com/a/b/i/av/a J
lconst_1
ladd
putfield com/a/b/i/av/a J
L0:
iload 1
ireturn
.limit locals 2
.limit stack 5
.end method

.method public final read([BII)I
aload 0
getfield com/a/b/i/av/in Ljava/io/InputStream;
aload 1
iload 2
iload 3
invokevirtual java/io/InputStream/read([BII)I
istore 2
iload 2
iconst_m1
if_icmpeq L0
aload 0
aload 0
getfield com/a/b/i/av/a J
iload 2
i2l
ladd
putfield com/a/b/i/av/a J
L0:
iload 2
ireturn
.limit locals 4
.limit stack 5
.end method

.method public final reset()V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
.catch all from L3 to L4 using L1
aload 0
monitorenter
L0:
aload 0
getfield com/a/b/i/av/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/markSupported()Z
ifne L2
new java/io/IOException
dup
ldc "Mark not supported"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 1
aload 0
monitorexit
aload 1
athrow
L2:
aload 0
getfield com/a/b/i/av/b J
ldc2_w -1L
lcmp
ifne L3
new java/io/IOException
dup
ldc "Mark not set"
invokespecial java/io/IOException/<init>(Ljava/lang/String;)V
athrow
L3:
aload 0
getfield com/a/b/i/av/in Ljava/io/InputStream;
invokevirtual java/io/InputStream/reset()V
aload 0
aload 0
getfield com/a/b/i/av/b J
putfield com/a/b/i/av/a J
L4:
aload 0
monitorexit
return
.limit locals 2
.limit stack 4
.end method

.method public final skip(J)J
aload 0
getfield com/a/b/i/av/in Ljava/io/InputStream;
lload 1
invokevirtual java/io/InputStream/skip(J)J
lstore 1
aload 0
aload 0
getfield com/a/b/i/av/a J
lload 1
ladd
putfield com/a/b/i/av/a J
lload 1
lreturn
.limit locals 3
.limit stack 5
.end method
