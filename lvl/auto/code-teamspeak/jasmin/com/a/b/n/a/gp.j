.bytecode 50.0
.class final synchronized com/a/b/n/a/gp
.super java/lang/Object
.implements java/lang/Thread$UncaughtExceptionHandler
.annotation invisible Lcom/a/b/a/d;
.end annotation

.field private static final 'a' Ljava/util/logging/Logger;

.field private final 'b' Ljava/lang/Runtime;

.method static <clinit>()V
ldc com/a/b/n/a/gp
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/n/a/gp/a Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 1
.end method

.method <init>(Ljava/lang/Runtime;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/n/a/gp/b Ljava/lang/Runtime;
return
.limit locals 2
.limit stack 2
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
.catch java/lang/Throwable from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L5 using L3
L0:
getstatic com/a/b/n/a/gp/a Ljava/util/logging/Logger;
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
ldc "Caught an exception in %s.  Shutting down."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
aload 2
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
L1:
aload 0
getfield com/a/b/n/a/gp/b Ljava/lang/Runtime;
iconst_1
invokevirtual java/lang/Runtime/exit(I)V
return
L2:
astore 1
L4:
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 2
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
getstatic java/lang/System/err Ljava/io/PrintStream;
aload 1
invokevirtual java/lang/Throwable/getMessage()Ljava/lang/String;
invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V
L5:
aload 0
getfield com/a/b/n/a/gp/b Ljava/lang/Runtime;
iconst_1
invokevirtual java/lang/Runtime/exit(I)V
return
L3:
astore 1
aload 0
getfield com/a/b/n/a/gp/b Ljava/lang/Runtime;
iconst_1
invokevirtual java/lang/Runtime/exit(I)V
aload 1
athrow
.limit locals 3
.limit stack 7
.end method
