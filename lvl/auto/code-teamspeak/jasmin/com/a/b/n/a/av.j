.bytecode 50.0
.class public final synchronized com/a/b/n/a/av
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final 'a' Ljava/util/concurrent/ConcurrentHashMap;

.field private transient 'b' Ljava/util/Map;

.method private <init>(Ljava/util/concurrent/ConcurrentHashMap;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/ConcurrentHashMap
putfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
return
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)J
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 1
aload 1
ifnonnull L0
lconst_0
lreturn
L0:
aload 1
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lreturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/Object;J)J
L0:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 9
aload 9
astore 8
aload 9
ifnonnull L1
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 9
aload 9
astore 8
aload 9
ifnonnull L1
lload 2
lreturn
L1:
aload 8
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 4
lload 4
lconst_0
lcmp
ifne L2
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 8
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
lload 2
lreturn
L2:
lload 4
lload 2
ladd
lstore 6
aload 8
lload 4
lload 6
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ifeq L1
lload 6
lreturn
.limit locals 10
.limit stack 7
.end method

.method private static a()Lcom/a/b/n/a/av;
new com/a/b/n/a/av
dup
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
invokespecial com/a/b/n/a/av/<init>(Ljava/util/concurrent/ConcurrentHashMap;)V
areturn
.limit locals 0
.limit stack 4
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/n/a/av;
new com/a/b/n/a/av
dup
new java/util/concurrent/ConcurrentHashMap
dup
invokespecial java/util/concurrent/ConcurrentHashMap/<init>()V
invokespecial com/a/b/n/a/av/<init>(Ljava/util/concurrent/ConcurrentHashMap;)V
astore 1
aload 1
aload 0
invokespecial com/a/b/n/a/av/b(Ljava/util/Map;)V
aload 1
areturn
.limit locals 2
.limit stack 4
.end method

.method private a(Ljava/lang/Object;JJ)Z
lload 2
lconst_0
lcmp
ifne L0
L1:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 9
aload 9
astore 8
aload 9
ifnonnull L2
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
new java/util/concurrent/atomic/AtomicLong
dup
lload 4
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 9
aload 9
astore 8
aload 9
ifnonnull L2
lconst_0
lstore 2
L3:
lload 2
lconst_0
lcmp
ifne L4
iconst_1
ireturn
L2:
aload 8
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 6
lload 6
lstore 2
lload 6
lconst_0
lcmp
ifne L3
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 8
new java/util/concurrent/atomic/AtomicLong
dup
lload 4
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L1
lconst_0
lstore 2
goto L3
L4:
iconst_0
ireturn
L0:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 1
aload 1
ifnonnull L5
iconst_0
ireturn
L5:
aload 1
lload 2
lload 4
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ireturn
.limit locals 10
.limit stack 7
.end method

.method private b(Ljava/lang/Object;)J
aload 0
aload 1
lconst_1
invokespecial com/a/b/n/a/av/a(Ljava/lang/Object;J)J
lreturn
.limit locals 2
.limit stack 4
.end method

.method private b(Ljava/lang/Object;J)J
L0:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 7
aload 7
astore 6
aload 7
ifnonnull L1
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 7
aload 7
astore 6
aload 7
ifnonnull L1
lconst_0
lreturn
L1:
aload 6
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 4
lload 4
lconst_0
lcmp
ifne L2
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 6
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
lconst_0
lreturn
L2:
aload 6
lload 4
lload 4
lload 2
ladd
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ifeq L1
lload 4
lreturn
.limit locals 8
.limit stack 7
.end method

.method private b()V
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/keySet()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 2
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 3
aload 3
ifnull L0
aload 3
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lconst_0
lcmp
ifne L0
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 2
aload 3
invokevirtual java/util/concurrent/ConcurrentHashMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z
pop
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method private b(Ljava/util/Map;)V
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 7
L0:
aload 7
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 7
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 1
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
astore 8
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lstore 2
L2:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 8
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 6
aload 6
astore 1
aload 6
ifnonnull L3
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 8
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 1
aload 1
ifnull L0
L3:
aload 1
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 4
lload 4
lconst_0
lcmp
ifne L4
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 8
aload 1
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L2
goto L0
L4:
aload 1
lload 4
lload 2
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ifeq L3
goto L0
L1:
return
.limit locals 9
.limit stack 7
.end method

.method private c()J
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/values()Ljava/util/Collection;
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
lconst_0
lstore 1
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/concurrent/atomic/AtomicLong
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lload 1
ladd
lstore 1
goto L0
L1:
lload 1
lreturn
.limit locals 4
.limit stack 4
.end method

.method private c(Ljava/lang/Object;)J
aload 0
aload 1
ldc2_w -1L
invokespecial com/a/b/n/a/av/a(Ljava/lang/Object;J)J
lreturn
.limit locals 2
.limit stack 4
.end method

.method private c(Ljava/lang/Object;J)J
L0:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 7
aload 7
astore 6
aload 7
ifnonnull L1
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 7
aload 7
astore 6
aload 7
ifnonnull L1
lconst_0
lreturn
L1:
aload 6
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 4
lload 4
lconst_0
lcmp
ifne L2
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 6
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
lconst_0
lreturn
L2:
aload 6
lload 4
lload 2
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ifeq L1
lload 4
lreturn
.limit locals 8
.limit stack 7
.end method

.method private d(Ljava/lang/Object;)J
aload 0
aload 1
lconst_1
invokespecial com/a/b/n/a/av/b(Ljava/lang/Object;J)J
lreturn
.limit locals 2
.limit stack 4
.end method

.method private d(Ljava/lang/Object;J)J
L0:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 7
aload 7
astore 6
aload 7
ifnonnull L1
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 7
aload 7
astore 6
aload 7
ifnonnull L1
lconst_0
lreturn
L1:
aload 6
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 4
lload 4
lconst_0
lcmp
ifne L2
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 6
new java/util/concurrent/atomic/AtomicLong
dup
lload 2
invokespecial java/util/concurrent/atomic/AtomicLong/<init>(J)V
invokevirtual java/util/concurrent/ConcurrentHashMap/replace(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
lconst_0
lreturn
L2:
lload 4
lreturn
.limit locals 8
.limit stack 7
.end method

.method private d()Ljava/util/Map;
aload 0
getfield com/a/b/n/a/av/b Ljava/util/Map;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
new com/a/b/n/a/aw
dup
aload 0
invokespecial com/a/b/n/a/aw/<init>(Lcom/a/b/n/a/av;)V
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
astore 1
aload 0
aload 1
putfield com/a/b/n/a/av/b Ljava/util/Map;
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method private e(Ljava/lang/Object;)J
aload 0
aload 1
ldc2_w -1L
invokespecial com/a/b/n/a/av/b(Ljava/lang/Object;J)J
lreturn
.limit locals 2
.limit stack 4
.end method

.method private e()Ljava/util/Map;
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
new com/a/b/n/a/aw
dup
aload 0
invokespecial com/a/b/n/a/aw/<init>(Lcom/a/b/n/a/av;)V
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Lcom/a/b/b/bj;)Ljava/util/Map;
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
areturn
.limit locals 1
.limit stack 4
.end method

.method private e(Ljava/lang/Object;J)Z
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 6
aload 6
ifnonnull L0
iconst_0
ireturn
L0:
aload 6
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 4
lload 4
lload 2
lcmp
ifeq L1
iconst_0
ireturn
L1:
lload 4
lconst_0
lcmp
ifeq L2
aload 6
lload 4
lconst_0
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ifeq L3
L2:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 6
invokevirtual java/util/concurrent/ConcurrentHashMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z
pop
iconst_1
ireturn
L3:
iconst_0
ireturn
.limit locals 7
.limit stack 5
.end method

.method private f()I
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f(Ljava/lang/Object;)J
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/get(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/concurrent/atomic/AtomicLong
astore 4
aload 4
ifnonnull L0
lconst_0
lreturn
L0:
aload 4
invokevirtual java/util/concurrent/atomic/AtomicLong/get()J
lstore 2
lload 2
lconst_0
lcmp
ifeq L1
aload 4
lload 2
lconst_0
invokevirtual java/util/concurrent/atomic/AtomicLong/compareAndSet(JJ)Z
ifeq L0
L1:
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
aload 4
invokevirtual java/util/concurrent/ConcurrentHashMap/remove(Ljava/lang/Object;Ljava/lang/Object;)Z
pop
lload 2
lreturn
.limit locals 5
.limit stack 5
.end method

.method private g()Z
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/isEmpty()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
aload 1
invokevirtual java/util/concurrent/ConcurrentHashMap/containsKey(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private h()V
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/n/a/av/a Ljava/util/concurrent/ConcurrentHashMap;
invokevirtual java/util/concurrent/ConcurrentHashMap/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
