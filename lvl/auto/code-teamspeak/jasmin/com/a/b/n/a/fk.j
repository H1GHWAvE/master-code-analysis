.bytecode 50.0
.class final synchronized com/a/b/n/a/fk
.super java/lang/Object

.field final 'a' Lcom/a/b/n/a/dw;

.field final 'b' Lcom/a/b/d/aac;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
.end field

.field final 'c' Lcom/a/b/d/xc;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
.end field

.field final 'd' Ljava/util/Map;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
.end field

.field 'e' Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
.end field

.field 'f' Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
.end field

.field final 'g' I

.field final 'h' Lcom/a/b/n/a/dx;

.field final 'i' Lcom/a/b/n/a/dx;

.field final 'j' Ljava/util/List;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
.end field

.method <init>(Lcom/a/b/d/iz;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/a/b/n/a/dw
dup
invokespecial com/a/b/n/a/dw/<init>()V
putfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 0
new java/util/EnumMap
dup
ldc com/a/b/n/a/ew
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
new com/a/b/n/a/fl
dup
aload 0
invokespecial com/a/b/n/a/fl/<init>(Lcom/a/b/n/a/fk;)V
invokestatic com/a/b/d/we/b(Ljava/util/Map;Lcom/a/b/b/dz;)Lcom/a/b/d/aac;
putfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
aload 0
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
invokeinterface com/a/b/d/aac/q()Lcom/a/b/d/xc; 0
putfield com/a/b/n/a/fk/c Lcom/a/b/d/xc;
aload 0
invokestatic com/a/b/d/sz/f()Ljava/util/IdentityHashMap;
putfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 0
new com/a/b/n/a/fm
dup
aload 0
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokespecial com/a/b/n/a/fm/<init>(Lcom/a/b/n/a/fk;Lcom/a/b/n/a/dw;)V
putfield com/a/b/n/a/fk/h Lcom/a/b/n/a/dx;
aload 0
new com/a/b/n/a/fn
dup
aload 0
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokespecial com/a/b/n/a/fn/<init>(Lcom/a/b/n/a/fk;Lcom/a/b/n/a/dw;)V
putfield com/a/b/n/a/fk/i Lcom/a/b/n/a/dx;
aload 0
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
invokestatic java/util/Collections/synchronizedList(Ljava/util/List;)Ljava/util/List;
putfield com/a/b/n/a/fk/j Ljava/util/List;
aload 0
aload 1
invokevirtual com/a/b/d/iz/size()I
putfield com/a/b/n/a/fk/g I
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
aload 1
invokeinterface com/a/b/d/aac/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z 2
pop
return
.limit locals 2
.limit stack 5
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)V
.catch all from L0 to L1 using L1
.catch all from L2 to L3 using L1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 0
getfield com/a/b/n/a/fk/h Lcom/a/b/n/a/dx;
lload 1
aload 3
invokevirtual com/a/b/n/a/dw/b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
ifne L2
ldc "Timeout waiting for the services to become healthy. The following services have not started: "
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/b Lcom/a/b/n/a/ew;
invokestatic com/a/b/d/lo/b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/d/we/a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 4
new java/util/concurrent/TimeoutException
dup
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
iconst_0
iadd
aload 4
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/util/concurrent/TimeoutException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 3
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 3
athrow
L2:
aload 0
invokevirtual com/a/b/n/a/fk/d()V
L3:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
.limit locals 5
.limit stack 6
.end method

.method private a(Lcom/a/b/n/a/et;)V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/b/dw
ifnonnull L1
aload 0
getfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 1
invokestatic com/a/b/b/dw/a()Lcom/a/b/b/dw;
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
.limit locals 2
.limit stack 3
.end method

.method private b(JLjava/util/concurrent/TimeUnit;)V
.catch all from L0 to L1 using L1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 0
getfield com/a/b/n/a/fk/i Lcom/a/b/n/a/dx;
lload 1
aload 3
invokevirtual com/a/b/n/a/dw/b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
ifne L2
ldc "Timeout waiting for the services to stop. The following services have not stopped: "
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 3
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
getstatic com/a/b/n/a/ew/e Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
invokestatic com/a/b/d/lo/b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
invokestatic com/a/b/b/cp/a(Ljava/util/Collection;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/we/a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 4
new java/util/concurrent/TimeoutException
dup
new java/lang/StringBuilder
dup
aload 3
invokevirtual java/lang/String/length()I
iconst_0
iadd
aload 4
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 3
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 4
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/util/concurrent/TimeoutException/<init>(Ljava/lang/String;)V
athrow
L1:
astore 3
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 3
athrow
L2:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
.limit locals 5
.limit stack 6
.end method

.method private b(Lcom/a/b/n/a/et;)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new com/a/b/n/a/fp
dup
aload 0
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 18
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "failed({service="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "})"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial com/a/b/n/a/fp/<init>(Lcom/a/b/n/a/fk;Ljava/lang/String;Lcom/a/b/n/a/et;)V
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokevirtual com/a/b/n/a/fp/a(Ljava/lang/Iterable;)V
return
.limit locals 3
.limit stack 7
.end method

.method private e()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 0
getfield com/a/b/n/a/fk/h Lcom/a/b/n/a/dx;
invokevirtual com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;)V
L0:
aload 0
invokevirtual com/a/b/n/a/fk/d()V
L1:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method private f()V
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
aload 0
getfield com/a/b/n/a/fk/i Lcom/a/b/n/a/dx;
invokevirtual com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;)V
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
.limit locals 1
.limit stack 2
.end method

.method private g()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
invokestatic com/a/b/n/a/fd/b()Lcom/a/b/n/a/dt;
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokevirtual com/a/b/n/a/dt/a(Ljava/lang/Iterable;)V
return
.limit locals 1
.limit stack 2
.end method

.method private h()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
invokestatic com/a/b/n/a/fd/c()Lcom/a/b/n/a/dt;
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokevirtual com/a/b/n/a/dt/a(Ljava/lang/Iterable;)V
return
.limit locals 1
.limit stack 2
.end method

.method private i()V
iconst_0
istore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
ifne L0
iconst_1
istore 2
L1:
iload 2
ldc "It is incorrect to execute listeners with the monitor held."
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
L2:
iload 1
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokeinterface java/util/List/size()I 0
if_icmpge L3
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast com/a/b/n/a/ds
invokevirtual com/a/b/n/a/ds/a()V
iload 1
iconst_1
iadd
istore 1
goto L2
L0:
iconst_0
istore 2
goto L1
L3:
return
.limit locals 3
.limit stack 2
.end method

.method final a()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/f Z
ifne L3
aload 0
iconst_1
putfield com/a/b/n/a/fk/e Z
L1:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
L3:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 1
aload 0
invokevirtual com/a/b/n/a/fk/b()Lcom/a/b/d/kk;
invokevirtual com/a/b/d/kk/x()Lcom/a/b/d/iz;
invokevirtual com/a/b/d/iz/iterator()Ljava/util/Iterator;
astore 2
L4:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
astore 3
aload 3
invokeinterface com/a/b/n/a/et/f()Lcom/a/b/n/a/ew; 0
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
if_acmpeq L4
aload 1
aload 3
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L5:
goto L4
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
L6:
ldc "Services started transitioning asynchronously before the ServiceManager was constructed: "
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
iconst_0
iadd
aload 1
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L7:
.limit locals 4
.limit stack 6
.end method

.method final a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L7 to L8 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
iconst_1
istore 4
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
aload 3
if_acmpeq L12
L13:
iload 4
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
iconst_1
putfield com/a/b/n/a/fk/f Z
aload 0
getfield com/a/b/n/a/fk/e Z
istore 4
L1:
iload 4
ifne L3
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 0
invokespecial com/a/b/n/a/fk/i()V
return
L12:
iconst_0
istore 4
goto L13
L3:
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
aload 2
aload 1
invokeinterface com/a/b/d/aac/c(Ljava/lang/Object;Ljava/lang/Object;)Z 2
ldc "Service %s not at the expected location in the state map %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
aload 3
aload 1
invokeinterface com/a/b/d/aac/a(Ljava/lang/Object;Ljava/lang/Object;)Z 2
ldc "Service %s in the state map unexpectedly at %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/b/dw
astore 5
L4:
aload 5
astore 2
aload 5
ifnonnull L6
L5:
invokestatic com/a/b/b/dw/a()Lcom/a/b/b/dw;
astore 2
aload 0
getfield com/a/b/n/a/fk/d Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L6:
aload 3
getstatic com/a/b/n/a/ew/c Lcom/a/b/n/a/ew;
invokevirtual com/a/b/n/a/ew/compareTo(Ljava/lang/Enum;)I
iflt L7
aload 2
getfield com/a/b/b/dw/a Z
ifeq L7
aload 2
invokevirtual com/a/b/b/dw/c()Lcom/a/b/b/dw;
pop
aload 1
instanceof com/a/b/n/a/fi
ifne L7
invokestatic com/a/b/n/a/fd/a()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/FINE Ljava/util/logging/Level;
ldc "Started {0} in {1}."
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 2
aastore
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V
L7:
aload 3
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
if_acmpne L8
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new com/a/b/n/a/fp
dup
aload 0
new java/lang/StringBuilder
dup
aload 2
invokevirtual java/lang/String/length()I
bipush 18
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "failed({service="
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "})"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 1
invokespecial com/a/b/n/a/fp/<init>(Lcom/a/b/n/a/fk;Ljava/lang/String;Lcom/a/b/n/a/et;)V
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokevirtual com/a/b/n/a/fp/a(Ljava/lang/Iterable;)V
L8:
aload 0
getfield com/a/b/n/a/fk/c Lcom/a/b/d/xc;
getstatic com/a/b/n/a/ew/c Lcom/a/b/n/a/ew;
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
aload 0
getfield com/a/b/n/a/fk/g I
if_icmpne L10
invokestatic com/a/b/n/a/fd/c()Lcom/a/b/n/a/dt;
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokevirtual com/a/b/n/a/dt/a(Ljava/lang/Iterable;)V
L9:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 0
invokespecial com/a/b/n/a/fk/i()V
return
L10:
aload 0
getfield com/a/b/n/a/fk/c Lcom/a/b/d/xc;
getstatic com/a/b/n/a/ew/e Lcom/a/b/n/a/ew;
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
aload 0
getfield com/a/b/n/a/fk/c Lcom/a/b/d/xc;
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
iadd
aload 0
getfield com/a/b/n/a/fk/g I
if_icmpne L9
invokestatic com/a/b/n/a/fd/b()Lcom/a/b/n/a/dt;
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
invokevirtual com/a/b/n/a/dt/a(Ljava/lang/Iterable;)V
L11:
goto L9
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 0
invokespecial com/a/b/n/a/fk/i()V
aload 1
athrow
.limit locals 6
.limit stack 7
.end method

.method final a(Lcom/a/b/n/a/fh;Ljava/util/concurrent/Executor;)V
.catch all from L0 to L1 using L2
aload 1
ldc "listener"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
ldc "executor"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/i Lcom/a/b/n/a/dx;
invokevirtual com/a/b/n/a/dx/a()Z
ifne L1
aload 0
getfield com/a/b/n/a/fk/j Ljava/util/List;
new com/a/b/n/a/ds
dup
aload 1
aload 2
invokespecial com/a/b/n/a/ds/<init>(Ljava/lang/Object;Ljava/util/concurrent/Executor;)V
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L1:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
return
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
.limit locals 3
.limit stack 5
.end method

.method final b()Lcom/a/b/d/kk;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
invokestatic com/a/b/d/lr/c()Lcom/a/b/d/ls;
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
invokeinterface com/a/b/d/aac/u()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
instanceof com/a/b/n/a/fi
ifne L1
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual com/a/b/d/ls/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ls;
pop
L3:
goto L1
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
L4:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
invokevirtual com/a/b/d/ls/a()Lcom/a/b/d/lr;
areturn
.limit locals 4
.limit stack 3
.end method

.method final c()Lcom/a/b/d/jt;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 0
getfield com/a/b/n/a/fk/d Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
invokestatic com/a/b/d/ov/a(I)Ljava/util/ArrayList;
astore 1
aload 0
getfield com/a/b/n/a/fk/d Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L1:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 4
aload 4
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast com/a/b/n/a/et
astore 3
aload 4
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/b/dw
astore 4
aload 4
getfield com/a/b/b/dw/a Z
ifne L1
aload 3
instanceof com/a/b/n/a/fi
ifne L1
aload 1
aload 3
aload 4
getstatic java/util/concurrent/TimeUnit/MILLISECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/b/dw/a(Ljava/util/concurrent/TimeUnit;)J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
L3:
goto L1
L2:
astore 1
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
L4:
aload 0
getfield com/a/b/n/a/fk/a Lcom/a/b/n/a/dw;
invokevirtual com/a/b/n/a/dw/a()V
aload 1
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
new com/a/b/n/a/fo
dup
aload 0
invokespecial com/a/b/n/a/fo/<init>(Lcom/a/b/n/a/fk;)V
invokevirtual com/a/b/d/yd/a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 2
aload 1
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 1
L5:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L6
aload 2
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
invokevirtual com/a/b/d/ju/a(Ljava/util/Map$Entry;)Lcom/a/b/d/ju;
pop
goto L5
L6:
aload 2
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
areturn
.limit locals 5
.limit stack 5
.end method

.method final d()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "monitor"
.end annotation
aload 0
getfield com/a/b/n/a/fk/c Lcom/a/b/d/xc;
getstatic com/a/b/n/a/ew/c Lcom/a/b/n/a/ew;
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
aload 0
getfield com/a/b/n/a/fk/g I
if_icmpeq L0
aload 0
getfield com/a/b/n/a/fk/b Lcom/a/b/d/aac;
getstatic com/a/b/n/a/ew/c Lcom/a/b/n/a/ew;
invokestatic com/a/b/b/cp/a(Ljava/lang/Object;)Lcom/a/b/b/co;
invokestatic com/a/b/b/cp/a(Lcom/a/b/b/co;)Lcom/a/b/b/co;
invokestatic com/a/b/d/we/a(Lcom/a/b/d/aac;Lcom/a/b/b/co;)Lcom/a/b/d/aac;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/IllegalStateException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 79
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Expected to be healthy after starting. The following services are not running: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
athrow
L0:
return
.limit locals 2
.limit stack 6
.end method
