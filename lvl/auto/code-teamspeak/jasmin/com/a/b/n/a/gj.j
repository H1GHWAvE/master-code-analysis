.bytecode 50.0
.class final synchronized com/a/b/n/a/gj
.super com/a/b/n/a/gi
.annotation invisible Lcom/a/b/a/d;
.end annotation

.field final 'a' Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final 'b' Lcom/a/b/b/dz;

.field final 'c' I

.field final 'e' Ljava/lang/ref/ReferenceQueue;

.method <init>(ILcom/a/b/b/dz;)V
aload 0
iload 1
invokespecial com/a/b/n/a/gi/<init>(I)V
aload 0
new java/lang/ref/ReferenceQueue
dup
invokespecial java/lang/ref/ReferenceQueue/<init>()V
putfield com/a/b/n/a/gj/e Ljava/lang/ref/ReferenceQueue;
aload 0
getfield com/a/b/n/a/gj/d I
iconst_m1
if_icmpne L0
ldc_w 2147483647
istore 1
L1:
aload 0
iload 1
putfield com/a/b/n/a/gj/c I
aload 0
new java/util/concurrent/atomic/AtomicReferenceArray
dup
aload 0
getfield com/a/b/n/a/gj/c I
invokespecial java/util/concurrent/atomic/AtomicReferenceArray/<init>(I)V
putfield com/a/b/n/a/gj/a Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 0
aload 2
putfield com/a/b/n/a/gj/b Lcom/a/b/b/dz;
return
L0:
aload 0
getfield com/a/b/n/a/gj/d I
iconst_1
iadd
istore 1
goto L1
.limit locals 3
.limit stack 4
.end method

.method private b()V
L0:
aload 0
getfield com/a/b/n/a/gj/e Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 1
aload 1
ifnull L1
aload 1
checkcast com/a/b/n/a/gk
astore 1
aload 0
getfield com/a/b/n/a/gj/a Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 1
getfield com/a/b/n/a/gk/a I
aload 1
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/compareAndSet(ILjava/lang/Object;Ljava/lang/Object;)Z
pop
goto L0
L1:
return
.limit locals 2
.limit stack 4
.end method

.method public final a()I
aload 0
getfield com/a/b/n/a/gj/c I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(I)Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/gj/c I
ldc_w 2147483647
if_icmpeq L0
iload 1
aload 0
getfield com/a/b/n/a/gj/c I
invokestatic com/a/b/b/cn/a(II)I
pop
L0:
aload 0
getfield com/a/b/n/a/gj/a Ljava/util/concurrent/atomic/AtomicReferenceArray;
iload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/n/a/gk
astore 3
aload 3
ifnonnull L1
aconst_null
astore 2
L2:
aload 2
ifnull L3
aload 2
areturn
L1:
aload 3
invokevirtual com/a/b/n/a/gk/get()Ljava/lang/Object;
astore 2
goto L2
L3:
aload 0
getfield com/a/b/n/a/gj/b Lcom/a/b/b/dz;
invokeinterface com/a/b/b/dz/a()Ljava/lang/Object; 0
astore 4
new com/a/b/n/a/gk
dup
aload 4
iload 1
aload 0
getfield com/a/b/n/a/gj/e Ljava/lang/ref/ReferenceQueue;
invokespecial com/a/b/n/a/gk/<init>(Ljava/lang/Object;ILjava/lang/ref/ReferenceQueue;)V
astore 5
L4:
aload 0
getfield com/a/b/n/a/gj/a Ljava/util/concurrent/atomic/AtomicReferenceArray;
iload 1
aload 3
aload 5
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/compareAndSet(ILjava/lang/Object;Ljava/lang/Object;)Z
ifne L5
aload 0
getfield com/a/b/n/a/gj/a Ljava/util/concurrent/atomic/AtomicReferenceArray;
iload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/n/a/gk
astore 3
aload 3
ifnonnull L6
aconst_null
astore 2
L7:
aload 2
ifnull L4
aload 2
areturn
L6:
aload 3
invokevirtual com/a/b/n/a/gk/get()Ljava/lang/Object;
astore 2
goto L7
L5:
aload 0
getfield com/a/b/n/a/gj/e Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 2
aload 2
ifnull L8
aload 2
checkcast com/a/b/n/a/gk
astore 2
aload 0
getfield com/a/b/n/a/gj/a Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 2
getfield com/a/b/n/a/gk/a I
aload 2
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/compareAndSet(ILjava/lang/Object;Ljava/lang/Object;)Z
pop
goto L5
L8:
aload 4
areturn
.limit locals 6
.limit stack 5
.end method
