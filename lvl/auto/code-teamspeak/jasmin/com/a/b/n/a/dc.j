.bytecode 50.0
.class final synchronized com/a/b/n/a/dc
.super com/a/b/n/a/df

.field private final 'a' Ljava/util/concurrent/CancellationException;

.method <init>()V
aload 0
iconst_0
invokespecial com/a/b/n/a/df/<init>(B)V
aload 0
new java/util/concurrent/CancellationException
dup
ldc "Immediate cancelled future."
invokespecial java/util/concurrent/CancellationException/<init>(Ljava/lang/String;)V
putfield com/a/b/n/a/dc/a Ljava/util/concurrent/CancellationException;
return
.limit locals 1
.limit stack 4
.end method

.method public final get()Ljava/lang/Object;
ldc "Task was cancelled."
aload 0
getfield com/a/b/n/a/dc/a Ljava/util/concurrent/CancellationException;
invokestatic com/a/b/n/a/g/a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;
athrow
.limit locals 1
.limit stack 2
.end method

.method public final isCancelled()Z
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method
