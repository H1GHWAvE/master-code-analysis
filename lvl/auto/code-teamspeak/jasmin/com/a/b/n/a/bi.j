.bytecode 50.0
.class final synchronized com/a/b/n/a/bi
.super java/util/concurrent/locks/ReentrantReadWriteLock
.implements com/a/b/n/a/bf

.field final synthetic 'a' Lcom/a/b/n/a/bd;

.field private final 'b' Lcom/a/b/n/a/bh;

.field private final 'c' Lcom/a/b/n/a/bj;

.field private final 'd' Lcom/a/b/n/a/bl;

.method private <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V
aload 0
aload 1
putfield com/a/b/n/a/bi/a Lcom/a/b/n/a/bd;
aload 0
iconst_0
invokespecial java/util/concurrent/locks/ReentrantReadWriteLock/<init>(Z)V
aload 0
new com/a/b/n/a/bh
dup
aload 1
aload 0
invokespecial com/a/b/n/a/bh/<init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bi;)V
putfield com/a/b/n/a/bi/b Lcom/a/b/n/a/bh;
aload 0
new com/a/b/n/a/bj
dup
aload 1
aload 0
invokespecial com/a/b/n/a/bj/<init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bi;)V
putfield com/a/b/n/a/bi/c Lcom/a/b/n/a/bj;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/n/a/bl
putfield com/a/b/n/a/bi/d Lcom/a/b/n/a/bl;
return
.limit locals 3
.limit stack 5
.end method

.method synthetic <init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/n/a/bi/<init>(Lcom/a/b/n/a/bd;Lcom/a/b/n/a/bl;)V
return
.limit locals 4
.limit stack 3
.end method

.method public final a()Lcom/a/b/n/a/bl;
aload 0
getfield com/a/b/n/a/bi/d Lcom/a/b/n/a/bl;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final b()Z
aload 0
invokevirtual com/a/b/n/a/bi/isWriteLockedByCurrentThread()Z
ifne L0
aload 0
invokevirtual com/a/b/n/a/bi/getReadHoldCount()I
ifle L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic readLock()Ljava/util/concurrent/locks/Lock;
aload 0
invokevirtual com/a/b/n/a/bi/readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;
aload 0
getfield com/a/b/n/a/bi/b Lcom/a/b/n/a/bh;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic writeLock()Ljava/util/concurrent/locks/Lock;
aload 0
invokevirtual com/a/b/n/a/bi/writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final writeLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$WriteLock;
aload 0
getfield com/a/b/n/a/bi/c Lcom/a/b/n/a/bj;
areturn
.limit locals 1
.limit stack 1
.end method
