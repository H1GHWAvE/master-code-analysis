.bytecode 50.0
.class public synchronized abstract com/a/b/n/a/cb
.super com/a/b/d/hg
.implements java/util/concurrent/Future

.method protected <init>()V
aload 0
invokespecial com/a/b/d/hg/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method protected abstract b()Ljava/util/concurrent/Future;
.end method

.method public cancel(Z)Z
aload 0
invokevirtual com/a/b/n/a/cb/b()Ljava/util/concurrent/Future;
iload 1
invokeinterface java/util/concurrent/Future/cancel(Z)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public get()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/cb/b()Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/get()Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/cb/b()Ljava/util/concurrent/Future;
lload 1
aload 3
invokeinterface java/util/concurrent/Future/get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object; 3
areturn
.limit locals 4
.limit stack 4
.end method

.method public isCancelled()Z
aload 0
invokevirtual com/a/b/n/a/cb/b()Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/isCancelled()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isDone()Z
aload 0
invokevirtual com/a/b/n/a/cb/b()Ljava/util/concurrent/Future;
invokeinterface java/util/concurrent/Future/isDone()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method protected synthetic k_()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/n/a/cb/b()Ljava/util/concurrent/Future;
areturn
.limit locals 1
.limit stack 1
.end method
