.bytecode 50.0
.class synchronized com/a/b/n/a/bk
.super java/lang/IllegalStateException

.field static final 'a' [Ljava/lang/StackTraceElement;

.field static 'b' Ljava/util/Set;

.method static <clinit>()V
iconst_0
anewarray java/lang/StackTraceElement
putstatic com/a/b/n/a/bk/a [Ljava/lang/StackTraceElement;
ldc com/a/b/n/a/bd
invokevirtual java/lang/Class/getName()Ljava/lang/String;
ldc com/a/b/n/a/bk
invokevirtual java/lang/Class/getName()Ljava/lang/String;
ldc com/a/b/n/a/bl
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic com/a/b/d/lo/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/lo;
putstatic com/a/b/n/a/bk/b Ljava/util/Set;
return
.limit locals 0
.limit stack 3
.end method

.method <init>(Lcom/a/b/n/a/bl;Lcom/a/b/n/a/bl;)V
aload 1
getfield com/a/b/n/a/bl/c Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 2
getfield com/a/b/n/a/bl/c Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
aload 0
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
iconst_4
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " -> "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalStateException/<init>(Ljava/lang/String;)V
aload 0
invokevirtual com/a/b/n/a/bk/getStackTrace()[Ljava/lang/StackTraceElement;
astore 1
iconst_0
istore 3
aload 1
arraylength
istore 4
L0:
iload 3
iload 4
if_icmpge L1
ldc com/a/b/n/a/bs
invokevirtual java/lang/Class/getName()Ljava/lang/String;
aload 1
iload 3
aaload
invokevirtual java/lang/StackTraceElement/getClassName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 0
getstatic com/a/b/n/a/bk/a [Ljava/lang/StackTraceElement;
invokevirtual com/a/b/n/a/bk/setStackTrace([Ljava/lang/StackTraceElement;)V
L1:
return
L2:
getstatic com/a/b/n/a/bk/b Ljava/util/Set;
aload 1
iload 3
aaload
invokevirtual java/lang/StackTraceElement/getClassName()Ljava/lang/String;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifne L3
aload 0
aload 1
iload 3
iload 4
invokestatic java/util/Arrays/copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;
checkcast [Ljava/lang/StackTraceElement;
invokevirtual com/a/b/n/a/bk/setStackTrace([Ljava/lang/StackTraceElement;)V
return
L3:
iload 3
iconst_1
iadd
istore 3
goto L0
.limit locals 5
.limit stack 5
.end method
