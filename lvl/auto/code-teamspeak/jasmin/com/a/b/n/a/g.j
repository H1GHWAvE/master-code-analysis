.bytecode 50.0
.class public synchronized abstract com/a/b/n/a/g
.super java/lang/Object
.implements com/a/b/n/a/dp

.field final 'a' Lcom/a/b/n/a/h;

.field private final 'b' Lcom/a/b/n/a/bu;

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/a/b/n/a/h
dup
invokespecial com/a/b/n/a/h/<init>()V
putfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
aload 0
new com/a/b/n/a/bu
dup
invokespecial com/a/b/n/a/bu/<init>()V
putfield com/a/b/n/a/g/b Lcom/a/b/n/a/bu;
return
.limit locals 1
.limit stack 3
.end method

.method static final a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
new java/util/concurrent/CancellationException
dup
aload 0
invokespecial java/util/concurrent/CancellationException/<init>(Ljava/lang/String;)V
astore 0
aload 0
aload 1
invokevirtual java/util/concurrent/CancellationException/initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
pop
aload 0
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a()V
return
.limit locals 0
.limit stack 0
.end method

.method private b()Z
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
invokevirtual com/a/b/n/a/h/d()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
aload 0
getfield com/a/b/n/a/g/b Lcom/a/b/n/a/bu;
aload 1
aload 2
invokevirtual com/a/b/n/a/bu/a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
return
.limit locals 3
.limit stack 3
.end method

.method protected a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
aload 1
aconst_null
iconst_2
invokevirtual com/a/b/n/a/h/a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
istore 2
iload 2
ifeq L0
aload 0
getfield com/a/b/n/a/g/b Lcom/a/b/n/a/bu;
invokevirtual com/a/b/n/a/bu/a()V
L0:
iload 2
ireturn
.limit locals 3
.limit stack 4
.end method

.method protected a(Ljava/lang/Throwable;)Z
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
aconst_null
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Throwable
iconst_2
invokevirtual com/a/b/n/a/h/a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
istore 2
iload 2
ifeq L0
aload 0
getfield com/a/b/n/a/g/b Lcom/a/b/n/a/bu;
invokevirtual com/a/b/n/a/bu/a()V
L0:
iload 2
ireturn
.limit locals 3
.limit stack 4
.end method

.method public cancel(Z)Z
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
astore 3
iload 1
ifeq L0
bipush 8
istore 2
L1:
aload 3
aconst_null
aconst_null
iload 2
invokevirtual com/a/b/n/a/h/a(Ljava/lang/Object;Ljava/lang/Throwable;I)Z
ifne L2
iconst_0
ireturn
L0:
iconst_4
istore 2
goto L1
L2:
aload 0
getfield com/a/b/n/a/g/b Lcom/a/b/n/a/bu;
invokevirtual com/a/b/n/a/bu/a()V
iconst_1
ireturn
.limit locals 4
.limit stack 4
.end method

.method public get()Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
astore 1
aload 1
iconst_m1
invokevirtual com/a/b/n/a/h/acquireSharedInterruptibly(I)V
aload 1
invokevirtual com/a/b/n/a/h/a()Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
astore 4
aload 4
iconst_m1
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
invokevirtual com/a/b/n/a/h/tryAcquireSharedNanos(IJ)Z
ifne L0
new java/util/concurrent/TimeoutException
dup
ldc "Timeout waiting for task."
invokespecial java/util/concurrent/TimeoutException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 4
invokevirtual com/a/b/n/a/h/a()Ljava/lang/Object;
areturn
.limit locals 5
.limit stack 5
.end method

.method public isCancelled()Z
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
invokevirtual com/a/b/n/a/h/c()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isDone()Z
aload 0
getfield com/a/b/n/a/g/a Lcom/a/b/n/a/h;
invokevirtual com/a/b/n/a/h/b()Z
ireturn
.limit locals 1
.limit stack 1
.end method
