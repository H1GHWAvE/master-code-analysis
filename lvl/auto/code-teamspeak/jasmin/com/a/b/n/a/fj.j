.bytecode 50.0
.class final synchronized com/a/b/n/a/fj
.super com/a/b/n/a/ev

.field final 'a' Lcom/a/b/n/a/et;

.field final 'b' Ljava/lang/ref/WeakReference;

.method <init>(Lcom/a/b/n/a/et;Ljava/lang/ref/WeakReference;)V
aload 0
invokespecial com/a/b/n/a/ev/<init>()V
aload 0
aload 1
putfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
aload 0
aload 2
putfield com/a/b/n/a/fj/b Ljava/lang/ref/WeakReference;
return
.limit locals 3
.limit stack 2
.end method

.method public final a()V
aload 0
getfield com/a/b/n/a/fj/b Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast com/a/b/n/a/fk
astore 1
aload 1
ifnull L0
aload 1
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
getstatic com/a/b/n/a/ew/a Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/b Lcom/a/b/n/a/ew;
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
instanceof com/a/b/n/a/fi
ifne L0
invokestatic com/a/b/n/a/fd/a()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/FINE Ljava/util/logging/Level;
ldc "Starting {0}."
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Object;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public final a(Lcom/a/b/n/a/ew;)V
aload 0
getfield com/a/b/n/a/fj/b Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast com/a/b/n/a/fk
astore 2
aload 2
ifnull L0
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
instanceof com/a/b/n/a/fi
ifne L1
invokestatic com/a/b/n/a/fd/a()Ljava/util/logging/Logger;
getstatic java/util/logging/Level/FINE Ljava/util/logging/Level;
ldc "Service {0} has terminated. Previous state was: {1}"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
aastore
dup
iconst_1
aload 1
aastore
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V
L1:
aload 2
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
aload 1
getstatic com/a/b/n/a/ew/e Lcom/a/b/n/a/ew;
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V
L0:
return
.limit locals 3
.limit stack 7
.end method

.method public final a(Lcom/a/b/n/a/ew;Ljava/lang/Throwable;)V
aload 0
getfield com/a/b/n/a/fj/b Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast com/a/b/n/a/fk
astore 3
aload 3
ifnull L0
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
instanceof com/a/b/n/a/fi
ifne L1
invokestatic com/a/b/n/a/fd/a()Ljava/util/logging/Logger;
astore 4
getstatic java/util/logging/Level/SEVERE Ljava/util/logging/Level;
astore 5
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 6
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 7
aload 4
aload 5
new java/lang/StringBuilder
dup
aload 6
invokevirtual java/lang/String/length()I
bipush 34
iadd
aload 7
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Service "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 6
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " has failed in the "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 7
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " state."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
aload 2
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
L1:
aload 3
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
aload 1
getstatic com/a/b/n/a/ew/f Lcom/a/b/n/a/ew;
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V
L0:
return
.limit locals 8
.limit stack 6
.end method

.method public final b()V
aload 0
getfield com/a/b/n/a/fj/b Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast com/a/b/n/a/fk
astore 1
aload 1
ifnull L0
aload 1
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
getstatic com/a/b/n/a/ew/b Lcom/a/b/n/a/ew;
getstatic com/a/b/n/a/ew/c Lcom/a/b/n/a/ew;
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V
L0:
return
.limit locals 2
.limit stack 4
.end method

.method public final b(Lcom/a/b/n/a/ew;)V
aload 0
getfield com/a/b/n/a/fj/b Ljava/lang/ref/WeakReference;
invokevirtual java/lang/ref/WeakReference/get()Ljava/lang/Object;
checkcast com/a/b/n/a/fk
astore 2
aload 2
ifnull L0
aload 2
aload 0
getfield com/a/b/n/a/fj/a Lcom/a/b/n/a/et;
aload 1
getstatic com/a/b/n/a/ew/d Lcom/a/b/n/a/ew;
invokevirtual com/a/b/n/a/fk/a(Lcom/a/b/n/a/et;Lcom/a/b/n/a/ew;Lcom/a/b/n/a/ew;)V
L0:
return
.limit locals 3
.limit stack 4
.end method
