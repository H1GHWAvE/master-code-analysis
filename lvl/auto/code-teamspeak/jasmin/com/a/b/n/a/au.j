.bytecode 50.0
.class public final synchronized com/a/b/n/a/au
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'a' J = 0L


.field private transient 'b' Ljava/util/concurrent/atomic/AtomicLongArray;

.method private <init>(I)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicLongArray
dup
iload 1
invokespecial java/util/concurrent/atomic/AtomicLongArray/<init>(I)V
putfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
return
.limit locals 2
.limit stack 4
.end method

.method private <init>([D)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
arraylength
istore 3
iload 3
newarray long
astore 4
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 4
iload 2
aload 1
iload 2
daload
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
new java/util/concurrent/atomic/AtomicLongArray
dup
aload 4
invokespecial java/util/concurrent/atomic/AtomicLongArray/<init>([J)V
putfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
return
.limit locals 5
.limit stack 4
.end method

.method private a(I)D
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
invokevirtual java/util/concurrent/atomic/AtomicLongArray/get(I)J
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 2
.limit stack 2
.end method

.method private a()I
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
invokevirtual java/util/concurrent/atomic/AtomicLongArray/length()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(ID)V
dload 2
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 4
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
lload 4
invokevirtual java/util/concurrent/atomic/AtomicLongArray/set(IJ)V
return
.limit locals 6
.limit stack 4
.end method

.method private a(Ljava/io/ObjectInputStream;)V
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 1
invokevirtual java/io/ObjectInputStream/readInt()I
istore 3
aload 0
new java/util/concurrent/atomic/AtomicLongArray
dup
iload 3
invokespecial java/util/concurrent/atomic/AtomicLongArray/<init>(I)V
putfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 0
iload 2
aload 1
invokevirtual java/io/ObjectInputStream/readDouble()D
invokespecial com/a/b/n/a/au/a(ID)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 4
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
invokevirtual java/util/concurrent/atomic/AtomicLongArray/length()I
istore 3
aload 1
iload 3
invokevirtual java/io/ObjectOutputStream/writeInt(I)V
iconst_0
istore 2
L0:
iload 2
iload 3
if_icmpge L1
aload 1
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 2
invokevirtual java/util/concurrent/atomic/AtomicLongArray/get(I)J
invokestatic java/lang/Double/longBitsToDouble(J)D
invokevirtual java/io/ObjectOutputStream/writeDouble(D)V
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
return
.limit locals 4
.limit stack 3
.end method

.method private a(IDD)Z
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
dload 2
invokestatic java/lang/Double/doubleToRawLongBits(D)J
dload 4
invokestatic java/lang/Double/doubleToRawLongBits(D)J
invokevirtual java/util/concurrent/atomic/AtomicLongArray/compareAndSet(IJJ)Z
ireturn
.limit locals 6
.limit stack 6
.end method

.method private b(ID)V
aload 0
iload 1
dload 2
invokespecial com/a/b/n/a/au/a(ID)V
return
.limit locals 4
.limit stack 4
.end method

.method private b(IDD)Z
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
dload 2
invokestatic java/lang/Double/doubleToRawLongBits(D)J
dload 4
invokestatic java/lang/Double/doubleToRawLongBits(D)J
invokevirtual java/util/concurrent/atomic/AtomicLongArray/weakCompareAndSet(IJJ)Z
ireturn
.limit locals 6
.limit stack 6
.end method

.method private c(ID)D
dload 2
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 4
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
lload 4
invokevirtual java/util/concurrent/atomic/AtomicLongArray/getAndSet(IJ)J
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 6
.limit stack 4
.end method

.method private d(ID)D
L0:
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
invokevirtual java/util/concurrent/atomic/AtomicLongArray/get(I)J
lstore 6
lload 6
invokestatic java/lang/Double/longBitsToDouble(J)D
dstore 4
dload 4
dload 2
dadd
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 8
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
lload 6
lload 8
invokevirtual java/util/concurrent/atomic/AtomicLongArray/compareAndSet(IJJ)Z
ifeq L0
dload 4
dreturn
.limit locals 10
.limit stack 6
.end method

.method private e(ID)D
L0:
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
invokevirtual java/util/concurrent/atomic/AtomicLongArray/get(I)J
lstore 6
lload 6
invokestatic java/lang/Double/longBitsToDouble(J)D
dload 2
dadd
dstore 4
dload 4
invokestatic java/lang/Double/doubleToRawLongBits(D)J
lstore 8
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
lload 6
lload 8
invokevirtual java/util/concurrent/atomic/AtomicLongArray/compareAndSet(IJJ)Z
ifeq L0
dload 4
dreturn
.limit locals 10
.limit stack 6
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
invokevirtual java/util/concurrent/atomic/AtomicLongArray/length()I
iconst_1
isub
istore 2
iload 2
iconst_m1
if_icmpne L0
ldc "[]"
areturn
L0:
new java/lang/StringBuilder
dup
iload 2
iconst_1
iadd
bipush 19
imul
invokespecial java/lang/StringBuilder/<init>(I)V
astore 3
aload 3
bipush 91
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iconst_0
istore 1
L1:
aload 3
aload 0
getfield com/a/b/n/a/au/b Ljava/util/concurrent/atomic/AtomicLongArray;
iload 1
invokevirtual java/util/concurrent/atomic/AtomicLongArray/get(I)J
invokestatic java/lang/Double/longBitsToDouble(J)D
invokevirtual java/lang/StringBuilder/append(D)Ljava/lang/StringBuilder;
pop
iload 1
iload 2
if_icmpne L2
aload 3
bipush 93
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
L2:
aload 3
bipush 44
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
bipush 32
invokevirtual java/lang/StringBuilder/append(C)Ljava/lang/StringBuilder;
pop
iload 1
iconst_1
iadd
istore 1
goto L1
.limit locals 4
.limit stack 4
.end method
