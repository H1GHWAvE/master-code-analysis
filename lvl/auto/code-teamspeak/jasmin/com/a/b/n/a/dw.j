.bytecode 50.0
.class public final synchronized com/a/b/n/a/dw
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field final 'a' Ljava/util/concurrent/locks/ReentrantLock;

.field private final 'b' Z

.field private 'c' Lcom/a/b/n/a/dx;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
.end field

.method public <init>()V
aload 0
iconst_0
invokespecial com/a/b/n/a/dw/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aconst_null
putfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
aload 0
iconst_0
putfield com/a/b/n/a/dw/b Z
aload 0
new java/util/concurrent/locks/ReentrantLock
dup
iconst_0
invokespecial java/util/concurrent/locks/ReentrantLock/<init>(Z)V
putfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
return
.limit locals 2
.limit stack 4
.end method

.method private static synthetic a(Lcom/a/b/n/a/dw;)Ljava/util/concurrent/locks/ReentrantLock;
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Lcom/a/b/n/a/dx;Z)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
.catch all from L0 to L1 using L2
iload 2
ifeq L3
aload 0
invokespecial com/a/b/n/a/dw/k()V
L3:
aload 0
aload 1
invokespecial com/a/b/n/a/dw/k(Lcom/a/b/n/a/dx;)V
L0:
aload 1
getfield com/a/b/n/a/dx/c Ljava/util/concurrent/locks/Condition;
invokeinterface java/util/concurrent/locks/Condition/await()V 0
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 2
L1:
iload 2
ifeq L0
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
return
L2:
astore 3
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method private a(JLjava/util/concurrent/TimeUnit;)Z
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L5 using L6
iconst_1
istore 9
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 4
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 3
aload 0
getfield com/a/b/n/a/dw/b Z
ifne L7
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock()Z
ifeq L7
L8:
iload 9
ireturn
L7:
invokestatic java/lang/System/nanoTime()J
lstore 6
invokestatic java/lang/Thread/interrupted()Z
istore 8
lload 4
lstore 1
L0:
aload 3
lload 1
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock(JLjava/util/concurrent/TimeUnit;)Z
istore 10
L1:
iload 10
istore 9
iload 8
ifeq L8
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
iload 10
ireturn
L2:
astore 11
L4:
invokestatic java/lang/System/nanoTime()J
lstore 1
L5:
lload 6
lload 4
ladd
lload 1
lsub
lstore 1
iconst_1
istore 8
goto L0
L3:
astore 3
L9:
iload 8
ifeq L10
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L10:
aload 3
athrow
L6:
astore 3
iconst_1
istore 8
goto L9
.limit locals 12
.limit stack 4
.end method

.method private a(Lcom/a/b/n/a/dx;JZ)Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
.catch all from L0 to L1 using L2
iload 4
ifeq L3
aload 0
invokespecial com/a/b/n/a/dw/k()V
L3:
aload 0
aload 1
invokespecial com/a/b/n/a/dw/k(Lcom/a/b/n/a/dx;)V
L4:
lload 2
lconst_0
lcmp
ifge L0
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
iconst_0
ireturn
L0:
aload 1
getfield com/a/b/n/a/dx/c Ljava/util/concurrent/locks/Condition;
lload 2
invokeinterface java/util/concurrent/locks/Condition/awaitNanos(J)J 2
lstore 2
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 4
L1:
iload 4
ifeq L4
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
iconst_1
ireturn
L2:
astore 5
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
aload 5
athrow
.limit locals 6
.limit stack 4
.end method

.method private a(Ljava/lang/Thread;)Z
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
aload 1
invokevirtual java/util/concurrent/locks/ReentrantLock/hasQueuedThread(Ljava/lang/Thread;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b()V
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
return
.limit locals 1
.limit stack 1
.end method

.method private b(Lcom/a/b/n/a/dx;Z)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
.catch all from L0 to L1 using L2
iload 2
ifeq L3
aload 0
invokespecial com/a/b/n/a/dw/k()V
L3:
aload 0
aload 1
invokespecial com/a/b/n/a/dw/k(Lcom/a/b/n/a/dx;)V
L0:
aload 1
getfield com/a/b/n/a/dx/c Ljava/util/concurrent/locks/Condition;
invokeinterface java/util/concurrent/locks/Condition/awaitUninterruptibly()V 0
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 2
L1:
iload 2
ifeq L0
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
return
L2:
astore 3
aload 0
aload 1
invokespecial com/a/b/n/a/dw/l(Lcom/a/b/n/a/dx;)V
aload 3
athrow
.limit locals 4
.limit stack 2
.end method

.method private b(JLjava/util/concurrent/TimeUnit;)Z
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
lload 1
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock(JLjava/util/concurrent/TimeUnit;)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method private c()V
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lockInterruptibly()V
return
.limit locals 1
.limit stack 1
.end method

.method private c(Lcom/a/b/n/a/dx;)V
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 3
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
istore 2
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/lockInterruptibly()V
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L1
aload 0
aload 1
iload 2
invokespecial com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;Z)V
L1:
return
L2:
astore 1
aload 0
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method private c(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L5
aload 4
lload 2
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 7
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L6
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L6:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 11
aload 11
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
istore 9
aload 0
getfield com/a/b/n/a/dw/b Z
ifne L7
lload 7
lstore 5
aload 11
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock()Z
ifne L0
L7:
invokestatic java/lang/System/nanoTime()J
lstore 5
aload 11
lload 2
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock(JLjava/util/concurrent/TimeUnit;)Z
ifne L8
iconst_0
ireturn
L8:
lload 7
lload 5
ladd
invokestatic java/lang/System/nanoTime()J
lsub
lstore 5
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L9
aload 0
aload 1
lload 5
iload 9
invokespecial com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;JZ)Z
istore 10
L1:
iload 10
ifeq L10
L9:
iconst_1
istore 9
L11:
iload 9
ifne L12
aload 11
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
L12:
iload 9
ireturn
L10:
iconst_0
istore 9
goto L11
L2:
astore 1
iload 9
ifne L4
L3:
aload 0
invokespecial com/a/b/n/a/dw/k()V
L4:
aload 11
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
L5:
astore 1
aload 11
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 12
.limit stack 5
.end method

.method private d()Z
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d(Lcom/a/b/n/a/dx;)Z
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 3
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/lockInterruptibly()V
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 2
L1:
iload 2
ifne L4
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
L4:
iload 2
ireturn
L2:
astore 1
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method private d(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
lload 2
aload 4
invokespecial com/a/b/n/a/dw/a(JLjava/util/concurrent/TimeUnit;)Z
ifne L0
iconst_0
istore 5
L4:
iload 5
ireturn
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 6
L1:
iload 6
istore 5
iload 6
ifne L4
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
iload 6
ireturn
L2:
astore 1
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 7
.limit stack 4
.end method

.method private e()Z
aload 0
getfield com/a/b/n/a/dw/b Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private e(Lcom/a/b/n/a/dx;)Z
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 4
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock()Z
ifne L0
iconst_0
istore 2
L4:
iload 2
ireturn
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 3
L1:
iload 3
istore 2
iload 3
ifne L4
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
iload 3
ireturn
L2:
astore 1
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 5
.limit stack 2
.end method

.method private e(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 7
aload 7
lload 2
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock(JLjava/util/concurrent/TimeUnit;)Z
ifne L0
iconst_0
istore 5
L4:
iload 5
ireturn
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 6
L1:
iload 6
istore 5
iload 6
ifne L4
aload 7
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
iload 6
ireturn
L2:
astore 1
aload 7
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 8
.limit stack 4
.end method

.method private f(Lcom/a/b/n/a/dx;)V
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpne L0
iconst_1
istore 2
L1:
iload 2
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
iand
ifne L2
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L0:
iconst_0
istore 2
goto L1
L2:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L3
aload 0
aload 1
iconst_1
invokespecial com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;Z)V
L3:
return
.limit locals 3
.limit stack 3
.end method

.method private f()Z
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isLocked()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
iconst_0
istore 6
aload 4
lload 2
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpne L0
iconst_1
istore 5
L1:
iload 5
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
iand
ifne L2
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L0:
iconst_0
istore 5
goto L1
L2:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L3
aload 0
aload 1
lload 2
iconst_1
invokespecial com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;JZ)Z
ifeq L4
L3:
iconst_1
istore 6
L4:
iload 6
ireturn
.limit locals 7
.limit stack 5
.end method

.method private g(Lcom/a/b/n/a/dx;)V
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpne L0
iconst_1
istore 2
L1:
iload 2
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
iand
ifne L2
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L0:
iconst_0
istore 2
goto L1
L2:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L3
aload 0
aload 1
iconst_1
invokespecial com/a/b/n/a/dw/b(Lcom/a/b/n/a/dx;Z)V
L3:
return
.limit locals 3
.limit stack 3
.end method

.method private g()Z
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h()I
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/getHoldCount()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private h(Lcom/a/b/n/a/dx;)Z
aload 0
aload 1
invokespecial com/a/b/n/a/dw/i(Lcom/a/b/n/a/dx;)I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method private i()I
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/getQueueLength()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private i(Lcom/a/b/n/a/dx;)I
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 1
getfield com/a/b/n/a/dx/d I
istore 2
L1:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
iload 2
ireturn
L2:
astore 1
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method private j()Z
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/hasQueuedThreads()Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private j(Lcom/a/b/n/a/dx;)Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
.catch java/lang/Throwable from L0 to L1 using L2
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 2
L1:
iload 2
ireturn
L2:
astore 3
aload 0
getfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
astore 1
L3:
aload 1
ifnull L4
aload 1
getfield com/a/b/n/a/dx/c Ljava/util/concurrent/locks/Condition;
invokeinterface java/util/concurrent/locks/Condition/signalAll()V 0
aload 1
getfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
astore 1
goto L3
L4:
aload 3
invokestatic com/a/b/b/ei/a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;
athrow
.limit locals 4
.limit stack 1
.end method

.method private k()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
aload 0
getfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
astore 1
L0:
aload 1
ifnull L1
aload 0
aload 1
invokespecial com/a/b/n/a/dw/j(Lcom/a/b/n/a/dx;)Z
ifeq L2
aload 1
getfield com/a/b/n/a/dx/c Ljava/util/concurrent/locks/Condition;
invokeinterface java/util/concurrent/locks/Condition/signal()V 0
L1:
return
L2:
aload 1
getfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
astore 1
goto L0
.limit locals 2
.limit stack 2
.end method

.method private k(Lcom/a/b/n/a/dx;)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
aload 1
getfield com/a/b/n/a/dx/d I
istore 2
aload 1
iload 2
iconst_1
iadd
putfield com/a/b/n/a/dx/d I
iload 2
ifne L0
aload 1
aload 0
getfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
putfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
aload 0
aload 1
putfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
L0:
return
.limit locals 3
.limit stack 3
.end method

.method private l()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
aload 0
getfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
astore 1
L0:
aload 1
ifnull L1
aload 1
getfield com/a/b/n/a/dx/c Ljava/util/concurrent/locks/Condition;
invokeinterface java/util/concurrent/locks/Condition/signalAll()V 0
aload 1
getfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
astore 1
goto L0
L1:
return
.limit locals 2
.limit stack 1
.end method

.method private l(Lcom/a/b/n/a/dx;)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "lock"
.end annotation
aload 1
getfield com/a/b/n/a/dx/d I
iconst_1
isub
istore 2
aload 1
iload 2
putfield com/a/b/n/a/dx/d I
iload 2
ifne L0
aload 0
getfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
astore 3
aconst_null
astore 4
L1:
aload 3
aload 1
if_acmpne L2
aload 4
ifnonnull L3
aload 0
aload 3
getfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
putfield com/a/b/n/a/dw/c Lcom/a/b/n/a/dx;
L4:
aload 3
aconst_null
putfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
L0:
return
L3:
aload 4
aload 3
getfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
putfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
goto L4
L2:
aload 3
getfield com/a/b/n/a/dx/e Lcom/a/b/n/a/dx;
astore 5
aload 3
astore 4
aload 5
astore 3
goto L1
.limit locals 6
.limit stack 2
.end method

.method public final a()V
.catch all from L0 to L1 using L2
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 1
L0:
aload 1
invokevirtual java/util/concurrent/locks/ReentrantLock/getHoldCount()I
iconst_1
if_icmpne L1
aload 0
invokespecial com/a/b/n/a/dw/k()V
L1:
aload 1
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
return
L2:
astore 2
aload 1
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 2
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a(Lcom/a/b/n/a/dx;)V
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 3
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
istore 2
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L1
aload 0
aload 1
iload 2
invokespecial com/a/b/n/a/dw/b(Lcom/a/b/n/a/dx;Z)V
L1:
return
L2:
astore 1
aload 0
invokevirtual com/a/b/n/a/dw/a()V
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public final a(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch java/lang/InterruptedException from L5 to L6 using L7
.catch all from L5 to L6 using L2
.catch all from L8 to L9 using L2
.catch java/lang/InterruptedException from L10 to L11 using L12
.catch all from L10 to L11 using L13
.catch java/lang/InterruptedException from L14 to L15 using L12
.catch all from L14 to L15 using L13
.catch all from L16 to L17 using L18
.catch all from L19 to L20 using L13
.catch all from L21 to L22 using L18
.catch all from L23 to L18 using L18
aload 4
lload 2
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 5
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L24
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L24:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 4
invokestatic java/lang/System/nanoTime()J
lload 5
ladd
lstore 7
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
istore 12
invokestatic java/lang/Thread/interrupted()Z
istore 10
iload 10
istore 9
L0:
aload 0
getfield com/a/b/n/a/dw/b Z
ifne L25
L1:
iload 10
istore 9
L3:
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock()Z
istore 11
L4:
lload 5
lstore 2
iload 10
istore 9
iload 11
ifne L26
L25:
iconst_0
istore 11
L27:
iload 10
istore 9
L5:
aload 4
lload 5
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/locks/ReentrantLock/tryLock(JLjava/util/concurrent/TimeUnit;)Z
istore 13
L6:
iload 13
ifne L28
iload 10
ifeq L29
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L29:
iconst_0
istore 11
L30:
iload 11
ireturn
L28:
iload 13
istore 11
L31:
iload 10
istore 9
L8:
invokestatic java/lang/System/nanoTime()J
lstore 2
L9:
lload 7
lload 2
lsub
lstore 2
iload 11
ifeq L32
iload 10
istore 9
L26:
iload 12
istore 11
L33:
iload 9
istore 10
L10:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifne L34
L11:
iload 9
istore 10
L14:
aload 0
aload 1
lload 2
iload 11
invokespecial com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;JZ)Z
istore 11
L15:
iload 11
ifeq L35
L34:
iconst_1
istore 10
L36:
iload 10
ifne L17
iload 9
istore 11
L16:
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
L17:
iload 10
istore 11
iload 9
ifeq L30
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
iload 10
ireturn
L7:
astore 14
iconst_1
istore 10
goto L31
L35:
iconst_0
istore 10
goto L36
L12:
astore 14
iconst_1
istore 10
iconst_1
istore 9
iconst_0
istore 11
L19:
invokestatic java/lang/System/nanoTime()J
lstore 2
L20:
lload 7
lload 2
lsub
lstore 2
goto L33
L13:
astore 1
iload 10
istore 11
L21:
aload 4
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
L22:
iload 10
istore 11
L23:
aload 1
athrow
L18:
astore 1
iload 11
istore 9
L37:
iload 9
ifeq L38
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L38:
aload 1
athrow
L2:
astore 1
goto L37
L32:
lload 2
lstore 5
goto L27
.limit locals 15
.limit stack 5
.end method

.method public final b(Lcom/a/b/n/a/dx;)Z
.catch all from L0 to L1 using L2
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpeq L3
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L3:
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
astore 3
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/lock()V
L0:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 2
L1:
iload 2
ifne L4
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
L4:
iload 2
ireturn
L2:
astore 1
aload 3
invokevirtual java/util/concurrent/locks/ReentrantLock/unlock()V
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final b(Lcom/a/b/n/a/dx;JLjava/util/concurrent/TimeUnit;)Z
.catch java/lang/InterruptedException from L0 to L1 using L2
.catch all from L0 to L1 using L3
.catch all from L4 to L5 using L6
.catch all from L7 to L8 using L6
iconst_1
istore 12
iconst_1
istore 11
aload 4
lload 2
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
lstore 6
aload 1
getfield com/a/b/n/a/dx/b Lcom/a/b/n/a/dw;
aload 0
if_acmpne L9
iconst_1
istore 5
L10:
iload 5
aload 0
getfield com/a/b/n/a/dw/a Ljava/util/concurrent/locks/ReentrantLock;
invokevirtual java/util/concurrent/locks/ReentrantLock/isHeldByCurrentThread()Z
iand
ifne L11
new java/lang/IllegalMonitorStateException
dup
invokespecial java/lang/IllegalMonitorStateException/<init>()V
athrow
L9:
iconst_0
istore 5
goto L10
L11:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
ifeq L12
L13:
iload 11
ireturn
L12:
invokestatic java/lang/System/nanoTime()J
lstore 8
invokestatic java/lang/Thread/interrupted()Z
istore 10
iconst_1
istore 11
lload 6
lstore 2
L0:
aload 0
aload 1
lload 2
iload 11
invokespecial com/a/b/n/a/dw/a(Lcom/a/b/n/a/dx;JZ)Z
istore 11
L1:
iload 11
istore 12
iload 12
istore 11
iload 10
ifeq L13
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
iload 12
ireturn
L2:
astore 4
L4:
aload 1
invokevirtual com/a/b/n/a/dx/a()Z
istore 10
L5:
iload 10
ifeq L7
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
iconst_1
ireturn
L7:
invokestatic java/lang/System/nanoTime()J
lstore 2
L8:
lload 8
lload 6
ladd
lload 2
lsub
lstore 2
iconst_1
istore 10
iconst_0
istore 11
goto L0
L3:
astore 1
L14:
iload 10
ifeq L15
invokestatic java/lang/Thread/currentThread()Ljava/lang/Thread;
invokevirtual java/lang/Thread/interrupt()V
L15:
aload 1
athrow
L6:
astore 1
iload 12
istore 10
goto L14
.limit locals 13
.limit stack 5
.end method
