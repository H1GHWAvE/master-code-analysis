.bytecode 50.0
.class synchronized abstract com/a/b/m/ax
.super java/lang/Object
.annotation invisible Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation

.field private final 'a' Ljava/util/Set;

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new java/util/HashSet
dup
invokespecial java/util/HashSet/<init>()V
putfield com/a/b/m/ax/a Ljava/util/Set;
return
.limit locals 1
.limit stack 3
.end method

.method a(Ljava/lang/Class;)V
return
.limit locals 2
.limit stack 0
.end method

.method a(Ljava/lang/reflect/GenericArrayType;)V
return
.limit locals 2
.limit stack 0
.end method

.method a(Ljava/lang/reflect/ParameterizedType;)V
return
.limit locals 2
.limit stack 0
.end method

.method a(Ljava/lang/reflect/TypeVariable;)V
return
.limit locals 2
.limit stack 0
.end method

.method a(Ljava/lang/reflect/WildcardType;)V
return
.limit locals 2
.limit stack 0
.end method

.method public final transient a([Ljava/lang/reflect/Type;)V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
aload 1
arraylength
istore 3
iconst_0
istore 2
L13:
iload 2
iload 3
if_icmpge L12
aload 1
iload 2
aaload
astore 4
aload 4
ifnull L14
aload 0
getfield com/a/b/m/ax/a Ljava/util/Set;
aload 4
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
ifeq L14
L0:
aload 4
instanceof java/lang/reflect/TypeVariable
ifeq L3
aload 0
aload 4
checkcast java/lang/reflect/TypeVariable
invokevirtual com/a/b/m/ax/a(Ljava/lang/reflect/TypeVariable;)V
L1:
goto L14
L3:
aload 4
instanceof java/lang/reflect/WildcardType
ifeq L5
aload 0
aload 4
checkcast java/lang/reflect/WildcardType
invokevirtual com/a/b/m/ax/a(Ljava/lang/reflect/WildcardType;)V
L4:
goto L14
L2:
astore 1
aload 0
getfield com/a/b/m/ax/a Ljava/util/Set;
aload 4
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
aload 1
athrow
L5:
aload 4
instanceof java/lang/reflect/ParameterizedType
ifeq L7
aload 0
aload 4
checkcast java/lang/reflect/ParameterizedType
invokevirtual com/a/b/m/ax/a(Ljava/lang/reflect/ParameterizedType;)V
L6:
goto L14
L7:
aload 4
instanceof java/lang/Class
ifeq L9
aload 0
aload 4
checkcast java/lang/Class
invokevirtual com/a/b/m/ax/a(Ljava/lang/Class;)V
L8:
goto L14
L9:
aload 4
instanceof java/lang/reflect/GenericArrayType
ifeq L11
aload 0
aload 4
checkcast java/lang/reflect/GenericArrayType
invokevirtual com/a/b/m/ax/a(Ljava/lang/reflect/GenericArrayType;)V
L10:
goto L14
L11:
aload 4
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/AssertionError
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 14
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unknown type: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
L12:
return
L14:
iload 2
iconst_1
iadd
istore 2
goto L13
.limit locals 5
.limit stack 6
.end method
