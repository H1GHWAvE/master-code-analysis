.bytecode 50.0
.class public final synchronized com/a/b/m/w
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Lcom/a/b/m/z;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/a/b/m/z
dup
invokespecial com/a/b/m/z/<init>()V
putfield com/a/b/m/w/a Lcom/a/b/m/z;
return
.limit locals 1
.limit stack 3
.end method

.method private <init>(Lcom/a/b/m/z;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/m/w/a Lcom/a/b/m/z;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/m/z;B)V
aload 0
aload 1
invokespecial com/a/b/m/w/<init>(Lcom/a/b/m/z;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)Lcom/a/b/m/w;
invokestatic com/a/b/d/sz/c()Ljava/util/HashMap;
astore 3
aload 3
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/Type
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
aload 0
aload 3
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
areturn
.limit locals 4
.limit stack 3
.end method

.method static a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
.catch java/lang/ClassCastException from L0 to L1 using L2
L0:
aload 0
aload 1
invokevirtual java/lang/Class/cast(Ljava/lang/Object;)Ljava/lang/Object;
astore 2
L1:
aload 2
areturn
L2:
astore 2
aload 1
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
invokevirtual java/lang/Class/getSimpleName()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 0
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 10
iadd
aload 0
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc " is not a "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 0
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
.limit locals 3
.limit stack 6
.end method

.method private a(Ljava/lang/reflect/ParameterizedType;)Ljava/lang/reflect/ParameterizedType;
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
astore 2
aload 2
ifnonnull L0
aconst_null
astore 2
L1:
aload 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 3
aload 0
aload 1
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
astore 1
aload 2
aload 3
checkcast java/lang/Class
aload 1
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
areturn
L0:
aload 0
aload 2
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 2
goto L1
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/reflect/GenericArrayType;)Ljava/lang/reflect/Type;
aload 0
aload 1
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/lang/reflect/WildcardType;)Ljava/lang/reflect/WildcardType;
aload 1
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 2
aload 1
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 1
new com/a/b/m/bp
dup
aload 0
aload 2
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
aload 0
aload 1
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
invokespecial com/a/b/m/bp/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 3
.limit stack 5
.end method

.method static a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
aload 1
aload 2
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L0
return
L0:
new com/a/b/m/x
dup
aload 0
aload 2
invokespecial com/a/b/m/x/<init>(Ljava/util/Map;Ljava/lang/reflect/Type;)V
iconst_1
anewarray java/lang/reflect/Type
dup
iconst_0
aload 1
aastore
invokevirtual com/a/b/m/x/a([Ljava/lang/reflect/Type;)V
return
.limit locals 3
.limit stack 5
.end method

.method private static synthetic a(Lcom/a/b/m/w;[Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
aload 0
aload 1
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/reflect/Type;)Lcom/a/b/m/w;
new com/a/b/m/w
dup
invokespecial com/a/b/m/w/<init>()V
aload 0
invokestatic com/a/b/m/y/a(Ljava/lang/reflect/Type;)Lcom/a/b/d/jt;
invokevirtual com/a/b/m/w/a(Ljava/util/Map;)Lcom/a/b/m/w;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static synthetic b(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokestatic com/a/b/m/w/a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static synthetic b(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
aload 0
aload 1
aload 2
invokestatic com/a/b/m/w/a(Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/lang/reflect/Type;)V
return
.limit locals 3
.limit stack 3
.end method

.method final a(Ljava/util/Map;)Lcom/a/b/m/w;
new com/a/b/m/w
dup
aload 0
getfield com/a/b/m/w/a Lcom/a/b/m/z;
aload 1
invokevirtual com/a/b/m/z/a(Ljava/util/Map;)Lcom/a/b/m/z;
invokespecial com/a/b/m/w/<init>(Lcom/a/b/m/z;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
instanceof java/lang/reflect/TypeVariable
ifeq L0
aload 0
getfield com/a/b/m/w/a Lcom/a/b/m/z;
astore 2
aload 1
checkcast java/lang/reflect/TypeVariable
astore 1
aload 2
aload 1
new com/a/b/m/aa
dup
aload 2
aload 1
aload 2
invokespecial com/a/b/m/aa/<init>(Lcom/a/b/m/z;Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)V
invokevirtual com/a/b/m/z/a(Ljava/lang/reflect/TypeVariable;Lcom/a/b/m/z;)Ljava/lang/reflect/Type;
astore 2
L1:
aload 2
areturn
L0:
aload 1
instanceof java/lang/reflect/ParameterizedType
ifeq L2
aload 1
checkcast java/lang/reflect/ParameterizedType
astore 2
aload 2
invokeinterface java/lang/reflect/ParameterizedType/getOwnerType()Ljava/lang/reflect/Type; 0
astore 1
aload 1
ifnonnull L3
aconst_null
astore 1
L4:
aload 0
aload 2
invokeinterface java/lang/reflect/ParameterizedType/getRawType()Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 3
aload 0
aload 2
invokeinterface java/lang/reflect/ParameterizedType/getActualTypeArguments()[Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
astore 2
aload 1
aload 3
checkcast java/lang/Class
aload 2
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;Ljava/lang/Class;[Ljava/lang/reflect/Type;)Ljava/lang/reflect/ParameterizedType;
areturn
L3:
aload 0
aload 1
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
astore 1
goto L4
L2:
aload 1
instanceof java/lang/reflect/GenericArrayType
ifeq L5
aload 0
aload 1
checkcast java/lang/reflect/GenericArrayType
invokeinterface java/lang/reflect/GenericArrayType/getGenericComponentType()Ljava/lang/reflect/Type; 0
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
invokestatic com/a/b/m/ay/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
areturn
L5:
aload 1
astore 2
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L1
aload 1
checkcast java/lang/reflect/WildcardType
astore 2
aload 2
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
astore 1
aload 2
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
astore 2
new com/a/b/m/bp
dup
aload 0
aload 1
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
aload 0
aload 2
invokevirtual com/a/b/m/w/a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
invokespecial com/a/b/m/bp/<init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
areturn
.limit locals 4
.limit stack 7
.end method

.method final a([Ljava/lang/reflect/Type;)[Ljava/lang/reflect/Type;
aload 1
arraylength
anewarray java/lang/reflect/Type
astore 3
iconst_0
istore 2
L0:
iload 2
aload 1
arraylength
if_icmpge L1
aload 3
iload 2
aload 0
aload 1
iload 2
aaload
invokevirtual com/a/b/m/w/a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 3
areturn
.limit locals 4
.limit stack 5
.end method
