.bytecode 50.0
.class final synchronized com/a/b/m/bo
.super java/lang/Object
.implements java/lang/reflect/TypeVariable

.field private final 'a' Ljava/lang/reflect/GenericDeclaration;

.field private final 'b' Ljava/lang/String;

.field private final 'c' Lcom/a/b/d/jl;

.method <init>(Ljava/lang/reflect/GenericDeclaration;Ljava/lang/String;[Ljava/lang/reflect/Type;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 3
ldc "bound for type variable"
invokestatic com/a/b/m/ay/a([Ljava/lang/reflect/Type;Ljava/lang/String;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/reflect/GenericDeclaration
putfield com/a/b/m/bo/a Ljava/lang/reflect/GenericDeclaration;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/m/bo/b Ljava/lang/String;
aload 0
aload 3
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
putfield com/a/b/m/bo/c Lcom/a/b/d/jl;
return
.limit locals 4
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
getstatic com/a/b/m/bm/a Z
ifeq L0
aload 1
instanceof com/a/b/m/bo
ifeq L1
aload 1
checkcast com/a/b/m/bo
astore 1
aload 0
getfield com/a/b/m/bo/b Ljava/lang/String;
aload 1
invokevirtual com/a/b/m/bo/getName()Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 0
getfield com/a/b/m/bo/a Ljava/lang/reflect/GenericDeclaration;
aload 1
invokevirtual com/a/b/m/bo/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L2
aload 0
getfield com/a/b/m/bo/c Lcom/a/b/d/jl;
aload 1
getfield com/a/b/m/bo/c Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/equals(Ljava/lang/Object;)Z
ifeq L2
L3:
iconst_1
ireturn
L2:
iconst_0
ireturn
L1:
iconst_0
ireturn
L0:
aload 1
instanceof java/lang/reflect/TypeVariable
ifeq L4
aload 1
checkcast java/lang/reflect/TypeVariable
astore 1
aload 0
getfield com/a/b/m/bo/b Ljava/lang/String;
aload 1
invokeinterface java/lang/reflect/TypeVariable/getName()Ljava/lang/String; 0
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
getfield com/a/b/m/bo/a Ljava/lang/reflect/GenericDeclaration;
aload 1
invokeinterface java/lang/reflect/TypeVariable/getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration; 0
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifne L3
L5:
iconst_0
ireturn
L4:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final getBounds()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bo/c Lcom/a/b/d/jl;
invokestatic com/a/b/m/ay/a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getGenericDeclaration()Ljava/lang/reflect/GenericDeclaration;
aload 0
getfield com/a/b/m/bo/a Ljava/lang/reflect/GenericDeclaration;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getName()Ljava/lang/String;
aload 0
getfield com/a/b/m/bo/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/m/bo/a Ljava/lang/reflect/GenericDeclaration;
invokevirtual java/lang/Object/hashCode()I
aload 0
getfield com/a/b/m/bo/b Ljava/lang/String;
invokevirtual java/lang/String/hashCode()I
ixor
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/bo/b Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
