.bytecode 50.0
.class final synchronized com/a/b/m/bp
.super java/lang/Object
.implements java/io/Serializable
.implements java/lang/reflect/WildcardType

.field private static final 'c' J = 0L


.field private final 'a' Lcom/a/b/d/jl;

.field private final 'b' Lcom/a/b/d/jl;

.method <init>([Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 1
ldc "lower bound for wildcard"
invokestatic com/a/b/m/ay/a([Ljava/lang/reflect/Type;Ljava/lang/String;)V
aload 2
ldc "upper bound for wildcard"
invokestatic com/a/b/m/ay/a([Ljava/lang/reflect/Type;Ljava/lang/String;)V
aload 0
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 1
invokevirtual com/a/b/m/bh/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
putfield com/a/b/m/bp/a Lcom/a/b/d/jl;
aload 0
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 2
invokevirtual com/a/b/m/bh/a([Ljava/lang/reflect/Type;)Lcom/a/b/d/jl;
putfield com/a/b/m/bp/b Lcom/a/b/d/jl;
return
.limit locals 3
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof java/lang/reflect/WildcardType
ifeq L0
aload 1
checkcast java/lang/reflect/WildcardType
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/m/bp/a Lcom/a/b/d/jl;
aload 1
invokeinterface java/lang/reflect/WildcardType/getLowerBounds()[Ljava/lang/reflect/Type; 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokevirtual com/a/b/d/jl/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/m/bp/b Lcom/a/b/d/jl;
aload 1
invokeinterface java/lang/reflect/WildcardType/getUpperBounds()[Ljava/lang/reflect/Type; 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokevirtual com/a/b/d/jl/equals(Ljava/lang/Object;)Z
ifeq L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final getLowerBounds()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bp/a Lcom/a/b/d/jl;
invokestatic com/a/b/m/ay/a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final getUpperBounds()[Ljava/lang/reflect/Type;
aload 0
getfield com/a/b/m/bp/b Lcom/a/b/d/jl;
invokestatic com/a/b/m/ay/a(Ljava/util/Collection;)[Ljava/lang/reflect/Type;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/m/bp/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/hashCode()I
aload 0
getfield com/a/b/m/bp/b Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/hashCode()I
ixor
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
new java/lang/StringBuilder
dup
ldc "?"
invokespecial java/lang/StringBuilder/<init>(Ljava/lang/String;)V
astore 1
aload 0
getfield com/a/b/m/bp/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/reflect/Type
astore 3
aload 1
ldc " super "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 3
invokevirtual com/a/b/m/bh/c(Ljava/lang/reflect/Type;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L0
L1:
aload 0
getfield com/a/b/m/bp/b Lcom/a/b/d/jl;
invokestatic com/a/b/m/ay/a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/reflect/Type
astore 3
aload 1
ldc " extends "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
getstatic com/a/b/m/bh/d Lcom/a/b/m/bh;
aload 3
invokevirtual com/a/b/m/bh/c(Ljava/lang/reflect/Type;)Ljava/lang/String;
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
pop
goto L2
L3:
aload 1
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 3
.end method
