.bytecode 50.0
.class public synchronized com/a/b/m/e
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private final 'a' Ljava/lang/String;

.field final 'b' Ljava/lang/ClassLoader;

.method <init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
putfield com/a/b/m/e/a Ljava/lang/String;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/ClassLoader
putfield com/a/b/m/e/b Ljava/lang/ClassLoader;
return
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/lang/String;Ljava/lang/ClassLoader;)Lcom/a/b/m/e;
aload 0
ldc ".class"
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L0
new com/a/b/m/d
dup
aload 0
aload 1
invokespecial com/a/b/m/d/<init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
areturn
L0:
new com/a/b/m/e
dup
aload 0
aload 1
invokespecial com/a/b/m/e/<init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private a()Ljava/net/URL;
aload 0
getfield com/a/b/m/e/b Ljava/lang/ClassLoader;
aload 0
getfield com/a/b/m/e/a Ljava/lang/String;
invokevirtual java/lang/ClassLoader/getResource(Ljava/lang/String;)Ljava/net/URL;
ldc "Failed to load resource: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/m/e/a Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/net/URL
areturn
.limit locals 1
.limit stack 6
.end method

.method private b()Ljava/lang/String;
aload 0
getfield com/a/b/m/e/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method public equals(Ljava/lang/Object;)Z
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/m/e
ifeq L0
aload 1
checkcast com/a/b/m/e
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/m/e/a Ljava/lang/String;
aload 1
getfield com/a/b/m/e/a Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L0
iload 3
istore 2
aload 0
getfield com/a/b/m/e/b Ljava/lang/ClassLoader;
aload 1
getfield com/a/b/m/e/b Ljava/lang/ClassLoader;
if_acmpne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public hashCode()I
aload 0
getfield com/a/b/m/e/a Ljava/lang/String;
invokevirtual java/lang/String/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/m/e/a Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
