.bytecode 50.0
.class public synchronized abstract com/a/b/m/a
.super java/lang/Object
.implements java/lang/reflect/InvocationHandler
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' [Ljava/lang/Object;

.method static <clinit>()V
iconst_0
anewarray java/lang/Object
putstatic com/a/b/m/a/a [Ljava/lang/Object;
return
.limit locals 0
.limit stack 1
.end method

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Class;)Z
aload 1
aload 0
invokevirtual java/lang/Class/isInstance(Ljava/lang/Object;)Z
ifne L0
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokestatic java/lang/reflect/Proxy/isProxyClass(Ljava/lang/Class;)Z
ifeq L1
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getInterfaces()[Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/getInterfaces()[Ljava/lang/Class;
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifeq L1
L0:
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method protected abstract a()Ljava/lang/Object;
.end method

.method public equals(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public hashCode()I
aload 0
invokespecial java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
iconst_1
istore 5
aload 3
astore 6
aload 3
ifnonnull L0
getstatic com/a/b/m/a/a [Ljava/lang/Object;
astore 6
L0:
aload 6
arraylength
ifne L1
aload 2
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
ldc "hashCode"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
invokevirtual com/a/b/m/a/hashCode()I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
areturn
L1:
aload 6
arraylength
iconst_1
if_icmpne L2
aload 2
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
ldc "equals"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L2
aload 2
invokevirtual java/lang/reflect/Method/getParameterTypes()[Ljava/lang/Class;
iconst_0
aaload
ldc java/lang/Object
if_acmpne L2
aload 6
iconst_0
aaload
astore 2
aload 2
ifnonnull L3
iconst_0
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L3:
aload 1
aload 2
if_acmpne L4
iconst_1
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L4:
aload 1
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
astore 1
aload 1
aload 2
invokevirtual java/lang/Class/isInstance(Ljava/lang/Object;)Z
ifne L5
aload 2
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokestatic java/lang/reflect/Proxy/isProxyClass(Ljava/lang/Class;)Z
ifeq L6
aload 2
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getInterfaces()[Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/getInterfaces()[Ljava/lang/Class;
invokestatic java/util/Arrays/equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
ifeq L6
L5:
iconst_1
istore 4
L7:
iload 4
ifeq L8
aload 0
aload 2
invokestatic java/lang/reflect/Proxy/getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;
invokevirtual com/a/b/m/a/equals(Ljava/lang/Object;)Z
ifeq L8
L9:
iload 5
invokestatic java/lang/Boolean/valueOf(Z)Ljava/lang/Boolean;
areturn
L6:
iconst_0
istore 4
goto L7
L8:
iconst_0
istore 5
goto L9
L2:
aload 6
arraylength
ifne L10
aload 2
invokevirtual java/lang/reflect/Method/getName()Ljava/lang/String;
ldc "toString"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L10
aload 0
invokevirtual com/a/b/m/a/toString()Ljava/lang/String;
areturn
L10:
aload 0
invokevirtual com/a/b/m/a/a()Ljava/lang/Object;
areturn
.limit locals 7
.limit stack 2
.end method

.method public toString()Ljava/lang/String;
aload 0
invokespecial java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
