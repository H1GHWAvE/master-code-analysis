.bytecode 50.0
.class public final synchronized com/a/b/b/ds
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation

.field private static final 'a' Ljava/lang/String; = "Chunk [%s] is not a valid entry"

.field private final 'b' Lcom/a/b/b/di;

.field private final 'c' Lcom/a/b/b/di;

.method private <init>(Lcom/a/b/b/di;Lcom/a/b/b/di;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
putfield com/a/b/b/ds/b Lcom/a/b/b/di;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/di
putfield com/a/b/b/ds/c Lcom/a/b/b/di;
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/b/di;Lcom/a/b/b/di;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/b/ds/<init>(Lcom/a/b/b/di;Lcom/a/b/b/di;)V
return
.limit locals 4
.limit stack 3
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/Map;
new java/util/LinkedHashMap
dup
invokespecial java/util/LinkedHashMap/<init>()V
astore 3
aload 0
getfield com/a/b/b/ds/b Lcom/a/b/b/di;
aload 1
invokevirtual com/a/b/b/di/a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 4
aload 0
getfield com/a/b/b/ds/c Lcom/a/b/b/di;
aload 4
invokevirtual com/a/b/b/di/b(Ljava/lang/CharSequence;)Ljava/util/Iterator;
astore 5
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ldc "Chunk [%s] is not a valid entry"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 6
aload 3
aload 6
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L2
iconst_1
istore 2
L3:
iload 2
ldc "Duplicate key [%s] found."
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 6
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ldc "Chunk [%s] is not a valid entry"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 3
aload 6
aload 5
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
aload 5
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L4
iconst_1
istore 2
L5:
iload 2
ldc "Chunk [%s] is not a valid entry"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 4
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
goto L0
L2:
iconst_0
istore 2
goto L3
L4:
iconst_0
istore 2
goto L5
L1:
aload 3
invokestatic java/util/Collections/unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;
areturn
.limit locals 7
.limit stack 6
.end method
