.bytecode 50.0
.class public final synchronized com/a/b/b/az
.super java/lang/Object
.implements java/io/Serializable

.field private static final 'c' J = 0L


.field private final 'a' Lcom/a/b/b/au;

.field private final 'b' Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.end field

.method private <init>(Lcom/a/b/b/au;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
putfield com/a/b/b/az/a Lcom/a/b/b/au;
aload 0
aload 2
putfield com/a/b/b/az/b Ljava/lang/Object;
return
.limit locals 3
.limit stack 2
.end method

.method synthetic <init>(Lcom/a/b/b/au;Ljava/lang/Object;B)V
aload 0
aload 1
aload 2
invokespecial com/a/b/b/az/<init>(Lcom/a/b/b/au;Ljava/lang/Object;)V
return
.limit locals 4
.limit stack 3
.end method

.method private a()Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/b/az/b Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/b/az
ifeq L1
aload 1
checkcast com/a/b/b/az
astore 1
aload 0
getfield com/a/b/b/az/a Lcom/a/b/b/au;
aload 1
getfield com/a/b/b/az/a Lcom/a/b/b/au;
invokevirtual java/lang/Object/equals(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/a/b/b/az/a Lcom/a/b/b/au;
aload 0
getfield com/a/b/b/az/b Ljava/lang/Object;
aload 1
getfield com/a/b/b/az/b Ljava/lang/Object;
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/b/az/a Lcom/a/b/b/au;
aload 0
getfield com/a/b/b/az/b Ljava/lang/Object;
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;)I
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/az/a Lcom/a/b/b/au;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
getfield com/a/b/b/az/b Ljava/lang/Object;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 7
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ".wrap("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 3
.limit stack 4
.end method
