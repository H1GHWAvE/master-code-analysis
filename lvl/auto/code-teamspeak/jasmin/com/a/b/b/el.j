.bytecode 50.0
.class public final synchronized com/a/b/b/el
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/CharSequence;)I
iconst_0
istore 3
aload 0
invokeinterface java/lang/CharSequence/length()I 0
istore 5
iconst_0
istore 2
L0:
iload 2
iload 5
if_icmpge L1
aload 0
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
sipush 128
if_icmpge L1
iload 2
iconst_1
iadd
istore 2
goto L0
L2:
iload 2
iload 5
if_icmpge L3
aload 0
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 4
iload 4
sipush 2048
if_icmpge L4
iload 1
bipush 127
iload 4
isub
bipush 31
iushr
iadd
istore 1
iload 2
iconst_1
iadd
istore 2
goto L2
L4:
aload 0
invokeinterface java/lang/CharSequence/length()I 0
istore 7
L5:
iload 2
iload 7
if_icmpge L6
aload 0
iload 2
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 8
iload 8
sipush 2048
if_icmpge L7
iload 3
bipush 127
iload 8
isub
bipush 31
iushr
iadd
istore 3
iload 2
istore 4
L8:
iload 4
iconst_1
iadd
istore 2
goto L5
L7:
iload 3
iconst_2
iadd
istore 6
iload 6
istore 3
iload 2
istore 4
ldc_w 55296
iload 8
if_icmpgt L8
iload 6
istore 3
iload 2
istore 4
iload 8
ldc_w 57343
if_icmpgt L8
aload 0
iload 2
invokestatic java/lang/Character/codePointAt(Ljava/lang/CharSequence;I)I
ldc_w 65536
if_icmpge L9
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 39
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unpaired surrogate at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L9:
iload 2
iconst_1
iadd
istore 4
iload 6
istore 3
goto L8
L6:
iload 3
iload 1
iadd
istore 1
L10:
iload 1
iload 5
if_icmpge L11
iload 1
i2l
lstore 9
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 54
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "UTF-8 length does not fit in int: "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
lload 9
ldc2_w 4294967296L
ladd
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L11:
iload 1
ireturn
L3:
goto L10
L1:
iload 5
istore 1
goto L2
.limit locals 11
.limit stack 7
.end method

.method private static a(Ljava/lang/CharSequence;I)I
aload 0
invokeinterface java/lang/CharSequence/length()I 0
istore 5
iconst_0
istore 2
L0:
iload 1
iload 5
if_icmpge L1
aload 0
iload 1
invokeinterface java/lang/CharSequence/charAt(I)C 1
istore 6
iload 6
sipush 2048
if_icmpge L2
iload 2
bipush 127
iload 6
isub
bipush 31
iushr
iadd
istore 2
iload 1
istore 3
L3:
iload 3
iconst_1
iadd
istore 1
goto L0
L2:
iload 2
iconst_2
iadd
istore 4
iload 4
istore 2
iload 1
istore 3
ldc_w 55296
iload 6
if_icmpgt L3
iload 4
istore 2
iload 1
istore 3
iload 6
ldc_w 57343
if_icmpgt L3
aload 0
iload 1
invokestatic java/lang/Character/codePointAt(Ljava/lang/CharSequence;I)I
ldc_w 65536
if_icmpge L4
new java/lang/IllegalArgumentException
dup
new java/lang/StringBuilder
dup
bipush 39
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Unpaired surrogate at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L4:
iload 1
iconst_1
iadd
istore 3
iload 4
istore 2
goto L3
L1:
iload 2
ireturn
.limit locals 7
.limit stack 5
.end method

.method private static a([B)Z
aload 0
arraylength
iconst_0
iadd
istore 3
iconst_0
iload 3
aload 0
arraylength
invokestatic com/a/b/b/cn/a(III)V
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpge L1
aload 0
iload 1
baload
ifge L2
L3:
iload 1
iload 3
if_icmpge L1
iload 1
iconst_1
iadd
istore 2
aload 0
iload 1
baload
istore 1
iload 1
ifge L4
iload 1
bipush -32
if_icmpge L5
iload 2
iload 3
if_icmpne L6
iconst_0
ireturn
L6:
iload 1
bipush -62
if_icmplt L7
iload 2
iconst_1
iadd
istore 1
aload 0
iload 2
baload
bipush -65
if_icmple L3
L7:
iconst_0
ireturn
L5:
iload 1
bipush -16
if_icmpge L8
iload 2
iconst_1
iadd
iload 3
if_icmplt L9
iconst_0
ireturn
L9:
iload 2
iconst_1
iadd
istore 4
aload 0
iload 2
baload
istore 2
iload 2
bipush -65
if_icmpgt L10
iload 1
bipush -32
if_icmpne L11
iload 2
bipush -96
if_icmplt L10
L11:
iload 1
bipush -19
if_icmpne L12
bipush -96
iload 2
if_icmple L10
L12:
iload 4
iconst_1
iadd
istore 1
aload 0
iload 4
baload
bipush -65
if_icmple L3
L10:
iconst_0
ireturn
L8:
iload 2
iconst_2
iadd
iload 3
if_icmplt L13
iconst_0
ireturn
L13:
iload 2
iconst_1
iadd
istore 4
aload 0
iload 2
baload
istore 2
iload 2
bipush -65
if_icmpgt L14
iload 1
bipush 28
ishl
iload 2
bipush 112
iadd
iadd
bipush 30
ishr
ifne L14
iload 4
iconst_1
iadd
istore 2
aload 0
iload 4
baload
bipush -65
if_icmpgt L14
iload 2
iconst_1
iadd
istore 1
aload 0
iload 2
baload
bipush -65
if_icmple L3
L14:
iconst_0
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_1
ireturn
L4:
iload 2
istore 1
goto L3
.limit locals 5
.limit stack 3
.end method

.method private static a([BI)Z
iload 1
iconst_0
iadd
istore 3
iconst_0
iload 3
aload 0
arraylength
invokestatic com/a/b/b/cn/a(III)V
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpge L1
aload 0
iload 1
baload
ifge L2
L3:
iload 1
iload 3
if_icmplt L4
iconst_1
ireturn
L4:
iload 1
iconst_1
iadd
istore 2
aload 0
iload 1
baload
istore 1
iload 1
ifge L5
iload 1
bipush -32
if_icmpge L6
iload 2
iload 3
if_icmpne L7
iconst_0
ireturn
L7:
iload 1
bipush -62
if_icmplt L8
iload 2
iconst_1
iadd
istore 1
aload 0
iload 2
baload
bipush -65
if_icmple L3
L8:
iconst_0
ireturn
L6:
iload 1
bipush -16
if_icmpge L9
iload 2
iconst_1
iadd
iload 3
if_icmplt L10
iconst_0
ireturn
L10:
iload 2
iconst_1
iadd
istore 4
aload 0
iload 2
baload
istore 2
iload 2
bipush -65
if_icmpgt L11
iload 1
bipush -32
if_icmpne L12
iload 2
bipush -96
if_icmplt L11
L12:
iload 1
bipush -19
if_icmpne L13
bipush -96
iload 2
if_icmple L11
L13:
iload 4
iconst_1
iadd
istore 1
aload 0
iload 4
baload
bipush -65
if_icmple L3
L11:
iconst_0
ireturn
L9:
iload 2
iconst_2
iadd
iload 3
if_icmplt L14
iconst_0
ireturn
L14:
iload 2
iconst_1
iadd
istore 4
aload 0
iload 2
baload
istore 2
iload 2
bipush -65
if_icmpgt L15
iload 1
bipush 28
ishl
iload 2
bipush 112
iadd
iadd
bipush 30
ishr
ifne L15
iload 4
iconst_1
iadd
istore 2
aload 0
iload 4
baload
bipush -65
if_icmpgt L15
iload 2
iconst_1
iadd
istore 1
aload 0
iload 2
baload
bipush -65
if_icmple L3
L15:
iconst_0
ireturn
L2:
iload 1
iconst_1
iadd
istore 1
goto L0
L1:
iconst_1
ireturn
L5:
iload 2
istore 1
goto L3
.limit locals 5
.limit stack 3
.end method

.method private static a([BII)Z
L0:
iload 1
iload 2
if_icmplt L1
iconst_1
ireturn
L1:
iload 1
iconst_1
iadd
istore 3
aload 0
iload 1
baload
istore 1
iload 1
ifge L2
iload 1
bipush -32
if_icmpge L3
iload 3
iload 2
if_icmpne L4
iconst_0
ireturn
L4:
iload 1
bipush -62
if_icmplt L5
iload 3
iconst_1
iadd
istore 1
aload 0
iload 3
baload
bipush -65
if_icmple L6
L5:
iconst_0
ireturn
L3:
iload 1
bipush -16
if_icmpge L7
iload 3
iconst_1
iadd
iload 2
if_icmplt L8
iconst_0
ireturn
L8:
iload 3
iconst_1
iadd
istore 4
aload 0
iload 3
baload
istore 3
iload 3
bipush -65
if_icmpgt L9
iload 1
bipush -32
if_icmpne L10
iload 3
bipush -96
if_icmplt L9
L10:
iload 1
bipush -19
if_icmpne L11
bipush -96
iload 3
if_icmple L9
L11:
iload 4
iconst_1
iadd
istore 1
aload 0
iload 4
baload
bipush -65
if_icmple L0
L9:
iconst_0
ireturn
L7:
iload 3
iconst_2
iadd
iload 2
if_icmplt L12
iconst_0
ireturn
L12:
iload 3
iconst_1
iadd
istore 4
aload 0
iload 3
baload
istore 3
iload 3
bipush -65
if_icmpgt L13
iload 1
bipush 28
ishl
iload 3
bipush 112
iadd
iadd
bipush 30
ishr
ifne L13
iload 4
iconst_1
iadd
istore 3
aload 0
iload 4
baload
bipush -65
if_icmpgt L13
iload 3
iconst_1
iadd
istore 1
aload 0
iload 3
baload
bipush -65
if_icmple L6
L13:
iconst_0
ireturn
L6:
goto L0
L2:
iload 3
istore 1
goto L0
.limit locals 5
.limit stack 3
.end method
