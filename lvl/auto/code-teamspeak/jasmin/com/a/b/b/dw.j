.bytecode 50.0
.class public final synchronized com/a/b/b/dw
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public 'a' Z

.field private final 'b' Lcom/a/b/b/ej;

.field private 'c' J

.field private 'd' J

.method public <init>()V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokestatic com/a/b/b/ej/b()Lcom/a/b/b/ej;
invokespecial com/a/b/b/dw/<init>(Lcom/a/b/b/ej;)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(Lcom/a/b/b/ej;)V
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
ldc "ticker"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/ej
putfield com/a/b/b/dw/b Lcom/a/b/b/ej;
return
.limit locals 2
.limit stack 3
.end method

.method public static a()Lcom/a/b/b/dw;
new com/a/b/b/dw
dup
invokespecial com/a/b/b/dw/<init>()V
invokevirtual com/a/b/b/dw/b()Lcom/a/b/b/dw;
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Lcom/a/b/b/ej;)Lcom/a/b/b/dw;
new com/a/b/b/dw
dup
aload 0
invokespecial com/a/b/b/dw/<init>(Lcom/a/b/b/ej;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(J)Ljava/util/concurrent/TimeUnit;
getstatic java/util/concurrent/TimeUnit/DAYS Ljava/util/concurrent/TimeUnit;
lload 0
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L0
getstatic java/util/concurrent/TimeUnit/DAYS Ljava/util/concurrent/TimeUnit;
areturn
L0:
getstatic java/util/concurrent/TimeUnit/HOURS Ljava/util/concurrent/TimeUnit;
lload 0
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L1
getstatic java/util/concurrent/TimeUnit/HOURS Ljava/util/concurrent/TimeUnit;
areturn
L1:
getstatic java/util/concurrent/TimeUnit/MINUTES Ljava/util/concurrent/TimeUnit;
lload 0
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L2
getstatic java/util/concurrent/TimeUnit/MINUTES Ljava/util/concurrent/TimeUnit;
areturn
L2:
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
lload 0
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L3
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
areturn
L3:
getstatic java/util/concurrent/TimeUnit/MILLISECONDS Ljava/util/concurrent/TimeUnit;
lload 0
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L4
getstatic java/util/concurrent/TimeUnit/MILLISECONDS Ljava/util/concurrent/TimeUnit;
areturn
L4:
getstatic java/util/concurrent/TimeUnit/MICROSECONDS Ljava/util/concurrent/TimeUnit;
lload 0
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L5
getstatic java/util/concurrent/TimeUnit/MICROSECONDS Ljava/util/concurrent/TimeUnit;
areturn
L5:
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b(Lcom/a/b/b/ej;)Lcom/a/b/b/dw;
new com/a/b/b/dw
dup
aload 0
invokespecial com/a/b/b/dw/<init>(Lcom/a/b/b/ej;)V
invokevirtual com/a/b/b/dw/b()Lcom/a/b/b/dw;
areturn
.limit locals 1
.limit stack 3
.end method

.method private static b(Ljava/util/concurrent/TimeUnit;)Ljava/lang/String;
getstatic com/a/b/b/dx/a [I
aload 0
invokevirtual java/util/concurrent/TimeUnit/ordinal()I
iaload
tableswitch 1
L0
L1
L2
L3
L4
L5
L6
default : L7
L7:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
ldc "ns"
areturn
L1:
ldc "\u03bcs"
areturn
L2:
ldc "ms"
areturn
L3:
ldc "s"
areturn
L4:
ldc "min"
areturn
L5:
ldc "h"
areturn
L6:
ldc "d"
areturn
.limit locals 1
.limit stack 2
.end method

.method private static d()Lcom/a/b/b/dw;
new com/a/b/b/dw
dup
invokespecial com/a/b/b/dw/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private e()Z
aload 0
getfield com/a/b/b/dw/a Z
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Lcom/a/b/b/dw;
aload 0
lconst_0
putfield com/a/b/b/dw/c J
aload 0
iconst_0
putfield com/a/b/b/dw/a Z
aload 0
areturn
.limit locals 1
.limit stack 3
.end method

.method private g()J
aload 0
getfield com/a/b/b/dw/a Z
ifeq L0
aload 0
getfield com/a/b/b/dw/b Lcom/a/b/b/ej;
invokevirtual com/a/b/b/ej/a()J
aload 0
getfield com/a/b/b/dw/d J
lsub
aload 0
getfield com/a/b/b/dw/c J
ladd
lreturn
L0:
aload 0
getfield com/a/b/b/dw/c J
lreturn
.limit locals 1
.limit stack 4
.end method

.method public final a(Ljava/util/concurrent/TimeUnit;)J
aload 1
aload 0
invokespecial com/a/b/b/dw/g()J
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lreturn
.limit locals 2
.limit stack 4
.end method

.method public final b()Lcom/a/b/b/dw;
aload 0
getfield com/a/b/b/dw/a Z
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "This stopwatch is already running."
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
iconst_1
putfield com/a/b/b/dw/a Z
aload 0
aload 0
getfield com/a/b/b/dw/b Lcom/a/b/b/ej;
invokevirtual com/a/b/b/ej/a()J
putfield com/a/b/b/dw/d J
aload 0
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 3
.end method

.method public final c()Lcom/a/b/b/dw;
aload 0
getfield com/a/b/b/dw/b Lcom/a/b/b/ej;
invokevirtual com/a/b/b/ej/a()J
lstore 1
aload 0
getfield com/a/b/b/dw/a Z
ldc "This stopwatch is already stopped."
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
iconst_0
putfield com/a/b/b/dw/a Z
aload 0
getfield com/a/b/b/dw/c J
lstore 3
aload 0
lload 1
aload 0
getfield com/a/b/b/dw/d J
lsub
lload 3
ladd
putfield com/a/b/b/dw/c J
aload 0
areturn
.limit locals 5
.limit stack 5
.end method

.method public final toString()Ljava/lang/String;
.annotation invisible Lcom/a/b/a/c;
a s = "String.format()"
.end annotation
aload 0
invokespecial com/a/b/b/dw/g()J
lstore 3
getstatic java/util/concurrent/TimeUnit/DAYS Ljava/util/concurrent/TimeUnit;
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L0
getstatic java/util/concurrent/TimeUnit/DAYS Ljava/util/concurrent/TimeUnit;
astore 5
L1:
lload 3
l2d
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
lconst_1
aload 5
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
l2d
ddiv
dstore 1
getstatic com/a/b/b/dx/a [I
aload 5
invokevirtual java/util/concurrent/TimeUnit/ordinal()I
iaload
tableswitch 1
L2
L3
L4
L5
L6
L7
L8
default : L9
L9:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L0:
getstatic java/util/concurrent/TimeUnit/HOURS Ljava/util/concurrent/TimeUnit;
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L10
getstatic java/util/concurrent/TimeUnit/HOURS Ljava/util/concurrent/TimeUnit;
astore 5
goto L1
L10:
getstatic java/util/concurrent/TimeUnit/MINUTES Ljava/util/concurrent/TimeUnit;
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L11
getstatic java/util/concurrent/TimeUnit/MINUTES Ljava/util/concurrent/TimeUnit;
astore 5
goto L1
L11:
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L12
getstatic java/util/concurrent/TimeUnit/SECONDS Ljava/util/concurrent/TimeUnit;
astore 5
goto L1
L12:
getstatic java/util/concurrent/TimeUnit/MILLISECONDS Ljava/util/concurrent/TimeUnit;
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L13
getstatic java/util/concurrent/TimeUnit/MILLISECONDS Ljava/util/concurrent/TimeUnit;
astore 5
goto L1
L13:
getstatic java/util/concurrent/TimeUnit/MICROSECONDS Ljava/util/concurrent/TimeUnit;
lload 3
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
invokevirtual java/util/concurrent/TimeUnit/convert(JLjava/util/concurrent/TimeUnit;)J
lconst_0
lcmp
ifle L14
getstatic java/util/concurrent/TimeUnit/MICROSECONDS Ljava/util/concurrent/TimeUnit;
astore 5
goto L1
L14:
getstatic java/util/concurrent/TimeUnit/NANOSECONDS Ljava/util/concurrent/TimeUnit;
astore 5
goto L1
L2:
ldc "ns"
astore 5
L15:
ldc "%.4g %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
dload 1
invokestatic java/lang/Double/valueOf(D)Ljava/lang/Double;
aastore
dup
iconst_1
aload 5
aastore
invokestatic java/lang/String/format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
areturn
L3:
ldc "\u03bcs"
astore 5
goto L15
L4:
ldc "ms"
astore 5
goto L15
L5:
ldc "s"
astore 5
goto L15
L6:
ldc "min"
astore 5
goto L15
L7:
ldc "h"
astore 5
goto L15
L8:
ldc "d"
astore 5
goto L15
.limit locals 6
.limit stack 6
.end method
