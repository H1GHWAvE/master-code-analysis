.bytecode 50.0
.class public synchronized abstract com/a/b/b/ak
.super java/lang/Object
.implements com/a/b/b/bj
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final 'a' Z

.field private transient 'b' Lcom/a/b/b/ak;

.method public <init>()V
aload 0
iconst_0
invokespecial com/a/b/b/ak/<init>(B)V
return
.limit locals 1
.limit stack 2
.end method

.method private <init>(B)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield com/a/b/b/ak/a Z
return
.limit locals 2
.limit stack 2
.end method

.method private static a(Lcom/a/b/b/bj;Lcom/a/b/b/bj;)Lcom/a/b/b/ak;
new com/a/b/b/ao
dup
aload 0
aload 1
iconst_0
invokespecial com/a/b/b/ao/<init>(Lcom/a/b/b/bj;Lcom/a/b/b/bj;B)V
areturn
.limit locals 2
.limit stack 5
.end method

.method private a(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 1
ldc "fromIterable"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/b/al
dup
aload 0
aload 1
invokespecial com/a/b/b/al/<init>(Lcom/a/b/b/ak;Ljava/lang/Iterable;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static b()Lcom/a/b/b/ak;
getstatic com/a/b/b/ap/a Lcom/a/b/b/ap;
areturn
.limit locals 0
.limit stack 1
.end method

.method private b(Lcom/a/b/b/ak;)Lcom/a/b/b/ak;
aload 0
aload 1
invokevirtual com/a/b/b/ak/a(Lcom/a/b/b/ak;)Lcom/a/b/b/ak;
areturn
.limit locals 2
.limit stack 2
.end method

.method private f(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/b/ak/c(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public a()Lcom/a/b/b/ak;
aload 0
getfield com/a/b/b/ak/b Lcom/a/b/b/ak;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/b/aq
dup
aload 0
invokespecial com/a/b/b/aq/<init>(Lcom/a/b/b/ak;)V
astore 1
aload 0
aload 1
putfield com/a/b/b/ak/b Lcom/a/b/b/ak;
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method a(Lcom/a/b/b/ak;)Lcom/a/b/b/ak;
new com/a/b/b/an
dup
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/ak
invokespecial com/a/b/b/an/<init>(Lcom/a/b/b/ak;Lcom/a/b/b/ak;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract b(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method c(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/b/ak/a Z
ifeq L0
aload 1
ifnonnull L1
aconst_null
areturn
L1:
aload 0
aload 1
invokevirtual com/a/b/b/ak/b(Ljava/lang/Object;)Ljava/lang/Object;
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/b/ak/b(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method d(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/b/ak/a Z
ifeq L0
aload 1
ifnonnull L1
aconst_null
areturn
L1:
aload 0
aload 1
invokevirtual com/a/b/b/ak/a(Ljava/lang/Object;)Ljava/lang/Object;
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
L0:
aload 0
aload 1
invokevirtual com/a/b/b/ak/a(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final e(Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljava/lang/Deprecated;
.end annotation
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/b/ak/c(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method
