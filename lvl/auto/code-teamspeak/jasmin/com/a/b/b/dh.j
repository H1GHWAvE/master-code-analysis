.bytecode 50.0
.class final synchronized com/a/b/b/dh
.super com/a/b/b/ae
.annotation invisible Lcom/a/b/a/c;
a s = "no precomputation is done in GWT"
.end annotation

.field static final 's' I = 1023


.field private static final 'w' I = -862048943


.field private static final 'x' I = 461845907


.field private static final 'y' D = 0.5D


.field private final 't' [C

.field private final 'u' Z

.field private final 'v' J

.method <init>([CJZLjava/lang/String;)V
aload 0
aload 5
invokespecial com/a/b/b/ae/<init>(Ljava/lang/String;)V
aload 0
aload 1
putfield com/a/b/b/dh/t [C
aload 0
lload 2
putfield com/a/b/b/dh/v J
aload 0
iload 4
putfield com/a/b/b/dh/u Z
return
.limit locals 6
.limit stack 3
.end method

.method static a(I)I
ldc_w 461845907
ldc_w -862048943
iload 0
imul
bipush 15
invokestatic java/lang/Integer/rotateLeft(II)I
imul
ireturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/BitSet;Ljava/lang/String;)Lcom/a/b/b/m;
lconst_0
lstore 5
aload 0
invokevirtual java/util/BitSet/cardinality()I
istore 4
aload 0
iconst_0
invokevirtual java/util/BitSet/get(I)Z
istore 7
iload 4
iconst_1
if_icmpne L0
iconst_2
istore 3
L1:
iload 3
newarray char
astore 8
aload 8
arraylength
iconst_1
isub
istore 4
aload 0
iconst_0
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 2
L2:
iload 2
iconst_m1
if_icmpeq L3
lload 5
lconst_1
iload 2
lshl
lor
lstore 5
iload 2
invokestatic com/a/b/b/dh/a(I)I
iload 4
iand
istore 3
L4:
aload 8
iload 3
caload
ifne L5
aload 8
iload 3
iload 2
i2c
castore
aload 0
iload 2
iconst_1
iadd
invokevirtual java/util/BitSet/nextSetBit(I)I
istore 2
goto L2
L0:
iload 4
iconst_1
isub
invokestatic java/lang/Integer/highestOneBit(I)I
iconst_1
ishl
istore 2
L6:
iload 2
istore 3
iload 2
i2d
ldc2_w 0.5D
dmul
iload 4
i2d
dcmpg
ifge L1
iload 2
iconst_1
ishl
istore 2
goto L6
L5:
iload 3
iconst_1
iadd
iload 4
iand
istore 3
goto L4
L3:
new com/a/b/b/dh
dup
aload 8
lload 5
iload 7
aload 1
invokespecial com/a/b/b/dh/<init>([CJZLjava/lang/String;)V
areturn
.limit locals 9
.limit stack 7
.end method

.method private b(I)Z
lconst_1
aload 0
getfield com/a/b/b/dh/v J
iload 1
lshr
lconst_1
land
lcmp
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 6
.end method

.method private static c(I)I
.annotation invisible Lcom/a/b/a/d;
.end annotation
iload 0
iconst_1
if_icmpne L0
iconst_2
istore 2
L1:
iload 2
ireturn
L0:
iload 0
iconst_1
isub
invokestatic java/lang/Integer/highestOneBit(I)I
iconst_1
ishl
istore 1
L2:
iload 1
istore 2
iload 1
i2d
ldc2_w 0.5D
dmul
iload 0
i2d
dcmpg
ifge L1
iload 1
iconst_1
ishl
istore 1
goto L2
.limit locals 3
.limit stack 4
.end method

.method final a(Ljava/util/BitSet;)V
iconst_0
istore 2
aload 0
getfield com/a/b/b/dh/u Z
ifeq L0
aload 1
iconst_0
invokevirtual java/util/BitSet/set(I)V
L0:
aload 0
getfield com/a/b/b/dh/t [C
astore 5
aload 5
arraylength
istore 3
L1:
iload 2
iload 3
if_icmpge L2
aload 5
iload 2
caload
istore 4
iload 4
ifeq L3
aload 1
iload 4
invokevirtual java/util/BitSet/set(I)V
L3:
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
return
.limit locals 6
.limit stack 2
.end method

.method public final c(C)Z
iload 1
ifne L0
aload 0
getfield com/a/b/b/dh/u Z
ireturn
L0:
lconst_1
aload 0
getfield com/a/b/b/dh/v J
iload 1
lshr
lconst_1
land
lcmp
ifne L1
iconst_1
istore 2
L2:
iload 2
ifne L3
iconst_0
ireturn
L1:
iconst_0
istore 2
goto L2
L3:
aload 0
getfield com/a/b/b/dh/t [C
arraylength
iconst_1
isub
istore 5
iload 1
invokestatic com/a/b/b/dh/a(I)I
iload 5
iand
istore 3
iload 3
istore 2
L4:
aload 0
getfield com/a/b/b/dh/t [C
iload 2
caload
ifne L5
iconst_0
ireturn
L5:
aload 0
getfield com/a/b/b/dh/t [C
iload 2
caload
iload 1
if_icmpne L6
iconst_1
ireturn
L6:
iload 2
iconst_1
iadd
iload 5
iand
istore 4
iload 4
istore 2
iload 4
iload 3
if_icmpne L4
iconst_0
ireturn
.limit locals 6
.limit stack 6
.end method
