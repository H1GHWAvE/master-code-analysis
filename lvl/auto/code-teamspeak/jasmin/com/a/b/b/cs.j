.bytecode 50.0
.class final synchronized com/a/b/b/cs
.super java/lang/Object
.implements com/a/b/b/co
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/c;
a s = "Class.isAssignableFrom"
.end annotation

.field private static final 'b' J = 0L


.field private final 'a' Ljava/lang/Class;

.method private <init>(Ljava/lang/Class;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Class
putfield com/a/b/b/cs/a Ljava/lang/Class;
return
.limit locals 2
.limit stack 2
.end method

.method synthetic <init>(Ljava/lang/Class;B)V
aload 0
aload 1
invokespecial com/a/b/b/cs/<init>(Ljava/lang/Class;)V
return
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/Class;)Z
aload 0
getfield com/a/b/b/cs/a Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic a(Ljava/lang/Object;)Z
aload 1
checkcast java/lang/Class
astore 1
aload 0
getfield com/a/b/b/cs/a Ljava/lang/Class;
aload 1
invokevirtual java/lang/Class/isAssignableFrom(Ljava/lang/Class;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iconst_0
istore 3
iload 3
istore 2
aload 1
instanceof com/a/b/b/cs
ifeq L0
aload 1
checkcast com/a/b/b/cs
astore 1
iload 3
istore 2
aload 0
getfield com/a/b/b/cs/a Ljava/lang/Class;
aload 1
getfield com/a/b/b/cs/a Ljava/lang/Class;
if_acmpne L0
iconst_1
istore 2
L0:
iload 2
ireturn
.limit locals 4
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/b/cs/a Ljava/lang/Class;
invokevirtual java/lang/Object/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/b/cs/a Ljava/lang/Class;
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 27
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Predicates.assignableFrom("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
