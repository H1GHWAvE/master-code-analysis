.bytecode 50.0
.class final synchronized com/a/b/d/wr
.super com/a/b/d/an
.implements com/a/b/d/aac
.implements java/io/Serializable

.field private static final 'b' J = 7845222491160860175L


.field final 'a' Ljava/util/Map;

.method <init>(Ljava/util/Map;)V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/d/wr/a Ljava/util/Map;
return
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/Set;
new com/a/b/d/ws
dup
aload 0
aload 1
invokespecial com/a/b/d/ws/<init>(Lcom/a/b/d/wr;Ljava/lang/Object;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final a(Lcom/a/b/d/vi;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final synthetic b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/wr/a(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Set;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/Set;
new java/util/HashSet
dup
iconst_2
invokespecial java/util/HashSet/<init>(I)V
astore 2
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifne L0
aload 2
areturn
L0:
aload 2
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
invokeinterface java/util/Set/add(Ljava/lang/Object;)Z 1
pop
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
aload 1
aload 2
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/wr/a(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
aload 1
aload 2
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method public final synthetic d(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/wr/b(Ljava/lang/Object;)Ljava/util/Set;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final f()I
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final g(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsValue(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final i()Ljava/util/Collection;
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic k()Ljava/util/Collection;
aload 0
invokevirtual com/a/b/d/wr/u()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method final l()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method final m()Ljava/util/Map;
new com/a/b/d/wf
dup
aload 0
invokespecial com/a/b/d/wf/<init>(Lcom/a/b/d/vi;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final p()Ljava/util/Set;
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final u()Ljava/util/Set;
aload 0
getfield com/a/b/d/wr/a Ljava/util/Map;
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method
