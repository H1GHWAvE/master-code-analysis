.bytecode 50.0
.class synchronized abstract com/a/b/d/tl
.super com/a/b/d/uj

.field final 'a' Ljava/util/Map;

.field final 'b' Lcom/a/b/b/co;

.method <init>(Ljava/util/Map;Lcom/a/b/b/co;)V
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 1
putfield com/a/b/d/tl/a Ljava/util/Map;
aload 0
aload 2
putfield com/a/b/d/tl/b Lcom/a/b/b/co;
return
.limit locals 3
.limit stack 2
.end method

.method final b(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/tl/b Lcom/a/b/b/co;
aload 1
aload 2
invokestatic com/a/b/d/sz/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map$Entry;
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 3
.end method

.method final c_()Ljava/util/Collection;
new com/a/b/d/ui
dup
aload 0
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 0
getfield com/a/b/d/tl/b Lcom/a/b/b/co;
invokespecial com/a/b/d/ui/<init>(Ljava/util/Map;Ljava/util/Map;Lcom/a/b/b/co;)V
areturn
.limit locals 1
.limit stack 5
.end method

.method public containsKey(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
aload 1
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
invokevirtual com/a/b/d/tl/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 2
aload 2
ifnull L0
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/tl/b(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 2
areturn
L0:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public isEmpty()Z
aload 0
invokevirtual com/a/b/d/tl/entrySet()Ljava/util/Set;
invokeinterface java/util/Set/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/tl/b(Ljava/lang/Object;Ljava/lang/Object;)Z
invokestatic com/a/b/b/cn/a(Z)V
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 1
aload 2
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public putAll(Ljava/util/Map;)V
aload 1
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual com/a/b/d/tl/b(Ljava/lang/Object;Ljava/lang/Object;)Z
invokestatic com/a/b/b/cn/a(Z)V
goto L0
L1:
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/putAll(Ljava/util/Map;)V 1
return
.limit locals 4
.limit stack 3
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual com/a/b/d/tl/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/tl/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method
