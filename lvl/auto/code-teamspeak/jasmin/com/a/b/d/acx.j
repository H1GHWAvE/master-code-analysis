.bytecode 50.0
.class final synchronized com/a/b/d/acx
.super com/a/b/d/adq

.field private static final 'a' J = 0L


.method <init>(Ljava/util/Set;Ljava/lang/Object;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/adq/<init>(Ljava/util/Set;Ljava/lang/Object;)V
return
.limit locals 3
.limit stack 3
.end method

.method public final contains(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Collection;Ljava/lang/Object;)Z
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final containsAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/util/Collection;)Z
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 1
aload 0
if_acmpne L5
iconst_1
ireturn
L5:
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Ljava/lang/Object;)Z
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/acy
dup
aload 0
aload 0
invokespecial com/a/b/d/adq/iterator()Ljava/util/Iterator;
invokespecial com/a/b/d/acy/<init>(Lcom/a/b/d/acx;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final remove(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
aload 1
invokestatic com/a/b/d/sz/b(Ljava/util/Collection;Ljava/lang/Object;)Z
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final removeAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/util/Collection;)Z
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final retainAll(Ljava/util/Collection;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Ljava/util/Collection;)Z
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final toArray()[Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
invokestatic com/a/b/d/yc/a(Ljava/util/Collection;)[Ljava/lang/Object;
astore 2
aload 1
monitorexit
L1:
aload 2
areturn
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/acx/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/acx/c()Ljava/util/Set;
aload 1
invokestatic com/a/b/d/yc/a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method
