.bytecode 50.0
.class final synchronized com/a/b/d/afw
.super com/a/b/d/afm

.field final synthetic 'b' Lcom/a/b/d/afm;

.field private final 'c' Lcom/a/b/d/yl;

.method <init>(Lcom/a/b/d/afm;Lcom/a/b/d/yl;)V
aload 0
aload 1
putfield com/a/b/d/afw/b Lcom/a/b/d/afm;
aload 0
new com/a/b/d/afx
dup
invokestatic com/a/b/d/yl/c()Lcom/a/b/d/yl;
aload 2
aload 1
getfield com/a/b/d/afm/a Ljava/util/NavigableMap;
iconst_0
invokespecial com/a/b/d/afx/<init>(Lcom/a/b/d/yl;Lcom/a/b/d/yl;Ljava/util/NavigableMap;B)V
iconst_0
invokespecial com/a/b/d/afm/<init>(Ljava/util/NavigableMap;B)V
aload 0
aload 2
putfield com/a/b/d/afw/c Lcom/a/b/d/yl;
return
.limit locals 3
.limit stack 7
.end method

.method public final a(Lcom/a/b/d/yl;)V
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
aload 1
invokevirtual com/a/b/d/yl/a(Lcom/a/b/d/yl;)Z
ldc "Cannot add range %s to subRangeSet(%s)"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokespecial com/a/b/d/afm/a(Lcom/a/b/d/yl;)V
return
.limit locals 2
.limit stack 6
.end method

.method public final a(Ljava/lang/Comparable;)Z
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
aload 1
invokevirtual com/a/b/d/yl/c(Ljava/lang/Comparable;)Z
ifeq L0
aload 0
getfield com/a/b/d/afw/b Lcom/a/b/d/afm;
aload 1
invokevirtual com/a/b/d/afm/a(Ljava/lang/Comparable;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
aload 1
invokevirtual com/a/b/d/yl/c(Ljava/lang/Comparable;)Z
ifne L0
L1:
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/afw/b Lcom/a/b/d/afm;
aload 1
invokevirtual com/a/b/d/afm/b(Ljava/lang/Comparable;)Lcom/a/b/d/yl;
astore 1
aload 1
ifnull L1
aload 1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final b()V
aload 0
getfield com/a/b/d/afw/b Lcom/a/b/d/afm;
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/afm/b(Lcom/a/b/d/yl;)V
return
.limit locals 1
.limit stack 2
.end method

.method public final b(Lcom/a/b/d/yl;)V
aload 1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/b(Lcom/a/b/d/yl;)Z
ifeq L0
aload 0
getfield com/a/b/d/afw/b Lcom/a/b/d/afm;
aload 1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
invokevirtual com/a/b/d/afm/b(Lcom/a/b/d/yl;)V
L0:
return
.limit locals 2
.limit stack 3
.end method

.method public final c(Lcom/a/b/d/yl;)Z
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/f()Z
ifne L0
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
aload 1
invokevirtual com/a/b/d/yl/a(Lcom/a/b/d/yl;)Z
ifeq L0
aload 0
getfield com/a/b/d/afw/b Lcom/a/b/d/afm;
astore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
getfield com/a/b/d/afm/a Ljava/util/NavigableMap;
aload 1
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
invokeinterface java/util/NavigableMap/floorEntry(Ljava/lang/Object;)Ljava/util/Map$Entry; 1
astore 2
aload 2
ifnull L1
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
aload 1
invokevirtual com/a/b/d/yl/a(Lcom/a/b/d/yl;)Z
ifeq L1
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
astore 1
L2:
aload 1
ifnull L3
aload 1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/f()Z
ifne L3
iconst_1
ireturn
L1:
aconst_null
astore 1
goto L2
L3:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final e(Lcom/a/b/d/yl;)Lcom/a/b/d/yr;
aload 1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/a(Lcom/a/b/d/yl;)Z
ifeq L0
aload 0
areturn
L0:
aload 1
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/b(Lcom/a/b/d/yl;)Z
ifeq L1
new com/a/b/d/afw
dup
aload 0
aload 0
getfield com/a/b/d/afw/c Lcom/a/b/d/yl;
aload 1
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
invokespecial com/a/b/d/afw/<init>(Lcom/a/b/d/afm;Lcom/a/b/d/yl;)V
areturn
L1:
invokestatic com/a/b/d/lf/c()Lcom/a/b/d/lf;
areturn
.limit locals 2
.limit stack 5
.end method
