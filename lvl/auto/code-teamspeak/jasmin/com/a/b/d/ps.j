.bytecode 50.0
.class final synchronized com/a/b/d/ps
.super com/a/b/d/hi

.field private final 'a' Lcom/a/b/d/pn;

.field private final 'b' Ljava/util/Set;

.method <init>(Ljava/util/Set;Lcom/a/b/d/pn;)V
aload 0
invokespecial com/a/b/d/hi/<init>()V
aload 0
aload 1
putfield com/a/b/d/ps/b Ljava/util/Set;
aload 0
aload 2
putfield com/a/b/d/ps/a Lcom/a/b/d/pn;
return
.limit locals 3
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/ps;)Lcom/a/b/d/pn;
aload 0
getfield com/a/b/d/ps/a Lcom/a/b/d/pn;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final a()Ljava/util/Set;
aload 0
getfield com/a/b/d/ps/b Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method protected final volatile synthetic b()Ljava/util/Collection;
aload 0
getfield com/a/b/d/ps/b Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final contains(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/ps/b Ljava/util/Set;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/util/Collection;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final containsAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
invokestatic com/a/b/d/aad/a(Ljava/util/Set;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final iterator()Ljava/util/Iterator;
new com/a/b/d/pt
dup
aload 0
aload 0
getfield com/a/b/d/ps/b Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/pt/<init>(Lcom/a/b/d/ps;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/ps/b Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final remove(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/ps/b Ljava/util/Set;
aload 1
invokestatic com/a/b/d/sz/b(Ljava/util/Collection;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokevirtual com/a/b/d/ps/b(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokevirtual com/a/b/d/ps/c(Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final toArray()[Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ps/p()[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final toArray([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
aload 1
invokestatic com/a/b/d/yc/a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method
