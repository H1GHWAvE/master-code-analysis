.bytecode 50.0
.class final synchronized com/a/b/d/eq
.super com/a/b/d/ep
.implements java/io/Serializable

.field private static final 'a' Lcom/a/b/d/eq;

.field private static final 'b' Ljava/math/BigInteger;

.field private static final 'c' Ljava/math/BigInteger;

.field private static final 'd' J = 0L


.method static <clinit>()V
new com/a/b/d/eq
dup
invokespecial com/a/b/d/eq/<init>()V
putstatic com/a/b/d/eq/a Lcom/a/b/d/eq;
ldc2_w -9223372036854775808L
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
putstatic com/a/b/d/eq/b Ljava/math/BigInteger;
ldc2_w 9223372036854775807L
invokestatic java/math/BigInteger/valueOf(J)Ljava/math/BigInteger;
putstatic com/a/b/d/eq/c Ljava/math/BigInteger;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial com/a/b/d/ep/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/math/BigInteger;Ljava/math/BigInteger;)J
aload 1
aload 0
invokevirtual java/math/BigInteger/subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;
getstatic com/a/b/d/eq/b Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/max(Ljava/math/BigInteger;)Ljava/math/BigInteger;
getstatic com/a/b/d/eq/c Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/min(Ljava/math/BigInteger;)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/longValue()J
lreturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/math/BigInteger;)Ljava/math/BigInteger;
aload 0
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Ljava/math/BigInteger;)Ljava/math/BigInteger;
aload 0
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 1
.limit stack 2
.end method

.method static synthetic c()Lcom/a/b/d/eq;
getstatic com/a/b/d/eq/a Lcom/a/b/d/eq;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static d()Ljava/lang/Object;
getstatic com/a/b/d/eq/a Lcom/a/b/d/eq;
areturn
.limit locals 0
.limit stack 1
.end method

.method public final synthetic a(Ljava/lang/Comparable;Ljava/lang/Comparable;)J
aload 1
checkcast java/math/BigInteger
astore 1
aload 2
checkcast java/math/BigInteger
aload 1
invokevirtual java/math/BigInteger/subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;
getstatic com/a/b/d/eq/b Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/max(Ljava/math/BigInteger;)Ljava/math/BigInteger;
getstatic com/a/b/d/eq/c Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/min(Ljava/math/BigInteger;)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/longValue()J
lreturn
.limit locals 3
.limit stack 2
.end method

.method public final synthetic a(Ljava/lang/Comparable;)Ljava/lang/Comparable;
aload 1
checkcast java/math/BigInteger
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/add(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic b(Ljava/lang/Comparable;)Ljava/lang/Comparable;
aload 1
checkcast java/math/BigInteger
getstatic java/math/BigInteger/ONE Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final toString()Ljava/lang/String;
ldc "DiscreteDomain.bigIntegers()"
areturn
.limit locals 1
.limit stack 1
.end method
