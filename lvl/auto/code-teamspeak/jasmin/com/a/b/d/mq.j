.bytecode 50.0
.class public final synchronized com/a/b/d/mq
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a()Lcom/a/b/b/bj;
new com/a/b/d/my
dup
invokespecial com/a/b/d/my/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static a(Lcom/a/b/d/iz;)Ljava/lang/Iterable;
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Iterable
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
new com/a/b/d/nb
dup
aload 0
iload 1
invokespecial com/a/b/d/nb/<init>(Ljava/lang/Iterable;I)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public static a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/nf
dup
aload 0
aload 1
invokespecial com/a/b/d/nf/<init>(Ljava/lang/Iterable;Lcom/a/b/b/bj;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
aload 1
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
aload 1
aload 2
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
aload 1
aload 2
aload 3
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/lang/Iterable;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
ldc "iterables"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
ldc "comparator"
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/ni
dup
new com/a/b/d/mx
dup
aload 0
aload 1
invokespecial com/a/b/d/mx/<init>(Ljava/lang/Iterable;Ljava/util/Comparator;)V
iconst_0
invokespecial com/a/b/d/ni/<init>(Ljava/lang/Iterable;B)V
areturn
.limit locals 2
.limit stack 6
.end method

.method private static transient a([Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/d/jl/a([Ljava/lang/Object;)Lcom/a/b/d/jl;
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static transient a([Ljava/lang/Object;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/d/ov/a([Ljava/lang/Object;)Ljava/util/ArrayList;
invokestatic com/a/b/d/mq/d(Ljava/lang/Iterable;)Ljava/lang/Iterable;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/lang/Iterable;ILjava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
invokestatic com/a/b/d/nj/a(I)V
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
astore 0
iload 1
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L1
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 2
L1:
aload 2
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
aload 0
iload 1
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;I)I
pop
aload 0
aload 2
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;Lcom/a/b/b/co;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
aload 2
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Lcom/a/b/b/co;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method static a(Ljava/util/List;)Ljava/lang/Object;
aload 0
aload 0
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
.limit locals 1
.limit stack 3
.end method

.method public static a(Ljava/lang/Iterable;)Ljava/lang/String;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/c(Ljava/util/Iterator;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static a(Ljava/util/List;Lcom/a/b/b/co;II)V
aload 0
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 4
L0:
iload 4
iload 3
if_icmple L1
aload 1
aload 0
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L2
aload 0
iload 4
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
L2:
iload 4
iconst_1
isub
istore 4
goto L0
L1:
iload 3
iconst_1
isub
istore 3
L3:
iload 3
iload 2
if_icmplt L4
aload 0
iload 3
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
iload 3
iconst_1
isub
istore 3
goto L3
L4:
return
.limit locals 5
.limit stack 3
.end method

.method public static a(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
aload 0
instanceof java/util/RandomAccess
ifeq L0
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
invokestatic com/a/b/d/mq/a(Ljava/util/List;Lcom/a/b/b/co;)Z
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;Ljava/lang/Object;)Z
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/lang/Object;)Z
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;Ljava/util/Collection;)Z
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
invokeinterface java/util/Collection/removeAll(Ljava/util/Collection;)Z 1
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
aload 1
instanceof java/util/Collection
ifeq L0
aload 0
aload 1
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
ireturn
L0:
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Iterable
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/util/List;Lcom/a/b/b/co;)Z
.catch java/lang/UnsupportedOperationException from L0 to L1 using L2
iconst_0
istore 5
iconst_0
istore 2
iconst_0
istore 3
L3:
iload 3
aload 0
invokeinterface java/util/List/size()I 0
if_icmpge L4
aload 0
iload 3
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
astore 6
iload 2
istore 4
aload 1
aload 6
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifne L5
iload 3
iload 2
if_icmple L1
L0:
aload 0
iload 2
aload 6
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
pop
L1:
iload 2
iconst_1
iadd
istore 4
L5:
iload 3
iconst_1
iadd
istore 3
iload 4
istore 2
goto L3
L2:
astore 6
aload 0
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 4
L6:
iload 4
iload 3
if_icmple L7
aload 1
aload 0
iload 4
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L8
aload 0
iload 4
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
L8:
iload 4
iconst_1
isub
istore 4
goto L6
L7:
iload 3
iconst_1
isub
istore 3
L9:
iload 3
iload 2
if_icmplt L10
aload 0
iload 3
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
pop
iload 3
iconst_1
isub
istore 3
goto L9
L10:
iconst_1
istore 5
L11:
iload 5
ireturn
L4:
aload 0
iload 2
aload 0
invokeinterface java/util/List/size()I 0
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
invokeinterface java/util/List/clear()V 0
iload 3
iload 2
if_icmpeq L11
iconst_1
ireturn
.limit locals 7
.limit stack 3
.end method

.method public static a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/c;
a s = "Array.newInstance(Class, int)"
.end annotation
aload 0
invokestatic com/a/b/d/mq/i(Ljava/lang/Iterable;)Ljava/util/Collection;
astore 0
aload 0
aload 1
aload 0
invokeinterface java/util/Collection/size()I 0
invokestatic com/a/b/d/yc/a(Ljava/lang/Class;I)[Ljava/lang/Object;
invokeinterface java/util/Collection/toArray([Ljava/lang/Object;)[Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static synthetic b()Lcom/a/b/b/bj;
new com/a/b/d/my
dup
invokespecial com/a/b/d/my/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
ifle L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
new com/a/b/d/nc
dup
aload 0
iload 1
invokespecial com/a/b/d/nc/<init>(Ljava/lang/Iterable;I)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method private static b(Ljava/lang/Iterable;Ljava/lang/Class;)Ljava/lang/Iterable;
.annotation invisible Lcom/a/b/a/c;
a s = "Class.isInstance"
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/ne
dup
aload 0
aload 1
invokespecial com/a/b/d/ne/<init>(Ljava/lang/Iterable;Ljava/lang/Class;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public static b(Ljava/lang/Iterable;)Ljava/lang/Object;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method static b(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
aload 1
aload 2
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
invokeinterface java/util/Iterator/remove()V 0
aload 2
areturn
L1:
aconst_null
areturn
.limit locals 3
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;Ljava/lang/Iterable;)Z
aload 0
instanceof java/util/Collection
ifeq L0
aload 1
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
astore 2
aload 1
checkcast java/util/Collection
astore 3
aload 2
invokeinterface java/util/Collection/size()I 0
aload 3
invokeinterface java/util/Collection/size()I 0
if_icmpeq L0
iconst_0
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/util/Iterator;)Z
ireturn
.limit locals 4
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;Ljava/util/Collection;)Z
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Collection
invokeinterface java/util/Collection/retainAll(Ljava/util/Collection;)Z 1
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static c(Ljava/lang/Iterable;Ljava/lang/Object;)I
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof com/a/b/d/xc
ifeq L0
aload 0
checkcast com/a/b/d/xc
aload 1
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
ireturn
L0:
aload 0
instanceof java/util/Set
ifeq L1
aload 0
checkcast java/util/Set
aload 1
invokeinterface java/util/Set/contains(Ljava/lang/Object;)Z 1
ifeq L2
iconst_1
ireturn
L2:
iconst_0
ireturn
L1:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/c(Ljava/util/Iterator;Ljava/lang/Object;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/nd
dup
aload 0
aload 1
invokespecial com/a/b/d/nd/<init>(Ljava/lang/Iterable;Lcom/a/b/b/co;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private static c(Ljava/lang/Iterable;I)Ljava/lang/Object;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
iload 1
invokestatic com/a/b/d/nj/c(Ljava/util/Iterator;I)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method static c(Ljava/lang/Iterable;)[Ljava/lang/Object;
aload 0
invokestatic com/a/b/d/mq/i(Ljava/lang/Iterable;)Ljava/util/Collection;
invokeinterface java/util/Collection/toArray()[Ljava/lang/Object; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public static d(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/mr
dup
aload 0
invokespecial com/a/b/d/mr/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static d(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "number to skip cannot be negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
instanceof java/util/List
ifeq L2
new com/a/b/d/ng
dup
aload 0
checkcast java/util/List
iload 1
invokespecial com/a/b/d/ng/<init>(Ljava/util/List;I)V
areturn
L0:
iconst_0
istore 2
goto L1
L2:
new com/a/b/d/ms
dup
aload 0
iload 1
invokespecial com/a/b/d/ms/<init>(Ljava/lang/Iterable;I)V
areturn
.limit locals 3
.limit stack 4
.end method

.method private static d(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L1
aload 1
areturn
L1:
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
invokestatic com/a/b/d/mq/a(Ljava/util/List;)Ljava/lang/Object;
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/e(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method public static d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/c(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/mz
dup
aload 0
invokespecial com/a/b/d/mz/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static e(Ljava/lang/Iterable;I)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "limit is negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
new com/a/b/d/mu
dup
aload 0
iload 1
invokespecial com/a/b/d/mu/<init>(Ljava/lang/Iterable;I)V
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 4
.end method

.method public static e(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public static f(Ljava/lang/Iterable;)Ljava/lang/Object;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aconst_null
invokestatic com/a/b/d/nj/d(Ljava/util/Iterator;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static f(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Object;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/e(Ljava/util/Iterator;Lcom/a/b/b/co;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static g(Ljava/lang/Iterable;Lcom/a/b/b/co;)Lcom/a/b/b/ci;
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/f(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/b/ci;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static g(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
instanceof com/a/b/d/ni
ifne L0
aload 0
instanceof com/a/b/d/iz
ifeq L1
L0:
aload 0
areturn
L1:
new com/a/b/d/ni
dup
aload 0
iconst_0
invokespecial com/a/b/d/ni/<init>(Ljava/lang/Iterable;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static h(Ljava/lang/Iterable;)I
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
invokeinterface java/util/Collection/size()I 0
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static h(Ljava/lang/Iterable;Lcom/a/b/b/co;)I
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/g(Ljava/util/Iterator;Lcom/a/b/b/co;)I
ireturn
.limit locals 2
.limit stack 2
.end method

.method private static i(Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/ov/a(Ljava/util/Iterator;)Ljava/util/ArrayList;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static j(Ljava/lang/Iterable;)Ljava/util/Iterator;
new com/a/b/d/na
dup
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/na/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static k(Ljava/lang/Iterable;)Ljava/lang/Object;
aload 0
instanceof java/util/List
ifeq L0
aload 0
checkcast java/util/List
astore 0
aload 0
invokeinterface java/util/List/isEmpty()Z 0
ifeq L1
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L1:
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/List;)Ljava/lang/Object;
areturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/f(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static l(Ljava/lang/Iterable;)Ljava/lang/Iterable;
aload 0
instanceof java/util/Queue
ifeq L0
new com/a/b/d/mv
dup
aload 0
invokespecial com/a/b/d/mv/<init>(Ljava/lang/Iterable;)V
areturn
L0:
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/mw
dup
aload 0
invokespecial com/a/b/d/mw/<init>(Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static m(Ljava/lang/Iterable;)Z
aload 0
instanceof java/util/Collection
ifeq L0
aload 0
checkcast java/util/Collection
invokeinterface java/util/Collection/isEmpty()Z 0
ireturn
L0:
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L1
iconst_1
ireturn
L1:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static synthetic n(Ljava/lang/Iterable;)Ljava/util/Iterator;
new com/a/b/d/na
dup
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokespecial com/a/b/d/na/<init>(Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 3
.end method
