.bytecode 50.0
.class public final synchronized com/a/b/d/yc
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field static final 'a' [Ljava/lang/Object;

.method static <clinit>()V
iconst_0
anewarray java/lang/Object
putstatic com/a/b/d/yc/a [Ljava/lang/Object;
return
.limit locals 0
.limit stack 1
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/lang/Object;I)Ljava/lang/Object;
aload 0
ifnonnull L0
new java/lang/NullPointerException
dup
new java/lang/StringBuilder
dup
bipush 20
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "at index "
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
iload 1
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L0:
aload 0
areturn
.limit locals 2
.limit stack 5
.end method

.method static a([Ljava/lang/Object;II)V
aload 0
iload 1
aaload
astore 3
aload 0
iload 1
aload 0
iload 2
aaload
aastore
aload 0
iload 2
aload 3
aastore
return
.limit locals 4
.limit stack 4
.end method

.method public static a(Ljava/lang/Class;I)[Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/c;
a s = "Array.newInstance(Class, int)"
.end annotation
aload 0
iload 1
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;I)Ljava/lang/Object;
checkcast [Ljava/lang/Object;
checkcast [Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;
iconst_0
istore 2
aload 0
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
iload 2
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
aastore
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method private static a(Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 1
arraylength
iconst_1
iadd
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 2
aload 2
iconst_0
aload 0
aastore
aload 1
iconst_0
aload 2
iconst_1
aload 1
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method static a(Ljava/util/Collection;)[Ljava/lang/Object;
aload 0
aload 0
invokeinterface java/util/Collection/size()I 0
anewarray java/lang/Object
invokestatic com/a/b/d/yc/a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a(Ljava/util/Collection;[Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
invokeinterface java/util/Collection/size()I 0
istore 2
aload 1
astore 3
aload 1
arraylength
iload 2
if_icmpge L0
aload 1
iload 2
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 3
L0:
aload 0
aload 3
invokestatic com/a/b/d/yc/a(Ljava/lang/Iterable;[Ljava/lang/Object;)[Ljava/lang/Object;
pop
aload 3
arraylength
iload 2
if_icmple L1
aload 3
iload 2
aconst_null
aastore
L1:
aload 3
areturn
.limit locals 4
.limit stack 3
.end method

.method static transient a([Ljava/lang/Object;)[Ljava/lang/Object;
aload 0
aload 0
arraylength
invokestatic com/a/b/d/yc/c([Ljava/lang/Object;I)[Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 2
.end method

.method public static a([Ljava/lang/Object;I)[Ljava/lang/Object;
aload 0
invokevirtual java/lang/Object/getClass()Ljava/lang/Class;
invokevirtual java/lang/Class/getComponentType()Ljava/lang/Class;
iload 1
invokestatic java/lang/reflect/Array/newInstance(Ljava/lang/Class;I)Ljava/lang/Object;
checkcast [Ljava/lang/Object;
checkcast [Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a([Ljava/lang/Object;II[Ljava/lang/Object;)[Ljava/lang/Object;
iload 1
iload 1
iload 2
iadd
aload 0
arraylength
invokestatic com/a/b/b/cn/a(III)V
aload 3
arraylength
iload 2
if_icmpge L0
aload 3
iload 2
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 4
L1:
aload 0
iload 1
aload 4
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 4
areturn
L0:
aload 3
astore 4
aload 3
arraylength
iload 2
if_icmple L1
aload 3
iload 2
aconst_null
aastore
aload 3
astore 4
goto L1
.limit locals 5
.limit stack 5
.end method

.method private static a([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 0
arraylength
iconst_1
iadd
invokestatic com/a/b/d/yc/b([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 2
aload 2
aload 0
arraylength
aload 1
aastore
aload 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public static a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/c;
a s = "Array.newInstance(Class, int)"
.end annotation
aload 2
aload 0
arraylength
aload 1
arraylength
iadd
invokestatic com/a/b/d/yc/a(Ljava/lang/Class;I)[Ljava/lang/Object;
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 1
iconst_0
aload 2
aload 0
arraylength
aload 1
arraylength
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 5
.end method

.method static b([Ljava/lang/Object;I)[Ljava/lang/Object;
aload 0
iload 1
invokestatic com/a/b/d/yc/a([Ljava/lang/Object;I)[Ljava/lang/Object;
astore 2
aload 0
iconst_0
aload 2
iconst_0
aload 0
arraylength
iload 1
invokestatic java/lang/Math/min(II)I
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 2
areturn
.limit locals 3
.limit stack 6
.end method

.method private static b([Ljava/lang/Object;II)[Ljava/lang/Object;
iload 1
iload 1
iload 2
iadd
aload 0
arraylength
invokestatic com/a/b/b/cn/a(III)V
iload 2
ifne L0
getstatic com/a/b/d/yc/a [Ljava/lang/Object;
areturn
L0:
iload 2
anewarray java/lang/Object
astore 3
aload 0
iload 1
aload 3
iconst_0
iload 2
invokestatic java/lang/System/arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
aload 3
areturn
.limit locals 4
.limit stack 5
.end method

.method static c([Ljava/lang/Object;I)[Ljava/lang/Object;
iconst_0
istore 2
L0:
iload 2
iload 1
if_icmpge L1
aload 0
iload 2
aaload
iload 2
invokestatic com/a/b/d/yc/a(Ljava/lang/Object;I)Ljava/lang/Object;
pop
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
areturn
.limit locals 3
.limit stack 2
.end method
