.bytecode 50.0
.class synchronized com/a/b/d/qc
.super com/a/b/d/gx
.implements java/io/Serializable

.field final 'a' Lcom/a/b/d/pn;

.field final 'b' Lcom/a/b/d/vi;

.field transient 'c' Ljava/util/Collection;

.field transient 'd' Ljava/util/Map;

.method public <init>(Lcom/a/b/d/vi;Lcom/a/b/d/pn;)V
aload 0
invokespecial com/a/b/d/gx/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/vi
putfield com/a/b/d/qc/b Lcom/a/b/d/vi;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/pn
putfield com/a/b/d/qc/a Lcom/a/b/d/pn;
return
.limit locals 3
.limit stack 2
.end method

.method public final a(Lcom/a/b/d/vi;)Z
aload 1
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
iconst_0
istore 2
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 0
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual com/a/b/d/qc/a(Ljava/lang/Object;Ljava/lang/Object;)Z
iload 2
ior
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 4
.limit stack 3
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/qc/a Lcom/a/b/d/pn;
aload 1
aload 2
invokeinterface com/a/b/d/pn/a(Ljava/lang/Object;Ljava/lang/Object;)V 2
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/a(Ljava/lang/Object;Ljava/lang/Object;)Z 2
ireturn
.limit locals 3
.limit stack 3
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
aload 1
aload 1
aload 2
aload 0
getfield com/a/b/d/qc/a Lcom/a/b/d/pn;
invokestatic com/a/b/d/po/a(Ljava/lang/Object;Ljava/lang/Iterable;Lcom/a/b/d/pn;)Ljava/util/Collection;
invokeinterface com/a/b/d/vi/b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection; 2
areturn
.limit locals 3
.limit stack 5
.end method

.method public final b()Ljava/util/Map;
aload 0
getfield com/a/b/d/qc/d Ljava/util/Map;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/qd
dup
aload 0
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokespecial com/a/b/d/qd/<init>(Lcom/a/b/d/qc;Ljava/util/Map;)V
astore 1
aload 0
aload 1
putfield com/a/b/d/qc/d Ljava/util/Map;
L0:
aload 1
areturn
.limit locals 3
.limit stack 4
.end method

.method protected final c()Lcom/a/b/d/vi;
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
new com/a/b/d/qe
dup
aload 0
aload 1
invokespecial com/a/b/d/qe/<init>(Lcom/a/b/d/qc;Ljava/lang/Object;)V
invokestatic com/a/b/d/dn/a(Ljava/util/Collection;Lcom/a/b/d/dm;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 5
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
aload 1
aload 1
aload 2
aload 0
getfield com/a/b/d/qc/a Lcom/a/b/d/pn;
invokestatic com/a/b/d/po/a(Ljava/lang/Object;Ljava/lang/Iterable;Lcom/a/b/d/pn;)Ljava/util/Collection;
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z 2
ireturn
.limit locals 3
.limit stack 5
.end method

.method public k()Ljava/util/Collection;
aload 0
getfield com/a/b/d/qc/c Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
astore 1
aload 0
getfield com/a/b/d/qc/a Lcom/a/b/d/pn;
astore 2
aload 1
instanceof java/util/Set
ifeq L1
aload 1
checkcast java/util/Set
aload 2
invokestatic com/a/b/d/po/a(Ljava/util/Set;Lcom/a/b/d/pn;)Ljava/util/Set;
astore 1
L2:
aload 0
aload 1
putfield com/a/b/d/qc/c Ljava/util/Collection;
L0:
aload 1
areturn
L1:
new com/a/b/d/px
dup
aload 1
aload 2
invokespecial com/a/b/d/px/<init>(Ljava/util/Collection;Lcom/a/b/d/pn;)V
astore 1
goto L2
.limit locals 3
.limit stack 4
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/qc/b Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method
