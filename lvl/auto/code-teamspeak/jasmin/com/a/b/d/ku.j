.bytecode 50.0
.class public synchronized abstract com/a/b/d/ku
.super com/a/b/d/iz
.implements com/a/b/d/xc
.annotation invisible Lcom/a/b/a/b;
a Z = 1
b Z = 1
.end annotation

.field private static final 'a' Lcom/a/b/d/ku;

.field private transient 'b' Lcom/a/b/d/lo;

.method static <clinit>()V
new com/a/b/d/zj
dup
invokestatic com/a/b/d/jt/k()Lcom/a/b/d/jt;
iconst_0
invokespecial com/a/b/d/zj/<init>(Lcom/a/b/d/jt;I)V
putstatic com/a/b/d/ku/a Lcom/a/b/d/ku;
return
.limit locals 0
.limit stack 4
.end method

.method <init>()V
aload 0
invokespecial com/a/b/d/iz/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;
aload 0
invokeinterface com/a/b/d/xc/a()Ljava/util/Set; 0
invokestatic com/a/b/d/ku/a(Ljava/util/Collection;)Lcom/a/b/d/ku;
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
aload 0
instanceof com/a/b/d/ku
ifeq L0
aload 0
checkcast com/a/b/d/ku
astore 1
aload 1
invokevirtual com/a/b/d/ku/h_()Z
ifne L0
aload 1
areturn
L0:
aload 0
instanceof com/a/b/d/xc
ifeq L1
aload 0
invokestatic com/a/b/d/xe/b(Ljava/lang/Iterable;)Lcom/a/b/d/xc;
astore 0
L2:
aload 0
invokestatic com/a/b/d/ku/a(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;
areturn
L1:
aload 0
invokestatic com/a/b/d/xe/a(Ljava/lang/Iterable;)I
invokestatic com/a/b/d/oi/a(I)Lcom/a/b/d/oi;
astore 1
aload 1
aload 0
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 1
astore 0
goto L2
.limit locals 2
.limit stack 2
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
iconst_3
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
iconst_4
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 4
.limit stack 4
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ku;
iconst_5
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
dup
iconst_4
aload 4
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 5
.limit stack 4
.end method

.method private static transient a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/ku;
new com/a/b/d/kw
dup
invokespecial com/a/b/d/kw/<init>()V
aload 0
invokevirtual com/a/b/d/kw/a(Ljava/lang/Object;)Lcom/a/b/d/kw;
aload 1
invokevirtual com/a/b/d/kw/a(Ljava/lang/Object;)Lcom/a/b/d/kw;
aload 2
invokevirtual com/a/b/d/kw/a(Ljava/lang/Object;)Lcom/a/b/d/kw;
aload 3
invokevirtual com/a/b/d/kw/a(Ljava/lang/Object;)Lcom/a/b/d/kw;
aload 4
invokevirtual com/a/b/d/kw/a(Ljava/lang/Object;)Lcom/a/b/d/kw;
aload 5
invokevirtual com/a/b/d/kw/a(Ljava/lang/Object;)Lcom/a/b/d/kw;
aload 6
invokevirtual com/a/b/d/kw/b([Ljava/lang/Object;)Lcom/a/b/d/kw;
invokevirtual com/a/b/d/kw/b()Lcom/a/b/d/ku;
areturn
.limit locals 7
.limit stack 2
.end method

.method static a(Ljava/util/Collection;)Lcom/a/b/d/ku;
invokestatic com/a/b/d/jt/l()Lcom/a/b/d/ju;
astore 4
aload 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 0
lconst_0
lstore 2
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 5
aload 5
invokeinterface com/a/b/d/xd/b()I 0
istore 1
iload 1
ifle L2
aload 4
aload 5
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
iload 1
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
invokevirtual com/a/b/d/ju/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/ju;
pop
iload 1
i2l
lload 2
ladd
lstore 2
L3:
goto L0
L1:
lload 2
lconst_0
lcmp
ifne L4
getstatic com/a/b/d/ku/a Lcom/a/b/d/ku;
areturn
L4:
new com/a/b/d/zj
dup
aload 4
invokevirtual com/a/b/d/ju/a()Lcom/a/b/d/jt;
lload 2
invokestatic com/a/b/l/q/b(J)I
invokespecial com/a/b/d/zj/<init>(Lcom/a/b/d/jt;I)V
areturn
L2:
goto L3
.limit locals 6
.limit stack 5
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/ku;
new com/a/b/d/oi
dup
invokespecial com/a/b/d/oi/<init>()V
astore 1
aload 1
aload 0
invokestatic com/a/b/d/nj/a(Ljava/util/Collection;Ljava/util/Iterator;)Z
pop
aload 1
invokestatic com/a/b/d/ku/a(Lcom/a/b/d/xc;)Lcom/a/b/d/ku;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a([Ljava/lang/Object;)Lcom/a/b/d/ku;
aload 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static b()Lcom/a/b/d/ku;
getstatic com/a/b/d/ku/a Lcom/a/b/d/ku;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static b(Ljava/lang/Object;)Lcom/a/b/d/ku;
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 1
.limit stack 4
.end method

.method private static transient b([Ljava/lang/Object;)Lcom/a/b/d/ku;
aload 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ku/a(Ljava/lang/Iterable;)Lcom/a/b/d/ku;
areturn
.limit locals 1
.limit stack 1
.end method

.method private final e()Lcom/a/b/d/lo;
aload 0
invokevirtual com/a/b/d/ku/isEmpty()Z
ifeq L0
invokestatic com/a/b/d/lo/h()Lcom/a/b/d/lo;
areturn
L0:
new com/a/b/d/kx
dup
aload 0
iconst_0
invokespecial com/a/b/d/kx/<init>(Lcom/a/b/d/ku;B)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static p()Lcom/a/b/d/kw;
new com/a/b/d/kw
dup
invokespecial com/a/b/d/kw/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;I)I
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method a([Ljava/lang/Object;I)I
.annotation invisible Lcom/a/b/a/c;
a s = "not present in emulated superclass"
.end annotation
aload 0
invokevirtual com/a/b/d/ku/o()Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/iterator()Ljava/util/Iterator;
astore 3
L0:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 4
aload 1
iload 2
aload 4
invokeinterface com/a/b/d/xd/b()I 0
iload 2
iadd
aload 4
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
invokestatic java/util/Arrays/fill([Ljava/lang/Object;IILjava/lang/Object;)V
iload 2
aload 4
invokeinterface com/a/b/d/xd/b()I 0
iadd
istore 2
goto L0
L1:
iload 2
ireturn
.limit locals 5
.limit stack 4
.end method

.method abstract a(I)Lcom/a/b/d/xd;
.end method

.method public final synthetic a()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/ku/o()Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Ljava/lang/Object;II)Z
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 4
.limit stack 2
.end method

.method public final b(Ljava/lang/Object;I)I
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;I)I
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public c()Lcom/a/b/d/agi;
new com/a/b/d/kv
dup
aload 0
aload 0
invokevirtual com/a/b/d/ku/o()Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/c()Lcom/a/b/d/agi;
invokespecial com/a/b/d/kv/<init>(Lcom/a/b/d/ku;Ljava/util/Iterator;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/ku/a(Ljava/lang/Object;)I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public containsAll(Ljava/util/Collection;)Z
aload 0
invokevirtual com/a/b/d/ku/n_()Ljava/util/Set;
aload 1
invokeinterface java/util/Set/containsAll(Ljava/util/Collection;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method g()Ljava/lang/Object;
new com/a/b/d/la
dup
aload 0
invokespecial com/a/b/d/la/<init>(Lcom/a/b/d/xc;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/ku/o()Lcom/a/b/d/lo;
invokestatic com/a/b/d/aad/a(Ljava/util/Set;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic iterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/ku/c()Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final o()Lcom/a/b/d/lo;
aload 0
getfield com/a/b/d/ku/b Lcom/a/b/d/lo;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/ku/isEmpty()Z
ifeq L1
invokestatic com/a/b/d/lo/h()Lcom/a/b/d/lo;
astore 1
L2:
aload 0
aload 1
putfield com/a/b/d/ku/b Lcom/a/b/d/lo;
L0:
aload 1
areturn
L1:
new com/a/b/d/kx
dup
aload 0
iconst_0
invokespecial com/a/b/d/kx/<init>(Lcom/a/b/d/ku;B)V
astore 1
goto L2
.limit locals 3
.limit stack 4
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/ku/o()Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
