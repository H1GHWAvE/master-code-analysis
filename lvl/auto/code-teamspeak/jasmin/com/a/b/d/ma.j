.bytecode 50.0
.class public synchronized abstract com/a/b/d/ma
.super com/a/b/d/md
.implements com/a/b/d/abn
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "hasn't been tested yet"
.end annotation

.field private static final 'b' Ljava/util/Comparator;

.field private static final 'c' Lcom/a/b/d/ma;

.field transient 'a' Lcom/a/b/d/ma;

.method static <clinit>()V
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
putstatic com/a/b/d/ma/b Ljava/util/Comparator;
new com/a/b/d/fb
dup
getstatic com/a/b/d/ma/b Ljava/util/Comparator;
invokespecial com/a/b/d/fb/<init>(Ljava/util/Comparator;)V
putstatic com/a/b/d/ma/c Lcom/a/b/d/ma;
return
.limit locals 0
.limit stack 3
.end method

.method <init>()V
aload 0
invokespecial com/a/b/d/md/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public static a(Lcom/a/b/d/abn;)Lcom/a/b/d/ma;
aload 0
invokeinterface com/a/b/d/abn/comparator()Ljava/util/Comparator; 0
aload 0
invokeinterface com/a/b/d/abn/a()Ljava/util/Set; 0
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static a(Ljava/lang/Comparable;)Lcom/a/b/d/ma;
new com/a/b/d/zp
dup
aload 0
invokestatic com/a/b/d/me/a(Ljava/lang/Comparable;)Lcom/a/b/d/me;
checkcast com/a/b/d/zq
iconst_1
newarray int
dup
iconst_0
iconst_1
iastore
iconst_2
newarray long
dup
iconst_0
lconst_0
lastore
dup
iconst_1
lconst_1
lastore
iconst_0
iconst_1
invokespecial com/a/b/d/zp/<init>(Lcom/a/b/d/zq;[I[JII)V
areturn
.limit locals 1
.limit stack 9
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_2
anewarray java/lang/Comparable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 2
.limit stack 5
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_3
anewarray java/lang/Comparable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 3
.limit stack 5
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_4
anewarray java/lang/Comparable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 4
.limit stack 5
.end method

.method private static a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
iconst_5
anewarray java/lang/Comparable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
dup
iconst_4
aload 4
aastore
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 5
.limit stack 5
.end method

.method private static transient a(Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;Ljava/lang/Comparable;[Ljava/lang/Comparable;)Lcom/a/b/d/ma;
aload 6
arraylength
bipush 6
iadd
invokestatic com/a/b/d/ov/a(I)Ljava/util/ArrayList;
astore 7
aload 7
bipush 6
anewarray java/lang/Comparable
dup
iconst_0
aload 0
aastore
dup
iconst_1
aload 1
aastore
dup
iconst_2
aload 2
aastore
dup
iconst_3
aload 3
aastore
dup
iconst_4
aload 4
aastore
dup
iconst_5
aload 5
aastore
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
aload 7
aload 6
invokestatic java/util/Collections/addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z
pop
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
aload 7
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 8
.limit stack 5
.end method

.method static a(Ljava/util/Comparator;)Lcom/a/b/d/ma;
getstatic com/a/b/d/ma/b Ljava/util/Comparator;
aload 0
invokeinterface java/util/Comparator/equals(Ljava/lang/Object;)Z 1
ifeq L0
getstatic com/a/b/d/ma/c Lcom/a/b/d/ma;
areturn
L0:
new com/a/b/d/fb
dup
aload 0
invokespecial com/a/b/d/fb/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
aload 1
instanceof com/a/b/d/ma
ifeq L0
aload 1
checkcast com/a/b/d/ma
astore 2
aload 0
aload 2
invokevirtual com/a/b/d/ma/comparator()Ljava/util/Comparator;
invokeinterface java/util/Comparator/equals(Ljava/lang/Object;)Z 1
ifeq L0
aload 2
astore 1
aload 2
invokevirtual com/a/b/d/ma/h_()Z
ifeq L1
aload 0
aload 2
invokevirtual com/a/b/d/ma/o()Lcom/a/b/d/lo;
invokevirtual com/a/b/d/lo/f()Lcom/a/b/d/jl;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;
astore 1
L1:
aload 1
areturn
L0:
aload 1
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 1
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
invokestatic com/a/b/d/aer/a(Ljava/util/Comparator;)Lcom/a/b/d/aer;
astore 2
aload 2
aload 1
invokestatic com/a/b/d/mq/a(Ljava/util/Collection;Ljava/lang/Iterable;)Z
pop
aload 0
aload 2
invokevirtual com/a/b/d/aer/a()Ljava/util/Set;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;
areturn
.limit locals 3
.limit stack 2
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Collection;)Lcom/a/b/d/ma;
aload 1
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L0
aload 0
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;)Lcom/a/b/d/ma;
areturn
L0:
new com/a/b/d/jn
dup
aload 1
invokeinterface java/util/Collection/size()I 0
invokespecial com/a/b/d/jn/<init>(I)V
astore 3
aload 1
invokeinterface java/util/Collection/size()I 0
newarray int
astore 4
aload 1
invokeinterface java/util/Collection/size()I 0
iconst_1
iadd
newarray long
astore 5
aload 1
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 6
iconst_0
istore 2
L1:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 7
aload 3
aload 7
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
aload 4
iload 2
aload 7
invokeinterface com/a/b/d/xd/b()I 0
iastore
aload 5
iload 2
iconst_1
iadd
aload 5
iload 2
laload
aload 4
iload 2
iaload
i2l
ladd
lastore
iload 2
iconst_1
iadd
istore 2
goto L1
L2:
new com/a/b/d/zp
dup
new com/a/b/d/zq
dup
aload 3
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
aload 0
invokespecial com/a/b/d/zq/<init>(Lcom/a/b/d/jl;Ljava/util/Comparator;)V
aload 4
aload 5
iconst_0
aload 1
invokeinterface java/util/Collection/size()I 0
invokespecial com/a/b/d/zp/<init>(Lcom/a/b/d/zq;[I[JII)V
areturn
.limit locals 8
.limit stack 7
.end method

.method private static a(Ljava/util/Comparator;Ljava/util/Iterator;)Lcom/a/b/d/ma;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/mb
dup
aload 0
invokespecial com/a/b/d/mb/<init>(Ljava/util/Comparator;)V
aload 1
invokevirtual com/a/b/d/mb/c(Ljava/util/Iterator;)Lcom/a/b/d/mb;
invokevirtual com/a/b/d/mb/c()Lcom/a/b/d/ma;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/util/Iterator;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
astore 1
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/mb
dup
aload 1
invokespecial com/a/b/d/mb/<init>(Ljava/util/Comparator;)V
aload 0
invokevirtual com/a/b/d/mb/c(Ljava/util/Iterator;)Lcom/a/b/d/mb;
invokevirtual com/a/b/d/mb/c()Lcom/a/b/d/ma;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a([Ljava/lang/Comparable;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
aload 0
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 1
.limit stack 2
.end method

.method private static b(Ljava/lang/Iterable;)Lcom/a/b/d/ma;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
aload 0
invokestatic com/a/b/d/ma/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/ma;
areturn
.limit locals 1
.limit stack 2
.end method

.method private b(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
aload 0
invokevirtual com/a/b/d/ma/comparator()Ljava/util/Comparator;
aload 1
aload 3
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifgt L0
iconst_1
istore 5
L1:
iload 5
ldc "Expected lowerBound <= upperBound but %s > %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/ma/b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
aload 3
aload 4
invokevirtual com/a/b/d/ma/a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
areturn
L0:
iconst_0
istore 5
goto L1
.limit locals 6
.limit stack 6
.end method

.method private static b(Ljava/util/Comparator;)Lcom/a/b/d/mb;
new com/a/b/d/mb
dup
aload 0
invokespecial com/a/b/d/mb/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static p()Lcom/a/b/d/ma;
getstatic com/a/b/d/ma/c Lcom/a/b/d/ma;
areturn
.limit locals 0
.limit stack 1
.end method

.method private static q()Lcom/a/b/d/mb;
new com/a/b/d/mb
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokevirtual com/a/b/d/yd/a()Lcom/a/b/d/yd;
invokespecial com/a/b/d/mb/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 0
.limit stack 3
.end method

.method private static r()Lcom/a/b/d/mb;
new com/a/b/d/mb
dup
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
invokespecial com/a/b/d/mb/<init>(Ljava/util/Comparator;)V
areturn
.limit locals 0
.limit stack 3
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/a/b/d/ce;Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
aload 0
invokevirtual com/a/b/d/ma/comparator()Ljava/util/Comparator;
aload 1
aload 3
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
ifgt L0
iconst_1
istore 5
L1:
iload 5
ldc "Expected lowerBound <= upperBound but %s > %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/ma/b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
aload 3
aload 4
invokevirtual com/a/b/d/ma/a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
areturn
L0:
iconst_0
istore 5
goto L1
.limit locals 6
.limit stack 6
.end method

.method public abstract a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
.end method

.method public abstract b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
.end method

.method public abstract b()Lcom/a/b/d/me;
.end method

.method public synthetic c(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/ma/b(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final comparator()Ljava/util/Comparator;
aload 0
invokevirtual com/a/b/d/ma/b()Lcom/a/b/d/me;
invokevirtual com/a/b/d/me/comparator()Ljava/util/Comparator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic d(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/abn;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/ma/a(Ljava/lang/Object;Lcom/a/b/d/ce;)Lcom/a/b/d/ma;
areturn
.limit locals 3
.limit stack 3
.end method

.method public e()Lcom/a/b/d/ma;
aload 0
getfield com/a/b/d/ma/a Lcom/a/b/d/ma;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
new com/a/b/d/el
dup
aload 0
invokespecial com/a/b/d/el/<init>(Lcom/a/b/d/ma;)V
astore 1
aload 0
aload 1
putfield com/a/b/d/ma/a Lcom/a/b/d/ma;
L0:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method public synthetic e_()Ljava/util/NavigableSet;
aload 0
invokevirtual com/a/b/d/ma/b()Lcom/a/b/d/me;
areturn
.limit locals 1
.limit stack 1
.end method

.method final g()Ljava/lang/Object;
new com/a/b/d/mc
dup
aload 0
invokespecial com/a/b/d/mc/<init>(Lcom/a/b/d/abn;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final j()Lcom/a/b/d/xd;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public final k()Lcom/a/b/d/xd;
.annotation visible Ljava/lang/Deprecated;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 1
.limit stack 2
.end method

.method public synthetic m()Lcom/a/b/d/abn;
aload 0
invokevirtual com/a/b/d/ma/e()Lcom/a/b/d/ma;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic n()Ljava/util/SortedSet;
aload 0
invokevirtual com/a/b/d/ma/b()Lcom/a/b/d/me;
areturn
.limit locals 1
.limit stack 1
.end method

.method public synthetic n_()Ljava/util/Set;
aload 0
invokevirtual com/a/b/d/ma/b()Lcom/a/b/d/me;
areturn
.limit locals 1
.limit stack 1
.end method
