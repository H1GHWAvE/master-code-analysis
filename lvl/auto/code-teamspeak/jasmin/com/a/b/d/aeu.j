.bytecode 50.0
.class final synchronized com/a/b/d/aeu
.super java/lang/Object
.implements java/util/Iterator

.field 'a' Lcom/a/b/d/aez;

.field 'b' Lcom/a/b/d/xd;

.field final synthetic 'c' Lcom/a/b/d/aer;

.method <init>(Lcom/a/b/d/aer;)V
aload 0
aload 1
putfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
invokestatic com/a/b/d/aer/d(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
putfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
aload 0
aconst_null
putfield com/a/b/d/aeu/b Lcom/a/b/d/xd;
return
.limit locals 2
.limit stack 2
.end method

.method private a()Lcom/a/b/d/xd;
aload 0
invokevirtual com/a/b/d/aeu/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
invokestatic com/a/b/d/aer/a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;
astore 1
aload 0
aload 1
putfield com/a/b/d/aeu/b Lcom/a/b/d/xd;
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
invokestatic com/a/b/d/aer/c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
if_acmpne L1
aload 0
aconst_null
putfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
aload 1
areturn
L1:
aload 0
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
putfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
ifnonnull L0
iconst_0
ireturn
L0:
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
invokestatic com/a/b/d/aer/b(Lcom/a/b/d/aer;)Lcom/a/b/d/hs;
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
getfield com/a/b/d/aez/a Ljava/lang/Object;
invokevirtual com/a/b/d/hs/a(Ljava/lang/Object;)Z
ifeq L1
aload 0
aconst_null
putfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
iconst_0
ireturn
L1:
iconst_1
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final synthetic next()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/aeu/hasNext()Z
ifne L0
new java/util/NoSuchElementException
dup
invokespecial java/util/NoSuchElementException/<init>()V
athrow
L0:
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
invokestatic com/a/b/d/aer/a(Lcom/a/b/d/aer;Lcom/a/b/d/aez;)Lcom/a/b/d/xd;
astore 1
aload 0
aload 1
putfield com/a/b/d/aeu/b Lcom/a/b/d/xd;
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
invokestatic com/a/b/d/aer/c(Lcom/a/b/d/aer;)Lcom/a/b/d/aez;
if_acmpne L1
aload 0
aconst_null
putfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
aload 1
areturn
L1:
aload 0
aload 0
getfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
getfield com/a/b/d/aez/g Lcom/a/b/d/aez;
putfield com/a/b/d/aeu/a Lcom/a/b/d/aez;
aload 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final remove()V
aload 0
getfield com/a/b/d/aeu/b Lcom/a/b/d/xd;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/aeu/c Lcom/a/b/d/aer;
aload 0
getfield com/a/b/d/aeu/b Lcom/a/b/d/xd;
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
iconst_0
invokevirtual com/a/b/d/aer/c(Ljava/lang/Object;I)I
pop
aload 0
aconst_null
putfield com/a/b/d/aeu/b Lcom/a/b/d/xd;
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 3
.end method
