.bytecode 50.0
.class public synchronized abstract com/a/b/d/gd
.super java/lang/Object
.implements java/lang/Iterable
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field public final 'c' Ljava/lang/Iterable;

.method protected <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 0
putfield com/a/b/d/gd/c Ljava/lang/Iterable;
return
.limit locals 1
.limit stack 2
.end method

.method <init>(Ljava/lang/Iterable;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Iterable
putfield com/a/b/d/gd/c Ljava/lang/Iterable;
return
.limit locals 2
.limit stack 2
.end method

.method private a()I
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
astore 1
aload 1
instanceof java/util/Collection
ifeq L0
aload 1
checkcast java/util/Collection
invokeinterface java/util/Collection/size()I 0
ireturn
L0:
aload 1
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;)I
ireturn
.limit locals 2
.limit stack 1
.end method

.method private a(I)Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
astore 3
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "number to skip cannot be negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 3
instanceof java/util/List
ifeq L2
new com/a/b/d/ng
dup
aload 3
checkcast java/util/List
iload 1
invokespecial com/a/b/d/ng/<init>(Ljava/util/List;I)V
astore 3
L3:
aload 3
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
L0:
iconst_0
istore 2
goto L1
L2:
new com/a/b/d/ms
dup
aload 3
iload 1
invokespecial com/a/b/d/ms/<init>(Ljava/lang/Iterable;I)V
astore 3
goto L3
.limit locals 4
.limit stack 4
.end method

.method private a(Lcom/a/b/b/bj;)Lcom/a/b/d/gd;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 2
.limit stack 2
.end method

.method private static a(Lcom/a/b/d/gd;)Lcom/a/b/d/gd;
.annotation visible Ljava/lang/Deprecated;
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/gd
areturn
.limit locals 1
.limit stack 1
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
aload 0
instanceof com/a/b/d/gd
ifeq L0
aload 0
checkcast com/a/b/d/gd
areturn
L0:
new com/a/b/d/ge
dup
aload 0
aload 0
invokespecial com/a/b/d/ge/<init>(Ljava/lang/Iterable;Ljava/lang/Iterable;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method private static a([Ljava/lang/Object;)Lcom/a/b/d/gd;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
invokestatic com/a/b/d/ov/a([Ljava/lang/Object;)Ljava/util/ArrayList;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/util/Comparator;)Lcom/a/b/d/jl;
aload 1
invokestatic com/a/b/d/yd/a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokevirtual com/a/b/d/yd/b(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Lcom/a/b/b/bv;)Ljava/lang/String;
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 1
aload 0
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
areturn
.limit locals 2
.limit stack 2
.end method

.method private a(Ljava/util/Collection;)Ljava/util/Collection;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
instanceof java/util/Collection
ifeq L0
aload 1
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokeinterface java/util/Collection/addAll(Ljava/util/Collection;)Z 1
pop
L1:
aload 1
areturn
L0:
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L2:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
pop
goto L2
.limit locals 3
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
astore 2
aload 2
instanceof java/util/Collection
ifeq L0
aload 2
checkcast java/util/Collection
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Ljava/lang/Object;)Z
ireturn
L0:
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Ljava/lang/Object;)Z
ireturn
.limit locals 3
.limit stack 2
.end method

.method private b()Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokestatic com/a/b/d/mq/d(Ljava/lang/Iterable;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 1
.limit stack 1
.end method

.method private b(I)Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
astore 3
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
iload 1
iflt L0
iconst_1
istore 2
L1:
iload 2
ldc "limit is negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
new com/a/b/d/mu
dup
aload 3
iload 1
invokespecial com/a/b/d/mu/<init>(Ljava/lang/Iterable;I)V
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 4
.limit stack 4
.end method

.method private b(Lcom/a/b/b/bj;)Lcom/a/b/d/gd;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 2
.limit stack 2
.end method

.method private transient b([Ljava/lang/Object;)Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.annotation invisible Lcom/a/b/a/a;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Ljava/lang/Iterable;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/util/Comparator;)Lcom/a/b/d/me;
aload 1
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokestatic com/a/b/d/me/a(Ljava/util/Comparator;Ljava/lang/Iterable;)Lcom/a/b/d/me;
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(Lcom/a/b/b/co;)Z
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/d(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Class;)[Ljava/lang/Object;
.annotation invisible Lcom/a/b/a/c;
a s = "Array.newArray(Class, int)"
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method

.method private c()Lcom/a/b/b/ci;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
areturn
L0:
invokestatic com/a/b/b/ci/f()Lcom/a/b/b/ci;
areturn
.limit locals 2
.limit stack 1
.end method

.method private c(Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/sz/a(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(I)Ljava/lang/Object;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
astore 2
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
instanceof java/util/List
ifeq L0
aload 2
checkcast java/util/List
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
L0:
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
iload 1
invokestatic com/a/b/d/nj/c(Ljava/util/Iterator;I)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 2
.end method

.method private c(Lcom/a/b/b/co;)Z
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/e(Ljava/lang/Iterable;Lcom/a/b/b/co;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method private d()Lcom/a/b/b/ci;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
instanceof java/util/List
ifeq L0
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
checkcast java/util/List
astore 1
aload 1
invokeinterface java/util/List/isEmpty()Z 0
ifeq L1
invokestatic com/a/b/b/ci/f()Lcom/a/b/b/ci;
areturn
L1:
aload 1
aload 1
invokeinterface java/util/List/size()I 0
iconst_1
isub
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
areturn
L0:
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 1
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L2
invokestatic com/a/b/b/ci/f()Lcom/a/b/b/ci;
areturn
L2:
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
instanceof java/util/SortedSet
ifeq L3
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
checkcast java/util/SortedSet
invokeinterface java/util/SortedSet/last()Ljava/lang/Object; 0
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
areturn
L3:
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 2
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L3
aload 2
invokestatic com/a/b/b/ci/b(Ljava/lang/Object;)Lcom/a/b/b/ci;
areturn
.limit locals 3
.limit stack 3
.end method

.method private d(Lcom/a/b/b/co;)Lcom/a/b/b/ci;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
aload 1
invokestatic com/a/b/d/nj/f(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/b/ci;
areturn
.limit locals 2
.limit stack 2
.end method

.method private d(Lcom/a/b/b/bj;)Lcom/a/b/d/jr;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
invokestatic com/a/b/d/jr/c()Lcom/a/b/d/js;
astore 3
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 4
aload 4
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 3
aload 1
aload 4
invokeinterface com/a/b/b/bj/e(Ljava/lang/Object;)Ljava/lang/Object; 1
aload 4
invokevirtual com/a/b/d/js/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/js;
pop
goto L0
L1:
aload 3
invokevirtual com/a/b/d/js/a()Lcom/a/b/d/jr;
areturn
.limit locals 5
.limit stack 3
.end method

.method private e(Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/sz/b(Ljava/lang/Iterable;Lcom/a/b/b/bj;)Lcom/a/b/d/jt;
areturn
.limit locals 2
.limit stack 2
.end method

.method private e()Z
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private f()Lcom/a/b/d/jl;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private g()Lcom/a/b/d/lo;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokestatic com/a/b/d/lo/a(Ljava/lang/Iterable;)Lcom/a/b/d/lo;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/a/b/b/co;)Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
aload 1
invokestatic com/a/b/d/mq/c(Ljava/lang/Iterable;Lcom/a/b/b/co;)Ljava/lang/Iterable;
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Class;)Lcom/a/b/d/gd;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "Class.isInstance"
.end annotation
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
astore 2
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/d/ne
dup
aload 2
aload 1
invokespecial com/a/b/d/ne/<init>(Ljava/lang/Iterable;Ljava/lang/Class;)V
invokestatic com/a/b/d/gd/a(Ljava/lang/Iterable;)Lcom/a/b/d/gd;
areturn
.limit locals 3
.limit stack 4
.end method

.method public toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/gd/c Ljava/lang/Iterable;
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;)Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
