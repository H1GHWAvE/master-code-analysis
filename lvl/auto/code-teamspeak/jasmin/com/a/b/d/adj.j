.bytecode 50.0
.class synchronized com/a/b/d/adj
.super com/a/b/d/adn
.implements com/a/b/d/vi

.field private static final 'f' J = 0L


.field transient 'a' Ljava/util/Set;

.field transient 'b' Ljava/util/Collection;

.field transient 'c' Ljava/util/Collection;

.field transient 'd' Ljava/util/Map;

.field transient 'e' Lcom/a/b/d/xc;

.method <init>(Lcom/a/b/d/vi;)V
aload 0
aload 1
aconst_null
invokespecial com/a/b/d/adn/<init>(Ljava/lang/Object;Ljava/lang/Object;)V
return
.limit locals 2
.limit stack 3
.end method

.method a()Lcom/a/b/d/vi;
aload 0
invokespecial com/a/b/d/adn/d()Ljava/lang/Object;
checkcast com/a/b/d/vi
areturn
.limit locals 1
.limit stack 1
.end method

.method public final a(Lcom/a/b/d/vi;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/a(Lcom/a/b/d/vi;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/a(Ljava/lang/Object;Ljava/lang/Object;)Z 2
istore 3
aload 4
monitorexit
L1:
iload 3
ireturn
L2:
astore 1
L3:
aload 4
monitorexit
L4:
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection; 2
astore 1
aload 3
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method public final b()Ljava/util/Map;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adj/d Ljava/util/Map;
ifnonnull L1
aload 0
new com/a/b/d/acw
dup
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
invokespecial com/a/b/d/acw/<init>(Ljava/util/Map;Ljava/lang/Object;)V
putfield com/a/b/d/adj/d Ljava/util/Map;
L1:
aload 0
getfield com/a/b/d/adj/d Ljava/util/Map;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 5
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/b(Ljava/lang/Object;Ljava/lang/Object;)Z 2
istore 3
aload 4
monitorexit
L1:
iload 3
ireturn
L2:
astore 1
L3:
aload 4
monitorexit
L4:
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z 2
istore 3
aload 4
monitorexit
L1:
iload 3
ireturn
L2:
astore 1
L3:
aload 4
monitorexit
L4:
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 4
aload 4
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;Ljava/lang/Object;)Z 2
istore 3
aload 4
monitorexit
L1:
iload 3
ireturn
L2:
astore 1
L3:
aload 4
monitorexit
L4:
aload 1
athrow
.limit locals 5
.limit stack 3
.end method

.method synthetic d()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/d(Ljava/lang/Object;)Ljava/util/Collection; 1
astore 1
aload 2
monitorexit
L1:
aload 1
areturn
L2:
astore 1
L3:
aload 2
monitorexit
L4:
aload 1
athrow
.limit locals 3
.limit stack 2
.end method

.method public equals(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 1
aload 0
if_acmpne L5
iconst_1
ireturn
L5:
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/equals(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final f()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/f()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final f(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/f(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public final g()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/g()V 0
aload 1
monitorexit
L1:
return
L2:
astore 2
L3:
aload 1
monitorexit
L4:
aload 2
athrow
.limit locals 3
.limit stack 1
.end method

.method public final g(Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 3
aload 3
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/g(Ljava/lang/Object;)Z 1
istore 2
aload 3
monitorexit
L1:
iload 2
ireturn
L2:
astore 1
L3:
aload 3
monitorexit
L4:
aload 1
athrow
.limit locals 4
.limit stack 2
.end method

.method public hashCode()I
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/hashCode()I 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final i()Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adj/b Ljava/util/Collection;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/i()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/a(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
putfield com/a/b/d/adj/b Ljava/util/Collection;
L1:
aload 0
getfield com/a/b/d/adj/b Ljava/util/Collection;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public k()Ljava/util/Collection;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adj/c Ljava/util/Collection;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/b(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/Collection;
putfield com/a/b/d/adj/c Ljava/util/Collection;
L1:
aload 0
getfield com/a/b/d/adj/c Ljava/util/Collection;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final n()Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/n()Z 0
istore 1
aload 2
monitorexit
L1:
iload 1
ireturn
L2:
astore 3
L3:
aload 2
monitorexit
L4:
aload 3
athrow
.limit locals 4
.limit stack 1
.end method

.method public final p()Ljava/util/Set;
.catch all from L0 to L1 using L2
.catch all from L1 to L3 using L2
.catch all from L4 to L5 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 1
aload 1
monitorenter
L0:
aload 0
getfield com/a/b/d/adj/a Ljava/util/Set;
ifnonnull L1
aload 0
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
invokestatic com/a/b/d/acu/b(Ljava/util/Set;Ljava/lang/Object;)Ljava/util/Set;
putfield com/a/b/d/adj/a Ljava/util/Set;
L1:
aload 0
getfield com/a/b/d/adj/a Ljava/util/Set;
astore 2
aload 1
monitorexit
L3:
aload 2
areturn
L2:
astore 2
L4:
aload 1
monitorexit
L5:
aload 2
athrow
.limit locals 3
.limit stack 3
.end method

.method public final q()Lcom/a/b/d/xc;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L4 to L5 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 2
aload 2
monitorenter
L0:
aload 0
getfield com/a/b/d/adj/e Lcom/a/b/d/xc;
ifnonnull L4
aload 0
invokevirtual com/a/b/d/adj/a()Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/q()Lcom/a/b/d/xc; 0
astore 1
aload 0
getfield com/a/b/d/adj/h Ljava/lang/Object;
astore 3
aload 1
instanceof com/a/b/d/adk
ifne L10
aload 1
instanceof com/a/b/d/ku
ifeq L6
L1:
goto L10
L3:
aload 0
aload 1
putfield com/a/b/d/adj/e Lcom/a/b/d/xc;
L4:
aload 0
getfield com/a/b/d/adj/e Lcom/a/b/d/xc;
astore 1
aload 2
monitorexit
L5:
aload 1
areturn
L6:
new com/a/b/d/adk
dup
aload 1
aload 3
invokespecial com/a/b/d/adk/<init>(Lcom/a/b/d/xc;Ljava/lang/Object;)V
astore 1
L7:
goto L3
L2:
astore 1
L8:
aload 2
monitorexit
L9:
aload 1
athrow
L10:
goto L3
.limit locals 4
.limit stack 4
.end method
