.bytecode 50.0
.class public final synchronized com/a/b/d/fe
.super com/a/b/d/a
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field private static final 'c' J = 0L

.annotation invisible Lcom/a/b/a/c;
a s = "only needed in emulated source."
.end annotation
.end field

.field transient 'b' Ljava/lang/Class;

.method private <init>(Ljava/lang/Class;)V
aload 0
new java/util/EnumMap
dup
aload 1
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
aload 1
invokevirtual java/lang/Class/getEnumConstants()[Ljava/lang/Object;
checkcast [Ljava/lang/Enum;
arraylength
invokestatic com/a/b/d/sz/a(I)Ljava/util/HashMap;
invokespecial com/a/b/d/a/<init>(Ljava/util/Map;Ljava/util/Map;)V
aload 0
aload 1
putfield com/a/b/d/fe/b Ljava/lang/Class;
return
.limit locals 2
.limit stack 4
.end method

.method private static a(Ljava/lang/Class;)Lcom/a/b/d/fe;
new com/a/b/d/fe
dup
aload 0
invokespecial com/a/b/d/fe/<init>(Ljava/lang/Class;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/util/Map;)Lcom/a/b/d/fe;
new com/a/b/d/fe
dup
aload 0
invokestatic com/a/b/d/fd/a(Ljava/util/Map;)Ljava/lang/Class;
invokespecial com/a/b/d/fe/<init>(Ljava/lang/Class;)V
astore 1
aload 1
aload 0
invokevirtual com/a/b/d/fe/putAll(Ljava/util/Map;)V
aload 1
areturn
.limit locals 2
.limit stack 3
.end method

.method private static a(Ljava/lang/Enum;)Ljava/lang/Enum;
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Enum
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/io/ObjectInputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectInputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectInputStream/defaultReadObject()V
aload 0
aload 1
invokevirtual java/io/ObjectInputStream/readObject()Ljava/lang/Object;
checkcast java/lang/Class
putfield com/a/b/d/fe/b Ljava/lang/Class;
aload 0
new java/util/EnumMap
dup
aload 0
getfield com/a/b/d/fe/b Ljava/lang/Class;
invokespecial java/util/EnumMap/<init>(Ljava/lang/Class;)V
invokestatic com/a/b/d/agm/a(Ljava/util/Map;)Lcom/a/b/d/agm;
new java/util/HashMap
dup
aload 0
getfield com/a/b/d/fe/b Ljava/lang/Class;
invokevirtual java/lang/Class/getEnumConstants()[Ljava/lang/Object;
checkcast [Ljava/lang/Enum;
arraylength
iconst_3
imul
iconst_2
idiv
invokespecial java/util/HashMap/<init>(I)V
invokevirtual com/a/b/d/fe/a(Ljava/util/Map;Ljava/util/Map;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Ljava/util/Map;Ljava/io/ObjectInputStream;)V
return
.limit locals 2
.limit stack 6
.end method

.method private a(Ljava/io/ObjectOutputStream;)V
.annotation invisible Lcom/a/b/a/c;
a s = "java.io.ObjectOutputStream"
.end annotation
aload 1
invokevirtual java/io/ObjectOutputStream/defaultWriteObject()V
aload 1
aload 0
getfield com/a/b/d/fe/b Ljava/lang/Class;
invokevirtual java/io/ObjectOutputStream/writeObject(Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/d/zz/a(Ljava/util/Map;Ljava/io/ObjectOutputStream;)V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
aload 2
invokespecial com/a/b/d/a/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method private d()Ljava/lang/Class;
aload 0
getfield com/a/b/d/fe/b Ljava/lang/Class;
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
aload 1
checkcast java/lang/Enum
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/Enum
areturn
.limit locals 2
.limit stack 1
.end method

.method public final volatile synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
checkcast java/lang/Enum
aload 2
invokespecial com/a/b/d/a/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic b()Lcom/a/b/d/bw;
aload 0
invokespecial com/a/b/d/a/b()Lcom/a/b/d/bw;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic clear()V
aload 0
invokespecial com/a/b/d/a/clear()V
return
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic containsValue(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/a/containsValue(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic entrySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/a/entrySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic j_()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/a/j_()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic keySet()Ljava/util/Set;
aload 0
invokespecial com/a/b/d/a/keySet()Ljava/util/Set;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final volatile synthetic put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
checkcast java/lang/Enum
aload 2
invokespecial com/a/b/d/a/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final volatile synthetic putAll(Ljava/util/Map;)V
aload 0
aload 1
invokespecial com/a/b/d/a/putAll(Ljava/util/Map;)V
return
.limit locals 2
.limit stack 2
.end method

.method public final volatile synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/b/d/a/remove(Ljava/lang/Object;)Ljava/lang/Object;
areturn
.limit locals 2
.limit stack 2
.end method
