.bytecode 50.0
.class final synchronized com/a/b/d/os
.super java/lang/Object
.implements java/util/ListIterator

.field 'a' I

.field 'b' Lcom/a/b/d/or;

.field 'c' Lcom/a/b/d/or;

.field 'd' Lcom/a/b/d/or;

.field 'e' I

.field final synthetic 'f' Lcom/a/b/d/oj;

.method <init>(Lcom/a/b/d/oj;I)V
aload 0
aload 1
putfield com/a/b/d/os/f Lcom/a/b/d/oj;
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
aload 0
getfield com/a/b/d/os/f Lcom/a/b/d/oj;
invokestatic com/a/b/d/oj/a(Lcom/a/b/d/oj;)I
putfield com/a/b/d/os/e I
aload 1
invokevirtual com/a/b/d/oj/f()I
istore 3
iload 2
iload 3
invokestatic com/a/b/b/cn/b(II)I
pop
iload 2
iload 3
iconst_2
idiv
if_icmplt L0
aload 0
aload 1
invokestatic com/a/b/d/oj/b(Lcom/a/b/d/oj;)Lcom/a/b/d/or;
putfield com/a/b/d/os/d Lcom/a/b/d/or;
aload 0
iload 3
putfield com/a/b/d/os/a I
L1:
iload 2
iload 3
if_icmpge L2
aload 0
invokespecial com/a/b/d/os/c()Lcom/a/b/d/or;
pop
iload 2
iconst_1
iadd
istore 2
goto L1
L0:
aload 0
aload 1
invokestatic com/a/b/d/oj/c(Lcom/a/b/d/oj;)Lcom/a/b/d/or;
putfield com/a/b/d/os/b Lcom/a/b/d/or;
L3:
iload 2
ifle L2
aload 0
invokespecial com/a/b/d/os/b()Lcom/a/b/d/or;
pop
iload 2
iconst_1
isub
istore 2
goto L3
L2:
aload 0
aconst_null
putfield com/a/b/d/os/c Lcom/a/b/d/or;
return
.limit locals 4
.limit stack 3
.end method

.method private a()V
aload 0
getfield com/a/b/d/os/f Lcom/a/b/d/oj;
invokestatic com/a/b/d/oj/a(Lcom/a/b/d/oj;)I
aload 0
getfield com/a/b/d/os/e I
if_icmpeq L0
new java/util/ConcurrentModificationException
dup
invokespecial java/util/ConcurrentModificationException/<init>()V
athrow
L0:
return
.limit locals 1
.limit stack 2
.end method

.method private a(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
ifnull L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
aload 1
putfield com/a/b/d/or/b Ljava/lang/Object;
return
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private b()Lcom/a/b/d/or;
aload 0
invokespecial com/a/b/d/os/a()V
aload 0
getfield com/a/b/d/os/b Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/e(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/os/b Lcom/a/b/d/or;
astore 1
aload 0
aload 1
putfield com/a/b/d/os/c Lcom/a/b/d/or;
aload 0
aload 1
putfield com/a/b/d/os/d Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/os/b Lcom/a/b/d/or;
getfield com/a/b/d/or/c Lcom/a/b/d/or;
putfield com/a/b/d/os/b Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/os/a I
iconst_1
iadd
putfield com/a/b/d/os/a I
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
areturn
.limit locals 2
.limit stack 3
.end method

.method private c()Lcom/a/b/d/or;
aload 0
invokespecial com/a/b/d/os/a()V
aload 0
getfield com/a/b/d/os/d Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/e(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/os/d Lcom/a/b/d/or;
astore 1
aload 0
aload 1
putfield com/a/b/d/os/c Lcom/a/b/d/or;
aload 0
aload 1
putfield com/a/b/d/os/b Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/os/d Lcom/a/b/d/or;
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/os/d Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/os/a I
iconst_1
isub
putfield com/a/b/d/os/a I
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
areturn
.limit locals 2
.limit stack 3
.end method

.method private static d()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 0
.limit stack 2
.end method

.method private static e()V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 0
.limit stack 2
.end method

.method public final synthetic add(Ljava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final hasNext()Z
aload 0
invokespecial com/a/b/d/os/a()V
aload 0
getfield com/a/b/d/os/b Lcom/a/b/d/or;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final hasPrevious()Z
aload 0
invokespecial com/a/b/d/os/a()V
aload 0
getfield com/a/b/d/os/d Lcom/a/b/d/or;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic next()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/os/b()Lcom/a/b/d/or;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final nextIndex()I
aload 0
getfield com/a/b/d/os/a I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic previous()Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/os/c()Lcom/a/b/d/or;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final previousIndex()I
aload 0
getfield com/a/b/d/os/a I
iconst_1
isub
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final remove()V
aload 0
invokespecial com/a/b/d/os/a()V
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
ifnull L0
iconst_1
istore 1
L1:
iload 1
ldc "no calls to next() since the last call to remove()"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
aload 0
getfield com/a/b/d/os/b Lcom/a/b/d/or;
if_acmpeq L2
aload 0
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
getfield com/a/b/d/or/d Lcom/a/b/d/or;
putfield com/a/b/d/os/d Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/os/a I
iconst_1
isub
putfield com/a/b/d/os/a I
L3:
aload 0
getfield com/a/b/d/os/f Lcom/a/b/d/oj;
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
invokestatic com/a/b/d/oj/a(Lcom/a/b/d/oj;Lcom/a/b/d/or;)V
aload 0
aconst_null
putfield com/a/b/d/os/c Lcom/a/b/d/or;
aload 0
aload 0
getfield com/a/b/d/os/f Lcom/a/b/d/oj;
invokestatic com/a/b/d/oj/a(Lcom/a/b/d/oj;)I
putfield com/a/b/d/os/e I
return
L0:
iconst_0
istore 1
goto L1
L2:
aload 0
aload 0
getfield com/a/b/d/os/c Lcom/a/b/d/or;
getfield com/a/b/d/or/c Lcom/a/b/d/or;
putfield com/a/b/d/os/b Lcom/a/b/d/or;
goto L3
.limit locals 2
.limit stack 3
.end method

.method public final synthetic set(Ljava/lang/Object;)V
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method
