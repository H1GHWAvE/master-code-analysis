.bytecode 50.0
.class final synchronized com/a/b/d/ci
.super java/util/AbstractList
.implements java/util/RandomAccess
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private final transient 'a' Lcom/a/b/d/jl;

.field private final transient 'b' [I

.method <init>(Lcom/a/b/d/jl;)V
.catch java/lang/ArithmeticException from L0 to L1 using L2
.catch java/lang/ArithmeticException from L3 to L4 using L2
aload 0
invokespecial java/util/AbstractList/<init>()V
aload 0
aload 1
putfield com/a/b/d/ci/a Lcom/a/b/d/jl;
aload 1
invokevirtual com/a/b/d/jl/size()I
iconst_1
iadd
newarray int
astore 3
aload 3
aload 1
invokevirtual com/a/b/d/jl/size()I
iconst_1
iastore
L0:
aload 1
invokevirtual com/a/b/d/jl/size()I
iconst_1
isub
istore 2
L1:
iload 2
iflt L5
L3:
aload 3
iload 2
aload 3
iload 2
iconst_1
iadd
iaload
aload 1
iload 2
invokevirtual com/a/b/d/jl/get(I)Ljava/lang/Object;
checkcast java/util/List
invokeinterface java/util/List/size()I 0
invokestatic com/a/b/j/g/b(II)I
iastore
L4:
iload 2
iconst_1
isub
istore 2
goto L1
L2:
astore 1
new java/lang/IllegalArgumentException
dup
ldc "Cartesian product too large; must have size at most Integer.MAX_VALUE"
invokespecial java/lang/IllegalArgumentException/<init>(Ljava/lang/String;)V
athrow
L5:
aload 0
aload 3
putfield com/a/b/d/ci/b [I
return
.limit locals 4
.limit stack 5
.end method

.method private a(II)I
iload 1
aload 0
getfield com/a/b/d/ci/b [I
iload 2
iconst_1
iadd
iaload
idiv
aload 0
getfield com/a/b/d/ci/a Lcom/a/b/d/jl;
iload 2
invokevirtual com/a/b/d/jl/get(I)Ljava/lang/Object;
checkcast java/util/List
invokeinterface java/util/List/size()I 0
irem
ireturn
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/ci;II)I
iload 1
aload 0
getfield com/a/b/d/ci/b [I
iload 2
iconst_1
iadd
iaload
idiv
aload 0
getfield com/a/b/d/ci/a Lcom/a/b/d/jl;
iload 2
invokevirtual com/a/b/d/jl/get(I)Ljava/lang/Object;
checkcast java/util/List
invokeinterface java/util/List/size()I 0
irem
ireturn
.limit locals 3
.limit stack 4
.end method

.method private a(I)Lcom/a/b/d/jl;
iload 1
aload 0
invokevirtual com/a/b/d/ci/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
new com/a/b/d/cj
dup
aload 0
iload 1
invokespecial com/a/b/d/cj/<init>(Lcom/a/b/d/ci;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/ci;)Lcom/a/b/d/jl;
aload 0
getfield com/a/b/d/ci/a Lcom/a/b/d/jl;
areturn
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/util/List;)Ljava/util/List;
new com/a/b/d/jn
dup
aload 0
invokeinterface java/util/List/size()I 0
invokespecial com/a/b/d/jn/<init>(I)V
astore 1
aload 0
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/List
invokestatic com/a/b/d/jl/a(Ljava/util/Collection;)Lcom/a/b/d/jl;
astore 2
aload 2
invokeinterface java/util/List/isEmpty()Z 0
ifeq L2
invokestatic com/a/b/d/jl/d()Lcom/a/b/d/jl;
areturn
L2:
aload 1
aload 2
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
goto L0
L1:
new com/a/b/d/ci
dup
aload 1
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
invokespecial com/a/b/d/ci/<init>(Lcom/a/b/d/jl;)V
areturn
.limit locals 3
.limit stack 3
.end method

.method public final contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof java/util/List
ifne L0
iconst_0
ireturn
L0:
aload 1
checkcast java/util/List
astore 1
aload 1
invokeinterface java/util/List/size()I 0
aload 0
getfield com/a/b/d/ci/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
if_icmpeq L1
iconst_0
ireturn
L1:
aload 1
invokeinterface java/util/List/listIterator()Ljava/util/ListIterator; 0
astore 1
L2:
aload 1
invokeinterface java/util/ListIterator/hasNext()Z 0
ifeq L3
aload 1
invokeinterface java/util/ListIterator/nextIndex()I 0
istore 2
aload 0
getfield com/a/b/d/ci/a Lcom/a/b/d/jl;
iload 2
invokevirtual com/a/b/d/jl/get(I)Ljava/lang/Object;
checkcast java/util/List
aload 1
invokeinterface java/util/ListIterator/next()Ljava/lang/Object; 0
invokeinterface java/util/List/contains(Ljava/lang/Object;)Z 1
ifne L2
iconst_0
ireturn
L3:
iconst_1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final synthetic get(I)Ljava/lang/Object;
iload 1
aload 0
invokevirtual com/a/b/d/ci/size()I
invokestatic com/a/b/b/cn/a(II)I
pop
new com/a/b/d/cj
dup
aload 0
iload 1
invokespecial com/a/b/d/cj/<init>(Lcom/a/b/d/ci;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final size()I
aload 0
getfield com/a/b/d/ci/b [I
iconst_0
iaload
ireturn
.limit locals 1
.limit stack 2
.end method
