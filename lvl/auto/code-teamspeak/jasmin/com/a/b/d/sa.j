.bytecode 50.0
.class synchronized com/a/b/d/sa
.super java/util/concurrent/locks/ReentrantLock

.field final 'a' Lcom/a/b/d/qy;

.field volatile 'b' I

.field 'c' I

.field 'd' I

.field volatile 'e' Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final 'f' I

.field final 'g' Ljava/lang/ref/ReferenceQueue;

.field final 'h' Ljava/lang/ref/ReferenceQueue;

.field final 'i' Ljava/util/Queue;

.field final 'j' Ljava/util/concurrent/atomic/AtomicInteger;

.field final 'k' Ljava/util/Queue;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
.end field

.field final 'l' Ljava/util/Queue;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
.end field

.method <init>(Lcom/a/b/d/qy;II)V
aconst_null
astore 5
aload 0
invokespecial java/util/concurrent/locks/ReentrantLock/<init>()V
aload 0
new java/util/concurrent/atomic/AtomicInteger
dup
invokespecial java/util/concurrent/atomic/AtomicInteger/<init>()V
putfield com/a/b/d/sa/j Ljava/util/concurrent/atomic/AtomicInteger;
aload 0
aload 1
putfield com/a/b/d/sa/a Lcom/a/b/d/qy;
aload 0
iload 3
putfield com/a/b/d/sa/f I
iload 2
invokestatic com/a/b/d/sa/a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 4
aload 0
aload 4
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_3
imul
iconst_4
idiv
putfield com/a/b/d/sa/d I
aload 0
getfield com/a/b/d/sa/d I
aload 0
getfield com/a/b/d/sa/f I
if_icmpne L0
aload 0
aload 0
getfield com/a/b/d/sa/d I
iconst_1
iadd
putfield com/a/b/d/sa/d I
L0:
aload 0
aload 4
putfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 1
invokevirtual com/a/b/d/qy/e()Z
ifeq L1
new java/lang/ref/ReferenceQueue
dup
invokespecial java/lang/ref/ReferenceQueue/<init>()V
astore 4
L2:
aload 0
aload 4
putfield com/a/b/d/sa/g Ljava/lang/ref/ReferenceQueue;
aload 5
astore 4
aload 1
invokevirtual com/a/b/d/qy/f()Z
ifeq L3
new java/lang/ref/ReferenceQueue
dup
invokespecial java/lang/ref/ReferenceQueue/<init>()V
astore 4
L3:
aload 0
aload 4
putfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
aload 1
invokevirtual com/a/b/d/qy/b()Z
ifne L4
aload 1
invokevirtual com/a/b/d/qy/d()Z
ifeq L5
L4:
new java/util/concurrent/ConcurrentLinkedQueue
dup
invokespecial java/util/concurrent/ConcurrentLinkedQueue/<init>()V
astore 4
L6:
aload 0
aload 4
putfield com/a/b/d/sa/i Ljava/util/Queue;
aload 1
invokevirtual com/a/b/d/qy/b()Z
ifeq L7
new com/a/b/d/rp
dup
invokespecial com/a/b/d/rp/<init>()V
astore 4
L8:
aload 0
aload 4
putfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokevirtual com/a/b/d/qy/c()Z
ifeq L9
new com/a/b/d/rs
dup
invokespecial com/a/b/d/rs/<init>()V
astore 1
L10:
aload 0
aload 1
putfield com/a/b/d/sa/l Ljava/util/Queue;
return
L1:
aconst_null
astore 4
goto L2
L5:
invokestatic com/a/b/d/qy/i()Ljava/util/Queue;
astore 4
goto L6
L7:
invokestatic com/a/b/d/qy/i()Ljava/util/Queue;
astore 4
goto L8
L9:
invokestatic com/a/b/d/qy/i()Ljava/util/Queue;
astore 1
goto L10
.limit locals 6
.limit stack 3
.end method

.method private static a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
new java/util/concurrent/atomic/AtomicReferenceArray
dup
iload 0
invokespecial java/util/concurrent/atomic/AtomicReferenceArray/<init>(I)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private a(Lcom/a/b/d/rz;J)V
aload 1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/w Lcom/a/b/b/ej;
invokevirtual com/a/b/b/ej/a()J
lload 2
ladd
invokeinterface com/a/b/d/rz/a(J)V 2
return
.limit locals 4
.limit stack 5
.end method

.method private a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/p Lcom/a/b/d/sh;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/sh/a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Ljava/lang/Object;)Lcom/a/b/d/sr;
invokeinterface com/a/b/d/rz/a(Lcom/a/b/d/sr;)V 1
aload 0
invokespecial com/a/b/d/sa/l()V
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/c()Z
ifeq L0
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/d()Z
ifeq L1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/r J
lstore 3
L2:
aload 0
aload 1
lload 3
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;J)V
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
L0:
return
L1:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/s J
lstore 3
goto L2
.limit locals 5
.limit stack 5
.end method

.method private a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V
aload 0
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_3
imul
iconst_4
idiv
putfield com/a/b/d/sa/d I
aload 0
getfield com/a/b/d/sa/d I
aload 0
getfield com/a/b/d/sa/f I
if_icmpne L0
aload 0
aload 0
getfield com/a/b/d/sa/d I
iconst_1
iadd
putfield com/a/b/d/sa/d I
L0:
aload 0
aload 1
putfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
return
.limit locals 2
.limit stack 3
.end method

.method private a(Lcom/a/b/d/rz;ILcom/a/b/d/qq;)Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
getfield com/a/b/d/sa/b I
istore 4
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 7
iload 2
aload 7
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 2
aload 7
iload 2
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 6
aload 6
astore 5
L0:
aload 5
ifnull L1
aload 5
aload 1
if_acmpne L2
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 5
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
aload 5
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
aload 3
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 6
aload 5
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
getfield com/a/b/d/sa/b I
istore 4
aload 7
iload 2
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 4
iconst_1
isub
putfield com/a/b/d/sa/b I
iconst_1
ireturn
L2:
aload 5
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 5
goto L0
L1:
iconst_0
ireturn
.limit locals 8
.limit stack 4
.end method

.method private static a(Lcom/a/b/d/sr;)Z
aload 0
invokeinterface com/a/b/d/sr/b()Z 0
ifeq L0
L1:
iconst_0
ireturn
L0:
aload 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
ifnonnull L1
iconst_1
ireturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)Z
.annotation invisible Lcom/a/b/a/d;
.end annotation
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
L0:
aload 0
getfield com/a/b/d/sa/b I
ifeq L11
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 6
aload 6
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
istore 3
L1:
iconst_0
istore 2
L12:
iload 2
iload 3
if_icmpge L11
L3:
aload 6
iload 2
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 5
L4:
aload 5
ifnull L13
L5:
aload 0
aload 5
invokevirtual com/a/b/d/sa/c(Lcom/a/b/d/rz;)Ljava/lang/Object;
astore 7
L6:
aload 7
ifnull L9
L7:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/n Lcom/a/b/b/au;
aload 1
aload 7
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
istore 4
L8:
iload 4
ifeq L9
aload 0
invokevirtual com/a/b/d/sa/a()V
iconst_1
ireturn
L9:
aload 5
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 5
L10:
goto L4
L13:
iload 2
iconst_1
iadd
istore 2
goto L12
L11:
aload 0
invokevirtual com/a/b/d/sa/a()V
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/a()V
aload 1
athrow
.limit locals 8
.limit stack 3
.end method

.method private b(I)Lcom/a/b/d/rz;
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 2
aload 2
aload 2
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iload 1
iand
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
areturn
.limit locals 3
.limit stack 3
.end method

.method private b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 2
invokeinterface java/util/Queue/remove(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 2
invokeinterface java/util/Queue/remove(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/sa/b I
istore 3
aload 2
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 5
aload 1
astore 4
aload 5
astore 1
L0:
aload 4
aload 2
if_acmpeq L1
aload 0
aload 4
aload 1
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 5
aload 5
ifnull L2
aload 5
astore 1
L3:
aload 4
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 4
goto L0
L2:
aload 0
aload 4
invokespecial com/a/b/d/sa/e(Lcom/a/b/d/rz;)V
iload 3
iconst_1
isub
istore 3
goto L3
L1:
aload 0
iload 3
putfield com/a/b/d/sa/b I
aload 1
areturn
.limit locals 6
.limit stack 3
.end method

.method private d(Lcom/a/b/d/rz;)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
invokespecial com/a/b/d/sa/l()V
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/c()Z
ifeq L0
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/d()Z
ifeq L1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/r J
lstore 2
L2:
aload 0
aload 1
lload 2
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;J)V
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
L0:
return
L1:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/s J
lstore 2
goto L2
.limit locals 4
.limit stack 4
.end method

.method private e(Ljava/lang/Object;I)Lcom/a/b/d/rz;
aload 0
aload 1
iload 2
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;I)Lcom/a/b/d/rz;
astore 1
aload 1
ifnonnull L0
aconst_null
areturn
L0:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/c()Z
ifeq L1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
aload 1
invokevirtual com/a/b/d/qy/a(Lcom/a/b/d/rz;)Z
ifeq L1
aload 0
invokespecial com/a/b/d/sa/m()V
aconst_null
areturn
L1:
aload 1
areturn
.limit locals 3
.limit stack 3
.end method

.method private e()V
.catch all from L0 to L1 using L2
aload 0
invokevirtual com/a/b/d/sa/tryLock()Z
ifeq L3
L0:
aload 0
invokespecial com/a/b/d/sa/f()V
L1:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
L3:
return
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method private e(Lcom/a/b/d/rz;)V
aload 0
aload 1
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/remove(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/remove(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 3
.end method

.method private f()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
iconst_0
istore 2
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/e()Z
ifeq L0
iconst_0
istore 1
L1:
aload 0
getfield com/a/b/d/sa/g Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 4
aload 4
ifnull L0
aload 4
checkcast com/a/b/d/rz
astore 4
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
astore 5
aload 4
invokeinterface com/a/b/d/rz/c()I 0
istore 3
aload 5
iload 3
invokevirtual com/a/b/d/qy/a(I)Lcom/a/b/d/sa;
aload 4
iload 3
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;I)Z
pop
iload 1
iconst_1
iadd
istore 1
iload 1
bipush 16
if_icmpne L2
L0:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/f()Z
ifeq L3
iload 2
istore 1
L4:
aload 0
getfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 4
aload 4
ifnull L3
aload 4
checkcast com/a/b/d/sr
astore 4
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
astore 5
aload 4
invokeinterface com/a/b/d/sr/a()Lcom/a/b/d/rz; 0
astore 6
aload 6
invokeinterface com/a/b/d/rz/c()I 0
istore 2
aload 5
iload 2
invokevirtual com/a/b/d/qy/a(I)Lcom/a/b/d/sa;
aload 6
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
iload 2
aload 4
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z
pop
iload 1
iconst_1
iadd
istore 2
iload 2
istore 1
iload 2
bipush 16
if_icmpne L4
L3:
return
L2:
goto L1
.limit locals 7
.limit stack 4
.end method

.method private g()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
iconst_0
istore 1
L0:
aload 0
getfield com/a/b/d/sa/g Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 3
aload 3
ifnull L1
aload 3
checkcast com/a/b/d/rz
astore 3
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
astore 4
aload 3
invokeinterface com/a/b/d/rz/c()I 0
istore 2
aload 4
iload 2
invokevirtual com/a/b/d/qy/a(I)Lcom/a/b/d/sa;
aload 3
iload 2
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;I)Z
pop
iload 1
iconst_1
iadd
istore 1
iload 1
bipush 16
if_icmpne L2
L1:
return
L2:
goto L0
.limit locals 5
.limit stack 3
.end method

.method private h()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
iconst_0
istore 1
L0:
aload 0
getfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
astore 3
aload 3
ifnull L1
aload 3
checkcast com/a/b/d/sr
astore 3
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
astore 4
aload 3
invokeinterface com/a/b/d/sr/a()Lcom/a/b/d/rz; 0
astore 5
aload 5
invokeinterface com/a/b/d/rz/c()I 0
istore 2
aload 4
iload 2
invokevirtual com/a/b/d/qy/a(I)Lcom/a/b/d/sa;
aload 5
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
iload 2
aload 3
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z
pop
iload 1
iconst_1
iadd
istore 1
iload 1
bipush 16
if_icmpne L2
L1:
return
L2:
goto L0
.limit locals 6
.limit stack 4
.end method

.method private i()V
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/e()Z
ifeq L0
L1:
aload 0
getfield com/a/b/d/sa/g Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
ifnonnull L1
L0:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/f()Z
ifeq L2
L3:
aload 0
getfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
ifnonnull L3
L2:
return
.limit locals 1
.limit stack 1
.end method

.method private j()V
L0:
aload 0
getfield com/a/b/d/sa/g Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
ifnonnull L0
return
.limit locals 1
.limit stack 1
.end method

.method private k()V
L0:
aload 0
getfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
ifnonnull L0
return
.limit locals 1
.limit stack 1
.end method

.method private l()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
L0:
aload 0
getfield com/a/b/d/sa/i Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast com/a/b/d/rz
astore 1
aload 1
ifnull L1
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/contains(Ljava/lang/Object;)Z 1
ifeq L2
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
L2:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/d()Z
ifeq L0
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/contains(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
goto L0
L1:
return
.limit locals 2
.limit stack 2
.end method

.method private m()V
.catch all from L0 to L1 using L2
aload 0
invokevirtual com/a/b/d/sa/tryLock()Z
ifeq L3
L0:
aload 0
invokespecial com/a/b/d/sa/n()V
L1:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
L3:
return
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 1
athrow
.limit locals 2
.limit stack 1
.end method

.method private n()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
invokespecial com/a/b/d/sa/l()V
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
invokeinterface java/util/Queue/isEmpty()Z 0
ifeq L0
L1:
return
L0:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/w Lcom/a/b/b/ej;
invokevirtual com/a/b/b/ej/a()J
lstore 1
L2:
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
invokeinterface java/util/Queue/peek()Ljava/lang/Object; 0
checkcast com/a/b/d/rz
astore 3
aload 3
ifnull L1
aload 3
lload 1
invokestatic com/a/b/d/qy/a(Lcom/a/b/d/rz;J)Z
ifeq L1
aload 0
aload 3
aload 3
invokeinterface com/a/b/d/rz/c()I 0
getstatic com/a/b/d/qq/d Lcom/a/b/d/qq;
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;ILcom/a/b/d/qq;)Z
ifne L2
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
.limit locals 4
.limit stack 4
.end method

.method private o()Z
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/b()Z
ifeq L0
aload 0
getfield com/a/b/d/sa/b I
aload 0
getfield com/a/b/d/sa/f I
if_icmplt L0
aload 0
invokespecial com/a/b/d/sa/l()V
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
invokeinterface java/util/Queue/remove()Ljava/lang/Object; 0
checkcast com/a/b/d/rz
astore 1
aload 0
aload 1
aload 1
invokeinterface com/a/b/d/rz/c()I 0
getstatic com/a/b/d/qq/e Lcom/a/b/d/qq;
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;ILcom/a/b/d/qq;)Z
ifne L1
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 4
.end method

.method private p()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 10
aload 10
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
istore 5
iload 5
ldc_w 1073741824
if_icmplt L0
return
L0:
aload 0
getfield com/a/b/d/sa/b I
istore 1
iload 5
iconst_1
ishl
invokestatic com/a/b/d/sa/a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 11
aload 0
aload 11
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_3
imul
iconst_4
idiv
putfield com/a/b/d/sa/d I
aload 11
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
istore 6
iconst_0
istore 3
L1:
iload 3
iload 5
if_icmpge L2
aload 10
iload 3
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 8
aload 8
ifnull L3
aload 8
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 7
aload 8
invokeinterface com/a/b/d/rz/c()I 0
iload 6
iand
istore 2
aload 7
ifnonnull L4
aload 11
iload 2
aload 8
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
L5:
iload 3
iconst_1
iadd
istore 3
goto L1
L4:
aload 8
astore 9
L6:
aload 7
ifnull L7
aload 7
invokeinterface com/a/b/d/rz/c()I 0
iload 6
iand
istore 4
iload 4
iload 2
if_icmpeq L8
aload 7
astore 9
iload 4
istore 2
L9:
aload 7
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 7
goto L6
L7:
aload 11
iload 2
aload 9
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 8
astore 7
iload 1
istore 2
L10:
iload 2
istore 1
aload 7
aload 9
if_acmpeq L5
aload 7
invokeinterface com/a/b/d/rz/c()I 0
iload 6
iand
istore 1
aload 0
aload 7
aload 11
iload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 8
aload 8
ifnull L11
aload 11
iload 1
aload 8
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
iload 2
istore 1
L12:
aload 7
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 7
iload 1
istore 2
goto L10
L11:
aload 0
aload 7
invokespecial com/a/b/d/sa/e(Lcom/a/b/d/rz;)V
iload 2
iconst_1
isub
istore 1
goto L12
L2:
aload 0
aload 11
putfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 0
iload 1
putfield com/a/b/d/sa/b I
return
L8:
goto L9
L3:
goto L5
.limit locals 12
.limit stack 4
.end method

.method private q()V
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L11 to L12 using L2
.catch all from L12 to L13 using L2
.catch all from L13 to L14 using L2
.catch all from L14 to L15 using L2
aload 0
getfield com/a/b/d/sa/b I
ifeq L16
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 3
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/t Ljava/util/Queue;
getstatic com/a/b/d/qy/y Ljava/util/Queue;
if_acmpeq L17
L1:
iconst_0
istore 1
L3:
iload 1
aload 3
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
if_icmpge L17
aload 3
iload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 2
L4:
aload 2
ifnull L18
L5:
aload 2
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/b()Z 0
ifne L6
aload 0
aload 2
getstatic com/a/b/d/qq/a Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V
L6:
aload 2
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 2
L7:
goto L4
L8:
iload 1
aload 3
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
if_icmpge L10
aload 3
iload 1
aconst_null
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
L9:
iload 1
iconst_1
iadd
istore 1
goto L8
L10:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/e()Z
ifeq L12
L11:
aload 0
getfield com/a/b/d/sa/g Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
ifnonnull L11
L12:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/f()Z
ifeq L14
L13:
aload 0
getfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
invokevirtual java/lang/ref/ReferenceQueue/poll()Ljava/lang/ref/Reference;
ifnonnull L13
L14:
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
invokeinterface java/util/Queue/clear()V 0
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
invokeinterface java/util/Queue/clear()V 0
aload 0
getfield com/a/b/d/sa/j Ljava/util/concurrent/atomic/AtomicInteger;
iconst_0
invokevirtual java/util/concurrent/atomic/AtomicInteger/set(I)V
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
iconst_0
putfield com/a/b/d/sa/b I
L15:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
L16:
return
L2:
astore 2
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 2
athrow
L18:
iload 1
iconst_1
iadd
istore 1
goto L3
L17:
iconst_0
istore 1
goto L8
.limit locals 4
.limit stack 3
.end method

.method private r()V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
invokevirtual com/a/b/d/sa/c()V
return
.limit locals 1
.limit stack 1
.end method

.method private s()V
aload 0
invokevirtual com/a/b/d/sa/d()V
return
.limit locals 1
.limit stack 1
.end method

.method final a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 1
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
ifnonnull L0
L1:
aconst_null
areturn
L0:
aload 1
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
astore 3
aload 3
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 4
aload 4
ifnonnull L2
aload 3
invokeinterface com/a/b/d/sr/b()Z 0
ifeq L1
L2:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/v Lcom/a/b/d/re;
aload 0
aload 1
aload 2
invokevirtual com/a/b/d/re/a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 1
aload 3
aload 0
getfield com/a/b/d/sa/h Ljava/lang/ref/ReferenceQueue;
aload 4
aload 1
invokeinterface com/a/b/d/sr/a(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;Lcom/a/b/d/rz;)Lcom/a/b/d/sr; 3
invokeinterface com/a/b/d/rz/a(Lcom/a/b/d/sr;)V 1
aload 1
areturn
.limit locals 5
.limit stack 5
.end method

.method final a(Ljava/lang/Object;I)Lcom/a/b/d/rz;
aload 0
getfield com/a/b/d/sa/b I
ifeq L0
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 3
aload 3
aload 3
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iload 2
iand
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 3
L1:
aload 3
ifnull L0
aload 3
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L2
aload 3
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 4
aload 4
ifnonnull L3
aload 0
invokespecial com/a/b/d/sa/e()V
L2:
aload 3
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 3
goto L1
L3:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 4
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L2
aload 3
areturn
L0:
aconst_null
areturn
.limit locals 5
.limit stack 3
.end method

.method final a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
.annotation visibleparam 3 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/v Lcom/a/b/d/re;
aload 0
aload 1
iload 2
aload 3
invokevirtual com/a/b/d/re/a(Lcom/a/b/d/sa;Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;
areturn
.limit locals 4
.limit stack 5
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
invokevirtual com/a/b/d/sa/c()V
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 7
iload 2
aload 7
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 4
aload 7
iload 4
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 6
L1:
aload 6
astore 5
L13:
aload 5
ifnull L14
L3:
aload 5
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 8
aload 5
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L11
L4:
aload 8
ifnull L11
L5:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 8
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L11
aload 5
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
astore 9
aload 9
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 10
L6:
aload 10
ifnonnull L9
L7:
aload 9
invokestatic com/a/b/d/sa/a(Lcom/a/b/d/sr;)Z
ifeq L8
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 8
aload 10
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 6
aload 5
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 7
iload 4
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 2
iconst_1
isub
putfield com/a/b/d/sa/b I
L8:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aconst_null
areturn
L9:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 1
aload 10
getstatic com/a/b/d/qq/b Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 5
aload 3
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
L10:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 10
areturn
L11:
aload 5
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 5
L12:
goto L13
L14:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aconst_null
areturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
.limit locals 11
.limit stack 4
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
.catch all from L15 to L16 using L2
.catch all from L17 to L18 using L2
.catch all from L19 to L20 using L2
.catch all from L21 to L22 using L2
.catch all from L22 to L23 using L2
.catch all from L24 to L25 using L2
.catch all from L26 to L27 using L2
.catch all from L27 to L28 using L2
.catch all from L28 to L29 using L2
.catch all from L30 to L31 using L2
.catch all from L32 to L33 using L2
.catch all from L34 to L35 using L2
.catch all from L35 to L36 using L2
.catch all from L37 to L38 using L2
.catch all from L39 to L40 using L2
.catch all from L41 to L42 using L2
.catch all from L43 to L44 using L2
.catch all from L45 to L46 using L2
.catch all from L46 to L47 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
invokevirtual com/a/b/d/sa/c()V
aload 0
getfield com/a/b/d/sa/b I
iconst_1
iadd
istore 6
L1:
iload 6
istore 5
L3:
iload 6
aload 0
getfield com/a/b/d/sa/d I
if_icmple L28
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 14
aload 14
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
istore 9
L4:
iload 9
ldc_w 1073741824
if_icmpge L27
L5:
aload 0
getfield com/a/b/d/sa/b I
istore 5
iload 9
iconst_1
ishl
invokestatic com/a/b/d/sa/a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 15
aload 0
aload 15
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_3
imul
iconst_4
idiv
putfield com/a/b/d/sa/d I
aload 15
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
istore 10
L6:
iconst_0
istore 7
L48:
iload 7
iload 9
if_icmpge L26
L7:
aload 14
iload 7
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 13
L8:
iload 5
istore 6
aload 13
ifnull L49
L9:
aload 13
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 11
aload 13
invokeinterface com/a/b/d/rz/c()I 0
iload 10
iand
istore 6
L10:
aload 11
ifnonnull L50
L11:
aload 15
iload 6
aload 13
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
L12:
goto L51
L52:
aload 11
ifnull L17
L13:
aload 11
invokeinterface com/a/b/d/rz/c()I 0
iload 10
iand
istore 8
L14:
iload 8
iload 6
if_icmpeq L53
iload 8
istore 6
aload 11
astore 12
L15:
aload 11
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 11
L16:
goto L52
L17:
aload 15
iload 6
aload 12
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
L18:
aload 13
astore 11
L54:
iload 5
istore 6
aload 11
aload 12
if_acmpeq L49
L19:
aload 11
invokeinterface com/a/b/d/rz/c()I 0
iload 10
iand
istore 6
aload 0
aload 11
aload 15
iload 6
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 13
L20:
aload 13
ifnull L24
L21:
aload 15
iload 6
aload 13
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
L22:
aload 11
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 11
L23:
goto L54
L24:
aload 0
aload 11
invokespecial com/a/b/d/sa/e(Lcom/a/b/d/rz;)V
L25:
iload 5
iconst_1
isub
istore 5
goto L22
L26:
aload 0
aload 15
putfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
aload 0
iload 5
putfield com/a/b/d/sa/b I
L27:
aload 0
getfield com/a/b/d/sa/b I
iconst_1
iadd
istore 5
L28:
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 13
iload 2
aload 13
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 6
aload 13
iload 6
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 12
L29:
aload 12
astore 11
L55:
aload 11
ifnull L45
L30:
aload 11
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 14
aload 11
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L43
L31:
aload 14
ifnull L43
L32:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 14
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L43
aload 11
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
astore 12
aload 12
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 13
L33:
aload 13
ifnonnull L56
L34:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 11
aload 3
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
aload 12
invokeinterface com/a/b/d/sr/b()Z 0
ifne L37
aload 0
aload 1
aload 13
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
getfield com/a/b/d/sa/b I
istore 5
L35:
aload 0
iload 5
putfield com/a/b/d/sa/b I
L36:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aconst_null
areturn
L37:
aload 0
invokespecial com/a/b/d/sa/o()Z
ifeq L35
aload 0
getfield com/a/b/d/sa/b I
iconst_1
iadd
istore 5
L38:
goto L35
L56:
iload 4
ifeq L41
L39:
aload 0
aload 11
invokevirtual com/a/b/d/sa/b(Lcom/a/b/d/rz;)V
L40:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 13
areturn
L41:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 1
aload 13
getstatic com/a/b/d/qq/b Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 11
aload 3
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
L42:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 13
areturn
L43:
aload 11
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 11
L44:
goto L55
L45:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 1
iload 2
aload 12
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;ILcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
aload 1
aload 3
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
aload 13
iload 6
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
invokespecial com/a/b/d/sa/o()Z
ifeq L57
aload 0
getfield com/a/b/d/sa/b I
iconst_1
iadd
istore 2
L46:
aload 0
iload 2
putfield com/a/b/d/sa/b I
L47:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aconst_null
areturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
L57:
iload 5
istore 2
goto L46
L49:
iload 6
istore 5
goto L51
L53:
goto L15
L51:
iload 7
iconst_1
iadd
istore 7
goto L48
L50:
aload 13
astore 12
goto L52
.limit locals 16
.limit stack 4
.end method

.method final a()V
aload 0
getfield com/a/b/d/sa/j Ljava/util/concurrent/atomic/AtomicInteger;
invokevirtual java/util/concurrent/atomic/AtomicInteger/incrementAndGet()I
bipush 63
iand
ifne L0
aload 0
invokevirtual com/a/b/d/sa/b()V
L0:
return
.limit locals 1
.limit stack 2
.end method

.method final a(Lcom/a/b/d/rz;)V
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/d()Z
ifeq L0
aload 0
aload 1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/r J
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;J)V
L0:
aload 0
getfield com/a/b/d/sa/i Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 4
.end method

.method final a(Lcom/a/b/d/rz;Lcom/a/b/d/qq;)V
aload 1
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 3
aload 1
invokeinterface com/a/b/d/rz/c()I 0
pop
aload 0
aload 3
aload 1
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
aload 2
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
return
.limit locals 4
.limit stack 4
.end method

.method final a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/t Ljava/util/Queue;
getstatic com/a/b/d/qy/y Ljava/util/Queue;
if_acmpeq L0
new com/a/b/d/qx
dup
aload 1
aload 2
aload 3
invokespecial com/a/b/d/qx/<init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
astore 1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/t Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/offer(Ljava/lang/Object;)Z 1
pop
L0:
return
.limit locals 4
.limit stack 5
.end method

.method final a(Lcom/a/b/d/rz;I)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
getfield com/a/b/d/sa/b I
istore 3
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 6
iload 2
aload 6
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 2
aload 6
iload 2
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 5
L1:
aload 5
astore 4
L7:
aload 4
ifnull L8
aload 4
aload 1
if_acmpne L5
L3:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 4
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
aload 4
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 5
aload 4
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
getfield com/a/b/d/sa/b I
istore 3
aload 6
iload 2
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 3
iconst_1
isub
putfield com/a/b/d/sa/b I
L4:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_1
ireturn
L5:
aload 4
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 4
L6:
goto L7
L8:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
.limit locals 7
.limit stack 4
.end method

.method final a(Ljava/lang/Object;ILcom/a/b/d/sr;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
getfield com/a/b/d/sa/b I
istore 4
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 7
iload 2
aload 7
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 4
aload 7
iload 4
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 6
L1:
aload 6
astore 5
L9:
aload 5
ifnull L10
L3:
aload 5
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 8
aload 5
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L7
L4:
aload 8
ifnull L7
L5:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 8
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L7
aload 5
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
aload 3
if_acmpne L11
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 1
aload 3
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 6
aload 5
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 7
iload 4
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 2
iconst_1
isub
putfield com/a/b/d/sa/b I
L6:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/isHeldByCurrentThread()Z
ifne L12
aload 0
invokevirtual com/a/b/d/sa/d()V
L12:
iconst_1
ireturn
L11:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/isHeldByCurrentThread()Z
ifne L13
aload 0
invokevirtual com/a/b/d/sa/d()V
L13:
iconst_0
ireturn
L7:
aload 5
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 5
L8:
goto L9
L10:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/isHeldByCurrentThread()Z
ifne L14
aload 0
invokevirtual com/a/b/d/sa/d()V
L14:
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/isHeldByCurrentThread()Z
ifne L15
aload 0
invokevirtual com/a/b/d/sa/d()V
L15:
aload 1
athrow
.limit locals 9
.limit stack 4
.end method

.method final a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L9 to L10 using L2
.catch all from L11 to L12 using L2
.catch all from L13 to L14 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
invokevirtual com/a/b/d/sa/c()V
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 8
iload 2
aload 8
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 5
aload 8
iload 5
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 7
L1:
aload 7
astore 6
L15:
aload 6
ifnull L16
L3:
aload 6
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 9
aload 6
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L13
L4:
aload 9
ifnull L13
L5:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 9
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L13
aload 6
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
astore 10
aload 10
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 11
L6:
aload 11
ifnonnull L9
L7:
aload 10
invokestatic com/a/b/d/sa/a(Lcom/a/b/d/sr;)Z
ifeq L8
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 9
aload 11
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 7
aload 6
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 8
iload 5
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 2
iconst_1
isub
putfield com/a/b/d/sa/b I
L8:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L9:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/n Lcom/a/b/b/au;
aload 3
aload 11
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L11
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 1
aload 11
getstatic com/a/b/d/qq/b Lcom/a/b/d/qq;
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 6
aload 4
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;Ljava/lang/Object;)V
L10:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_1
ireturn
L11:
aload 0
aload 6
invokevirtual com/a/b/d/sa/b(Lcom/a/b/d/rz;)V
L12:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L13:
aload 6
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 6
L14:
goto L15
L16:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
.limit locals 12
.limit stack 4
.end method

.method final b(Ljava/lang/Object;I)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
L0:
aload 0
aload 1
iload 2
invokespecial com/a/b/d/sa/e(Ljava/lang/Object;I)Lcom/a/b/d/rz;
astore 1
L1:
aload 1
ifnonnull L3
aload 0
invokevirtual com/a/b/d/sa/a()V
aconst_null
areturn
L3:
aload 1
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 3
L4:
aload 3
ifnull L7
L5:
aload 0
aload 1
invokevirtual com/a/b/d/sa/a(Lcom/a/b/d/rz;)V
L6:
aload 0
invokevirtual com/a/b/d/sa/a()V
aload 3
areturn
L7:
aload 0
invokespecial com/a/b/d/sa/e()V
L8:
goto L6
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/a()V
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method final b()V
aload 0
invokevirtual com/a/b/d/sa/c()V
aload 0
invokevirtual com/a/b/d/sa/d()V
return
.limit locals 1
.limit stack 1
.end method

.method final b(Lcom/a/b/d/rz;)V
.annotation invisible Ljavax/annotation/concurrent/GuardedBy;
value s = "Segment.this"
.end annotation
aload 0
getfield com/a/b/d/sa/k Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/d()Z
ifeq L0
aload 0
aload 1
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/r J
invokespecial com/a/b/d/sa/a(Lcom/a/b/d/rz;J)V
aload 0
getfield com/a/b/d/sa/l Ljava/util/Queue;
aload 1
invokeinterface java/util/Queue/add(Ljava/lang/Object;)Z 1
pop
L0:
return
.limit locals 2
.limit stack 4
.end method

.method final b(Ljava/lang/Object;ILcom/a/b/d/sr;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 7
iload 2
aload 7
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 4
aload 7
iload 4
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 6
L1:
aload 6
astore 5
L9:
aload 5
ifnull L10
L3:
aload 5
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 8
aload 5
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L7
L4:
aload 8
ifnull L7
L5:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 8
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L7
aload 5
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
aload 3
if_acmpne L11
aload 7
iload 4
aload 0
aload 6
aload 5
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
L6:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_1
ireturn
L11:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L7:
aload 5
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 5
L8:
goto L9
L10:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
.limit locals 9
.limit stack 5
.end method

.method final b(Ljava/lang/Object;ILjava/lang/Object;)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L6 to L7 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
invokevirtual com/a/b/d/sa/c()V
aload 0
getfield com/a/b/d/sa/b I
istore 4
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 8
iload 2
aload 8
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 4
aload 8
iload 4
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 7
L1:
aload 7
astore 6
L12:
aload 6
ifnull L13
L3:
aload 6
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 9
aload 6
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L10
L4:
aload 9
ifnull L10
L5:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 9
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L10
aload 6
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
astore 1
aload 1
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 10
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/n Lcom/a/b/b/au;
aload 3
aload 10
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L8
getstatic com/a/b/d/qq/a Lcom/a/b/d/qq;
astore 1
L6:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 9
aload 10
aload 1
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 7
aload 6
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 3
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 8
iload 4
aload 3
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 2
iconst_1
isub
putfield com/a/b/d/sa/b I
getstatic com/a/b/d/qq/a Lcom/a/b/d/qq;
astore 3
L7:
aload 1
aload 3
if_acmpne L14
iconst_1
istore 5
L15:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iload 5
ireturn
L8:
aload 1
invokestatic com/a/b/d/sa/a(Lcom/a/b/d/sr;)Z
ifeq L16
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
astore 1
L9:
goto L6
L16:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L14:
iconst_0
istore 5
goto L15
L10:
aload 6
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 6
L11:
goto L12
L13:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
.limit locals 11
.limit stack 4
.end method

.method final c(Lcom/a/b/d/rz;)Ljava/lang/Object;
aload 1
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
ifnonnull L0
aload 0
invokespecial com/a/b/d/sa/e()V
aconst_null
areturn
L0:
aload 1
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 2
aload 2
ifnonnull L1
aload 0
invokespecial com/a/b/d/sa/e()V
aconst_null
areturn
L1:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
invokevirtual com/a/b/d/qy/c()Z
ifeq L2
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
aload 1
invokevirtual com/a/b/d/qy/a(Lcom/a/b/d/rz;)Z
ifeq L2
aload 0
invokespecial com/a/b/d/sa/m()V
aconst_null
areturn
L2:
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method final c()V
.catch all from L0 to L1 using L2
aload 0
invokevirtual com/a/b/d/sa/tryLock()Z
ifeq L3
L0:
aload 0
invokespecial com/a/b/d/sa/f()V
aload 0
invokespecial com/a/b/d/sa/n()V
aload 0
getfield com/a/b/d/sa/j Ljava/util/concurrent/atomic/AtomicInteger;
iconst_0
invokevirtual java/util/concurrent/atomic/AtomicInteger/set(I)V
L1:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
L3:
return
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 1
athrow
.limit locals 2
.limit stack 2
.end method

.method final c(Ljava/lang/Object;I)Z
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
iconst_0
istore 3
L0:
aload 0
getfield com/a/b/d/sa/b I
ifeq L5
aload 0
aload 1
iload 2
invokespecial com/a/b/d/sa/e(Ljava/lang/Object;I)Lcom/a/b/d/rz;
astore 1
L1:
aload 1
ifnonnull L3
aload 0
invokevirtual com/a/b/d/sa/a()V
iconst_0
ireturn
L3:
aload 1
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 1
L4:
aload 1
ifnull L6
iconst_1
istore 3
L6:
aload 0
invokevirtual com/a/b/d/sa/a()V
iload 3
ireturn
L5:
aload 0
invokevirtual com/a/b/d/sa/a()V
iconst_0
ireturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/a()V
aload 1
athrow
.limit locals 4
.limit stack 3
.end method

.method final d(Ljava/lang/Object;I)Ljava/lang/Object;
.catch all from L0 to L1 using L2
.catch all from L3 to L4 using L2
.catch all from L5 to L6 using L2
.catch all from L7 to L8 using L2
.catch all from L8 to L9 using L2
.catch all from L10 to L11 using L2
.catch all from L12 to L13 using L2
aload 0
invokevirtual com/a/b/d/sa/lock()V
L0:
aload 0
invokevirtual com/a/b/d/sa/c()V
aload 0
getfield com/a/b/d/sa/b I
istore 3
aload 0
getfield com/a/b/d/sa/e Ljava/util/concurrent/atomic/AtomicReferenceArray;
astore 6
iload 2
aload 6
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/length()I
iconst_1
isub
iand
istore 3
aload 6
iload 3
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/get(I)Ljava/lang/Object;
checkcast com/a/b/d/rz
astore 5
L1:
aload 5
astore 4
L14:
aload 4
ifnull L15
L3:
aload 4
invokeinterface com/a/b/d/rz/d()Ljava/lang/Object; 0
astore 7
aload 4
invokeinterface com/a/b/d/rz/c()I 0
iload 2
if_icmpne L12
L4:
aload 7
ifnull L12
L5:
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
getfield com/a/b/d/qy/m Lcom/a/b/b/au;
aload 1
aload 7
invokevirtual com/a/b/b/au/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L12
aload 4
invokeinterface com/a/b/d/rz/a()Lcom/a/b/d/sr; 0
astore 1
aload 1
invokeinterface com/a/b/d/sr/get()Ljava/lang/Object; 0
astore 8
L6:
aload 8
ifnull L10
L7:
getstatic com/a/b/d/qq/a Lcom/a/b/d/qq;
astore 1
L8:
aload 0
aload 0
getfield com/a/b/d/sa/c I
iconst_1
iadd
putfield com/a/b/d/sa/c I
aload 0
aload 7
aload 8
aload 1
invokevirtual com/a/b/d/sa/a(Ljava/lang/Object;Ljava/lang/Object;Lcom/a/b/d/qq;)V
aload 0
aload 5
aload 4
invokespecial com/a/b/d/sa/b(Lcom/a/b/d/rz;Lcom/a/b/d/rz;)Lcom/a/b/d/rz;
astore 1
aload 0
getfield com/a/b/d/sa/b I
istore 2
aload 6
iload 3
aload 1
invokevirtual java/util/concurrent/atomic/AtomicReferenceArray/set(ILjava/lang/Object;)V
aload 0
iload 2
iconst_1
isub
putfield com/a/b/d/sa/b I
L9:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 8
areturn
L10:
aload 1
invokestatic com/a/b/d/sa/a(Lcom/a/b/d/sr;)Z
ifeq L16
getstatic com/a/b/d/qq/c Lcom/a/b/d/qq;
astore 1
L11:
goto L8
L16:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aconst_null
areturn
L12:
aload 4
invokeinterface com/a/b/d/rz/b()Lcom/a/b/d/rz; 0
astore 4
L13:
goto L14
L15:
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aconst_null
areturn
L2:
astore 1
aload 0
invokevirtual com/a/b/d/sa/unlock()V
aload 0
invokevirtual com/a/b/d/sa/d()V
aload 1
athrow
.limit locals 9
.limit stack 4
.end method

.method final d()V
aload 0
invokevirtual com/a/b/d/sa/isHeldByCurrentThread()Z
ifne L0
aload 0
getfield com/a/b/d/sa/a Lcom/a/b/d/qy;
astore 1
L1:
aload 1
getfield com/a/b/d/qy/t Ljava/util/Queue;
invokeinterface java/util/Queue/poll()Ljava/lang/Object; 0
checkcast com/a/b/d/qx
ifnonnull L1
L0:
return
.limit locals 2
.limit stack 1
.end method
