.bytecode 50.0
.class synchronized com/a/b/d/wv
.super com/a/b/d/an

.field final 'a' Lcom/a/b/d/vi;

.field final 'b' Lcom/a/b/d/tv;

.method <init>(Lcom/a/b/d/vi;Lcom/a/b/d/tv;)V
aload 0
invokespecial com/a/b/d/an/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/vi
putfield com/a/b/d/wv/a Lcom/a/b/d/vi;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/tv
putfield com/a/b/d/wv/b Lcom/a/b/d/tv;
return
.limit locals 3
.limit stack 2
.end method

.method a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
aload 0
getfield com/a/b/d/wv/b Lcom/a/b/d/tv;
aload 1
invokestatic com/a/b/d/sz/a(Lcom/a/b/d/tv;Ljava/lang/Object;)Lcom/a/b/b/bj;
astore 1
aload 2
instanceof java/util/List
ifeq L0
aload 2
checkcast java/util/List
aload 1
invokestatic com/a/b/d/ov/a(Ljava/util/List;Lcom/a/b/b/bj;)Ljava/util/List;
areturn
L0:
aload 2
aload 1
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;
areturn
.limit locals 3
.limit stack 2
.end method

.method public final a(Lcom/a/b/d/vi;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Iterable;)Ljava/util/Collection;
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
invokevirtual com/a/b/d/wv/a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 4
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Iterable;)Z
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Z
aload 0
aload 1
invokevirtual com/a/b/d/wv/c(Ljava/lang/Object;)Ljava/util/Collection;
aload 2
invokeinterface java/util/Collection/remove(Ljava/lang/Object;)Z 1
ireturn
.limit locals 3
.limit stack 2
.end method

.method public d(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/d(Ljava/lang/Object;)Ljava/util/Collection; 1
invokevirtual com/a/b/d/wv/a(Ljava/lang/Object;Ljava/util/Collection;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 4
.end method

.method public final f()I
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/f()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final f(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/f(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final g()V
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/g()V 0
return
.limit locals 1
.limit stack 1
.end method

.method final l()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
aload 0
getfield com/a/b/d/wv/b Lcom/a/b/d/tv;
invokestatic com/a/b/d/sz/b(Lcom/a/b/d/tv;)Lcom/a/b/b/bj;
invokestatic com/a/b/d/nj/a(Ljava/util/Iterator;Lcom/a/b/b/bj;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 2
.end method

.method final m()Ljava/util/Map;
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
new com/a/b/d/ww
dup
aload 0
invokespecial com/a/b/d/ww/<init>(Lcom/a/b/d/wv;)V
invokestatic com/a/b/d/sz/a(Ljava/util/Map;Lcom/a/b/d/tv;)Ljava/util/Map;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final n()Z
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/n()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final p()Ljava/util/Set;
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final q()Lcom/a/b/d/xc;
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/q()Lcom/a/b/d/xc; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method final s()Ljava/util/Collection;
aload 0
getfield com/a/b/d/wv/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/k()Ljava/util/Collection; 0
aload 0
getfield com/a/b/d/wv/b Lcom/a/b/d/tv;
invokestatic com/a/b/d/sz/a(Lcom/a/b/d/tv;)Lcom/a/b/b/bj;
invokestatic com/a/b/d/cm/a(Ljava/util/Collection;Lcom/a/b/b/bj;)Ljava/util/Collection;
areturn
.limit locals 1
.limit stack 2
.end method
