.bytecode 50.0
.class final synchronized com/a/b/d/wf
.super com/a/b/d/uj

.field final 'a' Lcom/a/b/d/vi;

.method <init>(Lcom/a/b/d/vi;)V
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/vi
putfield com/a/b/d/wf/a Lcom/a/b/d/vi;
return
.limit locals 2
.limit stack 2
.end method

.method static synthetic a(Lcom/a/b/d/wf;)Lcom/a/b/d/vi;
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
aload 1
invokeinterface java/util/Set/remove(Ljava/lang/Object;)Z 1
pop
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/wf/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private c(Ljava/lang/Object;)Ljava/util/Collection;
aload 0
aload 1
invokevirtual com/a/b/d/wf/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/d(Ljava/lang/Object;)Ljava/util/Collection; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method protected final a()Ljava/util/Set;
new com/a/b/d/wg
dup
aload 0
invokespecial com/a/b/d/wg/<init>(Lcom/a/b/d/wf;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/g()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public final containsKey(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/f(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual com/a/b/d/wf/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final isEmpty()Z
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/n()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final keySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual com/a/b/d/wf/containsKey(Ljava/lang/Object;)Z
ifeq L0
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/d(Ljava/lang/Object;)Ljava/util/Collection; 1
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method public final size()I
aload 0
getfield com/a/b/d/wf/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/p()Ljava/util/Set; 0
invokeinterface java/util/Set/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
