.bytecode 50.0
.class final synchronized com/a/b/d/aah
.super com/a/b/d/gh
.implements java/util/Set

.field private final transient 'a' Lcom/a/b/d/jl;

.field private final transient 'b' Lcom/a/b/d/ci;

.method private <init>(Lcom/a/b/d/jl;Lcom/a/b/d/ci;)V
aload 0
invokespecial com/a/b/d/gh/<init>()V
aload 0
aload 1
putfield com/a/b/d/aah/a Lcom/a/b/d/jl;
aload 0
aload 2
putfield com/a/b/d/aah/b Lcom/a/b/d/ci;
return
.limit locals 3
.limit stack 2
.end method

.method static a(Ljava/util/List;)Ljava/util/Set;
new com/a/b/d/jn
dup
aload 0
invokeinterface java/util/List/size()I 0
invokespecial com/a/b/d/jn/<init>(I)V
astore 1
aload 0
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 0
L0:
aload 0
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 0
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Set
invokestatic com/a/b/d/lo/a(Ljava/util/Collection;)Lcom/a/b/d/lo;
astore 2
aload 2
invokevirtual com/a/b/d/lo/isEmpty()Z
ifeq L2
invokestatic com/a/b/d/lo/h()Lcom/a/b/d/lo;
areturn
L2:
aload 1
aload 2
invokevirtual com/a/b/d/jn/c(Ljava/lang/Object;)Lcom/a/b/d/jn;
pop
goto L0
L1:
aload 1
invokevirtual com/a/b/d/jn/b()Lcom/a/b/d/jl;
astore 0
new com/a/b/d/aah
dup
aload 0
new com/a/b/d/ci
dup
new com/a/b/d/aai
dup
aload 0
invokespecial com/a/b/d/aai/<init>(Lcom/a/b/d/jl;)V
invokespecial com/a/b/d/ci/<init>(Lcom/a/b/d/jl;)V
invokespecial com/a/b/d/aah/<init>(Lcom/a/b/d/jl;Lcom/a/b/d/ci;)V
areturn
.limit locals 3
.limit stack 8
.end method

.method protected final b()Ljava/util/Collection;
aload 0
getfield com/a/b/d/aah/b Lcom/a/b/d/ci;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
instanceof com/a/b/d/aah
ifeq L0
aload 1
checkcast com/a/b/d/aah
astore 1
aload 0
getfield com/a/b/d/aah/a Lcom/a/b/d/jl;
aload 1
getfield com/a/b/d/aah/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/equals(Ljava/lang/Object;)Z
ireturn
L0:
aload 0
aload 1
invokespecial java/lang/Object/equals(Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
invokevirtual com/a/b/d/aah/size()I
iconst_1
isub
istore 1
iconst_0
istore 2
L0:
iload 2
aload 0
getfield com/a/b/d/aah/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
if_icmpge L1
iload 1
bipush 31
imul
iconst_m1
ixor
iconst_m1
ixor
istore 1
iload 2
iconst_1
iadd
istore 2
goto L0
L1:
aload 0
getfield com/a/b/d/aah/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/iterator()Ljava/util/Iterator;
astore 4
iconst_1
istore 2
L2:
aload 4
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L3
aload 4
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Set
astore 5
aload 0
invokevirtual com/a/b/d/aah/size()I
aload 5
invokeinterface java/util/Set/size()I 0
idiv
istore 3
aload 5
invokeinterface java/util/Set/hashCode()I 0
iload 3
imul
iload 2
bipush 31
imul
iadd
iconst_m1
ixor
iconst_m1
ixor
istore 2
goto L2
L3:
iload 2
iload 1
iadd
iconst_m1
ixor
iconst_m1
ixor
ireturn
.limit locals 6
.limit stack 3
.end method

.method protected final volatile synthetic k_()Ljava/lang/Object;
aload 0
getfield com/a/b/d/aah/b Lcom/a/b/d/ci;
areturn
.limit locals 1
.limit stack 1
.end method
