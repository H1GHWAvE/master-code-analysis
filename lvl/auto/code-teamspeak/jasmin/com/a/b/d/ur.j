.bytecode 50.0
.class synchronized com/a/b/d/ur
.super com/a/b/d/uj

.field final 'a' Ljava/util/Map;

.field final 'b' Lcom/a/b/d/tv;

.method <init>(Ljava/util/Map;Lcom/a/b/d/tv;)V
aload 0
invokespecial com/a/b/d/uj/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Map
putfield com/a/b/d/ur/a Ljava/util/Map;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/tv
putfield com/a/b/d/ur/b Lcom/a/b/d/tv;
return
.limit locals 3
.limit stack 2
.end method

.method protected final a()Ljava/util/Set;
new com/a/b/d/us
dup
aload 0
invokespecial com/a/b/d/us/<init>(Lcom/a/b/d/ur;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public clear()V
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
invokeinterface java/util/Map/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method public containsKey(Ljava/lang/Object;)Z
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
astore 2
aload 2
ifnonnull L0
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L1
L0:
aload 0
getfield com/a/b/d/ur/b Lcom/a/b/d/tv;
aload 1
aload 2
invokeinterface com/a/b/d/tv/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
L1:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method public keySet()Ljava/util/Set;
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L0
aload 0
getfield com/a/b/d/ur/b Lcom/a/b/d/tv;
aload 1
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
aload 1
invokeinterface java/util/Map/remove(Ljava/lang/Object;)Ljava/lang/Object; 1
invokeinterface com/a/b/d/tv/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 4
.end method

.method public size()I
aload 0
getfield com/a/b/d/ur/a Ljava/util/Map;
invokeinterface java/util/Map/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method
