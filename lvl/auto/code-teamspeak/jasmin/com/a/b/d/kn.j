.bytecode 50.0
.class public synchronized com/a/b/d/kn
.super java/lang/Object

.field 'a' Lcom/a/b/d/vi;

.field 'b' Ljava/util/Comparator;

.field 'c' Ljava/util/Comparator;

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
new com/a/b/d/ko
dup
invokespecial com/a/b/d/ko/<init>()V
putfield com/a/b/d/kn/a Lcom/a/b/d/vi;
return
.limit locals 1
.limit stack 3
.end method

.method public a(Lcom/a/b/d/vi;)Lcom/a/b/d/kn;
aload 1
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 1
L0:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 2
aload 0
aload 2
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/lang/Iterable
invokevirtual com/a/b/d/kn/a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;
pop
goto L0
L1:
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;
aload 1
ifnonnull L0
aload 2
invokestatic com/a/b/d/mq/a(Ljava/lang/Iterable;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 1
invokevirtual java/lang/String/length()I
ifeq L1
ldc "null key in entry: null="
aload 1
invokevirtual java/lang/String/concat(Ljava/lang/String;)Ljava/lang/String;
astore 1
L2:
new java/lang/NullPointerException
dup
aload 1
invokespecial java/lang/NullPointerException/<init>(Ljava/lang/String;)V
athrow
L1:
new java/lang/String
dup
ldc "null key in entry: null="
invokespecial java/lang/String/<init>(Ljava/lang/String;)V
astore 1
goto L2
L0:
aload 0
getfield com/a/b/d/kn/a Lcom/a/b/d/vi;
aload 1
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;)Ljava/util/Collection; 1
astore 3
aload 2
invokeinterface java/lang/Iterable/iterator()Ljava/util/Iterator; 0
astore 2
L3:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 4
aload 1
aload 4
invokestatic com/a/b/d/cl/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 3
aload 4
invokeinterface java/util/Collection/add(Ljava/lang/Object;)Z 1
pop
goto L3
L4:
aload 0
areturn
.limit locals 5
.limit stack 3
.end method

.method public transient a(Ljava/lang/Object;[Ljava/lang/Object;)Lcom/a/b/d/kn;
aload 0
aload 1
aload 2
invokestatic java/util/Arrays/asList([Ljava/lang/Object;)Ljava/util/List;
invokevirtual com/a/b/d/kn/a(Ljava/lang/Object;Ljava/lang/Iterable;)Lcom/a/b/d/kn;
areturn
.limit locals 3
.limit stack 3
.end method

.method public a(Ljava/util/Comparator;)Lcom/a/b/d/kn;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/kn/c Ljava/util/Comparator;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method

.method public a(Ljava/util/Map$Entry;)Lcom/a/b/d/kn;
aload 0
aload 1
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 1
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
invokevirtual com/a/b/d/kn/b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;
areturn
.limit locals 2
.limit stack 3
.end method

.method public b()Lcom/a/b/d/kk;
aload 0
getfield com/a/b/d/kn/c Ljava/util/Comparator;
ifnull L0
aload 0
getfield com/a/b/d/kn/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 1
L1:
aload 1
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L0
aload 1
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Collection
checkcast java/util/List
aload 0
getfield com/a/b/d/kn/c Ljava/util/Comparator;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
goto L1
L0:
aload 0
getfield com/a/b/d/kn/b Ljava/util/Comparator;
ifnull L2
new com/a/b/d/ko
dup
invokespecial com/a/b/d/ko/<init>()V
astore 1
aload 0
getfield com/a/b/d/kn/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
invokeinterface java/util/Map/entrySet()Ljava/util/Set; 0
invokestatic com/a/b/d/ov/a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
astore 2
aload 2
aload 0
getfield com/a/b/d/kn/b Ljava/util/Comparator;
invokestatic com/a/b/d/yd/a(Ljava/util/Comparator;)Lcom/a/b/d/yd;
invokestatic com/a/b/d/sz/a()Lcom/a/b/b/bj;
invokevirtual com/a/b/d/yd/a(Lcom/a/b/b/bj;)Lcom/a/b/d/yd;
invokestatic java/util/Collections/sort(Ljava/util/List;Ljava/util/Comparator;)V
aload 2
invokeinterface java/util/List/iterator()Ljava/util/Iterator; 0
astore 2
L3:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/util/Map$Entry
astore 3
aload 1
aload 3
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
aload 3
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast java/lang/Iterable
invokeinterface com/a/b/d/vi/c(Ljava/lang/Object;Ljava/lang/Iterable;)Z 2
pop
goto L3
L4:
aload 0
aload 1
putfield com/a/b/d/kn/a Lcom/a/b/d/vi;
L2:
aload 0
getfield com/a/b/d/kn/a Lcom/a/b/d/vi;
invokestatic com/a/b/d/kk/b(Lcom/a/b/d/vi;)Lcom/a/b/d/kk;
areturn
.limit locals 4
.limit stack 3
.end method

.method public b(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/kn;
aload 1
aload 2
invokestatic com/a/b/d/cl/a(Ljava/lang/Object;Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/kn/a Lcom/a/b/d/vi;
aload 1
aload 2
invokeinterface com/a/b/d/vi/a(Ljava/lang/Object;Ljava/lang/Object;)Z 2
pop
aload 0
areturn
.limit locals 3
.limit stack 3
.end method

.method public b(Ljava/util/Comparator;)Lcom/a/b/d/kn;
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/util/Comparator
putfield com/a/b/d/kn/b Ljava/util/Comparator;
aload 0
areturn
.limit locals 2
.limit stack 2
.end method
