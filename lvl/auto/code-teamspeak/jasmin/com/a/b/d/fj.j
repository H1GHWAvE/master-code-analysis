.bytecode 50.0
.class final synchronized com/a/b/d/fj
.super com/a/b/d/uj

.field final synthetic 'a' Lcom/a/b/d/fi;

.method <init>(Lcom/a/b/d/fi;)V
aload 0
aload 1
putfield com/a/b/d/fj/a Lcom/a/b/d/fi;
aload 0
invokespecial com/a/b/d/uj/<init>()V
return
.limit locals 2
.limit stack 2
.end method

.method private b(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/fj/a Lcom/a/b/d/fi;
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 2
aload 2
ifnonnull L0
aconst_null
astore 1
L1:
aload 1
areturn
L0:
aload 2
new com/a/b/d/fr
dup
aload 0
getfield com/a/b/d/fj/a Lcom/a/b/d/fi;
aload 1
invokespecial com/a/b/d/fr/<init>(Lcom/a/b/d/fi;Ljava/lang/Object;)V
invokestatic com/a/b/d/fi/a(Ljava/util/Collection;Lcom/a/b/b/co;)Ljava/util/Collection;
astore 2
aload 2
astore 1
aload 2
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L1
aconst_null
areturn
.limit locals 3
.limit stack 5
.end method

.method public final a(Ljava/lang/Object;)Ljava/util/Collection;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/fj/a Lcom/a/b/d/fi;
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
invokeinterface com/a/b/d/vi/b()Ljava/util/Map; 0
aload 1
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/util/Collection
astore 3
aload 3
ifnonnull L0
aconst_null
areturn
L0:
new java/util/ArrayList
dup
invokespecial java/util/ArrayList/<init>()V
astore 2
aload 3
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
astore 3
L1:
aload 3
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L2
aload 3
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
astore 4
aload 0
getfield com/a/b/d/fj/a Lcom/a/b/d/fi;
aload 1
aload 4
invokestatic com/a/b/d/fi/a(Lcom/a/b/d/fi;Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L1
aload 3
invokeinterface java/util/Iterator/remove()V 0
aload 2
aload 4
invokeinterface java/util/List/add(Ljava/lang/Object;)Z 1
pop
goto L1
L2:
aload 2
invokeinterface java/util/List/isEmpty()Z 0
ifeq L3
aconst_null
areturn
L3:
aload 0
getfield com/a/b/d/fj/a Lcom/a/b/d/fi;
getfield com/a/b/d/fi/a Lcom/a/b/d/vi;
instanceof com/a/b/d/aac
ifeq L4
new java/util/LinkedHashSet
dup
aload 2
invokestatic com/a/b/d/cm/a(Ljava/lang/Iterable;)Ljava/util/Collection;
invokespecial java/util/LinkedHashSet/<init>(Ljava/util/Collection;)V
invokestatic java/util/Collections/unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;
areturn
L4:
aload 2
invokestatic java/util/Collections/unmodifiableList(Ljava/util/List;)Ljava/util/List;
areturn
.limit locals 5
.limit stack 3
.end method

.method final a()Ljava/util/Set;
new com/a/b/d/fl
dup
aload 0
invokespecial com/a/b/d/fl/<init>(Lcom/a/b/d/fj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method final c_()Ljava/util/Collection;
new com/a/b/d/fn
dup
aload 0
aload 0
invokespecial com/a/b/d/fn/<init>(Lcom/a/b/d/fj;Ljava/util/Map;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final clear()V
aload 0
getfield com/a/b/d/fj/a Lcom/a/b/d/fi;
invokevirtual com/a/b/d/fi/g()V
return
.limit locals 1
.limit stack 1
.end method

.method public final containsKey(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokespecial com/a/b/d/fj/b(Ljava/lang/Object;)Ljava/util/Collection;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method final e()Ljava/util/Set;
new com/a/b/d/fk
dup
aload 0
aload 0
invokespecial com/a/b/d/fk/<init>(Lcom/a/b/d/fj;Ljava/util/Map;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/b/d/fj/b(Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic remove(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokevirtual com/a/b/d/fj/a(Ljava/lang/Object;)Ljava/util/Collection;
areturn
.limit locals 2
.limit stack 2
.end method
