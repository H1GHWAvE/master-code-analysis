.bytecode 50.0
.class final synchronized com/a/b/d/afq
.super com/a/b/d/av

.field private final 'a' Ljava/util/NavigableMap;

.field private final 'b' Ljava/util/NavigableMap;

.field private final 'c' Lcom/a/b/d/yl;

.method <init>(Ljava/util/NavigableMap;)V
aload 0
aload 1
invokestatic com/a/b/d/yl/c()Lcom/a/b/d/yl;
invokespecial com/a/b/d/afq/<init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V
return
.limit locals 2
.limit stack 3
.end method

.method private <init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V
aload 0
invokespecial com/a/b/d/av/<init>()V
aload 0
aload 1
putfield com/a/b/d/afq/a Ljava/util/NavigableMap;
aload 0
new com/a/b/d/aft
dup
aload 1
invokespecial com/a/b/d/aft/<init>(Ljava/util/NavigableMap;)V
putfield com/a/b/d/afq/b Ljava/util/NavigableMap;
aload 0
aload 2
putfield com/a/b/d/afq/c Lcom/a/b/d/yl;
return
.limit locals 3
.limit stack 4
.end method

.method static synthetic a(Lcom/a/b/d/afq;)Lcom/a/b/d/yl;
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private a(Ljava/lang/Object;)Lcom/a/b/d/yl;
.annotation visible Ljavax/annotation/Nullable;
.end annotation
.catch java/lang/ClassCastException from L0 to L1 using L2
.catch java/lang/ClassCastException from L3 to L4 using L2
aload 1
instanceof com/a/b/d/dw
ifeq L5
L0:
aload 1
checkcast com/a/b/d/dw
astore 1
aload 0
aload 1
iconst_1
invokespecial com/a/b/d/afq/b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
invokeinterface java/util/NavigableMap/firstEntry()Ljava/util/Map$Entry; 0
astore 2
L1:
aload 2
ifnull L5
L3:
aload 2
invokeinterface java/util/Map$Entry/getKey()Ljava/lang/Object; 0
checkcast com/a/b/d/dw
aload 1
invokevirtual com/a/b/d/dw/equals(Ljava/lang/Object;)Z
ifeq L5
aload 2
invokeinterface java/util/Map$Entry/getValue()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
astore 1
L4:
aload 1
areturn
L2:
astore 1
aconst_null
areturn
L5:
aconst_null
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
aload 0
aload 1
iload 2
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
invokestatic com/a/b/d/yl/a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
invokespecial com/a/b/d/afq/a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
areturn
.limit locals 3
.limit stack 3
.end method

.method private a(Lcom/a/b/d/dw;ZLcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
aload 0
aload 1
iload 2
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
aload 3
iload 4
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
invokestatic com/a/b/d/yl/a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
invokespecial com/a/b/d/afq/a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
areturn
.limit locals 5
.limit stack 5
.end method

.method private a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
aload 1
invokevirtual com/a/b/d/yl/b(Lcom/a/b/d/yl;)Z
ifne L0
invokestatic com/a/b/d/lw/m()Lcom/a/b/d/lw;
areturn
L0:
aload 1
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/c(Lcom/a/b/d/yl;)Lcom/a/b/d/yl;
astore 1
new com/a/b/d/afq
dup
aload 0
getfield com/a/b/d/afq/a Ljava/util/NavigableMap;
aload 1
invokespecial com/a/b/d/afq/<init>(Ljava/util/NavigableMap;Lcom/a/b/d/yl;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method private b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
aload 0
aload 1
iload 2
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
invokestatic com/a/b/d/yl/b(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
invokespecial com/a/b/d/afq/a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
areturn
.limit locals 3
.limit stack 3
.end method

.method final a()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/d()Z
ifeq L0
aload 0
getfield com/a/b/d/afq/b Ljava/util/NavigableMap;
astore 2
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/c()Ljava/lang/Comparable;
astore 3
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/a()Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/b Lcom/a/b/d/ce;
if_acmpne L1
iconst_1
istore 1
L2:
aload 2
aload 3
iload 1
invokeinterface java/util/NavigableMap/tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokeinterface java/util/NavigableMap/values()Ljava/util/Collection; 0
astore 2
L3:
aload 2
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/j(Ljava/util/Iterator;)Lcom/a/b/d/yi;
astore 3
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
invokestatic com/a/b/d/dw/d()Lcom/a/b/d/dw;
invokevirtual com/a/b/d/yl/c(Ljava/lang/Comparable;)Z
ifeq L4
aload 3
invokeinterface com/a/b/d/yi/hasNext()Z 0
ifeq L5
aload 3
invokeinterface com/a/b/d/yi/a()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
invokestatic com/a/b/d/dw/d()Lcom/a/b/d/dw;
if_acmpeq L4
L5:
invokestatic com/a/b/d/dw/d()Lcom/a/b/d/dw;
astore 2
L6:
new com/a/b/d/afr
dup
aload 0
aload 2
aload 3
invokespecial com/a/b/d/afr/<init>(Lcom/a/b/d/afq;Lcom/a/b/d/dw;Lcom/a/b/d/yi;)V
areturn
L1:
iconst_0
istore 1
goto L2
L0:
aload 0
getfield com/a/b/d/afq/b Ljava/util/NavigableMap;
invokeinterface java/util/NavigableMap/values()Ljava/util/Collection; 0
astore 2
goto L3
L4:
aload 3
invokeinterface com/a/b/d/yi/hasNext()Z 0
ifeq L7
aload 3
invokeinterface com/a/b/d/yi/next()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
astore 2
goto L6
L7:
invokestatic com/a/b/d/nj/a()Lcom/a/b/d/agi;
areturn
.limit locals 4
.limit stack 5
.end method

.method final b()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/e()Z
ifeq L0
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/c()Ljava/lang/Comparable;
checkcast com/a/b/d/dw
astore 2
L1:
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
invokevirtual com/a/b/d/yl/e()Z
ifeq L2
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
invokevirtual com/a/b/d/dw/b()Lcom/a/b/d/ce;
getstatic com/a/b/d/ce/b Lcom/a/b/d/ce;
if_acmpne L2
iconst_1
istore 1
L3:
aload 0
getfield com/a/b/d/afq/b Ljava/util/NavigableMap;
aload 2
iload 1
invokeinterface java/util/NavigableMap/headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokeinterface java/util/NavigableMap/descendingMap()Ljava/util/NavigableMap; 0
invokeinterface java/util/NavigableMap/values()Ljava/util/Collection; 0
invokeinterface java/util/Collection/iterator()Ljava/util/Iterator; 0
invokestatic com/a/b/d/nj/j(Ljava/util/Iterator;)Lcom/a/b/d/yi;
astore 3
aload 3
invokeinterface com/a/b/d/yi/hasNext()Z 0
ifeq L4
aload 3
invokeinterface com/a/b/d/yi/a()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
invokestatic com/a/b/d/dw/e()Lcom/a/b/d/dw;
if_acmpne L5
aload 3
invokeinterface com/a/b/d/yi/next()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
getfield com/a/b/d/yl/b Lcom/a/b/d/dw;
astore 2
L6:
new com/a/b/d/afs
dup
aload 0
aload 2
invokestatic com/a/b/d/dw/e()Lcom/a/b/d/dw;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/dw
aload 3
invokespecial com/a/b/d/afs/<init>(Lcom/a/b/d/afq;Lcom/a/b/d/dw;Lcom/a/b/d/yi;)V
areturn
L0:
invokestatic com/a/b/d/dw/e()Lcom/a/b/d/dw;
astore 2
goto L1
L2:
iconst_0
istore 1
goto L3
L5:
aload 0
getfield com/a/b/d/afq/a Ljava/util/NavigableMap;
aload 3
invokeinterface com/a/b/d/yi/a()Ljava/lang/Object; 0
checkcast com/a/b/d/yl
getfield com/a/b/d/yl/c Lcom/a/b/d/dw;
invokeinterface java/util/NavigableMap/higherKey(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/dw
astore 2
goto L6
L4:
aload 0
getfield com/a/b/d/afq/c Lcom/a/b/d/yl;
invokestatic com/a/b/d/dw/d()Lcom/a/b/d/dw;
invokevirtual com/a/b/d/yl/c(Ljava/lang/Comparable;)Z
ifeq L7
aload 0
getfield com/a/b/d/afq/a Ljava/util/NavigableMap;
invokestatic com/a/b/d/dw/d()Lcom/a/b/d/dw;
invokeinterface java/util/NavigableMap/containsKey(Ljava/lang/Object;)Z 1
ifeq L8
L7:
invokestatic com/a/b/d/nj/a()Lcom/a/b/d/agi;
areturn
L8:
aload 0
getfield com/a/b/d/afq/a Ljava/util/NavigableMap;
invokestatic com/a/b/d/dw/d()Lcom/a/b/d/dw;
invokeinterface java/util/NavigableMap/higherKey(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast com/a/b/d/dw
astore 2
goto L6
.limit locals 4
.limit stack 5
.end method

.method public final comparator()Ljava/util/Comparator;
invokestatic com/a/b/d/yd/d()Lcom/a/b/d/yd;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final containsKey(Ljava/lang/Object;)Z
aload 0
aload 1
invokespecial com/a/b/d/afq/a(Ljava/lang/Object;)Lcom/a/b/d/yl;
ifnull L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
aload 1
invokespecial com/a/b/d/afq/a(Ljava/lang/Object;)Lcom/a/b/d/yl;
areturn
.limit locals 2
.limit stack 2
.end method

.method public final synthetic headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
aload 0
aload 1
checkcast com/a/b/d/dw
iload 2
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
invokestatic com/a/b/d/yl/a(Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
invokespecial com/a/b/d/afq/a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
areturn
.limit locals 3
.limit stack 3
.end method

.method public final size()I
aload 0
invokevirtual com/a/b/d/afq/a()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final synthetic subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;
aload 1
checkcast com/a/b/d/dw
astore 1
aload 3
checkcast com/a/b/d/dw
astore 3
aload 0
aload 1
iload 2
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
aload 3
iload 4
invokestatic com/a/b/d/ce/a(Z)Lcom/a/b/d/ce;
invokestatic com/a/b/d/yl/a(Ljava/lang/Comparable;Lcom/a/b/d/ce;Ljava/lang/Comparable;Lcom/a/b/d/ce;)Lcom/a/b/d/yl;
invokespecial com/a/b/d/afq/a(Lcom/a/b/d/yl;)Ljava/util/NavigableMap;
areturn
.limit locals 5
.limit stack 5
.end method

.method public final synthetic tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap;
aload 0
aload 1
checkcast com/a/b/d/dw
iload 2
invokespecial com/a/b/d/afq/b(Lcom/a/b/d/dw;Z)Ljava/util/NavigableMap;
areturn
.limit locals 3
.limit stack 3
.end method
