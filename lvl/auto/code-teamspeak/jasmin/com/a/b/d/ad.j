.bytecode 50.0
.class synchronized com/a/b/d/ad
.super com/a/b/d/ab
.implements java/util/List

.field final synthetic 'g' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)V
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 4 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
putfield com/a/b/d/ad/g Lcom/a/b/d/n;
aload 0
aload 1
aload 2
aload 3
aload 4
invokespecial com/a/b/d/ab/<init>(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/Collection;Lcom/a/b/d/ab;)V
return
.limit locals 5
.limit stack 5
.end method

.method private d()Ljava/util/List;
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
areturn
.limit locals 1
.limit stack 1
.end method

.method public add(ILjava/lang/Object;)V
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/isEmpty()Z 0
istore 3
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 1
aload 2
invokeinterface java/util/List/add(ILjava/lang/Object;)V 2
aload 0
getfield com/a/b/d/ad/g Lcom/a/b/d/n;
invokestatic com/a/b/d/n/c(Lcom/a/b/d/n;)I
pop
iload 3
ifeq L0
aload 0
invokevirtual com/a/b/d/ad/c()V
L0:
return
.limit locals 4
.limit stack 3
.end method

.method public addAll(ILjava/util/Collection;)Z
aload 2
invokeinterface java/util/Collection/isEmpty()Z 0
ifeq L0
iconst_0
istore 4
L1:
iload 4
ireturn
L0:
aload 0
invokevirtual com/a/b/d/ad/size()I
istore 3
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 1
aload 2
invokeinterface java/util/List/addAll(ILjava/util/Collection;)Z 2
istore 5
iload 5
istore 4
iload 5
ifeq L1
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
invokeinterface java/util/Collection/size()I 0
istore 1
aload 0
getfield com/a/b/d/ad/g Lcom/a/b/d/n;
iload 1
iload 3
isub
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;I)I
pop
iload 5
istore 4
iload 3
ifne L1
aload 0
invokevirtual com/a/b/d/ad/c()V
iload 5
ireturn
.limit locals 6
.limit stack 3
.end method

.method public get(I)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public indexOf(Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
aload 1
invokeinterface java/util/List/indexOf(Ljava/lang/Object;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
aload 1
invokeinterface java/util/List/lastIndexOf(Ljava/lang/Object;)I 1
ireturn
.limit locals 2
.limit stack 2
.end method

.method public listIterator()Ljava/util/ListIterator;
aload 0
invokevirtual com/a/b/d/ad/a()V
new com/a/b/d/ae
dup
aload 0
invokespecial com/a/b/d/ae/<init>(Lcom/a/b/d/ad;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public listIterator(I)Ljava/util/ListIterator;
aload 0
invokevirtual com/a/b/d/ad/a()V
new com/a/b/d/ae
dup
aload 0
iload 1
invokespecial com/a/b/d/ae/<init>(Lcom/a/b/d/ad;I)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public remove(I)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 1
invokeinterface java/util/List/remove(I)Ljava/lang/Object; 1
astore 2
aload 0
getfield com/a/b/d/ad/g Lcom/a/b/d/n;
invokestatic com/a/b/d/n/b(Lcom/a/b/d/n;)I
pop
aload 0
invokevirtual com/a/b/d/ad/b()V
aload 2
areturn
.limit locals 3
.limit stack 2
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 1
aload 2
invokeinterface java/util/List/set(ILjava/lang/Object;)Ljava/lang/Object; 2
areturn
.limit locals 3
.limit stack 3
.end method

.method public subList(II)Ljava/util/List;
aload 0
invokevirtual com/a/b/d/ad/a()V
aload 0
getfield com/a/b/d/ad/g Lcom/a/b/d/n;
astore 4
aload 0
getfield com/a/b/d/ab/b Ljava/lang/Object;
astore 5
aload 0
getfield com/a/b/d/ab/c Ljava/util/Collection;
checkcast java/util/List
iload 1
iload 2
invokeinterface java/util/List/subList(II)Ljava/util/List; 2
astore 6
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
ifnonnull L0
aload 0
astore 3
L1:
aload 4
aload 5
aload 6
aload 3
invokestatic com/a/b/d/n/a(Lcom/a/b/d/n;Ljava/lang/Object;Ljava/util/List;Lcom/a/b/d/ab;)Ljava/util/List;
areturn
L0:
aload 0
getfield com/a/b/d/ab/d Lcom/a/b/d/ab;
astore 3
goto L1
.limit locals 7
.limit stack 4
.end method
