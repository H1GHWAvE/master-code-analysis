.bytecode 50.0
.class final synchronized com/a/b/d/cy
.super com/a/b/d/yd
.implements java/io/Serializable
.annotation invisible Lcom/a/b/a/b;
a Z = 1
.end annotation

.field private static final 'b' J = 0L


.field final 'a' Lcom/a/b/d/jl;

.method <init>(Ljava/lang/Iterable;)V
aload 0
invokespecial com/a/b/d/yd/<init>()V
aload 0
aload 1
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
putfield com/a/b/d/cy/a Lcom/a/b/d/jl;
return
.limit locals 2
.limit stack 2
.end method

.method <init>(Ljava/util/Comparator;Ljava/util/Comparator;)V
aload 0
invokespecial com/a/b/d/yd/<init>()V
aload 0
aload 1
aload 2
invokestatic com/a/b/d/jl/a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/d/jl;
putfield com/a/b/d/cy/a Lcom/a/b/d/jl;
return
.limit locals 3
.limit stack 3
.end method

.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
aload 0
getfield com/a/b/d/cy/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
istore 4
iconst_0
istore 3
L0:
iload 3
iload 4
if_icmpge L1
aload 0
getfield com/a/b/d/cy/a Lcom/a/b/d/jl;
iload 3
invokevirtual com/a/b/d/jl/get(I)Ljava/lang/Object;
checkcast java/util/Comparator
aload 1
aload 2
invokeinterface java/util/Comparator/compare(Ljava/lang/Object;Ljava/lang/Object;)I 2
istore 5
iload 5
ifeq L2
iload 5
ireturn
L2:
iload 3
iconst_1
iadd
istore 3
goto L0
L1:
iconst_0
ireturn
.limit locals 6
.limit stack 3
.end method

.method public final equals(Ljava/lang/Object;)Z
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/d/cy
ifeq L1
aload 1
checkcast com/a/b/d/cy
astore 1
aload 0
getfield com/a/b/d/cy/a Lcom/a/b/d/jl;
aload 1
getfield com/a/b/d/cy/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/equals(Ljava/lang/Object;)Z
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/d/cy/a Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/d/cy/a Lcom/a/b/d/jl;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
bipush 19
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
ldc "Ordering.compound("
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc ")"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
areturn
.limit locals 2
.limit stack 4
.end method
