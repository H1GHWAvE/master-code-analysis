.bytecode 50.0
.class public abstract interface com/a/b/d/adv
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract a()Ljava/util/Set;
.end method

.method public abstract a(Lcom/a/b/d/adv;)V
.end method

.method public abstract a(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract a(Ljava/lang/Object;Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract b()Ljava/util/Set;
.end method

.method public abstract b(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.annotation visibleparam 2 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract c()Z
.end method

.method public abstract c(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract d(Ljava/lang/Object;)Ljava/util/Map;
.end method

.method public abstract d()V
.end method

.method public abstract e(Ljava/lang/Object;)Ljava/util/Map;
.end method

.method public abstract e()Ljava/util/Set;
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract h()Ljava/util/Collection;
.end method

.method public abstract hashCode()I
.end method

.method public abstract k()I
.end method

.method public abstract l()Ljava/util/Map;
.end method

.method public abstract m()Ljava/util/Map;
.end method
