.bytecode 50.0
.class synchronized abstract com/a/b/d/as
.super java/util/AbstractCollection
.implements com/a/b/d/xc
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private transient 'a' Ljava/util/Set;

.field private transient 'b' Ljava/util/Set;

.method <init>()V
aload 0
invokespecial java/util/AbstractCollection/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method public a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
invokevirtual com/a/b/d/as/a()Ljava/util/Set;
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 2
L0:
aload 2
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L1
aload 2
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast com/a/b/d/xd
astore 3
aload 3
invokeinterface com/a/b/d/xd/a()Ljava/lang/Object; 0
aload 1
invokestatic com/a/b/b/ce/a(Ljava/lang/Object;Ljava/lang/Object;)Z
ifeq L0
aload 3
invokeinterface com/a/b/d/xd/b()I 0
ireturn
L1:
iconst_0
ireturn
.limit locals 4
.limit stack 2
.end method

.method public a(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method public a()Ljava/util/Set;
aload 0
getfield com/a/b/d/as/b Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/as/f()Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/as/b Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public a(Ljava/lang/Object;II)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
iload 2
iload 3
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;II)Z
ireturn
.limit locals 4
.limit stack 4
.end method

.method public add(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/as/a(Ljava/lang/Object;I)I
pop
iconst_1
ireturn
.limit locals 2
.limit stack 3
.end method

.method public addAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public b(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
new java/lang/UnsupportedOperationException
dup
invokespecial java/lang/UnsupportedOperationException/<init>()V
athrow
.limit locals 3
.limit stack 2
.end method

.method abstract b()Ljava/util/Iterator;
.end method

.method abstract c()I
.end method

.method public c(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
iload 2
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;I)I
ireturn
.limit locals 3
.limit stack 3
.end method

.method public clear()V
aload 0
invokevirtual com/a/b/d/as/b()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/i(Ljava/util/Iterator;)V
return
.limit locals 1
.limit stack 1
.end method

.method public contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokevirtual com/a/b/d/as/a(Ljava/lang/Object;)I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method e()Ljava/util/Set;
new com/a/b/d/at
dup
aload 0
invokespecial com/a/b/d/at/<init>(Lcom/a/b/d/as;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
invokestatic com/a/b/d/xe/a(Lcom/a/b/d/xc;Ljava/lang/Object;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method f()Ljava/util/Set;
new com/a/b/d/au
dup
aload 0
invokespecial com/a/b/d/au/<init>(Lcom/a/b/d/as;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method public hashCode()I
aload 0
invokevirtual com/a/b/d/as/a()Ljava/util/Set;
invokeinterface java/util/Set/hashCode()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public isEmpty()Z
aload 0
invokevirtual com/a/b/d/as/a()Ljava/util/Set;
invokeinterface java/util/Set/isEmpty()Z 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public iterator()Ljava/util/Iterator;
aload 0
invokestatic com/a/b/d/xe/b(Lcom/a/b/d/xc;)Ljava/util/Iterator;
areturn
.limit locals 1
.limit stack 1
.end method

.method public n_()Ljava/util/Set;
aload 0
getfield com/a/b/d/as/a Ljava/util/Set;
astore 2
aload 2
astore 1
aload 2
ifnonnull L0
aload 0
invokevirtual com/a/b/d/as/e()Ljava/util/Set;
astore 1
aload 0
aload 1
putfield com/a/b/d/as/a Ljava/util/Set;
L0:
aload 1
areturn
.limit locals 3
.limit stack 2
.end method

.method public remove(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/as/b(Ljava/lang/Object;I)I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 3
.end method

.method public removeAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/xe/b(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public retainAll(Ljava/util/Collection;)Z
aload 0
aload 1
invokestatic com/a/b/d/xe/c(Lcom/a/b/d/xc;Ljava/util/Collection;)Z
ireturn
.limit locals 2
.limit stack 2
.end method

.method public size()I
aload 0
invokestatic com/a/b/d/xe/c(Lcom/a/b/d/xc;)I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public toString()Ljava/lang/String;
aload 0
invokevirtual com/a/b/d/as/a()Ljava/util/Set;
invokevirtual java/lang/Object/toString()Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
