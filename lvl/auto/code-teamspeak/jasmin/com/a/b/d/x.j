.bytecode 50.0
.class final synchronized com/a/b/d/x
.super com/a/b/d/aa
.implements java/util/NavigableSet
.annotation invisible Lcom/a/b/a/c;
a s = "NavigableSet"
.end annotation

.field final synthetic 'b' Lcom/a/b/d/n;

.method <init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
aload 0
aload 1
putfield com/a/b/d/x/b Lcom/a/b/d/n;
aload 0
aload 1
aload 2
invokespecial com/a/b/d/aa/<init>(Lcom/a/b/d/n;Ljava/util/SortedMap;)V
return
.limit locals 3
.limit stack 3
.end method

.method private a(Ljava/lang/Object;)Ljava/util/NavigableSet;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/x/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/NavigableSet;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/x/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 5
.end method

.method private b(Ljava/lang/Object;)Ljava/util/NavigableSet;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/x/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method private c()Ljava/util/NavigableMap;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method final volatile synthetic a()Ljava/util/SortedMap;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
areturn
.limit locals 1
.limit stack 1
.end method

.method public final ceiling(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/ceilingKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final descendingIterator()Ljava/util/Iterator;
aload 0
invokevirtual com/a/b/d/x/descendingSet()Ljava/util/NavigableSet;
invokeinterface java/util/NavigableSet/iterator()Ljava/util/Iterator; 0
areturn
.limit locals 1
.limit stack 1
.end method

.method public final descendingSet()Ljava/util/NavigableSet;
new com/a/b/d/x
dup
aload 0
getfield com/a/b/d/x/b Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
invokeinterface java/util/NavigableMap/descendingMap()Ljava/util/NavigableMap; 0
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 1
.limit stack 4
.end method

.method public final floor(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/floorKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
new com/a/b/d/x
dup
aload 0
getfield com/a/b/d/x/b Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
iload 2
invokeinterface java/util/NavigableMap/headMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public final volatile synthetic headSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_0
invokevirtual com/a/b/d/x/headSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method

.method public final higher(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/higherKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final lower(Ljava/lang/Object;)Ljava/lang/Object;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
invokeinterface java/util/NavigableMap/lowerKey(Ljava/lang/Object;)Ljava/lang/Object; 1
areturn
.limit locals 2
.limit stack 2
.end method

.method public final pollFirst()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/x/iterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/h(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final pollLast()Ljava/lang/Object;
aload 0
invokevirtual com/a/b/d/x/descendingIterator()Ljava/util/Iterator;
invokestatic com/a/b/d/nj/h(Ljava/util/Iterator;)Ljava/lang/Object;
areturn
.limit locals 1
.limit stack 1
.end method

.method public final subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
new com/a/b/d/x
dup
aload 0
getfield com/a/b/d/x/b Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
iload 2
aload 3
iload 4
invokeinterface java/util/NavigableMap/subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap; 4
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 5
.limit stack 8
.end method

.method public final volatile synthetic subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
aload 2
iconst_0
invokevirtual com/a/b/d/x/subSet(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 3
.limit stack 5
.end method

.method public final tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
new com/a/b/d/x
dup
aload 0
getfield com/a/b/d/x/b Lcom/a/b/d/n;
aload 0
invokespecial com/a/b/d/aa/a()Ljava/util/SortedMap;
checkcast java/util/NavigableMap
aload 1
iload 2
invokeinterface java/util/NavigableMap/tailMap(Ljava/lang/Object;Z)Ljava/util/NavigableMap; 2
invokespecial com/a/b/d/x/<init>(Lcom/a/b/d/n;Ljava/util/NavigableMap;)V
areturn
.limit locals 3
.limit stack 6
.end method

.method public final volatile synthetic tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;
aload 0
aload 1
iconst_1
invokevirtual com/a/b/d/x/tailSet(Ljava/lang/Object;Z)Ljava/util/NavigableSet;
areturn
.limit locals 2
.limit stack 3
.end method
