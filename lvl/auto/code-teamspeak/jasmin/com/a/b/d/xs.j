.bytecode 50.0
.class final synchronized com/a/b/d/xs
.super com/a/b/d/as

.field final 'a' Lcom/a/b/d/xc;

.field final 'b' Lcom/a/b/b/co;

.method <init>(Lcom/a/b/d/xc;Lcom/a/b/b/co;)V
aload 0
invokespecial com/a/b/d/as/<init>()V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/d/xc
putfield com/a/b/d/xs/a Lcom/a/b/d/xc;
aload 0
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/co
putfield com/a/b/d/xs/b Lcom/a/b/b/co;
return
.limit locals 3
.limit stack 2
.end method

.method private g()Lcom/a/b/d/agi;
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/iterator()Ljava/util/Iterator; 0
aload 0
getfield com/a/b/d/xs/b Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
aload 1
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;)I 1
istore 2
iload 2
ifle L0
aload 0
getfield com/a/b/d/xs/b Lcom/a/b/b/co;
aload 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ifeq L1
iload 2
ireturn
L1:
iconst_0
ireturn
L0:
iconst_0
ireturn
.limit locals 3
.limit stack 2
.end method

.method public final a(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 0
getfield com/a/b/d/xs/b Lcom/a/b/b/co;
aload 1
invokeinterface com/a/b/b/co/a(Ljava/lang/Object;)Z 1
ldc "Element %s does not match predicate %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
dup
iconst_1
aload 0
getfield com/a/b/d/xs/b Lcom/a/b/b/co;
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
aload 1
iload 2
invokeinterface com/a/b/d/xc/a(Ljava/lang/Object;I)I 2
ireturn
.limit locals 3
.limit stack 6
.end method

.method public final b(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
iload 2
ldc "occurrences"
invokestatic com/a/b/d/cl/a(ILjava/lang/String;)I
pop
iload 2
ifne L0
aload 0
aload 1
invokevirtual com/a/b/d/xs/a(Ljava/lang/Object;)I
ireturn
L0:
aload 0
aload 1
invokevirtual com/a/b/d/xs/contains(Ljava/lang/Object;)Z
ifeq L1
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
aload 1
iload 2
invokeinterface com/a/b/d/xc/b(Ljava/lang/Object;I)I 2
ireturn
L1:
iconst_0
ireturn
.limit locals 3
.limit stack 3
.end method

.method final b()Ljava/util/Iterator;
new java/lang/AssertionError
dup
ldc "should never be called"
invokespecial java/lang/AssertionError/<init>(Ljava/lang/Object;)V
athrow
.limit locals 1
.limit stack 3
.end method

.method final c()I
aload 0
invokevirtual com/a/b/d/xs/n_()Ljava/util/Set;
invokeinterface java/util/Set/size()I 0
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final clear()V
aload 0
invokevirtual com/a/b/d/xs/n_()Ljava/util/Set;
invokeinterface java/util/Set/clear()V 0
return
.limit locals 1
.limit stack 1
.end method

.method final e()Ljava/util/Set;
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/n_()Ljava/util/Set; 0
aload 0
getfield com/a/b/d/xs/b Lcom/a/b/b/co;
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 2
.end method

.method final f()Ljava/util/Set;
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/a()Ljava/util/Set; 0
new com/a/b/d/xt
dup
aload 0
invokespecial com/a/b/d/xt/<init>(Lcom/a/b/d/xs;)V
invokestatic com/a/b/d/aad/a(Ljava/util/Set;Lcom/a/b/b/co;)Ljava/util/Set;
areturn
.limit locals 1
.limit stack 4
.end method

.method public final synthetic iterator()Ljava/util/Iterator;
aload 0
getfield com/a/b/d/xs/a Lcom/a/b/d/xc;
invokeinterface com/a/b/d/xc/iterator()Ljava/util/Iterator; 0
aload 0
getfield com/a/b/d/xs/b Lcom/a/b/b/co;
invokestatic com/a/b/d/nj/b(Ljava/util/Iterator;Lcom/a/b/b/co;)Lcom/a/b/d/agi;
areturn
.limit locals 1
.limit stack 2
.end method
