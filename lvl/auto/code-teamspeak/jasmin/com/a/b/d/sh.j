.bytecode 50.0
.class public synchronized abstract enum com/a/b/d/sh
.super java/lang/Enum

.field public static final enum 'a' Lcom/a/b/d/sh;

.field public static final enum 'b' Lcom/a/b/d/sh;

.field public static final enum 'c' Lcom/a/b/d/sh;

.field private static final synthetic 'd' [Lcom/a/b/d/sh;

.method static <clinit>()V
new com/a/b/d/si
dup
ldc "STRONG"
invokespecial com/a/b/d/si/<init>(Ljava/lang/String;)V
putstatic com/a/b/d/sh/a Lcom/a/b/d/sh;
new com/a/b/d/sj
dup
ldc "SOFT"
invokespecial com/a/b/d/sj/<init>(Ljava/lang/String;)V
putstatic com/a/b/d/sh/b Lcom/a/b/d/sh;
new com/a/b/d/sk
dup
ldc "WEAK"
invokespecial com/a/b/d/sk/<init>(Ljava/lang/String;)V
putstatic com/a/b/d/sh/c Lcom/a/b/d/sh;
iconst_3
anewarray com/a/b/d/sh
dup
iconst_0
getstatic com/a/b/d/sh/a Lcom/a/b/d/sh;
aastore
dup
iconst_1
getstatic com/a/b/d/sh/b Lcom/a/b/d/sh;
aastore
dup
iconst_2
getstatic com/a/b/d/sh/c Lcom/a/b/d/sh;
aastore
putstatic com/a/b/d/sh/d [Lcom/a/b/d/sh;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/d/sh/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/d/sh;
ldc com/a/b/d/sh
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/d/sh
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/d/sh;
getstatic com/a/b/d/sh/d [Lcom/a/b/d/sh;
invokevirtual [Lcom/a/b/d/sh;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/d/sh;
areturn
.limit locals 0
.limit stack 1
.end method

.method abstract a()Lcom/a/b/b/au;
.end method

.method abstract a(Lcom/a/b/d/sa;Lcom/a/b/d/rz;Ljava/lang/Object;)Lcom/a/b/d/sr;
.end method
