.bytecode 50.0
.class public abstract interface com/a/b/d/xc
.super java/lang/Object
.implements java/util/Collection
.annotation invisible Lcom/a/b/a/b;
.end annotation

.method public abstract a(Ljava/lang/Object;)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract a(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract a()Ljava/util/Set;
.end method

.method public abstract a(Ljava/lang/Object;II)Z
.end method

.method public abstract add(Ljava/lang/Object;)Z
.end method

.method public abstract b(Ljava/lang/Object;I)I
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract c(Ljava/lang/Object;I)I
.end method

.method public abstract contains(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract containsAll(Ljava/util/Collection;)Z
.end method

.method public abstract equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract hashCode()I
.end method

.method public abstract iterator()Ljava/util/Iterator;
.end method

.method public abstract n_()Ljava/util/Set;
.end method

.method public abstract remove(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
.end method

.method public abstract removeAll(Ljava/util/Collection;)Z
.end method

.method public abstract retainAll(Ljava/util/Collection;)Z
.end method

.method public abstract toString()Ljava/lang/String;
.end method
