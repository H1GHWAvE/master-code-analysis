.bytecode 50.0
.class public synchronized abstract com/a/b/c/ab
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.method public <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method private static a(Lcom/a/b/b/bj;)Lcom/a/b/c/ab;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/c/ae
dup
aload 0
invokespecial com/a/b/c/ae/<init>(Lcom/a/b/b/bj;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/b/dz;)Lcom/a/b/c/ab;
.annotation invisible Lcom/a/b/a/a;
.end annotation
new com/a/b/c/ag
dup
aload 0
invokespecial com/a/b/c/ag/<init>(Lcom/a/b/b/dz;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Lcom/a/b/c/ab;Ljava/util/concurrent/Executor;)Lcom/a/b/c/ab;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "Executor + Futures"
.end annotation
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
new com/a/b/c/ac
dup
aload 0
aload 1
invokespecial com/a/b/c/ac/<init>(Lcom/a/b/c/ab;Ljava/util/concurrent/Executor;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/a/b/n/a/dp;
.annotation invisible Lcom/a/b/a/c;
a s = "Futures"
.end annotation
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 2
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
aload 1
invokevirtual com/a/b/c/ab/a(Ljava/lang/Object;)Ljava/lang/Object;
invokestatic com/a/b/n/a/ci/a(Ljava/lang/Object;)Lcom/a/b/n/a/dp;
areturn
.limit locals 3
.limit stack 2
.end method

.method public abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public a(Ljava/lang/Iterable;)Ljava/util/Map;
new com/a/b/c/ah
dup
invokespecial com/a/b/c/ah/<init>()V
athrow
.limit locals 2
.limit stack 2
.end method
