.bytecode 50.0
.class public final synchronized com/a/b/c/f
.super java/lang/Object
.annotation invisible Lcom/a/b/a/b;
b Z = 1
.end annotation

.field static final 'a' Lcom/a/b/b/dz;

.field static final 'b' Lcom/a/b/c/ai;

.field static final 'c' Lcom/a/b/b/dz;

.field static final 'd' Lcom/a/b/b/ej;

.field static final 'e' I = -1


.field private static final 'v' I = 16


.field private static final 'w' I = 4


.field private static final 'x' I = 0


.field private static final 'y' I = 0


.field private static final 'z' Ljava/util/logging/Logger;

.field 'f' Z

.field 'g' I

.field 'h' I

.field 'i' J

.field 'j' J

.field 'k' Lcom/a/b/c/do;

.field 'l' Lcom/a/b/c/bw;

.field 'm' Lcom/a/b/c/bw;

.field 'n' J

.field 'o' J

.field 'p' J

.field 'q' Lcom/a/b/b/au;

.field 'r' Lcom/a/b/b/au;

.field 's' Lcom/a/b/c/dg;

.field 't' Lcom/a/b/b/ej;

.field 'u' Lcom/a/b/b/dz;

.method static <clinit>()V
new com/a/b/b/eg
dup
new com/a/b/c/g
dup
invokespecial com/a/b/c/g/<init>()V
invokespecial com/a/b/b/eg/<init>(Ljava/lang/Object;)V
putstatic com/a/b/c/f/a Lcom/a/b/b/dz;
new com/a/b/c/ai
dup
lconst_0
lconst_0
lconst_0
lconst_0
lconst_0
lconst_0
invokespecial com/a/b/c/ai/<init>(JJJJJJ)V
putstatic com/a/b/c/f/b Lcom/a/b/c/ai;
new com/a/b/c/h
dup
invokespecial com/a/b/c/h/<init>()V
putstatic com/a/b/c/f/c Lcom/a/b/b/dz;
new com/a/b/c/i
dup
invokespecial com/a/b/c/i/<init>()V
putstatic com/a/b/c/f/d Lcom/a/b/b/ej;
ldc com/a/b/c/f
invokevirtual java/lang/Class/getName()Ljava/lang/String;
invokestatic java/util/logging/Logger/getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;
putstatic com/a/b/c/f/z Ljava/util/logging/Logger;
return
.limit locals 0
.limit stack 14
.end method

.method <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
iconst_1
putfield com/a/b/c/f/f Z
aload 0
iconst_m1
putfield com/a/b/c/f/g I
aload 0
iconst_m1
putfield com/a/b/c/f/h I
aload 0
ldc2_w -1L
putfield com/a/b/c/f/i J
aload 0
ldc2_w -1L
putfield com/a/b/c/f/j J
aload 0
ldc2_w -1L
putfield com/a/b/c/f/n J
aload 0
ldc2_w -1L
putfield com/a/b/c/f/o J
aload 0
ldc2_w -1L
putfield com/a/b/c/f/p J
aload 0
getstatic com/a/b/c/f/a Lcom/a/b/b/dz;
putfield com/a/b/c/f/u Lcom/a/b/b/dz;
return
.limit locals 1
.limit stack 3
.end method

.method private a(Z)Lcom/a/b/b/ej;
aload 0
getfield com/a/b/c/f/t Lcom/a/b/b/ej;
ifnull L0
aload 0
getfield com/a/b/c/f/t Lcom/a/b/b/ej;
areturn
L0:
iload 1
ifeq L1
invokestatic com/a/b/b/ej/b()Lcom/a/b/b/ej;
areturn
L1:
getstatic com/a/b/c/f/d Lcom/a/b/b/ej;
areturn
.limit locals 2
.limit stack 1
.end method

.method public static a()Lcom/a/b/c/f;
new com/a/b/c/f
dup
invokespecial com/a/b/c/f/<init>()V
areturn
.limit locals 0
.limit stack 2
.end method

.method private a(Lcom/a/b/b/au;)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
aload 0
getfield com/a/b/c/f/q Lcom/a/b/b/au;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
ldc "key equivalence was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/q Lcom/a/b/b/au;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
putfield com/a/b/c/f/q Lcom/a/b/b/au;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method private a(Lcom/a/b/b/ej;)Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/f/t Lcom/a/b/b/ej;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/b(Z)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/ej
putfield com/a/b/c/f/t Lcom/a/b/b/ej;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private a(Lcom/a/b/c/dg;)Lcom/a/b/c/f;
.annotation visible Ljavax/annotation/CheckReturnValue;
.end annotation
aload 0
getfield com/a/b/c/f/s Lcom/a/b/c/dg;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/b(Z)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/dg
putfield com/a/b/c/f/s Lcom/a/b/c/dg;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 2
.end method

.method private a(Lcom/a/b/c/do;)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
aload 0
getfield com/a/b/c/f/k Lcom/a/b/c/do;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/b(Z)V
aload 0
getfield com/a/b/c/f/f Z
ifeq L2
aload 0
getfield com/a/b/c/f/i J
ldc2_w -1L
lcmp
ifne L3
iconst_1
istore 2
L4:
iload 2
ldc "weigher can not be combined with maximum size"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/i J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
L2:
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/do
putfield com/a/b/c/f/k Lcom/a/b/c/do;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
L3:
iconst_0
istore 2
goto L4
.limit locals 3
.limit stack 7
.end method

.method private static a(Lcom/a/b/c/l;)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
new com/a/b/c/f
dup
invokespecial com/a/b/c/f/<init>()V
astore 5
aload 0
getfield com/a/b/c/l/a Ljava/lang/Integer;
ifnull L0
aload 0
getfield com/a/b/c/l/a Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 1
aload 5
getfield com/a/b/c/f/g I
iconst_m1
if_icmpne L1
iconst_1
istore 2
L2:
iload 2
ldc "initial capacity was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 5
getfield com/a/b/c/f/g I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
iflt L3
iconst_1
istore 2
L4:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 5
iload 1
putfield com/a/b/c/f/g I
L0:
aload 0
getfield com/a/b/c/l/b Ljava/lang/Long;
ifnull L5
aload 5
aload 0
getfield com/a/b/c/l/b Ljava/lang/Long;
invokevirtual java/lang/Long/longValue()J
invokevirtual com/a/b/c/f/a(J)Lcom/a/b/c/f;
pop
L5:
aload 0
getfield com/a/b/c/l/c Ljava/lang/Long;
ifnull L6
aload 5
aload 0
getfield com/a/b/c/l/c Ljava/lang/Long;
invokevirtual java/lang/Long/longValue()J
invokevirtual com/a/b/c/f/b(J)Lcom/a/b/c/f;
pop
L6:
aload 0
getfield com/a/b/c/l/d Ljava/lang/Integer;
ifnull L7
aload 5
aload 0
getfield com/a/b/c/l/d Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
invokevirtual com/a/b/c/f/a(I)Lcom/a/b/c/f;
pop
L7:
aload 0
getfield com/a/b/c/l/e Lcom/a/b/c/bw;
ifnull L8
getstatic com/a/b/c/m/a [I
aload 0
getfield com/a/b/c/l/e Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/ordinal()I
iaload
tableswitch 1
L9
default : L10
L10:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 2
goto L4
L9:
aload 5
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
pop
L8:
aload 0
getfield com/a/b/c/l/f Lcom/a/b/c/bw;
ifnull L11
getstatic com/a/b/c/m/a [I
aload 0
getfield com/a/b/c/l/f Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/ordinal()I
iaload
tableswitch 1
L12
L13
default : L14
L14:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L13:
aload 5
getstatic com/a/b/c/bw/b Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
pop
L11:
aload 0
getfield com/a/b/c/l/g Ljava/lang/Boolean;
ifnull L15
aload 0
getfield com/a/b/c/l/g Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L15
aload 5
getstatic com/a/b/c/f/c Lcom/a/b/b/dz;
putfield com/a/b/c/f/u Lcom/a/b/b/dz;
L15:
aload 0
getfield com/a/b/c/l/i Ljava/util/concurrent/TimeUnit;
ifnull L16
aload 5
aload 0
getfield com/a/b/c/l/h J
aload 0
getfield com/a/b/c/l/i Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/c/f/a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
pop
L16:
aload 0
getfield com/a/b/c/l/k Ljava/util/concurrent/TimeUnit;
ifnull L17
aload 5
aload 0
getfield com/a/b/c/l/j J
aload 0
getfield com/a/b/c/l/k Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/c/f/b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
pop
L17:
aload 0
getfield com/a/b/c/l/m Ljava/util/concurrent/TimeUnit;
ifnull L18
aload 0
getfield com/a/b/c/l/l J
lstore 3
aload 0
getfield com/a/b/c/l/m Ljava/util/concurrent/TimeUnit;
astore 0
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 5
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L19
iconst_1
istore 2
L20:
iload 2
ldc "refresh was already set to %s ns"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 5
getfield com/a/b/c/f/p J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 3
lconst_0
lcmp
ifle L21
iconst_1
istore 2
L22:
iload 2
ldc "duration must be positive: %s %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 0
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 5
aload 0
lload 3
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
putfield com/a/b/c/f/p J
L18:
aload 5
iconst_0
putfield com/a/b/c/f/f Z
aload 5
areturn
L12:
aload 5
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
pop
goto L11
L19:
iconst_0
istore 2
goto L20
L21:
iconst_0
istore 2
goto L22
.limit locals 6
.limit stack 7
.end method

.method private static a(Ljava/lang/String;)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
aload 0
invokestatic com/a/b/c/l/a(Ljava/lang/String;)Lcom/a/b/c/l;
astore 5
new com/a/b/c/f
dup
invokespecial com/a/b/c/f/<init>()V
astore 0
aload 5
getfield com/a/b/c/l/a Ljava/lang/Integer;
ifnull L0
aload 5
getfield com/a/b/c/l/a Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
istore 1
aload 0
getfield com/a/b/c/f/g I
iconst_m1
if_icmpne L1
iconst_1
istore 2
L2:
iload 2
ldc "initial capacity was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/g I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
iflt L3
iconst_1
istore 2
L4:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iload 1
putfield com/a/b/c/f/g I
L0:
aload 5
getfield com/a/b/c/l/b Ljava/lang/Long;
ifnull L5
aload 0
aload 5
getfield com/a/b/c/l/b Ljava/lang/Long;
invokevirtual java/lang/Long/longValue()J
invokevirtual com/a/b/c/f/a(J)Lcom/a/b/c/f;
pop
L5:
aload 5
getfield com/a/b/c/l/c Ljava/lang/Long;
ifnull L6
aload 0
aload 5
getfield com/a/b/c/l/c Ljava/lang/Long;
invokevirtual java/lang/Long/longValue()J
invokevirtual com/a/b/c/f/b(J)Lcom/a/b/c/f;
pop
L6:
aload 5
getfield com/a/b/c/l/d Ljava/lang/Integer;
ifnull L7
aload 0
aload 5
getfield com/a/b/c/l/d Ljava/lang/Integer;
invokevirtual java/lang/Integer/intValue()I
invokevirtual com/a/b/c/f/a(I)Lcom/a/b/c/f;
pop
L7:
aload 5
getfield com/a/b/c/l/e Lcom/a/b/c/bw;
ifnull L8
getstatic com/a/b/c/m/a [I
aload 5
getfield com/a/b/c/l/e Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/ordinal()I
iaload
tableswitch 1
L9
default : L10
L10:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 2
goto L4
L9:
aload 0
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
pop
L8:
aload 5
getfield com/a/b/c/l/f Lcom/a/b/c/bw;
ifnull L11
getstatic com/a/b/c/m/a [I
aload 5
getfield com/a/b/c/l/f Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/ordinal()I
iaload
tableswitch 1
L12
L13
default : L14
L14:
new java/lang/AssertionError
dup
invokespecial java/lang/AssertionError/<init>()V
athrow
L13:
aload 0
getstatic com/a/b/c/bw/b Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
pop
L11:
aload 5
getfield com/a/b/c/l/g Ljava/lang/Boolean;
ifnull L15
aload 5
getfield com/a/b/c/l/g Ljava/lang/Boolean;
invokevirtual java/lang/Boolean/booleanValue()Z
ifeq L15
aload 0
getstatic com/a/b/c/f/c Lcom/a/b/b/dz;
putfield com/a/b/c/f/u Lcom/a/b/b/dz;
L15:
aload 5
getfield com/a/b/c/l/i Ljava/util/concurrent/TimeUnit;
ifnull L16
aload 0
aload 5
getfield com/a/b/c/l/h J
aload 5
getfield com/a/b/c/l/i Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/c/f/a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
pop
L16:
aload 5
getfield com/a/b/c/l/k Ljava/util/concurrent/TimeUnit;
ifnull L17
aload 0
aload 5
getfield com/a/b/c/l/j J
aload 5
getfield com/a/b/c/l/k Ljava/util/concurrent/TimeUnit;
invokevirtual com/a/b/c/f/b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
pop
L17:
aload 5
getfield com/a/b/c/l/m Ljava/util/concurrent/TimeUnit;
ifnull L18
aload 5
getfield com/a/b/c/l/l J
lstore 3
aload 5
getfield com/a/b/c/l/m Ljava/util/concurrent/TimeUnit;
astore 5
aload 5
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L19
iconst_1
istore 2
L20:
iload 2
ldc "refresh was already set to %s ns"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/p J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 3
lconst_0
lcmp
ifle L21
iconst_1
istore 2
L22:
iload 2
ldc "duration must be positive: %s %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
lload 3
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 5
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 5
lload 3
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
putfield com/a/b/c/f/p J
L18:
aload 0
iconst_0
putfield com/a/b/c/f/f Z
aload 0
areturn
L12:
aload 0
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
pop
goto L11
L19:
iconst_0
istore 2
goto L20
L21:
iconst_0
istore 2
goto L22
.limit locals 6
.limit stack 7
.end method

.method private b(I)Lcom/a/b/c/f;
iconst_1
istore 3
aload 0
getfield com/a/b/c/f/g I
iconst_m1
if_icmpne L0
iconst_1
istore 2
L1:
iload 2
ldc "initial capacity was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/g I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
iflt L2
iload 3
istore 2
L3:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iload 1
putfield com/a/b/c/f/g I
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 4
.limit stack 6
.end method

.method private b(Lcom/a/b/b/au;)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
aload 0
getfield com/a/b/c/f/r Lcom/a/b/b/au;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
ldc "value equivalence was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/r Lcom/a/b/b/au;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
putfield com/a/b/c/f/r Lcom/a/b/b/au;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method private c(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported (synchronously)."
.end annotation
aload 3
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
pop
aload 0
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 4
L1:
iload 4
ldc "refresh was already set to %s ns"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/p J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 1
lconst_0
lcmp
ifle L2
iconst_1
istore 4
L3:
iload 4
ldc "duration must be positive: %s %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
putfield com/a/b/c/f/p J
aload 0
areturn
L0:
iconst_0
istore 4
goto L1
L2:
iconst_0
istore 4
goto L3
.limit locals 5
.limit stack 7
.end method

.method private e()Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
aload 0
iconst_0
putfield com/a/b/c/f/f Z
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method private f()Lcom/a/b/b/au;
aload 0
getfield com/a/b/c/f/q Lcom/a/b/b/au;
aload 0
invokevirtual com/a/b/c/f/b()Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/a()Lcom/a/b/b/au;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
areturn
.limit locals 1
.limit stack 2
.end method

.method private g()Lcom/a/b/b/au;
aload 0
getfield com/a/b/c/f/r Lcom/a/b/b/au;
aload 0
invokevirtual com/a/b/c/f/c()Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/a()Lcom/a/b/b/au;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/b/au
areturn
.limit locals 1
.limit stack 2
.end method

.method private h()I
aload 0
getfield com/a/b/c/f/g I
iconst_m1
if_icmpne L0
bipush 16
ireturn
L0:
aload 0
getfield com/a/b/c/f/g I
ireturn
.limit locals 1
.limit stack 2
.end method

.method private i()I
aload 0
getfield com/a/b/c/f/h I
iconst_m1
if_icmpne L0
iconst_4
ireturn
L0:
aload 0
getfield com/a/b/c/f/h I
ireturn
.limit locals 1
.limit stack 2
.end method

.method private j()J
aload 0
getfield com/a/b/c/f/n J
lconst_0
lcmp
ifeq L0
aload 0
getfield com/a/b/c/f/o J
lconst_0
lcmp
ifne L1
L0:
lconst_0
lreturn
L1:
aload 0
getfield com/a/b/c/f/k Lcom/a/b/c/do;
ifnonnull L2
aload 0
getfield com/a/b/c/f/i J
lreturn
L2:
aload 0
getfield com/a/b/c/f/j J
lreturn
.limit locals 1
.limit stack 4
.end method

.method private k()Lcom/a/b/c/do;
aload 0
getfield com/a/b/c/f/k Lcom/a/b/c/do;
getstatic com/a/b/c/k/a Lcom/a/b/c/k;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/do
areturn
.limit locals 1
.limit stack 2
.end method

.method private l()Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.ref.WeakReference"
.end annotation
aload 0
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
areturn
.limit locals 1
.limit stack 2
.end method

.method private m()Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.ref.WeakReference"
.end annotation
aload 0
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
areturn
.limit locals 1
.limit stack 2
.end method

.method private n()Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "java.lang.ref.SoftReference"
.end annotation
aload 0
getstatic com/a/b/c/bw/b Lcom/a/b/c/bw;
invokevirtual com/a/b/c/f/b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
areturn
.limit locals 1
.limit stack 2
.end method

.method private o()J
aload 0
getfield com/a/b/c/f/n J
ldc2_w -1L
lcmp
ifne L0
lconst_0
lreturn
L0:
aload 0
getfield com/a/b/c/f/n J
lreturn
.limit locals 1
.limit stack 4
.end method

.method private p()J
aload 0
getfield com/a/b/c/f/o J
ldc2_w -1L
lcmp
ifne L0
lconst_0
lreturn
L0:
aload 0
getfield com/a/b/c/f/o J
lreturn
.limit locals 1
.limit stack 4
.end method

.method private q()J
aload 0
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L0
lconst_0
lreturn
L0:
aload 0
getfield com/a/b/c/f/p J
lreturn
.limit locals 1
.limit stack 4
.end method

.method private r()Lcom/a/b/c/dg;
aload 0
getfield com/a/b/c/f/s Lcom/a/b/c/dg;
getstatic com/a/b/c/j/a Lcom/a/b/c/j;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/dg
areturn
.limit locals 1
.limit stack 2
.end method

.method private s()Lcom/a/b/c/f;
aload 0
getstatic com/a/b/c/f/c Lcom/a/b/b/dz;
putfield com/a/b/c/f/u Lcom/a/b/b/dz;
aload 0
areturn
.limit locals 1
.limit stack 2
.end method

.method private t()Z
aload 0
getfield com/a/b/c/f/u Lcom/a/b/b/dz;
getstatic com/a/b/c/f/c Lcom/a/b/b/dz;
if_acmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private u()Lcom/a/b/b/dz;
aload 0
getfield com/a/b/c/f/u Lcom/a/b/b/dz;
areturn
.limit locals 1
.limit stack 1
.end method

.method private v()Lcom/a/b/c/e;
aload 0
invokevirtual com/a/b/c/f/d()V
aload 0
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "refreshAfterWrite requires a LoadingCache"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
new com/a/b/c/bo
dup
aload 0
invokespecial com/a/b/c/bo/<init>(Lcom/a/b/c/f;)V
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 4
.end method

.method private w()V
aload 0
getfield com/a/b/c/f/p J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 1
L1:
iload 1
ldc "refreshAfterWrite requires a LoadingCache"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
return
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 4
.end method

.method public final a(Lcom/a/b/c/ab;)Lcom/a/b/c/an;
aload 0
invokevirtual com/a/b/c/f/d()V
new com/a/b/c/bn
dup
aload 0
aload 1
invokespecial com/a/b/c/bn/<init>(Lcom/a/b/c/f;Lcom/a/b/c/ab;)V
areturn
.limit locals 2
.limit stack 4
.end method

.method public final a(I)Lcom/a/b/c/f;
iconst_1
istore 3
aload 0
getfield com/a/b/c/f/h I
iconst_m1
if_icmpne L0
iconst_1
istore 2
L1:
iload 2
ldc "concurrency level was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/h I
invokestatic java/lang/Integer/valueOf(I)Ljava/lang/Integer;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
iload 1
ifle L2
iload 3
istore 2
L3:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
aload 0
iload 1
putfield com/a/b/c/f/h I
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
L2:
iconst_0
istore 2
goto L3
.limit locals 4
.limit stack 6
.end method

.method public final a(J)Lcom/a/b/c/f;
iconst_1
istore 4
aload 0
getfield com/a/b/c/f/i J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 3
L1:
iload 3
ldc "maximum size was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/i J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/c/f/j J
ldc2_w -1L
lcmp
ifne L2
iconst_1
istore 3
L3:
iload 3
ldc "maximum weight was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/j J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/c/f/k Lcom/a/b/c/do;
ifnonnull L4
iconst_1
istore 3
L5:
iload 3
ldc "maximum size can not be combined with weigher"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
lload 1
lconst_0
lcmp
iflt L6
iload 4
istore 3
L7:
iload 3
ldc "maximum size must not be negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
lload 1
putfield com/a/b/c/f/i J
aload 0
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
L4:
iconst_0
istore 3
goto L5
L6:
iconst_0
istore 3
goto L7
.limit locals 5
.limit stack 7
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/f/n J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 4
L1:
iload 4
ldc "expireAfterWrite was already set to %s ns"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/n J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 1
lconst_0
lcmp
iflt L2
iconst_1
istore 4
L3:
iload 4
ldc "duration cannot be negative: %s %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
putfield com/a/b/c/f/n J
aload 0
areturn
L0:
iconst_0
istore 4
goto L1
L2:
iconst_0
istore 4
goto L3
.limit locals 5
.limit stack 7
.end method

.method public final a(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/f/l Lcom/a/b/c/bw;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
ldc "Key strength was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/l Lcom/a/b/c/bw;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/bw
putfield com/a/b/c/f/l Lcom/a/b/c/bw;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method final b()Lcom/a/b/c/bw;
aload 0
getfield com/a/b/c/f/l Lcom/a/b/c/bw;
getstatic com/a/b/c/bw/a Lcom/a/b/c/bw;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/bw
areturn
.limit locals 1
.limit stack 2
.end method

.method public final b(J)Lcom/a/b/c/f;
.annotation invisible Lcom/a/b/a/c;
a s = "To be supported"
.end annotation
iconst_1
istore 4
aload 0
getfield com/a/b/c/f/j J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 3
L1:
iload 3
ldc "maximum weight was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/j J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/c/f/i J
ldc2_w -1L
lcmp
ifne L2
iconst_1
istore 3
L3:
iload 3
ldc "maximum size was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/i J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
lload 1
putfield com/a/b/c/f/j J
lload 1
lconst_0
lcmp
iflt L4
iload 4
istore 3
L5:
iload 3
ldc "maximum weight must not be negative"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
aload 0
areturn
L0:
iconst_0
istore 3
goto L1
L2:
iconst_0
istore 3
goto L3
L4:
iconst_0
istore 3
goto L5
.limit locals 5
.limit stack 7
.end method

.method public final b(JLjava/util/concurrent/TimeUnit;)Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/f/o J
ldc2_w -1L
lcmp
ifne L0
iconst_1
istore 4
L1:
iload 4
ldc "expireAfterAccess was already set to %s ns"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/o J
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
lload 1
lconst_0
lcmp
iflt L2
iconst_1
istore 4
L3:
iload 4
ldc "duration cannot be negative: %s %s"
iconst_2
anewarray java/lang/Object
dup
iconst_0
lload 1
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
aastore
dup
iconst_1
aload 3
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 3
lload 1
invokevirtual java/util/concurrent/TimeUnit/toNanos(J)J
putfield com/a/b/c/f/o J
aload 0
areturn
L0:
iconst_0
istore 4
goto L1
L2:
iconst_0
istore 4
goto L3
.limit locals 5
.limit stack 7
.end method

.method final b(Lcom/a/b/c/bw;)Lcom/a/b/c/f;
aload 0
getfield com/a/b/c/f/m Lcom/a/b/c/bw;
ifnonnull L0
iconst_1
istore 2
L1:
iload 2
ldc "Value strength was already set to %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/c/f/m Lcom/a/b/c/bw;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/bw
putfield com/a/b/c/f/m Lcom/a/b/c/bw;
aload 0
areturn
L0:
iconst_0
istore 2
goto L1
.limit locals 3
.limit stack 6
.end method

.method final c()Lcom/a/b/c/bw;
aload 0
getfield com/a/b/c/f/m Lcom/a/b/c/bw;
getstatic com/a/b/c/bw/a Lcom/a/b/c/bw;
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
checkcast com/a/b/c/bw
areturn
.limit locals 1
.limit stack 2
.end method

.method final d()V
iconst_1
istore 2
iconst_1
istore 1
aload 0
getfield com/a/b/c/f/k Lcom/a/b/c/do;
ifnonnull L0
aload 0
getfield com/a/b/c/f/j J
ldc2_w -1L
lcmp
ifne L1
L2:
iload 1
ldc "maximumWeight requires weigher"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
L3:
return
L1:
iconst_0
istore 1
goto L2
L0:
aload 0
getfield com/a/b/c/f/f Z
ifeq L4
aload 0
getfield com/a/b/c/f/j J
ldc2_w -1L
lcmp
ifeq L5
iload 2
istore 1
L6:
iload 1
ldc "weigher requires maximumWeight"
invokestatic com/a/b/b/cn/b(ZLjava/lang/Object;)V
return
L5:
iconst_0
istore 1
goto L6
L4:
aload 0
getfield com/a/b/c/f/j J
ldc2_w -1L
lcmp
ifne L3
getstatic com/a/b/c/f/z Ljava/util/logging/Logger;
getstatic java/util/logging/Level/WARNING Ljava/util/logging/Level;
ldc "ignoring weigher specified without maximumWeight"
invokevirtual java/util/logging/Logger/log(Ljava/util/logging/Level;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method public final toString()Ljava/lang/String;
aload 0
invokestatic com/a/b/b/ca/a(Ljava/lang/Object;)Lcom/a/b/b/cc;
astore 3
aload 0
getfield com/a/b/c/f/g I
iconst_m1
if_icmpeq L0
aload 3
ldc "initialCapacity"
aload 0
getfield com/a/b/c/f/g I
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;I)Lcom/a/b/b/cc;
pop
L0:
aload 0
getfield com/a/b/c/f/h I
iconst_m1
if_icmpeq L1
aload 3
ldc "concurrencyLevel"
aload 0
getfield com/a/b/c/f/h I
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;I)Lcom/a/b/b/cc;
pop
L1:
aload 0
getfield com/a/b/c/f/i J
ldc2_w -1L
lcmp
ifeq L2
aload 3
ldc "maximumSize"
aload 0
getfield com/a/b/c/f/i J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
pop
L2:
aload 0
getfield com/a/b/c/f/j J
ldc2_w -1L
lcmp
ifeq L3
aload 3
ldc "maximumWeight"
aload 0
getfield com/a/b/c/f/j J
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;J)Lcom/a/b/b/cc;
pop
L3:
aload 0
getfield com/a/b/c/f/n J
ldc2_w -1L
lcmp
ifeq L4
aload 0
getfield com/a/b/c/f/n J
lstore 1
aload 3
ldc "expireAfterWrite"
new java/lang/StringBuilder
dup
bipush 22
invokespecial java/lang/StringBuilder/<init>(I)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "ns"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L4:
aload 0
getfield com/a/b/c/f/o J
ldc2_w -1L
lcmp
ifeq L5
aload 0
getfield com/a/b/c/f/o J
lstore 1
aload 3
ldc "expireAfterAccess"
new java/lang/StringBuilder
dup
bipush 22
invokespecial java/lang/StringBuilder/<init>(I)V
lload 1
invokevirtual java/lang/StringBuilder/append(J)Ljava/lang/StringBuilder;
ldc "ns"
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L5:
aload 0
getfield com/a/b/c/f/l Lcom/a/b/c/bw;
ifnull L6
aload 3
ldc "keyStrength"
aload 0
getfield com/a/b/c/f/l Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/toString()Ljava/lang/String;
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L6:
aload 0
getfield com/a/b/c/f/m Lcom/a/b/c/bw;
ifnull L7
aload 3
ldc "valueStrength"
aload 0
getfield com/a/b/c/f/m Lcom/a/b/c/bw;
invokevirtual com/a/b/c/bw/toString()Ljava/lang/String;
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
invokevirtual com/a/b/b/cc/a(Ljava/lang/String;Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L7:
aload 0
getfield com/a/b/c/f/q Lcom/a/b/b/au;
ifnull L8
aload 3
ldc "keyEquivalence"
invokevirtual com/a/b/b/cc/a(Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L8:
aload 0
getfield com/a/b/c/f/r Lcom/a/b/b/au;
ifnull L9
aload 3
ldc "valueEquivalence"
invokevirtual com/a/b/b/cc/a(Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L9:
aload 0
getfield com/a/b/c/f/s Lcom/a/b/c/dg;
ifnull L10
aload 3
ldc "removalListener"
invokevirtual com/a/b/b/cc/a(Ljava/lang/Object;)Lcom/a/b/b/cc;
pop
L10:
aload 3
invokevirtual com/a/b/b/cc/toString()Ljava/lang/String;
areturn
.limit locals 4
.limit stack 5
.end method
