.bytecode 50.0
.class public synchronized abstract enum com/a/b/c/bw
.super java/lang/Enum

.field public static final enum 'a' Lcom/a/b/c/bw;

.field public static final enum 'b' Lcom/a/b/c/bw;

.field public static final enum 'c' Lcom/a/b/c/bw;

.field private static final synthetic 'd' [Lcom/a/b/c/bw;

.method static <clinit>()V
new com/a/b/c/bx
dup
ldc "STRONG"
invokespecial com/a/b/c/bx/<init>(Ljava/lang/String;)V
putstatic com/a/b/c/bw/a Lcom/a/b/c/bw;
new com/a/b/c/by
dup
ldc "SOFT"
invokespecial com/a/b/c/by/<init>(Ljava/lang/String;)V
putstatic com/a/b/c/bw/b Lcom/a/b/c/bw;
new com/a/b/c/bz
dup
ldc "WEAK"
invokespecial com/a/b/c/bz/<init>(Ljava/lang/String;)V
putstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
iconst_3
anewarray com/a/b/c/bw
dup
iconst_0
getstatic com/a/b/c/bw/a Lcom/a/b/c/bw;
aastore
dup
iconst_1
getstatic com/a/b/c/bw/b Lcom/a/b/c/bw;
aastore
dup
iconst_2
getstatic com/a/b/c/bw/c Lcom/a/b/c/bw;
aastore
putstatic com/a/b/c/bw/d [Lcom/a/b/c/bw;
return
.limit locals 0
.limit stack 4
.end method

.method private <init>(Ljava/lang/String;I)V
aload 0
aload 1
iload 2
invokespecial java/lang/Enum/<init>(Ljava/lang/String;I)V
return
.limit locals 3
.limit stack 3
.end method

.method synthetic <init>(Ljava/lang/String;IB)V
aload 0
aload 1
iload 2
invokespecial com/a/b/c/bw/<init>(Ljava/lang/String;I)V
return
.limit locals 4
.limit stack 3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/a/b/c/bw;
ldc com/a/b/c/bw
aload 0
invokestatic java/lang/Enum/valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
checkcast com/a/b/c/bw
areturn
.limit locals 1
.limit stack 2
.end method

.method public static values()[Lcom/a/b/c/bw;
getstatic com/a/b/c/bw/d [Lcom/a/b/c/bw;
invokevirtual [Lcom/a/b/c/bw;/clone()Ljava/lang/Object;
checkcast [Lcom/a/b/c/bw;
areturn
.limit locals 0
.limit stack 1
.end method

.method abstract a()Lcom/a/b/b/au;
.end method

.method abstract a(Lcom/a/b/c/bt;Lcom/a/b/c/bs;Ljava/lang/Object;I)Lcom/a/b/c/cg;
.end method
