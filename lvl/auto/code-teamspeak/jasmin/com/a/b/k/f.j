.bytecode 50.0
.class public final synchronized com/a/b/k/f
.super java/lang/Object
.annotation invisible Lcom/a/b/a/a;
.end annotation
.annotation invisible Lcom/a/b/a/b;
.end annotation

.field private static final 'a' Lcom/a/b/b/m;

.field private static final 'b' Lcom/a/b/b/di;

.field private static final 'c' Lcom/a/b/b/bv;

.field private static final 'd' I = -1


.field private static final 'e' Ljava/lang/String; = "\\."

.field private static final 'f' I = 127


.field private static final 'g' I = 253


.field private static final 'h' I = 63


.field private static final 'l' Lcom/a/b/b/m;

.field private static final 'm' Lcom/a/b/b/m;

.field private final 'i' Ljava/lang/String;

.field private final 'j' Lcom/a/b/d/jl;

.field private final 'k' I

.method static <clinit>()V
ldc ".\u3002\uff0e\uff61"
invokestatic com/a/b/b/m/a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;
putstatic com/a/b/k/f/a Lcom/a/b/b/m;
bipush 46
invokestatic com/a/b/b/di/a(C)Lcom/a/b/b/di;
putstatic com/a/b/k/f/b Lcom/a/b/b/di;
bipush 46
invokestatic com/a/b/b/bv/a(C)Lcom/a/b/b/bv;
putstatic com/a/b/k/f/c Lcom/a/b/b/bv;
ldc "-_"
invokestatic com/a/b/b/m/a(Ljava/lang/CharSequence;)Lcom/a/b/b/m;
putstatic com/a/b/k/f/l Lcom/a/b/b/m;
getstatic com/a/b/b/m/f Lcom/a/b/b/m;
getstatic com/a/b/k/f/l Lcom/a/b/b/m;
invokevirtual com/a/b/b/m/b(Lcom/a/b/b/m;)Lcom/a/b/b/m;
putstatic com/a/b/k/f/m Lcom/a/b/b/m;
return
.limit locals 0
.limit stack 2
.end method

.method private <init>(Ljava/lang/String;)V
aload 0
invokespecial java/lang/Object/<init>()V
getstatic com/a/b/k/f/a Lcom/a/b/b/m;
aload 1
bipush 46
invokevirtual com/a/b/b/m/a(Ljava/lang/CharSequence;C)Ljava/lang/String;
invokestatic com/a/b/b/e/a(Ljava/lang/String;)Ljava/lang/String;
astore 3
aload 3
astore 1
aload 3
ldc "."
invokevirtual java/lang/String/endsWith(Ljava/lang/String;)Z
ifeq L0
aload 3
iconst_0
aload 3
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/substring(II)Ljava/lang/String;
astore 1
L0:
aload 1
invokevirtual java/lang/String/length()I
sipush 253
if_icmpgt L1
iconst_1
istore 2
L2:
iload 2
ldc "Domain name too long: '%s':"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 1
putfield com/a/b/k/f/i Ljava/lang/String;
aload 0
getstatic com/a/b/k/f/b Lcom/a/b/b/di;
aload 1
invokevirtual com/a/b/b/di/a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
invokestatic com/a/b/d/jl/a(Ljava/lang/Iterable;)Lcom/a/b/d/jl;
putfield com/a/b/k/f/j Lcom/a/b/d/jl;
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
bipush 127
if_icmpgt L3
iconst_1
istore 2
L4:
iload 2
ldc "Domain has too many parts: '%s'"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
invokestatic com/a/b/k/f/a(Ljava/util/List;)Z
ldc "Not a valid domain name: '%s'"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 1
aastore
invokestatic com/a/b/b/cn/a(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 0
invokespecial com/a/b/k/f/b()I
putfield com/a/b/k/f/k I
return
L1:
iconst_0
istore 2
goto L2
L3:
iconst_0
istore 2
goto L4
.limit locals 4
.limit stack 6
.end method

.method private a(I)Lcom/a/b/k/f;
getstatic com/a/b/k/f/c Lcom/a/b/b/bv;
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
iload 1
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
invokevirtual com/a/b/d/jl/a(II)Lcom/a/b/d/jl;
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
invokestatic com/a/b/k/f/a(Ljava/lang/String;)Lcom/a/b/k/f;
areturn
.limit locals 2
.limit stack 4
.end method

.method public static a(Ljava/lang/String;)Lcom/a/b/k/f;
new com/a/b/k/f
dup
aload 0
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokespecial com/a/b/k/f/<init>(Ljava/lang/String;)V
areturn
.limit locals 1
.limit stack 3
.end method

.method private static a(Ljava/lang/String;Z)Z
aload 0
invokevirtual java/lang/String/length()I
ifle L0
aload 0
invokevirtual java/lang/String/length()I
bipush 63
if_icmple L1
L0:
iconst_0
ireturn
L1:
getstatic com/a/b/b/m/b Lcom/a/b/b/m;
invokevirtual com/a/b/b/m/a()Lcom/a/b/b/m;
aload 0
invokevirtual com/a/b/b/m/h(Ljava/lang/CharSequence;)Ljava/lang/String;
astore 2
getstatic com/a/b/k/f/m Lcom/a/b/b/m;
aload 2
invokevirtual com/a/b/b/m/c(Ljava/lang/CharSequence;)Z
ifeq L0
getstatic com/a/b/k/f/l Lcom/a/b/b/m;
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokevirtual com/a/b/b/m/c(C)Z
ifne L0
getstatic com/a/b/k/f/l Lcom/a/b/b/m;
aload 0
aload 0
invokevirtual java/lang/String/length()I
iconst_1
isub
invokevirtual java/lang/String/charAt(I)C
invokevirtual com/a/b/b/m/c(C)Z
ifne L0
iload 1
ifeq L2
getstatic com/a/b/b/m/c Lcom/a/b/b/m;
aload 0
iconst_0
invokevirtual java/lang/String/charAt(I)C
invokevirtual com/a/b/b/m/c(C)Z
ifne L0
L2:
iconst_1
ireturn
.limit locals 3
.limit stack 4
.end method

.method private static a(Ljava/util/List;)Z
aload 0
invokeinterface java/util/List/size()I 0
iconst_1
isub
istore 2
aload 0
iload 2
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/String
iconst_1
invokestatic com/a/b/k/f/a(Ljava/lang/String;Z)Z
ifne L0
iconst_0
ireturn
L0:
iconst_0
istore 1
L1:
iload 1
iload 2
if_icmpge L2
aload 0
iload 1
invokeinterface java/util/List/get(I)Ljava/lang/Object; 1
checkcast java/lang/String
iconst_0
invokestatic com/a/b/k/f/a(Ljava/lang/String;Z)Z
ifne L3
iconst_0
ireturn
L3:
iload 1
iconst_1
iadd
istore 1
goto L1
L2:
iconst_1
ireturn
.limit locals 3
.limit stack 2
.end method

.method private b()I
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
istore 3
iconst_0
istore 1
L0:
iload 1
iload 3
if_icmpge L1
getstatic com/a/b/k/f/c Lcom/a/b/b/bv;
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
iload 1
iload 3
invokevirtual com/a/b/d/jl/a(II)Lcom/a/b/d/jl;
invokevirtual com/a/b/b/bv/a(Ljava/lang/Iterable;)Ljava/lang/String;
astore 4
getstatic com/a/d/a/a/a Lcom/a/b/d/jt;
aload 4
invokevirtual com/a/b/d/jt/containsKey(Ljava/lang/Object;)Z
ifeq L2
L3:
iload 1
ireturn
L2:
getstatic com/a/d/a/a/c Lcom/a/b/d/jt;
aload 4
invokevirtual com/a/b/d/jt/containsKey(Ljava/lang/Object;)Z
ifeq L4
iload 1
iconst_1
iadd
ireturn
L4:
aload 4
ldc "\\."
iconst_2
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 4
aload 4
arraylength
iconst_2
if_icmpne L5
getstatic com/a/d/a/a/b Lcom/a/b/d/jt;
aload 4
iconst_1
aaload
invokevirtual com/a/b/d/jt/containsKey(Ljava/lang/Object;)Z
ifeq L5
iconst_1
istore 2
L6:
iload 2
ifne L3
iload 1
iconst_1
iadd
istore 1
goto L0
L5:
iconst_0
istore 2
goto L6
L1:
iconst_m1
ireturn
.limit locals 5
.limit stack 4
.end method

.method private b(Ljava/lang/String;)Lcom/a/b/k/f;
aload 1
invokestatic com/a/b/b/cn/a(Ljava/lang/Object;)Ljava/lang/Object;
checkcast java/lang/String
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 1
aload 0
getfield com/a/b/k/f/i Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
invokestatic java/lang/String/valueOf(Ljava/lang/Object;)Ljava/lang/String;
astore 2
new java/lang/StringBuilder
dup
aload 1
invokevirtual java/lang/String/length()I
iconst_1
iadd
aload 2
invokevirtual java/lang/String/length()I
iadd
invokespecial java/lang/StringBuilder/<init>(I)V
aload 1
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
ldc "."
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
aload 2
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
invokestatic com/a/b/k/f/a(Ljava/lang/String;)Lcom/a/b/k/f;
areturn
.limit locals 3
.limit stack 4
.end method

.method private c()Lcom/a/b/d/jl;
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
areturn
.limit locals 1
.limit stack 1
.end method

.method private static c(Ljava/lang/String;)Z
.catch java/lang/IllegalArgumentException from L0 to L1 using L2
L0:
aload 0
invokestatic com/a/b/k/f/a(Ljava/lang/String;)Lcom/a/b/k/f;
pop
L1:
iconst_1
ireturn
L2:
astore 0
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private d()Z
aload 0
getfield com/a/b/k/f/k I
ifne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private static d(Ljava/lang/String;)Z
aload 0
ldc "\\."
iconst_2
invokevirtual java/lang/String/split(Ljava/lang/String;I)[Ljava/lang/String;
astore 0
aload 0
arraylength
iconst_2
if_icmpne L0
getstatic com/a/d/a/a/b Lcom/a/b/d/jt;
aload 0
iconst_1
aaload
invokevirtual com/a/b/d/jt/containsKey(Ljava/lang/Object;)Z
ifeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 3
.end method

.method private e()Lcom/a/b/k/f;
aload 0
invokevirtual com/a/b/k/f/a()Z
ifeq L0
aload 0
aload 0
getfield com/a/b/k/f/k I
invokespecial com/a/b/k/f/a(I)Lcom/a/b/k/f;
areturn
L0:
aconst_null
areturn
.limit locals 1
.limit stack 2
.end method

.method private f()Z
aload 0
getfield com/a/b/k/f/k I
ifle L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 1
.end method

.method private g()Z
aload 0
getfield com/a/b/k/f/k I
iconst_1
if_icmpne L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private h()Lcom/a/b/k/f;
aload 0
getfield com/a/b/k/f/k I
iconst_1
if_icmpne L0
iconst_1
istore 1
L1:
iload 1
ifeq L2
aload 0
areturn
L0:
iconst_0
istore 1
goto L1
L2:
aload 0
getfield com/a/b/k/f/k I
ifle L3
iconst_1
istore 2
L4:
iload 2
ldc "Not under a public suffix: %s"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/k/f/i Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
aload 0
getfield com/a/b/k/f/k I
iconst_1
isub
invokespecial com/a/b/k/f/a(I)Lcom/a/b/k/f;
areturn
L3:
iconst_0
istore 2
goto L4
.limit locals 3
.limit stack 6
.end method

.method private i()Z
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
iconst_1
if_icmple L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method private j()Lcom/a/b/k/f;
aload 0
getfield com/a/b/k/f/j Lcom/a/b/d/jl;
invokevirtual com/a/b/d/jl/size()I
iconst_1
if_icmple L0
iconst_1
istore 1
L1:
iload 1
ldc "Domain '%s' has no parent"
iconst_1
anewarray java/lang/Object
dup
iconst_0
aload 0
getfield com/a/b/k/f/i Ljava/lang/String;
aastore
invokestatic com/a/b/b/cn/b(ZLjava/lang/String;[Ljava/lang/Object;)V
aload 0
iconst_1
invokespecial com/a/b/k/f/a(I)Lcom/a/b/k/f;
areturn
L0:
iconst_0
istore 1
goto L1
.limit locals 2
.limit stack 6
.end method

.method public final a()Z
aload 0
getfield com/a/b/k/f/k I
iconst_m1
if_icmpeq L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 1
.limit stack 2
.end method

.method public final equals(Ljava/lang/Object;)Z
.annotation visibleparam 1 Ljavax/annotation/Nullable;
.end annotation
aload 1
aload 0
if_acmpne L0
iconst_1
ireturn
L0:
aload 1
instanceof com/a/b/k/f
ifeq L1
aload 1
checkcast com/a/b/k/f
astore 1
aload 0
getfield com/a/b/k/f/i Ljava/lang/String;
aload 1
getfield com/a/b/k/f/i Ljava/lang/String;
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ireturn
L1:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method public final hashCode()I
aload 0
getfield com/a/b/k/f/i Ljava/lang/String;
invokevirtual java/lang/String/hashCode()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final toString()Ljava/lang/String;
aload 0
getfield com/a/b/k/f/i Ljava/lang/String;
areturn
.limit locals 1
.limit stack 1
.end method
