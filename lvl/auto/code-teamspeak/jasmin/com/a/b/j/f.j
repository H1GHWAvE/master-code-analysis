.bytecode 50.0
.class final synchronized com/a/b/j/f
.super java/lang/Object

.field static final 'a' J = 4503599627370495L


.field static final 'b' J = 9218868437227405312L


.field static final 'c' J = -9223372036854775808L


.field static final 'd' I = 52


.field static final 'e' I = 1023


.field static final 'f' J = 4503599627370496L


.field private static final 'g' J

.method static <clinit>()V
dconst_1
invokestatic java/lang/Double/doubleToRawLongBits(D)J
putstatic com/a/b/j/f/g J
return
.limit locals 0
.limit stack 2
.end method

.method private <init>()V
aload 0
invokespecial java/lang/Object/<init>()V
return
.limit locals 1
.limit stack 1
.end method

.method static a(Ljava/math/BigInteger;)D
iconst_1
istore 2
aload 0
invokevirtual java/math/BigInteger/abs()Ljava/math/BigInteger;
astore 9
aload 9
invokevirtual java/math/BigInteger/bitLength()I
iconst_1
isub
istore 3
iload 3
bipush 63
if_icmpge L0
aload 0
invokevirtual java/math/BigInteger/longValue()J
l2d
dreturn
L0:
iload 3
sipush 1023
if_icmple L1
aload 0
invokevirtual java/math/BigInteger/signum()I
i2d
ldc2_w +doubleinfinity
dmul
dreturn
L1:
iload 3
bipush 52
isub
iconst_1
isub
istore 4
aload 9
iload 4
invokevirtual java/math/BigInteger/shiftRight(I)Ljava/math/BigInteger;
invokevirtual java/math/BigInteger/longValue()J
lstore 5
lload 5
iconst_1
lshr
ldc2_w 4503599627370495L
land
lstore 7
lload 5
lconst_1
land
lconst_0
lcmp
ifeq L2
iload 2
istore 1
lload 7
lconst_1
land
lconst_0
lcmp
ifne L3
aload 9
invokevirtual java/math/BigInteger/getLowestSetBit()I
iload 4
if_icmpge L2
iload 2
istore 1
L3:
lload 7
lstore 5
iload 1
ifeq L4
lload 7
lconst_1
ladd
lstore 5
L4:
lload 5
iload 3
sipush 1023
iadd
i2l
bipush 52
lshl
ladd
aload 0
invokevirtual java/math/BigInteger/signum()I
i2l
ldc2_w -9223372036854775808L
land
lor
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
L2:
iconst_0
istore 1
goto L3
.limit locals 10
.limit stack 6
.end method

.method static a(D)J
dload 0
invokestatic com/a/b/j/f/b(D)Z
ldc "not a normal value"
invokestatic com/a/b/b/cn/a(ZLjava/lang/Object;)V
dload 0
invokestatic java/lang/Math/getExponent(D)I
istore 2
dload 0
invokestatic java/lang/Double/doubleToRawLongBits(D)J
ldc2_w 4503599627370495L
land
lstore 3
iload 2
sipush -1023
if_icmpne L0
lload 3
iconst_1
lshl
lreturn
L0:
ldc2_w 4503599627370496L
lload 3
lor
lreturn
.limit locals 5
.limit stack 4
.end method

.method static b(D)Z
dload 0
invokestatic java/lang/Math/getExponent(D)I
sipush 1023
if_icmpgt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method static c(D)Z
dload 0
invokestatic java/lang/Math/getExponent(D)I
sipush -1022
if_icmplt L0
iconst_1
ireturn
L0:
iconst_0
ireturn
.limit locals 2
.limit stack 2
.end method

.method static d(D)D
dload 0
invokestatic java/lang/Double/doubleToRawLongBits(D)J
ldc2_w 4503599627370495L
land
getstatic com/a/b/j/f/g J
lor
invokestatic java/lang/Double/longBitsToDouble(J)D
dreturn
.limit locals 2
.limit stack 4
.end method

.method private static e(D)D
dload 0
dneg
invokestatic java/lang/Math/nextUp(D)D
dneg
dreturn
.limit locals 2
.limit stack 2
.end method

.method private static f(D)D
dload 0
invokestatic java/lang/Double/isNaN(D)Z
ifne L0
iconst_1
istore 2
L1:
iload 2
invokestatic com/a/b/b/cn/a(Z)V
dload 0
dconst_0
dcmpl
ifle L2
dload 0
dreturn
L0:
iconst_0
istore 2
goto L1
L2:
dconst_0
dreturn
.limit locals 3
.limit stack 4
.end method
