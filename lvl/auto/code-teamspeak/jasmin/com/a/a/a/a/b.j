.bytecode 50.0
.class public final synchronized com/a/a/a/a/b
.super java/lang/Object
.implements com/a/a/a/a/s

.field public static final 'a' I = 0


.field public static final 'b' I = 1


.field private static final 'f' Ljava/lang/String; = "APKExpansionPolicy"

.field private static final 'g' Ljava/lang/String; = "com.android.vending.licensing.APKExpansionPolicy"

.field private static final 'h' Ljava/lang/String; = "lastResponse"

.field private static final 'i' Ljava/lang/String; = "validityTimestamp"

.field private static final 'j' Ljava/lang/String; = "retryUntil"

.field private static final 'k' Ljava/lang/String; = "maxRetries"

.field private static final 'l' Ljava/lang/String; = "retryCount"

.field private static final 'm' Ljava/lang/String; = "0"

.field private static final 'n' Ljava/lang/String; = "0"

.field private static final 'o' Ljava/lang/String; = "0"

.field private static final 'p' Ljava/lang/String; = "0"

.field private static final 'q' J = 60000L


.field private 'A' Ljava/util/Vector;

.field private 'r' J

.field private 's' J

.field private 't' J

.field private 'u' J

.field private 'v' J

.field private 'w' I

.field private 'x' Lcom/a/a/a/a/t;

.field private 'y' Ljava/util/Vector;

.field private 'z' Ljava/util/Vector;

.method private <init>(Landroid/content/Context;Lcom/a/a/a/a/r;)V
aload 0
invokespecial java/lang/Object/<init>()V
aload 0
lconst_0
putfield com/a/a/a/a/b/v J
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/a/a/a/a/b/y Ljava/util/Vector;
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/a/a/a/a/b/z Ljava/util/Vector;
aload 0
new java/util/Vector
dup
invokespecial java/util/Vector/<init>()V
putfield com/a/a/a/a/b/A Ljava/util/Vector;
aload 0
new com/a/a/a/a/t
dup
aload 1
ldc "com.android.vending.licensing.APKExpansionPolicy"
iconst_0
invokevirtual android/content/Context/getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;
aload 2
invokespecial com/a/a/a/a/t/<init>(Landroid/content/SharedPreferences;Lcom/a/a/a/a/r;)V
putfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
aload 0
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "lastResponse"
sipush 291
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
putfield com/a/a/a/a/b/w I
aload 0
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "validityTimestamp"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/b/r J
aload 0
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "retryUntil"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/b/s J
aload 0
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "maxRetries"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/b/t J
aload 0
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "retryCount"
ldc "0"
invokevirtual com/a/a/a/a/t/b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
putfield com/a/a/a/a/b/u J
return
.limit locals 3
.limit stack 6
.end method

.method private a(I)V
aload 0
invokestatic java/lang/System/currentTimeMillis()J
putfield com/a/a/a/a/b/v J
aload 0
iload 1
putfield com/a/a/a/a/b/w I
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "lastResponse"
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 2
.limit stack 3
.end method

.method private a(IJ)V
iload 1
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L0
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
iload 1
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L0:
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
iload 1
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 4
.limit stack 4
.end method

.method private a(ILjava/lang/String;)V
iload 1
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L0
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
iload 1
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L0:
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
iload 1
aload 2
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 3
.limit stack 3
.end method

.method private a(J)V
aload 0
lload 1
putfield com/a/a/a/a/b/u J
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "retryCount"
lload 1
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 3
.limit stack 4
.end method

.method private a(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/a/a/a/a/b/r J
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "validityTimestamp"
aload 4
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "License validity timestamp (VT) missing, caching for a minute"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 60000L
ladd
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
aload 1
invokevirtual java/lang/Long/longValue()J
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
astore 4
goto L3
.limit locals 6
.limit stack 4
.end method

.method private b(I)Ljava/lang/String;
iload 1
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
iload 1
invokevirtual java/util/Vector/elementAt(I)Ljava/lang/Object;
checkcast java/lang/String
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private b(ILjava/lang/String;)V
iload 1
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L0
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
iload 1
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L0:
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
iload 1
aload 2
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
return
.limit locals 3
.limit stack 3
.end method

.method private b(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/a/a/a/a/b/s J
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "retryUntil"
aload 4
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "License retry timestamp (GT) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private c(I)Ljava/lang/String;
iload 1
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
iload 1
invokevirtual java/util/Vector/elementAt(I)Ljava/lang/Object;
checkcast java/lang/String
areturn
L0:
aconst_null
areturn
.limit locals 2
.limit stack 2
.end method

.method private c()V
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "lastResponse"
sipush 291
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/b/b(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/b/c(Ljava/lang/String;)V
aload 0
ldc "0"
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
invokespecial com/a/a/a/a/b/a(J)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/b/a(Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
invokevirtual com/a/a/a/a/t/a()V
return
.limit locals 1
.limit stack 3
.end method

.method private c(Ljava/lang/String;)V
.catch java/lang/NumberFormatException from L0 to L1 using L2
L0:
aload 1
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 2
L1:
lload 2
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 5
aload 1
astore 4
aload 5
astore 1
L3:
aload 0
aload 1
invokevirtual java/lang/Long/longValue()J
putfield com/a/a/a/a/b/t J
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "maxRetries"
aload 4
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
return
L2:
astore 1
ldc "APKExpansionPolicy"
ldc "Licence retry count (GR) missing, grace period disabled"
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
ldc "0"
astore 4
lconst_0
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
astore 1
goto L3
.limit locals 6
.limit stack 3
.end method

.method private d()J
aload 0
getfield com/a/a/a/a/b/u J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private d(I)J
iload 1
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmpge L0
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
iload 1
invokevirtual java/util/Vector/elementAt(I)Ljava/lang/Object;
checkcast java/lang/Long
invokevirtual java/lang/Long/longValue()J
lreturn
L0:
ldc2_w -1L
lreturn
.limit locals 2
.limit stack 2
.end method

.method private static d(Ljava/lang/String;)Ljava/util/Map;
.catch java/lang/Exception from L0 to L1 using L2
.catch java/lang/Exception from L3 to L4 using L2
.catch java/lang/Exception from L5 to L6 using L2
.catch java/lang/Exception from L7 to L8 using L2
.catch java/lang/Exception from L9 to L10 using L2
new java/util/HashMap
dup
invokespecial java/util/HashMap/<init>()V
astore 4
L0:
aload 0
ldc "&"
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 5
aload 5
arraylength
istore 3
L1:
iconst_0
istore 1
L11:
iload 1
iload 3
if_icmpge L12
L3:
aload 5
iload 1
aaload
ldc "="
invokevirtual java/lang/String/split(Ljava/lang/String;)[Ljava/lang/String;
astore 6
L4:
aload 6
iconst_0
aaload
astore 0
iconst_0
istore 2
L5:
aload 4
aload 0
invokeinterface java/util/Map/containsKey(Ljava/lang/Object;)Z 1
ifeq L9
new java/lang/StringBuilder
dup
invokespecial java/lang/StringBuilder/<init>()V
aload 6
iconst_0
aaload
invokevirtual java/lang/StringBuilder/append(Ljava/lang/String;)Ljava/lang/StringBuilder;
astore 0
L6:
iload 2
iconst_1
iadd
istore 2
L7:
aload 0
iload 2
invokevirtual java/lang/StringBuilder/append(I)Ljava/lang/StringBuilder;
invokevirtual java/lang/StringBuilder/toString()Ljava/lang/String;
astore 0
L8:
goto L5
L9:
aload 4
aload 0
aload 6
iconst_1
aaload
invokeinterface java/util/Map/put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object; 2
pop
L10:
iload 1
iconst_1
iadd
istore 1
goto L11
L2:
astore 0
ldc "APKExpansionPolicy"
ldc "Invalid syntax error while decoding extras data from server."
invokestatic android/util/Log/w(Ljava/lang/String;Ljava/lang/String;)I
pop
L12:
aload 4
areturn
.limit locals 7
.limit stack 4
.end method

.method private e()J
aload 0
getfield com/a/a/a/a/b/r J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private f()J
aload 0
getfield com/a/a/a/a/b/s J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private g()J
aload 0
getfield com/a/a/a/a/b/t J
lreturn
.limit locals 1
.limit stack 2
.end method

.method private h()I
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
ireturn
.limit locals 1
.limit stack 1
.end method

.method public final a(ILcom/a/a/a/a/u;)V
iload 1
sipush 291
if_icmpeq L0
aload 0
lconst_0
invokespecial com/a/a/a/a/b/a(J)V
L1:
iload 1
sipush 256
if_icmpne L2
aload 2
getfield com/a/a/a/a/u/g Ljava/lang/String;
invokestatic com/a/a/a/a/b/d(Ljava/lang/String;)Ljava/util/Map;
astore 2
aload 0
iload 1
putfield com/a/a/a/a/b/w I
aload 0
invokestatic java/lang/System/currentTimeMillis()J
ldc2_w 60000L
ladd
invokestatic java/lang/Long/toString(J)Ljava/lang/String;
invokespecial com/a/a/a/a/b/a(Ljava/lang/String;)V
aload 2
invokeinterface java/util/Map/keySet()Ljava/util/Set; 0
invokeinterface java/util/Set/iterator()Ljava/util/Iterator; 0
astore 6
L3:
aload 6
invokeinterface java/util/Iterator/hasNext()Z 0
ifeq L4
aload 6
invokeinterface java/util/Iterator/next()Ljava/lang/Object; 0
checkcast java/lang/String
astore 7
aload 7
ldc "VT"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L5
aload 0
aload 2
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/a/a/a/a/b/a(Ljava/lang/String;)V
goto L3
L0:
aload 0
aload 0
getfield com/a/a/a/a/b/u J
lconst_1
ladd
invokespecial com/a/a/a/a/b/a(J)V
goto L1
L5:
aload 7
ldc "GT"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L6
aload 0
aload 2
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/a/a/a/a/b/b(Ljava/lang/String;)V
goto L3
L6:
aload 7
ldc "GR"
invokevirtual java/lang/String/equals(Ljava/lang/Object;)Z
ifeq L7
aload 0
aload 2
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokespecial com/a/a/a/a/b/c(Ljava/lang/String;)V
goto L3
L7:
aload 7
ldc "FILE_URL"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L8
aload 7
bipush 8
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
istore 3
aload 2
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 7
iload 3
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L9
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
iload 3
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L9:
aload 0
getfield com/a/a/a/a/b/y Ljava/util/Vector;
iload 3
aload 7
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
goto L3
L8:
aload 7
ldc "FILE_NAME"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L10
aload 7
bipush 9
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
istore 3
aload 2
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
astore 7
iload 3
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L11
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
iload 3
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L11:
aload 0
getfield com/a/a/a/a/b/z Ljava/util/Vector;
iload 3
aload 7
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
goto L3
L10:
aload 7
ldc "FILE_SIZE"
invokevirtual java/lang/String/startsWith(Ljava/lang/String;)Z
ifeq L3
aload 7
bipush 9
invokevirtual java/lang/String/substring(I)Ljava/lang/String;
invokestatic java/lang/Integer/parseInt(Ljava/lang/String;)I
iconst_1
isub
istore 3
aload 2
aload 7
invokeinterface java/util/Map/get(Ljava/lang/Object;)Ljava/lang/Object; 1
checkcast java/lang/String
invokestatic java/lang/Long/parseLong(Ljava/lang/String;)J
lstore 4
iload 3
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
invokevirtual java/util/Vector/size()I
if_icmplt L12
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
iload 3
iconst_1
iadd
invokevirtual java/util/Vector/setSize(I)V
L12:
aload 0
getfield com/a/a/a/a/b/A Ljava/util/Vector;
iload 3
lload 4
invokestatic java/lang/Long/valueOf(J)Ljava/lang/Long;
invokevirtual java/util/Vector/set(ILjava/lang/Object;)Ljava/lang/Object;
pop
goto L3
L2:
iload 1
sipush 561
if_icmpne L4
aload 0
ldc "0"
invokespecial com/a/a/a/a/b/a(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/b/b(Ljava/lang/String;)V
aload 0
ldc "0"
invokespecial com/a/a/a/a/b/c(Ljava/lang/String;)V
L4:
aload 0
invokestatic java/lang/System/currentTimeMillis()J
putfield com/a/a/a/a/b/v J
aload 0
iload 1
putfield com/a/a/a/a/b/w I
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
ldc "lastResponse"
iload 1
invokestatic java/lang/Integer/toString(I)Ljava/lang/String;
invokevirtual com/a/a/a/a/t/a(Ljava/lang/String;Ljava/lang/String;)V
aload 0
getfield com/a/a/a/a/b/x Lcom/a/a/a/a/t;
invokevirtual com/a/a/a/a/t/a()V
return
.limit locals 8
.limit stack 5
.end method

.method public final a(Lcom/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;)V
return
.limit locals 4
.limit stack 0
.end method

.method public final a()Z
invokestatic java/lang/System/currentTimeMillis()J
lstore 1
aload 0
getfield com/a/a/a/a/b/w I
sipush 256
if_icmpne L0
lload 1
aload 0
getfield com/a/a/a/a/b/r J
lcmp
ifgt L1
L2:
iconst_1
ireturn
L0:
aload 0
getfield com/a/a/a/a/b/w I
sipush 291
if_icmpne L1
lload 1
aload 0
getfield com/a/a/a/a/b/v J
ldc2_w 60000L
ladd
lcmp
ifge L1
lload 1
aload 0
getfield com/a/a/a/a/b/s J
lcmp
ifle L2
aload 0
getfield com/a/a/a/a/b/u J
aload 0
getfield com/a/a/a/a/b/t J
lcmp
ifle L2
iconst_1
ireturn
L1:
iconst_1
ireturn
.limit locals 3
.limit stack 6
.end method

.method public final b()Lcom/a/a/a/a/w;
aconst_null
areturn
.limit locals 1
.limit stack 1
.end method
