package jadx.core.dex.nodes;

import jadx.api.IJadxArgs;
import jadx.api.ResourceFile;
import jadx.api.ResourceType;
import jadx.api.ResourcesLoader;
import jadx.api.ResourcesLoader.ResourceDecoder;
import jadx.core.clsp.ClspGraph;
import jadx.core.dex.info.ClassInfo;
import jadx.core.utils.ErrorsCounter;
import jadx.core.utils.exceptions.DecodeException;
import jadx.core.utils.exceptions.JadxException;
import jadx.core.utils.files.InputFile;
import jadx.core.xmlgen.ResContainer;
import jadx.core.xmlgen.ResTableParser;
import jadx.core.xmlgen.ResourceStorage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RootNode {
    private static final Logger LOG = LoggerFactory.getLogger(RootNode.class);
    @Nullable
    private String appPackage;
    private ClassNode appResClass;
    private final IJadxArgs args;
    private ClspGraph clsp;
    private List<DexNode> dexNodes;
    private final ErrorsCounter errorsCounter = new ErrorsCounter();
    private Map<Integer, String> resourcesNames = new HashMap();

    public RootNode(IJadxArgs args) {
        this.args = args;
    }

    public void load(List<InputFile> dexFiles) throws DecodeException {
        this.dexNodes = new ArrayList(dexFiles.size());
        for (InputFile dex : dexFiles) {
            try {
                this.dexNodes.add(new DexNode(this, dex));
            } catch (Throwable e) {
                throw new DecodeException("Error decode file: " + dex, e);
            }
        }
        for (DexNode dexNode : this.dexNodes) {
            dexNode.loadClasses();
        }
        initInnerClasses();
    }

    public void loadResources(List<ResourceFile> resources) {
        ResourceFile arsc = null;
        for (ResourceFile rf : resources) {
            if (rf.getType() == ResourceType.ARSC) {
                arsc = rf;
                break;
            }
        }
        if (arsc == null) {
            LOG.debug("'.arsc' file not found");
            return;
        }
        final ResTableParser parser = new ResTableParser();
        try {
            ResourcesLoader.decodeStream(arsc, new ResourceDecoder() {
                public ResContainer decode(long size, InputStream is) throws IOException {
                    parser.decode(is);
                    return null;
                }
            });
            ResourceStorage resStorage = parser.getResStorage();
            this.resourcesNames = resStorage.getResourcesNames();
            this.appPackage = resStorage.getAppPackage();
        } catch (JadxException e) {
            LOG.error("Failed to parse '.arsc' file", e);
        }
    }

    public void initAppResClass() {
        if (this.appPackage == null) {
            this.appResClass = makeClass("R");
            return;
        }
        String fullName = this.appPackage + ".R";
        ClassNode resCls = searchClassByName(fullName);
        if (resCls != null) {
            this.appResClass = resCls;
        } else {
            this.appResClass = makeClass(fullName);
        }
    }

    private ClassNode makeClass(String clsName) {
        DexNode firstDex = (DexNode) this.dexNodes.get(0);
        return new ClassNode(firstDex, ClassInfo.fromName(firstDex, clsName));
    }

    public void initClassPath() throws DecodeException {
        try {
            if (this.clsp == null) {
                ClspGraph clsp = new ClspGraph();
                clsp.load();
                List<ClassNode> classes = new ArrayList();
                for (DexNode dexNode : this.dexNodes) {
                    classes.addAll(dexNode.getClasses());
                }
                clsp.addApp(classes);
                this.clsp = clsp;
            }
        } catch (Throwable e) {
            throw new DecodeException("Error loading classpath", e);
        }
    }

    private void initInnerClasses() {
        for (DexNode dexNode : this.dexNodes) {
            dexNode.initInnerClasses();
        }
    }

    public List<ClassNode> getClasses(boolean includeInner) {
        List<ClassNode> classes = new ArrayList();
        for (DexNode dex : this.dexNodes) {
            if (includeInner) {
                classes.addAll(dex.getClasses());
            } else {
                for (ClassNode cls : dex.getClasses()) {
                    if (!cls.getClassInfo().isInner()) {
                        classes.add(cls);
                    }
                }
            }
        }
        return classes;
    }

    public ClassNode searchClassByName(String fullName) {
        for (DexNode dexNode : this.dexNodes) {
            ClassNode cls = dexNode.resolveClass(ClassInfo.fromName(dexNode, fullName));
            if (cls != null) {
                return cls;
            }
        }
        return null;
    }

    public List<DexNode> getDexNodes() {
        return this.dexNodes;
    }

    public ClspGraph getClsp() {
        return this.clsp;
    }

    public ErrorsCounter getErrorsCounter() {
        return this.errorsCounter;
    }

    public Map<Integer, String> getResourcesNames() {
        return this.resourcesNames;
    }

    @Nullable
    public String getAppPackage() {
        return this.appPackage;
    }

    public ClassNode getAppResClass() {
        return this.appResClass;
    }

    public IJadxArgs getArgs() {
        return this.args;
    }
}
