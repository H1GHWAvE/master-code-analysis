package jadx.core.utils.files;

import com.android.dex.Dex;
import jadx.core.utils.AsmUtils;
import jadx.core.utils.exceptions.DecodeException;
import jadx.core.utils.exceptions.JadxException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InputFile {
    private static final Logger LOG = LoggerFactory.getLogger(InputFile.class);
    private final Dex dexBuf;
    private final File file;

    public InputFile(File file) throws IOException, DecodeException {
        if (file.exists()) {
            this.file = file;
            this.dexBuf = loadDexBuffer();
            return;
        }
        throw new IOException("File not found: " + file.getAbsolutePath());
    }

    private Dex loadDexBuffer() throws IOException, DecodeException {
        String fileName = this.file.getName();
        if (fileName.endsWith(".dex")) {
            return new Dex(this.file);
        }
        if (fileName.endsWith(".class")) {
            return loadFromClassFile(this.file);
        }
        Dex dex;
        if (fileName.endsWith(".apk") || fileName.endsWith(".zip")) {
            dex = loadFromZip(this.file);
            if (dex != null) {
                return dex;
            }
            throw new IOException("File 'classes.dex' not found in file: " + this.file);
        } else if (fileName.endsWith(".jar")) {
            dex = loadFromZip(this.file);
            return dex == null ? loadFromJar(this.file) : dex;
        } else {
            throw new DecodeException("Unsupported input file format: " + this.file);
        }
    }

    private static Dex loadFromJar(File jarFile) throws DecodeException {
        try {
            LOG.info("converting to dex: {} ...", jarFile.getName());
            JavaToDex j2d = new JavaToDex();
            byte[] ba = j2d.convert(jarFile.getAbsolutePath());
            if (ba.length == 0) {
                throw new JadxException(j2d.isError() ? j2d.getDxErrors() : "Empty dx output");
            }
            if (j2d.isError()) {
                LOG.warn("dx message: {}", j2d.getDxErrors());
            }
            return new Dex(ba);
        } catch (Throwable e) {
            DecodeException decodeException = new DecodeException("java class to dex conversion error:\n " + e.getMessage(), e);
        }
    }

    private static Dex loadFromZip(File file) throws IOException {
        ZipFile zf = new ZipFile(file);
        ZipEntry dex = zf.getEntry("classes.dex");
        if (dex == null) {
            zf.close();
            return null;
        }
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        InputStream in = null;
        try {
            in = zf.getInputStream(dex);
            byte[] buffer = new byte[8192];
            while (true) {
                int count = in.read(buffer);
                if (count == -1) {
                    break;
                }
                bytesOut.write(buffer, 0, count);
            }
            if (in != null) {
                in.close();
            }
            zf.close();
            return new Dex(bytesOut.toByteArray());
        } catch (Throwable th) {
            if (in != null) {
                in.close();
            }
            zf.close();
        }
    }

    private static Dex loadFromClassFile(File file) throws IOException, DecodeException {
        Throwable th;
        File outFile = File.createTempFile("jadx-tmp-", System.nanoTime() + ".jar");
        outFile.deleteOnExit();
        FileOutputStream out = null;
        JarOutputStream jo = null;
        try {
            JarOutputStream jo2;
            FileOutputStream out2 = new FileOutputStream(outFile);
            try {
                jo2 = new JarOutputStream(out2);
            } catch (Throwable th2) {
                th = th2;
                out = out2;
                if (jo != null) {
                    jo.close();
                }
                if (out != null) {
                    out.close();
                }
                throw th;
            }
            try {
                String clsName = AsmUtils.getNameFromClassFile(file);
                if (clsName == null) {
                    throw new IOException("Can't read class name from file: " + file);
                }
                FileUtils.addFileToJar(jo2, file, clsName + ".class");
                if (jo2 != null) {
                    jo2.close();
                }
                if (out2 != null) {
                    out2.close();
                }
                return loadFromJar(outFile);
            } catch (Throwable th3) {
                th = th3;
                jo = jo2;
                out = out2;
                if (jo != null) {
                    jo.close();
                }
                if (out != null) {
                    out.close();
                }
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            if (jo != null) {
                jo.close();
            }
            if (out != null) {
                out.close();
            }
            throw th;
        }
    }

    public File getFile() {
        return this.file;
    }

    public Dex getDexBuffer() {
        return this.dexBuf;
    }

    public String toString() {
        return this.file.toString();
    }
}
