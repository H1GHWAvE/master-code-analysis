package jadx.core.utils.files;

import jadx.core.utils.exceptions.JadxRuntimeException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

public class FileUtils {
    private FileUtils() {
    }

    public static void addFileToJar(JarOutputStream jar, File source, String entryName) throws IOException {
        Throwable th;
        BufferedInputStream in = null;
        try {
            JarEntry entry = new JarEntry(entryName);
            entry.setTime(source.lastModified());
            jar.putNextEntry(entry);
            BufferedInputStream in2 = new BufferedInputStream(new FileInputStream(source));
            try {
                byte[] buffer = new byte[8192];
                while (true) {
                    int count = in2.read(buffer);
                    if (count == -1) {
                        break;
                    }
                    jar.write(buffer, 0, count);
                }
                jar.closeEntry();
                if (in2 != null) {
                    in2.close();
                }
            } catch (Throwable th2) {
                th = th2;
                in = in2;
            }
        } catch (Throwable th3) {
            th = th3;
            if (in != null) {
                in.close();
            }
            throw th;
        }
    }

    public static void makeDirsForFile(File file) {
        File dir = file.getParentFile();
        if (dir != null && !dir.exists() && !dir.mkdirs() && !dir.exists()) {
            throw new JadxRuntimeException("Can't create directory " + dir);
        }
    }
}
