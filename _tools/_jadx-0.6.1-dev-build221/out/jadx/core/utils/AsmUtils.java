package jadx.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.objectweb.asm.ClassReader;

public class AsmUtils {
    private AsmUtils() {
    }

    public static String getNameFromClassFile(File file) throws IOException {
        Throwable th;
        FileInputStream in = null;
        try {
            FileInputStream in2 = new FileInputStream(file);
            try {
                String className = new ClassReader(in2).getClassName();
                if (in2 != null) {
                    in2.close();
                }
                return className;
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                if (in != null) {
                    in.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            if (in != null) {
                in.close();
            }
            throw th;
        }
    }
}
