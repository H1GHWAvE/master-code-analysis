package jadx.core.xmlgen;

import jadx.api.ResourceFile;
import jadx.api.ResourceType;
import jadx.core.codegen.CodeWriter;
import java.io.File;
import java.util.List;

public class ResourcesSaver implements Runnable {
    private File outDir;
    private final ResourceFile resourceFile;

    public ResourcesSaver(File outDir, ResourceFile resourceFile) {
        this.resourceFile = resourceFile;
        this.outDir = outDir;
    }

    public void run() {
        if (ResourceType.isSupportedForUnpack(this.resourceFile.getType())) {
            ResContainer rc = this.resourceFile.getContent();
            if (rc != null) {
                saveResources(rc);
            }
        }
    }

    private void saveResources(ResContainer rc) {
        if (rc != null) {
            List<ResContainer> subFiles = rc.getSubFiles();
            if (subFiles.isEmpty()) {
                CodeWriter cw = rc.getContent();
                if (cw != null) {
                    cw.save(new File(this.outDir, rc.getFileName()));
                    return;
                }
                return;
            }
            for (ResContainer subFile : subFiles) {
                saveResources(subFile);
            }
        }
    }
}
