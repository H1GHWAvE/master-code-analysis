package jadx.core.xmlgen;

import jadx.core.codegen.CodeWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jetbrains.annotations.Nullable;

public class ResContainer implements Comparable<ResContainer> {
    @Nullable
    private CodeWriter content;
    private final String name;
    private final List<ResContainer> subFiles;

    private ResContainer(String name, @Nullable CodeWriter content, List<ResContainer> subFiles) {
        this.name = name;
        this.content = content;
        this.subFiles = subFiles;
    }

    public static ResContainer singleFile(String name, CodeWriter content) {
        return new ResContainer(name, content, Collections.emptyList());
    }

    public static ResContainer multiFile(String name) {
        return new ResContainer(name, null, new ArrayList());
    }

    public String getName() {
        return this.name;
    }

    public String getFileName() {
        return this.name.replace("/", File.separator);
    }

    @Nullable
    public CodeWriter getContent() {
        return this.content;
    }

    public void setContent(@Nullable CodeWriter content) {
        this.content = content;
    }

    public List<ResContainer> getSubFiles() {
        return this.subFiles;
    }

    public int compareTo(ResContainer o) {
        return this.name.compareTo(o.name);
    }

    public String toString() {
        return "ResContainer{name='" + this.name + "'" + ", content=" + this.content + ", subFiles=" + this.subFiles + "}";
    }
}
