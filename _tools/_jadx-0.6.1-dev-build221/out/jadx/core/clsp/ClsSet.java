package jadx.core.clsp;

import jadx.core.dex.instructions.args.ArgType;
import jadx.core.dex.nodes.ClassNode;
import jadx.core.dex.nodes.RootNode;
import jadx.core.utils.exceptions.DecodeException;
import jadx.core.utils.exceptions.JadxRuntimeException;
import jadx.core.utils.files.FileUtils;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClsSet {
    private static final String CLST_EXTENSION = ".jcst";
    private static final String CLST_FILENAME = "core.jcst";
    private static final String CLST_PKG_PATH = ClsSet.class.getPackage().getName().replace('.', '/');
    private static final String JADX_CLS_SET_HEADER = "jadx-cst";
    private static final Logger LOG = LoggerFactory.getLogger(ClsSet.class);
    private static final String STRING_CHARSET = "US-ASCII";
    private static final int VERSION = 1;
    private NClass[] classes;

    public void load(RootNode root) {
        List<ClassNode> list = root.getClasses(true);
        Map<String, NClass> names = new HashMap(list.size());
        int k = 0;
        for (ClassNode cls : list) {
            String clsRawName = cls.getRawName();
            if (!cls.getAccessFlags().isPublic()) {
                names.put(clsRawName, null);
            } else if (names.put(clsRawName, new NClass(clsRawName, k)) != null) {
                throw new JadxRuntimeException("Duplicate class: " + clsRawName);
            } else {
                k += VERSION;
            }
        }
        this.classes = new NClass[k];
        k = 0;
        for (ClassNode cls2 : list) {
            if (cls2.getAccessFlags().isPublic()) {
                NClass nClass = getCls(cls2.getRawName(), names);
                if (nClass == null) {
                    throw new JadxRuntimeException("Missing class: " + cls2);
                }
                nClass.setParents(makeParentsArray(cls2, names));
                this.classes[k] = nClass;
                k += VERSION;
            }
        }
    }

    public static NClass[] makeParentsArray(ClassNode cls, Map<String, NClass> names) {
        NClass c;
        List<NClass> parents = new ArrayList(cls.getInterfaces().size() + VERSION);
        ArgType superClass = cls.getSuperClass();
        if (superClass != null) {
            c = getCls(superClass.getObject(), names);
            if (c != null) {
                parents.add(c);
            }
        }
        for (ArgType iface : cls.getInterfaces()) {
            c = getCls(iface.getObject(), names);
            if (c != null) {
                parents.add(c);
            }
        }
        return (NClass[]) parents.toArray(new NClass[parents.size()]);
    }

    private static NClass getCls(String fullName, Map<String, NClass> names) {
        NClass id = (NClass) names.get(fullName);
        if (id == null && !names.containsKey(fullName)) {
            LOG.debug("Class not found: {}", fullName);
        }
        return id;
    }

    void save(File output) throws IOException {
        FileUtils.makeDirsForFile(output);
        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(output));
        OutputStream out;
        try {
            String outputName = output.getName();
            if (outputName.endsWith(CLST_EXTENSION)) {
                save(outputStream);
            } else if (outputName.endsWith(".jar")) {
                out = new ZipOutputStream(outputStream);
                out.putNextEntry(new ZipEntry(CLST_PKG_PATH + "/" + CLST_FILENAME));
                save(out);
                out.close();
            } else {
                throw new JadxRuntimeException("Unknown file format: " + outputName);
            }
            outputStream.close();
        } catch (Throwable th) {
            outputStream.close();
        }
    }

    public void save(OutputStream output) throws IOException {
        DataOutputStream out = new DataOutputStream(output);
        try {
            int i$;
            out.writeBytes(JADX_CLS_SET_HEADER);
            out.writeByte(VERSION);
            LOG.info("Classes count: {}", Integer.valueOf(this.classes.length));
            out.writeInt(this.classes.length);
            NClass[] arr$ = this.classes;
            int len$ = arr$.length;
            for (i$ = 0; i$ < len$; i$ += VERSION) {
                writeString(out, arr$[i$].getName());
            }
            arr$ = this.classes;
            len$ = arr$.length;
            for (int i = 0; i < len$; i += VERSION) {
                NClass[] parents = arr$[i].getParents();
                out.writeByte(parents.length);
                NClass[] arr$2 = parents;
                int len$2 = arr$2.length;
                for (i$ = 0; i$ < len$2; i$ += VERSION) {
                    out.writeInt(arr$2[i$].getId());
                }
            }
        } finally {
            out.close();
        }
    }

    public void load() throws IOException, DecodeException {
        InputStream input = getClass().getResourceAsStream(CLST_FILENAME);
        if (input == null) {
            throw new JadxRuntimeException("Can't load classpath file: core.jcst");
        }
        try {
            load(input);
        } finally {
            input.close();
        }
    }

    public void load(File input) throws IOException, DecodeException {
        InputStream in;
        String name = input.getName();
        InputStream inputStream = new FileInputStream(input);
        try {
            if (name.endsWith(CLST_EXTENSION)) {
                load(inputStream);
            } else if (name.endsWith(".jar")) {
                in = new ZipInputStream(inputStream);
                for (ZipEntry entry = in.getNextEntry(); entry != null; entry = in.getNextEntry()) {
                    if (entry.getName().endsWith(CLST_EXTENSION)) {
                        load(in);
                    }
                }
                in.close();
            } else {
                throw new JadxRuntimeException("Unknown file format: " + name);
            }
            inputStream.close();
        } catch (Throwable th) {
            inputStream.close();
        }
    }

    public void load(InputStream input) throws IOException, DecodeException {
        DataInputStream in = new DataInputStream(input);
        try {
            byte[] header = new byte[JADX_CLS_SET_HEADER.length()];
            int readHeaderLength = in.read(header);
            int version = in.readByte();
            if (readHeaderLength == JADX_CLS_SET_HEADER.length() && JADX_CLS_SET_HEADER.equals(new String(header, STRING_CHARSET)) && version == VERSION) {
                int i;
                int count = in.readInt();
                this.classes = new NClass[count];
                for (i = 0; i < count; i += VERSION) {
                    this.classes[i] = new NClass(readString(in), i);
                }
                for (i = 0; i < count; i += VERSION) {
                    int pCount = in.readByte();
                    NClass[] parents = new NClass[pCount];
                    for (int j = 0; j < pCount; j += VERSION) {
                        parents[j] = this.classes[in.readInt()];
                    }
                    this.classes[i].setParents(parents);
                }
                return;
            }
            throw new DecodeException("Wrong jadx class set header");
        } finally {
            in.close();
        }
    }

    private void writeString(DataOutputStream out, String name) throws IOException {
        byte[] bytes = name.getBytes(STRING_CHARSET);
        out.writeByte(bytes.length);
        out.write(bytes);
    }

    private static String readString(DataInputStream in) throws IOException {
        int len = in.readByte();
        byte[] bytes = new byte[len];
        int count = in.read(bytes);
        while (count != len) {
            int res = in.read(bytes, count, len - count);
            if (res == -1) {
                throw new IOException("String read error");
            }
            count += res;
        }
        return new String(bytes, STRING_CHARSET);
    }

    public int getClassesCount() {
        return this.classes.length;
    }

    public void addToMap(Map<String, NClass> nameMap) {
        NClass[] arr$ = this.classes;
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$ += VERSION) {
            NClass cls = arr$[i$];
            nameMap.put(cls.getName(), cls);
        }
    }
}
