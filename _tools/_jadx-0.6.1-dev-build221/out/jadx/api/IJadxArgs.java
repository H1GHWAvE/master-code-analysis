package jadx.api;

import java.io.File;

public interface IJadxArgs {
    int getDeobfuscationMaxLength();

    int getDeobfuscationMinLength();

    File getOutDir();

    int getThreadsCount();

    boolean isCFGOutput();

    boolean isDeobfuscationForceSave();

    boolean isDeobfuscationOn();

    boolean isFallbackMode();

    boolean isRawCFGOutput();

    boolean isShowInconsistentCode();

    boolean isSkipResources();

    boolean isSkipSources();

    boolean isVerbose();

    boolean useSourceNameAsClassAlias();
}
